# Any of these parameters can be overridden via command line using
# --<option_name> <option_value>
#
# Example, to override output_dir, enter the following command:
# <path_to_exe> --output_dir <new path>

# Directory to store generated chains (no trailing slash).
# This is just a placeholder, it is usually better to set via shell
output_dir = ../../../output/tutB/06

# Number of samples to be generated for each run of the program.
# A sample may consist of multiple chromatin chains if modeling multiple loci
# simultaneously.
ensemble_size = 50

# A trial is an attempt to complete the simulation for all requested samples.
# A negative number indicates to use the maximum limit
max_trials = -1

# Set nuclear sphere confinement. Nodes are rejected if they do not fit within
# nuclear sphere.
#
# Units: Angstroms
nuclear_diameter = 5000

# Number of nodes (monomers) for a chromatin polymer chain.
#
# Format:
# num_nodes_<locus_id> = <positive integer>
#
# where <locus_id> is a unique, sequential 0-based index identifying the
# chromatin chain and <positive integer> is the number of nodes belonging to
# that chain.
#
# Each <locus_id> represents a unique genomic loci.
#
# E.g.:
# num_nodes_0 = 100
#
# will assign 100 chromatin nodes to the 0-th chromatin chain
#
# If <locus_id> is omitted (e.g. - num_nodes = 100), then the <locus_id> will
# default to 0.
#
# Note: to support multiple loci, the model .json 'growth' property must be
# set to 'mloc'
num_nodes_0 = 150

# Nodes are rejected if they collide with an existing node based on this
# length.
#
# Note: node == smallest monomer unit modeled when generating a chromatin
# polymer chain.
#
# Units: Angstroms
max_node_diameter = 110

# Number of points to sample on surface of node sphere when growing a chain
# and deciding where to add the next node.
num_unit_sphere_sample_points = 50

# If '1', then on simulation finish, report the percentage of positions that
# are unique at each monomer index. To disable, set to '0' or remove from this
# configuration file. This information is useful for simulations with
# resampling to assess population diversity.
report_perc_uniq = 1

# Checkpoint interval - resampling will occur on multiples of this value
qc_schedule_lag_slot0 = 20

# The rejection control cutoff weight at each checkpoint is determined as:
#   w_cut = c_min * w_min + c_mean * w_mean + c_max * w_max
#       where w_min, w_mean, and w_max are the minimum, mean, and maximum
#       weight among set of sample weights respectively. Any sample with
#       weight less than w_cut has probability of being regrown from start.
rc_scale_interp_weights_c_min = 0.0
rc_scale_interp_weights_c_mean = 0.25
rc_scale_interp_weights_c_max = 0.75

