# Tutorial B: Quality control introduction

## Overview

This tutorial will cover the concept of *quality control* with regards to modeling chromatin folding.

This tutorial assumes that [tutorial A: folding basics](../A/tutorial.md) has been completed.

Our chromatin folder relies on Monte Carlo sampling, specifically [sequential importance sampling (SIS)](importance_sampling_primer.pdf). Each generated chromatin fold has an associated statistical weight used for correcting sampling bias. Specifically, the weight of each chromatin fold is the ratio of its probability under the *target* distrubtion to its probability under the *sampling* distribution. The weight is used for correcting bias introduced by the divergence of the sampling distribution from the target distribution; this facilitates indirect sampling from the target probability distribution. For example, the [Boltzmann distribution](https://en.wikipedia.org/wiki/Boltzmann_distribution) is a commonly used target distribution in many biophysical applications. In the context of chromatin folding, our *sampling* distribution will be based on the set of local sites which produce a connected, nuclear-confined, self-avoiding polymer, and our *target* distribution will typically be a uniform or a Boltzmann-like energy model.

The term 'quality control' refers to procedures for increasing the statistical quality of the generated chromatin population relative to the target distribution; this typically involves enriching the final set of sample weights. A commonly used quality criterion is the [effective sample size (ESS)](https://en.wikipedia.org/wiki/Effective_sample_size) which approximates the number of samples generated from the target distribution. The ESS is *inversely* proportional to the *variance* of the sample weights, and therefore, *quality control* in the context of SIS is more commonly referred to as *variance reduction*. For more details on Monte Carlo, effective sample size, and variance reduction (i.e. quality control), please see:

* Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science & Business Media, 2008.

This tutorial is organized as follows:

* **Foundations**: Introduces various quality control algorithms
* **Getting started**: Instructions for model baking, etc.
* **B00**: Simulation with *no* quality control
* **B01**: Simulation with *resampling* and homogeneous lag schedule
* **B02**: Simulation with *resampling* and heterogeneous lag schedule
* **B03**: Simulation with *resampling* and homogeneous uniform lag schedule
* **B04**: Simulation with *resampling* and dynamic ESS schedule
* **B05**: Simulation with *resampling* and residual sampler
* **B06**: Simulation with *rejection control* and homogeneous lag schedule

## Foundations

### Importance sampling primer (optional)

It is highly encouraged to review the following [importance sampling primer](importance_sampling_primer.pdf) which provides a theoretical foundation behind the *quality control* mechanisms of **resampling** and **rejection control**.

### Resampling

Our chromatin folder generates a polymer through sequential monomer addition until the target polymer size is reached. During the intermediate growth stages, prior to reaching the target polymer size, there is also an intermediate weight associated with each sample fold. **Resampling**, in the context of SIS, refers to probabilistically culling intermediate samples with low intermediate weights. The culled intermediate samples tend to be replaced with higher weighted intermediate samples. Once the resampling stage is completed, sequential monomer growth resumes as normal using the resampled polymer fragments. Ultimately, resampling tends to enrich the final population weights and increase the ESS; however, as shown in the following animation, some bias may be introduced as the monomer positions among the final population may now be correlated. This bias may be mitigated by limiting the number of resampling stages, running many independent simulations, and also through techniques to be introduced in **tutorial C**.

![Resampling animation](./resample.gif)

The animation demonstrates a 2-D polymer growth process with 2 resampling stages (aka *checkpoints*). In this example, there are 6 polymers. Each polymer is grown sequentially by placing the next monomer node such that the polymer remains connected and self-avoiding. The first resampling stage occurs once all samples have reached a size of 4 nodes. Here, the population is resampled (with replacement) such that polymer chains with higher weights (not shown) are more likely to be selected. After the first resampling stage has completed, we observe that the population diversity has decreased because some (likely higher-weighted) chains were selected multiple times (e.g. the purple polymer fragment was selected 3 times). Sequential growth then resumes until all chains reach a size of 7 monomer nodes. Once all chains are size 7, a second resampling stage occurs. Similar to the first resampling stage, chains with higher weights are more likely to be selected and, again, the resulting population may be less diverse as some of the chains were selected multiple times. Next, sequential growth resumes and 3 more nodes are placed at each chain at which point the hypothetical folding simulation has generated the target polymer size and terminates.

Our chromatin folder with resampling works similarly to this pedagogical example. However, some obvious differences include that our folding simulations are in 3-D Euclidean space and that the polymers must also reside within a confining nucleus.

### Rejection control

An alternative to resampling is the **rejection control** algorithm as described in the following references:

* Liu, J. S., Chen, R., & Wong, W. H. (1998). Rejection control and sequential importance sampling. Journal of the American Statistical Association, 93(443), 1022-1031.
* Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science & Business Media, 2008.

Similar to resampling, rejection control has a user-specified checkpoint schedule. At each checkpoint, the weight of an intermediate sample is compared against a cutoff weight specific to that checkpoint. If the sample's weight is above the cutoff, the sample is retained and growth may proceed until the next checkpoint or the target polymer size is reached. If the sample's weight is below the cutoff, the sample is retained with some probability proportional to how close the sample's weight is to the cutoff. If the sample is not retained (i.e. the checkpoint test fails), the sample must be *regrown* from scratch and must pass all previous checkpoints.

![Rejection control animation](./rejection_control.gif)

The animation demonstrates a 2-D polymer growth process with two rejection control checkpoints. The polymer:

1. Grows until the 1st checkpoint
1. Fails the 1st checkpoint
1. Is regrown from start until it again reaches the 1st checkpoint
1. Passes the 1st checkpoint
1. Grows until the 2nd checkpoint
1. Fails the 2nd checkpoint
1. Is regrown from start until it yet again reaches the 1st checkpoint
1. Passes the 1st checkpoint
1. Grows until the 2nd checkpoint
1. Passes the 2nd checkpoint
1. Grows until completion

Rejection control has the advantages of both increasing ESS and maintaining population diversity (unlike resampling). However, as polymer size is increased and/or more checkpoints are added, the computational cost may quickly become intractable. **Tutorial C** presents a method which can allow rejection control to scale to larger polymers.

## Getting started

This tutorial is designed for running on a Linux operating system. All paths given are relative to the directory containing this [tutorial.MD](./) unless noted otherwise. Any path starting with a `/` (e.g. `/scripts`) is relative to the [root project directory](../../).

Let's get started by inspecting the .bun file for this tutorial: [B.bun](B.bun):

```bash
cat ./B.bun
```

We see output along the following:

```csv
# FILENAME, USAGE, CMDKEY
./00/B00.json, null, tutB00
./01/B01.json, null, tutB01
./02/B02.json, null, tutB02
./03/B03.json, null, tutB03
./04/B04.json, null, tutB04
./05/B05.json, null, tutB05
./06/B06.json, null, tutB06
```

The model [B00](./00/B00.json) is a *reference* model which does *not* perform any quality control procedures. Models [B01](./01/B01.json), [B02](./02/B02.json), [B03](./03/B03.json), [B04](./04/B04.json), and [B05](./05/B05.json) utilize *resampling* as the quality control algorithm. Model [B06](./06/B06.json) utilizes *rejection control* as the quality control algorithm.

Please bake all models with the provided [bake.sh](bake.sh) shell script:

```bash
./bake.sh
```

After baking is finished, note how the script echos the *output directory*

```
Finished tutorial B copy binaries
Please inspect output directory: <OUTPUT_DIRECTORY>
```

Please navigate to the *output directory* to get a feel for the various files created:

```bash
cd <OUTPUT_DIRECTORY>
ls
cd ./build
ls
```

If needed, refer to [tutorial A](../A/tutorial.md) for a review of the baking process.

Now that the models have been baked, let's walk through each of them.

## Tutorial B00: Reference model

**Tutorial B00** provides a comparison *reference* model *without* any quality control. All later tutorials, **B01** through **B06**, will have similar configuration but will also feature a quality control component.

First, list the files under the [00](00) folder:

```bash
cd ./00
ls
```

We see several files:

* [B00.json](00/B00.json)
* [B00.ini](00/B00.ini)
* [run.sh](00/run.sh)

Let's inspect [B00.json](00/B00.json):

```json
{
    "name":"tutB00",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": "null"
    }
}
```

We see that the reference model specifies a single genomic locus with homogeneous monomer nodes (i.e. all nodes have the same diameter). All later sections in this tutorial will introduce quality control algorithms using this reference model; however these quality control algorithms will work with any polymer configuration - see [tutorial A](../A/tutorial.md) for examples of more complex polymer configurations such as modeling multiple genomic loci.

Let's examine the INI file [B00.ini](00/B00.ini):

```bash
cat ./00/B00.ini
```

We see several fields (note, comments are not displayed):

```ini
output_dir = ../../../output/tutB/00
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
```

The only *new* field not encountered in [tutorial A](../A/tutorial.md) is:

* `report_perc_uniq = 1`: This optional field stands for 'Report percent unique'. If set to `1`, the percentage of unique positions at each monomer index, as well as the average percentage, will be reported at the end of the simulation. This feature can be disabled by setting to `0` or by simply removing it from the .ini file. This field is useful for assessing final population diversity.

Now let's run the simulation. In a terminal, enter:

```bash
./00/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. How many polymer samples are generated by each run of the simulation (hint, see `ensemble_size` in the .ini)?
1. How many monomer nodes does each simulated polymer sample have?
1. What is the diameter in Angstroms of each simulated monomer node?
1. What is the reported *effective sample size* (ESS) when running script `./00/run.sh`? Does this value change when re-running the script? If so, why?
1. In `./00/B00.ini`, set `report_perc_uniq = 0` and then run `./00/run.sh`. How does the program output change?
1. With `report_perc_uniq = 1`, run the script `./00/run.sh`. What is the reported *percent unique* at the following (0-based) monomer indices: 0, 51, 92, 111, 140, and 149? What is the reported *average percent unique*?

## Tutorial B01: Resampling with homogeneous lag schedule

This tutorial will show how to configure a simulation with *resampling* for quality control. To begin, inspect the files located in the [01](01) directory:

```bash
cd ./01
ls
```

We see the following files listed:

* [B01.json](01/B01.json)
* [B01.ini](01/B01.ini)
* [run.sh](01/run.sh)

Let's examine [B01.json](01/B01.json):

```json
{
    "name":"tutB01",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            }
        }
    }
}
```

The major difference from **tutorial B00's** JSON is the `"qc"` field is no longer `"null"`. Recall that the `"qc"` field is used to specify the *quality control* algorithm; this field is now configured to use *resampling* for quality control. Let's examine the subfield entries within the JSON `"qc"` field:

* `"type": "resample"`: The `"type"` subfield specifies the specific algorithm to use for quality control. By setting to `"resample"`, this lets the bake process know we want to use *resampling*. Note, the baking process also understands the equivalent shorthand: `"type": "rs"`. In **tutorial B06**, we will change this type to allow *rejection control*.
* `"sampler": { "type": "power", "alpha_slot": "0" }`: When a resampling checkpoint is encountered, the `"sampler"` subfield specifies *how* samples are probabilistically selected by the resampler. Let's examine the subfields for this specific *sampler* configuration:
    *  `"type": "power"`: This informs the baking process to use a *power* sampler. Under a *power* sampler configuration, a sample *i* with weight *w_i* has the following probability of being selected during each (independent) resampling trial: `P_select = w_i^alpha / sum_j{w_j^alpha}`, where `w_i^alpha` is sample *i*'s weight raised to the *alpha* power and `sum_j` is a summation over all samples *j* in the population. The exponent *alpha* is specified in the run-time .ini configuration and will be also discussed when we examine [B01.ini](01/B01.ini). Note, the baking process also understands the equivalent shorthand: `"type": "pow"`. See **tutorial B05** for how to specify a different sampler algorithm.
    *  `"alpha_slot": "0"`: This informs the baking process that the *alpha* exponent, as used by the *power* sampler, is specified in *slot 0* of the command-line or .ini configuration. Specifically, the run-time configuration parameter `qc_sampler_power_alpha_slot0` (within command line or .ini) can be used to specify the value of the exponent used by the power sampler. If instead, the JSON specified the following `"alpha_slot": "1"`, then the corresponding run-time configuration parameter would be `qc_sampler_power_alpha_slot1`. If set to `"alpha_slot": "2"`, then the corresponding run-time configuration parameter would be `qc_sampler_power_alpha_slot2`, etc. The maximum slot value as of this writing is `"7"`. The purpose of *slots* is to disambiguate parameters for models featuring *multiple* quality control algorithms as in **tutorial C**. However, for **tutorial B**, models will only feature at most a single quality control algorithm and, therefore, *all* slot-based parameters will be for slot `"0"`.
* `"schedule": { "type": "lag_homg", "lag_slot": "0" }`: The *schedule* determines *when* resampling checkpoints happen. If a checkpoint is encountered, then the entire population of intermediate polymer folds is resampled according to the previously described *sampler* configuration. Let's examine the subfields for this specific *schedule* configuration:
    * `"type": "lag_homg"`: This subfield informs the baking process of a *homogeneous lag* schedule. Specifically, the user in the run-time configuration will specificy a single, positive integer *lag* value. All resampling checkpoints will occur on *multiples* of this value.
    * `"lag_slot": "0"`: This informs the baking process that the *lag* value, as used by the *homogeneous lag* schedule, is specified in *slot 0* of the command-line or .ini configuration. Specifically, the run-time configuration parameter `qc_schedule_lag_slot0` (within command line or .ini) can be used to specify the lag value used by the schedule. Again, *slots* are used to disambiguate parameters when using multiple quality control algorithms as in **tutorial C**. Since **tutorial B** only features models with at most a single quality control algorithm, all slot-based parameters will be for slot `"0"`.

Next, let's examine [B01.ini](01/B01.ini):

```ini
output_dir = ../../../output/tutB/01
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
qc_sampler_power_alpha_slot0 = 1.0
qc_schedule_lag_slot0 = 20
```

Here are the important differences from **tutorial B00's** INI (besides `output_dir`):

* `qc_sampler_power_alpha_slot0 = 1.0`: This specifies the *alpha* exponent used to determine the selection probability of each sample during each independent resampling trial. Recall, sample *i*'s selection probability is given by the expression: `P_select = w_i^alpha / sum_j{w_j^alpha}`, where `w_i^alpha` is sample *i's* weight raised to the *alpha* power and `sum_j` is a summation over all samples *j* in the population. In this case, the exponent is simply `1.0`. The motivation behind the *alpha* exponent is mainly related to sampling *diversity*. If *alpha > 1*, then samples with higher weights are more likely to be selected (relative to *alpha = 1*); this may increase the final ESS but may also increase the bias due to increased sample correlation (i.e. decreased sample diversity). If *alpha < 1*, then samples with higher weights are less likely to be selected (relative to *alpha = 1*); this may decrease the final ESS but may also decrease the bias due to sample correlation (i.e. increased sample diversity).
* `qc_schedule_lag_slot0 = 20`: Resampling checkpoints will occur on multiples of this *lag* value. This value must be an integer greater than 0. In this case, the lag is `20`, therefore the entire population will be resampled when all polymers have been grown to the each of the following monomer counts: 20, 40, 60, 80, ..., etc.

Now let's run the simulation. In a terminal, enter:

```bash
./01/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. A script may be helpful here. Repeat **exercise 1** with each of the following *alpha* exponents: 0.1, 0.25, 0.5, 0.75, 1.25, 1.5, 1.75, 2.0, 2.5, 3.0, 5.0. Combine all data to generate the following scatter plots (they may be overlaid on each other):
    * Averaged *ESS* x 100 versus *Alpha*
    * Averaged *average percent unique* versus *Alpha*
1. Describe your results from **exercise 2**. Is there a relationship between average *ESS* and *alpha*? Is there a relationship between average *average percent unique* and *alpha*? 
1. Again, a script may be helpful here. Repeat **exercise 1** with each of the following *lag* values: 5, 10, 15, 30, 40, 50, 75, 100, 125. Again, generate the following scatter plots (they may be overlaid on each other):
    * Averaged *ESS* x 100 versus *Lag*
    * Averaged *average percent unique* versus *Lag*
1. Interpret your results from **exercise 4**. Is there a relationship between averaged *ESS* and *lag*? Is there a relationship between averaged *average percent unique* and *lag*?
1. Repeat **exercise 1** using a polymer consisting of 2000 monomer nodes. In addition, generate a scatter plot of *Percent unique* versus *Monomer index* for the following 0-based monomer indices: 0, 50, 100, 250, 500, 750, 1000, 1250, 1500, 1750, 1999.

## Tutorial B02: Resampling with heterogeneous lag schedule

In this tutorial, we allow the resampling checkpoint schedule to be specified using different (*heterogeneous*) lag values. The lag values are specified in a similar fashion to heterogeneous node diameters (see **tutorial A02**) - namely via an external [run-length encoded (RLE)](https://en.wikipedia.org/wiki/Run-length_encoding) file.

To begin, list the files located in the [02](02) directory:

```bash
cd ./02
ls
```

We see the following files listed:

* [B02.json](02/B02.json)
* [B02.ini](02/B02.ini)
* [diam.txt](02/sched.txt)
* [run.sh](02/run.sh)

Let's first examine [B02.json](02/B02.json):

```json
{
    "name":"tutB02",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "lag_hetr",
                "lag_file_slot": "0",
                "lag_slot": "0"
            }
        }
    }
}
```

The major difference from **tutorial B01's** JSON is the `"schedule"` subfield, within the parent `"qc"` field, is now configured for a *heterogeneous* lag schedule. Let's examine the `"schedule"` subfield:

* `"type": "lag_hetr"`: This informs the baking process of a *heterogeneous lag* schedule. Specifically, the user in the run-time configuration can now specify multiple lag values. As will be explained later, only a single lag value will ever be active at any one time.
* `"lag_file_slot": "0"`: This informs the baking process of the parameter slot used for specifying the run-time path to the RLE schedule *file*. Specifically, the run-time parameter `qc_schedule_lag_file_slot0` can be used to set the path to the schedule file.
* `"lag_slot": "0"`: The heterogeneous lag schedule is a generalization of the *homogeneous* schedule from **tutorial B01**. As such, the user may still specify a single lag value via the command line using the slot system. In this case, the run-time argument `qc_schedule_lag_slot0` can still be used to specify a homogeneous lag schedule. However, if a schedule file exists and is successfully loaded, then this argument is ignored.

Next, let's examine [B02.ini](02/B02.ini):

```ini
output_dir = ../../../output/tutB/02
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
qc_sampler_power_alpha_slot0 = 1.0
qc_schedule_lag_file_slot0 = sched.txt
```

The major difference from **tutorial B01's** INI is `qc_schedule_lag_slot0` has now been replaced by:

* `qc_schedule_lag_file_slot0 = sched.txt`: The heterogeneous lag values are specified according to the file [sched.txt](02/sched.txt). Recall, all INI parameters may also be specified by the command line, and further, we recommend all path-based arguments to be specified by command line. In this case, the parameter may be specified on the command line using `--qc_schedule_lag_file_slot0 = [path]` where `[path]` is the file path.

Next, examine the RLE file [sched.txt](02/sched.txt):

```csv
# Run, Lag
# First 100 nodes
75    25
25    5
# Next 50 nodes
10    2
40    20
```

According to [sched.txt](02/sched.txt), the resampling checkpoint schedule is as follows:

* For the first 75 monomer nodes, resampling checkpoints will occur whenever the polymer population reaches a monomer count that is a multiple of 25
* For the next 25 monomer nodes, resampling checkpoints will occur whenever the polymer population reaches a monomer count that is a multiple of 5
* For the next 10 monomer nodes, resampling checkpoints will occur whenever the polymer population reaches a monomer count that is a multiple of 2
* For the final 40 monomer nodes, resampling checkpoints will occur whenever the polymer population reaches a monomer count that is a multiple of 20

Now let's run the simulation. In a terminal, enter:

```bash
./02/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. List the monomer counts that trigger a resampling checkpoint.
1. Modify the checkpoint schedule so that the first 50 monomer nodes have a lag of 10 and the next 50 monomer nodes have a lag of 20. Report the new *ESS* and new *average percent unique*.
1. Without re-baking, convert the resampling schedule into a homogeneous lag schedule with a single lag value of 25. How can this be done by modifying only the .ini? Verify your configuration works as intended by re-running the simulation and listing the monomer counts at which checkpoints are encountered.

## Tutorial B03: Resampling with homogeneous uniform lag schedule

In this tutorial, we will use a schedule that is a hybrid between the *homogeneous* and *heterogeneous* lag schedules. In particular, a single *scalar* lag value is used as in a homogeneous schedule. However, each resampling checkpoint occurs at a *uniform random* location within the corresponding interval defined by the lag argument; this transforms the homogeneous lag into a heterogeneous lag schedule and only requires the user to specify a single scalar lag argument.

Let's start by examining [B03.json](03/B03.json):

```json
{
    "name":"tutB03",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "lag_homg_unif",
                "lag_slot": "0"
            }
        }
    }
}
```

There is only a **single** pertinent difference from tutorial **B01's** JSON configuration, namely the quality control schedule's `"type"` field has been changed from `"lag_homg"` to `"lag_homg_unif"`.

All *other* configuration setttings within the JSON and INI should be identical to **B01**.

To understand the *uniform random* aspect of the schedule, lets use a *lag* argument of `20` with a simple polymer consisting of `100` monomers. From the lag multiples, we can partition the polymer into the following *5* closed intervals: \[1,20\], \[21,40\], \[41, 60\], \[61,80\], and \[81, 100\]. On simulation start, the schedule will select *5* checkpoints by random uniform sampling from each of the intervals. For instance, the following is a valid sequence of checkpoints: *15, 31, 48, 62, 97*. Notice that a single checkpoint is sampled from each lag interval. If the simulation where to be run a second time, a *new* random sequence would be generated - for example: *5, 27, 51, 72, 84*. To contrast with a schedule of `"type": "lag_homg"`, the checkpoints would instead *always* occur at the lag multiples: *20, 40, 60, 80, 100*.

Now let's run the simulation. In a terminal, enter:

```bash
./03/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the sequence of resampling checkpoints for each execution of the program. What are the sampling intervals used based on the lag value?
1. Modify the simulation to use a lag of `10` and repeat previous exercise.

## Tutorial B04: Resampling with dynamic ESS schedule

In the previous resampling configurations, the user *explicitly* specified when checkpoints occurred according to lag values. Alternatively, a schedule may be *implicitly* specified according to the population's *effective sample size* (ESS). In the *dynamic ESS* schedule, a checkpoint is triggered whenever the population ESS falls below a user-specified threshold.

To begin, list the files located in the [04](04) directory:

```bash
cd ./04
ls
```

We see the following files:

* [B04.json](04/B04.json)
* [B04.ini](04/B04.ini)
* [run.sh](04/run.sh)

Let's examine [B04.json](04/B04.json):

```json
{
    "name":"tutB04",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "dynamic_ess",
                "ess_slot": "0"
            }
        }
    }
}
```

The major difference from **tutorial B01's** JSON is the `"schedule"` subfield, within the parent `"qc"` field, is now configured for a *dynamic ESS* schedule. Let's examine the `"schedule"` subfield:

* `"type": "dynamic_ess"`: This informs the baking process of a *dynamic ESS* schedule. Specifically, the user in the run-time configuration can now specify a real-valued effective sample size for triggering checkpoints.
* `"ess_slot": "0"`: This informs the baking process of the parameter slot used for specifying the run-time threshold ESS. Specifically, the run-time parameter `qc_schedule_dynamic_ess_thresh_slot0` can be used to set the threshold ESS at which checkpoints are triggered.

Next examine [B04.ini](04/B04.ini):

```ini
output_dir = ../../../output/tutB/04
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
qc_sampler_power_alpha_slot0 = 1.0
qc_schedule_dynamic_ess_thresh_slot0 = 0.33
```

The major difference from **tutorials B01's** INI is `qc_schedule_lag_slot0` has now been replaced by:

* `qc_schedule_dynamic_ess_thresh_slot0 = 0.33`: This configures the simulation to trigger resampling checkpoints whenever the intermediate population ESS falls below 0.33. It should be noted that the computed ESS value will always be in the range [0,1]. Therefore, an ESS threshold of 1.0 will trigger a checkpoint at all growth stages. Conversely, an ESS threshold of 0.0 will never trigger a checkpoint.

For details on how ESS is computed, please refer to:

* Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science & Business Media, 2008.

Now let's run the simulation. In a terminal, enter:

```bash
./04/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs? Do the monomer counts at which checkpoints are triggered stay the same for each simulation run?
2. A script may be helpful here. Repeat **exercise 1** with each of the following *ESS* schedule thresholds: 0.1, 0.25, 0.4, 0.5, 0.6, 0.75. Generate the following scatter plots (they may be overlaid on each other):
    * Averaged *ESS* x 100 versus *ESS schedule threshold*
    * Averaged *average percent unique* versus *ESS schedule threshold*
3. Interpret your results from **exercise 2**. Is there a relationship between averaged *ESS* and *ESS schedule threshold*? Is there a relationship between averaged *average percent unique* and *ESS schedule threshold*?
4. How would you change the configuration file(s) to allow the schedule's ESS threshold to be specified by slot 1 (instead of slot 0) parameters?

## Tutorial B05: Resampling with residual sampler

Resampling may be configured to use either the *power* sampler (**tutorials B01, B02, B03, B04**) or the *residual* sampler. The *residual* sampler may offer a higher ESS than the *power* sampler for similar polymer configurations. For details on the *residual* sampler, please refer to:

* Page 72 of Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science & Business Media, 2008.
* Douc, Randal, and Olivier Cappe. "Comparison of resampling schemes for particle filtering." Image and Signal Processing and Analysis, 2005. ISPA 2005. Proceedings of the 4th International Symposium on. IEEE, 2005.

To begin, list the files located in the [05](05) directory:

```bash
cd ./05
ls
```

We see the following files:

* [B05.json](05/B05.json)
* [B05.ini](05/B05.ini)
* [run.sh](05/run.sh)

Let's examine [B05.json](05/B05.json):

```json
{
    "name":"tutB05",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "resample",
            "sampler": "residual",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            }
        }
    }
}
```

The major difference from **tutorial B01's** JSON is the `"sampler"` subfield, within the parent `"qc"` field, is now configured to use a *residual* sampler.

Next examine [B05.ini](05/B05.ini):

```ini
output_dir = ../../../output/tutB/05
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
qc_schedule_lag_slot0 = 20
```

The major difference from **tutorial B01's** INI is the line `qc_sampler_power_alpha_slot0` has now been *removed*. Other than the target `output_dir`, all other parameters have the same value.

Now let's run the simulation. In a terminal, enter:

```bash
./05/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. A script may be helpful here. Repeat **exercise 1** with each of the following *lag* schedules: 5, 10, 15, 30, 40, 50, 75, 100, 125. Generate the following scatter plots (they may be overlaid on each other):
    * Averaged *ESS* x 100 versus *Lag*
    * Averaged *average percent unique* versus *Lag*
1. Interpret your results from **exercise 2**. Is there a relationship between averaged *ESS* and *Lag*? Is there a relationship between averaged *average percent unique* and *Lag*? How do the results compare to **exercise 4** of **tutorial B01**?

## Tutorial B06: Rejection control

*Rejection control* is an alternative quality control algorithm. Similar to resampling, rejection control has a user-specified checkpoint schedule. At each checkpoint, the weight of an intermediate sample is compared against a cutoff weight specific to that checkpoint. If the sample's weight is above the cutoff, the sample is retained and growth may proceed until the next checkpoint or the target polymer size is reached. If the sample's weight is below the cutoff, the sample is retained with some probability proportional to how close the sample's weight is to the cutoff. If the sample is not retained (i.e. the checkpoint test fails), the sample must be *regrown* from scratch and must pass all previous checkpoints.

The rejection control algorithm is described in the following references:

* Liu, J. S., Chen, R., & Wong, W. H. (1998). Rejection control and sequential importance sampling. Journal of the American Statistical Association, 93(443), 1022-1031.
* Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science & Business Media, 2008.

Unlike resampling, rejection control has the advantages of both increasing ESS *and* maintaining population diversity. However, as polymer size is increased and/or more checkpoints are added, the computational cost may quickly become intractable. **Tutorial C** presents a method which can allow rejection control to scale to larger polymers and for combining with *resampling*.

To begin, list the files located in the [06](06) directory:

```bash
cd ./06
ls
```

We see the following files:

* [B06.json](06/B06.json)
* [B06.ini](06/B06.ini)
* [run.sh](06/run.sh)

Let's examine [B06.json](06/B06.json):

```json
{
    "name":"tutB06",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": {
            "type": "rc",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "scale": "interp_weights"
        }
    }
}
```

The major difference from the JSON files from previous tutorials is that the `"qc"` field is now configured for *rejection control*. Let's examine the `"qc"` field:

* `"type": "rc"`: This lets the baking process know that we will be using *rejection control* as the quality control algorithm.
* `"schedule": { "type": "lag_homg", "lag_slot": "0" }`: Notice that the checkpoint `"schedule"` subfield is configured in the same exact same fashion as for *resampling*. In this case, the *schedule* is a homogeneous lag schedule with the run-time lag parameter set to slot 0 (see also **tutorial B01**). In general, any checkpoint schedule that works with *resampling*, including *dynamic ESS*, will also work with *rejection control*.
* `"scale": "interp_weights"`: This informs the baking process of the method used for determining the cutoff weight at each rejection control checkpoint. In this case, `"interp_weights"` specifies that the cutoff weight will be determined using the intermediate population's min, mean, and max weights.

Next examine [B06.ini](06/B06.ini):

```ini
output_dir = ../../../output/tutB/06
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
qc_schedule_lag_slot0 = 20
rc_scale_interp_weights_c_min = 0.0
rc_scale_interp_weights_c_mean = 0.25
rc_scale_interp_weights_c_max = 0.75
```

The major difference from **tutorial B01's** INI are that `qc_sampler_power_alpha_slot0` has now been replaced by the parameters:

* `rc_scale_interp_weights_c_min = 0.0`: The interpolation constant associated to the minimum population weight is set to 0.0.
* `rc_scale_interp_weights_c_mean = 0.25`: The interpolation constant associated to the mean population weight is set to 0.25.
* `rc_scale_interp_weights_c_max = 0.75`: The interpolation constant associated to the max population weight is set to 0.75.

These parameters are used for determining the cutoff weight at each rejection control checkpoint according to the formula: `w_cut = c_min * w_min + c_mean * w_mean + c_max * w_max` where `c_min` is specified by parameter `rc_scale_interp_weights_c_min`, `w_min` is the population's minimum weight, `c_mean` is specified by parameter `rc_scale_interp_weights_c_mean`, `w_mean` is the population's mean weight, `c_max` is specified by parameter `rc_scale_interp_weights_c_max`, and `w_max` is the population's maximum weight. It is expected that the interpolation constants `c_min`, `c_mean`, and `c_max` are non\-negative and sum to 1. Recall that at each rejection control checkpoint, samples with weights below the cutoff have a probability of being regrown from scratch *and* must pass all prior checkpoints; this regrowth probability increases as the sample weight decreases.

Now let's run the simulation. In a terminal, enter:

```bash
./06/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. A script may be helpful here. Repeat **exercise 1** with each of the following *lag* schedules: 30, 40, 50, 75, 100, 125. Generate the following scatter plots (they may be overlaid on each other):
    * Averaged *ESS* x 100 versus *Lag*
    * Averaged *average percent unique* versus *Lag*
1. Interpret your results from **exercise 2**. Is there a relationship between averaged *ESS* and *Lag*? Is there a relationship between averaged *average percent unique* and *Lag*? How do the results compare to **exercise 4** of **tutorial B01**?
1. How can the model be configured to instead use a *dynamic ESS schedule* with a threshold of 0.33?
1. Summarize the advantages and disadvantages of *rejection control* versus *resampling*.

## Conclusion

Congratulations on finishing the introduction to quality control tutorial!

One possible question: Is it possible to combine both *resampling* and *rejection control*? In [tutorial C](../C/tutorial.md), we will explore how we can combine these two quality control algorithms within a single simulation.
