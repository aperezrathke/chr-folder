# Tutorial D: Quality control with Delphic resampling

## Overview

This tutorial will cover the concept of *quality control* using Delphic resampling.

This tutorial assumes that [tutorial A: folding basics](../A/tutorial.md), [tutorial B: quality control intro](../B/tutorial.md), and [tutorial C: fractal Monte Carlo](../C/tutorial.md) have been completed.

Delphic resampling extends the *power* sampling framework usable by both [resampling](../B/tutorial.md) quality control policies and fractal [landmark trial runners](../C/tutorial.md).

The basic idea behind Delphic resampling is that a candidate polymer fragment is selected according to its *expected future weight* rather the weight at the current growth step. The expected future weight is computed by repeatedly extending each candidate fragment for a fixed number of additional (i.e. *look-ahead*) growth steps beyond the current step; we may refer to these extended candidate fragments as *trajectories*. The weights from the look-ahead trajectories are then averaged to produce the expected future weight for the original candidate fragment. For a Delphic power sampler, the selection probability of a candidate fragment is then proportional to its expected weight raised to a power alpha.

This tutorial is organized as follows:

* **Getting started**: Instructions for model baking, etc.
* **D01**: Simulation with mean Delphic power resampler
* **D02**: Simulation with median Delphic power resampler
* **D03**: Simulation with Delphic selection landmark trial runner
* **Command line options**: Describes how to set command line options for child simulations using Delphic modules

## Getting started

This tutorial is designed for running on a Linux operating system. All paths given are relative to the directory containing this [tutorial.MD](./) unless noted otherwise. Any path starting with a `/` (e.g. `/scripts`) is relative to the [root project directory](../../).

Let's get started by inspecting the .bun file for this tutorial: [D.bun](D.bun):

```bash
cat ./D.bun
```

We see output along the following:

```
# FILENAME, USAGE, CMDKEY
./00/D01.json, null, tutD01
./01/D01.json, null, tutD02
./02/D02.json, null, tutD03
```

Models [D01](./01/D01.json) and [D02](./02/D02.json) show how to configure a Delphic resampler. Model [D03](./03/D03.json) shows how to incorporate Delphic selection into a fractal Monte Carlo simulation.

Please bake all models with the provided [bake.sh](bake.sh) shell script:

```bash
./bake.sh
```

After baking is finished, note how the script echos the *output directory*

```
Finished tutorial D copy binaries
Please inspect output directory: <OUTPUT_DIRECTORY>
```

Please navigate to the *output directory* to get a feel for the various files created:

```bash
cd <OUTPUT_DIRECTORY>
ls
cd ./build
ls
```

If needed, refer to [tutorial A](../A/tutorial.md) for a review of the baking process.

Now that the models have been baked, let's walk through each of them.

## Tutorial D01: Simulation with mean Delphic power resampler

This tutorial will configure a resampling simulation which uses a Delphic power sampler at each checkpoint. Specifically, we will show how to configure a *parent* simulation which utilizes resampling for quality control. The parent's resampler will in turn generate look-ahead trajectories for each of the population's fragments using a *child* sub-simulation. To begin, inspect the files located in the [01](01) directory:

```bash
cd ./01
ls
```

We see the following files listed:

* [D01.json](01/D01.json)
* [D01.ini](01/D01.ini)
* [D01.child.ini](01/D01.child.ini)
* [run.sh](01/run.sh)

### JSON

First, let's inspect [D01.json](01/D01.json):

```json
{
    "name": "tutD01",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": "canonical",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "delphic_power",
                "summary": "mean_weight",
                "sub": "spec_child",
                "slot": "0"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            }
        }
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": "null"
    },
    "top": "spec_parent"
}
```

We assume that [tutorial C](../C/tutorial.md) has been completed which introduces the concepts of *archetypes*, *parent*, and *child* specification JSON objects. Let's focus on the new *Delphic power* sampler JSON configuration within `spec_parent`; we've isolated the relevant JSON snippet below:

```json
{
    "spec_parent": {
        "qc": {
            "sampler": {
                    "type": "delphic_power",
                    "summary": "mean_weight",
                    "sub": "spec_child",
                    "slot": "0"
                }
        }
    }
}
```

Let's examine each `"sampler"` field:

* `"type": "delphic_power"`: This configures the resampler to use the Delphic look-ahead approach for selecting among fragments at each resample checkpoint. The following equivalent shorthands are also acceptable in place of `"delphic_power"`: `"del_pow"`, `"del_power"`, and `"delphic_pow"`.
* `"summary": "mean_weight"`: This configures the single [summary statistic](https://en.wikipedia.org/wiki/Summary_statistics) used for computing the *expected future weight* of each fragment in the population. In this case `"mean_weight"` means that the expected future weight is computed as the average weight of the fragment's look-ahead trajectories. The shorthand's `"mea_w"`, `"mea_weight"`, and `"mean_w"` are also acceptable.
* `"sub": "spec_child"`: Identical to the [landmark trial runner's](../C/tutorial.md) `"sub"` property, this specifies the name of the *sub*-simulation specification used to grow each of the look-ahead trajectories. In this case, the Delphic power sampler will utilize a sub-simulation according to the `"spec_child"` configuration when extending each population fragment.
* `"slot": "0"`: Similar to usage for *lag* and *alpha* slots, the `"slot"` field is used to disambiguate command-line and INI run-time arguments among potentially many distinct Delphic sub-simulations (as can be the case when using fractal simulations, see **tutorial D03**). The slot field in this case maps to the arguments `delphic_power_alpha_slot0` and `delphic_steps_slot0` which will be discussed in the next **INI** section.

### INI

Now, let's inspect [D01.ini](01/D01.ini):

```ini
output_dir = ../../../output/tutD/01
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
report_perc_uniq = 1
############################
# Delphic resampler
qc_schedule_lag_slot0 = 20

# Set resampler smoothing exponent 'alpha'
delphic_power_alpha_slot0 = 1.0

# Number of look-ahead steps used for future weight estimation
delphic_steps_slot0 = 5

# Configure path to child ini
conf_child_qc = D01.child.ini
```

This configures a simulation as in [tutorial B01](../B/tutorial.md), but instead the resampler uses a Delphic power sampler. Let's examine the relevant resampling fields within the INI:

* `qc_schedule_lag_slot0 = 20`: This field is not new, resampling checkpoints will occur after every `20`th bead is placed.
* `delphic_power_alpha_slot0 = 1.0`: As in a regular *power* sampler, the expected future weight of each fragment is raised to the *alpha* exponent when computing the selection probability during a resampling checkpoint.
* `delphic_steps_slot0 = 5`. This determines the number of beads to extend each fragment in the population when simulating look-ahead trajectories. In this case, each look-ahead trajectory is `5` additional beads beyond the fragment.
* `conf_child_qc = D01.child.ini`: The Delphic power sampler uses a sub-simulation for its look-ahead trajectories. This field specifies the configuration files used by the Delphic power sampler's child sub-simulation. Note, this is similar to `conf_child` or (equivalently) `conf_child_tr` when specifying the configuration file for a landmark trial runner's sub-simulation.

Next, let's briefly inspect the child ini [D01.child.ini](01/D01.child.ini):

```ini
ensemble_size = 50
max_trials = 100
```

There should be nothing new in the child INI configuration. Each fragment in the parent population will have `ensemble_size = 50` trajectories computed to estimate the expected future weight during resampling.

Now let's run the simulation. In a terminal, enter:

```bash
./01/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Try the following *ensemble sizes* for the Delphic child configuration: 5, 10, 25, 75, 100. What are the reported effective sample sizes and total CPU times for each simulation run?
1. Now, try the following Delphic *step* sizes: 1, 2, 4, 8, 16, 32. What are the reported effective sample sizes and total CPU times for each simulation run?

## Tutorial D02: Simulation with median Delphic power resampler

In this tutorial, we make a single change: the Delphic sampler JSON `"summary"` field is switched from `"mean_weight"` to `"median_weight"`:

See [D02.json](02/D02.json) and compare with [D01.json](01/D01.json):

```JSON
{
    "name": "tutD02",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": "canonical",
        "qc": {
            "type": "resample",
            "sampler": {
                "type": "delphic_power",
                "summary": "median_weight",
                "sub": "spec_child",
                "slot": "0"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            }
        }
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": "null"
    },
    "top": "spec_parent"
}
```

The `"median_weight"` summary may also be specified using any of the following shorthands: `"med_w"`, `"med_weight"`, and `"median_w"`.

The simulation is identical to tutorial **D01** except that the expected future weight at each population fragment is computed as the *median* weight of its corresponding look-ahead trajectories.

The INI configurations are not substantively different to tutorial **D01**.

Now let's run the simulation. In a terminal, enter:

```bash
./02/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Try the following *ensemble sizes* for the Delphic child configuration: 5, 10, 25, 75, 100. What are the reported effective sample sizes and total CPU times for each simulation run?
1. Now, try the following Delphic *step* sizes: 1, 2, 4, 8, 16, 32. What are the reported effective sample sizes and total CPU times for each simulation run?

## Tutorial D03: Simulation with Delphic selection landmark trial runner

We will now use Delphic selection within a fractal Monte Carlo simulation. The configuration is similar to [tutorial C01](../C/tutorial.md) which featured a *parent* simulation with a *resampling* child. In **C01**, the parent's landmark trial runner utilized a *power* selection which uses the weight of the child fragment to determine the selection probability in a similar fashion as a power resampler. In this tutorial, we will instead use a *Delphic power selection* in the parent's landmark trial runner.

### JSON

Let's inspect [D03.json](03/D03.json):

```json
{
    "name": "tutD03",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "delphic_power",
                "summary": "mean_weight",
                "sub": "spec_child",
                "slot": "0"
            },
            "sub": "spec_child"
        },
        "qc": "null"
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "1"
            }
        }
    },
   "top": "spec_parent"
}
```

Again, the JSON is similar to tutorial **C01**, but we have replaced the `"select"` property in the parent's landmark trial runner with a Delphic power selector. The relevant JSON snippet is isolated here:

```json
{
    "spec_parent": {
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "delphic_power",
                "summary": "mean_weight",
                "sub": "spec_child",
                "slot": "0"
            },
            "sub": "spec_child"
        }
    }
}
```

Notice that there are now two (2) `"sub"` fields! The landmark trial runner uses a child sub-simulation to generate candidate fragments; the Delphic power selection also uses an identifically configured child-simulation to perform look-ahead trajectories on each of the child sub-simulation's candidate fragments. Though in this tutorial both child sub-simulation JSON configurations are the same, this is not required and one may create distinct JSON specifications for each of the child sub-sims.

In this case, there are no fields that have not been encountered in previous tutorials. Though we are configuring the landmark trial runner's *selection*, the properties have identical definitions to the quality control Delphic *resampler* fields from the earlier tutorials.

### INI

Let's inspect [D03.ini](03/D03.ini):

```ini
output_dir = ../../../output/tutD/03
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1

############################
# Delphic landmark

qc_schedule_lag_slot0 = 70

# Set resampler smoothing exponent 'alpha'
delphic_power_alpha_slot0 = 1.0

# Number of look-ahead steps used for future weight estimation
delphic_steps_slot0 = 5

# Configure paths to child INIs
conf_child_tr = D03.child.ini
conf_child_tr_sel = D03.child.ini
```

The configuration is similar to tutorial **C01** but we must now configure the landmark trial runner's Delphic selection. Similar to earlier tutorials, we set the alpha exponent and look-ahead steps using `delphic_power_alpha_slot0 = 1.0` and `delphic_steps_slot0 = 5` respectively.

We must also specify the paths to both the *trial runner's* and the trial runner *selection's* child sub-simulation configurations. These paths can be disambiguated using `conf_child_tr` for the *trial runner* and `conf_child_tr_sel` for the trial runner *selection*. In this case, both values defer to the same ini configuration file, but, again, this is not a requirement.

### Putting it all together

In summary, we have a 2-D fractal simulation. The top-level *parent* simulation uses a landmark trial runner which defers to a *child* sub-simulation to generate an ensemble of candidate fragments. The parent selects a child candidate using a Delphic selection policy. The Delphic selection policy also uses a *child* sub-simulation to perform look-ahead trajectories at each candidate fragment for estimating the expected future weight. A single child fragment is then selected from each child ensemble with probability based on its expected future weight.

Now let's run the simulation. In a terminal, enter:

```bash
./03/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. How can you overload the `ensemble_size` for the Delphic child configuration only? (hint: see **Command line options** section). As before, try the following ensemble sizes for the Delphic sub-simulation: 5, 10, 25, 75, 100. What are the reported effective sample sizes and total CPU times for each simulation run?
2. Now, try the following Delphic *step* sizes: 1, 2, 4, 8, 16, 32. What are the reported effective sample sizes and total CPU times for each simulation run?

## Command line options

Before reading this section, we recommend to review the *Commnand line options* section from [tutorial C](../C/tutorial.md). Recall, without modification, command line arguments are *only* available to the *top-level* simulation and are not passed to any *child* sub-simulations. In this section we will describe how to disambiguate command line arguments intended for various child sub-simulations, including Delphic resampling.

Let's look at some examples. In tutorial **D01**, we have a canonical trial runner with a Delphic power resampler for quality control. To configure the ensemble size for the top-level (i.e *parent*) ensemble via command line, we simply use `--ensemble_size <+integer>`; for instance, `--ensemble_size 10` will set the parent ensemble size to `10`. How do we instead set the ensemble size for the Delphic power sampler's child sub-simulation via command line? We use `--+ensemble_size 10`. Notice the difference? The `+` symbol disambiguates whether the argument is intended for the top-level simulation (*no* `+`) or the quality control sub-simulation (`+`).

In tutorial **D03**, we have a 2-D fractal simulation, with two child sub-simulations: 1) the landmark trial runner's sub-simulation, 2) the landmark trial runner Delphic selection's sub-simulation. How do we set the ensemble size for each of these simulations via command line? As before, the top-level parent ensemble size can be set like so `--ensemble_size <...>`. Also, as described in [tutorial C](../C/tutorial.md), the landmark trial runner's sub-simulation argument can be set like so: `--.ensemble_size <...>`. Now, to set the trial runner *selection's* sub-simulation, we can enter `--*ensemble_size <...>`. For example, the command line snippet `--ensemble_size 10 --.ensemble_size 20 --*ensemble_size 25` will configure the top-level parent ensemble size to `10`, the landmark trial runner's sub-simulation ensemble size to `20`, and the trial runner Delphic selection's sub-simulation ensemble size to `25`.

Here is the general pattern used for targeting arguments (apologies for unescaped special characters): `--<(.|*|+)*><name> <value>` where:

* `--` means the argument has a value
* `.` is 0 or more `.` characters and encodes arguments intended for the landmark trial runner sub-simulation, with the number of `.` characters being the fractal depth.
* `*` is 0 or more `*` characters and encodes arguments intended for the trial runner's *selection* sub-simulation (e.g. *Delphic power selection*), where the number of `*` characters is again related to the fractal depth.
* `+` is 0 or more `+` characters and encodes arguments intended for the *quality control* sub-simulation (e.g. *Delphic power resampler*), where the number of `+` characters is related to the fractal depth.
* `<(.|*|+)*>`: The grouping means these characters `.`, `*`, or `+` can be placed in *any order* and *amount* to target a specific child sub-simulation at any depth. We will revisit with some examples below.
* `<name>` is the name of the command line argument (e.g. `ensemble_size`)
* `<value>` is the value associated to the argument

Note, the group expression pattern with escaped characters would be something like: `--<(\.|\*|\+)*> ...`

Let's examine the group pattern a bit more. How do we interpret the following `--.+ensemble_size 10`? This implies the top-level simulation has a landmark trial runner. The first `.` targets the *landmark trial runner's* sub-simulation. The next `+` character targets the *quality control* sub-simulation of the *landmark trial runner's* sub-simulation. For instance, if the landmark trial runner's sub-simulation uses a *Delphic power resampler* for quality control. Ultimately, this would set the ensemble size for the *Delphic power resampler's* sub-simulation to `10` (where the Delphic resampler is the quality control for the landmark trial runner's sub-simulation).

How about `--+.ensemble_size 10`? The first `+` targets the top-level *quality control's* sub-simulation. For instance, the top-level simulation may have a *Delphic power resampler* for quality control. The next `.` implies that the Delphic power resampler's sub-simulation has a landmark trial runner and the `.` targets this trial runner's sub-simulation.

How about the syntax `--*+ensemble_size 10`? The first `*` character will target the top-level trial runner selection's sub-simulation. The next `+` will target the *quality control's* sub-simulation (e.g. Delphic power resampler) within the *selection's* sub-simulation.

Note, the *`.`, `*`, or `+` prefix syntax* only applies to *command line* arguments. Arguments specified via INI should *never* be prefixed with these special characters!

### Exercises

Describe which sub-simulation arguments are targeted by each of the following expressions:

1. `--.*+ensemble_size 10`
2. `--*+.ensemble_size 10`
3. `--..+ensemble_size 10`
4. Give examples of JSON configurations which would be consistent with the previous expressions.

## Conclusion

Congratulations on finishing the Delphic resampling tutorial!

In the next [tutorial E](../E/tutorial.md), we will explore how to simulate chromatin-to-chromatin proximity interactions.
