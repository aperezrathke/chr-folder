# Tutorial C: Quality control with fractal Monte Carlo

## Overview

This tutorial will cover the concept of *quality control* using fractal Monte Carlo simulation.

This tutorial assumes that [tutorial A: folding basics](../A/tutorial.md) and [tutorial B: quality control intro](../B/tutorial.md) have been completed.

Fractal Monte Carlo allows multiple quality control algorithms to be combined in a nested fashion. Specifically, parent simulations may run child sub-simulations where the parent and its children may be running different quality control algorithms. For example, the parent simulation may use *resampling* while the child sub-simulations may instead use *rejection control*. The *fractal* nature of the system allows the child sub-simulations to also run their own sub-simulations using a possibly different quality control scheme. The depth of this recursive process can be configured arbitrarily by the user.

The core advantages of fractal Monte Carlo are that it allows multiples passes of population weight enrichment while also maintaining sample diversity. Ultimately, the goal of fractal Monte Carlo sampling is to enable efficient *exploration* and *exploitation* of the **global** probabilistic landscape.

![https://rosettacode.org/wiki/Fractal_tree](fractal_tree.svg)

The basic intuition behind the fractal sampling scheme is represented by the [fractal tree](https://rosettacode.org/wiki/Fractal_tree) graphic. The *leaves* of the tree represent the child sub-simulations which are only seeking to gain coverage of their local conformational landscape. The parents of the child simulations, represented by the *small branches*, have a slightly more global context as their quality control is based on the results of the child simulations. Similarly, there may be grandparent simulations, represented by *large branches or trunks*, which aggregate the parent simulations and thereby gain a broad, global perspective of the conformational landscape.

This tutorial is organized as follows:

* **Getting started**: Instructions for model baking, etc.
* **C00**: Simulation with *no* quality control
* **C01**: Simulation of parent with nested resampling child
* **C02**: Simulation of grandparent with nested resampling parent with nested resampling child
* **C03**: Simulation of grandparent with nested resampling parent with nested rejection control child
* **Command line options**: Describes how to set command line options for child simulations

## Getting started

This tutorial is designed for running on a Linux operating system. All paths given are relative to the directory containing this [tutorial.MD](./) unless noted otherwise. Any path starting with a `/` (e.g. `/scripts`) is relative to the [root project directory](../../).

Let's get started by inspecting the .bun file for this tutorial: [C.bun](C.bun):

```bash
cat ./C.bun
```

We see output along the following:

```
# FILENAME, USAGE, CMDKEY
./00/C00.json, null, tutC00
./01/C01.json, null, tutC01
./02/C02.json, null, tutC02
./03/C03.json, null, tutC03
```

The model [C00](./00/C00.json) is a *reference* model which does *not* perform any quality control procedures. Models [C01](./01/C01.json), [C02](./02/C02.json), and [C03](./03/C03.json) utilize some form of fractal Monte Carlo sampling.

Please bake all models with the provided [bake.sh](bake.sh) shell script:

```bash
./bake.sh
```

After baking is finished, note how the script echos the *output directory*

```
Finished tutorial C copy binaries
Please inspect output directory: <OUTPUT_DIRECTORY>
```

Please navigate to the *output directory* to get a feel for the various files created:

```bash
cd <OUTPUT_DIRECTORY>
ls
cd ./build
ls
```

If needed, refer to [tutorial A](../A/tutorial.md) for a review of the baking process.

Now that the models have been baked, let's walk through each of them.

## Tutorial C00: Reference model

*Note, this model should be similar if not identical to [tutorial B00](../B/tutorial.md).*

**Tutorial C00** provides a comparison *reference* model *without* any quality control (and hence no fractal Monte Carlo). All later tutorials, **C01** through **C03**, will have similar configuration but will also feature a quality control component. 

First, list the files under the [00](00) folder:

```bash
cd ./00
ls
```

We see several files:

* [C00.json](00/C00.json)
* [C00.ini](00/C00.ini)
* [run.sh](00/run.sh)

Let's inspect [C00.json](00/C00.json):

```json
{
    "name":"tutC00",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif",
        "qc": "null"
    }
}
```

We see that the reference model specifies a single genomic locus with homogeneous monomer nodes (i.e. all nodes have the same diameter). All later sections in this tutorial will introduce fractal Monte Carlo sampling using this reference model; however the fractal Monte Carlo algorithms will work with any polymer configuration - see [tutorial A](../A/tutorial.md) for examples of more complex polymer configurations such as modeling multiple genomic loci.

Let's examine the INI file [C00.ini](00/C00.ini):

```bash
cat ./00/C00.ini
```

We see several fields (note, comments are not displayed):

```ini
output_dir = ../../../output/tutC/00
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
```

For a description of these .ini configuration fields, please refer to [tutorial B00](../B/tutorial.md).

Now let's run the simulation. In a terminal, enter:

```bash
./00/run.sh
```

### Exercises

Please see **exercises** for [tutorial B00](../B/tutorial.md).

## Tutorial C01: Simulation of parent with nested resampling child

This tutorial will configure a fractal simulation of depth 2. Specifically, we will show how to configure a *parent* simulation which utilizes a *child* sub-simulation. The parent simulation uses no quality control; however, each child sub-simulation uses resampling for its quality control. To begin, inspect the files located in the [01](01) directory:

```bash
cd ./01
ls
```

We see the following files listed:

* [C01.json](01/C01.json)
* [C01.ini](01/C01.ini)
* [C01.child.ini](01/C01.child.ini)
* [run.sh](01/run.sh)

### JSON

In the following sections we will examine in detail the JSON bake configuration file [C01.json](01/C01.json). Specifically, over the next few sections, we will cover the following concepts:

* **Archetypes**: a new JSON object used for avoiding redundant specification of common properties
* **Landmark trial runners**: a new trial runner type used for linking parent and child simulations
* **Parent simulation specification**: the top-level simulation which uses a landmark trial runner
* **Child simulation specification**: the nested sub-simulation utilized by the parent simulation's landmark trial runner

First, let's inspect [C01.json](01/C01.json):

```json
{
    "name": "tutC01",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        },
        "qc": "null"
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "1"
            }
        }
    },
   "top": "spec_parent"
}
```

#### Archetype JSON

The JSON is substantially more complex than those encountered in tutorials [A](../A/tutorial.md) or [B](../B/tutorial.md)! We should notice a new JSON object called `"arch0"`; the "arch" prefix is short for "archetype". This archetype object is used for defining the polymer configuration properties *shared* by multiple simulation types. In this case, the parent simulation (defined by `"spec_parent"`) and the child simulation (defined by `"spec_child"`) both have their `"arch"` fields set to the name of this JSON object.

Note, the name of the archetype JSON object can be arbitrarily chosen by the user, so long as it is assigned to the `"arch"` field of the relevant simulations.

The archetype object is *optional* and is simply used to avoid re-specifying common configuration properties across simulation types. Specifically, if the JSON simulation specification (e.g. `"spec_parent"`) does not explicitly define an expected property (e.g. `"diameter"`), then the value of the property is obtained from the associated archetype object if available. In the case that a property is defined in *both* the simulation specification *and* the archetype, the simulation specification is used (i.e. the specification *overrides* the archetype value). In other words, the archetype object can be used to specify the *default* bake configuration used by the simulation specifications.

Let's examine the fields within the `"arch0"` **archetype** object, all these fields should be familiar from tutorials [A](../A/tutorial.md) and [B](../B/tutorial.md):

```json
{
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    }
}
```

* `"diameter": "homg"`: Any simulation specification using this archetype will by default have homogeneous monomer nodes (all nodes assumed to have same radius).
* `"growth": "sloc"`: Any simulation specification using this archetype will by default have a single-locus growth model.
* `"intr_chr" : "null"`: Any simulation specification using this archetype will by default have a null chromatin-to-chromatin interaction policy.
* `"energy" : "unif"`: Any simulation specification using this archetype will by default have a uniform energy model.

#### Landmark trial runner JSON

If we look within the JSON's `"spec_parent"` simulation specification, we now see that the `"trial_runner"` is no longer of `"type": "canon"`. Instead, the  `"trial_runner"` is of `"type": "lmrk"` where "lmrk" is short for "landmark".

The landmark trial runner is the *key* to **fractal Monte Carlo**!

The landmark trial runner grows a single polymer sample by running a sub-simulation to generate a set of candidate polymer fragments, where a fragment typically consists of multiple monomer nodes. It then selects a single fragment from the set of candidate fragments. The next fragment of the polymer is grown in a similar fashion, but only fragments that maintain connectivity with the previous fragment are considered. The process repeats until the target polymer length is reached. The process is similar to resampling-based quality control except, to maintain population diversity, only a single fragment is retained at each checkpoint and the rest are culled. Furthermore, as we will see in tutorials **C02** and **C03**, the sub-simulations may in turn have their own landmark trial runner and quality control algorithms and thereby define a recursive (i.e. fractal) polymer growth process.

Let's examine the `"trial_runner"` field within `"spec_parent"`:

```json
{
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        }
}
```

* `"type": "lmrk"`: This sets the trial runner to be a "landmark" trial runner (as opposed to "canonical") . The following long-hand is also acceptable: `"type": "landmark"`
* `"schedule": { "type": "lag_homg", "lag_slot": "0"}`: This specifies the *checkpoint* schedule for the landmark trial runner. It is specified in the same manner as the `"schedule"` field for *quality control* objects (see [tutorial B](../B/tutorial.md)). To recap, the landmark trial runner will use a sub-simulation to grow a set of candidate fragment until a checkpoint is encountered. Once a checkpoint is reached, only a single fragment is retained. The retained fragment is then used as a base for growing the next set of candidate fragments, again with only a single fragment retained at the next checkpoint. The process repeats until the entire polymer is grown. In this case, the schedule is a homogeneous lag schedule with landmark checkpoints specified by the `qc_schedule_lag_slot0` run-time argument.
* `"select": { "type": "power", "alpha_slot": "0" }`: This specifies the *selection* policy used for determining which candidate fragment is retained at each landmark checkpoint. In this case, a fragment is selected according to the same *power* sampler routine used in resampling-based quality control. Recall from [tutorial B](../B/tutorial.md), under a *power* sampler configuration, a fragment *i* with weight *w_i* has the following probability of being selected: `P_select = w_i^alpha / sum_j{w_j^alpha}`, where `w_i^alpha` is fragment *i*'s weight raised to the *alpha* power and `sum_j` is a summation over all *j* candidate fragments. The exponent *alpha* is specified using the run-time `qc_sampler_power_alpha_slot0` argument.
* `"sub": "spec_child"`: This field specifies the name of the *sub*-simulation specification used to grow each of the candidate fragments. In this case, the landmark trial-runner will utilize a sub-simulation according to the `"spec_child"` configuration when growing candidate fragments to each landmark checkpoint.

#### Landmark trial runner schedules

For simplicity, this tutorial will use a landmark trial runner schedule of type `"lag_homg"`. However, this may introduce some periodicity, so it is recommended to instead use a schedule of type `"lag_homg_unif"` (see [tutorial B03](../B/tutorial.md)) or even `"lag_hetr"`. To configure the trial runner for a homogeneous uniform lag schedule, simply assign `"lag_homg_unif"` to the schedule's `"type"` field. For example:

```json
{
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg_unif",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        }
}
```

#### Parent simulation JSON

Now we are ready to examine each of the JSON simulation specifications. Let's start with the parent simulation as defined by `"spec_parent"`:

```json
{
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        },
        "qc": "null"
    }
}
```

* `"arch": "arch0"`: This optional field is used for specifying default simulation properties. In this case, any expected property not explicitly defined within `"spec_parent"` is instead obtained from the `"arch0"` JSON object. See the section **Archetype JSON** for further reference. Ultimately, this defaults the specification to be configured as single-locus, homogeneous monomer, uniform energy, and with null chromatin-to-chromatin interactions.
* `"trial_runner": {...}`: As discussed in the  **Landmark trial runner JSON** section, the parent simulation is configured to use a landmark trial runner which defers to child sub-simulations for growing each polymer sample to scheduled checkpoints.
* `"qc": "null"`: The parent simulation specification does not use any direct form of quality control.

Also, we inform the baking process of which specification is the top-level (i.e. parent) simulation by setting `"top": "spec_parent"`.

#### Child simulation JSON

Next, let's examine the JSON of the child simulation as defined by "`spec_child`":

```json
{
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "1"
            }
        }
    }
}
```

* `"arch": "arch0"`: As also used by the parent simulation, this optional field is used for specifying default simulation properties. In this case, any expected property not explicitly defined within `"spec_child"` is instead obtained from the `"arch0"` JSON object. See the section on **archetype** objects for further reference. Ultimately, this defaults the specification to be configured as single-locus, homogeneous monomer, uniform energy, and with null chromatin-to-chromatin interactions.
* `"trial_runner": "canon"`: The child simulation relies on a canonical trial runner which directly grows polymers by sequentially placing monomers which satisfy confinement, excluded volume, and connectivity constraints. The canonical trial runner has no dependence on sub-simulations. **Note, all *landmark* trial runners must ultimately rely on a nested sub-simulation that utilizes a canonical trial runner!** Specifically, the simulation at the highest fractal depth must be configured with a canonical trail runner - see also tutorials **C02** and **C03**.
* `"qc": {...}"`: For quality control, the child simulation is configured to use resampling with a homogeneous lag schedule and a power sampler, see [tutorial B](../B/tutorial.md). The schedule lag and sampler exponent alpha are specified by the run-time arguments `qc_schedule_lag_slot1` and `qc_sampler_power_alpha_slot1` respectively. Note the use of slot `1` to avoid conflicting with the corresponding parent simulation arguments which are slot `0`.

Recall, the child simulation is used by the parent simulation's landmark trial runner for growing candidate polymer fragments. This wiring is achieved by setting the field `"sub": "spec_child"` within the parent's trial runner object.  

#### JSON summary

In summary, the JSON specifies a top-level parent simulation in `"spec_parent"` and a child sub-simulation in `"spec_child"`. The parent and child simulations are configured with the same archetype for their common properties: `"arch": "arch0"`. The chromatin folder is aware of the top-level parent simulation through the setting `"top": "spec_parent"`. The parent simulation grows polymers using a landmark trial runner. The landmark trial runner incrementally grows polymer samples by selecting from candidate fragments generated using the child sub-simulation. The landmark trial runner uses the child sub-simulation specified according to its field: `"sub": "spec_child"`. The parent simulation has no quality control algorithm; however, the child sub-simulation uses resampling to enrich the importance weights of the generated candidate fragments.

### INI

Unlike previous tutorials, we now see *two* INI run-time configuration files listed:

* [C01.ini](01/C01.ini)
* [C01.child.ini](01/C01.child.ini)

Why are there are two INIs instead of just one? The answer: because the JSON specified two simulations: `"spec_parent"` and `"spec_child"`. The configuration [C01.ini](01/C01.ini) specifies the run-time arguments for the *parent* simulation. The configuration [C01.child.ini](01/C01.child.ini) specifies the run-time arguments for the *child* sub-simulation.

#### Parent simulation INI

Let's examine the [C01.ini](01/C01.ini) configuration for the parent simulation:

```ini
output_dir = ../../../output/tutC/01
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
### Landmark:
qc_schedule_lag_slot0 = 70
qc_sampler_power_alpha_slot0 = 1.0
conf_child = C01.child.ini
```

Note that lines beginning with `#` are considered comments and are ignored by the INI parser.

The only fields requiring explanation are those below the `### Landmark` comment. As implied by the comment, all these fields are used for configuring the parent simulation's landmark trial runner:

* `qc_schedule_lag_slot0 = 70`: This configures the parent's landmark trial runner to have checkpoints after every `70` monomers have been grown and/or when the target polymer length is reached. Specifically, *for each polymer sample*, the landmark trial runner will run its child sub-simulation to generate candidate polymer fragments each of length 70. For each polymer sample, it will select a single fragment to append to that polymer chain. The process repeats until the target polymer length is reached for all polymer samples. In this case, with a target polymer length of `num_nodes_0 = 150`, the first landmark checkpoint will occur at polymer length 70, the next landmark checkpoint will occur at polymer length 140, and the final checkpoint will occur at 150.
* `qc_sampler_power_alpha_slot0 = 1.0`: Recall, the parent's landmark trial runner uses a power sampler to select among the pool of candidate fragments generated by the child sub-simulation. This specifies the *alpha* exponent used by the power sampler. Please refer to previous description of the power sampler selection scheme in the **Landmark trial runner JSON** section.
* `conf_child = C01.child.ini`: This lets the landmark trial runner know which INI configuration file to use for the child sub-simulation. Note, any relative path within the INI configuration is relative to the directory containing the INI file itself. In this case, the file `C01.child.ini` is assumed to reside in the same directory as the parent INI `C01.ini`.

#### Child simulation INI

Let's examine the [C01.child.ini](01/C01.child.ini) configuration for the child sub-simulation:

```ini
ensemble_size = 50
max_trials = 100
### Resampling:
qc_schedule_lag_slot1 = 35
qc_sampler_power_alpha_slot1 = 1.0
```

**Notice that the child INI is relatively small as it defers to the parent's [C01.ini](01/C01.ini) for any *missing* properties!**

* `ensemble_size = 50`: For each parent polymer sample, the child sub-simulation generates a candidate pool of polymer fragments, one of which is selected by the parent's landmark trial runner at each checkpoint. The number of candidate fragments generated is `50`.
* `max_trials = 100`: Recall, the child sub-simulation is generating candidate fragments given the current state of a parent polymer. Given the parent polymer state, it may not be feasible to generate fragments of the desired length, in which case the sub-simulation should fail after a relatively low number of trials. In this case, the child sub-simulation will attempt up to `100` trials to generate the desired pool of candidate fragments. If the pool of candidate fragments cannot be generated in this number of trials, the parent polymer sample will need to be regrown from scratch during the parent simulation's next trial.

The remaining INI fields are used for configuring the child's *resampling-based* quality control:

* `qc_schedule_lag_slot1 = 35`: This instructs the child sub-simulation to perform resampling (see [tutorial B](../B/tutorial.md)) after every `35` monomer nodes have been grown.
* `qc_sampler_power_alpha_slot1 = 1.0`: As in previous descriptions of the sampler field (see [tutorial B](../B/tutorial.md)), this sets the *alpha* exponent for the child's power resampler to be `1.0`.

#### INI summary

In summary, there are two INI configuration files corresponding to the parent and child simulations respectively. The parent simulation is configured by [C01.ini](01/C01.ini) and the child sub-simulation is configured via [C01.child.ini](01/C01.child.ini). For any queried child properties not specified directly within the [C01.child.ini](01/C01.child.ini), the value from the parent's [C01.ini](01/C01.ini) will be used.

Note, the top-level parent INI configuration **must** be specified by the command line argument `--conf <path to INI file>`. The child configuration may be specified via the `conf_child` field within the parent INI file or by command line argument `--conf_child <path to INI file>`. The command line may only be used to specify the child INI for fractal simulations of depth 2 as in this tutorial; for depths > 2, the child INI paths must be specified within the corresponding parent INI files. See the [run.sh](/01/run.sh) shell script for command line syntax.

### Putting it all together

The JSON and INI files specify a fractal simulation of **depth 2**. Specifically, there is a top-level *parent* simulation which utilizes a *child* simulation for generating candidate fragments at each parent polymer sample.

Within the JSON, the **archetype** field is used to avoid specifying redundant properties between the parent and child simulations.

The parent simulation utilizes a **landmark trial runner** which has a checkpoint schedule similar to the quality control field. One important difference is that landmark trial runner schedules must be *deterministic*; therefore, the schedule types `"lag_homg"`, `"lag_homg_unif"`, and `"lag_hetr"` are compatible but `"dynamic_ess"` is not!

For each parent polymer sample, the landmark trial runner uses the child sub-simulation to generate a set of candidate fragments. For each parent polymer sample, only one fragment is selected at each landmark checkpoint and appended to the growing parent sample. The process is repeated until the all parent polymer samples reach their target size.

Ultimately, the fractal nature has the benefit of balancing the statistical enrichment of the generated fragments at the child simulation while also maintaining conformational diversity at the parent simulation. In later tutorials **C02** and **C03**, we will investigate simulations with fractal depth 3 and which may also combine both resampling and rejection control. 

Now let's run the simulation. In a terminal, enter:

```bash
./01/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Modify the reference [C00.json](00/C00.json) from previous **tutorial C00** to use an `"arch"` field. Name the archetype object `"arch_ref"`. Re-bake and verify the simulation runs as expected.
1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. List the monomer counts that trigger a landmark selection checkpoint.
1. List the monomer counts that trigger a resampling quality control checkpoint.
1. Modify the parent simulation so that landmark selection checkpoints occur after every 35th monomer is grown. Run the simulation and report effective sample size and average percent unique.
1. Modify the parent's landmark schedule to use a heterogeneous lag of 40 for the first 100 monomer nodes and 20 for the last 50 monomer nodes. List the monomer counts that trigger a landmark selection checkpoint with this new schedule. Re-run the simulation and report ESS.
1. Modify the child simulation so that resampling uses an alpha exponent of 0.5 *and* resampling checkpoints occur after every 7th monomer is grown. Re-run the simulation and report the effective sample size and average percent unique.
1. Modify the landmark trial runner to use a *homogeneous uniform* schedule. List the monomer counts that trigger a landmark selection checkpoint.  

## Tutorial C02: Simulation of grandparent with nested resampling parent with nested resampling child

In this tutorial, we will increase the fractal depth to **3** by adding a top-level *grandparent* simulation. The grandparent simulation will have no direct quality control; however, the grandparent simulation will contain a *landmark* trial runner which will utilize a *parent* sub-simulation configured with resampling-based quality control. The *parent* simulation will also contain its own landmark trial runner which will utilize a *child* sub-simulation also configured with resampling-based quality control. The hierarchical configuration is illustrated in the following figure: 

![C02 hierarchy](./02/hierarchy.svg)

To begin, list the files located in the [02](02) directory:

```bash
cd ./02
ls
```

We see the following files listed:

* [C02.json](02/C02.json)
* [C02.ini](02/C02.ini)
* [C02.parent.ini](02/C02.parent.ini)
* [C02.child.ini](02/C02.parent.ini)
* [run.sh](02/run.sh)

### JSON

Let's first examine [C02.json](02/C02.json):

```json
{
    "name": "tutC02",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_grandparent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "sub": "spec_parent"
        },
        "qc": "null"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "power",
                "alpha_slot": "1"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sub": "spec_child"
        },
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "2"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "2"
            }
        }
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "3"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "3"
            }
        }
    },
   "top": "spec_grandparent"
}
```

The JSON now specifies **3** simulations: *spec_grandparent*, *spec_parent*, and *spec_child*. Note, the *top-level* simulation is specified by the line `"top": "spec_grandparent"`. As before, the *archetype* `"arch0"` is used to avoid redundant specification of common configuration properties shared by the simulations. Actually, all fields in this JSON have been described in previous sections; however, we will review each specification to make clear how the grandparent, parent, and child simulations are wired together.

#### Grandparent simulation JSON

The top-level grandparent simulation is defined by the `"spec_grandparent"` JSON entry. Let's examine its components:

```json
{
    "spec_grandparent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "sub": "spec_parent"
        },
        "qc": "null"
    }
}
```

The `"spec_grandparent"` JSON entry defines a simulation with a *landmark* trial runner and *null* quality control. The landmark trial runner uses a *sub-simulation* defined by `"spec_parent"`. Notice that the grandparent arguments (alpha and lag) have been assigned to slot `"0"` to disambiguate from the parent and child simulations.

As noted before, the baking process is informed of the grandparent's top-level status via the entry  `"top": "spec_grandparent"`. Also, as before, any unspecified properties needed by the baking process are queried from the `"arch0"` archetype.

#### Parent simulation JSON

The *parent* simulation is used by the *grandparent's* landmark trial runner for generating candidate fragments. The relationship is similar to the parent and child simulations from **tutorial C01**.

Let's examine the parent's JSON specification defined by `"spec_parent"`:

```json
{
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "power",
                "alpha_slot": "1"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sub": "spec_child"
        },
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "2"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "2"
            }
        }
    }
}
```

One noteable difference from **tutorial C01** is that the parent simulation uses both a landmark trial runner *and* resampling for quality control - denoted by the fields `"trial_runner": { "type": "lmrk", ... }` and `"qc": { "type": "rs" }` respectively. In this case, both the landmark trial runner and the quality control resampler use power samplers and homogeneous lag schdules. To disambiguate the landmark trial runner's alpha exponent and lag from the quality control resampler's alpha exponent and lag, slot `"1"` is assigned to the landmark trial runner and slot `"2"` is assigned to the quality control resampler. Note, the slots also disambiguate arguments intended for grandparent or child simulations.

From the JSON, we see that the parent simulation grows polymer samples according to its landmark trial runner, where the landmark trial runner selects from candidate fragments generated by the child sub-simulation. In addition, after landmark fragment selection has occured, the resulting ensemble of parent polymer samples will also undergo resampling-based quality control according to the parent resampler's checkpoint schedule. Ultimately, the resampling-enriched parent polymer ensemble will be presented as candidate fragments to the grandparent simulation's landmark trial runner!

#### Child simulation JSON

The *child* simulation is used by the *parent's* landmark trial runner for generating candidate fragments. The relationship is identical to the parent and child simulations from **tutorial C01**.

Let's examine the child's JSON specification defined by `"spec_child"`:

```json
{
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "3"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "3"
            }
        }
    }
}
```

The child specification is similar to the child from **tutorial C01**, namely a canonical trial runner is used alongside a resampling-based quality control. The only difference is that the *slots* for the schedule lag and sampler alpha exponent are now `"3"` to disambiguate from similar parent and grandparent arguments.

#### JSON summary

The JSON species a fractal simulation of depth **3** and consists of a top-level *grandparent* simulation specified by `"spec_grandparent"`. The *grandparent* simulation uses a landmark trial runner which uses the *parent* sub-simulation specified by `"spec_parent"` for candidate polymer fragment generation. The *parent* simulation (defined by `"spec_parent"`) *also* uses a landmark trial runner which uses a *child* sub-simulation specified by `"spec_child"`. The *parent* simulation also uses resampling-based quality control to enrich the population of candidate fragments presented to the *grandparent's* landmark trial runner. The *child* simulation (defined by `"spec_child"`) uses a *canonical* trial runner - recall, the simulation at the highest fractal depth must be a *canonical* trial runner. The *child* simulation also uses resampling-based quality control to enrich the candidate fragments presented to the *parent's* landmark trial runner.

The slot system is used to disambiguate command-line arguments for the various simulation components. Specifically, slot `"0"` is reserved for the grandparent's arguments, slots `"1"` and `"2"` are reserved for the parent's arguments, and slot `"3"` is reserved for the child's arguments.

### INI

We now see *three* INI run-time configuration files listed:

* [C02.ini](02/C02.ini)
* [C02.parent.ini](02/C02.parent.ini)
* [C02.child.ini](02/C02.child.ini)

As in **tutorial C01**, there are *three* INI configurations to match the *three* simulations specified in the JSON: `"spec_grandparent"`, `"spec_parent"`, and `"spec_child"`. The configuration [C02.ini](02/C02.ini) specifies the run-time arguments for the top-level *grandparent* simulation. The configuration [C02.parent.ini](02/C02.parent.ini) specifies the run-time arguments for the *parent* sub-simulation (used by the *grandparent's* landmark trial runner). The configuration [C02.child.ini](02/C02.child.ini) species the run-time arguments for the *child* sub-simulation (used by the *parent's* landmark trial runner).

#### Grandparent simulation INI

Let's examine the [C02.ini](02/C02.ini) configuration for the top-level grandparent simulation:

```ini
output_dir = ../../../output/tutC/02
ensemble_size = 50
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 150
max_node_diameter = 110
num_unit_sphere_sample_points = 50
report_perc_uniq = 1
### Landmark:
qc_schedule_lag_slot0 = 70
qc_sampler_power_alpha_slot0 = 1.0
conf_child = C02.parent.ini
```

The *grandparent's* configuration [C02.ini](02/C02.ini) is nearly identical to **tutorial C01's** top-level [C01.ini](01/C01.ini). The *only* difference is that the field `conf_child`, used by the *grandparent* landmark trial runner's sub-simulation, is now set to the *parent's* configuration `C02.parent.ini`.

#### Parent simulation INI

Let's examine the [C02.parent.ini](02/C02.parent.ini) configuration for the parent simulation:

```ini
ensemble_size = 50
max_trials = 100
### Landmark:
qc_schedule_lag_slot1 = 35
qc_sampler_power_alpha_slot1 = 1.0
conf_child = C02.child.ini
### Resampling:
qc_schedule_lag_slot2 = 35
qc_sampler_power_alpha_slot2 = 1.0
```

Recall, the *parent* simulation generates candidate polymer fragments for the *grandparent's* landmark trial runner. In this case, the number of candidate fragments generated is set by the field `ensemble_size = 50`. Also, as in **tutorial C01**, the line `max_trials = 100` makes sure that if no candidate fragments can be generated in `100` trials, then the grandparent polymer sample will need to be regrown from scratch in the next trial.

The parent configuration must configure both a landmark trial runner and a resampling-based quality control policy.

Let's examine the parent's landmark trial runner configuration:

* `qc_schedule_lag_slot1 = 35`: This configures the parent's landmark trial runner to have checkpoints after every `35` monomer nodes have been grown.
* `qc_sampler_power_alpha_slot1 = 1.0`: This configures the parent's landmark trial runner to use a weight smoothing exponent of `1.0` when selecting a candidate fragment.
* `conf_child = C02.child.ini`: This lets the *parent's* landmark trial runner know which INI file to use for configuring its *child* sub-simulation.

Next, let's examine the parent's resampling-based quality control configuration:

* `qc_schedule_lag_slot2 = 35`: This configures the parent's quality control resampler to have checkpoints after every 35 monomer nodes have been grown.
* `qc_sampler_power_alpha_slot2 = 1.0`: This configures the parent's quality control resampler to use a weight smoothing exponent of `1.0`.

So, to recap, the parent uses a landmark trial runner to grow candidate fragments. The landmark checkpoints occur after every `35` monomer nodes have been grown, at which point a single fragment is selected and appended to the parent sample. In addition, after the landmark selection checkpoint has completed for all parent polymer samples, a resampling quality control checkpoint will also be triggered. In this case, the landmark checkpoints and resampling checkpoints occur at the same monomer growth intervals (`35`). **There is no restriction that this must be the case.** However, it should be noted that fragments are grown at the resolution of the landmark trial runner (`35`) and, therefore, a resampling checkpoint, or any quality control checkpoint, will only be triggered if it coincides with a landmark checkpoint. For homogeneous lag schedules, this occurs when the monomer count is a multiple of both the resampling lag and the landmark lag. For example, if the resampling lag is `10`, with a landmark lag of `35`, then the first resampling checkpoint will not be triggered until a monomer growth count of 70 is reached.

#### Child simulation INI

Now, let's examine the child configuration [C02.child.ini](02/C02.child.ini):

```ini
ensemble_size = 25
max_trials = 100
### Resampling:
qc_schedule_lag_slot3 = 35
qc_sampler_power_alpha_slot3 = 1.0
```

The child configuration is similar to the child configuration from **tutorial C02**. The only differences are that the ensemble size has been reduced to `25` to help the simulation complete faster. Also the resampler arguments are in slot `3`.

#### INI summary

In summary, there are *three* INI configuration files corresponding to the grandparent, parent and child simulations respectively. The grandparent simulation is configured by [C02.ini](02/C02.ini), the parent sub-simulation is configured via [C02.parent.ini](02/C02.parent.ini), and the child sub-simulation is configured via [C02.child.ini](02/C02.child.ini).

As in **tutorial C01**, any queried properties missing from a sub-simulation INI configuration are assumed to be specified in a parent-level INI. For instance, for any property missing in the child's [C02.child.ini](02/C02.child.ini), first the direct parent's [C02.parent.ini](02/C02.parent.ini) will be checked for the value. If found, then the parent value will be used, else the grandparent's [C02.ini](02/C02.ini) will be assumed to contain the needed value and the grandparent value will be used.

Note, the top-level INI configuration **must** be specified by the command line argument `--conf <path to INI file>`. Since the fractal depth is greater than 2, the path to the sub-simulation configurations must be specified via the `conf_child` fields within the grandparent's [C02.ini](02/C02.ini) and the parent's [C02.parent.ini](02/C02.parent.ini). 

### Putting it all together

The JSON and INI files specify a fractal simulation of **depth 3**. Specifically, there is a top-level *grandparent* simulation which utilizes a *parent* simulation for generating candidate fragments at each grandparent polymer sample. The *parent* simulation in turn utilizes a *child* simulation for generating candidate fragments at each parent polymer sample.

Within the JSON, as in **tutorial C01**, the **archetype** field is used to avoid specifying redundant properties between the grandparent, parent, and child simulations.

Ultimately, the increased fractal depth should lead to more robust weight enrichment in the final ensemble of polymer conformations, while still maintaining conformational diversity. In tutorial **C03**, we will investigate another simulation also with fractal depth 3 but which combines both *resampling* and *rejection control*.

Now let's run the simulation. In a terminal, enter:

```bash
./02/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?
1. List the monomer counts that trigger a landmark selection checkpoint.
1. List the monomer counts that trigger a resampling quality control checkpoint in the parent simulation. List the monomer counts that trigger a resampling quality control checkpoint in the child simulation.
1. Modify the parent simulation so that landmark selection checkpoints occur after every 35th monomer is grown and resampling checkpoints occur after every 70th monomer is grown. Run the simulation and report effective sample size and average percent unique.
1. Modify the child simulation so that resampling uses an alpha exponent of 0.5 *and* resampling checkpoints occur after every 7th monomer is grown. Re-run the simulation and report the effective sample size and average percent unique.
1. Create a new fractal simulation of **fractal depth 4** consisting of great-grandparent, grandparent, parent, and child simulations. Set the great-grandparent quality control to null and all others to be resampling. To allow timely completion, set the child ensemble size to 5, the parent ensemble size to 10, the grandparent ensemble size to 15, and the great-grandparent ensemble size to 20. What landmark and resampling checkpoint schedules did you use? Run the simulation and report the *ESS*.

## Tutorial C03: Simulation of grandparent with nested resampling parent with nested rejection control child

We will keep the same general framework as **tutorial C02**. Namely, the fractal simulation will be of depth **3** and consist of nested grandparent, parent, and child simulations. The only relevant modification will be to the child simulation. Specifically, the child simulation will now utilize rejection control instead of resampling-based quality control.

To begin, list the files located in the [03](03) directory:

```bash
cd ./03
ls
```

We see the following files listed:

* [C03.json](03/C03.json)
* [C03.ini](03/C03.ini)
* [C03.parent.ini](03/C03.parent.ini)
* [C03.child.ini](03/C03.parent.ini)
* [run.sh](03/run.sh)

### JSON

Lets inspect [C03.json](03/C03.json):

```json
{
    "name": "tutC03",
    "arch0": {
        "diameter" : "homg",
        "growth" : "sloc",
        "intr_chr" : "null",
        "energy" : "unif"
    },
    "spec_grandparent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": "unif",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "sub": "spec_parent"
        },
        "qc": "null"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "select": {
                "type": "power",
                "alpha_slot": "1"
            },
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "1"
            },
            "sub": "spec_child"
        },
        "qc": {
            "type": "rs",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "2"
            },
            "sampler": {
                "type": "power",
                "alpha_slot": "2"
            }
        }
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": {
            "type": "rc",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "3"
            },
            "scale": "interp_weights"
        }
    },
   "top": "spec_grandparent"
}
```

As in **tutorial C02**, the grandparent, parent, and child simulations are specified by `"spec_grandparent"`, `"spec_parent"`, and `"spec_child"` respectively. There are two notable differences:

1. In `spec_grandparent`, the landmark trial runner sets the property `"select": "unif"` which configures the trial runner to use a uniform selection policy. This means all samples generated by the trial runner's sub-simulation will have equal probability of being selected regardless of weight. This could be equivalently set using `"select": "uniform"`.
1. In `"spec_child"`, the quality control policy is now configured for *rejection control* via `"qc": { "type": "rc" ... }`. For a review on rejection control JSON configuration, please refer to [tutorial B](../B/tutorial.md).

### INI

As in **tutorial C02**, we see *three* INI run-time configuration files listed:

* [C03.ini](03/C03.ini)
* [C03.parent.ini](03/C03.parent.ini)
* [C03.child.ini](03/C03.child.ini)

Similarly, the top-level grandparent simulation is specified by [C03.ini](03/C03.ini), the parent simulation is specified [C03.parent.ini](03/C03.parent.ini), and the child simulation is specified by [C03.child.ini](03/C03.child.ini).

The only relevant changes from **tutorial C02** are within the child's [C03.child.ini](03/C03.child.ini):

```ini
ensemble_size = 25
max_trials = 100
### Rejection control:
qc_schedule_lag_slot3 = 35
rc_scale_interp_weights_c_min = 0.0
rc_scale_interp_weights_c_mean = 0.25
rc_scale_interp_weights_c_max = 0.75
```

We see that the child is configured for rejection control instead of resampling. Basically, after every 35 monomer nodes, the child will perform a rejection control checkpoint instead of a resampling checkpoint as in **tutorial C02**. For a review on rejection control INI configuration, please refer to [tutorial B](../B/tutorial.md).

Also, if we were to inspect [C03.ini](03/C03.ini), we would no longer see a setting for `qc_sampler_power_alpha_slot0` as this is not used by the *uniform* selection policy. Note, the uniform selection policy is equivalent to `qc_sampler_power_alpha_slot0 = 0.0` for a *power* selection; however, it is recommonded to modify the JSON to use uniform selection as it results in a slightly more efficient compiled simulation.

Now let's run the simulation. In a terminal, enter:

```bash
./03/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Re-run the simulation 5 times and record the reported *ESS*, *percent unique* at the 0-based monomer indices {0, 51, 92, 111, 140, and 149}, and *average percent unique* for each run. What are the averaged *ESS* and averaged *average percent unique* over all runs?

## Command line options

Without modification, command line arguments are *only* available to the *top-level* simulation and are not passed to any nested *trial runner* sub-simulation. For example, the command line argument `--ensemble_size 10` will only be seen by the *parent* simulation of tutorial **C01** and the *grandparent* simulation of tutorial **C02**; it will set the output ensemble size to `10` in the respective top-level simulations and will not be passed to any nested sub-simulations.

This section briefly describes how to set nested *trial runner* sub-simulation parameters via the command line. To specify command line arguments for a sub-simulation, we use a *dot prefix syntax*. Specifically, the `.` character is used to target command line arguments to a specific fractal *depth*, where the number of `.` characters in the prefix encodes the target depth. All arguments with a `.` prefix are only used by the simulation at the target fractal depth and are ignored by all others.

For example, in tutorial C01 which consists of a parent simulation with a nested child sub-simulation, to override the ensemble size to be `10` in the child via command line, simply use the following: `--.ensemble_size 10`. Notice the single `.` which tells the program that the argument is intended for the child and should be ignored by the parent. Similarly, to set the maximum number of trials in the child to `3` via command line: `--.max_trials 3`.

In tutorial C02, which consists of a grandparent simulation with a nested parent sub-simulation with a nested child sub-simulation, we must use a *single* `.` to target command line arguments to the *parent* and *two* `..` to target command line arguments to the *child*. For example, `--.ensemble_size 10` will set the *parent* ensemble size to `10` and `--..ensemble_size 10` will set the *child* ensemble size to `10`. Similarly, the command line `--.max_trials 3` will set the maximum number of trials in the *parent* to `3` whereas `--..max_trails 3` will set the maximum number of trials in the *child* to `3`.

The general pattern for any argument with an associated value is `--<.*><name> <value>` where:

* `--` means the argument has a value
* `<.*>` is 0 or more `.` characters and encodes the target fractal depth
* `<name>` is the name of the command line argument (e.g. `ensemble_size`)
* `<value>` is the value associated to the argument

Note, the *dot prefix syntax* only applies to *command line* arguments. Arguments specified via INI should *never* be prefixed with `.` characters!

## Conclusion

Congratulations on finishing the fractal simulation tutorial!

To recap, fractal Monte Carlo sampling allows for population weight enrichment while also maintaining sample diversity. In this tutorial, to reduce the computational burden, we use a relatively small polymer length of only 150 nodes. At this small polymer length, it may not be apparent the necessitity of fractal sampling compared to plain techniques from tutorials **A** and **B**. However, in our experience, the statistical quality of the population may rapidly degrade as the polymer length increases. In particular, resampling techniques will not be able to maintain conformational diversity and rejection control will very quickly become intractable as the polymer length is increased. Fractal Monte Carlo helps alleviate these scalability issues and allows larger polymers to be simulated while also maintaining statistical quality.

In the next [tutorial D](../D/tutorial.md), we will explore how to extend the power sampling framework using Delphic resampling.
