# Tutorial A: Chromatin folding basics

## Overview

![overview](A.png)

This tutorial covers the basics of modeling with our chromatin folder.

A chromatin locus is represented as a connected polymer chain of spheres (or monomer "nodes") and is simulated as a random self-avoiding walk. Namely, the chromatin locus is folded (or "grown") by sequentially placing nodes until the target chain length is reached.

The tutorials available in this section are as follows:

* **Getting started**: Introduces the model baking process
* **A01**: Modeling a single chromatin locus with all nodes the same diameter
* **A02**: Modeling a single chromatin locus where nodes can have varying diameter
* **A03**: Modeling multiple chromatin loci with all nodes the same diameter
* **A04**: Modeling multiple chromatin loci where nodes can have varying diameter

This tutorial is designed for running on a Linux operating system. All paths given are relative to the directory containing this [tutorial.MD](./) unless noted otherwise. Any path starting with a `/` (e.g. `/scripts`) is relative to the [root project directory](../../).

## Getting started

To begin, we need to go over the concept of *baking* a model. Baking transforms all model details from a "high-level" JSON specification into "low-level" C++ code which can then be compiled. This is accomplished by specifying a model using [JSON](https://en.wikipedia.org/wiki/JSON), registering the JSON specification within a *bun* file, and finally running [bake.py](../../scripts/Template/bake.py) on the bun file.

### Bun files

If we inspect the .bun file for this tutorial: [A.bun](A.bun)

```bash
cat ./A.bun
```

We see output along the following:

```
# FILENAME, USAGE, CMDKEY
./01/A01.json, null, tutA01
./02/A02.json, null, tutA02
./03/A03.json, null, tutA03
./04/A04.json, null, tutA04
```

The bun file consists of rows of comma-separated values (CSV). Each row contains three entries:

1. The relative path to the model JSON file. The path is considered relative to the bun file itself.
2. The target usage for the model. In this case, all usages are for *null* modeling.
3. The command line key which can be used to run the model for the target use case. For example, to run the first model listed, we would enter the following `<path_to_compiled_binary> --null_dispatch tutA01` 

From inspection of [A.bun](A.bun), we see that four model JSONs are registered, they are all to be used for *null* modeling, and  their command line keys are *tutA01*, *tutA02*, *tutA03*, and *tutA04* respectively.

### Baking

Next, to bake all these models and generate compilable C++ code, we need to call [bake.py](../../scripts/Template/bake.py). This python script has two non-optional arguments (short-hand|long-hand):

* `-b|--bun <path>`: The path to the bun file
* `-o|--out <path>`: The path to directory in which to create the compiled output build

The [bake.py](../../scripts/Template/bake.py) script also has two *optional* arguments:

* `-c|--compile <target>`: If target is *none*, the generated C++ code will not be compiled. However, if a build target is specified, then the code will be compiled according to the script associated with the target. For a list of registered targets, see [targets.txt](../../scripts/Template/build/targets.txt). A whitespace separated list of targets (e.g. `-c <target1> <target2> ...`) can be used to compile multiple build targets.
* `f|--force`: If an existing build already exists, this switch will erase the previous build and allow the baking process to proceed, else the baking process will halt.

We've provided a [bash](https://en.wikipedia.org/wiki/Bash%5F%28Unix_shell%29) shell script, [bake.sh](bake.sh), to bake all models used in this tutorial. Please run this script by entering the following in the command terminal:

```bash
./bake.sh
```

Notice that [bake.sh](bake.sh) uses a *linux-gnu-no-avx* compile target which should work with most Linux systems that have a [C++11](https://en.wikipedia.org/wiki/C%2B%2B11) compatible [g++](https://gcc.gnu.org/) compiler. For production simulations however, we recommend a machine with AVX2 support and using a *linux-gnu* or intel compiler build target. Again, see [targets.txt](../../scripts/Template/build/targets.txt) for available compilation targets and also for how to associate your own compile scripts.

The [bake.sh](bake.sh) script will also run [copy_bin.sh](../../scripts/Template/build/copy_bin.sh) which moves compiled binary executables into a local *bin* folder within the output build directory. This is to allow modification of the build source without affecting any outstanding simulations.

Please note the final line of [bake.sh](bake.sh) which echos the resulting *output directory*.

```
Finished tutorial A copy binaries
Please inspect output directory: <OUTPUT_DIRECTORY>
```

Please navigate to the *output directory* to get a feel for the various files created:

```bash
cd <OUTPUT_DIRECTORY>
ls
cd ./build
ls
```

Within the *output directory*, the baking process created a *build* folder. Within the *build* folder, there are many source code related folders (e.g *source*, *vstudio*, etc) and there is also a *bin* folder. It is in this `<OUTPUT_DIRECTORY>/build/bin` folder that the compiled executable binaries for the chromatin folder reside!

## Tutorial A01: Single locus, homogeneous nodes

![Single locus, homogeneous nodes](01/A01.png)

Now we are ready to start **tutorial A01**. If we navigate to the [01](01) folder:

```bash
cd ./01
ls
```

We see several files:

* [A01.json](01/A01.json)
* [A01.ini](01/A01.ini)
* [run.sh](01/run.sh)

Over the next few sections, we will examine each of these files in detail.

### The JSON compile-time model specification

The .json file is used for specifying the chromatin folding model components. The baking process parses the JSON specification to generate the necessary C++ code. **The baking process must be redone whenever a .json or .bun file is changed!** For advanced users, it should be noted that bake.py moves all scripts in [/scripts/Template](../../scripts/Template) to /scripts/Local. So if any scripts within /scripts/Template are modified, they must be recopied to the Local folder (see [scripts/Template/create_local_workspace.py](../../scripts/Template/create_local_workspace.py)).

Let's inspect [A01.json](01/A01.json):

```json
{
    "name":"tutA01",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "qc": "null",
        "diameter" : "homg",
        "growth" : "sloc",
        "interaction_chromatin" : "null",
        "energy" : "unif"
    }
}
```

This is a JSON specification for the simplest chromatin folding model possible: a single genomic locus with homogeneous nodes (i.e. all nodes have the same diameter). Let's now look at each JSON entry:

* `"name":"tutA01"`: This assigns the *name* of the model to be *tutA01*. This name must be unique from other models and must not contain any whitespace or any character that would generate invalid C++: **only letters, numbers, and underscore characters are allowed!**
* `"top": "spec"`: This line says the *top-level* model specification is given by the JSON object named *spec*. This is letting the baking process know which JSON object actually contains the model specification from which to generate C++ code. In [tutorial C](../C), we will see another reason why it is important to explicitly denote the top-level model. Note that the value associated to *top* (e.g. in this case: *spec*) is user defined but is also under the same constraints as the value for the *name* field, specifically it **must contain only letters, numbers, and underscore characters (no whitespace allowed)!**
* `"spec": { ... }`: The *spec* JSON object contains an actual model specification. The name of this object can be arbitrarily set by the user (e.g. `"my_very_long_spec": { ... }` is a perfectly valid name), but it must be assignable to the `"top"` field and, therefore as stated previously, it must contain only letters, numbers, and underscore characters without whitespace.
* `"trial_runner": "canonical"`: This says the model simulation should use a *canonical* trial runner. Trial runners are an advanced concept covered in [tutorial C](../C). Let's not worry about this concept for now, just know that *canonical* is the most basic trial runner without any bells and whistles. Note, the baking process also understands the equivalent shorthand: `"tr": "canon"`. If this entry is ommitted, the default trial runner will implicitly be `"canon"`.
* `"qc": "null"`: This field specifies the *quality control* policy. In this case it's *null* which means we are not using any form of quality control! Quality control policies are the subject of [tutorial B](../B). If this entry is ommitted, the default quality control will implicitly be `"null"`.
* `"diameter": "homg"`: This field specifies the node diameter policy. The value *homg* says all nodes are homogeneous and therefore are of the same diameter. We will relax this assumption in **tutorial A02**. Note, the following equivalent shorthand: `"diam": "homg"`.
* `"growth": "sloc"`: This field specifies a **s**ingle-**loc**us growth policy. We will relax this assumption in **tutorial A03**. The following shorthand is also acceptable: `"grow": "sloc"`.
* `"interaction_chromatin": "null"`: This field specifies a *null* chromatin-to-chromatin interaction policy. Chromatin-to-chromatin interactions are used for modeling knock-in or knock-out genomic spatial proximity constraints. The value *null* means we are not enforcing any interaction constraints. Interaction constraints are typically not used for modeling *null* distributions, instead they will be the subject of a future tutorial. The key `"interaction_chromatin"` may also be specified with any of the following shorthands: `"interaction_chrom"`, `"interaction_chr"`, `"intr_chromatin"`, `"intr_chrom"`, and `"intr_chr"`. If this entry is ommitted, the default chromatin-to-chromatin interaction policy will implicitly be `"null"`.
* `"energy": "unif"`: This field specifies the *energy* policy used for selecting candidate nodes during the sequential chain growth random walk process. The value *unif* means that all candidate nodes are considered to have the same energy and therefore they are selected according to a [discrete uniform distribution](https://en.wikipedia.org/wiki/Discrete_uniform_distribution) - in other words, all candidate nodes have equal probability of being selected. An alternative is to assign the value *bend* which calculates an energy term at each candidate based on [persistence length](https://en.wikipedia.org/wiki/Persistence_length) criteria and then selects among the candidates using a [Boltzmann distribution](https://en.wikipedia.org/wiki/Boltzmann_distribution); this will be the subject of a future tutorial. The following shorthand is also acceptable: `"ener": "unif"`. If this entry is ommitted, the default energy policy will implicitly be `"unif"`.

To summarize, the JSON file specifies the components that define the model. Together the .json and .bun files define the *compile-time* properties of the model. Therefore, any changes to these files requires rebaking. However, as long as these files are not modified, the baking step can be avoided after the initial call to bake.py.

### The INI run-time configuration

Whereas JSON is used to specifiy the *compile-time* components of the model, the actual *run-time* values of the component parameters are specified in an [INI](https://en.wikipedia.org/wiki/INI_file) (.ini) configuration file, as well as through the command line. In fact, *any* .ini field can also be specified on the command line using the convention:

```
--<field_name> <field_value>
```

Lets examine the INI file [A01.ini](01/A01.ini):

```bash
cat ./01/A01.ini
```

We see several fields (note, comments are not displayed):

```ini
output_dir = ../../../output/tutA/01
ensemble_size = 10
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 2000
max_node_diameter = 110
num_unit_sphere_sample_points = 50
```

Let's examine each INI field in detail:

* `output_dir = ../../../output/tutA/01`: This field specifies the output directory. When running the model simulation, all output files such as sample coordinates will be written to this directory or a subfolder within this directory. **Note, all relative paths specified within an INI configuration file are assumed relative to the directory containing the INI file.** To equivalently specify on the command line, use the arguments (note that the relative path is now to the executable's working directory):
    * `--output_dir ../../../output/tutorial/A/01`
* `ensemble_size = 10`: The field specifies the *ensemble* size. An *ensemble* is the collection of folded chromatin samples. This field says whenever we run the folding simulation, we should generate a random ensemble consisting of 10 folded chromatin samples. To equivalently specify on the command line, use the arguments:
    * `--ensemble_size 10`
* `max_trials = -1`: This specifies the maximum number of *trials* the model folding simulation may run. A single trial consists of an attempt to generate the target ensemble size. During a trial, it is possible that a sample may fail to fold and therefore must be regrown in a subsequent trial. After a trial finishes, all completed samples are saved and the next trial proceeds to fold the remaining failed samples. This procedure repeats until either the target ensemble size is reached (e.g. 10 samples have successfully folded) or the maximum trial count is exceeded. In this case, `-1` or any negative number indicates no effective limit on the number of trials. To equivalently specify on the command line, use the arguments:
    * `--max_trails -1`
* `nuclear_diameter = 5000`: This field specifies the diameter of the confining spherical nuclear compartment in Angstroms. This value should be set such that the empirical chromatin density (bp/volume) is conserved. To equivalently specify on the command line, use the arguments:
    * `--nuclear_diameter 5000`
* `num_nodes_0 = 2000`: This specifies the number of spherical nodes defining the first genomic locus chain, *note the 0-based indexing*. Since we are a single-locus model, only a single chain may be specified by this field, this assumption is relaxed in **tutorial A03**. To equivalently specify on the command line, use the arguments:
    * `--num_nodes_0 2000`
* `max_node_diameter = 110`: This specifies the diameter of each spherical node constituting the chromatin chain in Angstroms. This also specifies the self-avoidance constraint as nodes are not allowed to overlap. This value should be chosen according to an empirical fiber density (e.g. 11 nm fiber). Since we are a homogeneous model, all monomer nodes will have the same diameter (this notion is relaxed in **tutorial A02**). To equivalently specify on the command line, use the arguments:
    * `--max_node_diameter 110`
* `num_unit_sphere_sample_points = 50`: This specifies the number of candidate positions to consider when sequentially growing the chromatin chain. This number specifies how many candidate positions will be sampled on the spherical surface of the last placed node. Any candidate positions which fail constraints such as nuclear confinement, self-avoidance, or interaction proximity will be culled. If all candidate positions for a sample chain are invalid, the chain must be regrown in the next trial. This parameter must be chosen with care as a large value will drastically increase the computational complexity and make it harder to adequately sample the resulting conformational space (and therefore decrease the statistical weight of each chromatin sample). However, values which are too small may be non-physical and/or lead to increased frequency of intermediate samples which fail to satisfy constraints (and therefore need to be regrown). To equivalently specify on the command line, use the arguments:
    * `--num_unit_sphere_sample_points 50`

**In the case where a field is specified both in the INI and the command line, the command line value always take precedence.** Again, the INI specifies the model's run-time configuration parameters; therefore, a rebake is **not necessary** when modifying INI fields.

### Running a model folding simulation

We are now ready to run our model! The basic command line syntax to run a null folding simulation is:

```
<path_to_binary_exe> --conf <path_to_INI> --null_dispatch <cmd_key> [... additional args]
```

* `<path_to_binary_exe>`: The path to the binary executable as discussed in the **baking** section.
* `--conf <path_to_INI>`: Specifies the location of the INI configuration file. For example, `--conf ./01/A01.ini` would run a folding simulation using the [A01.ini](01/A01.ini) configuration file.
* `--null_dispatch <cmd_key>`: runs the *null* folding simulation mapped to the *command key* specified in the .bun file. For instance, `--null_dispatch tutA01` runs the null folding simulation for **tutorial A01**.

In addition, there are several important export option switches:

* `-export_csv`: Export the 3-D spatial coordinates (node centers) for each folded chromatin sample as separate comma-separated (CSV) files. Samples are exported to folder `<output_dir>/csv`.
* `-export_pdb`: Export the 3-D spatial coordinates (node centers) for each folded chromatin sample as separate [PDB](http://www.wwpdb.org/documentation/file-format) files. Coordinates are scaled to mitigate overflowing PDB column width boundaries. Further, this format cannot support heterogeneous nodes. Samples are exported to folder `<output_dir>/pdb`.
* `-export_pml`: Export the 3-D spatial coordinates (node centers) for each folded chromatin sample as  separate [PyMOL](https://pymol.org/) PML script files. For a discussion of PML files, see [here](http://tubiana.me/how-to-execute-a-py-script-on-pymol/) and [here](https://www.biostars.org/p/44769/). Like PDB format, coordinates are scaled to avoid overflowing column boundaries; however, this format *does* support heterogeneous nodes. Samples are exported to folder `<output_dir>/pml`.
* `-export_csv_gz`, `-export_pdb_gz`, `-export_pml_gz`: Export the 3-D spatial coordinates in the corresponding *gzip-compressed* format.
* `-export_extended`: Export additional extended information such as nuclear diameter and individual node diameters, currently only applies if `-export_csv` switch is present, but may be incorporated into other export formats as well. This extended information is more important when modeling multiple loci or heterogeneous monomer nodes. When modeling a single locus with homogeneous nodes as in **tutorial A01**, this field is unimportant and may be left unspecified to save on hard disk space.
* `-export_log_weights`: Export the log of the importance weight for each generated sample. The importance weight defines the statistical weight (i.e. coefficient) assigned to each sample within the ensemble. The log weights are exported to folder `<output_dir>/log_weights`.
* `-export_ligc_csv`, `-export_ligc_csv_gz`: Exports all ligation contact (ligc) tuples at each sample in *i, j* CSV format, where *i* and *j* are 0-based monomer indices. The ligation threshold can be specified with the argument `--ligc_dist <scalar>` and defaults to approximately 800 Angstroms (80 nm). Two monomers *i* and *j* are considered ligated if the Euclidean distance between any two points on their respective surfaces is less than or equal to `--ligc_dist`. This can be used for capturing ligation events in Hi-C datasets (see also [Tutorial E: Chromatin-to-chromatin proximity interaction modeling](../E/tutorial.md)).

Note, *short-hand* export options of form `-<option>`, e.g. `-export_csv`, can only be specified via command-line. However, all export options have corresponding *long-hand* forms which can be specified via INI and/or command-line. To specify a *long-hand* export option:

* **command-line**: `--<option> <0|1>`, e.g. `--export_csv 1` will configure polymer geometry to be exported as CSV format
* **INI**: `<option> = <0|1>`, e.g. `export_csv = 1` is equivalent to the command-line `--export_csv 1`

*Short-hand* export options have only a single `-` prefix and can only be used on the command-line. In contrast, *long-hand* export options have a double `--` prefix on the command line and are always followed by a `0` or `1`; the long-hand export options may also be configured via INI unlike the short-hand.

In general, CSV format data as generated by `-export_csv` is easier to analyze and also **does not scale any values**, therefore this format should be used for all serious folding investigations (along with `-export_log_weights` and possibly `-export_extended`). In contrast, PDB format data is useful for visualization as many tools such as [PyMOL](https://pymol.org), [Chimera](https://www.cgl.ucsf.edu/chimera/), and [VMD](http://www.ks.uiuc.edu/Research/vmd/) can easily render PDB spatial coordinates. However, the PDB specification is limited in its ability to specify arbitrary node diameters, therefore PML format is preferred for heterogeneous node simulations (with the caveat that PML format is specific to the PyMOL viewer).

For viewing PML and PDB formats, we recommend using open-source versions of PyMOL, please refer to:

* [PyMOL Linux installation](https://pymolwiki.org/index.php/Linux_Install)
* [PyMOL Windows installation](https://pymolwiki.org/index.php/Windows_Install)
* [PyMOL Mac installation](https://pymolwiki.org/index.php/MAC_Install)

Another noteworthy command line option is `--job_id <prefix>`. All exported data files (coordinates and log weights) will be named using the `<prefix>` argument. For example, the command line `--job_id tut01` will cause all exported data to have a *tut01* prefix. For example, the first CSV sample coordinates file will be named *tut01.0.csv*, the second will be named *tut01.1.csv*, the third *tut01.2.csv*, etc. (note the zero-based indexing). The *job_id* prefix is used primarily to **prevent overwriting previously exported data!** and hence is typically assigned by a shell script so as to avoid naming clashes with any previous folding simulation runs. For example, the following bash code will generate a unique prefix every time the bash shell script is run:

```bash
job_prefix="$(date +%Y%m%d%H%M%S).$RANDOM"
```

We have provided a [shell script](01/run.sh) which runs the single locus, homogeneous node simulation. In a terminal, enter:

```bash
./01/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. What is the reported effective sample size when running script `./01/run.sh`? Note, [effective sample size](https://en.wikipedia.org/wiki/Effective_sample_size) will be broached in [tutorial B](../B), but it is a heuristic (i.e. *rule-of-thumb*) measure of the sampling efficiency under the target distribution.
2. How many trials did the folding simulation need to generate the target ensemble size?
3. Navigate to `<output_dir>` and compare the exported files in a text editor: `tutA01.0.csv`, `tutA01.0.pdb`, and `tutA01.0.pml`. What type of information is present in each of these file formats? What are the differences between the formats - for example, is there any information reported in one format that is not reported in the other formats?
4. Modify `./01/run.sh` to remove the `-export_extended` switch, then re-run the script. Repeat previous exercise and note any changes to the reported information.
5. Install a PDB viewer such as PyMOL, Chimera, or VMD to visualize `tutA01.0.pdb`. Are the nodes self-avoiding? Are there gaps between any of the nodes or are they continuous?
6. Modify `./01/run.sh` to avoid overwriting previously exported data each time the script is executed. Re-run the script to verify old data is being preserved and new data is being generated.
7. Modify `./01/A01.ini` to double the node diameter, re-run the simulation. Repeat this exercise to double the node diameter but instead use the command line (i.e. modify run.sh).
8. Set the run-time parameter `num_unit_sphere_sample_points` to 20. Re-run the folding simulation. How many trials are needed to complete the simulation? What is the effective sample size? Don't forget to reset `num_unit_sphere_sample_points` back to it's original value before attempting any further exercises.
9. Modify the run-time parameters to generate an ensemble of size 50, re-run the simulation and inspect the output.
10. Modify the run-time parameters to generate an ensemble of size 200 with a maximum trial count capped to 1. Re-run the simulation, does it generate the target ensemble size?
11. Re-run the folding simulation using an ensemble size of 300. Find the exported log weights file `log_weights.csv` and generate a box plot of the log weights. Next, subtract the max log weight and raise `e` by the resulting value; here is the equivalent R pseudocode: `w = exp(log_w - max(log_w))` where `log_w` is the vector of log weights. Generate a box plot for the transformed weight vector `w`, compare with box plot of `log_w`.
12. Modify the run-time parameters to model a 2 megabase genomic locus at a nuclear chromatin density of 4.4 megabase per cubic [micron](https://en.wikipedia.org/wiki/Micrometre). Assume a chromatin fiber density of 1 base pair per 1.43 cubic Angstroms. Assume each monomer node represents 500 base pairs. Re-run the simulation and inspect the output.

## Tutorial A02: Single locus, heterogeneous nodes

![Single locus, heterogeneous nodes](02/A02.png)

In this tutorial, we relax the constraint that all nodes must be the same diameter.

To begin, inspect the files located in the [02](02) directory:

```bash
cd ./02
ls
```

We see similar files as in **tutorial A01**:

* [A02.json](02/A02.json)
* [A02.ini](02/A02.ini)
* [diam.txt](02/diam.txt)
* [run.sh](02/run.sh)

Let's first examine [A02.json](02/A02.json):

```json
{
    "name": "tutA02",
    "top": "spec",
    "spec": {
        "trial_runner": "canon",
        "qc": "null",
        "diameter" : "hetr",
        "growth" : "sloc",
        "energy" : "unif"
    }
}
```

Here are the relevant differences from **tutorial A01's** JSON:

* `"name": "tutA02"`: To avoid C++ naming clashes, all model names must be unique. For comparison, the name field in **tutorial A01** was *tutA01*.
* `"diameter" : "hetr"`: To specify a heterogeneous node diameter model, simply set the value of this field to *hetr*. For comparison, in **tutorial A01** this field was set to *homg*. The following shorthand is also acceptable: `"diam": "hetr"`.
* We have ommitted specifying `"interaction_chromatin" : "null"` since it is the *default* chromatin-to-chromatin interaction policy.

Next, let's examine [A02.ini](02/A02.ini):

```ini
output_dir = ../../../output/tutA/02
ensemble_size = 10
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 2000
node_diameter_fpath = diam.txt
num_unit_sphere_sample_points = 50
```

Here are the relevant differences from **tutorial A01's** INI:

* `output_dir = ../../../output/tutA/02`: To avoid overwriting data generated by **tutorial A01**, we specify a separate output folder.
* `node_diameter_fpath = diam.txt`: This field replaces **tutorial A01's** `max_node_diameter` entry. Since nodes are allowed to vary in size, we instead defer to an external [run-length encoded](https://en.wikipedia.org/wiki/Run-length_encoding) (RLE) format file to specify the diameter at each node. Recall that all relative paths within an INI configuration file are relative to the directory containing the INI file. This argument may also be set by command line (with paths now relative to the current working directory): `--node_diameter_fpath <path_to_RLE_diameter_file>`
  * It should be noted that the scalar `max_node_diameter` argument is still available and will be deferred to in the case that `node_diameter_fpath` is not specified or ill-formatted; in this case, all nodes will have the same diameter. If both arguments are present, then diameters specified in `node_diameter_fpath` will take precedence.

Next, let's examine the [run-length encoded](https://en.wikipedia.org/wiki/Run-length_encoding) (RLE) file [diam.txt](02/diam.txt):

```csv
# Run, Diameter
500    110.0
500    220.0
500    110.0
500    55.0
```

The RLE format can be comma (CSV) or whitespace delimited. Comment `#` lines are ignored. The first column specifies the *run* and the second column specifies the diameter for the corresponding run. For [diam.txt](02/diam.txt), we see the first 500 nodes have a diameter of 110 Angstroms, the next 500 nodes have diameter 220 Angstroms, the next 500 nodes have diameter 110 Angstroms, and the last set of 500 nodes have diameter 55 Angstroms.

We have provided a [shell script](02/run.sh) which runs the single locus, heterogeneous node simulation. In a terminal, enter:

```bash
./02/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. In a text editor, compare the **tutorial A01** to the **tutorial A02** exported files. Specifically, compare `tutA01.0.csv` to `tutA02.0.csv`, `tutA01.0.pdb` to `tutA02.0.pdb`, and `tutA01.0.pml` to `tutA02.0.pml`. What are the main data differences (besides 3-D coordinate positions) among **tutorial A01** vs **tutorial A02** files (i.e. which columns are similar, which are different)?
1. Using the **tutorial A02** model only, is it still possible to run a 'homogeneous' folding simulation where all nodes have the same diameter (without rebaking), how so?
1. Modify the run-time parameters such that the first 1000 nodes have diameter 110 Angstroms and the last 1000 nodes have diameter 220 Angstroms, re-run the simulation and inspect the output.
1. Modify the run-time parameters such that the first 500 nodes have diameter 220 Angstroms, the next 1000 nodes have diameter 55 Angstroms, and the last 1000 nodes have diameter 85 Angstroms. Note the total monomer node count is 500 + 1000 + 1000 = 2500 nodes! Re-run the simulation and inspect the output.

## Tutorial A03: Multiple loci, homogeneous nodes

![Multiple loci, homogeneous nodes](03/A03.png)

In this tutorial, we extend the model from **tutorial A01** to allow multiple genomic loci.

To begin, inspect the files located in the [03](03) directory:

```bash
cd ./03
ls
```

We see similar files as in **tutorial A01**:

* [A03.json](03/A03.json)
* [A03.ini](03/A03.ini)
* [run.sh](03/run.sh)

Let's first examine [A03.json](03/A03.json):

```json
{
    "name":"tutA03",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "qc": "null",
        "diameter" : "homg",
        "growth" : "mloc",
        "energy" : "unif"
    }
}
```

Here are the relevant differences from **tutorial A01's** JSON:

* `"name": "tutA03"`: To avoid C++ naming clashes, all model names must be unique. For comparison, the name field in **tutorial A01** was *tutA01*.
* `"growth" : "mloc"`: The value *mloc* is used to specify a **m**ultiple **loc**i growth policy, compare with *sloc* value in **tutorial A01**. The following shorthand is also acceptable: `"grow": "mloc"`.
* We have ommitted specifying `"interaction_chromatin" : "null"` since it is the *default* chromatin-to-chromatin interaction policy.

Next, let's examine [A03.ini](03/A03.ini):

```ini
output_dir = ../../../output/tutA/03
ensemble_size = 10
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 1000
num_nodes_1 = 1000
max_node_diameter = 110
num_unit_sphere_sample_points = 50
```

The first relevant difference from **tutorial A01's** INI is:

* `output_dir = ../../../output/tutA/03`: To avoid overwriting data generated by other tutorials, we specify a unique output folder.

However, the key INI fields allowing a multiple loci folding simulation are the lines:

* `num_nodes_0 = 1000`: This configures the first genomic locus as a chromatin chain with 1000 nodes.
* `num_nodes_1 = 1000`: This configures a second genomic locus as a chromatin chain also with 1000 nodes, note the zero-based indexing!

In general, to add an additional genomic locus, simply add the INI field `num_nodes_<locus_index> = <number of nodes>`, where `<locus_index>` is the next subsequent 0-based index and `<number of nodes>` is the desired number of monomer nodes in the chromatin chain.

We have provided a [shell script](03/run.sh) which runs the multiple locus, homogeneous node simulation. In a terminal, enter:

```bash
./03/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. In a text editor, compare the **tutorial A01** to the **tutorial A03** exported files. Specifically, compare `tutA01.0.csv` to `tutA03.0.csv`, `tutA01.0.pdb` to `tutA03.0.pdb`, and `tutA01.0.pml` to `tutA03.0.pml`. What are the main data differences (besides 3-D coordinate positions) among **tutorial A01** vs **tutorial A03** files (i.e. which columns are similar, which are different)?
1. Using the **tutorial A03** model only, is it still possible to run a 'single-locus' folding simulation (without rebaking), how so?
1. Modify the run-time parameters to add a third locus with 250 nodes. Re-run the simulation and inspect the output.
1. Modify the *compile-time* parameters so that the new command line key for the **tutorial A03** null model is *tutA03_null*. Is a re-bake necessary? Re-run the simulation with the modified command line key and inspect the output.

## Tutorial A04: Multiple loci, heterogeneous nodes

![Multiple loci, heterogeneous nodes](04/A04.png)

In this tutorial, we combine **tutorials A01**, **A02**, and **A03** to create a general model which can simulate the folding of multiple genomic loci with heterogeneous node diameters.

To begin, inspect the files located in the [04](04) directory:

```bash
cd ./04
ls
```

We see similar files as in the previous tutorials.

* [A04.json](04/A04.json)
* [A04.ini](04/A04.ini)
* [diam.txt](04/diam.txt)
* [run.sh](04/run.sh)

Let's first examine [A04.json](04/A04.json):

```json
{
    "name":"tutA04",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "qc": "null",
        "diameter" : "hetr",
        "growth" : "mloc",
        "energy" : "uniform"
    }
}
```

At this point, the fields in the JSON should not be a surprise. As covered in the previous tutorials, the relevant JSON settings are:

* `"diameter" : "hetr"`: This allows nodes to have varying diameters, see **tutorial A02**.
* `"growth" : "mloc"`: This allows modeling of multiple loci, see **tutorial A03**.
* We have ommitted specifying `"interaction_chromatin" : "null"` since it is the *default* chromatin-to-chromatin interaction policy.

Next, examine [A04.ini](04/A04.ini):

```ini
output_dir = ../../../output/tutA/04
ensemble_size = 10
max_trials = -1
nuclear_diameter = 5000
num_nodes_0 = 1000
num_nodes_1 = 1000
node_diameter_fpath = diam.txt
num_unit_sphere_sample_points = 50
```

Again, the INI settings should not be a surprise. As covered in the previous tutorials, the relevant INI settings are:

* `output_dir = ../../../output/tutA/04`: To avoid overwriting exported data from other tutorials, we specify a unique output folder. As in previous tutorials, this path can also be specified via the command line arguments `--output_dir <path_to_output_dir>`.
* `num_nodes_0 = 1000`: This sets the first genomic locus to have 1000 nodes, see **tutorial A03**.
* `num_nodes_1 = 1000`: This sets the second genomic locus to also have 1000 nodes, see **tutorial A03**.
* `node_diameter_fpath = diam.txt`: This specifies the path to the run-length encoded (RLE), plain-text file specifying the diameter at all nodes, see **tutorial A02**.

In fact, the only new concept to understand is how the node diameters are specified across multiple loci. If we view [04/diam.txt](04/diam.txt), we see the exact same values as [02/diam.txt](02/diam.txt):

```csv
# Run, Diameter
# 1st genomic locus:
500    110.0
500    220.0
# 2nd genomic locus:
500    110.0
500    55.0
```

Note that comment lines `#` are ignored by the RLE parser and therefore have no functional consequences. For the purposes of assigning node diameters, the chromatin folder "linearizes" the genomic loci in a tip-to-tail fashion and effectively views the result as a single locus:

![tip-to-tail](04/tip2tail.svg)

In this case, the first 1000 nodes represent the first locus and the last 1000 nodes represent the second locus. In more detail, the first 500 nodes of the first locus will have diameter 110 Angstroms, the last 500 nodes of the first locus will have diameter 220 Angstroms, the first 500 nodes of the second locus will have diameter 110 Angstroms, and the last 500 nodes of the second locus will have diameter 55 Angstroms.

To further solidify the concept, let's examine how the node diameters would be assigned if instead file *04/diam.txt* held the following values:

```csv
# Run, Diameter
1200    110.0
800     220.0
```

In this case, all 1000 nodes of the first locus will have a node diameter of 110 Angstroms, the first 200 nodes of the second locus will have a node diameter of 110 Angstroms, and the last 800 nodes of the second locus will have a node diameter of 220 Angstroms. Therefore, the *run* values may overlap boundaries between loci so long as the sum of the runs matches the sum of the node counts among all loci.

We have provided a [shell script](04/run.sh) which runs the multiple locus, heterogeneous node simulation. In a terminal, enter:

```bash
./04/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. In a text editor, compare the **tutorial A01** to the **tutorial A04** exported files. Specifically, compare `tutA01.0.csv` to `tutA04.0.csv`, `tutA01.0.pdb` to `tutA04.0.pdb`, and `tutA01.0.pml` to `tutA04.0.pml`. What are the main data differences (besides 3-D coordinate positions) among **tutorial A01** vs **tutorial A04** files (i.e. which columns are similar, which are different)?
1. Modify the run-time parameters to model an additional third genomic locus with 250 nodes, where the first 100 nodes have diameter 95 Angstroms and the last 150 nodes have diameter 75 Angstroms. Re-run the folding simulation and inspect the output.
1. Using the **tutorial A04** model only, is it still possible to run a 'single-locus', 'homogeneous node' folding simulation (without rebaking), how so?

## Conclusion

Congratulations on finishing the basic chromatin folding tutorial!

One possible question: Why not just use the "multiple loci, heterogeneous node" model from **tutorial A04** for everything? We suggest a best practice to only introduce model complexity if it is truly needed for the research question of interest. Having a more complex model will increase both the computational time of the folding simulation, the maintenance overhead from managing more configuration files, and the data analysis may also be more cumbersome. Lastly, for more advanced folding simulations as described in tutorials [B](../B/tutorial.md), [C](../C/tutorial.md), and those involving persistence length energy models, considerably more time may be spent tuning the parameters than in a simpler model.

Now that you've finished this tutorial, proceed to [tutorial B](../B/tutorial.md) which introduces quality control through variance reduction techniques.
