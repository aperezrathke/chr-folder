# Tutorial E: Chromatin-to-chromatin proximity interaction modeling

**Proximity interactions** can be used to model ligation contacts from experimental data sources such as [Hi-C](https://en.wikipedia.org/wiki/Chromosome_conformation_capture) or from pairwise distances captured by [FISH](https://en.wikipedia.org/wiki/Fluorescence_in_situ_hybridization).

## Overview

This tutorial will cover the concept of chromatin-to-chromatin *proximity interaction modeling* using a two-level fractal Monte Carlo simulation. This tutorial will show how to specify both *knock-in* and *knock-out* proximity interactions:

* A *knock-in* interaction constrains two genomic regions to be spatially *co-located* to within a threshold Euclidean distance.
* A *knock-out* interaction constrains two genomic regions to be spatially *separated* by greater than a threshold Euclidean distance.

![knock-in vs. knock-out](./kiko.svg)

This tutorial assumes that the following have been completed:

* [Tutorial A: Chromatin folding basics](../A/tutorial.md)
* [Tutorial B: Quality control introduction](../B/tutorial.md)
* [Tutorial C: Quality control with fractal Monte Carlo](../C/tutorial.md)

This tutorial is organized as follows:

* **Getting started**: Instructions for model baking, etc.
* **E01**: Simulation with homogeneous knock-in interactions
* **E02**: Simulation with heterogeneous knock-in interactions
* **E03**: Simulation with index masked knock-in interactions
* **E04**: Simulation with bit masked knock-in interactions
* **E05**: Simulation with knock-out interactions
* **E06**: Simulation with index masked knock-out interactions
* **E07**: Simulation with bit masked knock-out interactions
* **E08**: Simulation with knock-in and knock-out interactions
* **E09**: Simulation with bit masked knock-in and knock-out interactions
* **E10**: Simulation with budgeted knock-in interactions
* **E11**: Simulation with budgeted knock-out interactions

## Getting started

This tutorial is designed for running on a Linux operating system. All paths given are relative to the directory containing this [tutorial.MD](./) unless noted otherwise. Any path starting with a `/` (e.g. `/scripts`) is relative to the [root project directory](../../).

### Bun

Let's get started by inspecting the .bun file for this tutorial: [E.bun](E.bun):

```bash
cat ./E.bun
```

We see output along the following:

```
# FILENAME, USAGE, CMDKEY
./E.json, main, tutE
./E.budg.json, main, tutE_budg
```

The JSON specification [E.json](./E.json) will be used for lessons **E01** through **E09**. The JSON specification [E.budg.json](./E.budg.json) is used for lessons **E10** and **E11**. In both cases, notice the *usage* is set to `main` - this is simply a convention to signify that simulations are *not* intended for null distribution modeling.

### JSON

Here, we will examine the primary JSON specification [E.json](./E.json) used for most lessons in this tutorial. The other specificaton [E.budg.json](./E.budg.json) will be covered later in **tutorial E10**. To begin:

```bash
cat ./E.json
```

We see the following:

```json
{
    "name": "tutE",
    "arch0": {
        "diameter" : "hetr",
        "growth" : "mloc",
        "intr_chr" : "prox",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        },
        "qc": "null"
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": "null"
    },
   "top": "spec_parent"
}
```

The JSON describes a two-level (parent-child) fractal simulation with support for heterogeneous node radii and multiple loci.

Within the JSON archetype `arch0` block, we see a new configuration setting `"intr_chr" : "prox"`. This informs the bake process to enable chromatin-to-chromatin *proximity* interaction modeling. The equivalent long-hand is `"interaction_chromatin" : "proximity"`. This setting is defined within the archetype block to ensure both levels of the fractal simulation support this feature.

Note, a fractal simulation is **not** required to support proximity interaction modeling! In fact, the following JSON configuration will work just fine:

```json
{
    "name":"tutE_no_fractal",
    "top": "spec",
    "spec": {
        "trial_runner": "canonical",
        "diameter" : "hetr",
        "growth" : "mloc",
        "intr_chr" : "prox",
        "energy" : "unif",
        "qc": "null"
    }
}
```

where options such as `"diameter" : "homg"` or `"growth" : "sloc"` are also supported.

In practice, we have found that fractal simulations are **more efficient** at satisfying proximity constraints as the landmark trial runner avoids full regrowth of sample chains.

### INI and Fragments

All lessons within this tutorial re-use the same parent and child INI configurations for their core settings.

#### Parent INI

If we examine the top-level [E.parent.ini](./E.parent.ini):

```bash
cat ./E.parent.ini
```

We see the parent is configured with the following settings:

```ini
output_dir = ../../../output/tutE
ensemble_size = 5
max_trials = -1
num_nodes_0 = 500
nuclear_diameter = 5000
max_node_diameter = 200
num_unit_sphere_sample_points = 50
report_perc_uniq = 0

###############################
# Chromatin-to-chromatin interactions:
# Path to monomer node to fragment region mapping
chr_frag = frags.csv

###############################
# Landmark:
qc_schedule_lag_slot0 = 20
qc_sampler_power_alpha_slot0 = 1.0
# Relative path to child .ini
conf_child = E.child.ini
```

We should notice a new setting:

* `chr_frag = frags.csv`: This specifies the path to a CSV formatted file `frags.csv` for defining contiguous spans of chromatin nodes which we call _fragment_ regions. **The fragment regions are what are ultimately constrained by the knock-in or knock-out proximity interactions.** Note, if the `chr_frag` option is *not set* (via INI file or command line `--chr_frag`), the simulation will **default** to using each monomer bead as an individual fragment (e.g., if the simulation contains 500 monomer beads, then 500 fragments - a single fragment for each bead - will be implicitly defined).

#### Fragments

If we inspect `frags.csv`:

```bash
cat ./frags.csv
```

We see the file contents:

```csv
8,14
129,135
462,469
```

Each line of `frags.csv` represents a chromatin fragment (i.e. *genomic region*) defined by the inclusive span `[start,end]` where `start` is the 0-based index of the *first* node in the fragment and `end` is the 0-based index of the *last* node in the fragment; all monomer nodes with indices greater than or equal to `start` and less than or equal to `end` are considered part of the fragment. Further, the `end` index must be greater than or equal to the `start` index. In this case, `frags.csv` defines *3* fragments which are indexed by their line (0-based):

* **Fragment 0** near the beginning of the chain (nodes 8-14)
* **Fragment 1** near the middle of the chain (nodes 129-135)
* **Fragment 2** near the end of the chain (nodes 462-469)

Note, fragment definitions need not be disjoint from each other; for instance, the following defines two perfectly valid *overlapping* fragments:

```csv
8,14
12,16
```

**However, if two fragments are configured to interact with each other, then they MUST not overlap!**

A fragment may be defined as spanning only a *single* monomer node like so:

```csv
8,8
```

The previous example defines a single fragment consisting solely of the 9th monomer node (which is identified through its 0-based index `8`).

#### Child INI

Let's briefly examine the [E.child.ini](./E.child.ini) configuration:

```ini
ensemble_size = 5
max_trials = 1
```

Other than the setting `chr_frag = frags.csv` in the parent INI, there should be nothing new in either the parent or child INI that have not been covered in previous tutorials. Together, the INI configurations define a 2-D fractal simulation, where the child simulation grows an ensemble of size 5, and the top-level parent selects among the child ensembles to produce its final output ensemble. Please refer to tutorials [A](../A/tutorial.md), [B](../B/tutorial.md), and [C](../C/tutorial.md) for further details on INI configuration settings.

### Bake

Please bake all models with the provided [bake.sh](bake.sh) shell script:

```bash
./bake.sh
```

## Tutorial E01: Simulation with homogeneous knock-in interactions

This tutorial will show how to specify fragment pairs which may participate in knock-in, chromatin-to-chromatin proximity interactions. 

For this tutorial, all knock-in interactions are *homogeneous* meaning that only a single Euclidean distance threshold is used for all interactions. In tutorial **E02**, we will relax this setting and allow each knock-in interaction to have its own knock-in threshold.

### INI

To begin, let's examine [E01.ini](./01/E01.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/01

###############################
# Chromatin-to-chromatin interactions:
# Path to fragment knock-in or knock-out proximity interactions
chr_knock_in = ../prox.csv
# Default knock-in distance is 795 Angstroms as reported in:
#  Giorgetti, Luca, et al. "Predictive polymer modeling reveals coupled
#      fluctuations in chromosome conformation and transcription." Cell 157.4
#      (2014): 950-963.
#
# Here, we set the knock-in distance to 100 Angstroms for more obvious
# visualization of the proximity interaction(s)
chr_knock_in_dist = 100
```

We should see several new options:

* `arch = ../E.parent.ini`: This sets the path to the INI file **archetype**. Similar to how JSON specifications can have an *archetype* for resolving common settings, INI files may optionally specify an archetype. In this case, the INI file `E.parent.ini` in the base tutorial **E** folder will serve as the archetype. This means any configuration setting *not* explicitly specified in [E01.ini](./01/E01.ini) will be resolved using the settings within [E.parent.ini](./E.parent.ini). The resolution order for any queried setting is as follows: **1)** command line, **2)** the INI file itself, **3)** the archetype INI file if present, **4)** any *parent* configuration settings if within a child fractal simulation. Note, an INI serving as archetype cannot itself define a archetype. The specific advantage of using the INI archetype here is that we do not have to repeat all the common boilerplate settings required for configuring a fractal simulation!
* `chr_knock_in = ../prox.csv`: This specifies the path to a CSV formatted file `prox.csv` defining which *fragment* pairs (as defined by the `chr_frag` option) may participate in knock-in proximity interactions.
* `chr_knock_in_dist = 100`: This specifies that *all* knock-in fragment pairs must be within 100 Angstroms of each other (i.e. all interactions are *homogeneous*). Note, if this option is not specified, then the default Hi-C ligation threshold of 795 Angstroms is automatically used (see *Giorgetti, Luca, et al. "Predictive polymer modeling reveals coupled fluctuations in chromosome conformation and transcription." Cell 157.4 (2014): 950-963.*).

All of these options may be specified on the command line using the `--<key> <value>` pattern from previous tutorials. For example, the command line string `--chr_knock_in_dist 100` will equivalently set the knock-in interaction distance to be 100 Angstroms.

### Interactions

If we inspect [prox.csv](./prox.csv) located in the base folder of tutorial *E*:

```csv
0,1
0,2
1,2
```

The file is listing which fragment pairs are to participate in knock-in, chromatin-to-chromatin proximity interactions:

* The first line with entry `0,1` is denoting that fragment `0` is to participate in a knock-in interaction with fragment `1`. Recall, the fragment spans themselves are defined in [frags.csv](./frags.csv) (where fragment `0` maps to monomer nodes `8-14` and fragment `1` maps to monomer nodes `129-135` - see *Getting Started* section for review of `chr_frag` setting).
* The second line with entry `0,2` is denoting that fragments `0` and `2` are to participate in a knock-in interaction.
* The third line with entry `1,2` is denoting that fragments `1` and `2` are to participate in a knock-in interaction.

In this tutorial, *all* listed interactions within [prox.csv](./prox.csv) will be enforced. For certain use cases such as perturbation studies, it may be convenient to enforce only a subset of the interactions; this can be achieved using *masking* which will be introduced in tutorial **E03**.

### Export options

If we inspect the bash script for this lesson [run.sh](./01/run.sh), we notice a change has been made to the export options:

```bash
EXPORT_ARG="-export_intr_chr -export_csv -export_pdb -export_pml -export_log_weights -export_extended"
```

Specifically, there is a new setting `-export_intr_chr` which will create selections in the exported PML files containing both the fragment regions and also the paired interactions (with selections named according to **1-based** indexing as is the convention within PyMOL).

### Putting it all together

Tutorial **E01** is configured to run a 2-D fractal simulation with *3* knock-in proximity interactions. The knock-in interactions constrain the simulation to only generate samples that satisfy the specified proximity constraints. Specifically, each knock-in interaction requires that there exists a pair of monomers - one from each participating chromatin fragment - that are separated by at most 100 Angstroms. **Further, this interaction distance is measured between the two closest points on the surface of the monomer pairs and *not* the center-to-center distance!** Note, knock-in proximity interactions do not require that all monomer pairs from the participating fragments be within the knock-in distance; rather, they only require that *at least one* of the monomer pairs be within the knock-in distance.

The following figure illustrates that the interaction distance is defined as the closest distance between the monomer node surfaces:

![interaction distance](./01/dist.svg)

Now let's run the simulation. In a terminal, enter:

```bash
./01/run.sh
```

Here is an example polymer satisfying the homogeneous knock-in proximity interaction constraints (where the 3 interacting fragment regions have been colored red, yellow, and cyan):

![knock-in homogeneous](./01/ki.homg.png)

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL.
1. Increase the knock-in distance threshold to 300 Angstroms and re-run the simulation. Visualize the resulting output in PyMOL.
1. Modify the simulation so that only fragments 0 and 2 are forced to interact within the knock-in threshold distance. Re-run the simulation and visualize in PyMOL.
1. Modify the simulation so fragment 0 is mapped to the region spanning monomer nodes 50 through 60.  Re-run the simulation and visualize in PyMOL.

## Tutorial E02: Simulation with heterogeneous knock-in interactions

In tutorial **E01**, all knock-in, chromatin-to-chromatin interactions were constrained to interact according to a single, global threshold distance `chr_knock_in_dist`. In this tutorial, we show how to specify different interaction distances depending on which fragment pairs are interacting. This need may arise when modeling proximity constraints based on fluorescent *in situ* hybridization (FISH). 

Let's start by examining [E02.ini](./02/E02.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/02
chr_knock_in = ../prox.csv
chr_knock_in_dist_fpath = ki.dist.txt
```

The INI configuration is very similar to [E01.ini](./01/E01.ini) from the previous tutorial. However, there is *one* notable difference:

* `chr_knock_in_dist_fpath = ki.dist.txt`: This option specifies the path to a run-length encoded (RLE) file `ki_dist.txt` which specifies the knock-in proximity distance at each interacting fragment pair. If both `chr_knock_in_dist` and `chr_knock_in_dist_fpath` options are specified, then the `chr_knock_in_dist` option is ignored in favor of `chr_knock_in_dist_fpath`.

Let's examine [ki.dist.txt](./02/ki.dist.txt):

```csv
# Run, Dist (Angstroms)
1 250.0
2 500.0
```

The first entry `1 250.0` specifies that the first interacting fragment pair (defined in [prox.csv](./prox.csv) as `0,1`) has a knock-in threshold distance of `250.0` Angstroms. The second entry `2 500.0` specifies that the next *two* interacting fragment pairs (`0,2` and `1,2`) each have a knock-in threshold distance of `500.0` Angstroms.

Now let's run the simulation. In a terminal, enter:

```bash
./02/run.sh
```

Here is an example polymer satisfying the heterogeneous knock-in proximity interaction constraints (where the 3 interacting fragment regions have been colored red, yellow, and cyan):

![knock-in heterogeneous](./02/ki.hetr.png)

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL.
1. How would you modify the simulation so that fragment pair `0,1` has a knock-in distance threshold of `200.0` Angstroms, fragment pair `0,2` has a knock-in distance threshold of `300.0` Angstroms, and fragment pair `1,2` has a knock-in distance threshold of `100.0` Angstroms? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.

## Tutorial E03: Simulation with index masked knock-in interactions

For perturbation modeling, it may be convenient to enforce only a subset of the chromatin-to-chromatin proximity interactions. The subset may be specified using *mask* operations, specifically *index* and *bit* masks. This lesson will focus on *index* masks, bit masks will be covered in the next lesson.

Let's start by examining [E03.ini](./03/E03.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/03
chr_knock_in = ../prox.csv
chr_knock_in_dist = 100.0
chr_knock_in_index_mask = 0,2
```

Again, the INI configuration is very similar to [tutorial E01's ini](./01/E01.ini) with *one* notable difference:

* `chr_knock_in_index_mask = 0,2`: The index mask specifies the indices of the interactions to *retain* within the `chr_knock_in` file [prox.csv](./prox.csv). The mask is a set of comma-separated, **0-based** indices where each index *i* identifies the *i-th* interaction listed within the file [prox.csv](./prox.csv). In this case, the value `0,2` indicates that interaction `0` (between fragments `0` and `1`) and interaction `2` (between fragments `1` and `2`) should be retained, whereas interaction `1` (between fragments `0` and `2`) should be discarded (i.e. unenforced).

What if we only wanted to retain a single interaction, say the knock-in between fragments `0` and `2`? This interaction pair is listed *second* in the `chr_knock_in` file [prox.csv](./prox.csv) and therefore is identified by *index* `1`. We can specify to retain this single interaction like so (notice we no longer use a comma):

```ini
chr_knock_in_index_mask = 1
```

If instead, we would rather specify the index mask using an *external file*, say *index_mask.csv*, this is also possible by using the `chr_knock_in_index_mask_fpath` option:

```ini
chr_knock_in_index_mask_fpath = index_mask.csv
```

and the file contents of *index_mask.csv* would simply be a comma-delimited string of interaction indices to retain.

If both `chr_knock_in_index_mask` and `chr_knock_in_index_mask_fpath` are specified (via INI or command-line), then the `chr_knock_in_index_mask` value will be used and the `chr_knock_in_index_mask_fpath` will be ignored.

Now let's run the simulation. In a terminal, enter:

```bash
./03/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL. How many *fragment* selections are present in each of the PML files? How many knock-in (*ki*) selections are present in each of the PML files?
1. How would you modify the simulation so that only the knock-in interactions between fragment pairs `0,2` and `1,2` are retained? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.

## Tutorial E04: Simulation with bit masked knock-in interactions

*Bit* masks are an equivalent alternative to index masks (see tutorial **E03**). For example, it may be more convenient to use bit masks rather than index masks when searching for (non-)embeddable constraint configurations.

Let's start by examining [E04.ini](./04/E04.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/04
chr_knock_in = ../prox.csv
chr_knock_in_dist = 100.0
chr_knock_in_bit_mask = 101
```

Again, the INI configuration is very similar to both [tutorial E01's ini](./01/E01.ini) and [tutorial E03's ini](./03/E03.ini) with *one* notable difference:

* `chr_knock_in_bit_mask = 101`: The bit mask specifies which interactions to retain within the `chr_knock_in` file [prox.csv](./prox.csv). The mask is a binary `0|1` string where the *i-th* interaction is retained if bit *i* is `1` and discarded if bit *i* is `0`. Note, the **leftmost** position in the string is bit *0* and the rightmost position is bit *n-1* where *n* is the number of interactions listed in [prox.csv](./prox.csv)! In this case, the bit mask is equivalent to setting `chr_knock_in_index_mask = 0,2`. Specifically, the bit mask indicates that interaction `0` (between fragments `0` and `1`) and interaction `2` (between fragments `1` and `2`) should be retained, whereas interaction `1` (between fragments `0` and `2`) should be discarded (i.e. unenforced).

What if we only wanted to retain a single interaction, say the knock-in between fragments `0` and `2`? This interaction pair is listed *second* in the `chr_knock_in` file [prox.csv](./prox.csv) and therefore is identified by *bit* position `1`. We can specify to retain this single interaction like so:

```ini
chr_knock_in_bit_mask = 010
```

As another example, what if we wanted to retain the interactions specified by the equivalent index mask `chr_knock_in_index_mask = 1,2`? Here is the equivalent bit mask:

```ini
chr_knock_in_bit_mask = 011
```

If instead, we would rather specify the bit mask using an *external file*, say *bit_mask.txt*, this is also possible by using the `chr_knock_in_bit_mask_fpath` option:

```ini
chr_knock_in_bit_mask_fpath = bit_mask.txt
```

and the file contents of *bit_mask.txt* would simply be a `0|1` binary string indicating which interactions to retain. Note, the format of `bit_mask.txt` is somewhat arbitrary as the parser only retains `0` or `1` characters and all other characters are ignored.

If both `chr_knock_in_bit_mask` and `chr_knock_in_bit_mask_fpath` are specified (via INI or command-line), then the `chr_knock_in_bit_mask` value will be used and the `chr_knock_in_bit_mask_fpath` will be ignored.

Further, if both a *bit* mask (via external file, INI, or command line) and an *index* mask are specified, the *bit* mask will be retained and the *index* mask will be ignored.

Now let's run the simulation. In a terminal, enter:

```bash
./04/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL. How many *fragment* selections are present in each of the PML files? How many knock-in (*ki*) selections are present in each of the PML files?
2. What is the equivalent `chr_knock_in_index_mask` for `chr_knock_in_bit_mask = 111`? What is the equivalent `chr_knock_in_bit_mask` for `chr_knock_in_index_mask = 2`?
3. How would you modify the simulation so that only the knock-in interactions between fragment pairs `0,1` and `0,2` are retained? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.
4. Assume that `chr_knock_in = prox2.csv` where `prox2.csv` contains *n=4* interactions. What is the *bit* mask to retain only the 4th interaction? What is the *index* mask to retain only the 4th interaction?

## Tutorial E05: Simulation with knock-out interactions

This lesson will introduce modeling with *knock-out*, chromatin-to-chromatin proximity interactions. A *knock-out* interaction constrains **all** monomer nodes within the participating fragment pairs to be separated by a Euclidean distance *greater than* the knock-out distance threshold. Let's contrast with *knock-in* interactions:

* **Knock-out**: All monomer pairs `(i,j)`, where `i` is from first interacting fragment and `j` is from second interacting fragment, satisfy `distance(i,j) > ko_thresh`, where `ko_thresh` is the knock-out distance threshold.
* **Knock-in**: At least one monomer pair `(i,j)`, where `i` is from first interacting fragment and `j` is from second interacting fragment, satisfies `distance(i,j) <= ki_thresh`, where `ki_thresh` is the knock-in distance threshold.

We will see that *knock-out* interactions are specified in very similar fashion to *knock-in* interactions. Generally, we can simply replace the `chr_knock_in` setting with the analogous `chr_knock_out` setting! The following list shows the *knock-out* configuration settings with their corresponding *knock-in* configuration settings:

* `chr_knock_out` is analogous to `chr_knock_in`
* `chr_knock_out_dist` and `chr_knock_out_dist_fpath` are analogous to `chr_knock_in_dist` and `chr_knock_in_dist_fpath` respectively
* `chr_knock_out_index_mask` and `chr_knock_out_index_mask_fpath` are analogous to `chr_knock_in_index_mask` and `chr_knock_in_index_mask_fpath` respectively
* `chr_knock_out_bit_mask` and `chr_knock_out_bit_mask_fpath` are analogous to `chr_knock_in_bit_mask` and `chr_knock_in_bit_mask_fpath` respectively

Because of the high correspondence between *knock-out* and *knock-in* configuration settings, we will assume familiarity from the previous *knock-in* lessons and will mainly give examples using the `chr_knock_out` settings.

Let's start by examining [E05.ini](./05/E05.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/05
chr_knock_out = ../prox.csv
chr_knock_out_dist = 400
```

Notice that the *knock-out* tutorial [**EO5**.ini](./05/E05.ini) is very similar to the *knock-in* tutorial [**E01**.ini](./01/E01.ini). We have simply replaced settings `chr_knock_in` and `chr_knock_in_dist` with the corresponding settings `chr_knock_out` and `chr_knock_out_dist`. Let's examine the relevant settings:

* `chr_knock_out = ../prox.csv`: We are re-using the interactions file [prox.csv](./prox.csv) but now the simulation will enforce knock-out separations among the listed fragment pairs.
* `chr_knock_out_dist = 400`: This configures *homogeneous* knock-out interactions using a global separation threshold of 400 Angstroms.

For **heterogeneous** knock-out interactions, in which each fragment pair may have differing separation thresholds, there is the corresponding `chr_knock_out_dist_fpath`  setting which is specified in the same manner as `chr_knock_in_dist_fpath` (see tutorial **E02**).

Note, if no knock-out distance threshold is specified, the default behavior is to set a homogeneous knock-out distance of `1.25 * <default knock-in distance>`, where the default knock-in distance is 795 Angstroms (see tutorial **E01**).

Now let's run the simulation. In a terminal, enter:

```bash
./05/run.sh
```

Here is an example polymer satisfying homogeneous knock-out proximity interaction constraints (where the 3 interacting fragment regions have been colored red, yellow, and cyan):

![knock-out homogeneous](./05/ko.homg.png)

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL.
2. How would you modify the simulation so that fragment pair `0,1` has a knock-out distance threshold of `200.0` Angstroms, fragment pair `0,2` has a knock-out distance threshold of `300.0` Angstroms, and fragment pair `1,2` has a knock-out distance threshold of `400.0` Angstroms? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.

## Tutorial E06: Simulation with index masked knock-out interactions

This lesson will demonstrate index masking for knock-out, chromatin-to-chromatin interactions. We assume the corresponding knock-in tutorial **E03** has been completed and the reader is familiar with index masking operations.

Let's start by examining [E06.ini](./06/E06.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/06
chr_knock_out = ../prox.csv
chr_knock_out_dist = 400
chr_knock_out_index_mask = 0,2
```

The only relevant difference from previous lessons is the setting `chr_knock_out_index_mask = 0,2`. This configures the simulation to only enforce the knock-out interactions at indices `0` and `2` according to the listed order within [prox.csv](./prox.csv).

In summary, this lesson demonstrates index masking for knock-out interactions. We hope the correspondence is obvious to the knock-in tutorial **E03**, simply replace all instances of `chr_knock_in` with `chr_knock_out`! For more detail on index masking, please refer to tutorial **E03**.

Now let's run the simulation. In a terminal, enter:

```bash
./06/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL. How many *fragment* selections are present in each of the PML files? How many knock-out (*ko*) selections are present in each of the PML files?
2. How would you modify the simulation so that only the knock-out interactions between fragment pairs `0,2` and `1,2` are retained? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.

## Tutorial E07: Simulation with bit masked knock-out interactions

This lesson will demonstrate bit masking for knock-out, chromatin-to-chromatin interactions. We assume the corresponding knock-in tutorial **E04** has been completed and the reader is familiar with bit masking operations.

Let's start by examining [E07.ini](./07/E07.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/07
chr_knock_out = ../prox.csv
chr_knock_out_dist = 400
chr_knock_out_bit_mask = 101
```

The only relevant difference from previous lessons is the setting `chr_knock_out_bit_mask = 101`. This configures the simulation to only enforce the knock-out interactions at bit positions `0` and `2` according to the listed order within [prox.csv](./prox.csv).

In summary, this lesson demonstrates bit masking for knock-out interactions. We hope the correspondence is obvious to the knock-in tutorial **E04**, simply replace all instances of `chr_knock_in` with `chr_knock_out`! For more detail on bit masking, please refer to tutorial **E04**.

Now let's run the simulation. In a terminal, enter:

```bash
./07/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL. How many *fragment* selections are present in each of the PML files? How many knock-out (*ko*) selections are present in each of the PML files?
1. What is the equivalent `chr_knock_out_index_mask` for `chr_knock_out_bit_mask = 111`? What is the equivalent `chr_knock_out_bit_mask` for `chr_knock_out_index_mask = 2`?
1. How would you modify the simulation so that only the knock-out interactions between fragment pairs `0,1` and `0,2` are retained? Perform modification(s), re-run simulation, and visualize a few of the resulting structures in PyMOL.
1. Assume that `chr_knock_out = prox2.csv` where `prox2.csv` contains *n=4* interactions. What is the *bit* mask to retain only the 4th interaction? What is the *index* mask to retain only the 4th interaction?

## Tutorial E08: Simulation with knock-in and knock-out interactions

This lesson will model both *knock-in* and *knock-out* chromatin-to-chromatin proximity interactions within the same simulation.

Specifically, we will use both interaction types to constrain the separation between the fragment pairs such that:

* All monomer pairs `(i,j)`, where `i` is from first interacting fragment and `j` is from second interacting fragment, satisfy `distance(i,j) > ko_thresh`, where `ko_thresh` is the **knock-out** distance threshold.
* At least one monomer pair `(i,j)`, where `i` is from first interacting fragment and `j` is from second interacting fragment, satisfies `distance(i,j) <= ki_thresh`, where `ki_thresh` is the **knock-in** distance threshold.
* The relation `ko_thresh < ki_thresh` holds.

In other words, we can combine the interaction types to *bound* the upper and lower separation distances between the fragment pairs, where the lower bound is defined by the *knock-out* distance and the upper bound is defined by the *knock-in* distance.

Note, we are *not* modeling disjoint sets of *knock-in* and *knock-out* interactions. This use case is more easily specified using masking operations and will be covered in the next tutorial **E09**.

This non-disjoint model can work with *homogeneous* and *heterogeneous* interaction distances so long as the the knock-out distance is less than the corresponding knock-in distance.

Let's start by examining [E08.ini](./08/E08.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/08

###############################
# Knock-in:
chr_knock_in = ../prox.csv
chr_knock_in_dist = 600

###############################
# Knock-out:
chr_knock_out = ../prox.csv
chr_knock_out_dist = 400
```

We have configured both `chr_knock_in` and `chr_knock_out` to refer to the same interactions file [prox.csv](./prox.csv). We then set the following proximity bounds:

* `chr_knock_out_dist = 400`: Each interacting fragment pair must separated by greater than 400 Angstroms (lower bound)
* `chr_knock_in_dist = 600`: At least one interacting *monomer* pair from each interacting fragment must be within 600 Angstroms (upper bound).

Now let's run the simulation. In a terminal, enter:

```bash
./08/run.sh
```

Here is an example polymer satisfying the *bounded* knock-in, knock-out proximity interaction constraints (where the 3 interacting fragment regions have been colored red, yellow, and cyan):

![bounded, homogeneous knock-in and knock-out](./08/kiko.homg.png)

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL. How many *fragment* selections are present in each of the PML files? How many knock-in (*ki*) selections are present in each of the PML files? How many knock-out (*ko*) selections are present in each of the PML files?
2. For the knock-in and knock-out interactions to share the same listing [prox.csv](./prox.csv), in the absence of masking, why must this relation hold: `chr_knock_out_dist < chr_knock_in_dist`?

## Tutorial E09: Simulation with bit masked knock-in and knock-out interactions

In this tutorial, we simultaneously model *disjoint* sets of knock-in and knock-out chromatin-to-chromatin proximity interactions using a bit mask.

Let's start by examining [E09.ini](./09/E09.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/09

###############################
# Knock-in:
chr_knock_in = ../prox.csv
chr_knock_in_dist = 400
chr_knock_in_bit_mask = 101

###############################
# Knock-out:
chr_knock_out = ../prox.csv
chr_knock_out_dist = 800
chr_knock_out_bit_mask = 010
```

As in tutorial **E08**, the interactions file [prox.csv](./prox.csv) is shared by both `chr_knock_in` and `chr_knock_out`. However, there are two key configuration differences:

* The knock-in distance (`chr_knock_in_dist = 400`) is **less than** the knock-out distance (`chr_knock_out_dist = 800`).
* The bit masks specify that the 1st and 3rd interactions listed in [prox.csv](./prox.csv) are constrained as *knock-in* interactions (`chr_knock_in_bit_mask = 101`) whereas the 2nd interaction listed is constrained as a *knock-out* interaction (`chr_knock_out_bit_mask = 010`).

In summary, all proximity interactions are specified within [prox.csv](./prox.csv). However, the disjoint bit masks `chr_knock_in_bit_mask` and `chr_knock_out_bit_mask` are used to designate the interactions as knock-in or knock-out respectively.

Now let's run the simulation. In a terminal, enter:

```bash
./09/run.sh
```

Here is an example polymer satisfying the disjoint knock-in, knock-out proximity interaction constraints (where the 3 interacting fragment regions have been colored red, yellow, and cyan):

![disjoint knock-in and knock-out](./09/kiko.bitmask.png)

Can you deduce which color is fragment `1`? Recall, fragment `1` is constrained to *knock-in* interact with both fragments `0` and `2`; also, fragments `0` and `2` are constrained to *knock-out* interact. If you answered that fragment `1` is red, you are correct!

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. After running the simulation, inspect the output folder and visualize a few of the PML structures using PyMOL.
1. How would you modify the simulation to use index masks instead of bit masks?

## Tutorial E10: Simulation with budgeted knock-in interactions

In previous tutorials, simulations were required to enforce **all** interaction constraints. Only samples which satisfied all constraints were allowed to proceed through the polymer growth process; if at any point the sample failed to satisfy any interaction constraint, it would be discarded and regrown. However, as the number of interaction constraints increases, this may be *impractical* both from computational and geometric perspectives (i.e. as the number of constraints increases, it may take much longer in terms of CPU time to satisfy all constraints, and/or a subset of the constraints may not be geometrically feasible and therefore can never be satisfied).

To address these *practical limitations* in modeling a large number of constraints, we introduce the concept of a **failure budget**. The budget, in [0,1], specifies the proportion of constraints that may fail before a sample is culled and regrown. As long as the proportion of failed constraints does not exceed the budget, then the sample may continue being grown and may eventually succeed; however, the moment the proportion of failures exceeds the threshold, then the sample must be regrown.

In this tutorial, we introduce the failure budget concept for *knock-in*, chromatin-to-chromatin interactions. There is a separate failure budget for *knock-out*, chromatin-to-chromatin interactions that will be demonstrated in the next **tutorial E11**.

This tutorial is configured *identical* to **tutorial E01** except that budgeted knock-in interactions are enabled.

### JSON

To enable budgeted chromatin-to-chromatin interaction modeling, we must *bake* with a JSON specification containing the entry `"intr_chr" : "prox_budg"`. To begin, let's examine the JSON specification used by this lesson [E.budg.json](./E.budg.json):

```bash
cat ./E.budg.json
```

We see the following:

```json
{
    "name": "tutE_budg",
    "arch0": {
        "diameter" : "hetr",
        "growth" : "mloc",
        "intr_chr" : "prox_budg",
        "energy" : "unif"
    },
    "spec_parent": {
        "arch": "arch0",
        "trial_runner": {
            "type": "lmrk",
            "schedule": {
                "type": "lag_homg",
                "lag_slot": "0"
            },
            "select": {
                "type": "power",
                "alpha_slot": "0"
            },
            "sub": "spec_child"
        },
        "qc": "null"
    },
    "spec_child": {
        "arch": "arch0",
        "trial_runner": "canon",
        "qc": "null"
    },
   "top": "spec_parent"
}
```

Here are the relevant differences in [E.budg.json](./E.budg.json) from [E.json](./E.json) used by previous tutorials:

* The field `"name": "tutE_budg"` has been modified to be distinct from the corresponding field in [E.json](./E.json). In [E.json](./E.json), this field is set to `"name": "tutE"`. All JSON specifications listed in the same [bun](./E.bun) must have unique `"name"` fields else the bake process will fail.
* The field `"intr_chr" : "prox_budg"` within `"arch0"` has been modified from the corresponding field in [E.json](./E.json). In [E.json](./E.json), this field is set to `"intr_chr" : "prox"`. By setting to `"prox_budg"`, we enable budgeted chromatin-to-chromatin interaction support.

### Ini

Let's examine [E10.ini](./10/E10.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/10
chr_knock_in = ../prox.csv
chr_knock_in_dist = 100
chr_knock_in_fail_budget = 0.5
```

The INI configuration is very similar to [E01.ini](./01/E01.ini) used by **tutorial E01**. The only additional setting is `chr_knock_in_fail_budget = 0.5`. This says that up to **50%** of the *knock-in*, chromatin-to-chromatin interactions *may fail* before a polymer sample is culled and regrown. Here are some examples:

* If there are 4 knock-in interactions, then all observed polymer samples can contain up to 2 failed interactions.
* If there are 3 knock-in interactions, then all observed polymer samples can contain up to 2 failed interactions, notice that **rounding to the nearest integer** is implicitly performed (0.5 * 3 = 1.5 which rounds to 2)!

### Masking

What happens in the case of masking? If we additionaly specify a knock-in interaction index mask (see **tutorial E03**) or bit mask (see **tutorial E04**), then the fail budget is applied to the **retained** interactions after masking has been applied! Here are some examples:

* If there are 10 total knock-in interactions and a bit mask is applied which retains 6 interactions (i.e. 4 interactions were removed by the mask), then the setting `chr_knock_in_fail_budget = 0.5` will result in all observed polymer samples containing up to 3 failed interactions (6 * 0.5 = 3).
* If there are 50 knock-in interactions and an index mask is applied which retains 9 interactions, then the setting `chr_knock_in_fail_budget = 0.5` will result in all observed polymer samples containing up to 5 failed interactions (9 * 0.5 = 4.5 which rounds to 5).

In other words, for an index mask, the fail fraction is relative to the number of indices in the mask. For a bit mask, the fail fraction is relative to the number of *1's* in the mask.

### Caveats

Why not always use failure budget simulations? For instance, if we set `chr_knock_in_fail_budget = 0.0`, then 0% of the knock-in interactions are allowed to fail (i.e. all constraints must be satisfied); the resulting polymer samples will have identical properties to those polymer samples in **tutorial E01**. However, maintaining a failure budget at each polymer sample has *CPU run time overhead*. Therefore, if we know we will be using a 0% failure budget, then it is more efficient to use the non-budgeted JSON specification as in previous tutorials.

### Run

Now let's run the simulation. In a terminal, enter:

```bash
./10/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. What is the dispatch key used by the shell script [10/run.sh](./10/run.sh)? How is it different from the dispatch key used in previous lessons?
1. Run the simulation 5 times using `chr_knock_in_fail_budget = 0.5`, what is the average number of trials needed for the simulation to complete? Now, run the simulation 5 times using `chr_knock_in_fail_budget = 0.0`, what is the average number of trials needed for the simulation to complete?
1. How would you modify the simulation to remove via masking the 2nd (index=1) knock-in interaction? In this case, what is the maximum number of knock-in interactions that may fail when `chr_knock_in_fail_budget = 0.5`?

## Tutorial E11: Simulation with budgeted knock-out interactions

This tutorial demonstrates budgeted, knock-out chromatin-to-chromatin interaction modeling. The only difference from the previous tutorial is that the failure budget restriction is now applied to knock-out constraints instead of knock-in constraints.

To begin, let's examine [E11.ini](./11/E11.ini):

```ini
arch = ../E.parent.ini
output_dir = ../../../output/tutE/11
chr_knock_out = ../prox.csv
chr_knock_out_dist = 400
chr_knock_out_fail_budget = 0.5
```

The INI configuration is very similar to [E05.ini](./05/E05.ini) used by **tutorial E05**. The only additional setting is `chr_knock_out_fail_budget = 0.5`. This says that up to **50%** of the *knock-out*, chromatin-to-chromatin interactions *may fail* before a polymer sample is culled and regrown. Please see **tutorial E10** for a detailed description of failure budgets, the same concepts apply in this lesson.

### Knock-in and knock-out failure budgets within same simulation

Note, similar to masking, knock-in and knock-out failure budgets may co-exist in the same simulation. Just use the options `chr_knock_in_fail_budget` and `chr_knock_out_fail_budget` within the same INI file or set via the respective command-line options `--chr_knock_in_fail_budget` and `--chr_knock_out_fail_budget`.

### Run

Now let's run the simulation. In a terminal, enter:

```bash
./11/run.sh
```

### Exercises

*In general, please reset any modified parameters back to the original value before proceeding to the next exercise! If using git, consider commands `git status` and `git checkout -- <file>` to revert local changes.*

1. Run the simulation 5 times using `chr_knock_out_fail_budget = 0.5`, what is the average number of trials needed for the simulation to complete? Now, run the simulation 5 times using `chr_knock_out_fail_budget = 0.0`, what is the average number of trials needed for the simulation to complete?
1. How would you modify the simulation to remove via masking the 2nd (index=1) knock-out interaction? In this case, what is the maximum number of knock-out interactions that may fail when `chr_knock_out_fail_budget = 0.5`?
1. How would you modify **tutorial E09** to support a knock-in failure budget of 50% and a knock-out failure budget of 0%? What is the maximum number of *knock-in* interactions that may fail in this case? What is the maximum number of *knock-out* interactions that may fail in this case?

## Conclusion

Though all lessons in this tutorial were on a single locus, chromatin-to-chromatin proximity interactions may be specified between fragments on **different** loci. Recall that monomers are assigned a unique index according to a tip-to-tail convention (see [tutorial A04](../A/tutorial.md)). Therefore, to create a proximity interaction spanning two loci:

1. When specifying chromatin fragments in the `chr_frag` setting, use the unique set of monomer indices assigned to each locus; it is an error to create a chromatin fragment spanning multiple loci.
1. When specifying the proximity interaction pair in the `chr_knock_in` and/or `chr_knock_out` settings, it is no different from a single locus interaction: each interacting fragment is identified according to the *0-based* order in which they are listed within the `chr_frag` file.

Congratulations on finishing the *chromatin-to-chromatin proximity interaction modeling* tutorial!
