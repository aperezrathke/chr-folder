#!/bin/bash 

# Script bakes model .bun and compiles resulting C++ code

# This script must be called whenever the model .json or .bun are modified

###################################################
# Script paths
###################################################

# Path to this script's directory
TUT_SCRIPT_PATH="$(readlink -f $0)"
TUT_SCRIPT_DIR="$(dirname $TUT_SCRIPT_PATH)"
TUT_SCRIPT_DIR=$(cd "$TUT_SCRIPT_DIR"; pwd)

# Root project directory
ROOT_DIR="$TUT_SCRIPT_DIR/../.."
# Use absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)

# Determine target output directory
# Base output directory
OUTPUT_DIR="$ROOT_DIR/output"
# https://stackoverflow.com/questions/8223170/bash-extracting-last-two-dirs-for-a-pathname
# Determine name of tutorial folder
TUT_NAME="$(basename $TUT_SCRIPT_DIR)"
# Set target output directory
TARGET_OUTPUT_DIR="$OUTPUT_DIR/tut$TUT_NAME"

# Path to .bun file
BUN_PATH="$TUT_SCRIPT_DIR/$TUT_NAME.bun"

# Compilation target
COMPILE="linux-gnu-no-avx"

# Determine path to bake script
BAKE_SCRIPT_PATH="$ROOT_DIR/scripts/Template/bake.py"

# Build command line
cmd="python $BAKE_SCRIPT_PATH --bun $BUN_PATH --out $TARGET_OUTPUT_DIR --compile $COMPILE --force"

# Execute bake script
echo "Running tutorial $TUT_NAME bake:"
echo $cmd
$cmd
echo "Finished tutorial $TUT_NAME bake"
echo "Please inspect output directory: $TARGET_OUTPUT_DIR"
