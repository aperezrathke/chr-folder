#!/bin/bash 

# Script generates samples according to model configuration

# Path to this script's directory
TUT_SCRIPT_PATH="$(readlink -f $0)"
TUT_SCRIPT_DIR="$(dirname $TUT_SCRIPT_PATH)"
TUT_SCRIPT_DIR=$(cd "$TUT_SCRIPT_DIR"; pwd)

# Root project directory
ROOT_DIR="$TUT_SCRIPT_DIR/../../.."
# Use absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)

# Base output directory
OUTPUT_DIR="$ROOT_DIR/output"
# https://stackoverflow.com/questions/8223170/bash-extracting-last-two-dirs-for-a-pathname
# Determine name of sub-tutorial folder
SUBTUT_NAME="$(basename $TUT_SCRIPT_DIR)"
# Determine name of parent tutorial folder
PARTUT_NAME="$(basename $(dirname $TUT_SCRIPT_DIR))"
# Determine parent tutorial folder
OUTPUT_PARTUT_DIR="$OUTPUT_DIR/tut$PARTUT_NAME"

# Set export options
# Note: '-export_intr_chr' exports PML selections for chromatin-to-chromatin interactions
EXPORT_ARG="-export_intr_chr -export_csv -export_pdb -export_pml -export_log_weights -export_extended"

# Set .ini configuration option
KEY="$PARTUT_NAME$SUBTUT_NAME"
INI_PATH="$TUT_SCRIPT_DIR/$KEY.ini"
CONF_ARG="--conf $INI_PATH"

# Determine dispatch option
DISPATCH_ARG="--main_dispatch tut$PARTUT_NAME"_budg

# Determine job prefix - all output sample files will have this prefix. Set to
# random value to avoid overwriting previous data!
# e.g. :
#   job_prefix="$(date +%Y%m%d%H%M%S).$RANDOM"
# will ensure that old data is not overwritten. However, this is a tutorial,
# so we always use the same prefix!
JOB_PREFIX_ARG="--job_id $KEY"

# Determine path to executable binary
EXE_DIR="$OUTPUT_PARTUT_DIR/build/bin/Release_nossecol"
# Executable name
EXE_NAME="u_sac"
EXE_PATH="$EXE_DIR/$EXE_NAME"

# Build command line
cmd="$EXE_PATH $JOB_PREFIX_ARG $DISPATCH_ARG $CONF_ARG $EXPORT_ARG"

# Run command line
echo "Running tutorial $KEY simulation:"
echo $cmd
$cmd
echo "Finished tutorial $KEY simulation"
