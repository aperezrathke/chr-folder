#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script searches for infeasible chromatin-to-chromatin proximity constraints
#
#------------------------------------------------------------------------------
# Arguments
#------------------------------------------------------------------------------
# Script has the following non-optional arguments: (short-hand|long-hand)
#
#   -cmd|--cmd_key: Model command key passed to the 'fea' dispatch utility
#   -cnf|--conf: Path to top-level model INI configuration
#   -i|--intr <path>: Input file with [significant] chromatin-to-chromatin
#       proximity interactions encoded as pairs of 0-based fragment identifiers
#   -o|--out <path>: Output file path to export infeasible chromatin-to-
#       chromatin constraint configurations in CSV. Each row is an infeasible
#       configuration encoded as a boolean 0|1 vector where '0' indicates the
#       i-th constraint is knocked-out and '1' indicates the i-th constraint is
#       knocked-in.
#
# The following arguments are optional: (short-hand|long-hand)
#
#   [-arch|--arch_conf]: Overloads archetype configuration INI path
#   [-clob|--clobber]: If switch is present, any previous corpus of infeasible
#       constraints located at <--out> will be replaced with results of this
#       program run. If switch is not present, the program behavior is to
#       resume generation of the infeasible corpus using the previously
#       generated corpus (if present) and the unique results will simply be
#       appended to the previous file (if present).
#   [-dki|--dist_ki] <+real>: [Optional] Scalar knock-in distance in Angstroms.
#       A knock-in constraint is satisfied if at least one monomer node from
#       each fragment region is within this distance. Ignored if knock-in
#       distance file is present. May be specified as
#       'chr_knock_in_dist = <+real>' within 'conf' INI; the folder binary also
#       has a hard-coded default if not explicitly set by user.
#   [-dkif|--dist_ki_file] <path>: [Optional] File with knock-in distance
#       thresholds (in Angstroms) for each proximity interaction. May be
#       specified as 'chr_knock_in_dist_fpath = <path>" within 'conf' INI.
#   [-dko|--dist_ko] <+real>: [Optional] Scalar knock-out distance in
#       Angstroms. A knock-out constraint is satisfied if all monomer nodes
#       from each fragment region are greater than this distance from each
#       other. Ignored if knock-out distance file is present. May be specified
#       as 'chr_knock_out_dist = <+real>' within 'conf' INI; the folder binary
#       also has a hard-coded default if not explicitly set by user.
#   [-dkof|--dist_ko_file] <path>: [Optional] File with knock-out distance
#       thresholds (in Angstroms) for each proximity interaction. May be
#       specified as 'chr_knock_out_dist_fpath = <path>' within 'conf' INI.
#   [-e|--exe] <path>: [Optional] Path to executable folder binary with 'fea'
#       utility, defaults to <build>/bin/Release (serial executable)
#   [-exc|--exit_corp]: [Optional] Default is to ignore resumed corpus entries
#       which have errors (simply give warning and skip those entries); if
#       this switch is present, then will hard exit if any error encountered
#       in resumed corpus format
#   [-f|--frag] <path>: [Optional] Input file mapping fragment regions (e.g
#       Hi-C bins) to polymer node spans. May be specified as command line
#       argument or as "chr_frag = <path>" tuple within 'conf' INI; if not
#       specified, then will default to treating each monomer as individual
#       fragment.
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-hb|--heartb] <+integer>: Heartbeat interval, program status is reported
#       on intervals defined by this many folding attempts
#   [-kor|--ko_rand]: [Optional] Switch if present will treat '0's as random
#       folding instead of constrained knock-outs
#   [-ma|--max_attempt] <+integer>: Maximum number of attempted configurations,
#       this is same as maximum number of calls to 'fea' utility
#   [-mb|--max_batch] <+integer>: [Optional] Maximum number of constraint
#       configurations to generate prior to fold testing with 'fea'
#       utility. All configurations within this batch size should be unique.
#   [-mc|--max_corp] <+integer>: [Optional] Target maximum infeasible corpus
#       size, may be slightly exceeded depending on batch size resolution.
#       Program terminates when first of:
#           1.) maximum infeasible corpus size is reached -or-
#           2.) maximum number of attempted folds is reached
#   [-mf|--max_fail] <integer>: [Optional] Maximum number of failures allowed
#       for a single job configuration. If a job error code is encountered
#       more than this threshold, the job command line is reported and program
#       terminates
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes to use to work through each configuration batch. Note:
#           <--max_proc> should not exceed CPU logical cores
#   [-mt|--max_trial] <+integer>: [Optional] Number of trials to attempt
#       folding a given proximity constraint configuration before labeling as
#       infeasible
#   [-ssp|--sleep_sec_poll] <+real>: [Optional] Number of seconds to wait
#       before polling active folding simulations for completion
# @TODO:
#   [-fx|--fix] <path>: [Optional] CSV file specifies subset of interactions
#       to always knock-out or knock-in. Each CSV row is formatted:
#           <interaction 0-based index>, <0|1>
#       where the first entry is the 0-based proximity interaction index
#       (according to interactions file), and the second entry is '0' to fix
#       interaction as a knock-out and '1' to fix interaction as a knock-in

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For log() and exhaustive bit vector searches
import math

# For storing boolean interaction configurations
import numpy as np

# For file path testing
import os

# For running external processes
import subprocess

# For exiting if inputs invalid
import sys

# For sleeping until jobs finish
import time

# For keeping track of active jobs
from collections import namedtuple

# For mutable tuples!
# https://pypi.org/project/namedlist/
# https://stackoverflow.com/questions/29290359/existence-of-mutable-named-tuple-in-python
from namedlist import namedlist

# For determining number of CPU cores
from multiprocessing import cpu_count

# For determining complement of bit tuple
from operator import sub

###########################################
# Job descriptor named tuple
###########################################

# Job descriptor
# .ki_bits - Knock-in bit mask tuple
# .proc - Handle to active subprocess
# .cmd - Command line used to (re)launch process
# .num_open - Number of times this job has been created
Job = namedtuple("Job", "ki_bits, proc, cmd, num_open")

###########################################
# Args named list
###########################################

# Argument fields, must mirror result of argparse.ArgumentParser.parse_args()
Args_FIELDS = ["cmd_key", "conf", "intr", "out", "arch_conf", "clobber",
               "dist_ki", "dist_ki_file", "dist_ko", "dist_ko_file", "exe",
               "exit_corp", "frag", "heartb", "ko_rand", "max_attempt",
               "max_batch", "max_corp", "max_fail", "max_proc", "max_trial",
               "sleep_sec_poll"]
Args = namedlist("Args", Args_FIELDS)

###########################################
# Global defaults
###########################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Default path to 'folder' project root directory
DEF_ROOT_DIR = os.path.abspath(os.path.join(SCRIPT_DIR, "..", ".."))

# Default path to folder executable binary containing 'fea' utility, uses the
# serial (non-threaded) binary
DEF_EXE_DIR = os.path.join(DEF_ROOT_DIR, "bin", "Release")
DEF_EXE_PATH = os.path.join(DEF_EXE_DIR, "u_sac")

# Default maximum number of attempted configurations, same as maximum number
# of calls to 'fea' utility
DEF_MAX_ATTEMPT = 50000

# Default batch size, represent maximum number of constraint configurations
# to generate randomly prior to fold testing with 'fea' utility
DEF_MAX_BATCH = 1024

# Default maximum infeasible corpus size, program terminates when either the
# maximum attempts or maximum corpus size is reached
DEF_MAX_CORP = 512

# Default maximum number of trials attempted by 'fea' utility before returning
# a 'no_fold' exit code. For our purposes, if we encounter the 'no_fold' code,
# we label the constraint configuration as potentially infeasible and it is
# added to the output corpus.
DEF_MAX_TRIAL = 50000

# Default maximum processes is number of CPU cores
DEF_MAX_PROC = cpu_count()

# Default number of times a single job may return an abnormal error code. In
# my experience working on public compute clusters (with possibly ancient
# operating systems), background subprocesses may occasionally fail to launch
# and return with a non-zero error code. In this case, we can try to re-launch
# the process a few times to help mitigate this issue. If the subprocess still
# fails with an abnormal error code, then it's likely to be a user
# configuration error causing the folder simulation itself to crash!
DEF_MAX_FAIL = 2

# Default polling interval in seconds to check for active folding simulations
DEF_SLEEP_SEC_POLL = 2

# Status reports occur on intervals defined by this many folding attempts
DEF_HEARTB = 50

###########################################
# Global folder simulation return codes
###########################################

# Folder simulation return code for normal termination (fold okay)
RC_NORMAL = 0

# Folder simulation return code for non-specific error
RC_ERROR = 1

# Folder simulation return code for normal termination without fold generated
RC_NO_FOLD = 2

###########################################
# Global job queue variables
###########################################

# List of currently running jobs
JOB_QUEUE = []

# List of jobs that failed but that should be re-queued just to be sure
JOB_REQUEUE = []

# @HACK - Global job completion counter
NUM_FIN = 0

# @HACK - Global job failure threshold (ideally should be passed to all
#   functions which need it)
MAX_FAIL = DEF_MAX_FAIL

# Sleep seconds to wait before re-queue
# @TODO - make user arg
SLEEP_SEC_FAIL = 5

###########################################
# Error handling and status
###########################################

# Exit program with error message if condition is true
# @param b_yes - If True, program is terminated
# @param msg - [Optional] error message to display prior to exit
# @param code - [Optional] error code to return on exit, default is 1
def exit_if(b_yes, msg="", code=1):
    '''If condition met, prints error message and exits program'''
    if b_yes:
        if msg:
            print "ERROR: " + msg
        print "EXITING (" + str(code) + ")"
        sys.exit(code)

# Report program status
# @param heartb - Report interval
# @param num_attempt - Number of queued folding attempts
# @param num_fin - Number of finished attempts
# @param num_res - Length of resumed infeasible corpus
# @param corpus - Negative (infeasible) corpus to export
def heartbeat(heartb, num_attempt, num_fin, num_res, corp):
    if (heartb != 0) and ((num_attempt % heartb) != 0):
        # Early out, report interval not yet reached
        return
    # Report corpus size vs number of attempts
    freq = 100.0 * float(len(corp) - num_res)/max(float(num_fin), 1.0)
    print "After (" + str(num_attempt) + ", " + str(num_fin) + ")" + \
        " (attempt, finished) folds:" + \
        "\n\t-negative corpus size: " + str(len(corp)) + \
        "\n\t-infeasible frequency: " + str(freq) + "%"
    # Make sure print statement gets logged as soon as possible, without this
    # 'flush' call, the script may wait until its nearly complete before
    # actually printing to stdout (which is not very helpful for monitoring
    # program status)
    sys.stdout.flush()

###########################################
# Bit tools
###########################################

# Generates a random list of bit vectors of length 'n_rows' where each row is
# a tuple containing 'n_cols' random bits. Furthermore, all tuples in the list
# are unique from each other (i.e rows are unique) and from parameter 'diff'
# @param n_cols - each bit vector (tuple) is of this length
# @param n_rows - number of bit vectors (tuples) to generate
# @param diff - set of additional bit vectors (tuples) which generated
#   bit vectors must be disjoint from
# @return list with 'n_rows' unique, random tuples of size 'n_cols' bits
def rand_bit_matrix(n_cols, n_rows, diff):
    '''Generates a random bit matrix of n_rows x n_cols with rows unique'''
    out = set()
    while len(out) < n_rows:
        # @TODO: consider using dtype=uint8 or np.bool - however these types
        # may not be supported until more recent versions of numpy
        tmp = np.random.randint(2, size=(n_rows - len(out), n_cols))
        out = out.union(set(map(tuple, tmp)))
        out = out.difference(diff)
    return list(out)

# Generates exhaustive list of all bit vectors of length 'n_cols', here the
# prefix 'detr' stands for 'deterministic' in contrast to the random version
# of this method.
# @param n_cols - number of bits in each output bit vector tuple
# @param n_rows - unused argument, maintain rand_bit_matrix interface
# @param diff - unused argument, maintain rand_bit_matrix interface
# @return list with all possible 2^n_cols bit vector tuples
def detr_bit_matrix(n_cols, n_rows, diff):
    '''Generates all bit vectors of length n_cols'''
    # Create bit strings from 0 to 2^(n_cols-1)
    out = range(0, int(math.pow(2,n_cols)))
    # Strip '0b' prefix
    out = [bin(i)[2:] for i in out]
    # Append prefix 0's to binary strings and convert to integer tuples
    max_len = len(max(out, key=len))
    out = [tuple(map(int, tuple(i.zfill(max_len)))) for i in out]
    return out

# Updates arguments based on number of proximity interactions found.
# Specifically, will determine if search over bit masks is random or
# (exhaustive) deterministic.
# @param args - user arguments (post check), may be modified to allow an
#   exhaustive parameter search
# @param num_intr - number of proximity interactions
# @return function handle:
#       - rand_bit_matrix if non-exhaustive search
#       - detr_bit_matrix if exhaustive search
def init_bit_search(args, num_intr):
    '''Determines if exhaustive bit mask search is needed'''
    # Maximum bit vector length that can be exhaustively searched by user
    # number of batched fold attempts:
    max_batch_unique = args.max_batch + args.max_corp 
    max_batch_bits = math.log(float(max_batch_unique), 2)
    # Threshold number of interactions at which an exhaustive search will
    # always be triggered:
    # @TODO - Make this user configurable!
    max_exhaust_bits = 16
    # We want to perform an exhaustive search of all bit patterns under the
    # following conditions:
    #   - if num_intr <= max_batch_bits: in this case, we cannot possibly
    #       generate a target batch size of unique tuples since the number of
    #       batch tuples is greater than the number of possible bit states
    #   - if num_intr <= max_exhaust_bits: in this case, we can relatively
    #       easily perform an exhaustive search of the bit space as the
    #       number of bits is not too large
    if (num_intr > max_batch_bits) and (num_intr > max_exhaust_bits):
        # Early out, avoid exhaustive bit vector search
        return rand_bit_matrix
    # Warn if exhaustive search over bit lengths greater than threshold
    if max_batch_bits > max_exhaust_bits:
        print "Warning: target unique configurations (batch + corpus) of " + \
              str(max_batch_unique) + " >= 2^" + str(num_intr) + \
              " possible interaction configurations."
        print "Setting to exhaustive search which may exceed system RAM."
        print "To avoid possible crash, reduce batch + corpus size to be < 2^" + \
              str(num_intr) + "."
    # Configure iteration arguments
    args.max_batch = int(math.pow(2,num_intr))
    args.max_attempt = args.max_batch
    args.max_corp = min(args.max_corp, args.max_batch)
    # Set to deterministic enumeration of bit patterns
    return detr_bit_matrix

###########################################
# File parsing and manipulation
###########################################

# @param path - File path to possibly create directory missing parent
#   directory structure
def create_parent_dirs(path):
    '''Ensures parent directories exist for path'''
    d = os.path.dirname(path)
    if not os.path.exists(d):
        os.makedirs(d)

# Each non-empty, non-comment line in parameter file is considered an
# interaction. Exits program if 0 interactions found!
# @param fpath - File path to proximity interactions file
# @return number of interactions specified in file
def get_num_intr(fpath):
    '''Determines total number of proximity interactions'''
    num_intr = 0
    with open(fpath) as fin:
        for line in fin:
            line = line.strip()
            # Ignore empty (all whitespace) lines
            if line:
                # Ignore comment lines
                if not line.startswith('#'):
                    num_intr += 1
    # Terminate if no interactions found!
    exit_if(num_intr <= 0,
            msg = "0 interactions found in file: " + fpath)
    return num_intr

# Determine if search should resume from any previous corpus
# @param args - User configuration
# @param num_intr - Number of proximity interactions
# @param exit_on_error - Will exit if any format error encountered, else will
#   warn and skip to next row
def init_corp(args, num_intr, exit_on_error=False):
    '''Determines if previous corpus is available and if it should be used'''
    assert num_intr > 0
    corp = set()
    num_skip = 0
    if (not args.clobber) and os.path.isfile(args.out):
        with open(args.out) as fin:
            for line in fin:
                line = line.strip()
                # Ignore empty and comment lines
                if (not line) or line.startswith('#'):
                    continue
                # Parse CSV bit row
                bits = tuple([int(bit.strip()) for bit in line.split(',')])
                # Check expected number of bits read
                if len(bits) != num_intr:
                    msg = "Invalid corpus, expected row with " + \
                        str(num_intr) + " entries but encounted row with " + \
                        str(len(bits)) + " in file: " + args.out
                    exit_if(exit_on_error, msg=msg)
                    print "WARNING: " + msg
                    print "(Skipping)"
                    num_skip = num_skip + 1
                    continue
                # Check all values are in {0,1}
                if not all(((bit == 0) or (bit == 1)) for bit in bits):
                    msg = "Invalid corpus, encountered non 0|1 entry in file: " + \
                        args.out
                    exit_if(exit_on_error, msg=msg)
                    print "WARNING: " + msg
                    print "(Skipping)"
                    num_skip = num_skip + 1
                    continue
                # Append new corpus entry
                corp.add(bits)
        print "Resumed infeasible corpus from file:\n\t" + args.out
        print "Resumed with corpus size: " + str(len(corp))
        print "Skipped misformatted corpus entries: " + str(num_skip)
    return corp

# @param fpath - export file path
# @param corpus - Negative (infeasible) corpus to export
def export(fpath, corp):
    '''Exports infeasible corpus to destination path'''
    create_parent_dirs(fpath)
    # @TODO - add user configurable append option (only append unique tuples)
    with open(fpath, "w") as fout:
        for bit_tup in corp:
            line = ','.join(str(bit) for bit in bit_tup) + '\n'
            fout.write(line) 

###########################################
# Argument tools
###########################################

# Determines C++ bit mask argument
# @param bit_tup - tuple where each element is 1 or 0
# @param cpparg_key - C++ argument key
# @return List with C++ argument (key, val) tuple
def resolve_bit_mask_arg(bit_tup, cpparg_key):
    '''Converts bit tuples into a bit string C++ argument'''
    bit_str = ''.join(map(str, bit_tup))
    return [cpparg_key, bit_str]

# Determines proper C++ distance argument mapping from python arguments
# @param pyarg_scalar_val - Python scalar distance argument value (may be None)
# @param pyarg_fpath_val - Python file path distance argument value (may be
#   None), if not None, this value will take precedence over scalar
# @param cpparg_scalar_key - C++ argument key for scalar distance
# @param cpparg_fpath_key - C++ argument key for file path distance
# @return List (may be None) with C++ distance argument [key, val] tuple
def resolve_dist_arg(pyarg_scalar_val, pyarg_fpath_val, cpparg_scalar_key,
                     cpparg_arg_fpath_key):
    '''Determine C++ distance argument'''
    out = None
    if pyarg_fpath_val:
        # Make sure user specified file exists
        exit_if(not os.path.isfile(pyarg_fpath_val),
                msg = "Unable to find distance file: " + pyarg_fpath_val)
        out = [cpparg_arg_fpath_key, pyarg_fpath_val]
    elif pyarg_scalar_val is not None:
        # Make sure scalar distance is non-negative
        exit_if(float(pyarg_scalar_val) < 0,
                msg = "Scalar distance must be non-negative (" + \
                        str(pyarg_scalar_val) + ")")
        out = [cpparg_scalar_key, str(pyarg_scalar_val)]
    return out

# @param args - user arguments
# @return Portion of command line folder call invariant to proximity
#   interaction bit masks
def resolve_invar_args(args):
    '''Determines invariant arguments to C++ folder utility'''
    # Exe arg
    cppargs = [args.exe]
    # Dispatch arg
    cppargs.extend(["--fea_dispatch", args.cmd_key])
    # Conf arg
    cppargs.extend(["--conf", args.conf])
    # Max trials arg
    cppargs.extend(["--max_trials", str(args.max_trial)])
    # Force top-level ensemble size of 1
    cppargs.extend(["--ensemble_size", "1"])
    # Note - since ensemble size is 1, we really should only be using a single
    #   thread! Has no effect if using a serial executable
    cppargs.extend(["--num_worker_threads", "1"])
    # Archetype conf arg
    if args.arch_conf:
        cppargs.extend(["--arch", args.arch_conf])
    # Frag arg
    if args.frag:
        cppargs.extend(["--chr_frag", args.frag])
    # Knock-in arg
    cppargs.extend(["--chr_knock_in", args.intr])
    # Knock-out arg
    cppargs.extend(["--chr_knock_out", args.intr])
    # Knock-in distance(s) arg
    cpparg_dki = resolve_dist_arg(args.dist_ki, args.dist_ki_file,
                                  "--chr_knock_in_dist", "--chr_knock_in_dist_fpath")
    if cpparg_dki:
        cppargs.extend(cpparg_dki)
    # Knock-out distance(s) arg
    cpparg_dko = resolve_dist_arg(args.dist_ko, args.dist_ko_file,
                                  "--chr_knock_out_dist", "--chr_knock_out_dist_fpath")
    if cpparg_dko:
        cppargs.extend(cpparg_dko)
    # List of invariant arguments
    return cppargs

# Performs cursory argument validation, by no means exhaustive.
# @param args - user arguments
# @return Arguments, some which may have been modified during validation
def check_args(args):
    '''Performs argument validation'''
    # Make sure numeric arguments are properly typed
    assert isinstance(args.clobber, bool)
    assert isinstance(args.exit_corp, bool)
    assert isinstance(args.ko_rand, bool)
    args.max_attempt = int(args.max_attempt)
    args.max_batch = int(args.max_batch)
    args.max_corp = int(args.max_corp)
    args.max_trial = int(args.max_trial)
    args.max_proc = int(args.max_proc)
    args.max_fail = int(args.max_fail)
    args.sleep_sec_poll = float(args.sleep_sec_poll)
    args.heartb = int(args.heartb)
    # Check configuration file exists
    exit_if(not os.path.isfile(args.conf),
            msg = "Invalid configuration file path (--conf): " + str(args.conf))
    # Check proximity interactions file exists
    exit_if(not os.path.isfile(args.intr),
            msg = "Invalid proximity interactions file path (--intr): " + str(args.intr))
    # Check executable path exists
    exit_if(not os.path.isfile(args.exe),
            msg = "Invalid executable path (--exe): " + str(args.exe))
    # Check positive number of fold attempts
    exit_if(args.max_attempt <= 0,
            msg = "Invalid maximum number of fold attempts (--max_attempt), must be > 0")
    # Check positive batch size (if < 0, then error out as likely other things
    # wrong with script)
    exit_if(args.max_batch <= 0,
            msg = "Invalid batch fold size (--max_batch), must be > 0")
    # Constrain 1 <= batch size to <= fold attempts; in this case, we are more
    # forgiving and just warn the user
    if args.max_batch > args.max_attempt:
        print "Warning: setting batch fold to " + str(args.max_attempt) + \
            ", must be <= --max_attempt"
        args.max_batch = args.max_attempt
    # Check positive corpus size
    exit_if(args.max_corp <= 0,
            msg = "Invalid corpus size (--max_corp), must be > 0")
    # Warn user if corpus size > number of fold attempts
    if args.max_corp > args.max_attempt:
        print "Warning: setting maximum corpus size to " + \
            str(args.max_attempt) + ", must be <= --max_attempt"
        args.max_corp = args.max_attempt
    # Check positive trial count
    exit_if(args.max_trial <= 0,
            msg = "Invalid number of trials per fold attempt (--max_trial), must be > 0")
    # Warn user if 0 or less CPU cores allotted, set to 1
    if args.max_proc <= 0:
        args.max_proc = 1
        print "Warning: allotted parallel processes <= 0, setting to " + str(args.max_proc)
    # Warn user if PROC > CPU CORES
    num_cpu = cpu_count()
    if args.max_proc > num_cpu:
        print "Warning: MAX_PROC (" + args.max_proc + ") > CPU CORES (" + num_cpu + ")"
    # Warn user if non-positive heartbeat, set to default
    if args.heartb <= 0:
        args.heartb = DEF_HEARTB
        print "Warning: non-positive heartbeat detected (--heartb), setting to default: " \
                + str(DEF_HEARTB)
    # Warn if --clobber is present
    if args.clobber:
        print "Warning, --clobber switch detected, any previous infeasible corpus will be overwritten!"
    # Note, this return is not really necessary as args is passed by reference
    return args

###########################################
# Job utilities
###########################################

# Checks return code is not abnormal and updates negative corpus if no folds
# were generated
# @param job - Completed job tuple
# @param corp - Corpus of infeasible constraints, updated if return code is
#   'no_fold'
def handle_rc(job, corp):
    '''Handle folding simulation return codes'''
    global MAX_FAIL
    global NUM_FIN
    # Update number of finished jobs
    NUM_FIN = NUM_FIN + 1
    # Check return code
    rc = job.proc.returncode
    # Did job terminate abnormally?
    if rc == RC_ERROR:
        # If re-try threshold exceeded, force exit
        exit_if(job.num_open > MAX_FAIL,
                msg = "Folding simulation abnormal termination:\n\t" + \
                        str(job.cmd))
        # Else mark job for re-queue
        JOB_REQUEUE.append(job)
    # Job completed okay, check if unable to fold
    elif rc == RC_NO_FOLD:
        print "NO FOLD DETECTED:\n\t" + str(job.cmd)
        # No folds were generated, update infeasible corpus
        corp.add(job.ki_bits)

# @param job - Job named tuple
# @param corp - Negative corpus set of infeasible constraint configurations,
#   updated if simulation return code is 'no_fold'
# @return True if job completed, False otherwise
def poll_job(job, corp):
    '''Checks if job is complete, updates corpus'''
    if job.proc.poll() is None:
        # Job not yet complete
        return False
    # Check return code
    handle_rc(job=job, corp=corp)
    # Job completed!
    return True

# @param cmd - Job command line
# @param fnull - File handle to 'dev/null' for redirecting stdout/stderr
#   of child sub-processes
def spawn_job(cmd, fnull):
    '''Spawns new process and returns handle'''
    # Should we wait before each job spawn to help ensure diff rand seeds?
    # Open new job handle (non-blocking)
    # https://stackoverflow.com/questions/11269575/how-to-hide-output-of-subprocess-in-python-2-7
    return subprocess.Popen(cmd, stdout=fnull, stderr=subprocess.STDOUT,
                            shell=False)

# Respawns any jobs that failed and need to be re-tried
# @param fnull - File handle to 'dev/null' for redirecting stdout/stderr
#   of child sub-processes
def requeue_jobs(fnull):
    '''Requeues any jobs that need to be tried again'''
    global JOB_QUEUE
    global JOB_REQUEUE
    global SLEEP_SEC_FAIL
    if len(JOB_REQUEUE) > 0:
        # Wait a bit before re-queueing
        time.sleep(SLEEP_SEC_FAIL)
        for jr in JOB_REQUEUE:
            # Create job process and append to queue
            proc = spawn_job(cmd=jr.cmd, fnull=fnull)
            job = Job(ki_bits=jr.ki_bits, proc=proc, cmd=jr.cmd,
                      num_open=jr.num_open + 1)
            JOB_QUEUE.append(job)
        # Clear requeue
        JOB_REQUEUE = []

# @param cppcall_invar - Invariant arguments to fold simulation
# @param ki_bits - Tuple of knock-in bits
# @param ko_bits - Tuple of knock-out bits
# @param corp - Output corpus for recording any ki_bits that resulted in
#   'no_fold' return code
# @param max_corp - Target maximum corpus size
# @param max_proc - Maximum number of active folding simulations
# @param sleep_sec_poll - Seconds to wait before polling active jobs
# @param fnull - File handle to 'dev/null' for redirecting stdout/stderr
#   of child sub-processes
# @return 1 if job was queued, 0 o/w
def queue_job(cppcall_invar, ki_bits, ko_bits, corp, max_corp, max_proc,
              sleep_sec_poll, fnull):
    '''Queues folding simulation, polls until procs available'''
    global JOB_QUEUE
    # Check if job queue is full
    while (len(JOB_QUEUE) >= max_proc):
        # Wait a bit before checking if jobs have completed
        time.sleep(sleep_sec_poll)
        # Check active jobs for completion
        JOB_QUEUE[:] = [job for job in JOB_QUEUE if not poll_job(job, corp)]
        # Re-try any failed jobs (up to limit)
        requeue_jobs(fnull=fnull)
    # Job is unnecessary if target corpus size reached
    if len(corp) >= max_corp:
        # Signal no job was queued
        return 0
    # Resolve bit mask arguments
    cpparg_ki_bits = resolve_bit_mask_arg(ki_bits, "--chr_knock_in_bit_mask")
    cpparg_ko_bits = resolve_bit_mask_arg(ko_bits, "--chr_knock_out_bit_mask")
    # Shallow copy invariant folder arguments
    cppcall = cppcall_invar[:]
    # Finalize folder command line
    cppcall.extend(cpparg_ki_bits)
    cppcall.extend(cpparg_ko_bits)
    # Create job process and append to queue
    proc = spawn_job(cmd=cppcall, fnull=fnull)
    job = Job(ki_bits=ki_bits, proc=proc, cmd=cppcall, num_open=1)
    JOB_QUEUE.append(job)
    # Signal job was queued
    return 1

# @param corp - Corpus of infeasible constraint configurations, updated if any
#   outstanding folding simulations return with code 'no_fold'
# @param fnull - File handle to 'dev/null' for redirecting stdout/stderr
#   of child sub-processes
def sync_jobs(corp, fnull):
    '''Wait for all outstanding folding simulations to complete'''
    global JOB_QUEUE
    while len(JOB_QUEUE) > 0:
        for job in JOB_QUEUE:
            job.proc.wait();
            # Check return code
            handle_rc(job=job, corp=corp)
        # Clear queue
        JOB_QUEUE = []
        # Re-try any failed jobs (up to limit)
        requeue_jobs(fnull=fnull)

###########################################
# Infea
###########################################

# @param args - Args namedlist or duck-typed argparse result
def infea(args):
    '''Searches for infeasible proximity constraint configurations'''
    # Validate user arguments
    args = check_args(args)
    # @HACK - update job failure threshold
    global MAX_FAIL
    MAX_FAIL = args.max_fail
    # Initialize bit search
    num_intr = get_num_intr(args.intr)
    get_batch = init_bit_search(args=args, num_intr=num_intr)
    # Resolve invariant C++ arguments
    cppcall_invar = resolve_invar_args(args)
    # Tuple of (1, 1 ...) used to determine complement knock-out mask
    ones = tuple(np.ones((num_intr,), dtype=int))
    # Output negative corpus of infeasible constraint configurations
    corp = init_corp(args=args, num_intr=num_intr,
                     exit_on_error=args.exit_corp)
    num_resume = len(corp)

    # Extract additional arguments
    max_attempt = int(args.max_attempt)
    max_batch = int(args.max_batch)
    max_corp = int(args.max_corp)
    max_proc = int(args.max_proc)
    sleep_sec_poll = float(args.sleep_sec_poll)
    heartb = int(args.heartb)
    # Initialize knock-out bits as Tuple of (0, 0 ...)
    ko_bits = tuple(int(0) for i in xrange(num_intr))

    # Redirect stdout/stderr of child sub-processes
    fnull = open(os.devnull, 'w')
    # Attempt random folds and collect infeasible configurations
    num_attempt = 0
    while ((num_attempt < max_attempt) and (len(corp) < max_corp)):
        num_batch = min(max_attempt - num_attempt, max_batch)
        # Generate batch of bit configuration masks
        batch = get_batch(n_cols=num_intr, n_rows=num_batch, diff=corp)
        # Process each generated bit mask
        for ki_bits in batch:
            # Determine complement knock-out bits only if non-random
            if not args.ko_rand:
                # Note, according to timeit profiling, map(sub, ...) operation was
                # slightly faster than np.subtract(...) for subtracting tuples
                # See https://stackoverflow.com/questions/17418108/elegant-way-to-perform-tuple-arithmetic
                ko_bits = tuple(map(sub, ones, ki_bits))
            # Run folding simulation
            num_attempt += queue_job(cppcall_invar=cppcall_invar,
                                     ki_bits=ki_bits, ko_bits=ko_bits,
                                     corp=corp, max_corp=max_corp,
                                     max_proc=max_proc,
                                     sleep_sec_poll=sleep_sec_poll,
                                     fnull=fnull)
            # Report program status
            heartbeat(heartb, num_attempt, NUM_FIN, num_resume, corp)
            # If maximum corpus size reached, break loop
            if len(corp) >= max_corp:
                break
        # If maximum corpus size reached, break loop
        if len(corp) >= max_corp:
            break

    # Wait for outstanding jobs to finish
    print "Waiting for any outstanding folding simulations to finish"
    sync_jobs(corp=corp, fnull=fnull)
    fnull.close()
    # Report corpus size vs number of attempts
    heartbeat(num_attempt, num_attempt, NUM_FIN, num_resume, corp)
    # Export corpus
    print "Exporting negative corpus to:\n\t" + args.out
    export(args.out, corp)

###########################################
# Main
###########################################

# @param args - parsed argparse object
def print_cmd(args):
    '''Prints command-line to stdout'''
    for arg in vars(args):
        print "[--" + arg + "]"
        print str(getattr(args, arg))

# Main script entry point
def __main__():
    print "======================= infea ======================="
    logo = r"""
 _        __           
(_)      / _|          
 _ _ __ | |_ ___  __ _ 
| | '_ \|  _/ _ \/ _` |
| | | | | ||  __/ (_| |
|_|_| |_|_| \___|\__,_|
"""
    print logo
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Required arguments
    # https://stackoverflow.com/questions/24180527/argparse-required-arguments-listed-under-optional-arguments
    req_args = parser.add_argument_group('required named arguments')
    req_args.add_argument('-cmd', '--cmd_key', required=True,
                          help='Model command key passed to the folding simulation "fea" utility')
    req_args.add_argument('-cnf', '--conf', required=True,
                          help='Model INI configuration file path passed to the "fea" utility')                      
    req_args.add_argument('-i', '--intr', required=True,
                          help='Input file with [significant] chromatin-to-chromatin proximity interactions encoded as pairs of 0-based fragment identifiers')
    req_args.add_argument('-o', '--out', required=True,
                          help='Output file path to export infeasible chromatin-to-chromatin constraint configurations in CSV format, each row is 0|1 vector of infeasible configurations')
    # Optional arguments
    parser.add_argument('-arch', '--arch_conf', default=None,
                        help='Model INI archetype configuration file path passed to the "fea" utility')
    parser.add_argument('-clob', '--clobber', action='store_true',
                        help='If present, any previous infeasible corpus will be overwritten with new data, default (switch not present) is to resume using any previous corpus located at <--out>') 
    parser.add_argument('-dki', '--dist_ki', default=None,
                        help='Scalar knock-in distance in Angstroms, ignored if knock-in distance file is present')
    parser.add_argument('-dkif', '--dist_ki_file', default=None,
                        help='File with knock-in distance thresholds (in Angstroms) for each proximity interaction')
    parser.add_argument('-dko', '--dist_ko', default=None,
                        help='Scalar knock-out distance in Angstroms, ignored if knock-out distance file is present')
    parser.add_argument('-dkof', '--dist_ko_file', default=None,
                        help='File with knock-out distance thresholds (in Angstroms) for each proximity interaction')
    parser.add_argument('-e', '--exe', default=DEF_EXE_PATH,
                          help='Path to folding simulation executable binary with "fea" utility')
    parser.add_argument('-exc', '--exit_corp', action='store_true',
                        help='If present, will hard exit if any format error encountered in a resumed corpus (default is to warn and skip any misformatted rows)')
    parser.add_argument('-f', '--frag', default=None,
                        help='Input file mapping fragment regions (e.g Hi-C bins) to polymer node spans, must be specified as command-line argument or within "conf" INI')
    parser.add_argument('-hb', '--heartb', default=DEF_HEARTB,
                        help='Heartbeat interval, program status is reported on intervals defined by this many folding attempts')
    parser.add_argument('-kor', '--ko_rand', action='store_true',
                        help="Switch, if present, will treat '0's as random folding instead of constrained knock-outs")
    parser.add_argument('-ma', '--max_attempt', default=DEF_MAX_ATTEMPT,
                        help='Maximum number of attempted configurations, this is same as maximum number of calls to folding simulation tool')
    parser.add_argument('-mb', '--max_batch', default=DEF_MAX_BATCH,
                        help='Maximum number of constraint configurations to generate prior to fold testing')
    parser.add_argument('-mc', '--max_corp', default=DEF_MAX_CORP,
                        help='Target maximum infeasible corpus size (may be exceeded slightly based on batch size), program terminates when either max attempts or max corpus size is reached')
    parser.add_argument('-mf', '--max_fail', default=DEF_MAX_FAIL,
                        help='Maximum number of times a single job configuration may fail with error return code')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of active fold simulations')
    parser.add_argument('-mt', '--max_trial', default=DEF_MAX_TRIAL,
                        help='Maximum number of trials per fold attempt of a given constraint configuration before labeling as infeasible')
    parser.add_argument('-ssp', '--sleep_sec_poll', default=DEF_SLEEP_SEC_POLL,
                        help='Polling interval in seconds to check for completed active folding simulations')
    # @TODO - [-fx|--fix]
    # Parse command line
    args = parser.parse_args()
    print_cmd(args)
    # Find infeasible proximity constraint configurations!
    infea(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
