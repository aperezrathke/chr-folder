#!/bin/bash 

# Runs benchmark commandlet several times.
#
# Suggested workflow:
#
#   1.) Run benchmark and save log with timings
#           ./benchmark.sh > log.pre.txt
#
#   2.) Modify code and recompile
#
#   3.) Re-run benchmark to verify code modifications and compare with logs
#       from #1 to verify improved performance.
#

# Number of times to run benchmark (for later averaging, etc)
N_TRIALS=5

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
# Project root directory
ROOT_DIR="$SCRIPT_DIR/../.."
# Obtain absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)
# Target binary directory
BIN_DIR="$ROOT_DIR/bin"
# Executable dir
EXE_DIR="$BIN_DIR/Release_benchmark_commandlets"
# Executable name
EXE_NAME="u_sac"

# Switch to executable dir
pushd ./
cd $EXE_DIR

echo "================================================"
echo "BENCHMARK BEGIN"
echo "================================================"
echo ""

START_TIME=`date +%s`

for (( i = 0 ; i < $N_TRIALS ; i += 1 )); do
    echo "*************BENCH TRIAL**********************"
    ./$EXE_NAME -benchmark
done

END_TIME=`date +%s`
TOTAL_TIME=$((END_TIME - START_TIME))
MEAN_TIME=$( echo "$TOTAL_TIME / $N_TRIALS" | bc -l )

echo ""
echo "================================================"
echo "BENCHMARK END:"
echo "    -total time (s): $TOTAL_TIME"
echo "    -mean time (s): $MEAN_TIME"
echo "================================================"

popd
