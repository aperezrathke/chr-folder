#!/bin/bash 

# Script runs various landmark trial runner tests
# Example usage:
#
#   nohup <path_to_script> &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background

###################################################
# Script paths
###################################################

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
# Root project directory
ROOT_DIR=$SCRIPT_DIR/../..
# Executable path
EXE_DIR="$ROOT_DIR/bin/Release_threaded_tests"
EXE_NAME=u_sac
EXE_PATH="$EXE_DIR/$EXE_NAME"

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# Run tests
###################################################

echo "$(timestamp): Running landmark rc test"
$EXE_PATH -test_sis_lmrk_rc

echo "$(timestamp): Running landmark rs test"
$EXE_PATH -test_sis_lmrk_rs

echo "$(timestamp): Running landmark rs rc test"
$EXE_PATH -test_sis_lmrk_rs_rc

echo "$(timestamp): Running landmark rs rs test"
$EXE_PATH -test_sis_lmrk_rs_rs

echo "$(timestamp): Finished landmark tests"
