%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script scales a nuclear volume to maintain
% a constant chromatin density
%
% @param: nuclear_volume - volume of cell nucleus
% @param: total_genome_length - total genomic length of DNA within organism
% @param: total_loci_length - genomic length of loci of interest
% @param: monomer_length - genomic length of a monomer unit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference genome sizes go here
% Prefix GL_ = Genome Length
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the units for genome length
GL_UNITS = 'base pairs';

% Mouse - from EBI:
% http://www.ebi.ac.uk/ena/data/view/GCA_000001635.5
GL_MUS_MUSCULUS_HAPLOID = 2800055571;
GL_MUS_MUSCULUS_DIPLOID = 2 * GL_MUS_MUSCULUS_HAPLOID;

% Human (GRCh37.p13 or hg19) - from EBI
% http://www.ebi.ac.uk/ena/data/view/GCA_000001405.14
GL_HOMO_SAPIENS_HAPLOID_HG19 = 3234834689
GL_HOMO_SAPIENS_DIPLOID_HG19 = 2 * GL_HOMO_SAPIENS_HAPLOID_HG19

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference nuclear volumes go here
% Prefix NV_ = Nuclear Volume
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set units for diameter
DIAMETER_UNITS = 'microns';

% Set the units for nuclear volume
NV_UNITS = strcat(DIAMETER_UNITS,'^3');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mESC

% Mouse embryonic stem cell in 2i/LIF
% Source: Pagliara et al, 'Auxetic nuclei in embryonic stem cells
% exiting pluripotency', Nature Methods, 2014
% Measured single cell using confocal microscopy and modeled as ellipsoid
NV_MUS_MUSCULUS_ESC_2LI_LIF = 857;

% Documenting nuclear diameter used for modeling mESC (3.5 um) in:
% Chiariello, Andrea M., et al. "Polymer physics of chromosome large-scale
% 3D organisation." Scientific Reports 6 (2016).
NV_MUS_MUSCULUS_ESC_CHIARIELLO = (1/6)*pi*((3.5)^3)

% mESC nuclear volume based on Genome Architecture Mapping (GAMS) which
% gives an average mESC nuclear radius of 4.5 micrometers (um), see
% supplement page 5: Beagrie, Robert A., Antonio Scialdone, Markus
% Schueler, Dorothee CA Kraemer, Mita Chotalia, Sheila Q. Xie, Mariano
% Barbieri et al. "Complex multi-enhancer contacts captured by genome
% architecture mapping." Nature 543, no. 7646 (2017): 519-524.
NV_MUS_MUSCULUS_ESC_BEAGRIE = (4/3)*pi*((4.5)^3)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% K562 and/or CML

% K562 is a CML cell line where the cells resemble both undifferentiated
% granulocytes and erythrocytes.
% https://en.wikipedia.org/wiki/K562_cells 

% Smetana, K., et al. "A short note on the nuclear diameter in human early
% granulocytic progenitors." Hematology 11.5-6 (2006): 399-401.
% The reported diameters are in the 11-13 micron range. Therefore, the
% average is approximately a 12 micron diameter. This study did not
% explicitly use the K562 cell line but the cells measured are progenitor
% cells from the bone marrow of chronic CML patients.
NV_CML_GRANULOCYTE_PROGENITORS_SMETANA = (1/6)*pi*(12^3)

% K562 nuclear diameters reported in:
% Smetana, K., et al. "A karyometric study on ageing and butyrate or
% imatinib treated human leukemic myeloblasts represented by K562 cells
% originated from chronic myeloid leukaemia." Neoplasma 54.5 (2006):
% 359-364.
% Am averaging reported diameters of 14.6 and 13.6 microns
NV_K562_GRANULOCYTE_PROGENITORS_SMETANA = (1/6)*pi*((0.5*(14.6+13.6))^3)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Misc mammalian from Sanborn et al

% From Lieberman-Aiden lab paper:
% Sanborn, Adrian L., et al. "Chromatin extrusion explains key features of
% loop and domain formation in wild-type and engineered genomes."
% Proceedings of the National Academy of Sciences 112.47 (2015):
% E6456-E6465.

% GM12878 237 +- 84 cubic microns
% B-cell lymphocyte/lymphoblast
NV_GM12878_SANBORN = 237

% IMR90 381 +- 157 cubic microns
% Fetal lung myofibrolasts
NV_IMR90_SANBORN = 381

% NHEK 440 +- 90 cubic microns
% Normal human epidermal keratinocytes
NV_NHEK_SANBORN = 440

% HMEC 728 +- 307 cubic microns
% Human mammary epithelial cells
NV_HMEC_SANBORN = 728

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference loci lengths go here
% Previx LL_ = Loci Length 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the units for loci length
LL_UNITS = 'base pairs';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From:
% Phillips-Cremins et al, Cell 2013,
% "Architectural protein subclasses shape 3-D
%  organization of genomes during lineage commitment"
%
% Data is from Supplementary Table 1 found on
% page 9 of Supplementary Materials (pdf)

% Oct4 5C data, 2.1 Mb
LL_OCT4_5C_PHILLIPS_CREMINS_CELL_2013 = 2.1 * (10^6);

% Olig1-Olig2, 1.15 Mb
LL_OLIG1_OLIG2_5C_PHILLIPS_CREMINS_CELL_2013 = 1.15 * (10^6);

% Sox2, 1.0 Mb
LL_SOX2_5C_PHILLIPS_CREMINS_CELL_2013 = 1.0 * (10^6);

% Klf4, 1.0 Mb
LL_KLF4_5C_PHILLIPS_CREMINS_CELL_2013 = 1.0 * (10^6);

% Nestin, 1.1 Mb
LL_NESTIN_5C_PHILLIPS_CREMINS_CELL_2013 = 1.1 * (10^6);

% Nanog, 1.15 Mb
LL_NANOG_5C_PHILLIPS_CREMINS_CELL_2013 = 1.15 * (10^6);

% Nanog, 0.56 Mb
LL_GENE_DESERT_5C_PHILLIPS_CREMINS_CELL_2013 = 0.56 * (10^6);

% Super enhancer region, ~80 kb. Details from Valentina:
%   chr17, 40395298:40475950, score=38195.68411, STAT5A|STAT3|STAT5B,
%   length=80652bp
% The contact domain (arrowhead) from the Hi-C paper:
%   Rao, Suhas SP, et al. "A 3D map of the human genome at kilobase
%   resolution reveals principles of chromatin looping." Cell 159.7
%   (2014): 1665-1680.
% containing this region spans from 40690000 - 40360000 = ~330 KB
% (The K562 5KB resolution Hi-C data is available from here:
%   https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE63525)
% Note: technically it's +1, but that adds a whole monomer just for that
%   1 bp, so shaving last bp off.
LL_SE_CHR17_40395298_40475950_VALENTINA = 40690000 - 40360000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference monomer lengths go here
% Monomers are the atomic units used for modeling the chromatin fiber
% They represent the highest level of resolution
% Prefix ML_ = Monomer Length
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ML_UNITS = 'base pairs';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From:
% Xu, Gursoy et al,
% "Three-dimensional Modeling of Chromsome Conformation Capture Data
%  Characterizes the Differential Gene Activation in Diffent Cell Lines"
%
% Data is from Supplementary Information,
% section 4.2.1 'Physical properties of chromatin fiber' which states
% a 30 nm chromatin fiber has a mass density of 11 nm/kb. This value is
% from Dekker, 'Mapping in vivo chromatin interactions in yeast suggests an
% extended chromatin fiber with regional variation in compaction'.
% This equates to approximately 2727 base pairs.
% Assuming 30-nm monomer modeled as a sphere with mass density 11 nm/kb
ML_YEAST_30_NM_DEKKER = 2727;

% Value given by Gamze (from talking with biologists, might have citations)
ML_YEAST_30_NM_GAMZE = 3000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From:
% "Walhout, Marian, Marc Vidal, and Job Dekker, eds. Handbook of systems
% biology: concepts and insights. Academic Press, 2012."
% Chapter 7 - The Spatial Architecture of Chromosomes, pg 140
% which states beads-on-string chromatin fiber has a mass density of ~15 bp
% per nm which is equivalent to ~165 bp per 11 nm length.
% This is reasonable assuming the following:
%   - Nucleosome (histone octamer) is typically modeled as 10 nm diameter
%     disk that is approx 5-6 nm long. There are 146 +/- 1 bp wrapped
%     around histone core. If H1 is present (lets assume that for stem
%     cells H1 is generally not present), then an additional ~20 bp is also
%     attached to the nucleosome (giving ~166 bp per nucleosome)
%   - With the additional wrapped DNA the nucleosome disk diameter is
%     approximately 11 nm
%   - In addition there is linker DNA, typically B-form such that the
%     nucleotide repeat length is ~200 bp for mammalian cells - see
%     [Van Holde, Kensal E. "Chromatin." Springer series in molecular
%     biology.] for some of these numbers and dimensions. The linker DNA is
%     assumed to be unfolded which means 1 bp is 2 nm wide with linear
%     length ~0.34 nm (textbook values for B-form DNA based on crystal
%     structures given by R. Franklin, Watson & Crick). So linker DNA is
%     approx 2-nm fiber with 1 bp per 0.34 nm mass density.
%   - Hence, a compact length of 11 nm fiber with two nucleosomes directly
%     adjancent to each other:
%
%       /----5.5 nm----\  /----5.5 nm----\ 
%       | Nucleosome   |  | Nucleosome   |
%       |              |  |              |
%      11 nm           | 11 nm           |
%       |     A        |  |     A        |
%       |              |  |              |
%       \----5.5 nm----/  \----5.5 nm----/
%
%     would have a linear mass density of ~30 bp/nm = 300 bp / 11 nm
%   - Therefore, this value is somewhere between stacked linear segments
%     of nucleosomes and (nucleosomes + unfolded linker DNA).
%   - Furthermore, a homeogeneous sphere (rather than cylindrical disks)
%     are reasonable for approximating the underlying heterogenous
%     fiber which alternates crudely between 11 nm disks and 2 nm disks.
%
% UPDATE - April 4th, 2018 - THIS FIBER IS MORE DENSE THAN 30 NM FIBER!
% ACCORDING TO FOLLOWING:
%
%   BP_30 = 2727 bp
%   VOL_30 = SphereVol(diam=30 nm) = (1/6)*pi*(30^3) = 1.4137E04 nm^3
%   DENSITY_30 = BP_30 / VOL_30 = 0.1929 bp/nm^3
%
%   BP_11 = 165 bp
%   VOL_11 = SphereVol(diam=30 nm) = (1/6)*pi*(11^3) = 696.9100 nm^3
%   DENSITY_11 = BP_11 / VOL_11 = 0.2368 bp/nm^3
%
%   => DENSITY_30 < DENSITY_11
%   IT COULD ALSO BE THAT THERE IS TOO MUCH ERROR IN THE 30 NM FIBER
%   DENSITY AND THAT THE 30 NM FIBER IS INFLATING EFFECT OF EXCLUDED
%   VOLUME.
%
ML_11_NM_DEKKER = 165

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Alternate 11 nm model
%   Rippe, Karsten, ed. Genome organization and function in the cell nucleus.
%   John Wiley & Sons, 2012 (pg. 424) 
%
% 0.5 nucleosomes are present per linear 11 nm of the beads on string chain
%
% This number is in range of the recent experimental work in
%   Ricci, Maria Aurelia, et al. "Chromatin fibers are formed by heterogeneous
%   groups of nucleosomes in vivo." Cell 160.6 (2015): 1145-1158.
% which in Figure 3E,F gives nucleosome densities in stem cells "clutches"
% that are in this ballpark. They also agree with notion that nucleosome
% fluorescence signals are dimmer for "ground state" cells implying that
% there are more nucleosome depleted regions.
%
% More and more I am of the opinion that stem cells are best modeled as
% unfolded chromatin with relatively low nucleosomal density. For a
% homogeneous model, then a low nucleosome density makes sense. However,
% this may inflate the effect of excluded volume. Therefore, an alternative
% semi-isotropic model which represents nucleosomes (without H1) as
% 11 nm, 146 bp spheres followed by approx. five 3.4 nm, 10 bp spheres for
% the linker DNA. This heterogeneous model is currently not implemented in
% this script but is documented here for possible future use.
NUCLEOSOMES_PER_11_NM = 0.5

% Assume nucleosome repeat length (NRL) of ~200 bp 
% 1. Van Holde, Kensal E. "Chromatin." Springer series in molecular
%   biology.
% 2. Maeshima, Kazuhiro, et al. "Liquid-like behavior of chromatin." Current
%   opinion in genetics & development 37 (2016): 36-45.
% 3. Rippe, Karsten, ed. Genome organization and function in the cell nucleus.
%   John Wiley & Sons, 2012 (pg. 418) 
% Note, (NRL) = nucleosome (~147 bp) + linker DNA. Typically if H1 linker
% histone is present an additional ~20 bp is associated with the
% nucleosome.
% From Rippe text:
% The nucleosome consists of 145 to 147 bp of DNA wrapped around a histone
% octamer protein core. It has a cylindrical shape of 11 nm diameter and
% 5.5 nm height. The nucleosome repeat length (NRL)varies between 165 and
% 220 bp of DNA depending on the species and also on the cell type within
% a given organism. It amounts to about 200 bp in mammals.
NUCLEOSOME_REPEAT_LENGTH = 200

% This estimate is based on the nucleosome density given in Rippe text. If
% we multply the nucleosome density by a nucleosome repeat length, we can
% estimate the monomer length in base pairs for an 11 nm monomer sphere.
ML_11_NM_RIPPE = NUCLEOSOMES_PER_11_NM * NUCLEOSOME_REPEAT_LENGTH

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Monomer fiber diameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MF_UNITS = 'angstroms'

% 11 nm fiber
MF_11_NM = 110

% 30 nm fiber (never observed in vivo)
MF_30_NM = 300

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plug in parameters here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Nuclear volume
nuclear_volume = NV_K562_GRANULOCYTE_PROGENITORS_SMETANA;

% Total genomic length of DNA within organism
total_genome_length = GL_HOMO_SAPIENS_DIPLOID_HG19;

% Genomic length of loci
loci_lengths = [LL_SE_CHR17_40395298_40475950_VALENTINA];
total_loci_length = sum(loci_lengths);

% Genomic length of monomer
monomer_length = ML_11_NM_RIPPE;

% Monomer fiber diameter
monomer_fiber_diameter = MF_11_NM;

% Target base pair density per monomer
% Assumes same units as ML_UNITS!
target_monomer_length = 660;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

assert( (nuclear_volume > 0), ...
    'Error: nuclear volume must be positive');
assert( (total_genome_length > 0), ...
    'Error: total genome length must be positive');
assert( (total_genome_length > 0), ...
    'Error: total loci length must be positive');
assert( (total_loci_length <= total_genome_length), ...
    'Error: total loci length must be less than or equal to total genome length');
assert( (monomer_length > 0), ...
    'Error: monomer length must be positive');
assert( (monomer_length <= total_genome_length), ...
    'Error: monomer length must be less than or equal to total genome length');

% Compute target monomer diameter
monomer_volume = (1/6)*pi*(monomer_fiber_diameter^3)
monomer_density = monomer_length / monomer_volume
target_monomer_volume = target_monomer_length / monomer_density
target_monomer_diameter = (6 * target_monomer_volume / pi)^(1/3)

% Determine number of monomers needed to model each loci
num_loci_monomers = ceil(loci_lengths ./ target_monomer_length);

% Account for padding in locus length attributed to requiring integer
% number of monomers
total_loci_monomer_length = sum(num_loci_monomers) * target_monomer_length

% Compute original nuclear diameter
% For reference, average mammalian nuclear diameter is 6 microns
% Source: http://en.wikipedia.org/wiki/Cell_nucleus
% - Bruce Alberts, Alexander Johnson, Julian Lewis, Martin Raff,
% Keith Roberts, Peter Walter, ed. (2002). Molecular Biology of the Cell,
% Chapter 4, pages 191-234 (4th ed.). Garland Science.
nuclear_diameter = (6 * nuclear_volume / pi)^(1/3);

% Scale down nuclear volume to be proportional to the loci genomic size
scaled_nuclear_volume = ...
    (total_loci_monomer_length / total_genome_length) * nuclear_volume;

% Compute scaled diameter: Volume = 1/6 * PI * DIAMETER ^ 3
% is the same as 4/3 * PI * RADIUS ^ 3
scaled_nuclear_diameter = (6 * scaled_nuclear_volume / pi)^(1/3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Original nuclear volume: %f %s\n', nuclear_volume, NV_UNITS);
fprintf('Original nuclear diameter: %f %s\n', nuclear_diameter, DIAMETER_UNITS);
fprintf('Total genomic length: %f %s\n', total_genome_length, GL_UNITS);
fprintf('Loci genomic length: %f %s\n', total_loci_length, LL_UNITS);
fprintf('Loci genomic length (monomer padded): %f %s\n', total_loci_monomer_length, LL_UNITS);
fprintf('Scaled nuclear volume: %f %s\n', scaled_nuclear_volume, NV_UNITS);
fprintf('Scaled nuclear diameter: %f %s\n', ...
    scaled_nuclear_diameter, DIAMETER_UNITS);
fprintf('Target monomomer diameter: %f %s\n', ...
    target_monomer_diameter, MF_UNITS)
fprintf('Target monomer length: %f %s\n', ...
    target_monomer_length, ML_UNITS)

for locus = 1:length(num_loci_monomers)
    fprintf('Number of monomers for locus %d: %d\n', ...
        locus, num_loci_monomers(locus)) 
end
