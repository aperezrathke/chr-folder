#!/bin/bash  

#PBS -l walltime=1:00:00
#PBS -l nodes=1:ppn=20
#PBS -j oe  
#PBS -m n  
#PBS -M perezrat@uic.edu
#PBS -q jliang
#PBS -N centro

# Usage:
#   qsub <script_name.sh>
#
# Monitor using either:
#   qstat -u <netid>
# or
#   showq
#
# Delete using:
#   qdel <job_id_from_qstat>

# Determine path to script
HOME_DIR=$(echo ~)
PROJ_DIR="$HOME_DIR/u-sac"
PROJ_DIR=$(cd "$PROJ_DIR"; pwd)
SCRIPT_NAME="test_pdb_export.sh"
SCRIPT_DIR="$PROJ_DIR/scripts/Local"
SCRIPT_PATH="$SCRIPT_DIR/$SCRIPT_NAME"

echo "Running $SCRIPT_PATH"
$SCRIPT_PATH
