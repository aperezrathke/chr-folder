#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Script will generate single knock-in interaction (SKI) samples for each
# interaction listed in parameter interactions file. Script will periodically
# poll to see if compute nodes are available and enqueue a new job when a
# node is free.
#
# Usage:
#   python <path_to_this_script> --knock_in <path> --gen_script <path>
#     [--max_comp_nodes <int>] [--sleep_secs <real>] [--ex_sleep_secs <real>]
#     [--max_polls <int>] [--reverse] [--passes <int>] [target_n <int>]
#     [--skip_n <int>] [--skip_bin]
#
#   --knock_in: Path to plain-text interactions file
#   --gen_script: Path to locus specific script for generating SKI samples
#   --max_comp_nodes: Maximum number of compute nodes to use
#   --sleep_secs: Polling interval to check for finished jobs in seconds
#   --ex_sleep_secs: Polling interval if command line output returns
#       unexpected code
#   --max_polls: Max number of attempts to determine finished jobs
#   --reverse: Iterate from last knock-in index to 0-th index
#   --passes: Number of passes through each SKI index. Default value is
#       1. This value is ignored if target_n > 0.
#   --target_n: Will iterate over knock-in indices until at least <n> samples
#       are present at each index. Set to less than or equal to 0 to avoid
#       using this field. Note: default value is 0 if not specified
#   --skip_n: Skip knock-in indices with samples >= <n>. Value is ignored if
#       <n> <= 0. Note: default value is 0 if not specified
#   --skip_bin: Skip knock-in indices with binary .rdata

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For running shell commands
import subprocess

# For exiting if inputs invalid
import sys

# For sleeping until jobs finish
import time

# For determining user name
import getpass

# For conveniently passing user arguments 
from collections import namedtuple

###########################################
# Default parameters
###########################################

# Default max number of compute nodes to use
DEFAULT_MAX_COMPUTE_NODES = 6

# Default polling interval in seconds
DEFAULT_SLEEP_SECS = 20

# Default polling interval after a return code exception
DEFAULT_EXCEPTION_SLEEP_SECS = 30

# Default maximum number of attempts to determine finished jobs
DEFAULT_MAX_POLLS = 1000

# Default number of passes through each SKI index
DEFAULT_PASSES = 1

# Default number of target samples; if <= 0, then ignored
DEFAULT_TARGET_N = 0

# Default sample count skip threshold; if <= 0, then ignored
DEFAULT_SKIP_N = 0

###########################################
# Globals
###########################################

# Store user configuration
Config = namedtuple('Config', "knock_in, gen_script, max_comp_nodes, sleep_secs, ex_sleep_secs, max_polls, passes, user, target_n, skip_n, skip_bin")

# Information about an interaction pair
Interaction = namedtuple("Interaction", "i, j, bin_dir, sample_dir")

# Set of currently running job identifiers
JOB_QUEUE = set()

###########################################
# Methods
###########################################

def exit_if_invalid_args(cfg):
    """Validates user arguments, exits program if invalid."""
    if (not os.path.isfile(cfg.knock_in)):
        print "Error, invalid knock-in interactions path: " + cfg.knock_in
        sys.exit(1)
    if (not os.path.isfile(cfg.gen_script)):
        print "Error, invalid SKI generator script path: " + cfg.gen_script
        sys.exit(1)
    if (cfg.max_comp_nodes < 1):
        print "Error, max compute nodes must be positive integer"
        sys.exit(1)
    if (cfg.sleep_secs <= 0.0):
        print "Error, polling interval must be positive real"
        sys.exit(1)
    if (cfg.ex_sleep_secs <= 0.0):
        print "Error, exception polling interval must be positive real"
        sys.exit(1)
    if (cfg.max_polls <= 0):
        print "Error, poll attempts must be positive integer"
        sys.exit(1)
    if (cfg.passes < 1 and cfg.target_n < 1):
        print "Error, number of passes must be >= 1 or target n must be >= 1"
        sys.exit(1)
    if not cfg.user:
        print "Error, could not determine user name"
        sys.exit(1)

# WARNING: ASSUMES SKI DATA IS STORED RELATIVE TO KNOCK_IN PATH!!!
def parse_interactions(knock_in):
    """Returns list of interaction tuples"""
    loci_dir = os.path.dirname(knock_in)
    ski_base_dir = os.path.join(loci_dir, "ski")
    ints = []
    with open(knock_in, 'r') as f:
        for line in f:
            ij = line.split(",")
            # Frag indices (i,j) are assumed to be first two elements
            if len(ij) >= 2:
                i = ij[0].strip()
                j = ij[1].strip()
                if i and j:
                    ski_ij_base_dir = os.path.join(ski_base_dir, i + "." + j)
                    ski_ij_sample_dir = os.path.join(ski_ij_base_dir, "csv")
                    ints.append( Interaction(i=int(i),
                                             j=int(j),
                                             bin_dir=ski_ij_base_dir,
                                             sample_dir=ski_ij_sample_dir) )
    num = len(ints)
    if (num < 1):
        print "Error, no interactions found"
        sys.exit(1)
    print "Found " + str(num) + " interactions."
    return(ints)

def filter_bin(cfg, ints, kin_ls):
    """Filter indices with binary data (if configured to do so)"""
    # Early out if binary filtering is disabled
    if not cfg.skip_bin:
        return kin_ls

    kin_out = []
    for kix in kin_ls:
        int_ = ints[kix]
        if not os.path.isdir(int_.bin_dir):
            kin_out.append(kix)
        else:
            bin_exists = False
            for fname in os.listdir(int_.bin_dir):
                # Check for existence of binary rdata
                if fname.endswith('.rdata'):
                    bin_exists = True
                    break
            if not bin_exists:
                kin_out.append(kix)
            else:
                print "Filtering interaction (" + str(int_.i) + ", " + str(int_.j) + ") as binary data found in " + int_.bin_dir

    return kin_out

def filter_nsamp(cfg, ints, kin_ls):
    """Filter indices according to sample counts (if configured to do so)"""
    # Early out if filtering disabled
    if ((cfg.skip_n < 1) and (cfg.target_n < 1)):
        return kin_ls

    kin_out = []
    target_n_reached = True
    for kix in kin_ls:
        int_ = ints[kix]
        # Skip to next index if sample directory does not exist
        if not os.path.isdir(int_.sample_dir):
            kin_out.append(kix)
            target_n_reached = False
            continue
        
        # Assume sample directory only contains sample files and nothing else.
        # If this is not the case, consider using:
        # https://stackoverflow.com/questions/2632205/how-to-count-the-number-of-files-in-a-directory-using-python
        #   n = len([fname for fname in os.listdir(int_.sample_dir) if os.path.isfile(fname)])
        n = len(os.listdir(int_.sample_dir))
        if cfg.skip_n >= 1:
            if n < cfg.skip_n:
                target_n_reached = target_n_reached and (n >= cfg.target_n)
                kin_out.append(kix)
            else:
                # Note: avoid evaluating target_n_reached for skipped
                # interactions as this could result in an infinite loop if
                # this condition is met: n_skip <= n_samp < n_target.
                print "Filtering interaction (" + str(int_.i) + ", " + str(int_.j) + ") as " + str(n) + " samples found in " + int_.sample_dir
        else:
            target_n_reached = target_n_reached and (n >= cfg.target_n)
            if not target_n_reached:
                # Avoid possibly expensive iterations over lists of thousands
                # of files if not configured to skip based on sample count
                kin_out = kin_ls
                break

    if target_n_reached and (cfg.target_n >= 1):
        print "Filtering all interactions as target count reached!"
        kin_out = []

    return kin_out

def get_stdout(cmd, cfg):
    """Obtain command line standard output"""
    stdout = ""
    success = False
    # https://stackoverflow.com/questions/2083987/how-to-retry-after-exception-in-python
    # https://stackoverflow.com/questions/28675138/python-check-output-fails-with-exit-status-1-but-popen-works-for-same-command
    # https://www.tutorialspoint.com/python/python_exceptions.htm
    for attempt in xrange(0, cfg.max_polls):
        try:
            stdout = subprocess.check_output(cmd)
        except:
            time.sleep(cfg.ex_sleep_secs)
            continue
        else:
            success = True
            break
    if not success:
        print "Error, command failed: " + ' '.join(cmd)
        sys.exit(0)
    return stdout

def get_active_job_ids(cfg):
    """Determines active job identifiers"""
    job_ids = set()
    cmd = ["showq", "-u", cfg.user]
    stdout = get_stdout(cmd, cfg)
    lines = stdout.splitlines()
    iterlines = iter(lines)
    for line in iterlines:
        # Check if line is a job listing
        if (cfg.user in line):
            # JOBID is first column, so split by white space
            JOBID = line.split()[0]
            JOBID = ''.join(c for c in JOBID if c.isdigit())
            job_ids.add(int(JOBID))
    return job_ids

# @param cfg - User configuration tuple
# @param kix - Knock-in index
def queue_job(cfg, kix):
    """Calls qsub if compute nodes are available, else polls until free."""
    global JOB_QUEUE
    # Check if job queue is full
    while (len(JOB_QUEUE) >= cfg.max_comp_nodes):
        # Wait for a bit before checking if jobs have completed
        time.sleep(cfg.sleep_secs)
        JOB_QUEUE = JOB_QUEUE & get_active_job_ids(cfg)

    # If we reach here, then a compute node is available
    kix_arg = '"' + str(kix) + '"'
    cmd = ["qsub", "-F", kix_arg, cfg.gen_script]
    print "Queuing cmd: " + " ".join(cmd)
    job = get_stdout(cmd, cfg)
    # job now equals: '<integer>.admin.uic.edu', we want the <integer> part:
    job = job.split(".")[0]
    JOB_QUEUE.add(int(job))

# @param cfg - User configuration
def queue_ski(cfg, reverse):
    """Calls SKI generator script for each interaction."""
    # Validate user arguments
    exit_if_invalid_args(cfg)

    # Set working directory to gen script directory so that qsub files are
    # stored in that directory
    owd = os.getcwd()
    os.chdir(os.path.dirname(cfg.gen_script))

    # BEGIN HACK
    # "mutate" tuple, set number of passes to max int if target_n >= 1
    # https://stackoverflow.com/questions/31252939/changing-values-of-a-list-of-namedtuples
    if (cfg.target_n >= 1):
        cfg = cfg._replace(passes = sys.maxint)
    # END HACK

    # Determine interactions
    ints = parse_interactions(cfg.knock_in)
    num_kin = len(ints)

    # Initialize working set of indices
    kin_ls = []
    if not reverse:
        kin_ls = range(num_kin)
    else:
        kin_ls = range(num_kin-1, -1, -1)

    # Filter indices with binary data (if configured to do so)
    kin_ls = filter_bin(cfg, ints, kin_ls)

    for i in xrange(cfg.passes):        
        # Filter indices based on sample count (if configured to do so)
        kin_ls = filter_nsamp(cfg, ints, kin_ls)

        # Exit if no indices to process
        if not kin_ls:
            print "No more SKI indices to process!"
            break

        for kix in kin_ls:
            queue_job(cfg, kix)

    # Restore old working directory
    os.chdir(owd)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= QUEUE SKI MODELS ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-ki', '--knock_in',
        help='Path to knock-in interactions file')
    parser.add_argument('-gs', '--gen_script',
        help='Path to locus specific script for generating SKI samples')
    parser.add_argument('-mcn', '--max_comp_nodes',
        default=DEFAULT_MAX_COMPUTE_NODES,
        help='Maximum number of compute nodes to use')
    parser.add_argument('-ss', '--sleep_secs',
        default=DEFAULT_SLEEP_SECS,
        help='Polling interval to check for job completion in seconds')
    parser.add_argument('-xss', '--ex_sleep_secs',
        default=DEFAULT_EXCEPTION_SLEEP_SECS,
        help='Polling interval to check for job completion after command line exception in seconds')
    parser.add_argument('-mp', '--max_polls',
        default=DEFAULT_MAX_POLLS,
        help='Maximum number of attempts to check for job completion')
    parser.add_argument('-rev', '--reverse', action='store_true',
        help='Switch to toggle order of iteration. If present, will iterate from largest to smallest SKI index')
    parser.add_argument('-pass', '--passes',
        default=DEFAULT_PASSES,
        help='Number of passes through each SKI index. Ignored if --target_n is > 0')
    parser.add_argument('-tn', '--target_n',
        default=DEFAULT_TARGET_N,
        help='Runs until each SKI index has at least this many samples. Ignored if <= 0. If > 0, --passes is ignored.')
    parser.add_argument('-sn', '--skip_n',
        default=DEFAULT_SKIP_N,
        help='Skips SKI index if number of samples is greater than or equal to this value. Ignored if <= 0.')
    parser.add_argument('-sb', '--skip_bin', action='store_true',
        help='Switch to skip SKI indices with binary .rdata')
    
    # Parse command line
    args = parser.parse_args()

    # Determine login-name
    user = getpass.getuser()

    # Print command line
    print '\t-ki = ' + args.knock_in
    print '\t-gs = ' + args.gen_script
    print '\t-mcn = ' + str(args.max_comp_nodes)
    print '\t-ss = ' + str(args.sleep_secs)
    print '\t-xss = ' + str(args.ex_sleep_secs)
    print '\t-mp = ' + str(args.max_polls)
    print '\t-rev = ' + str(args.reverse)
    print '\t-pass = ' + str(args.passes)
    print '\t-tn = ' + str(args.target_n)
    print '\t-sn = ' + str(args.skip_n)
    print '\t-sb = ' + str(args.skip_bin)
    print '\tUSER = ' + user

    # Create 'typed' configuration
    cfg = Config(knock_in=args.knock_in,
                 gen_script=args.gen_script,
                 max_comp_nodes=int(args.max_comp_nodes),
                 sleep_secs=float(args.sleep_secs),
                 ex_sleep_secs=float(args.ex_sleep_secs),
                 max_polls=int(args.max_polls),
                 passes=int(args.passes),
                 user=user,
                 target_n=int(args.target_n),
                 skip_n=int(args.skip_n),
                 skip_bin=args.skip_bin)

    # Queue SKI models
    queue_ski(cfg, args.reverse)

    print "Queue SKI models finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
