#!/bin/bash 

# Runs PDB test commandlet

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
# Project root directory
ROOT_DIR="$SCRIPT_DIR/../.."
# Obtain absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)
# Target binary directory
BIN_DIR="$ROOT_DIR/bin"
# Executable dir
EXE_DIR="$BIN_DIR/Release_tests"
# Executable name
EXE_NAME="u_sac"

# Target directory to write PDBs
OUTPUT_DIR="$ROOT_DIR/output/test_pdb"
mkdir -p "$OUTPUT_DIR/pdb"

# Switch to executable dir
pushd ./
cd $EXE_DIR

# Run commandlet
./$EXE_NAME -test_pdb --output_dir $OUTPUT_DIR

popd

echo "Finished PDB test script."
