#!/usr/bin/env Rscript

###################################################################
# wrangle_chromatin_shell.R
#
# Shell interface for several data wrangling functions. Collection
# of various command utilities to process chromatin data.
#
# Comandline options have long and short forms. The long form
# begins with "--" prefix and the short form begins with a "-"
# prefix. Either form is acceptable and flags may be mixed for
# different options.
#
# All utilities are described by 3 main sections:
#   Utility: gives long form of the command line flag to run
#     target utility
#   Description: brief description of what utility does
#   Shell: format of command line expected by utility followed
#     by brief description of each utility specific flag.
#
# Utility: --util_munge_coords_and_log_weights
# Description: Munges all coordinate CSV files into single list
#   with each list element a NUM_NODE x 3 numeric matrix. Also,
#   creates a parallel numeric vector of log weights in same order
#   as list of coordinate matrices. Both the coordinates list
#   and log weight vector are exported as a single list object
#   stored at <sim_dir>/coords_and_log_weights.rdata. This rdata
#   object may serve as input to downstream analysis utilities.
# Shell: Rscript --vanilla <path_to_script> --util_munge_coords_and_log_weights --sim_id <string> [ski_id <string>] [-ov]
#   --util_munge_coords_and_log_weights|-munge_cords_and_log_weights:
#       Flags utility to be executed
#   --sim_id|-sid:
#       Name of simulation to process. Expects standard data layout
#       where paths for simulation data are derivable from
#       simulation name alone
#   --ski_id|-skid: [optional]
#       Name of single knock-in (SKI) simulation
#   --overwrite|-ov: [optional]
#       Force overwrite any cached data
#
# Utility: --util_compute_contacts
# Description: Computes contacting bins (i.e nodes) for both input
#   and output target resolutions. For each sample, will compute
#   the index matrix of contacting bin pairs. The results as well
#   as the corresponding log weights are cached to disk for faster
#   loading during downstream analysis.
# Shell: Rscript --vanilla <path_to_script> --util_compute_contacts --sim_id <string> --in_bin_bp <+number> --out_bin_bp <+number> --radius|--radius_path <+number|string path> --thresh_dist <+number> --num_cpu <integer> [--ski_id <string>] [-ov]
#   --util_compute_contacts|-compute_contacts:
#       Flags utility to be executed
#   --sim_id|-sid:
#       Name of simulation to process. Expects standard data layout
#       where paths for simulation data are derivable from
#       simulation name alone
#   --in_bin_bp|-ibbp:
#       Number of base pairs that each input bin (i.e. node) spans
#   --out_bin_bp|-obbp:
#       Number of base pairs that each output bin spans. Must be
#       greater than or equal to 'in_bin_bp'
#   --radius|-rad:
#       Scalar homogeneous radius at each node. If polymer nodes have
#       heterogeneous radii, then use radius_path option instead.
#   --radius_path|-radp:
#       File path to radii at each node in CSV format. Must *NOT*
#       contain a header row.
#   --threshold|-thresh:
#       Maximum threshold distance in Angstroms for two bins (nodes)
#       to be considered contacting.
#   --num_cpu|-ncpu
#       Number of CPU cores available for parallel processing. If
#       less than or equal to zero, all detected cores will be used.
#   --ski_id|-skid: [optional]
#       Name of single knock-in (SKI) simulation
#   --overwrite|-ov: [optional]
#       Force overwrite any cached contact data. Will not overwrite
#       coordinates data.
#
# Utility: --util_boot_contact_distrib
# Description: Computes distribution of contacting bin counts. Will
#   generate contact count data if not existent. Final data set is
#   written to:
#     <contact_data_path>.bt.<stat_id>.rdata
#   where <contact_data_path> is implicitly determined from sim_id
#   and other user arguments.
# Shell: Rscript --vanilla <path_to_script> --util_boot_contact_distrib_rank --sim_id <string> num_trial <+integer> --in_bin_bp <+number> --out_bin_bp <+number> --radius|--radius_path <+number|string path> --stat_id <string> --thresh_dist <+number> --parallel <string> --num_cpu <integer> [--ski_id <string>] [-ov]
#   --util_boot_contact_distrib_rank|-boot_contact_distrib_rank:
#       Flags utility to be executed
#   --sim_id|-sid:
#       Name of simulation to process. Expects standard data layout
#       where paths for simulation data are derivable from
#       simulation name alone
#   --num_trial|-ntrial:
#       Number of bootstrap trials
#   --in_bin_bp|-ibbp:
#       Number of base pairs that each input bin (i.e. node) spans
#   --out_bin_bp|-obbp:
#       Number of base pairs that each output bin spans. Must be
#       greater than or equal to 'in_bin_bp'
#   --radius|-rad:
#       Scalar homogeneous radius at each node. If polymer nodes have
#       heterogeneous radii, then use radius_path option instead.
#   --radius_path|-radp:
#       File path to radii at each node in CSV format. Must *NOT*
#       contain a header row.
#   --stat_id|stid:
#       Name of statistic to compute. Options are:
#         "counts" - computes observed contacts
#         "freqs" - computes frequency of observed contacts
#         "ranks" - computes max rank of observed contact counts
#   --threshold|-thresh:
#       Maximum threshold distance in Angstroms for two bins (nodes)
#       to be considered contacting.
#   --parallel|-para:
#       One of "multicore" | "snow" | "no"; specifies parallel
#       processing platform
#     - multicore: uses "multicore" package only available on linux
#     - snow: uses "snow" package available on all platforms
#     - no: disable parallel processing
#   --num_cpu|-ncpu
#       Number of CPU cores available for parallel processing. If
#       less than or equal to zero, all detected cores will be used.
#   --ski_id|-skid: [optional]
#       Name of single knock-in (SKI) simulation
#   --overwrite|-ov: [optional]
#       Force overwrite any cached contact data. Will not overwrite
#       coordinates data.
#
# Utility: --util_boot_strata_distrib
# Description: Similar to util_boot_contact_distrib but statistic is
#   assumed to be a function of two populations. One population is
#   always the null (unconstrained) contact set. The alternate
#   population may also be the null contact set or a single knock-in
#   (SKI) constraint contact set. Similarly, will compute
#   distribution of two-population statistic over contacting bin
#   counts. Will generate contact count data if not existent. Final
#   data set is written to:
#     <contact_data_path>.bt.<stat_id>.rdata
#   where <contact_data_path> is implicitly determined from sim_id
#   and other user arguments.
# Shell: see 'util_boot_contact_distrib'. If 'ski_id' is empty, then
#   alternate population is same as null (unconstrained) population.
###################################################################

###################################################################
# Libraries
###################################################################

# Similar to python optparse package
library(optparse)

###################################################################
# Common path utilities
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(SCRIPT_DIR) == 0) {
  cmdArgs = commandArgs(trailingOnly = FALSE)
  needle = "--file="
  match = grep(needle, cmdArgs)
  if (length(match) > 0) {
    # Rscript
    SCRIPT_DIR = dirname(normalizePath(sub(needle, "", cmdArgs[match])))
  }
}

# @return full path to directory containing this script
get_script_dir <- function() {
  return(SCRIPT_DIR)
}

###################################################################
# Core utilities
###################################################################

if (!exists("SOURCED_wrangle_chromatin_core")) {
  source(file.path(get_script_dir(), "wrangle_chromatin_core.R"))
}

###################################################################
# Parse command line
###################################################################

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
option_list = list(
  # Utilities (alpha-order)
  make_option(
    c("-boot_contact_distrib",
      "--util_boot_contact_distrib"),
    action = "store_true",
    default = FALSE,
    help = "Utility to compute a bootstrap distribution of contacting bin counts"
  ),
  make_option(
    c("-boot_strata_distrib",
      "--util_boot_strata_distrib"),
    action = "store_true",
    default = FALSE,
    help = "Utility to compute a bootstrap distribution over two populations of contacting bin counts"
  ),
  make_option(
    c("-compute_contacts",
      "--util_compute_contacts"),
    action = "store_true",
    default = FALSE,
    help = "Utility to compute contacting bin (i.e node) pairs"
  ),
  make_option(
    c(
      "-munge_coords_and_log_weights",
      "--util_munge_coords_and_log_weights"
    ),
    action = "store_true",
    default = FALSE,
    help = "Utility to munge all CSV coordinates and log weights to single rdata file"
  ),
  # Options (alpha-order)
  make_option(
    c("-ibbp", "--in_bin_bp"),
    type = "double",
    default = NULL,
    help = "Number of base pairs that each input bin (node) spans"
  ),
  make_option(
    c("-ncpu", "--num_cpu"),
    type = "integer",
    default = 1,
    help = "Number of CPU cores to use for parallel processing. If <= 0, all cores are used."
  ),
  make_option(
    c("-ntrial", "--num_trial"),
    type = "integer",
    default = NULL,
    help = "Number of bootstrap trials. Must be > 1."
  ),
  make_option(
    c("-obbp", "--out_bin_bp"),
    type = "double",
    default = NULL,
    help = "Number of base pairs that each output bin (node) spans"
  ),
  make_option(
    c("-ov", "--overwrite"),
    action = "store_true",
    default = FALSE,
    help = "Overwrite any cached data"
  ),
  make_option(
    c("-para", "--parallel"),
    type = "character",
    default = DEF_PARALLEL,
    help = "Parallel processing platform. One of 'multicore'|'snow'|'no'"
  ),
  make_option(
    c("-rad", "--radius"),
    type = "double",
    default = NULL,
    help = "Scalar radius at each node. If nodes have different radii, instead use --radius_path"
  ),
  make_option(
    c("-radp", "--radius_path"),
    type = "character",
    default = NULL,
    help = "CSV file with radius at each node. CSV must *NOT* have a header row! If all nodes are same, instead use --radius."
  ),
  make_option(
    c("-sid", "--sim_id"),
    type = "character",
    default = NULL,
    help = "Simulation identifier to process"
  ),
  make_option(
    c("-skid", "--ski_id"),
    type = "character",
    default = "",
    help = "Single knock-in (SKI) identifier"
  ),
  make_option(
    c("-stid", "--stat_id"),
    type = "character",
    default = DEF_BOOT_STAT_ID,
    help = "Bootstrap statistic to compute"
  ),
  make_option(
    c("-thresh", "--threshold"),
    type = "double",
    default = DEF_CONTACT_THRESH,
    help = "A threshold value such as maximum distance between contacting bins (nodes)"
  )
)

opt_parser = OptionParser(option_list = option_list)

opt = parse_args(opt_parser)

###################################################################
# Misc helper methods
###################################################################

# Handles scalar radius or loads from disk
load_radius <- function(opt) {
  radius = opt$radius
  if (!is.null(opt$radius_path))
  {
    radius = as.numeric(read.table(
      file = opt$radius_path,
      header = FALSE,
      sep = ",",
      strip.white = TRUE
    ))
  }
  stopifnot(!is.null(radius))
  stopifnot(radius >= 0)
  return(radius)
}

###################################################################
# Determine which utility to run
###################################################################

if (opt$util_munge_coords_and_log_weights) {
  result = get_coords(sim_id = opt$sim_id,
                      ski_id = opt$ski_id,
                      overwrite = opt$overwrite)
} else if (opt$util_compute_contacts) {
  result = get_contacts(
    sim_id = opt$sim_id,
    i_bin_bp = opt$in_bin_bp,
    o_bin_bp = opt$out_bin_bp,
    radius = load_radius(opt),
    thresh = opt$threshold,
    ncpu = opt$num_cpu,
    ski_id = opt$ski_id,
    overwrite = opt$overwrite
  )
} else if (opt$util_boot_contact_distrib) {
  result = get_boot_contact_distrib(
    sim_id = opt$sim_id,
    ntrial = opt$num_trial,
    i_bin_bp = opt$in_bin_bp,
    o_bin_bp = opt$out_bin_bp,
    radius = load_radius(opt),
    stat_id = opt$stat_id,
    thresh = opt$threshold,
    parallel = opt$parallel,
    ncpu = opt$num_cpu,
    ski_id = opt$ski_id,
    overwrite = opt$overwrite
  )
} else if (opt$util_boot_strata_distrib) {
  result = get_boot_strata_distrib(
    sim_id = opt$sim_id,
    ntrial = opt$num_trial,
    i_bin_bp = opt$in_bin_bp,
    o_bin_bp = opt$out_bin_bp,
    radius = load_radius(opt),
    stat_id = opt$stat_id,
    thresh = opt$threshold,
    parallel = opt$parallel,
    ncpu = opt$num_cpu,
    ski_id = opt$ski_id,
    overwrite = opt$overwrite
  )
} else {
  print("Utility not found. Exiting.")
}
