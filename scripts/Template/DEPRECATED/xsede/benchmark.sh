#!/bin/bash  


#SBATCH --job-name="bench"
#SBATCH --output="bench.%j.out"
#SBATCH --partition=shared
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --export=ALL
#SBATCH -t 00:05:00

# Usage:
#
#   sbatch <script_name.sh>
#
# Monitor using either:
#   squeue -u <user_id>
#
# Delete using:
#   scancel <job_id>
#
# For more details, see:
#
# https://portal.xsede.org/sdsc-comet
# https://slurm.schedmd.com/sbatch.html

# Determine path to script
HOME_DIR=$(echo ~)
PROJ_DIR="$HOME_DIR/u-sac"
PROJ_DIR=$(cd "$PROJ_DIR"; pwd)
SCRIPT_NAME="benchmark.sh"
SCRIPT_DIR="$PROJ_DIR/scripts/Local"
SCRIPT_PATH="$SCRIPT_DIR/$SCRIPT_NAME"

echo "Running $SCRIPT_PATH"
$SCRIPT_PATH
