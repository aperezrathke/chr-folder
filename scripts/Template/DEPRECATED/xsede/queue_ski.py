#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Script will generate single knock-in interaction (SKI) samples for each
# interaction listed in parameter interactions file. Script will periodically
# poll to see if compute nodes are available and enqueue a new job when a
# node is free.
#
# Usage:
#   python <path_to_this_script> --knock_in <path> --gen_script <path> [--max_comp_nodes <int>] [--sleep_secs <real>] [--passes <int>] [--reverse]
#
#   --knock_in: Path to plain-text interactions file
#   --gen_script: Path to locus specific script for generating SKI samples
#   --max_comp_nodes: Maximum number of compute nodes to use
#   --sleep_secs: Polling interval to check for finished jobs in seconds
#   --passes: Number of passes through each SKI index
#   --reverse: Iterate from last knock-in index to 0-th index

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For running shell commands
import subprocess

# For exiting if inputs invalid
import sys

# For sleeping until jobs finish
import time

# For determining user name
import getpass

# For conveniently passing user arguments 
from collections import namedtuple

###########################################
# Default parameters
###########################################

# Default max number of compute nodes to use
DEFAULT_MAX_COMPUTE_NODES = 6

# Default polling interval in seconds
DEFAULT_SLEEP_SECS = 10

# Default number of passes through each SKI index
DEFAULT_PASSES = 1

###########################################
# Globals
###########################################

# Store user configuration
Config = namedtuple('Config', "knock_in, gen_script, max_comp_nodes, sleep_secs, passes, user")

# Set of currently running job identifiers
JOB_QUEUE = set()

###########################################
# Methods
###########################################

def exit_if_invalid_args(cfg):
    """Validates user arguments, exits program if invalid."""
    if (not os.path.isfile(cfg.knock_in)):
        print "Error, invalid knock-in interactions path: " + cfg.knock_in
        sys.exit(1)
    if (not os.path.isfile(cfg.gen_script)):
        print "Error, invalid SKI generator script path: " + cfg.gen_script
        sys.exit(1)
    if (cfg.max_comp_nodes < 1):
        print "Error, max compute nodes must be positive integer"
        sys.exit(1)
    if (cfg.sleep_secs <= 0.0):
        print "Error, polling interval must be positive real"
        sys.exit(1)
    if (cfg.passes < 1):
        print "Error, number of passes must be >= 1"
        sys.exit(1)
    if not cfg.user:
        print "Error, could not determine user name"
        sys.exit(1)

def count_interactions(knock_in):
    """Determines number of interactions to model."""
    # Count number of non-empty lines in interaction file
    num = 0
    with open(knock_in, 'r') as f:
        num = len(list(filter(lambda x: x.strip(), f)))
    if (num <= 0):
        print "Error, no interactions found"
        sys.exit(1)
    print "Found " + str(num) + " interactions."
    return(num)

def get_active_job_ids(cfg):
    """Determines active job identifiers"""
    job_ids = set()
    cmd = ["squeue", "-u", cfg.user]
    stdout = subprocess.check_output(cmd)
    lines = stdout.splitlines()
    iterlines = iter(lines)
    # Skip header row
    next(iterlines)
    for line in iterlines:
        # JOBID is first column, so split by white space
        JOBID = line.split()[0]
        JOBID = ''.join(c for c in JOBID if c.isdigit())
        job_ids.add(int(JOBID))
    return job_ids

# @param cfg - User configuration tuple
# @param kix - Knock-in index
def queue_job(cfg, kix):
    """Calls qsub if compute nodes are available, else polls until free."""
    global JOB_QUEUE
    # Check if job queue is full
    while (len(JOB_QUEUE) >= cfg.max_comp_nodes):
        # Wait for a bit before checking if jobs have completed
        time.sleep(cfg.sleep_secs)
        JOB_QUEUE = JOB_QUEUE & get_active_job_ids(cfg)

    # If we reach here, then a compute node is available
    kix_arg=str(kix)
    cmd = ["sbatch", cfg.gen_script, kix_arg]
    print "Queuing cmd: " + " ".join(cmd)
    job = subprocess.check_output(cmd)
    # job now equals: 'Submitted batch job <integer>', we want the <integer> part:
    job = ''.join(c for c in job if c.isdigit())
    JOB_QUEUE.add(int(job))

# @param cfg - User configuration
def queue_ski(cfg, reverse):
    """Calls SKI generator script for each interaction."""
    # Validate user arguments
    exit_if_invalid_args(cfg)

    # Determine number of interactions
    num_kin = count_interactions(cfg.knock_in)

    # Set working directory to gen script directory so that qsub files are
    # stored in that directory
    owd = os.getcwd()
    os.chdir(os.path.dirname(cfg.gen_script))

    for i in xrange(cfg.passes):
        # BEGIN HACK
        # This cannot be factored out of loop as iterating over the xrange
        # list appears to "deplete" the list so that future traversals are
        # over an "empty" list. Therefore, we need to regenerate the xrange
        # for each pass.
        kin_ls = xrange(num_kin)
        if reverse:
            kin_ls = reversed(kin_ls)
        # END HACK
        for kix in kin_ls:
            queue_job(cfg, kix)

    # Restore old working directory
    os.chdir(owd)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= QUEUE SKI MODELS ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-ki', '--knock_in',
        help='Path to knock-in interactions file')
    parser.add_argument('-gs', '--gen_script',
        help='Path to locus specific script for generating SKI samples')
    parser.add_argument('-mcn', '--max_comp_nodes',
        default=DEFAULT_MAX_COMPUTE_NODES,
        help='Maximum number of compute nodes to use')
    parser.add_argument('-ss', '--sleep_secs',
        default=DEFAULT_SLEEP_SECS,
        help='Polling interval to check for job completion in seconds')
    parser.add_argument('-pass', '--passes',
        default=DEFAULT_PASSES,
        help='Number of passes through each SKI index')
    parser.add_argument('-rev', '--reverse', action='store_true')

    # Parse command line
    args = parser.parse_args()

    # Determine login-name
    user = getpass.getuser()

    # Print command line
    print '\t-ki = ' + args.knock_in
    print '\t-gs = ' + args.gen_script
    print '\t-mcn = ' + str(args.max_comp_nodes)
    print '\t-ss = ' + str(args.sleep_secs)
    print '\t-pass = ' + str(args.passes)
    print '\t-rev = ' + str(args.reverse)
    print '\tUSER = ' + user

    # Create 'typed' configuration
    cfg = Config(knock_in=args.knock_in,
                 gen_script=args.gen_script,
                 max_comp_nodes=int(args.max_comp_nodes),
                 sleep_secs=float(args.sleep_secs),
                 passes=int(args.passes),
                 user=user)

    # Queue SKI models
    queue_ski(cfg, args.reverse)

    print "Queue SKI models finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
