#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script parses a 'bun' CSV formatted file and generates a build configuration.
#
#------------------------------------------------------------------------------
# Arguments
#------------------------------------------------------------------------------
# Script has the following arguments: (short-hand|long-hand)
#
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   -b|--bun <path>: path to bun file
#   -o|--out <path>: path to target folder to create build directory
#   [-c|--compile] <target>: [Optional] Performs compilation for build target
#       as specified in '<build_scripts_dir>/targets.txt'. If using default
#       <build_scripts_directory> (./build), some possible options include:
#           none : no compilation step is performed
#           linux-gnu : generic linux build with g++ compiler
#           linux-gnu-threaded: linux g++ compiler with threading enabled
#           linux-gnu-no-avx : linux g++ compiler with AVX collisions disabled
#           linux-gnu-no-avx-threaded : linux g++, with threads, no AVX
#               collision
#           extreme1-intel : build for UIC extreme cluster login 1, with intel
#               compiler
#           extreme2-intel : build for UIC extreme cluster login 2, with intel
#               compiler
#           gobi-intel : build for Liang Lab gobi with intel compiler
#           gobi-intel-threaded: threaded build for Liang lab gobi intel
#               compiler
#           xsede-intel : build for XSEDE cluster with intel compiler
#       Multiple build targets may be listed as a whitespace separated list.
#       For example:
#           --compile linux-gnu linux-gnu-threaded
#       will compile both the serial and threaded build targets with linux g++
#   [-bscdir|--build_scripts_dir] <dirpath>: [Optional] Path to directory
#       containing a targets.txt file with CSV key, value mappings where 'key'
#       specifies the build target name (e.g. 'linux-gnu') and 'value'
#       specifies the relative path to the build script associated to the
#       'key'. By default, this value is set to the ./build folder relative to
#       this script. See example: ./build/targets.txt. This value is ignored
#       if --compile switch is 'none'!
#   [-f|--force] : [Optional] If switch is present, any previous build
#       directory will be removed. If switch is not present and a previous
#       build exists, then script will terminate with error.
#   [-ncb|--no_copy_bin]: [Optional] If switch is present, binaries in the
#       CMakeBuild folder are not automatically copied to the bin folder
#
#------------------------------------------------------------------------------
# BUN FORMAT
#------------------------------------------------------------------------------
# A valid bun file contains comma-separated values with no whitespace allowed
# in the entries. Any line beginning with a '#' is ignored. The expected
# columns are: <model_json_path>, <usage_target>, <cmd_key> where
#   - model_json_path: path to model json file, if entry is a file name
#       without path information, then file is expected to reside in same
#       folder as bun file; for certain usages, the entry 'default' is
#       acceptable and will result in the default JSON model
#   - usage: The usage/purpose of the model, currently 'main', 'null', and
#       'fea' are supported.
#   - cmd_key: a unique command line key for running the model under the target
#       use case. To run the (model, usage) pair, the expected command line
#       will have the following format:
#           <path_to_binary_exe> --<usage>_dispatch <cmd_key> [...]
#       For example, if the usage is 'null' and command key is 'vanilla', then
#       the model may be run like so:
#           <path_to_binary_exe> --null_dispatch vanilla [...]
#
# See tests/test_json2cpp/test.bun for an example file

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For exiting if inputs invalid
import sys

# For creating build folder
import shutil

# For running shell scripts
import subprocess

# For creating .bun entries
from collections import namedtuple

# For making sure local scripts folder exists
from create_local_workspace import create_local

# For converting json specification to C++
from transpile import json2cpp

# For compiling resulting build
# https://stackoverflow.com/questions/279237/import-a-module-from-a-relative-path
LIB_PATH = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build'))
sys.path.append(LIB_PATH)
from compile_target import get_available_compile_targets
from compile_target import compile_target

###########################################
# BunRec
###########################################

# 'Bun Record' : row within a .bun file
# .json - Path to model JSON
# .use - Target model usage
# .cmd - command line key for executing model under target usage
BunRec = namedtuple('BunRec', "json, use, cmd")

###########################################
# PathManager
###########################################

# Utility class for obtaining paths to all necessary files and folders
class PathManager:
    def __init__(self, user_out_dir):
        self.dst_root_dir = os.path.join(user_out_dir, "build")
        src_scripts_dir = os.path.dirname(os.path.realpath(__file__))
        self.src_root_dir = os.path.join(src_scripts_dir, "..", "..")

    def get_root_dir(self, is_src):
        return self.src_root_dir if is_src else self.dst_root_dir

    def get_scripts_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "scripts")

    def get_scripts_local_dir(self, is_src):
        return os.path.join(self.get_scripts_dir(is_src), "Local")

    def get_scripts_build_dir(self, is_src):
        return os.path.join(self.get_scripts_local_dir(is_src), "build")

    def get_scripts_build_config_dir(self, is_src):
        return os.path.join(self.get_scripts_build_dir(is_src), "config")

    def get_copy_bin_sh(self, is_src):
        return os.path.join(self.get_scripts_build_dir(is_src), "copy_bin.sh")

    def get_default_json(self, use, is_src):
        json_fname = use + ".default.json"
        return os.path.join(self.get_scripts_build_config_dir(is_src),
                            json_fname)

    def get_source_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "source")

    def get_vstudio_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "vstudio")

    def get_libraries_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "libraries")

    def get_dlls_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "dlls")

    def get_tools_dir(self, is_src):
        return os.path.join(self.get_root_dir(is_src), "tools")

    def get_canvas_glue_header(self):
        return os.path.join(self.get_source_dir(is_src=False), "uCanvasGlue.h")

    def get_canvas_main_header(self):
        return os.path.join(self.get_source_dir(is_src=False), "uCanvasMain.h")

    def get_canvas_null_header(self):
        return os.path.join(self.get_source_dir(is_src=False), "uCanvasNull.h")

    def get_canvas_fea_header(self):
        return os.path.join(self.get_source_dir(is_src=False), "uCanvasFea.h")

###########################################
# Utilities
###########################################

# Recursively creates build directory such that all parent directories are
# created if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
   '''Utility for making a directory if not existing.'''
   if not os.path.exists(d):
       os.makedirs(d)

# @param func - PathManager callback method
# @param msg - message to display on stdout
def copy_tree(func, msg):
    '''Utility copies source directory tree to user destination'''
    src_dir = func(is_src=True)
    dst_dir = func(is_src=False)
    print msg
    print "\tFROM: " + src_dir
    print "\tTO: " + dst_dir
    shutil.copytree(src_dir, dst_dir)

# @param func - PathManager callback method
# @parma files - list of file *names* to copy
# @param msg - message to display on stdout
def copy_files(func, files, msg):
    '''Utility copies source list of files to user destination'''
    src_dir = func(is_src=True)
    dst_dir = func(is_src=False)    
    assert type(files) in [str, list]
    f_ls = files if isinstance(files, list) else [files]
    print msg
    make_dir(dst_dir)
    for f in f_ls:
        src_f = os.path.join(src_dir, f)
        dst_f = os.path.join(dst_dir, f)
        print "\tFROM: " + src_f
        print "\tTO: " + dst_f
        shutil.copy(src_f, dst_f)

# @param d - Directory path
# @param exts - list of file extensions
def get_files_with_exts(d, exts):
    '''Non-recursively checks directory for files matching extensions'''
    out = []
    for f in os.listdir(d):
        if f.endswith(tuple(exts)):
            out.append(f)
    return out

# Based on http://code.activestate.com/recipes/193736-clean-up-a-directory-tree/
# @param path - file or empty directory to remove
# @param func - os.remove if path is a file, os.rmdir if path is a directory
def rm_internal_generic(path, rmfunc):
    '''Generic remove file or folder callback - DO NOT CALL DIRECTLY'''
    try:
        rmfunc(path)
    except OSError, (errno, strerror):
        ERROR_STR= """Error removing %(path)s, %(error)s """
        print ERROR_STR % {'path' : path, 'error': strerror }

# @param abs_dir - absolute directory path
# @param ignore_ls - list of files/folders to ignore
# Based on http://code.activestate.com/recipes/193736-clean-up-a-directory-tree/
def rm_internal_subtree(abs_dir, ignore_ls):
    '''Remove files within folder - DO NOT CALL DIRECTLY'''
    files = os.listdir(abs_dir)
    for f in files:
        abs_f = os.path.join(abs_dir, f)
        if abs_f in ignore_ls:
            continue
        if os.path.isfile(abs_f):
            rmfunc = os.remove
            rm_internal_generic(abs_f, rmfunc)
        elif os.path.isdir(abs_f):
            rmsubtree(abs_f, ignore_ls)
            # Check if folder is parent folder of a file we ignore, in which
            # case it will be non-empty and therefore cannot be deleted
            is_ignored = [s for s in ignore_ls if abs_f in s]
            if not is_ignored:
                rmfunc = os.rmdir
                rm_internal_generic(abs_f, rmfunc)

# Removes all files within directory but can keep specified subfolders and
# files. The starting directory 'path' is not removed.
# @param path - path to folder to clean
# @param ignore - list of sub-folders and files to ignore, any relative paths
#   are considered relative to 'path' variable
def rmsubtree(path, ignore):
    '''Remove files within folder'''
    if not os.path.isdir(path):
        return
    prev_work_dir = os.getcwd()
    abs_dir = os.path.abspath(path)
    os.chdir(abs_dir)
    ignore_ls = ignore if isinstance(ignore, list) else [ignore]
    ignore_ls = map(os.path.abspath, ignore_ls)
    os.chdir(prev_work_dir)
    rm_internal_subtree(abs_dir, ignore_ls)

###########################################
# Initialize destination build folder
###########################################

# @param args - user named arguments
# @param pathman - PathManager object
def init_build_dir(args, pathman):
    '''Copies all necessary build items to build folder'''
    # Create root build directory
    dst_root_dir = pathman.get_root_dir(is_src=False)
    print "Initializing build directory:\n\t" + dst_root_dir
    if os.path.exists(dst_root_dir):
        if args.force:
            print "Warning: cleaning existing build folder: " + dst_root_dir
            # Avoid clearing 'bin' subfolder as we may have jobs running
            rmsubtree(path=dst_root_dir, ignore="bin")
        else:
            print "Error: target folder already exists: " + dst_root_dir
            print "Please remove folder and re-run, or re-run with --force"
            print "Exiting."
            sys.exit(1)
    make_dir(dst_root_dir)
    # C++ source
    copy_tree(func=pathman.get_source_dir, msg="Copying C++ source files...")
    # Visual studio
    vstudio_files = get_files_with_exts(d=pathman.get_vstudio_dir(is_src=True),
                                        exts=[".sln", ".vcxproj", ".vcxproj.filters"])
    copy_files(func=pathman.get_vstudio_dir, files=vstudio_files,
               msg="Copying visual studio files...")
    # Binary libraries
    copy_tree(func=pathman.get_libraries_dir,
              msg="Copying binary libraries...")
    # Dlls
    copy_tree(func=pathman.get_dlls_dir, msg="Copying dlls...")
    # Utility scripts
    copy_files(func=pathman.get_scripts_local_dir,
               files=["bit2pol.py", "infea.py", "scale_nucleus.py"],
               msg="Copying utility scripts...")
    # Build scripts
    copy_tree(func=pathman.get_scripts_build_dir,
              msg="Copying build scripts...")
    # CMake file
    copy_files(func=pathman.get_root_dir, files=["CMakeLists.txt"],
               msg="Copying CMake files...")
    # External tools
    copy_files(func=pathman.get_tools_dir, files=["juicer_tools.jar"],
               msg="Copying external tools...")

###########################################
# Generate C++
###########################################

# @param bun_path - path to .bun file
# @param pathman - PathManager object
# @return List of BunRec tuples
def load_bun(bun_path, pathman):
    '''Parses .bun file, returns list of BunRec tuples'''
    print "Loading bun file: " + bun_path
    num = 0
    recs = []
    IX_JSON=0
    IX_USE=1
    IX_CMD=2
    NUM_COL=3
    with open(bun_path) as f:
        prev_work_dir = os.getcwd()
        bun_dir = os.path.abspath(os.path.dirname(bun_path))
        os.chdir(bun_dir)
        for line in f:
            num = num + 1
            line = line.strip()
            if not line or line.startswith('#'):
               continue
            row = map(str.strip, line.split(','))
            if len(row) != NUM_COL:
                print "Error: malformed line in " + bun_path
                print "\tline #" + str(num) + " has " + str(len(row)) + " values, expected " + str(NUM_COL)
                print "Exiting."
                sys.exit(1)
            # Handle default JSON
            json = row[IX_JSON]
            use = row[IX_USE]
            if (json == "default"):
                json = os.path.abspath(pathman.get_default_json(use=use, is_src=False))
            else:
                json = os.path.abspath(row[IX_JSON])
            # Create record
            rec = BunRec(json = json,
                         use = use,
                         cmd = row[IX_CMD])
            recs.append(rec)
        os.chdir(prev_work_dir)
    return recs

# @param cpp_path - path to C++ file
# @return base filename without extension
def get_cpp_core_name(cpp_path):
    '''C++ base name without extension'''
    base = os.path.basename(cpp_path)
    return os.path.splitext(base)[0]

# @param f - open file stream
# @param core_name - non-extension file name
def write_cpp_prefix(f, core_name):
    '''Prefix code at top of C++ header file'''
    # Prefix
    f.write('//****************************************************************************\n')
    f.write('// ' + core_name + '.h\n')
    f.write('//****************************************************************************\n')
    f.write('\n')
    f.write('/**\n')
    f.write(' * AUTO-GENERATED CODE FROM BAKE.PY\n')
    f.write(' */\n')
    f.write('\n')
    f.write('#ifndef ' + core_name + '_h\n')
    f.write('#define ' + core_name + '_h\n')
    f.write('\n')
    f.write('#include "uBuild.h"\n')
    f.write('\n')

# @param f - open file stream
# @param name - non-extension file name
def write_cpp_suffix(f, name):
    '''Suffix code at end of C++ header file'''
    # Suffix
    f.write('#endif  // ' + name + '_h\n')  

# @param recs - List of BunRec tuples
# @param pathman - PathManager object
# @return Dict from model json entries -> top-level C++ simulation names
def generate_glue_header(recs, pathman):
    '''Parses model JSON and generates glue C+++ header file'''
    # @TODO - Add build flags around any glue objects which only exist under
    #   commandlet or test builds
    cpp_path = pathman.get_canvas_glue_header()
    cpp_core_name = get_cpp_core_name(cpp_path)
    print "Generating C++ glue header file: " + cpp_path
    json2cppsim = {}
    headers = set()
    ns = {}
    with open(cpp_path, 'w') as f:
        # Prefix
        write_cpp_prefix(f, cpp_core_name)
        # Process .bun entries
        for rec in recs:
            # Skip model glue if already written
            if rec.json in json2cppsim:
                continue
            # Process JSON
            info = json2cpp(rec.json)
            # Check if non-unique namespace
            if info.name in ns:
                print "Error: JSON has non-unique model name: " + info.name
                print "These JSON files have same model name: "
                print "\t" + ns[info.name]
                print "\t" + rec.json
                print "Please correct by making 'name' fields different."
                print "Exiting.\n"
                sys.exit(1)
            ns[info.name] = rec.json
            # Determine new headers
            new_headers = sorted(list(info.headers.difference(headers)))
            headers = headers.union(new_headers)
            # Add headers
            for h in new_headers:
                f.write('#include "' + h + '"\n')
            # Begin glues namespace
            f.write('\n')
            cpp_namespace = 'u_' + info.name + "_model"
            f.write('namespace ' + cpp_namespace + ' {\n')
            # Write each glue object, child glues must be written first as they
            # are referred to by parent glues
            for g in reversed(info.glues):
                # Begin glue class
                f.write('\n')
                f.write('class ' + g.name + ' {\n')
                f.write('public:\n')
                it = iter(g)
                # Skip name field
                next(it)
                # Write glue entries
                for tydf in it:
                    f.write('    ' + tydf + ';\n')
                # End glue class
                f.write('};\n')
            # End glues namespace
            f.write('\n')
            f.write('}  // namespace ' + cpp_namespace + '\n')
            f.write('\n')                        
            # Create mapping from JSON to C++ top-level simulation type
            top_sim = cpp_namespace + '::' + info.glues[0].name + '::sim_t'
            json2cppsim[rec.json] = top_sim
        # Suffix
        write_cpp_suffix(f, cpp_core_name)
    return json2cppsim

# @param recs - List of BunRec tuples    
# @param json2cppsim - Dict: JSON model file name -> Top-level C++ sim name
# @param cpp_path - Destination path of canvas header file
# @param cpp_use - Bun usage key
# @param cpp_class - Name of C++ class containing apps
# @param core_app_header - C++ header containing core app utility
# @param core_app_name - C++ name of core app utility
def generate_app_header(recs, json2cppsim, cpp_path, cpp_use, cpp_class,
                        core_app_header, core_app_name):
    '''Generates app header file'''
    cpp_core_name = get_cpp_core_name(cpp_path)
    cpp_method_prefix = 'main'
    print "Generating C++ " + cpp_use + " header file: " + cpp_path
    cmds = {}
    tab1 = '    '
    tab2 = tab1 + tab1
    tab3 = tab2 + tab1
    with open(cpp_path, 'w') as f:
        # Prefix
        write_cpp_prefix(f, cpp_core_name)
        # Additional headers
        f.write('#include "' + core_app_header + '"\n')
        f.write('#include "uCanvasGlue.h"\n')
        f.write('#include "uCmdOptsMap.h"\n')
        f.write('\n')
        f.write('#include <string>\n')
        f.write('\n')
        # Begin enclosing class (acts as namespace)
        f.write('class ' + cpp_class + ' {\n')
        f.write('public:\n')
        # Process .bun entries
        for count, rec in enumerate(recs):
            assert rec.json in json2cppsim
            if rec.use != cpp_use:
                continue
            if rec.cmd in cmds:
                print "Error: duplicate command key found: " + rec.cmd
                print "Bun file usage targets must contain only unique keys."
                print "Exiting."
                sys.exit(1)
            cpp_method = cpp_method_prefix + str(count)
            cmds[rec.cmd] = cpp_method
            # Begin member main method
            f.write('\n')
            f.write(tab1 + 'static int ' + cpp_method + '(const uCmdOptsMap& cmd_opts) {\n')
            # Method body
            f.write(tab2 + 'typedef ' + json2cppsim[rec.json] + ' sim_t;\n')
            f.write(tab2 + 'const std::string announce(\n')
            f.write(tab3 + '"Running ' + cpp_use + ' model \'' + rec.cmd + '\'");\n')
            f.write(tab2 + 'return ' + core_app_name + '<sim_t>(cmd_opts, announce);\n')
            # End member main
            f.write(tab1 + '}  // '+ cpp_method + '\n')
        # Begin command key map
        f.write('\n')
        f.write(tab1 + 'static int dispatch(const uCmdOptsMap& cmd_opts, const std::string& cmd_key) {\n')
        for c in cmds:
            f.write(tab2 + 'if (cmd_key == "' + c + '") {\n')
            f.write(tab3 + 'return ' + cmds[c] + '(cmd_opts);\n')
            f.write(tab2 + '}\n')
        f.write(tab2 + 'return 0;\n')
        # End command key map
        f.write(tab1 + '}  // dispatch\n')
        # End enclosing class
        f.write('};  // ' + cpp_class + '\n')
        f.write('\n')
        # Suffix
        write_cpp_suffix(f, cpp_core_name)
    
# @param recs - List of BunRec tuples    
# @param json2cppsim - Dict: JSON model file name -> Top-level C++ sim name
# @param cpp_path - Destination path of canvas header file
# @param cpp_use - Bun usage key
# @param cpp_class - Name of C++ class containing apps
def generate_basic_app_header(recs, json2cppsim, cpp_path, cpp_use, cpp_class):
    '''Generates basic app header file'''
    generate_app_header(recs=recs, json2cppsim=json2cppsim, cpp_path=cpp_path,
                        cpp_use=cpp_use, cpp_class=cpp_class,
                        core_app_header="uAppUtils.h",
                        core_app_name="uAppUtils::default_main")

# @param recs - List of BunRec tuples
# @param pathman - PathManager object       
# @param json2cppsim - Dict: JSON model file name -> Top-level C++ sim name
def generate_main_header(recs, pathman, json2cppsim ):
    '''Generates header file for running main simulations'''
    generate_basic_app_header(recs=recs, json2cppsim=json2cppsim,
                              cpp_path=pathman.get_canvas_main_header(),
                              cpp_use='main', cpp_class='uSisMainApps')

# @param recs - List of BunRec tuples
# @param pathman - PathManager object       
# @param json2cppsim - Dict: JSON model file name -> Top-level C++ sim name
def generate_null_header(recs, pathman, json2cppsim):
    '''Generates header file for running null simulations'''
    generate_basic_app_header(recs=recs, json2cppsim=json2cppsim,
                              cpp_path=pathman.get_canvas_null_header(),
                              cpp_use='null', cpp_class='uSisNullApps')

# @param recs - List of BunRec tuples
# @param pathman - PathManager object       
# @param json2cppsim - Dict: JSON model file name -> Top-level C++ sim name
def generate_fea_header(recs, pathman, json2cppsim):
    '''Generates fea(sibility) app header file'''
    generate_app_header(recs=recs, json2cppsim=json2cppsim,
                        cpp_path=pathman.get_canvas_fea_header(),
                        cpp_use='fea', cpp_class='uSisFeaApps',
                        core_app_header="uAppFea.h",
                        core_app_name="uAppFea::main")

# @param args - user named arguments
# @param pathman - PathManager object            
def process_bun(args, pathman):
    '''Loads bun file and processes each entry'''
    recs = load_bun(args.bun, pathman)
    json2cppsim = generate_glue_header(recs, pathman)
    generate_main_header(recs, pathman, json2cppsim)
    generate_null_header(recs, pathman, json2cppsim)
    generate_fea_header(recs, pathman, json2cppsim)

###########################################
# Compile
###########################################

# @param args - user named arguments
# @param pathman - PathManager object
def check_compile(args, pathman):
    '''Check if we should compile'''
    print "Checking compilation target(s): " + str(args.compile)
    if not args.compile:
        # Early out if no compilation targets
        print "No compilation target(s) found."
        return
    # Check for custom build script path
    bsc_dir = pathman.get_scripts_build_dir(is_src=False)
    if args.build_scripts_dir is not None:
        bsc_dir = args.build_scripts_dir
    # Process each build target
    targets = args.compile if isinstance(args.compile, list) \
                           else [args.compile]
    for target in targets:
        # Try compiling this target
        print "Processing compilation target: " + target
        res = compile_target(target_key=target, wdir=bsc_dir,
                             print_targets=False)
        # Report possible invalid compile target
        if not res:
            print "Warning: invalid compile target: " + target
            targets = get_available_compile_targets(wdir=bsc_dir)
            print "AVAILABLE TARGETS for: " + str(bsc_dir)
            for t in targets:
                print "\t" + str(t)
    # See if we should copy compiled binaries
    if (args.build_scripts_dir is None) and (not args.no_copy_bin):
        copy_bin_sh = pathman.get_copy_bin_sh(is_src=False)
        subprocess.call(copy_bin_sh)

###########################################
# Bake
###########################################

# @param args - user named arguments
def bake(args):
    '''Creates build environment from user configuration recipe'''
    # Ensure local scripts folder exists
    create_local(force=True)
    # Initialize build folder
    pathman = PathManager(user_out_dir=args.out)
    init_build_dir(args=args, pathman=pathman)
    # Perform .bun ops
    process_bun(args=args, pathman=pathman)
    # Check if we should compile
    check_compile(args, pathman)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= BAKE ======================="
    logo = r"""
 ______     ______     __  __     ______    
/\  == \   /\  __ \   /\ \/ /    /\  ___\   
\ \  __<   \ \  __ \  \ \  _"-.  \ \  __\   
 \ \_____\  \ \_\ \_\  \ \_\ \_\  \ \_____\ 
  \/_____/   \/_/\/_/   \/_/\/_/   \/_____/ 
                                            
"""
    print logo
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Required arguments
    # https://stackoverflow.com/questions/24180527/argparse-required-arguments-listed-under-optional-arguments
    req_args = parser.add_argument_group('required named arguments')
    req_args.add_argument('-b', '--bun', required=True,
                          help='Path to bun file')
    req_args.add_argument('-o', '--out', required=True,
                          help="Path to *parent* directory where 'bread' build directory will be created")
    # Optional arguments
    parser.add_argument('-c', '--compile', default='none', nargs='*',
                        help='Compile generated build output used target compiler(s), see default ./build/targets.txt')
    parser.add_argument('-bscdir', '--build_scripts_dir', default=None,
                        help='Path to build scripts directory containing compilation targets.txt file')
    parser.add_argument('-f', '--force', action='store_true',
                        help="Remove any previously existing build output")
    parser.add_argument('-ncb', '--no_copy_bin', action='store_true',
                        help="Avoid overwriting binaries in bin folder, useful if active jobs are running")
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t--bun = ' + args.bun
    print '\t--out = ' + args.out
    print '\t[--compile] = ' + str(args.compile)
    print '\t[--build_scripts_dir] = ' + str(args.build_scripts_dir)
    print '\t[--force] = ' + str(args.force)
    print '\t[--no_copy_bin] = ' + str(args.no_copy_bin)
    # Bake some bread!    
    bake(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
