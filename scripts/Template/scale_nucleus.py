#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script computes scaled chromatin simulation parameters such that a constant
# chromatin density is maintained. Script accepts arguments (short|long):
#   -nv|--nuc_vol <+real microns> : Nuclear volume unscaled in microns
#   -gbp|--gen_bp <+real base pairs> : Whole genome length in base pairs
#   -lbp|--loc_bp <+real base pairs> : Total loci length(s) in base pairs, may
#       be scalar (single-locus) or vector (multi-locus)
#   -mbp|--mon_bp <+real base pairs> : Target monomer unit density|densities
#       in base pairs, may be scalar (homogeneous) or vector (heterogeneous);
#       if heterogeneous vector, then vector must be tip-to-tail in same order
#       as loci present in 'loc_bp' and must sum to loci base pair lengths
#   -fbp|--fib_bp <+real base pairs> : Intrinsic fiber unit density in base
#       pairs, may be scalar (homogeneous) or vector (heterogeneous) fiber
#   -fl|--fib_len <+real Angstroms> : Intrinsic fiber unit linear length in
#       Angstroms, e.g 110 Angstroms for 11 nm fiber, may be scalar
#       (homogeneous) or  vector (heterogeneous) fiber

####################################################################
# Imports
####################################################################

# For parsing user supplied arguments
import argparse

# For math.ceil
import math

# For working with vectors
import numpy as np

# For exiting if inputs invalid
import sys

# For named outputs/input
from collections import namedtuple

####################################################################
# Named tuples
####################################################################

# MlocHetrArg
# .loc_bp - +real np.ndarray of loci lengths in base pairs
# .mon_bp - +real np.ndarray of monomer lengths in base pairs
# .mon2loc - integer np.ndarray mapping monomer index to loci index (parallel
#   parallel array to mon_bp)    
# .fib_bp - +real np.ndarray of intrinsic fiber densities in base pairs at
#   each monomer index (parallel array to mon_bp)
# .fib_len - +real np.ndarray of intrinsic fiber linear lengths in Angstroms
#   at each monomer index (parallel array to mon_bp)
MlocHetrArg_Fields = ['loc_bp', 'mon_bp', 'mon2loc', 'fib_bp', 'fib_len']
MlocHetrArg = namedtuple('MlocHetrArg', MlocHetrArg_Fields)

# ScaledParam
# .nuc_diam = Scaled nuclear diameter in Angstroms (+real)
# .mon_diam = Scaled monomer diameters in Angstroms (+real np.ndarray)
# .mon_num = Number of monomers per locus (+integer np.ndarray)
# .mon_bp = Base pairs at each monomoer (+real np.ndarray)
# .mon2loc = Mapping from monomer index to locus index (integer np.ndarray)
ScaledParam_Fields = ['nuc_diam', 'mon_diam', 'mon_num', 'mon_bp', 'mon2loc']
ScaledParam = namedtuple('ScaledParam', ScaledParam_Fields)

####################################################################
# Global presets
####################################################################

###########################################
# Whole genome lengths in base pairs
###########################################

# Human (GRCh37.p13 or hg19) - from EBI
# http://www.ebi.ac.uk/ena/data/view/GCA_000001405.14
GBP_HG19_HAPLOID = 3234834689
GBP_HG19_DIPLOID = 2 * GBP_HG19_HAPLOID

# Mouse - from EBI:
# http://www.ebi.ac.uk/ena/data/view/GCA_000001635.5
GBP_MUS_MUSCULUS_HAPLOID = 2800055571
GBP_MUS_MUSCULUS_DIPLOID = 2 * GBP_MUS_MUSCULUS_HAPLOID

# Preset mapping for whole genome lengths
GBP = { \
    "GBP_HG19_DIPLOID" : GBP_HG19_DIPLOID,
    "GBP_HG19_HAPLOID" : GBP_HG19_HAPLOID,
    "GBP_MUS_MUSCULUS_DIPLOID" : GBP_MUS_MUSCULUS_DIPLOID,
    "GBP_MUS_MUSCULUS_HAPLOID" : GBP_MUS_MUSCULUS_HAPLOID}

###########################################
# Nuclear volumes in microns
###########################################

# For reference, average mammalian nuclear diameter is 6 microns
# Source: http://en.wikipedia.org/wiki/Cell_nucleus
#  Bruce Alberts, Alexander Johnson, Julian Lewis, Martin Raff, Keith Roberts,
#  Peter Walter, ed. (2002). Molecular Biology of the Cell, Chapter 4, pages
#  191-234 (4th ed.). Garland Science.

####################### mESC

# Mouse embryonic stem cell in 2i/LIF
# Source: Pagliara et al, 'Auxetic nuclei in embryonic stem cells
# exiting pluripotency', Nature Methods, 2014
# Measured single cell using confocal microscopy and modeled as ellipsoid
NV_MUS_MUSCULUS_ESC_2LI_LIF = 857.0

# Documenting nuclear diameter used for modeling mESC (3.5 um) in:
# Chiariello, Andrea M., et al. "Polymer physics of chromosome large-scale
# 3D organisation." Scientific Reports 6 (2016).
NV_MUS_MUSCULUS_ESC_CHIARIELLO = (1.0/6.0)*math.pi*((3.5)**3.0)

# mESC nuclear volume based on Genome Architecture Mapping (GAMS) which
# gives an average mESC nuclear radius of 4.5 micrometers (um), see
# supplement page 5: Beagrie, Robert A., Antonio Scialdone, Markus
# Schueler, Dorothee CA Kraemer, Mita Chotalia, Sheila Q. Xie, Mariano
# Barbieri et al. "Complex multi-enhancer contacts captured by genome
# architecture mapping." Nature 543, no. 7646 (2017): 519-524.
NV_MUS_MUSCULUS_ESC_BEAGRIE = (4.0/3.0)*math.pi*((4.5)**3)

####################### K562 and/or CML

# K562 is a CML cell line where the cells resemble both undifferentiated
# granulocytes and erythrocytes.
# https://en.wikipedia.org/wiki/K562_cells 

# Smetana, K., et al. "A short note on the nuclear diameter in human early
# granulocytic progenitors." Hematology 11.5-6 (2006): 399-401.
# The reported diameters are in the 11-13 micron range. Therefore, the
# average is approximately a 12 micron diameter. This study did not
# explicitly use the K562 cell line but the cells measured are progenitor
# cells from the bone marrow of chronic CML patients.
NV_CML_GRANULOCYTE_PROGENITORS_SMETANA = (1.0/6.0)*math.pi*(12.0**3.0)

# K562 nuclear diameters reported in:
# Smetana, K., et al. "A karyometric study on ageing and butyrate or
# imatinib treated human leukemic myeloblasts represented by K562 cells
# originated from chronic myeloid leukaemia." Neoplasma 54.5 (2006):
# 359-364.
# Am averaging reported diameters of 14.6 and 13.6 microns
NV_K562_GRANULOCYTE_PROGENITORS_SMETANA = (1.0/6.0)*math.pi*((0.5*(14.6+13.6))**3.0)

####################### Misc mammalian from Sanborn et al

# From Lieberman-Aiden lab paper:
# Sanborn, Adrian L., et al. "Chromatin extrusion explains key features of
# loop and domain formation in wild-type and engineered genomes."
# Proceedings of the National Academy of Sciences 112.47 (2015):
# E6456-E6465.

# GM12878 237 +- 84 cubic microns
# B-cell lymphocyte/lymphoblast
NV_GM12878_SANBORN = 237.0

# HMEC 728 +- 307 cubic microns
# Human mammary epithelial cells
NV_HMEC_SANBORN = 728.0

# IMR90 381 +- 157 cubic microns
# Fetal lung myofibrolasts
NV_IMR90_SANBORN = 381.0

# NHEK 440 +- 90 cubic microns
# Normal human epidermal keratinocytes
NV_NHEK_SANBORN = 440.0

# Preset mapping for nuclear volumes
NV = { \
    "NV_CML_GRANULOCYTE_PROGENITORS_SMETANA" : NV_CML_GRANULOCYTE_PROGENITORS_SMETANA,
    "NV_GM12878_SANBORN" : NV_GM12878_SANBORN,
    "NV_HMEC_SANBORN" : NV_HMEC_SANBORN,
    "NV_IMR90_SANBORN" : NV_IMR90_SANBORN,
    "NV_K562_GRANULOCYTE_PROGENITORS_SMETANA" : NV_K562_GRANULOCYTE_PROGENITORS_SMETANA,
    "NV_MUS_MUSCULUS_ESC_2LI_LIF" : NV_MUS_MUSCULUS_ESC_2LI_LIF,
    "NV_MUS_MUSCULUS_ESC_CHIARIELLO" : NV_MUS_MUSCULUS_ESC_CHIARIELLO,
    "NV_MUS_MUSCULUS_ESC_BEAGRIE" : NV_NHEK_SANBORN,
    "NV_NHEK_SANBORN" : NV_NHEK_SANBORN}

###########################################
# Fiber unit density in base pairs
###########################################

####################### 30 nm fiber

# From:
# Xu, Gursoy et al,
# "Three-dimensional Modeling of Chromsome Conformation Capture Data
#  Characterizes the Differential Gene Activation in Diffent Cell Lines"
#
# Data is from Supplementary Information,
# section 4.2.1 'Physical properties of chromatin fiber' which states
# a 30 nm chromatin fiber has a mass density of 11 nm/kb. This value is
# from Dekker, 'Mapping in vivo chromatin interactions in yeast suggests an
# extended chromatin fiber with regional variation in compaction'.
# This equates to approximately 2727 base pairs.
# Assuming 30-nm monomer modeled as a sphere with mass density 11 nm/kb
FBP_30_NM_YEAST_DEKKER = 2727

# Value given by Gamze Gursoy (from discussions with biologists)
FBP_30_NM_YEAST_GURSOY = 3000

####################### 11 nm fiber Dekker

# From:
# "Walhout, Marian, Marc Vidal, and Job Dekker, eds. Handbook of systems
# biology: concepts and insights. Academic Press, 2012."
# Chapter 7 - The Spatial Architecture of Chromosomes, pg 140
# which states beads-on-string chromatin fiber has a mass density of ~15 bp
# per nm which is equivalent to ~165 bp per 11 nm length.
# This is reasonable assuming the following:
#   - Nucleosome (histone octamer) is typically modeled as 10 nm diameter
#     disk that is approx 5-6 nm long. There are 146 +/- 1 bp wrapped
#     around histone core. If H1 is present (lets assume that for stem
#     cells H1 is generally not present), then an additional ~20 bp is also
#     attached to the nucleosome (giving ~166 bp per nucleosome)
#   - With the additional wrapped DNA the nucleosome disk diameter is
#     approximately 11 nm
#   - In addition there is linker DNA, typically B-form such that the
#     nucleotide repeat length is ~200 bp for mammalian cells - see
#     [Van Holde, Kensal E. "Chromatin." Springer series in molecular
#     biology.] for some of these numbers and dimensions. The linker DNA is
#     assumed to be unfolded which means 1 bp is 2 nm wide with linear
#     length ~0.34 nm (textbook values for B-form DNA based on crystal
#     structures given by R. Franklin, Watson & Crick). So linker DNA is
#     approx 2-nm fiber with 1 bp per 0.34 nm mass density.
#   - Hence, a compact length of 11 nm fiber with two nucleosomes directly
#     adjancent to each other:
#
#       /----5.5 nm----\  /----5.5 nm----\ 
#       | Nucleosome   |  | Nucleosome   |
#       |              |  |              |
#      11 nm           | 11 nm           |
#       |     A        |  |     A        |
#       |              |  |              |
#       \----5.5 nm----/  \----5.5 nm----/
#
#     would have a linear mass density of ~30 bp/nm = 300 bp / 11 nm
#   - Therefore, this value is somewhere between stacked linear segments
#     of nucleosomes and (nucleosomes + unfolded linker DNA).
#   - Furthermore, a homeogeneous sphere (rather than cylindrical disks)
#     are reasonable for approximating the underlying heterogenous
#     fiber which alternates crudely between 11 nm disks and 2 nm disks.
#
# UPDATE - April 4th, 2018 - THIS FIBER IS MORE DENSE THAN 30 NM FIBER!
# ACCORDING TO FOLLOWING:
#
#   BP_30 = 2727 bp
#   VOL_30 = SphereVol(diam=30 nm) = (1/6)*pi*(30^3) = 1.4137E04 nm^3
#   DENSITY_30 = BP_30 / VOL_30 = 0.1929 bp/nm^3
#
#   BP_11 = 165 bp
#   VOL_11 = SphereVol(diam=30 nm) = (1/6)*pi*(11^3) = 696.9100 nm^3
#   DENSITY_11 = BP_11 / VOL_11 = 0.2368 bp/nm^3
#
#   => DENSITY_30 < DENSITY_11
#   IT COULD ALSO BE THAT THERE IS TOO MUCH ERROR IN THE 30 NM FIBER
#   DENSITY AND THAT THE 30 NM FIBER IS INFLATING EFFECT OF EXCLUDED
#   VOLUME.
FBP_11_NM_DEKKER = 165

####################### 11 nm fiber Rippe

# Alternate 11 nm model
#   Rippe, Karsten, ed. Genome organization and function in the cell nucleus.
#   John Wiley & Sons, 2012 (pg. 424) 
#
# 0.5 nucleosomes are present per linear 11 nm of the beads on string chain
#
# This number is in range of the recent experimental work in
#   Ricci, Maria Aurelia, et al. "Chromatin fibers are formed by heterogeneous
#   groups of nucleosomes in vivo." Cell 160.6 (2015): 1145-1158.
# which in Figure 3E,F gives nucleosome densities in stem cells "clutches"
# that are in this ballpark. They also agree with notion that nucleosome
# fluorescence signals are dimmer for "ground state" cells implying that
# there are more nucleosome depleted regions.
#
# More and more I am of the opinion that stem cells are best modeled as
# unfolded chromatin with relatively low nucleosomal density. For a
# homogeneous model, then a low nucleosome density makes sense. However,
# this may inflate the effect of excluded volume. Therefore, an alternative
# semi-isotropic model which represents nucleosomes (without H1) as
# 11 nm, 146 bp spheres followed by approx. five 3.4 nm, 10 bp spheres for
# the linker DNA. This heterogeneous model is currently not implemented in
# this script but is documented here for possible future use.
NUCLEOSOMES_PER_11_NM = 0.5

# Assume nucleosome repeat length (NRL) of ~200 bp 
# 1. Van Holde, Kensal E. "Chromatin." Springer series in molecular
#   biology.
# 2. Maeshima, Kazuhiro, et al. "Liquid-like behavior of chromatin." Current
#   opinion in genetics & development 37 (2016): 36-45.
# 3. Rippe, Karsten, ed. Genome organization and function in the cell nucleus.
#   John Wiley & Sons, 2012 (pg. 418) 
# Note, (NRL) = nucleosome (~147 bp) + linker DNA. Typically if H1 linker
# histone is present an additional ~20 bp is associated with the
# nucleosome.
# From Rippe text:
# The nucleosome consists of 145 to 147 bp of DNA wrapped around a histone
# octamer protein core. It has a cylindrical shape of 11 nm diameter and
# 5.5 nm height. The nucleosome repeat length (NRL)varies between 165 and
# 220 bp of DNA depending on the species and also on the cell type within
# a given organism. It amounts to about 200 bp in mammals.
NUCLEOSOME_REPEAT_LENGTH_BP = 200

# This estimate is based on the nucleosome density given in Rippe text. If
# we multply the nucleosome density by a nucleosome repeat length, we can
# estimate the monomer length in base pairs for an 11 nm monomer sphere.
FBP_11_NM_RIPPE = NUCLEOSOMES_PER_11_NM * NUCLEOSOME_REPEAT_LENGTH_BP

# Preset mapping for fiber unit density in base pairs
FBP = { \
    "FBP_11_NM_DEKKER" : FBP_11_NM_DEKKER,
    "FBP_11_NM_RIPPE" : FBP_11_NM_RIPPE,
    "FBP_30_NM_YEAST_DEKKER" : FBP_30_NM_YEAST_DEKKER,
    "FBP_30_NM_YEAST_GAMZE" : FBP_30_NM_YEAST_GURSOY}

###########################################
# Fiber unit length in Angstroms
###########################################

# 11 nm fiber
FL_11_NM = 110

# 30 nm fiber (never observed in vivo)
FL_30_NM = 300

# Preset mapping for fiber unit length
FL = { \
    "FL_11_NM" : FL_11_NM,
    "FL_30_NM" : FL_30_NM}

###########################################
# Default arguments
###########################################

DEFAULT_NV = NV_GM12878_SANBORN
DEFAULT_GBP = GBP_HG19_DIPLOID
DEFAULT_LBP = 100000
DEFAULT_MBP = 5000
DEFAULT_FBP = FBP_11_NM_RIPPE
DEFAULT_FL = FL_11_NM

###########################################
# Scale nucleus
###########################################

# @param arg - Argument to resolve
# @param presets - Dictionary of preset values
def resolve(arg, presets):
    '''Resolves argument as preset key or actual user value'''
    return presets.get(str(arg), arg)

# @param loc_bp_scalar - Locus length in base pairs
# @parm mon_bp_scalar - Monomer bead length in base pairs
def get_mon_num_homg(loc_bp_scalar, mon_bp_scalar):
    '''Get scalar number of monomers needed to simulate homogeneous locus'''
    return math.ceil(float(loc_bp_scalar) / mon_bp_scalar)

# https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
def is_close(a, b, rel_tol=1e-09, abs_tol=0.0):
    '''Return True if floats are within a tolerance'''
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

# @param v - Variable value
# @param vname - Variable name string
# @param allowed -Tuple of allowed types for v
def on_type_mismatch(v, vname, allowed=(int, float, str, list, np.ndarray)):
    '''Inform user of type mismatch and exit'''
    allowed_str = map(str, allowed)
    print 'Error: Type mismatch (var, type):'
    print '(' + vname + ', ' + str(type(v)) + ')'
    print 'Type must be one of:'
    print allowed_str
    sys.exit(1)

# Converts user specification into most general specification:
#   multiple loci, heterogeneous monomers
# Note: single locus, homogeneous monomers format is just a special case of
#   this general format!
# @param loc_bp_arg - Loci length(s) in base pairs, may be of type scalar (int,
#   float, str for single-locus simulation) or vector(list or numpy array for
#   multi-locus simulation)
# @param mon_bp_arg - Target monomer length(s) in base pairs, may be of type
#   scalar (int, float, str) or vector (list or numpy array)
# @param fib_bp_arg - Fiber unit density in base pairs, may be of type scalar
#   (int, float, str) or vector (list or numpy array)
# @param fib_len_arg - Fiber unit length in Angstroms, may be of type scalar
#   (int, float, str) or vector (list or numpy array)
# @return Named tuple of type MlocHetrArg
def as_mloc_hetr(loc_bp_arg, mon_bp_arg, fib_bp_arg, fib_len_arg):
    '''Converts argumets to multiple-loci, heterogeneous monomers format'''
    ###########################################
    # Convert loc_bp_arg to 'mloc' np.ndarray
    loc_bp_arr = np.array([])
    if isinstance(loc_bp_arg, (int, float, str)):
        loc_bp_scalar = float(loc_bp_arg)
        assert loc_bp_scalar > 0.0
        loc_bp_arr = np.array([loc_bp_scalar])
    elif isinstance(loc_bp_arg, (list, np.ndarray)):
        loc_bp_arr = map(float, loc_bp_arg)
        loc_bp_arr = np.array(loc_bp_arr)
        assert np.all(loc_bp_arr > 0.0)
    else:
        on_type_mismatch(loc_bp_arg, 'loc_bp')
    ###########################################
    # Convert mon_bp_arg to 'hetr' np.ndarray
    mon_bp_arr = np.array([])
    mon2loc = np.array([])
    if isinstance(mon_bp_arg, (int, float, str)):
        mon_bp_scalar = float(mon_bp_arg)
        assert mon_bp_scalar > 0.0
        for loc_i, loc_bp_scalar in enumerate(loc_bp_arr):
            mon_num = get_mon_num_homg(loc_bp_scalar=loc_bp_scalar,
                                       mon_bp_scalar=mon_bp_scalar)
            mon_bp_arr = np.append(mon_bp_arr,
                                   np.repeat(mon_bp_scalar, mon_num))
            mon2loc = np.append(mon2loc,
                                np.repeat(loc_i, mon_num))
    elif isinstance(mon_bp_arg, (list, np.ndarray)):
        mon_bp_arr = map(float, mon_bp_arg)
        mon_bp_arr = np.array(mon_bp_arr)
        assert np.all(mon_bp_arr > 0.0)
        mon2loc = np.repeat(int(0), len(mon_bp_arr))
        # Verify monomer base pair sums match locus base pair sizes
        bp_sum = 0.0
        loc_i = int(0)
        for mon_i, mon_bp_scalar in enumerate(mon_bp_arr):
            mon2loc[mon_i] = int(loc_i)
            if loc_i >= len(loc_bp_arr):
                print "Size mismatch for mon_bp to loc_bp."
                sys.exit(1)
            bp_sum = bp_sum + mon_bp_scalar
            if is_close(bp_sum, loc_bp_arr[loc_i]):
                bp_sum = 0.0
                loc_i = loc_i + 1
            elif bp_sum > loc_bp_arr[loc_i]:
                print "Size mismatch for mon_bp to loc_bp."
                sys.exit(1)
        if loc_i != len(loc_bp_arr):
            print "Size mismatch for mon_bp to loc_bp."
            sys.exit(1)
    else:
        on_type_mismatch(mon_bp_arg, 'mon_bp')
    ###########################################
    # Convert fib_bp_arg to 'hetr' np.ndarray
    fib_bp_arr = np.array([])
    if isinstance(fib_bp_arg, (int, float, str)):
        fib_bp_scalar = float(resolve(fib_bp_arg, FBP))
        assert fib_bp_scalar > 0.0
        fib_bp_arr = np.repeat(fib_bp_scalar, len(mon_bp_arr))
    elif isinstance(fib_bp_arg, (list, np.ndarray)):
        assert len(fib_bp_arg) == len(mon_bp_arr)
        fib_bp_arr = [resolve(fib_bp_scalar, FBP) \
                        for fib_bp_scalar in fib_bp_arg]
        fib_bp_arr = np.array(map(float, fib_bp_arr))
        assert np.all(fib_bp_arr > 0.0)
    else:
        on_type_mismatch(fib_bp_arg, 'fib_bp')
    ###########################################
    # Convert fib_len_arg to 'hetr' np.ndarray
    fib_len_arr = np.array([])
    if isinstance(fib_len_arg, (int, float, str)):
        fib_len_scalar = float(resolve(fib_len_arg, FBP))
        assert fib_len_scalar > 0.0
        fib_len_arr = np.repeat(fib_len_scalar, len(mon_bp_arr))
    elif isinstance(fib_len_arg, (list, np.ndarray)):
        assert len(fib_len_arg) == len(mon_bp_arr)
        fib_len_arr = [resolve(fib_len_scalar, FL) \
                        for fib_len_scalar in fib_len_arg]
        fib_len_arr = np.array(map(float, fib_len_arr))
        assert np.all(fib_len_arr > 0.0)
    else:
        on_type_mismatch(fib_len_arg, 'fib_len')
    # Assert parallel arrays are same size
    assert len(mon_bp_arr) > 0
    assert len(mon_bp_arr) == len(mon2loc)
    assert len(mon_bp_arr) == len(fib_bp_arr)
    assert len(mon_bp_arr) == len(fib_len_arr)
    return MlocHetrArg(loc_bp=loc_bp_arr, mon_bp=mon_bp_arr, mon2loc=mon2loc,
                       fib_bp=fib_bp_arr, fib_len=fib_len_arr)

# @param diameter - Sphere diameter
def diam2vol(diam):
    '''Return sphere volume from diameter'''
    return (1.0/6.0)*math.pi*(diam**3)

# @param vol - Sphere volume
def vol2diam(vol):
    '''Return sphere diameter from volume'''
    return (6.0 * vol / math.pi)**(1.0/3.0)

# @param length - Linear length in microns
def micro2ang(length):
    '''Return Angstroms from microns'''
    return 10000.0 * length

# @para length - Linear length in Angstroms
def ang2micro(length):
    '''Return microns from Angstroms'''
    return length / 10000.0

# @param nuc_vol - Observed nuclear volume in microns
# @param gen_bp - Whole genome length (including multiplicity) in base pairs
# @param loc_bp - Loci length(s) in base pairs, may be of type scalar (int,
#   float, str) for single-locus simulation or vector(list or numpy array) for
#   multi-locus simulation
# @param mon_bp - Target monomer length(s) in base pairs, may be of type
#   scalar (int, float, str) or vector (list or numpy array)
# @param fib_bp - Fiber unit base pair density, may be of type
#   scalar (int, float, str) or vector (list or numpy array)
# @param fib_len - Fiber unit length in Angstroms, may be of type
#   scalar (int, float, str) or vector (list or numpy array)
# @return ScaledParam tuple
def scale_nucleus(nuc_vol, gen_bp, loc_bp, mon_bp, fib_bp=FBP_11_NM_RIPPE,
                  fib_len=FL_11_NM):
    '''Compute scaled chromatin folding simulation parameters'''
    # Resolve arguments
    nuc_vol = float(resolve(nuc_vol, NV))
    assert nuc_vol > 0.0
    gen_bp = float(resolve(gen_bp, GBP))
    assert gen_bp > 0.0
    mloc_hetr = as_mloc_hetr(loc_bp_arg=loc_bp, mon_bp_arg=mon_bp,
                             fib_bp_arg=fib_bp, fib_len_arg=fib_len)
    fib_vol = map(diam2vol, mloc_hetr.fib_len)
    fib_density = mloc_hetr.fib_bp / fib_vol
    mon_vol = mloc_hetr.mon_bp / fib_density
    # Compute monomer linear diameter in Angstroms
    mon_diam = np.array(map(vol2diam, mon_vol))
    # Determine number of monomers needed to model each loci
    unique, counts = np.unique(mloc_hetr.mon2loc, return_counts=True)
    mon_num = np.array(counts)
    assert len(mon_num) == len(mloc_hetr.loc_bp)
    # Account for padding in locus length attributed to requiring integer
    # number of monomers
    scaled_loc_bp = np.sum(mloc_hetr.mon_bp)
    # Scale down nuclear volume to be proportional to the loci genomic size
    scaled_nuc_vol_micro = (scaled_loc_bp / gen_bp) * nuc_vol
    scaled_nuc_diam_ang = micro2ang(vol2diam(scaled_nuc_vol_micro))
    return ScaledParam(nuc_diam=scaled_nuc_diam_ang, mon_diam=mon_diam,
                       mon_num=mon_num, mon_bp=mloc_hetr.mon_bp,
                       mon2loc=mloc_hetr.mon2loc)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================== scale_nucleus ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-nv', '--nuc_vol', default=DEFAULT_NV,
                          help='Unscaled nuclear volume in microns (+real)')
    parser.add_argument('-gbp', '--gen_bp', default=DEFAULT_GBP,
                          help='Whole genome length in base pairs (+real)')
    parser.add_argument('-lbp', '--loc_bp', default=DEFAULT_LBP, nargs='+',
                          help='Total loci length in base pairs (+real)')
    parser.add_argument('-mbp', '--mon_bp', default=DEFAULT_MBP, nargs='+',
                          help='Target monomer unit density in base pairs')
    parser.add_argument('-fbp', '--fib_bp', default=DEFAULT_FBP, nargs='+',
                        help='Intrinsic fiber unit density in base pairs')
    parser.add_argument('-fl', '--fib_len', default=DEFAULT_FL, nargs='+',
                        help='Intrinsic fiber unit length in Angstroms')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '[--nuc_vol] (microns^3) ='
    print str(args.nuc_vol)
    print '[--gen_bp] (base pairs) ='
    print str(args.gen_bp)
    print '[--loc_bp] (base pairs) ='
    print str(args.loc_bp)
    print '[--mon_bp] (base pairs) ='
    print str(args.mon_bp)
    print '[--fib_bp] (base pairs) ='
    print str(args.fib_bp)
    print '[--fib_len] (Angstroms) ='
    print str(args.fib_len)
    # Feed to utility
    params = scale_nucleus(nuc_vol=args.nuc_vol, gen_bp=args.gen_bp,
                           loc_bp=args.loc_bp, mon_bp=args.mon_bp,
                           fib_bp=args.fib_bp, fib_len=args.fib_len)
    # Report results
    nuc_vol = float(resolve(args.nuc_vol, NV))
    gen_bp = resolve(args.gen_bp, GBP)
    print "Original nuclear volume (microns^3):"
    print str(nuc_vol)
    print "Original nuclear diameter (microns):"
    print str(vol2diam(nuc_vol))
    print "Total genomic length (base pairs):"
    print str(gen_bp)
    print "Loci genomic length (base pairs):"
    print str(args.loc_bp)
    print "Loci genomic length monomer padded (base pairs):"
    print str(np.sum(params.mon_bp))
    print "Scaled nuclear volume (microns^3):"
    print str(diam2vol(ang2micro(params.nuc_diam)))
    print "Scaled nuclear volume (Angstroms^3):"
    print str(diam2vol(params.nuc_diam))
    print "Scaled nuclear diameter (Angstroms):"
    print str(params.nuc_diam)
    print "Monomer diameters (Angstroms):"
    print str(params.mon_diam)
    print "Monomer genomic lengths (base pairs):"
    print str(params.mon_bp)
    print "Number of monomers for each locus:"
    print str(params.mon_num)
    print "Monomer locus map:"
    print str(params.mon2loc)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
