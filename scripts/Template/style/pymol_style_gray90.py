# Usage: copy and paste into pymol

# Color everything same
color gray90

hide everything
show spheres

# Set background to white
bg_color white

# Turn on ray trace antiliasing
set antialias, 8

# Turn off shadows
set ray_shadows, 0

# Orthoscopic images are 2-4x faster than perspective
set ray_orthoscopic, on

# Normal color
set ray_trace_mode, 0

# Render image!
ray
