# Usage: copy and paste into pymol

# Color everything same
color gray90

hide everything
show sticks
set stick_radius=1.25

# Set background to white
bg_color white

# Turn on ray trace antiliasing
set antialias, 8

# Turn off shadows
set ray_shadows, 0

# Make transparent background
set ray_opaque_background, 0

# Orthoscopic images are 2-4x faster than perspective
#set ray_orthoscopic, on

# Normal color
set ray_trace_mode, 1

# Additional color schemes
# https://pymolwiki.org/index.php/Spectrum
#
# Carnival/Mardi Gras style rainbow:
#spectrum count, rainbow_cycle

# Smooth but constrating green, white, and blue
#spectrum count, green_white_blue

# Render image!
ray
