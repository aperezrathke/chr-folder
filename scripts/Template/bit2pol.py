#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script generates 3-D polymers from population bit matrix of
#   chromatin-to-chromatin interaction states
#
# -----------------------------------------------------------------------------
# Arguments
# -----------------------------------------------------------------------------
# Script has the following non-optional arguments: (short-hand|long-hand)
#
#   -bit|--bitpop: Path to input bit matrix frame (default column major, see
#       -row|--rowmaj option for when samples encoded in rows)
#   -cmd|--cmd_key: Model command key passed to the 'main' dispatch utility
#   -cnf|--conf: Path to top-level model INI configuration
#   -i|--intr <path>: Input file OR directory. File(s) contain significant
#       chromatin-to-chromatin proximity interactions encoded as CSV pairs of
#       0-based fragment identifiers. If directory, the listed files will be
#       uniform randomly sampled as input for each batch job; all files
#       assumed to have SAME NUMBER OF INPUT CONSTRAINTS!
#
# The following arguments are optional: (short-hand|long-hand)
#
#   [-arch|--arch_conf]: Overloads archetype configuration INI path
#   [-dki|--dist_ki] <+real>: [Optional] Scalar knock-in distance in Angstroms.
#       A knock-in constraint is satisfied if at least one monomer node from
#       each fragment region is within this distance. Ignored if knock-in
#       distance file is present, except for ligc export which always defers
#       to scalar for computing ligated contacts. May also be specified as:
#       'chr_knock_in_dist = <+real>' within 'conf' INI; the folder binary
#       also has a hard-coded default if not explicitly set by user.
#   [-dkif|--dist_ki_file] <path>: [Optional] File with knock-in distance
#       thresholds (in Angstroms) for each proximity interaction. May be also
#       be specified as 'chr_knock_in_dist_fpath = <path>" within 'conf' INI.
#   [-dko|--dist_ko] <+real>: [Optional] Scalar knock-out distance in
#       Angstroms. A knock-out constraint is satisfied if all monomer nodes
#       from each fragment region are greater than this distance from each
#       other. Ignored if knock-out distance file is present. May be specified
#       as 'chr_knock_out_dist = <+real>' within 'conf' INI; the folder binary
#       also has a hard-coded default if not explicitly set by user.
#   [-dkof|--dist_ko_file] <path>: [Optional] File with knock-out distance
#       thresholds (in Angstroms) for each proximity interaction. May be
#       specified as 'chr_knock_out_dist_fpath = <path>' within 'conf' INI.
#   [-e|--exe] <path>: [Optional] Path to executable folder binary with 'main'
#       dispatch utility, defaults to:
#           <SCRIPT_DIR>/../../output/<build>/bin/Release (serial executable)
#   [-esz|--ens_size] <+integer>: [Optional] Output ensemble size for each
#       chromatin-to-chromatin proximity configuration, default is 1
#   [-f|--frag] <path>: [Optional] Input file mapping fragment regions (e.g
#       Hi-C bins) to polymer node spans. Must be specified as command line
#       argument or as "chr_frag = <path>" tuple within 'conf' INI; if not
#       specified, then will default to treating each monomer as individual
#       fragment.
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-jpre|--job_prefix] <string>: [Optional] Output 3-D polymer files will
#       start with this prefix. Will default to base name of <--bitpop>
#   [-kor|--ko_rand]: [Optional] Switch if present will treat '0's as random
#       folding instead of constrained knock-outs, see also --no-ko_rand
#   [-no-kor|--no-ko_rand]: [Optional] Switch if present will enforce '0's as
#       constrained knock-outs, see also --ko_rand
#   [-lgp|--log_prefix]: [Optional] Path to log file prefix (default is None);
#       will create log file <log_prefix>.<job_id> for each batch job if
#       specified. WILL NOT CREATE ANY NEEDED PATH DIRECTORIES!
#   [-mf|--max_fail] <integer>: [Optional] Maximum number of failures allowed
#       for a single job configuration. If a job error code is encountered
#       more than this threshold, the job command line is reported and program
#       terminates
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes to use for generating each ensemble. Note:
#           <--max_proc> * <--max_thread> should not exceed CPU logical cores
#   [-mt|--max_trial] <+integer>: [Optional] Number of trials to attempt
#       folding a given proximity constraint configuration before giving up
#   [-mth|--max_thread] <+integer>: [Optional] Maximum number of worker
#       threads per process, default is 1. Has no effect for serial EXE. Note:
#           <--max_proc> * <--max_thread> should not exceed CPU logical cores
#   [-np|--num_proc] <+integer>: [Optional] Number of processes per
#       chromatin-to-chromatin bit configuration
#   [-od|--outdir]: [Optional] Path to output folder, default is to same
#       directory containing input bit population matrix
#   [-row|--rowmaj]: [Optional] Switch if present will treat rows of bit
#       population matrix as containing the individual samples
#   [-ssp|--sleep_sec_poll] <+real>: [Optional] Number of seconds to wait
#       before polling active folding simulations for completion
#   [-xcsv|--xport_csv] <-1|0|1>: [Optional] 0: disable, 1: enable CSV polymer
#       export. -1: (default) Defer to INI and/or polymer folder defaults
#   [-xcsv_gz|--xport_csv_gz] <-1|0|1>: [Optional] 0: disable, 1: enable gzip
#       compressed CSV polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xintr|--xport_intr] <-1|0|1>: [Optional] 0: disable, 1: enable
#       chromatin-to-chromatin proximity interactions constraints to be
#       exported as PML selections. -1: (default) Defer to INI and/or polymer
#       folder defaults
#   [-xligc_bond|--xport_ligc_bond] <-1|0|1>: [Optional] 0: disable, 1: enable
#       explicit export of (i,i) and (i,i+1) bonded ligations - will increase
#       file size (these ligations always occur!)
#   [-xligc_csv|--xport_ligc_csv] <-1|0|1>: [Optional] 0: disable, 1: enable
#       CSV ligation contact tuple export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xligc_csv_gz|--xport_ligc_csv_gz] <-1|0|1>: [Optional] 0: disable, 1:
#       enable gzip compressed CSV ligation contact tuple export. -1: (default)
#       Defer to INI and/or polymer folder defaults
#   [-xlogw|--xport_logw] <-1|0|1>: [Optional] 0: disable, 1: enable export of
#       population log importance weights. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xpdb|--xport_pdb]: [Optional] 0: disable, 1: enable PDB polymer export.
#       -1: (default) Defer to INI and/or polymer folder defaults
#   [-xpdb_gz|--xport_pdb_gz]: [Optional] 0: disable, 1: enable gzip
#       compressed PDB polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xpml|--xport_pml]: [[Optional] 0: disable, 1: enable PML polymer export.
#       -1: (default) Defer to INI and/or polymer folder defaults
#   [-xpml_gz|--xport_pml_gz]: [[Optional] 0: disable, 1: enable gzip
#       compressed PML polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults

# Note, total number of 3-D samples per bit population matrix is:
#   <num bit samples (e.g num cols if col maj)> * ens_size * num_proc

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For working with bit population
import numpy as np

# For file path testing
import os

# For running external processes
import subprocess

# For exiting if inputs invalid
import sys

# For sleeping until jobs finish
import time

# For keeping track of active jobs
from collections import namedtuple

# For determining number of CPU cores
from multiprocessing import cpu_count

# For determining complement of bit tuple
from operator import sub

# For selecting random interaction configuration
import random

###########################################
# Job descriptor named tuple
###########################################

# Job descriptor
# .proc - Handle to active subprocess
# .cmd - Command line used to (re)launch process
# .num_open - Number of times this job has been created
# .flog - Log file handle
# .has_log - If True, flog handle will be closed on job finish
Job_FIELDS = ['proc', 'cmd', 'num_open', 'flog', 'has_log']
Job = namedtuple("Job", Job_FIELDS)

###########################################
# Global defaults
###########################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Default path to 'folder' project root directory
DEF_ROOT_DIR = os.path.abspath(os.path.join(SCRIPT_DIR, "..", ".."))

# Default path to folder executable binary containing 'main' dispatch, uses the
# serial (non-threaded) binary
DEF_EXE_DIR = os.path.join(DEF_ROOT_DIR, "bin", "Release")
DEF_EXE_PATH = os.path.join(DEF_EXE_DIR, "u_sac")

# Default ensemble size per bit configuration
DEF_ENS_SIZE = 1

# Default number of times a single job may return an abnormal error code. In
# my experience working on public compute clusters (with possibly ancient
# operating systems), background subprocesses may occasionally fail to launch
# and return with a non-zero error code. In this case, we can try to re-launch
# the process a few times to help mitigate this issue. If the subprocess still
# fails with an abnormal error code, then it's likely to be a user
# configuration error causing the folder simulation itself to crash!
DEF_MAX_FAIL = 2

# Default maximum processes is number of CPU cores
DEF_MAX_PROC = cpu_count()

# Default maximum number of trials attempted by 'main' dispatch before
# terminating with no_fold error code
DEF_MAX_TRIAL = 50000

# Default number of threads per process
DEF_MAX_THREAD = 1

# Default number of processes per bet configuration
DEF_NUM_PROC = 1

# Default polling interval in seconds to check for active folding simulations
DEF_SLEEP_SEC_POLL = 1

###########################################
# Global folder simulation return codes
###########################################

# Folder simulation return code for normal termination (fold okay)
RC_NORMAL = 0

# Folder simulation return code for non-specific error
RC_ERROR = 1

# Folder simulation return code for normal termination without fold generated
RC_NO_FOLD = 2

###########################################
# Export state enumeration
###########################################

# Defer to INI or polymer folder defaults
X_INI = str(-1)

# Disable export
X_OFF = str(0)

# Enable export
X_ON = str(1)

# Set of export option choices
X_CHOICES = [X_INI, X_OFF, X_ON]

# Default export
X_DEF = X_INI

###########################################
# Global job queue variables
###########################################

# List of currently running jobs
JOB_QUEUE = []

# List of jobs that failed but that should be re-queued just to be sure
JOB_REQUEUE = []

# @HACK - Global job failure threshold (ideally should be passed to all
#   functions which need it)
MAX_FAIL = DEF_MAX_FAIL

# Sleep seconds to wait before re-queue
# @TODO - make user arg
SLEEP_SEC_FAIL = 5

# Sleep seconds between job queues
# @TODO - make user arg
#SLEEP_SEC_QUEUE = 1

###########################################
# Error handling
###########################################

# Exit program with error message if condition is true
# @param b_yes - If True, program is terminated
# @param msg - [Optional] error message to display prior to exit
# @param code - [Optional] error code to return on exit, default is 1
def exit_if(b_yes, msg="", code=1):
    '''If condition met, prints error message and exits program'''
    if b_yes:
        if msg:
            print "ERROR: " + msg
        print "EXITING (" + str(code) + ")"
        sys.exit(code)

###########################################
# File tools
###########################################

# Recursively creates build directory such that all parent directories are
# created if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
    '''Utility for making a directory if not existing.'''
    if not os.path.exists(d):
        os.makedirs(d)

###########################################
# File parsing
###########################################

# Each non-empty, non-comment line in parameter file is considered a
# chromatin-to-chromatin interaction. Exits program if 0 interactions found!
# @param fpath - Path to chromatin-to-chromatin proximity interactions file
# @return number of chromatin-to-chromatin interactions specified in file
def get_num_intr(fpath):
    '''Determines total number of proximity interactions'''
    num_intr = 0
    with open(fpath) as fin:
        for line in fin:
            line = line.strip()
            # Ignore empty (all whitespace) lines
            if line:
                # Ignore comment lines
                if not line.startswith('#'):
                    num_intr += 1
    # Terminate if no interactions found!
    exit_if(num_intr <= 0,
            msg="0 interactions found in file: " + fpath)
    return num_intr

# @param fpath - Path to plain-text bit-matrix, only 0|1 digits are retained
# @param rowmaj - If True, then samples are assumed to be in rows
# @param num_intr - Number of expected interactions per sample
# @return list where each element is a bit vector (tuple) representing a
#   sample from the bit population
def load_bitpop(fpath, rowmaj, num_intr):
    '''Load bit population matrix'''
    # Though numpy has its own loading utilities, this method will work on
    # practically any plain text format with any mixture of delimiter(s)
    bitpop = []
    with open(fpath) as fbit:
        for line in fbit:
            line = line.strip()
            # Ignore empty (all whitespace) lines
            if not line:
                continue
            # Ignore comment lines
            if line.startswith('#'):
                continue
            bits = tuple([int(ch) for ch in line if ch in ['0', '1']])
            # Ignore lines with missing bits
            if not bits:
                continue
            bitpop.append(bits)
    # Check if bits read
    exit_if(len(bitpop) == 0, msg="Unable to load bit population matrix")
    # Check all row entries have same size
    bitmat = np.array(bitpop)
    exit_if(len(bitmat.shape) != 2,
            msg="Row size mismatch in bit population matrix")
    # Transpose bit population matrix if not row major
    if not rowmaj:
        bitmat = bitmat.transpose()
        # Convert back to list of tuples
        bitpop = list(map(tuple, bitmat))
    # Check bit samples are expected size (number of interactions)
    exit_if(bitmat.shape[1] != num_intr,
            msg="Interaction size mismatch in bit population matrix")
    return bitpop

###########################################
# Argument tools
###########################################

# @param intr_path - Path to interactions file or directory with compatibly
#   formatted (e.g same number of interactions) interactions files
def get_intr_list(intr_path):
    '''Obtain list of candidate files to pass as interaction constraints'''
    # https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
    if os.path.isdir(intr_path):
        out = []
        fls = os.listdir(intr_path)
        for f in fls:
            fpath = os.path.join(intr_path, f)
            if os.path.isfile(fpath):
                out.append(fpath)
        return out
    assert os.path.isfile(intr_path)
    return [intr_path]

# Determines C++ bit mask argument
# @param bit_tup - tuple where each element is 1 or 0
# @param cpparg_key - C++ argument key
# @return List with C++ argument (key, val) tuple
def resolve_bit_mask_arg(bit_tup, cpparg_key):
    '''Converts bit tuples into a bit string C++ argument'''
    bit_str = ''.join(map(str, bit_tup))
    return [cpparg_key, bit_str]

# Determines proper C++ distance argument mapping from python arguments
# @param cppargs_out - output C++ args - may be modified (passed by reference)
# @param pyarg_scalar_val - Python scalar distance argument value (may be None)
# @param pyarg_fpath_val - Python file path distance argument value (may be
#   None), if not None, this value will take precedence over scalar
# @param cpparg_scalar_key - C++ argument key for scalar distance
# @param cpparg_fpath_key - C++ argument key for file path distance
# @return cppags_out
def resolve_dist_arg(cppargs_out, pyarg_scalar_val, pyarg_fpath_val,
                     cpparg_scalar_key, cpparg_arg_fpath_key):
    '''Determine C++ distance argument'''
    argtup = None
    if pyarg_fpath_val:
        # Make sure user specified file exists
        exit_if(not os.path.isfile(pyarg_fpath_val),
                msg="Unable to find distance file: " + pyarg_fpath_val)
        argtup = [cpparg_arg_fpath_key, pyarg_fpath_val]
    elif pyarg_scalar_val:
        # Make sure scalar distance is non-negative
        exit_if(float(pyarg_scalar_val) < 0,
                msg="Scalar distance must be non-negative (" +
                str(pyarg_scalar_val) + ")")
        argtup = [cpparg_scalar_key, str(pyarg_scalar_val)]
    if argtup:
        cppargs_out.extend(argtup)
    return cppargs_out

# Determines proper C++ export argument mapping from python argument
# @param cppargs_out - output C++ args - may be modified (passed by reference)
# @param pyarg_val - value of export argument in X_CHOICES
# @param cpparg_key - corresponding C++ argument key
# @return cppargs_out
def resolve_xport_arg(cppargs_out, pyarg_val, cpparg_key):
    '''Determine C++ distance argument'''
    assert pyarg_val in X_CHOICES
    if pyarg_val != X_INI:
        cppargs_out.extend([cpparg_key, str(pyarg_val)])
    return cppargs_out

# @param args - user arguments
# @param intr_paths - list of interaction configuration file paths
# @return Portion of command line folder call invariant to proximity
#   interaction bit masks
def resolve_invar_args(args, intr_paths):
    '''Determines invariant arguments to C++ folder utility'''
    # Exe arg
    cppargs = [args.exe]
    # Dispatch arg
    cppargs.extend(["--main_dispatch", args.cmd_key])
    # Conf arg
    cppargs.extend(["--conf", args.conf])
    # Output directory
    cppargs.extend(["--output_dir", args.outdir])
    # Max trials arg
    cppargs.extend(["--max_trials", str(args.max_trial)])
    # Force top-level ensemble size of 1
    cppargs.extend(["--ensemble_size", str(args.ens_size)])
    # Note - if ensemble size is 1, we really should only be using a single
    #   thread! Has no effect if using a serial executable
    cppargs.extend(["--num_worker_threads", str(args.max_thread)])
    # Archetype conf arg
    if args.arch_conf:
        cppargs.extend(["--arch", args.arch_conf])
    # Frag arg
    if args.frag:
        cppargs.extend(["--chr_frag", args.frag])
    # Knock-in and knock-out args
    if len(intr_paths) == 1:
        # If only single option, set as invariant argument
        cppargs.extend(["--chr_knock_in", intr_paths[0]])
        cppargs.extend(["--chr_knock_out", intr_paths[0]])
    # Knock-in distance(s) arg
    resolve_dist_arg(cppargs, args.dist_ki, args.dist_ki_file,
                     "--chr_knock_in_dist", "--chr_knock_in_dist_fpath")
    # Knock-out distance(s) arg
    resolve_dist_arg(cppargs, args.dist_ko, args.dist_ko_file,
                     "--chr_knock_out_dist", "--chr_knock_out_dist_fpath")
    # CSV export
    resolve_xport_arg(cppargs, args.xport_csv, "--export_csv")
    resolve_xport_arg(cppargs, args.xport_csv_gz, "--export_csv_gz")
    # PML chromatin-to-chromatin proximity interaction selection export
    resolve_xport_arg(cppargs, args.xport_intr, "--export_intr_chr")
    # Ligc bonded export
    resolve_xport_arg(cppargs, args.xport_ligc_bond, "--export_ligc_bonded")
    # Ligc CSV export
    resolve_xport_arg(cppargs, args.xport_ligc_csv, "--export_ligc_csv")
    # Ligc gzip CSV export
    resolve_xport_arg(cppargs, args.xport_ligc_csv_gz, "--export_ligc_csv_gz")
    # Log weights export
    resolve_xport_arg(cppargs, args.xport_logw, "--export_log_weights")
    # PDB export
    resolve_xport_arg(cppargs, args.xport_pdb, "--export_pdb")
    resolve_xport_arg(cppargs, args.xport_pdb_gz, "--export_pdb_gz")
    # PML export
    resolve_xport_arg(cppargs, args.xport_pml, "--export_pml")
    resolve_xport_arg(cppargs, args.xport_pml_gz, "--export_pml_gz")
    # List of invariant arguments
    return cppargs

# Performs cursory argument validation, by no means exhaustive.
# @param args - user arguments
# @return Arguments, some which may have been modified during validation
def check_args(args):
    '''Performs argument validation'''
    # Make sure numeric arguments are properly typed
    assert isinstance(args.ko_rand, bool)
    assert args.xport_csv in X_CHOICES
    assert args.xport_csv_gz in X_CHOICES
    assert args.xport_intr in X_CHOICES
    assert args.xport_ligc_bond in X_CHOICES
    assert args.xport_ligc_csv in X_CHOICES
    assert args.xport_ligc_csv_gz in X_CHOICES
    assert args.xport_logw in X_CHOICES
    assert args.xport_pdb in X_CHOICES
    assert args.xport_pdb_gz in X_CHOICES
    assert args.xport_pml in X_CHOICES
    assert args.xport_pml_gz in X_CHOICES
    args.ens_size = int(args.ens_size)
    args.max_trial = int(args.max_trial)
    args.max_proc = int(args.max_proc)
    args.max_thread = int(args.max_thread)
    args.max_fail = int(args.max_fail)
    args.num_proc = int(args.num_proc)
    args.sleep_sec_poll = float(args.sleep_sec_poll)
    # Check bit-population frame file exists
    exit_if(not os.path.isfile(args.bitpop),
            msg="Invalid bit population file path (--bitpop): " + str(args.bitpop))
    # Check configuration file exists
    exit_if(not os.path.isfile(args.conf),
            msg="Invalid configuration file path (--conf): " + str(args.conf))
    # Check proximity interactions file|directory exists
    exit_if(not os.path.exists(args.intr),
            msg="Invalid proximity interactions file|directory path (--intr): " + str(args.intr))
    # Check executable path exists
    exit_if(not os.path.isfile(args.exe),
            msg="Invalid executable path (--exe): " + str(args.exe))
    # Check positive ensemble size
    exit_if(args.ens_size <= 0,
            msg="Invalid ensemble size (--ens_size), must be > 0")
    # Check positive trial count
    exit_if(args.max_trial <= 0,
            msg="Invalid number of trials per fold attempt (--max_trial), must be > 0")
    # Warn user if 0 or less procs allotted, set to 1
    if args.max_proc <= 0:
        args.max_proc = 1
        print "Warning: allotted parallel processes <= 0, setting to " + \
            str(args.max_proc)
    # Warn user if 0 or less threads per proc allotted, set to 1
    if args.max_thread <= 0:
        args.max_thread = 1
        print "Warning: allotted parallel threads <= 0, setting to " + \
            str(args.max_thread)
    # Warn user if num_proc is less than 1
    if args.num_proc < 1:
        args.num_proc = 1
        print "Warning: number of processes per bit config must be >= 1, setting to " + \
            str(args.num_proc)
    # Warn user if PROC * THREAD > CPU CORES
    num_cpu = cpu_count()
    num_par = args.max_proc * args.max_thread
    if num_par > num_cpu:
        print "Warning: MAX_PROC * MAX_THREAD (" + \
            num_par + ") > CPU CORES (" + num_cpu + ")"
    # Determine if output directory needs to be set
    if not args.outdir:
        args.outdir = os.path.abspath(
            os.path.dirname(os.path.realpath(args.bitpop)))
        print "Output directory set to: " + args.outdir
    # Determine job prefix
    if not args.job_prefix:
        # https://stackoverflow.com/questions/678236/how-to-get-the-filename-without-the-extension-from-a-path-in-python
        args.job_prefix = os.path.basename(args.bitpop)
        args.job_prefix = os.path.splitext(args.job_prefix)[0]
        print "Job prefix set to: " + args.job_prefix
    # Note, this return is not really necessary as args is passed by reference
    return args

###########################################
# Job utilities
###########################################

# Checks return code is not abnormal
# @param job - Completed job tuple
def handle_rc(job):
    '''Handle folding simulation return codes'''
    global MAX_FAIL
    rc = job.proc.returncode
    # Did job terminate abnormally?
    if rc == RC_ERROR:
        # If re-try threshold exceeded, force exit
        exit_if(job.num_open > MAX_FAIL,
                msg="Folding simulation abnormal termination:\n\t" +
                str(job.cmd))
        # Else mark job for re-queue
        JOB_REQUEUE.append(job)
    else:
        # Job completed okay, check if file handle should be closed
        if job.has_log:
            job.flog.close()
        # Check if unable to fold
        if rc == RC_NO_FOLD:
            print "NO FOLD DETECTED:\n\t" + str(job.cmd)

# @param job - Job named tuple
# @return True if job completed, False otherwise
def poll_job(job):
    '''Checks if job is complete'''
    if job.proc.poll() is None:
        # Job not yet complete
        return False
    # Check return code
    handle_rc(job=job)
    # Job completed!
    return True

# @param cmd - Job command line
# @param flog - File handle to redirect stdout/stderr of child sub-processes
def spawn_job(cmd, flog):
    '''Spawns new process and returns handle'''
    # Wait a bit before each job spawn to help ensure different random seeds
    #global SLEEP_SEC_QUEUE
    #time.sleep(SLEEP_SEC_QUEUE)
    # Open new job handle (non-blocking)
    # https://stackoverflow.com/questions/11269575/how-to-hide-output-of-subprocess-in-python-2-7
    return subprocess.Popen(cmd, stdout=flog, stderr=subprocess.STDOUT,
                            shell=False)

# Respawns any jobs that failed and need to be re-tried
def requeue_jobs():
    '''Requeues any jobs that need to be tried again'''
    global JOB_QUEUE
    global JOB_REQUEUE
    global SLEEP_SEC_FAIL
    if len(JOB_REQUEUE) > 0:
        # Wait a bit before re-queueing
        time.sleep(SLEEP_SEC_FAIL)
        for jr in JOB_REQUEUE:
            # Create job process and append to queue
            proc = spawn_job(cmd=jr.cmd, flog=jr.flog)
            job = Job(proc=proc, cmd=jr.cmd, num_open=jr.num_open + 1,
                      flog=jr.flog, has_log=jr.has_log)
            JOB_QUEUE.append(job)
        # Clear requeue
        JOB_REQUEUE = []

# @param cppcall_invar - Invariant arguments to fold simulation
# @param job_prefix - Job prefix for all output data
# @param intr_paths - List of KO/KI configuration paths
# @param ki_bits - Tuple of knock-in bits
# @param ko_bits - Tuple of knock-out bits
# @param max_proc - Maximum number of active folding simulations
# @param sleep_sec_poll - Seconds to wait before polling active jobs
# @param flog - File handle to redirect stdout/stderr of child sub-processes
# @param has_job_log - If True, job is responsible for closing log file
#   handle, else flog is assumed to be 'dev/null' and will *not* be closed on
#   job completion
# @return 1 if job was queued, 0 o/w
def queue_job(cppcall_invar, intr_paths, job_prefix, ki_bits, ko_bits,
              max_proc, sleep_sec_poll, flog, has_job_log):
    '''Queues folding simulation, polls until procs available'''
    global JOB_QUEUE
    # Check if job queue is full
    while (len(JOB_QUEUE) >= max_proc):
        # Wait a bit before checking if jobs have completed
        time.sleep(sleep_sec_poll)
        # Check active jobs for completion
        JOB_QUEUE[:] = [job for job in JOB_QUEUE if not poll_job(job)]
        # Re-try any failed jobs (up to limit)
        requeue_jobs()
    # Resolve job prefix
    cpparg_job_prefix = ["--job_id", str(job_prefix)]
    # Resolve bit mask arguments
    cpparg_ki_bits = resolve_bit_mask_arg(ki_bits, "--chr_knock_in_bit_mask")
    cpparg_ko_bits = resolve_bit_mask_arg(ko_bits, "--chr_knock_out_bit_mask")
    # Shallow copy invariant folder arguments
    cppcall = cppcall_invar[:]
    # Resolve chr_knock_in and chr_knock_out!
    if len(intr_paths) > 1:
        # Multiple interaction configurations, randomly select one
        intr = random.choice(intr_paths)
        cppcall.extend(["--chr_knock_in", intr])
        cppcall.extend(["--chr_knock_out", intr])
    # Finalize folder command line
    cppcall.extend(cpparg_job_prefix)
    cppcall.extend(cpparg_ki_bits)
    cppcall.extend(cpparg_ko_bits)
    # Create job process and append to queue
    proc = spawn_job(cmd=cppcall, flog=flog)
    job = Job(proc=proc, cmd=cppcall, num_open=1, flog=flog,
              has_log=has_job_log)
    JOB_QUEUE.append(job)
    # Signal job was queued
    return 1

# @param corp - Corpus of infeasible constraint configurations, updated if any
#   outstanding folding simulations return with code 'no_fold'
def sync_jobs():
    '''Wait for all outstanding folding simulations to complete'''
    global JOB_QUEUE
    while len(JOB_QUEUE) > 0:
        for job in JOB_QUEUE:
            job.proc.wait()
            # Check return code
            handle_rc(job=job)
        # Clear queue
        JOB_QUEUE = []
        # Re-try any failed jobs (up to limit)
        requeue_jobs()

###########################################
# bit2pol
###########################################

# @param args - user named arguments
def bit2pol(args):
    '''Generates 3-D polymer ensemble from population bit matrix'''
    random.seed()
    # Validate user arguments
    args = check_args(args)

    # Interaction file paths
    intr_paths = get_intr_list(intr_path=args.intr)
    num_intr_config = len(intr_paths)
    exit_if(num_intr_config < 1,
            msg="Unable to detect interaction configuration(s) at " + \
                str(args.intr))
    print "Found " + str(num_intr_config) + " interaction configuration(s)"

    # Resolve invariant C++ arguments
    cppcall_invar = resolve_invar_args(args=args, intr_paths=intr_paths)
    job_prefix_base = args.job_prefix

    # @HACK - update job failure threshold
    global MAX_FAIL
    MAX_FAIL = args.max_fail
    max_proc = int(args.max_proc)
    sleep_sec_poll = float(args.sleep_sec_poll)

    # Conditionally redirect stdout/stderr of child sub-processes
    fnull = None
    has_job_log = True
    if not args.log_prefix:
        fnull = open(os.devnull, 'w')
        has_job_log = False

    # Get number of interactions (from first listed file)
    num_intr = get_num_intr(fpath=intr_paths[0])
    # Load bit population
    bitpop = load_bitpop(fpath=args.bitpop, rowmaj=args.rowmaj,
                         num_intr=num_intr)
    print "Loaded bit population of size " + str(len(bitpop))
    print "Bit samples have " + str(num_intr) + " bits"
    # Tuple of (1, 1 ...) used to determine complement knock-out mask when
    # knock-outs are explicitly modeled (args.ko_rand == False)
    ones = tuple(np.ones((num_intr,), dtype=int))
    # Initialize knock-out bits as Tuple of (0, 0 ...)
    ko_bits = tuple(int(0) for i in xrange(num_intr))

    # Process bit population
    for ix, ki_bits in enumerate(bitpop):
        # Determine complement knock-out bits only if non-random
        if not args.ko_rand:
            # Note, according to timeit profiling, map(sub, ...) operation was
            # slightly faster than np.subtract(...) for subtracting tuples
            # See https://stackoverflow.com/questions/17418108/elegant-way-to-perform-tuple-arithmetic
            ko_bits = tuple(map(sub, ones, ki_bits))
        for ip in xrange(args.num_proc):
            # Job prefix for this bit config
            job_prefix = '.'.join([job_prefix_base, str(ix), str(ip)])
            flog = fnull
            if has_job_log:
                log_path = '.'.join([args.log_prefix,
                                     os.path.basename(job_prefix), 'txt'])
                flog = open(log_path, 'w')
            # Queue job!
            queue_job(cppcall_invar=cppcall_invar,
                      job_prefix=job_prefix,
                      intr_paths=intr_paths,
                      ki_bits=ki_bits,
                      ko_bits=ko_bits,
                      max_proc=max_proc,
                      sleep_sec_poll=sleep_sec_poll,
                      flog=flog,
                      has_job_log=has_job_log)

    # Wait for outstanding jobs to finish
    print "Waiting for any outstanding folding simulations to finish"
    sync_jobs()
    # Close null handle
    if fnull is not None:
        assert not has_job_log
        fnull.close()

###########################################
# Main
###########################################

# @param args - parsed argparse object
def print_cmd(args):
    '''Prints command-line to stdout'''
    for arg in vars(args):
        print "[--" + arg + "]"
        print str(getattr(args, arg))

# Main script entry point
def __main__():
    print "======================= bit2pol ======================="
    logo = r"""
  _    ___        
 | |  |__ \       
 | |__   ) |_ __  
 | '_ \ / /| '_ \ 
 | |_) / /_| |_) |
 |_.__/____| .__/ 
           | |    
           |_|    
"""
    print logo
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Required arguments
    # https://stackoverflow.com/questions/24180527/argparse-required-arguments-listed-under-optional-arguments
    req_args = parser.add_argument_group('required named arguments')
    req_args.add_argument('-bit', '--bitpop', required=True,
                          help='Path to input bit matrix population frame (default column major, see --rowmaj)')
    req_args.add_argument('-cmd', '--cmd_key', required=True,
                          help='Model command key passed to the folding simulation "main" dispatch')
    req_args.add_argument('-cnf', '--conf', required=True,
                          help='Model INI configuration file path passed to the "main" dispatch')
    req_args.add_argument('-i', '--intr', required=True,
                          help='Input file or directory of files with [significant] chromatin-to-chromatin proximity interactions encoded as pairs of 0-based fragment identifiers. If directory, file listing will be uniform randomly sampled as input for each batch job.')
    # Optional arguments
    parser.add_argument('-arch', '--arch_conf', default=None,
                        help='Model INI archetype configuration file path passed to the "main" dispatch')
    parser.add_argument('-dki', '--dist_ki', default=None,
                        help='Scalar knock-in distance in Angstroms, ignored if knock-in distance file is present')
    parser.add_argument('-dkif', '--dist_ki_file', default=None,
                        help='File with knock-in distance thresholds (in Angstroms) for each chromatin-to-chromatin proximity interaction')
    parser.add_argument('-dko', '--dist_ko', default=None,
                        help='Scalar knock-out distance in Angstroms, ignored if knock-out distance file is present')
    parser.add_argument('-dkof', '--dist_ko_file', default=None,
                        help='File with knock-out distance thresholds (in Angstroms) for each chromatin-to-chromatin proximity interaction')
    parser.add_argument('-e', '--exe', default=DEF_EXE_PATH,
                        help='Path to folding simulation executable binary with "main" dispatch')
    parser.add_argument('-esz', '--ens_size', default=DEF_ENS_SIZE,
                        help='Ensemble size for each bit sample')
    parser.add_argument('-f', '--frag', default=None,
                        help='Input file mapping fragment regions (e.g Hi-C bins) to polymer node spans, may be specified as command-line argument or within "conf" INI')
    parser.add_argument('-jpre', '--job_prefix', default="",
                        help='Output 3-D polymer files will start with this prefix. Will default to base name of <--inframe>')
    # https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    parser.add_argument('-kor', '--ko_rand', dest='ko_rand', action='store_true',
                        help="Switch, if present, will treat '0's as random folding instead of constrained knock-outs")
    parser.add_argument('-no-kor', '--no-ko_rand', dest='ko_rand', action='store_false',
                        help="Switch, if present, will enforce '0's as constrained knock-outs")
    parser.set_defaults(ko_rand=True)
    parser.add_argument('-lgp', '--log_prefix', default=None,
                        help='Will create log file at <log_prefix>.<job_id> for each batch job if specified')
    parser.add_argument('-mf', '--max_fail', default=DEF_MAX_FAIL,
                        help='Maximum number of times a single job configuration may fail with error return code')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of active fold simulations')
    parser.add_argument('-mt', '--max_trial', default=DEF_MAX_TRIAL,
                        help='Maximum number of trials per fold attempt of a given constraint configuration before giving up')
    parser.add_argument('-mth', '--max_thread', default=DEF_MAX_THREAD,
                        help='Maximum number of worker threads per fold simulation')
    parser.add_argument('-np', '--num_proc', default=DEF_NUM_PROC,
                        help='Number of independent CPU processes for each bit sample')
    parser.add_argument('-od', '--outdir', default="",
                        help='Path to output folder, default is to same directory containing <--bitpop> file')
    parser.add_argument('-row', '--rowmaj', action='store_true',
                        help="Switch if present will treat rows of bit population matrix as containing the individual samples")
    parser.add_argument('-ssp', '--sleep_sec_poll', default=DEF_SLEEP_SEC_POLL,
                        help='Polling interval in seconds to check for completed active folding simulations')
    parser.add_argument('-xcsv', '--xport_csv', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable CSV export of polymer geometry")
    parser.add_argument('-xcsv_gz', '--xport_csv_gz', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed CSV export of polymer geometry")
    parser.add_argument('-xintr', '--xport_intr', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable export of PML selections for constrained chromatin-to-chromatin proximity interactions")
    parser.add_argument('-xligc_bond', '--xport_ligc_bond', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable explicit export of (i,i) and (i,i+1) polymer ligation contact tuples (increases file size, these ligations always occur!)")
    parser.add_argument('-xligc_csv', '--xport_ligc_csv', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable CSV export of polymer ligation contact tuples")
    parser.add_argument('-xligc_csv_gz', '--xport_ligc_csv_gz', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed CSV export of polymer ligation contact tuples")
    parser.add_argument('-xlogw', '--xport_logw', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable polymer log importance weight export")
    parser.add_argument('-xpdb', '--xport_pdb', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable PDB export of polymer geometry")
    parser.add_argument('-xpdb_gz', '--xport_pdb_gz', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed PDB export of polymer geometry")
    parser.add_argument('-xpml', '--xport_pml', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable PML export of polymer geometry")
    parser.add_argument('-xpml_gz', '--xport_pml_gz', choices=X_CHOICES, default=X_DEF,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed PML export of polymer geometry")
    # Parse command line
    args = parser.parse_args()
    print_cmd(args)
    # Generate 3-D polymers from population bit matrix!
    bit2pol(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
