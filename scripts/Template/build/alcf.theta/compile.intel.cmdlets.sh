#!/bin/bash

# Script for compiling on Argonne ALCF theta with intel compiler with commandlets

# Note: to run the compiled executable, intel compiler and BOOST variables
# must be in main bash environment. To do so, run the following on the
# commandline or within any launcher shell script:
#   module swap PrgEnv-cray PrgEnv-intel
#   module swap PrgEnv-gnu PrgEnv-intel
#   module load boost/intel/1.64.0
#
# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"


echo "Running build on $HOSTNAME"

# Force intel compiler environment
module swap PrgEnv-cray PrgEnv-intel
module swap PrgEnv-gnu PrgEnv-intel

# Load BOOST C++ libraries
module load boost/intel/1.64.0

# Path to local modules for finding intel MKL LAPACK binaries
cmake_local_modules_dir="$SCRIPT_DIR/../cmake.modules/"
cmake_local_modules_dir=$(cd "$cmake_local_modules_dir"; pwd)

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -commandlets 1 -intel 1 -iccfast 1 -cmakelocalmodulesdir "$cmake_local_modules_dir" -cmakelocalmodulelapack

# Compile
$SCRIPT_DIR/../cmake_build.sh -commandlets 1

echo "Finished build."
