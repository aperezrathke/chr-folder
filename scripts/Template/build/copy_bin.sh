#!/bin/bash 

# Copies compiled binaries into project bin folder

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Project root directory
ROOT_DIR="$SCRIPT_DIR/../../.."
# Obtain absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)

# Base CMake build directory
CMAKE_DIR="$ROOT_DIR/CMakeBuild"

# Target binary directory
BIN_DIR="$ROOT_DIR/bin"
mkdir -p $BIN_DIR

# Executable name
EXE_NAME="u_sac"

pushd ./

cd $CMAKE_DIR

# Iterate over build types
for d in $(find . -maxdepth 1 -mindepth 1 -type d)
do
    src=$(cd "$d"; pwd)
    src="$src/$EXE_NAME"
    dst="$BIN_DIR/$d"
    mkdir -p "$dst"
    dst="$dst/$EXE_NAME"
    echo "Copying $src to $dst"
    cp $src $dst
done # end iteration over build types

popd

echo "Finished binary copy."
