#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script loads targets.txt and compiles build key if found
#
# Usage:
#
#   <script_path> [-h|--help] [-t|--targets] <target_key>
#
# Positional argument(s):
#   <target_key>: unique identifier for build target specified in targets.txt
#
# Optional argument(s):
#   -t/--targets: prints list of build targets available
#   -h/--help: prints command line options

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For path manipulation
import os

# For running shell scripts
import subprocess

###########################################
# Compile
###########################################

# @param wdir - directory containing a 'targets.txt' file
def load_compile_targets(wdir=None):
    '''Parses targets configuration file, returns key -> shell script map'''
    work_dir = os.path.dirname(os.path.realpath(__file__)) if not wdir else wdir
    targets_path = os.path.join(work_dir, "targets.txt")
    targets = {}
    num = 0
    ix_k = 0
    ix_sh = 1
    EMPTY = {}
    with open(targets_path) as f:
        for line in f:
            num = num + 1
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            key2sh = map(str.strip, line.split(','))
            if len(key2sh) != 2:
                print "Error: malformed line in " + targets_path
                print "\tline #" + str(num) + " has " + str(len(key2sh)) + " values, expected 2."
                return EMPTY
            k = key2sh[ix_k]
            if k == 'none':
                print "Error: reserved key 'none' in " + targets_path
                print "\tline # " + str(num)
                return EMPTY
            if k in targets:
                print "Warning: duplicate key found in " + targets_path
                print "\tSee line #" + str(num) + " with key " + k
            sh = key2sh[ix_sh]
            targets[k] = os.path.join(work_dir, sh)
    return targets

# @param wdir - directory containing a 'targets.txt' file
def get_available_compile_targets(wdir=None):
    '''Return list of available build targets'''
    targets = load_compile_targets(wdir=wdir)
    out = sorted(list(targets.keys()))
    out.append('none')
    return out

# @param target_key - target to compile
# @param wdir - directory containing a 'targets.txt' file
# @param print_targets - if True, all available targets will be printed
# @return True if target found or target is 'none', False o/w
def compile_target(target_key, wdir=None, print_targets=False):
    '''Executes build target'''
    targets = load_compile_targets(wdir=wdir)
    if print_targets:
        print "AVAILABLE TARGETS:"
        for key in targets:
            print key + " : " + targets[key]
        print "none : avoid compilation"
    if target_key in targets:
        subprocess.call(targets[target_key])
        return True
    return target_key == "none"

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= PY COMPILE ======================="
    target_opts = get_available_compile_targets()
    # 'none' does not count as a target
    if len(target_opts) <= 1:
        print "Error: no targets available in. Exiting\n"
        return 1
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Positional argument(s)
    parser.add_argument('target_key', default='none', choices=target_opts,
                        nargs='?',
                        help="Target build key as specified in targets.txt or 'none'")
    # Optional argument(s)
    parser.add_argument('-t', '--targets', action='store_true',
                        help="Prints available build targets and mapped shell script")
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t--target_key = ' + args.target_key
    print '\t--targets = ' + str(args.targets)
    # Compile target   
    compile_target(target_key=args.target_key, print_targets=args.targets)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()