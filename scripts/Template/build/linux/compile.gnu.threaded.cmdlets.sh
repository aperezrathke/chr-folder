#!/bin/bash

# Script for compiling on linux with g++ and threading-enabled
#
# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -commandlets 1 -threads 1

# Compile
$SCRIPT_DIR/../cmake_build.sh -commandlets 1 -threads 1

echo "Finished build."
