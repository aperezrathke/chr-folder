#!/bin/bash

# Script for compiling on linux with g++ with no AVX collision support
# (most portable)

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -ssecol 0 -biggridcol128 1

# Compile
$SCRIPT_DIR/../cmake_build.sh -ssecol 0

echo "Finished build."
