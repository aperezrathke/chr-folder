#!/bin/bash

# Script for compiling on linux with g++, tests-enabled, asserts-enabled

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -tests 1 -forceassert

# Compile
$SCRIPT_DIR/../cmake_build.sh -tests 1

echo "Finished build."
