#!/bin/bash

# Script for compiling on intel (xeon) CPU with intel compiler. Assumes
# default installation path.

# Note: to run the compiled executable, the intel compiler variables must be
# in main bash environment. To do so, either source this script directly:
#       source <path_to_this_script>/compile.intel.sh
# Else source compilervars.sh after compiling, eg:
#       <path_to_this_script>/compile.intel.sh
#       source /opt/intel/bin/compilervars.sh intel64
# This will litter the bash environment with lots of intel compiler
# variables. To remove these variables, simply logout then login again.

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Setup intel compiler environment variables.
FC="ifort"
source /opt/intel/bin/compilervars.sh intel64

# Path to local modules for finding intel MKL LAPACK binaries
cmake_local_modules_dir="$SCRIPT_DIR/../cmake.modules/"
cmake_local_modules_dir=$(cd "$cmake_local_modules_dir"; pwd)

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -ssecol 0 -threads 1 -intel 1 -iccfast 1 -cmakelocalmodulesdir "$cmake_local_modules_dir" -cmakelocalmodulelapack

# Compile
$SCRIPT_DIR/../cmake_build.sh -ssecol 0 -threads 1

echo "Finished build."
