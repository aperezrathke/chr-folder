#!/bin/bash 
# Shell script compiles debug and release build directories. Takes the
#   following optional arguments:
#       -mode <Release|RelWithDebInfo|Debug>: Build target mode
#       -releasedir <dirpath>: Path to release build directory
#       -relwithdebinfodir <dirpath>: Path to release with debug info directory
#       -debugdir <dirpath>: Path to debug build directory
#       -threads <0|1>: If 1, enable threaded build (default is disabled)
#       -ssecol <0|1>: If 1, enable SSE AVX2 collisions (default is enabled)
#       -commandlets <0|1>: If 1, enable commandlets (default is disabled)
#       -tests <0|1>: If 1, enable tests (default is disabled)
#       -benchmark <0|1>: If 1, enable benchmark build (default is disabled)

##############################################################################
# Globals
##############################################################################

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
# Project root directory
PROJ_ROOT_DIR="$SCRIPT_DIR/../../.."
# Default build settings
RELEASE_DIR="$PROJ_ROOT_DIR/CMakeBuild/Release"
RELWITHDEBINFO_DIR="$PROJ_ROOT_DIR/CMakeBuild/RelWithDebInfo"
DEBUG_DIR="$PROJ_ROOT_DIR/CMakeBuild/Debug"
MODE="Release"
BUILD_DIR=$RELEASE_DIR
USE_THREADS=0
USE_SSECOL=1
USE_COMMANDLETS=0
USE_TESTS=0
USE_BENCHMARK=0

# Obtain number of physical CPU cores, tested on:
#   - Ubuntu 16.04 LTS, CentOs 6/7, Mac OS X 10.13 (High Sierra)
# https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line/18051445#18051445
NUM_CPU=$([[ $(uname) = 'Darwin' ]] && sysctl -n hw.physicalcpu_max || lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l)

# Allocate Max(0.5*CPU, 1) cores for building
BUILD_CPU=$(($NUM_CPU / 2))
if [ "$BUILD_CPU" -lt "1" ]; then
    BUILD_CPU=1
fi
BUILD_CPU_ARG="-j$BUILD_CPU"

##############################################################################
# doMake
##############################################################################

# Function to run make in parameter build directory
function doMake {
    local MAKE_BUILD_DIR="$1"
    echo "Running make for target dir $MAKE_BUILD_DIR"
    echo "Compiling with $BUILD_CPU CPU cores"
    # Navigate to target build directory
    pushd .
    cd "$MAKE_BUILD_DIR"
    # Run make in target build directory
    # https://blog.kitware.com/cmake-building-with-all-your-cores/
    make "$BUILD_CPU_ARG"
    popd
}

##############################################################################
# User overrides
##############################################################################

# Check if there are any parameter overrides
while [ $# -gt 0 ]
do
    case "$1" in
    -mode)
        MODE="$2"
        shift
        ;;
    -releasedir)
        RELEASE_DIR="$2"
        shift
        ;;
    -relwithdebinfodir)
        RELWITHDEBINFO_DIR="$2"
        shift
        ;;
    -debugdir)
        DEBUG_DIR="$2"
        shift
        ;;
    -threads)
        USE_THREADS="$2"
        shift
        ;;
    -ssecol)
        USE_SSECOL="$2"
        shift
        ;;
    -commandlets)
        USE_COMMANDLETS="$2"
        shift
        ;;
    -tests)
        USE_TESTS="$2"
        shift
        ;;
    -benchmark)
        USE_BENCHMARK="$2"
        shift
        ;;
    esac
    shift
done

##############################################################################
# Case insensitive compare to match build mode to build directory
##############################################################################

# https://stackoverflow.com/questions/2264428/converting-string-to-lower-case-in-bash
MODE_LC=$(echo "$MODE" | tr '[:upper:]' '[:lower:]')
if [ "$MODE_LC" == "release" ]
then
    MODE="Release"
    BUILD_DIR="$RELEASE_DIR"
elif [ "MODE_LC" == "relwithdebinfo"]
then
    MODE="RelWithDebInfo"
    BUILD_DIR="$RELWITHDEBINFO_DIR"
elif [ "MODE_LC" == "debug" ]
then
    MODE="Debug"
    BUILD_DIR="$DEBUG_DIR"
else
    echo "Unrecognized build mode '$MODE', default to 'Release'"
    MODE="Release"
    BUILD_DIR="$RELEASE_DIR"
fi

##############################################################################
# Apply modifiers to target build directory
##############################################################################

# @WARNING - THESE MUST MATCH SAME MODIFIERS IN cmake_init.sh (ORDER MATTERS)!

if [ "$USE_SSECOL" -eq 0 ]
then
    BUILD_DIR="$BUILD_DIR"_nossecol
fi

if [ "$USE_THREADS" -eq 1 ]
then
    BUILD_DIR="$BUILD_DIR"_threaded
fi

if [ "$USE_BENCHMARK" -eq 1 ]
then
    BUILD_DIR="$BUILD_DIR"_benchmark
fi

if [ "$USE_COMMANDLETS" -eq 1 ]
then
    BUILD_DIR="$BUILD_DIR"_commandlets
fi

if [ "$USE_TESTS" -eq 1 ]
then
    BUILD_DIR="$BUILD_DIR"_tests
fi

##############################################################################
# Call
##############################################################################

doMake "$BUILD_DIR"

echo "Finished building CMake configuration(s)."
