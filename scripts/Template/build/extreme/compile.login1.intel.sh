#!/bin/bash

# Script for compiling on UIC Extreme Cluster on login-1 node. This node has
# no SSE AVX2 or F16C support. If running on the 'batch' queue, then binary
# must be compiled on login-1 node as intel compiler 'xHost' option on login-2
# node will insert F16C and/or AVX2 instructions which trigger a hardware
# exception when running on nodes without support for those instruction sets
# as is the case for the 16-core nodes of the batch queue. Note, when running
# jobs on the Liang queue, make sure to use binaries compiled on login-2 as
# intel compiler will insert advanced instructions which greatly increase
# performance (e.g. AVX2 instructions for narrow phase collision detection).

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running login-1 build on $HOSTNAME"

# Load modules
module load compilers/intel
module load tools/Boost-1.58.0-intel

# Find boost headers
BOOST_INCLUDE_DIR="/export/share/tools/Boost-1.58.0-intel/include"
CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:$BOOST_INCLUDE_DIR"

# Generate make files
# Note: to force enable asserts in release, use "-forceassert" switch
$SCRIPT_DIR/../cmake_init.sh -intel 1 -cxx11 0 -ssecol 0 -boostincludedir $BOOST_INCLUDE_DIR

# Compile scripts
$SCRIPT_DIR/../cmake_build.sh -ssecol 0

echo "Finished extreme build."
