#!/bin/bash

# Script for compiling on UIC Extreme Cluster on login-2 node. This node has
# SSE AVX2 and F16C support. For jobs running on Liang queue and/or newer
# extreme nodes with AVX2 support, binaries must be compiled on login-2 node
# in order for intel compiler to detect support for these advanced instruction
# sets. In contrast, if running on the 'batch' queue, then binary must be
# compiled on login-1 node as login-2 compiled binaries will cause hardware
# exceptions on these older nodes due to lack of AVX2 and F16C instruction set
# support.

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running login-2 build on $HOSTNAME"

# Load modules
module load compilers/intel
module load tools/Boost-1.58.0-intel

# Find boost headers
BOOST_INCLUDE_DIR="/export/share/tools/Boost-1.58.0-intel/include"
CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:$BOOST_INCLUDE_DIR"

# Generate make files
# Note: to force enable asserts in release, use "-forceassert" switch
$SCRIPT_DIR/../cmake_init.sh -intel 1 -cxx11 0 -boostincludedir $BOOST_INCLUDE_DIR

# Compile scripts
$SCRIPT_DIR/../cmake_build.sh

echo "Finished extreme build."
