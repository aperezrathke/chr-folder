#!/bin/bash 
# Shell script which creates debug and release build directories using CMake.
#   Takes the following optional arguments:
#       -mode <Release|RelWithDebInfo|Debug>: Build target mode
#       -releasedir <dirpath>: Path to release build directory
#       -relwithdebinfodir <dirpath>: Path to release with debug info directory
#       -debugdir <dirpath>: Path to debug build directory
#       -threads <0|1>: If 1, enable threaded build (default is disabled)
#       -cxx11 <0|1>: If 1, enable usage of C++ 11 (default is enabled)
#       -ssecol <0|1>: If 1, enable SSE AVX2 collisions (default is enabled)
#       -biggridcol <0|1>: If 1, enables 64-bit keys for collision grid (default
#           is disabled)
#       -biggridcol64 <0|1>: Same as 'biggridcol'
#       -biggridcol128 <0|1>: If 1, enables 128-bit keys for collision grid
#           (default is disabled), will override biggridcol|biggridcol64
#       -intel <0|1>: If 1, enable usage of intel compiler (default is disabled)
#       -iccfast <0|1>: If 1, enable 'fast' intel compiler option (default is disabled)
#       -boostincludedir <dir>: (optional) custom path to Boost headers
#       -forceassert: (optional) enables asserts for release builds
#       -commandlets <0|1>: If 1, enable commandlets (default is disabled)
#       -tests <0|1>: If 1, enable tests (default is disabled)
#       -benchmark <0|1>: If 1, enable benchmark build (default is disabled)

##############################################################################
# Globals
##############################################################################

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
# Project root directory
PROJ_ROOT_DIR="$SCRIPT_DIR/../../.."
# Default build settings
RELEASE_DIR="$PROJ_ROOT_DIR/CMakeBuild/Release"
RELWITHDEBINFO_DIR="$PROJ_ROOT_DIR/CMakeBuild/RelWithDebInfo"
DEBUG_DIR="$PROJ_ROOT_DIR/CMakeBuild/Debug"
MODE="Release"
BUILD_DIR=$RELEASE_DIR
USE_THREADS=0
USE_CXX11=1
USE_SSECOL=1
USE_BIGGRIDCOL64=0
USE_BIGGRIDCOL128=0
USE_INTEL=0
USE_ICCFAST=0
USE_COMMANDLETS=0
USE_TESTS=0
USE_BENCHMARK=0
FORCE_ASSERT=0
NO_BOOSTCMAKE=0
BOOST_INCLUDEDIR=""
BOOST_LIBRARYDIR=""
CMAKE_LOCAL_MODULES_DIR=""
CMAKE_LOCAL_MODULE_LAPACK=0

##############################################################################
# initOutOfSource
##############################################################################

# Function to initialize an out-of-source CMake build
function initOutOfSource {
    local BUILD_DIR=$1
    local BUILD_TYPE=$2
    local USE_INTEL_COMPILER=$3
    local DISABLE_CXX11=$4
    local ENABLE_THREADS=$5
    local ENABLE_COMMANDLETS=$6
    local ENABLE_BENCHMARK=$7
    local ENABLE_TESTS=$8
    local DISABLE_SSE_COLLISION=$9
    # Apparently only 9 arguments allowed for a bash method?
    local ENABLE_BIG_GRID_COLLISION64="$USE_BIGGRIDCOL64"
    local ENABLE_BIG_GRID_COLLISION128="$USE_BIGGRIDCOL128"

    # @TODO - consider forcing asserts if testing is enabled

    echo "Generating out-of-source $BUILD_TYPE build at $BUILD_DIR using $PROJ_ROOT_DIR"
    echo "-Build disables C++ 11? $DISABLE_CXX11"
    echo "-Build has threading enabled? $ENABLE_THREADS"
    echo "-Build has commandlets enabled? $ENABLE_COMMANDLETS"
    echo "-Build disables cmake Boost? $NO_BOOSTCMAKE"
    echo "-Build has custom Boost include? $BOOST_INCLUDEDIR"
    echo "-Build has custom Boost library? $BOOST_LIBRARYDIR"
    echo "-Build is forcing asserts enabled? $FORCE_ASSERT"
    echo "-Build is for benchmarking? $ENABLE_BENCHMARK"
    echo "-Build has tests enabled? $ENABLE_TESTS"
    echo "-Build disables SSE (AVX2) collisions? $DISABLE_SSE_COLLISION"
    echo "-Build has big grid collisions (64-bit)? $ENABLE_BIG_GRID_COLLISION64"
    echo "-Build has big grid collisions (128-bit)? $ENABLE_BIG_GRID_COLLISION128"

    ENABLE_CXX11_ARG="-DDEFINE_BUILD_CXX_11=ON"
    if [ "$DISABLE_CXX11" -eq 1 ]; then
        ENABLE_CXX11_ARG="-DDEFINE_BUILD_CXX_11=OFF"
    fi

    ENABLE_SSE_COLLISION_ARG="-DDEFINE_ENABLE_SSE_COLLISION=ON"
    if [ "$DISABLE_SSE_COLLISION" -eq 1 ]; then
        BUILD_DIR="$BUILD_DIR"_nossecol
        ENABLE_SSE_COLLISION_ARG="-DDEFINE_ENABLE_SSE_COLLISION=OFF"
    fi

    ENABLE_BIG_GRID_COLLISION64_ARG="-DDEFINE_ENABLE_BIG_GRID_COLLISION64=OFF"
    if [ "$ENABLE_BIG_GRID_COLLISION64" -eq 1 ]; then
        ENABLE_BIG_GRID_COLLISION64_ARG="-DDEFINE_ENABLE_BIG_GRID_COLLISION64=ON"
    fi

    ENABLE_BIG_GRID_COLLISION128_ARG="-DDEFINE_ENABLE_BIG_GRID_COLLISION128=OFF"
    if [ "$ENABLE_BIG_GRID_COLLISION128" -eq 1 ]; then
        ENABLE_BIG_GRID_COLLISION128_ARG="-DDEFINE_ENABLE_BIG_GRID_COLLISION128=ON"
    fi

    ENABLE_THREADS_ARG="-DDEFINE_ENABLE_THREADS=OFF"
    if [ "$ENABLE_THREADS" -eq 1 ]; then
        BUILD_DIR="$BUILD_DIR"_threaded
        ENABLE_THREADS_ARG="-DDEFINE_ENABLE_THREADS=ON"
    fi

    # Check for benchmarking build flag:
    #   - turn off random seeding
    #   - turn on interval logging
    #   - turn on commandlets
    # Will override any other build flag
    ENABLE_BENCHMARK_ARG="-DDEFINE_ENABLE_BENCHMARK=OFF"
    if [ "$ENABLE_BENCHMARK" -eq 1 ]; then
        ENABLE_BENCHMARK_ARG="-DDEFINE_ENABLE_BENCHMARK=ON"
        echo "Overriding commandlets to be enabled."
        ENABLE_COMMANDLETS=1
        BUILD_DIR="$BUILD_DIR"_benchmark
    fi

    ENABLE_COMMANDLETS_ARG="-DDEFINE_ENABLE_COMMANDLETS=OFF"
    if [ "$ENABLE_COMMANDLETS" -eq 1 ]; then
        BUILD_DIR="$BUILD_DIR"_commandlets
        ENABLE_COMMANDLETS_ARG="-DDEFINE_ENABLE_COMMANDLETS=ON"
    fi

    ENABLE_TESTS_ARG="-DDEFINE_ENABLE_TESTS=OFF"
    if [ "$ENABLE_TESTS" -eq 1 ]; then
        BUILD_DIR="$BUILD_DIR"_tests
        ENABLE_TESTS_ARG="-DDEFINE_ENABLE_TESTS=ON"
    fi

    NO_BOOSTCMAKE_ARG=""
    if [ "$NO_BOOSTCMAKE" -eq 1 ]; then
        NO_BOOSTCMAKE_ARG="-DBoost_NO_BOOST_CMAKE=TRUE"
    fi

    # Check for non-empty (non-null) boost include path
    BOOST_INCLUDEDIR_ARG=""
    if [ -n "$BOOST_INCLUDEDIR" ]; then
        BOOST_INCLUDEDIR_ARG="-DBOOST_INCLUDEDIR=$BOOST_INCLUDEDIR"
    fi

    # Check for non-empty (non-null) boost library path
    BOOST_LIBRARYDIR_ARG=""
    if [ -n "$BOOST_LIBRARYDIR" ]; then
        BOOST_LIBRARYDIR_ARG="-DBOOST_LIBRARYDIR=$BOOST_LIBRARYDIR"
    fi

    # Check if asserts are being force enabled
    FORCE_ASSERT_ARG="-DDEFINE_FORCE_ASSERT=OFF"
    if [ "$FORCE_ASSERT" -eq 1 ]; then
        FORCE_ASSERT_ARG="-DDEFINE_FORCE_ASSERT=ON"
    fi

    # Check if we should use local CMAKE modules
    CMAKE_LOCAL_MODULES_DIR_ARG=""
    if [ -n "$CMAKE_LOCAL_MODULES_DIR" ]; then
        CMAKE_LOCAL_MODULES_DIR_ARG="-DCMAKE_LOCAL_MODULES_DIR__=$CMAKE_LOCAL_MODULES_DIR"
    fi

    # Check if we should use custom local module to find LAPACK
    CMAKE_LOCAL_MODULE_LAPACK_ARG="-DCMAKE_LOCAL_MODULE_LAPACK__=OFF"
    if [ "$CMAKE_LOCAL_MODULE_LAPACK" -eq 1 ]; then
        CMAKE_LOCAL_MODULE_LAPACK_ARG="-DCMAKE_LOCAL_MODULE_LAPACK__=ON"
    fi

    # Check intel fast flags
    ENABLE_ICC_FAST_ARG=""
    if [ "$USE_ICCFAST" -eq 1 ]; then
        ENABLE_ICC_FAST_ARG="-DENABLE_ICC_FAST_FLAGS=ON"
    fi

    # Clear out any previous build directory
    rm -rf "$BUILD_DIR"
    # Navigate to build directory
    mkdir -p "$BUILD_DIR"
    pushd .
    cd "$BUILD_DIR"

    # Construct command line definitions
    cmd="cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE $ENABLE_CXX11_ARG"
    cmd="$cmd $ENABLE_SSE_COLLISION_ARG $ENABLE_BIG_GRID_COLLISION64_ARG"
    cmd="$cmd $ENABLE_BIG_GRID_COLLISION128_ARG $ENABLE_THREADS_ARG"
    cmd="$cmd $ENABLE_COMMANDLETS_ARG $ENABLE_BENCHMARK_ARG"
    cmd="$cmd $ENABLE_TESTS_ARG $FORCE_ASSERT_ARG"
    cmd="$cmd $NO_BOOSTCMAKE_ARG $BOOST_INCLUDEDIR_ARG $BOOST_LIBRARYDIR_ARG"
    cmd="$cmd $CMAKE_LOCAL_MODULES_DIR_ARG $CMAKE_LOCAL_MODULE_LAPACK_ARG"
    if [ "$USE_INTEL_COMPILER" -eq 1 ]; then
        echo Using intel compiler
        cmd="$cmd -DCMAKE_CXX_COMPILER=icpc -DCMAKE_C_COMPILER=icc -DCMAKE_Fortran_COMPILER=ifort"
        cmd="$cmd $ENABLE_ICC_FAST_ARG"
    else
        echo "Using default compiler"
    fi
    # Execute command
    $cmd -G"CodeBlocks - Unix Makefiles" "$PROJ_ROOT_DIR"

    popd
}

##############################################################################
# User overrides
##############################################################################

# Check if there are any parameter overrides
while [ $# -gt 0 ]
do
    case "$1" in
    -mode)
        MODE="$2"
        shift
        ;;
    -releasedir)
        RELEASE_DIR="$2"
        shift
        ;;
    -relwithdebinfodir)
        RELWITHDEBINFO_DIR="$2"
        shift
        ;;
    -debugdir)
        DEBUG_DIR="$2"
        shift
        ;;
    -threads)
        USE_THREADS="$2"
        shift
        ;;
    -cxx11)
        USE_CXX11="$2"
        shift
        ;;
    -ssecol)
        USE_SSECOL="$2"
        shift
        ;;
    -biggridcol)
        USE_BIGGRIDCOL64="$2"
        shift
        ;;
    -biggridcol64)
        USE_BIGGRIDCOL64="$2"
        shift
        ;;
   -biggridcol128)
        USE_BIGGRIDCOL128="$2"
        shift
        ;;
    -intel)
        USE_INTEL="$2"
        shift
        ;;
    -iccfast)
        USE_ICCFAST="$2"
        shift
        ;;
    -noboostcmake)
        NO_BOOSTCMAKE=1
        ;;
    -boostincludedir)
        BOOST_INCLUDEDIR="$2"
        shift
        ;;
    -boostlibdir)
        BOOST_LIBRARYDIR="$2"
        shift
        ;;
    -forceassert)
        FORCE_ASSERT=1
        ;;
    -cmakelocalmodulesdir)
        CMAKE_LOCAL_MODULES_DIR="$2"
        shift
        ;;
    -cmakelocalmodulelapack)
        CMAKE_LOCAL_MODULE_LAPACK=1
        ;;
    -commandlets)
        USE_COMMANDLETS="$2"
        shift
        ;;
    -tests)
        USE_TESTS="$2"
        shift
        ;;
    -benchmark)
        USE_BENCHMARK="$2"
        shift
        ;;
    esac
    shift
done

##############################################################################
# Case insensitive compare to match build mode to build directory
##############################################################################

# https://stackoverflow.com/questions/2264428/converting-string-to-lower-case-in-bash
MODE_LC=$(echo "$MODE" | tr '[:upper:]' '[:lower:]')
if [ "$MODE_LC" == "release" ]
then
    MODE="Release"
    BUILD_DIR="$RELEASE_DIR"
elif [ "MODE_LC" == "relwithdebinfo"]
then
    MODE="RelWithDebInfo"
    BUILD_DIR="$RELWITHDEBINFO_DIR"
elif [ "MODE_LC" == "debug" ]
then
    MODE="Debug"
    BUILD_DIR="$DEBUG_DIR"
else
    echo "Unrecognized build mode '$MODE', default to 'Release'"
    MODE="Release"
    BUILD_DIR="$RELEASE_DIR"
fi

##############################################################################
# Determine negative toggles
##############################################################################

# Out-of-source method expects 'negative' conditionals for certain fields such
# as C++11 and SSE AVX2 collisions
DISABLE_CXX11=0
if [ "$USE_CXX11" -eq 0 ]
then
    DISABLE_CXX11=1
fi

DISABLE_SSE_COLLISION=0
if [ "$USE_SSECOL" -eq 0 ]
then
    DISABLE_SSE_COLLISION=1
fi

##############################################################################
# Call
##############################################################################

# Call out of source build
initOutOfSource "$BUILD_DIR" "$MODE" "$USE_INTEL" "$DISABLE_CXX11" "$USE_THREADS" "$USE_COMMANDLETS" "$USE_BENCHMARK" "$USE_TESTS" "$DISABLE_SSE_COLLISION"

echo "Finished initializing CMake."
