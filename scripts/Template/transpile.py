#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script generates C++ model specification from JSON, also known as a
# 'transpiler' from JSON to C++. This is meant to serve as a library file for
# use with other scripts. Main usage is to call json2cpp(file) method which
# transforms the json file into a C++ code string
#
# A command line interface is provided for testing/demonstration purposes.
# Usage: (test interface)
#   python <path_to_this_script> --file <path>
#   --file: Path to JSON model specification

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For parsing JSON
import json

# For exiting if inputs invalid
import sys

# For creating glue objects
from collections import namedtuple

###########################################
# Named tuples
###########################################

# Parse context
# When resolving 'variables', first the 'local' arch dictionary is checked,
# then the the 'global' js dictionary is checked
# js - JSON dictionary object, used for resolving variables
# glue_cpp_name - C++ glue name of enclosing glue object
# arch - archetype dict used for resolving defaults and variables, may be None
# rhs - right-hand side (rhs), i.e. 'value', of js element defining the
#   element to parse, may be string or dictionary
# is_top - True if element is within seniormost/outermost C++ glue
# headers - Modifiable set of C++ header file names
# tabs - Parsers named tuple of callback tables
# tr_info - TrInfo named tuple or None
# cbfunc_intr_lam_prox - Callback for proximity lamina interactions
# cbfunc_intr_lam_prox_budg - Callback for budgeted, proximity lamina
#   interactions
Ctx_FIELDS = ['js', 'glue_cpp_name', 'arch', 'rhs', 'is_top', 'headers',
              'tabs', 'tr_info', 'cbfunc_intr_lam_prox',
              'cbfunc_intr_lam_prox_budg']
Ctx = namedtuple('Ctx', Ctx_FIELDS)

# Parseable tuple
# key - parser key, name of parser callback key
# rhs - right-hand side element to pass to parser callback
Parseable_FIELDS = ['key', 'rhs']
Parseable = namedtuple('Parseable', Parseable_FIELDS)

# Handle to parser callback tables
Parsers_FIELDS = ['COLLIS', 'DIAM', 'ENER', 'GROW', 'INTR_CHR', 'INTR_LAM',
                  'INTR_NUCB', 'NUC', 'QC', 'RS_SAMPLER', 'RC_SCALE', 'SCHED',
                  'SEED', 'SEL', 'SUMMARY', 'TR']
Parsers = namedtuple('Parsers', Parsers_FIELDS)

# Trial runner info, result of parsing a trial runner
# cpp - C++ atom string
# sub_tr - name of trial runner child sub-simulation glue key
# sub_tr_sel - name of trial runner selection child sub-simulation glue key
TrInfo_FIELDS = ['cpp', 'sub_tr', 'sub_tr_sel']
TrInfo = namedtuple('TrInfo', TrInfo_FIELDS)

# Pairs cpp with any sub-simulation JSON key
# cpp - C++ atom string
# sub - name of child sub-simulation glue key
CppWithSubInfo_FIELDS = ['cpp', 'sub']
CppWithSubInfo = namedtuple('CppWithSubInfo', CppWithSubInfo_FIELDS)

# GlueCpp is a C++ entity for wiring together the simulation components
# name - C++ class name of enclosing glue
# sample - C++ class name of sample
# sim - C++ class name of simulation (sim)
# trial_runner - C++ atom string for trial runner sim mixin
# quality_control - C++ atom string for quality control sim mixin
# radius - C++ atom string for node radius sample mixin
# radius_sim - C++ atom string for node radius sim mixin
# growth - C++ atom string for growth sample mixin
# growth_sim - C++ atom string for growth sim mixin
# nuclear - C++ atom string for nuclear sample mixin
# nuclear_sim - C++ atom string for nuclear sim mixin
# collision - C++ atom string for collision sample mixin
# collision_sim - C++ atom string for collision sim mixin
# intr_chr - C++ atom string for chr-chr interaction sample mixin
# intr_chr_sim - C++ atom string for chr-chr interaction sim mixin
# intr_lam - C++ atom string for lamina interaction sample mixin
# intr_lam_sim - C++ atom string for lamina interaction sim mixin
# intr_nucb - C++ atom string for nuclear body interaction sample mixin
# intr_nucb_sim - C++ atom string for nuclear body interaction sim mixin
# energy - C++ atom string for energy sample mixin
# energy_sim - C++ atom string for energy sim mixin
# seed - C++ atom string for seed mixin
GlueCpp_FIELDS = ['name', 'sample', 'sim', 'trial_runner', 'quality_control',
                  'radius', 'radius_sim', 'growth', 'growth_sim', 'nuclear',
                  'nuclear_sim', 'collision', 'collision_sim', 'intr_chr',
                  'intr_chr_sim', 'intr_lam', 'intr_lam_sim', 'intr_nucb',
                  'intr_nucb_sim', 'energy', 'energy_sim', 'seed']
GlueCpp = namedtuple('GlueCpp', GlueCpp_FIELDS)

# GlueInfo
# tup - GlueCpp tuple
# sub_tr - name of trial runner child sub-simulation glue key
# sub_tr_sel - name of trial runner selection child sub-simulation glue key
# sub_qc - name of quality control child sub-simulation glue key
GlueInfo_FIELDS = ['tup', 'sub_tr', 'sub_tr_sel', 'sub_qc']
GlueInfo = namedtuple('GlueInfo', GlueInfo_FIELDS)

# C++ mixin info for paired simulation (sim) and sample (sam) mixins
MixinInfo_FIELDS = ['sim', 'sam']
MixinInfo = namedtuple('MixinInfo', MixinInfo_FIELDS)

# C++ nuclear mixin info along with lamina interaction callbacks if needed
# cbfunc_intr_lam_prox - Callback for proximity lamina interactions
# cbfunc_intr_lam_prox_budg - Callback for budgeted, proximity lamina
NucInfo_FIELDS = ['sim', 'sam', 'cbfunc_intr_lam_prox', 'cbfunc_intr_lam_prox_budg']
NucInfo = namedtuple('NucInfo', NucInfo_FIELDS)

# Final output
# name - user name of model, intended for use as namespace but has not been
#   sanitized
# headers - set of C++ header files needed by glues
# glues - list of glues with top-level in descending order of seniority (i.e
#   top-level glue is at index 0)
CppInfo_FIELDS = ['name', 'headers', 'glues']
CppInfo = namedtuple('CppInfo', CppInfo_FIELDS)

###########################################
# Tokens (alphanumeric)
###########################################

# Tokens usable by JSON
T_ALPHA_SLOT = "alpha_slot"
T_ARCH = ["arch", "archetype"]
T_BEND = "bend"
T_CANON = ["canon", "canonical"]
T_COLLIS = ["collis", "collision"]
T_COMMENT = "#"
T_DEL_POW = ["del_pow", "del_power", "delphic_pow", "delphic_power"]
T_DIAM = ["diam", "diameter"]
T_DYN_ESS = ["dyn_ess", "dynamic_ess"]
T_ELLIP = ["ellip", "ellipsoid"]
T_ENER = ["ener", "energy"]
T_ESS_SLOT = "ess_slot"
T_FIXED_QUANT = ["fixed_quant", "fixed_quantile"]
T_GROW = ["grow", "growth"]
T_HARD_SHELL = "hard_shell"
T_HETR = ["hetr", "heterogeneous"]
T_HOMG = ["homg", "homogeneous"]
T_INTERP_WEIGHTS = ["interp_weights", "interpolate_weights"]
T_INTR_CHR = ["intr_chr", "intr_chrom", "intr_chromatin", "interaction_chr", "interaction_chrom", "interaction_chromatin"]
T_INTR_FAIL = ["intr_fail", "intr_failure", "interaction_fail", "interaction_failure"]
T_INTR_LAM = ["intr_lam", "intr_lamina", "interaction_lam", "interaction_lamina"]
T_INTR_NUCB = ["intr_nucb", "interaction_nucb", "intr_nuclear_body", "interaction_nuclear_body"]
T_LAG_HETR = ["lag_hetr", "lag_heterogeneous"]
T_LAG_HOMG = ["lag_homg", "lag_homogeneous"]
T_LAG_HOMG_UNIF = ["lag_homg_unif", "lag_homogeneous_uniform"]
T_LAG_SLOT = "lag_slot"
T_LAG_FILE_SLOT = "lag_file_slot"
T_LMRK = ["lmrk", "landmark"]
T_MEAN_W = ["mea_w", "mea_weight", "mean_w", "mean_weight"]
T_MEDIAN_W = ["med_w", "med_weight", "median_w", "median_weight"]
T_MLOC = ["mloc", "multiple_loci"]
T_NAME = "name"
T_NUC = ["nuc", "nucleus"]
T_NULL = "null"
T_OFA = ["ofa", "overlap_factor"]
T_POW = ["pow", "power"]
T_PROX = ["prox", "proximity"]
T_PROX_BUDG = ["prox_budg", "prox_budget", "proximity_budg", "proximity_budget"]
T_QC = ["qc", "quality_control"]
T_RC = ["rc", "rejection_control"]
T_RCPART = ["rcpart", "rejection_control_partial"]
T_RESID = ["resid", "residual"]
T_RIGID_UNIF = ["rigid_unif", "rigid_uniform"]
T_RS = ["rs", "resample"]
T_SAMPLER = "sampler"
T_SCALE = "scale"
T_SCHED = ["sched", "schedule"]
T_SEED = "seed"
T_SEL = ["sel", "select"]
T_SLOC = ["sloc", "single_locus"]
T_SLOT = "slot"
T_SPHERE = "sphere"
T_SUB = ["sub", "sub_glue"]
T_SUMMARY = "summary"
T_TOP = "top"
T_TR = ["tr", "trun", "trial_runner"]
T_TYPE = "type"
T_UNIF = ["unif", "uniform"]
T_UNIF_CUBE = ["unif_cube", "uniform_cube"]

###########################################
# Misc globals
###########################################

# Minimum slot index for options
OPT_SLOT_MIN = 0

# Maximum slot index for options
OPT_SLOT_MAX = 7

# Maximum recursion depth allowed
MAX_RECUR_DEPTH = 50

###########################################
# JSON loader
###########################################

# JSON library encodes all strings as 'unicode' types, address this by
# converting all 'unicode' entities to 'str' entities
# https://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-from-json
# @return object with all unicode elements converted to byte str
def unicode2str(input):
    '''Convert unicode to byte str'''
    if isinstance(input, dict):
        out = {}
        for key, val in input.iteritems():
            out[unicode2str(key)] = unicode2str(val)
        return out
    elif isinstance(input, list):
        return [unicode2str(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

# Parses JSON model specification into dictionary, strips comment '#' lines
# @param json_path - path to JSON file
# @return JSON dictionary or terminal error
def load_json(json_fpath):
    '''Returns JSON dictionary'''
    lines = []
    # Strip empty lines and comment lines
    try:
        with open(json_fpath, 'r') as f:
            for line in f:
                # If first non-whitespace character is comment, then line is
                # is considered a comment
                line_core = line.strip()
                if line_core and not line_core.startswith(T_COMMENT):
                    lines.append(line.rstrip())
    except Exception as e:
        print(e)
        print "Error: could not read " + json_fpath + ". Exiting."
        sys.exit(1)
    if not lines:
        print "Error: " + json_fpath + " is empty. Exiting."
        sys.exit(1)
    # Merge all lines into single string
    raw = '\n'.join(lines)
    # Parse JSON
    out = {}
    try:
        out = json.loads(raw)
    except Exception as e:
        print(e)
        print "Error: could not parse JSON. Exiting."
        sys.exit(1)
    if not out:
        print "Error: JSON object is empty. Exiting."
        sys.exit(1)
    return unicode2str(out)

###########################################
# Parser: misc
###########################################

# @param tok - Token string or list of token strings
def get_tok_str(tok):
    '''Return parameter token string or first token string in list'''
    assert tok
    assert isinstance(tok, str) or \
        (isinstance(tok, list) and isinstance(tok[0], str))
    return tok if isinstance(tok, str) else tok[0]

# @param d - dictionary
# @param k - key string or list of key strings
def get_lhs_first(d, k):
    '''Return first left-hand side key in k found in d or None'''
    assert isinstance(d, dict)
    assert type(k) in [str, list]
    k_ls = k if isinstance(k, list) else [k]
    for k_elem in k_ls:
        if k_elem in d:
            return k_elem
    return None

# @param d - dictionary
# @param k - key string or list of key strings
def get_rhs_first(d, k):
    '''Return right-hand side of first key found or None'''
    assert isinstance(d, dict)
    assert type(k) in [str, list]
    k_ls = k if isinstance(k, list) else [k]
    for k_elem in k_ls:
        if k_elem in d:
            return d[k_elem]
    return None

# Try parseable resolution of key(s) k
# @param d - dictionary object
# @param k - key string or list of key strings
# @return Parseable named tuple if available, None if not found
def get_parseable(d, k):
    '''Return associated Parseable for key(s) k or None'''
    assert type(k) in [str, list]
    if not d:
        # Dictionary d is empty or None
        return None
    assert isinstance(d, dict)
    k_ls = k if isinstance(k, list) else [k]
    for k_elem in k_ls:
        if k_elem in d:
            # Key found!
            rhs = d[k_elem]
            assert rhs
            if isinstance(rhs, dict):
                assert T_TYPE in rhs
                assert isinstance(rhs[T_TYPE], str)
                assert rhs[T_TYPE]
                # Case: rhs is dict
                return Parseable(key=rhs[T_TYPE], rhs=rhs)
            assert isinstance(rhs, str)
            # Case: rhs is str
            return Parseable(key=rhs, rhs=rhs)
    # Key not found!
    return None

# Resolution of tokens failed
# @param ctx - Ctx named tuple
# @param tok - tokens which failed to resolve
def fail_resolve(ctx, tok):
    '''Terminal failure due to inability to resolve token(s)'''
    print "Error: unable to resolve token(s): "
    print "\t" + str(tok)
    print "Context:\n\t" + str(ctx.rhs)
    print "Exiting."
    sys.exit(1)

# Recursively attempts to resolve a parseable variable until key is found which
# maps to parameter parser table, otherwise terminal error
# @param ctx - Ctx named tuple
# @param tok - token string or list of token strings
# @param tab - "Parser table", dictionary of parse key to callback mappings
# @param tup - Parseable named tuple, current resolution state
# @param depth - integer recursion depth, program terminates with error if max
#   limit is exceeded
# @return Named tuple of type Parseable such that .key member is found in
#   parser table or terminates program if maximum recursion depth exceeded or
#   parse error encountered
def resolve_parseable(ctx, tok, tab, tup, depth):
    '''Obtain parseable tuple'''
    # Error: tup.key is not a non-empty string
    error_tup_key = (
        not tup) or (
        not tup.key) or (
            not isinstance(
                tup.key,
                str))
    # Error: tup.rhs is not a non-empty string or dictionary
    error_tup_rhs = (not tup) or (not tup.rhs) or \
        (type(tup.rhs) not in [str, dict])
    if error_tup_key or error_tup_rhs:
        fail_resolve(ctx, tok)
    # Check if variable is mapped to a parser callback
    if tup.key in tab:
        # Hurray, variable is resolved!
        return tup
    # Error: recursion depth exceeded
    if depth > MAX_RECUR_DEPTH:
        fail_resolve(ctx, tok)
    # Parser table look-up failed, that means we are a variable. Variables can
    # be resolved in the following priority order:
    #   1) local rhs dictionary
    #   2) local archetype dictionary -or-
    #   3) global js dictionary.
    # Check rhs dictionary
    tup_new = get_parseable(ctx.rhs, tup.key)
    if tup_new:
        return resolve_parseable(ctx, tok, tab, tup_new, depth + 1)
    # Check archetype dictionary
    tup_new = get_parseable(ctx.arch, tup.key)
    if tup_new:
        # Switch to archetype scope
        ctx_new = ctx._replace(arch=None, rhs=ctx.arch)
        return resolve_parseable(ctx_new, tok, tab, tup_new, depth + 1)
    # Check js dictionary
    tup_new = get_parseable(ctx.js, tup.key)
    # Switch ctx to global scope
    ctx_new = ctx._replace(arch=None, rhs=ctx.js)
    # Proceed with global js resolution
    return resolve_parseable(ctx_new, tok, tab, tup_new, depth + 1)

# @param ctx - Ctx named tuple
# @param tok - token string or list
# @param tab - 'parser table', a dictionary of parser callbacks
# @param defcb - Optional 'default callback' method which returns default
#   Parseable
# @return Data structure containing C++ atom string for token(s) as well
#   as any other data returned by token parser
def get_cpp(ctx, tok, tab, defcb = None):
    '''Return C++ atom string for token(s)'''
    tup = get_parseable(ctx.rhs, tok)
    if not tup:
        # Token not found, check archetype dictionary
        tup = get_parseable(ctx.arch, tok)
    if tup:
        # Token specified by user
        tup = resolve_parseable(ctx, tok, tab, tup, 0)
    else:
        # Token not specified by user, check default if available
        if defcb is not None:
            # Token not found in archetype, check for default
            tup = defcb()
        else:
            fail_resolve(ctx, tok)
    # Post-conditions for parseable resolution
    assert tup
    assert tup.key
    assert tup.key in tab
    # Call parser to obtain C++ atom string
    ctx_new = ctx._replace(rhs=tup.rhs)
    parser = tab[tup.key]
    cpp = parser(ctx_new)
    return cpp

# @param s - string
def get_glue_cpp_name(s):
    '''Return name of C++ glue class from string s'''
    assert isinstance(s, str)
    # Remove all whitespace
    s = ''.join(s.split())
    if not s:
        print "Error: invalid name for glue object. Exiting."
        exit(1)
    return "u_" + s + "_glue"

###########################################
# Parser: trial runner
###########################################

# @return Default Parseable for trial runner
def defcb_tr():
    '''Default callback for trial runner'''
    tok_str = get_tok_str(T_CANON)
    print "[json2cpp] Defaulting trial runner to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @return True if we are top-level glue with canonical trial runner. Only when
#   both of these conditions are True do we want to use threaded variants of
#   the following simulation mixins: Growth, Collision, Nuclear, Energy, and
#   possibly Interaction (this list may not be exhaustive)
def is_tr_canon_top(ctx):
    '''Is glue top-level with a canonical trial runner?'''
    assert ctx.tr_info
    # We are assuming that if sub-glue key is false, then we must be canonical
    return ctx.is_top and (not ctx.tr_info.sub_tr)

# @param ctx - Ctx named tuple
# @return TrInfo named tuple
def parse_tr_canon(ctx):
    '''Canonical trial runner'''
    # Add trial runner header file
    ctx.headers.add("uSisCanonicalTrialRunnerMixin.h")
    # Handle parallel vs serial
    grower_cpp = ""
    if ctx.is_top:
        # Parallel
        ctx.headers.add("uSisParallelBatchGrower.h")
        grower_cpp = "uSisParallelBatchGrower<" + ctx.glue_cpp_name + ">"
    else:
        # Serial
        ctx.headers.add("uSisSerialBatchGrower.h")
        grower_cpp = "uSisSerialBatchGrower<" + ctx.glue_cpp_name + ">"
    tr_cpp = "uSisCanonicalTrialRunnerMixin<" + \
        ctx.glue_cpp_name + ", " + grower_cpp + " >"
    return TrInfo(cpp=tr_cpp, sub_tr=None, sub_tr_sel=None)

# @param ctx - Ctx named tuple
# @return TrInfo named tuple
def parse_tr_lmrk(ctx):
    '''Landmark trial runnner'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_LMRK
    # Select
    sel_sub = None
    sel_cpp = get_cpp(ctx, T_SEL, ctx.tabs.SEL)
    if isinstance(sel_cpp, CppWithSubInfo):
        sel_sub = sel_cpp.sub
        sel_cpp = sel_cpp.cpp
    assert isinstance(sel_cpp, str)
    # Schedule
    sched_cpp = get_cpp(ctx, T_SCHED, ctx.tabs.SCHED)
    if "uSisDynamicEssScheduleMixin" in sched_cpp:
        # Dynamic schedules are not compatible with landmark runners
        print "Error: only deterministic schedules are compatible with landmark runners."
        print "Error encountered in context:\n" + str(ctx)
        print "Exiting."
        sys.exit(1)
    # Sub
    sub_val = get_rhs_first(ctx.rhs, T_SUB)
    if (not sub_val) or (not isinstance(sub_val, str)):
        print "Error: unable to parse sub for landmark trial runner."
        fail_resolve(ctx, T_SUB)
    sub_glue_cpp_name = get_glue_cpp_name(sub_val)
    if sub_glue_cpp_name == ctx.glue_cpp_name:
        print "Error: infinite recursion: Parent == Child sub for landmark trial runner."
        fail_resolve(ctx, T_SUB)
    sub_sim_cpp_name = sub_glue_cpp_name + "::sim_t"
    # Add trial runner header files
    ctx.headers.add("uSisLandmarkTrialRunnerMixin.h")
    ctx.headers.add("uSisSubSimAccess.h")
    ctx.headers.add("uSisSampleCopier_unsafe.h")
    # Handle parallel vs serial
    access_cpp = ""
    grower_cpp = ""
    heartbeat_cpp = ""
    if ctx.is_top:
        # Parallel
        ctx.headers.add("uSisParallelBatchGrower.h")
        grower_cpp = "uSisParallelBatchGrower<" + ctx.glue_cpp_name + ">"
        access_cpp = "uSisSubSimAccess_threaded<" + sub_sim_cpp_name + ">"
        heartbeat_cpp = "true"
    else:
        # Serial
        ctx.headers.add("uSisSerialBatchGrower.h")
        grower_cpp = "uSisSerialBatchGrower<" + ctx.glue_cpp_name + ">"
        access_cpp = "uSisSubSimAccess_serial<" + sub_sim_cpp_name + ">"
        heartbeat_cpp = "false"
    # Sample copier
    sample_copier_cpp = "uSisSampleCopier_unsafe"
    # Final cpp
    tr_cpp = ", ".join(
        ("uSisLandmarkTrialRunnerMixin<" +
         ctx.glue_cpp_name,
         sel_cpp,
         sched_cpp,
         access_cpp,
         sample_copier_cpp,
         grower_cpp,
         heartbeat_cpp))
    tr_cpp = tr_cpp + ">"
    return TrInfo(cpp=tr_cpp, sub_tr=sub_val, sub_tr_sel=sel_sub)

###########################################
# Parser: schedule
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string for homogeneous lag schedule
def parse_sched_lag_homg(ctx):
    '''Homogeneous lag schedule'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_LAG_HOMG
    # Add header
    ctx.headers.add("uSisHomgLagScheduleMixin.h")
    # Slot
    lag_slot = get_rhs_first(ctx.rhs, T_LAG_SLOT)
    if not lag_slot:
        print "Error: unable to locate lag slot for homogeneous lag schedule."
        fail_resolve(ctx, T_LAG_SLOT)
    lag_slot = int(lag_slot)
    assert lag_slot >= OPT_SLOT_MIN
    assert lag_slot <= OPT_SLOT_MAX
    lag_slot_enum = "uOpt_qc_schedule_lag_slot" + str(lag_slot)
    # Cpp code
    sched_cpp = "uSisHomgLagScheduleMixin<" + ctx.glue_cpp_name + ", " + \
        lag_slot_enum + ">"
    return sched_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for homogeneous uniform lag schedule
def parse_sched_lag_homg_unif(ctx):
    '''Homogeneous uniform lag schedule'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_LAG_HOMG_UNIF
    # Add header
    ctx.headers.add("uSisHomgUnifLagScheduleMixin.h")
    # Slot
    lag_slot = get_rhs_first(ctx.rhs, T_LAG_SLOT)
    if not lag_slot:
        print "Error: unable to locate lag slot for homogeneous uniform lag schedule."
        fail_resolve(ctx, T_LAG_SLOT)
    lag_slot = int(lag_slot)
    assert lag_slot >= OPT_SLOT_MIN
    assert lag_slot <= OPT_SLOT_MAX
    lag_slot_enum = "uOpt_qc_schedule_lag_slot" + str(lag_slot)
    # Cpp code
    sched_cpp = "uSisHomgUnifLagScheduleMixin<" + ctx.glue_cpp_name + ", " + \
        lag_slot_enum + ">"
    return sched_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for heterogeneous lag schedule
def parse_sched_lag_hetr(ctx):
    '''Heterogeneous lag schedule'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_LAG_HETR
    # Add header
    ctx.headers.add("uSisHetrLagScheduleMixin.h")
    # File slot
    lag_file_slot = get_rhs_first(ctx.rhs, T_LAG_FILE_SLOT)
    if not lag_file_slot:
        print "Error: unable to locate lag file slot for hetr lag schedule."
        fail_resolve(ctx, T_LAG_FILE_SLOT)
    lag_file_slot = int(lag_file_slot)
    assert lag_file_slot >= OPT_SLOT_MIN
    assert lag_file_slot <= OPT_SLOT_MAX
    lag_file_slot_enum = "uOpt_qc_schedule_lag_file_slot" + str(lag_file_slot)
    # Scalar slot
    lag_slot = get_rhs_first(ctx.rhs, T_LAG_SLOT)
    if not lag_slot:
        print "Error: unable to locate lag slot for hetr lag schedule."
        fail_resolve(ctx, T_LAG_SLOT)
    lag_slot = int(lag_slot)
    assert lag_slot >= OPT_SLOT_MIN
    assert lag_slot <= OPT_SLOT_MAX
    lag_slot_enum = "uOpt_qc_schedule_lag_slot" + str(lag_slot)
    # Cpp code
    sched_cpp = ", ".join(
        ("uSisHetrLagScheduleMixin<" +
         ctx.glue_cpp_name,
         lag_file_slot_enum,
         lag_slot_enum))
    sched_cpp = sched_cpp + ">"
    return sched_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for dynamic ess schedule
def parse_sched_dyn_ess(ctx):
    '''Dynamic ess schedule'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_DYN_ESS
    # Add header
    ctx.headers.add("uSisDynamicEssScheduleMixin.h")
    # Ess slot
    ess_slot = get_rhs_first(ctx.rhs, T_ESS_SLOT)
    if not ess_slot:
        fail_resolve(ctx, T_ESS_SLOT)
    ess_slot = int(ess_slot)
    assert ess_slot >= OPT_SLOT_MIN
    assert ess_slot <= OPT_SLOT_MAX
    ess_slot_enum = "uOpt_qc_schedule_dynamic_ess_thresh_slot" + str(ess_slot)
    # Cpp code
    sched_cpp = "uSisDynamicEssScheduleMixin<" + \
        ctx.glue_cpp_name + ", " + ess_slot_enum + ">"
    return sched_cpp

###########################################
# Parser: select
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string for Delphic power selector
def parse_sel_del_pow(ctx):
    '''Delphic power selector'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_DEL_POW
    # Add headers
    ctx.headers.add("uSisSelectDelphicPower.h")
    ctx.headers.add("uSisSubSimAccess.h")
    ctx.headers.add("uSisSampleCopier_unsafe.h")
    # Summary
    summary_cpp = get_cpp(ctx, T_SUMMARY, ctx.tabs.SUMMARY)
    # Sub
    sub_val = get_rhs_first(ctx.rhs, T_SUB)
    if (not sub_val) or (not isinstance(sub_val, str)):
        print "Error: unable to parse sub for Delphic power selector."
        fail_resolve(ctx, T_SUB)
    sub_glue_cpp_name = get_glue_cpp_name(sub_val)
    if sub_glue_cpp_name == ctx.glue_cpp_name:
        print "Error: infinite recursion: Parent == Child sub for Delphic power selector."
        fail_resolve(ctx, T_SUB)
    sub_sim_cpp_name = sub_glue_cpp_name + "::sim_t"
    # Handle parallel vs serial
    access_cpp = ""
    if ctx.is_top:
        # Parallel
        access_cpp = "uSisSubSimAccess_threaded<" + sub_sim_cpp_name + ">"
    else:
        # Serial
        access_cpp = "uSisSubSimAccess_serial<" + sub_sim_cpp_name + ">"
    # Sample copier
    sample_copier_cpp = "uSisSampleCopier_unsafe"
    # Slots
    slot = get_rhs_first(ctx.rhs, T_SLOT)
    if not slot:
        print "Error: unable to locate slot for Delphic power selector."
        fail_resolve(ctx, T_SLOT)
    slot = int(slot)
    assert slot >= OPT_SLOT_MIN
    assert slot <= OPT_SLOT_MAX
    delphic_steps_slot_enum = "uOpt_delphic_steps_slot" + str(slot)
    delphic_alpha_slot_enum = "uOpt_delphic_power_alpha_slot" + str(slot)
    # Final cpp
    cpp = ", ".join(
        ("uSisSelectDelphicPower<" +
         ctx.glue_cpp_name,
         summary_cpp,
         access_cpp,
         sample_copier_cpp,
         delphic_steps_slot_enum,
         delphic_alpha_slot_enum))
    cpp = cpp + ">"
    return CppWithSubInfo(cpp=cpp, sub=sub_val)

# @param ctx - Ctx named tuple
# @return C++ atom string for power weight selector
def parse_sel_pow(ctx):
    '''Power weight selector'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_POW
    # Add header
    ctx.headers.add("uSisSelectPower.h")
    # Slot
    alpha_slot = get_rhs_first(ctx.rhs, T_ALPHA_SLOT)
    if not alpha_slot:
        print "Error: unable to locate alpha slot for power weight selector."
        fail_resolve(ctx, T_ALPHA_SLOT)
    alpha_slot = int(alpha_slot)
    assert alpha_slot >= OPT_SLOT_MIN
    assert alpha_slot <= OPT_SLOT_MAX
    alpha_slot_enum = "uOpt_qc_sampler_power_alpha_slot" + str(alpha_slot)
    # Cpp code
    sel_cpp = "uSisSelectPower<" + ctx.glue_cpp_name + ", " + alpha_slot_enum \
        + ">"
    return sel_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for uniform selector
def parse_sel_unif(ctx):
    '''Uniform selector'''
    # Add header
    ctx.headers.add("uSisSelectUnif.h")
    # Cpp code
    sel_cpp = "uSisSelectUnif<" + ctx.glue_cpp_name + ">"
    return sel_cpp

###########################################
# Parser: quality control
###########################################

# @return Default Parseable for quality control
def defcb_qc():
    '''Default callback for quality control'''
    tok_str = get_tok_str(T_NULL)
    print "[json2cpp] Defaulting quality control to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return C++ atom string for null quality control
def parse_qc_null(ctx):
    # Add header
    ctx.headers.add("uSisNullQcSimLevelMixin.h")
    # Cpp code
    qc_cpp = "uSisNullQcSimLevelMixin<" + ctx.glue_cpp_name + ">"
    return qc_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for rejection-control quality control
def parse_qc_rc(ctx):
    '''Rejection-control quality control'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_RC
    # Add header
    ctx.headers.add("uSisRejectionControlQcSimLevelMixin.h")
    # Schedule
    sched_cpp = get_cpp(ctx, T_SCHED, ctx.tabs.SCHED)
    # Scale
    scale_cpp = get_cpp(ctx, T_SCALE, ctx.tabs.RC_SCALE)
    # Regrow
    regrow_cpp = get_rc_regrow(ctx)
    # Heartbeat
    heartbeat_cpp = "true" if ctx.is_top else "false"
    # Norm
    # There appears to be no reason why we wouldn't want to estimate the
    # rejection control normalization constant as the typical use case will
    # involve weight interpolation which is random for each run. Here is the
    # original code which avoided normalization if the top-level trial runner
    # was canonical, but even under this condition (owing to weight
    # interpolation), we should still normalize:
    #       norm_cpp = "false" if is_tr_canon_top(ctx) else "true"
    norm_cpp = "true"
    # Final cpp
    rc_cpp = ", ".join(
        ("uSisRejectionControlQcSimLevelMixin<" +
         ctx.glue_cpp_name,
         sched_cpp,
         scale_cpp,
         regrow_cpp,
         heartbeat_cpp,
         norm_cpp))
    rc_cpp = rc_cpp + ">"
    return rc_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for partial rejection-control quality control
def parse_qc_rcpart(ctx):
    '''Partial rejection-control quality control'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_RCPART
    # Add header
    ctx.headers.add("uSisPartialRejectionControlQcSimLevelMixin.h")
    # Schedule
    sched_cpp = get_cpp(ctx, T_SCHED, ctx.tabs.SCHED)
    # Scale
    scale_cpp = get_cpp(ctx, T_SCALE, ctx.tabs.RC_SCALE)
    # Regrow
    regrow_cpp = get_rc_regrow(ctx)
    # Final cpp
    rcpart_cpp = ", ".join(
        ("uSisPartialRejectionControlQcSimLevelMixin<" +
         ctx.glue_cpp_name,
         sched_cpp,
         scale_cpp,
         regrow_cpp))
    rcpart_cpp = rcpart_cpp + ">"
    return rcpart_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for resampling quality control
def parse_qc_rs(ctx):
    '''Resampling quality control'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_RS
    # Add header
    ctx.headers.add("uSisResamplingQcSimLevelMixin.h")
    # Schedule
    sched_cpp = get_cpp(ctx, T_SCHED, ctx.tabs.SCHED)
    # Sampler
    sampler_sub = None
    sampler_cpp = get_cpp(ctx, T_SAMPLER, ctx.tabs.RS_SAMPLER)
    if isinstance(sampler_cpp, CppWithSubInfo):
        sampler_sub = sampler_cpp.sub
        sampler_cpp = sampler_cpp.cpp
    assert isinstance(sampler_cpp, str)
    # Heartbeat
    heartbeat_cpp = "true" if ctx.is_top else "false"
    # Final cpp
    rs_cpp = ", ".join(
        ("uSisResamplingQcSimLevelMixin<" +
         ctx.glue_cpp_name,
         sched_cpp,
         sampler_cpp,
         heartbeat_cpp))
    rs_cpp = rs_cpp + ">"
    return CppWithSubInfo(cpp=rs_cpp, sub=sampler_sub)

###########################################
# Parser: resamplers
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string for Delphic power resampler
def parse_rs_sampler_del_pow(ctx):
    '''Delphic power resampler'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_DEL_POW
    # Add headers
    ctx.headers.add("uSisDelphicPowerSamplerRsMixin.h")
    ctx.headers.add("uSisSubSimAccess.h")
    ctx.headers.add("uSisSampleCopier_unsafe.h")
    # Summary
    summary_cpp = get_cpp(ctx, T_SUMMARY, ctx.tabs.SUMMARY)
    # Sub
    sub_val = get_rhs_first(ctx.rhs, T_SUB)
    if (not sub_val) or (not isinstance(sub_val, str)):
        print "Error: unable to parse sub for Delphic power resampler."
        fail_resolve(ctx, T_SUB)
    sub_glue_cpp_name = get_glue_cpp_name(sub_val)
    sub_sim_cpp = sub_glue_cpp_name + "::sim_t"
    # Sample copier
    sample_copier_cpp = "uSisSampleCopier_unsafe"
    # Slots
    slot = get_rhs_first(ctx.rhs, T_SLOT)
    if not slot:
        print "Error: unable to locate slot for Delphic power resampler."
        fail_resolve(ctx, T_SLOT)
    slot = int(slot)
    assert slot >= OPT_SLOT_MIN
    assert slot <= OPT_SLOT_MAX
    delphic_steps_slot_enum = "uOpt_delphic_steps_slot" + str(slot)
    delphic_alpha_slot_enum = "uOpt_delphic_power_alpha_slot" + str(slot)
    # Final cpp
    cpp = ", ".join(
        ("uSisDelphicPowerSamplerRsMixin<" +
         ctx.glue_cpp_name,
         summary_cpp,
         sub_sim_cpp,
         sample_copier_cpp,
         delphic_steps_slot_enum,
         delphic_alpha_slot_enum))
    cpp = cpp + ">"
    return CppWithSubInfo(cpp=cpp, sub=sub_val)

# @param ctx - Ctx named tuple
# @return C++ atom string for power resampler
def parse_rs_sampler_power(ctx):
    '''Power resampler'''
    # Pre-conditions
    assert isinstance(ctx.rhs, dict)
    assert T_TYPE in ctx.rhs
    assert ctx.rhs[T_TYPE] in T_POW
    # Add header
    ctx.headers.add("uSisPowerSamplerRsMixin.h")
    # Slot
    alpha_slot = get_rhs_first(ctx.rhs, T_ALPHA_SLOT)
    if not alpha_slot:
        print "Error: unable to locate alpha slot for power weight resampler."
        fail_resolve(ctx, T_ALPHA_SLOT)
    alpha_slot = int(alpha_slot)
    assert alpha_slot >= OPT_SLOT_MIN
    assert alpha_slot <= OPT_SLOT_MAX
    alpha_slot_enum = "uOpt_qc_sampler_power_alpha_slot" + str(alpha_slot)
    # Cpp code
    cpp = "uSisPowerSamplerRsMixin<" + ctx.glue_cpp_name + ", " + \
          alpha_slot_enum + ">"
    return cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for residual resampler
def parse_rs_sampler_residual(ctx):
    '''Residual resampler'''
    # Add headers
    ctx.headers.add("uSisResidualSamplerRsMixin.h")
    # Cpp code
    cpp = "uSisResidualSamplerRsMixin<" + ctx.glue_cpp_name + ">"
    return cpp

###########################################
# Parser: rejection control scale
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string for fixed quantile scale
def parse_rc_scale_fixed_quant(ctx):
    '''Rejection control scale: fixed quantile'''
    # Add headers
    ctx.headers.add("uSisFixedQuantileScaleRcMixin.h")
    # Cpp code
    cpp = "uSisFixedQuantileScaleRcMixin<" + ctx.glue_cpp_name + ">"
    return cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for interpolated weights scale
def parse_rc_scale_interp_weights(ctx):
    '''Rejection control scale: interp weights'''
    # Add headers
    ctx.headers.add("uSisInterpWeightsScaleRcMixin.h")
    # Cpp code
    cpp = "uSisInterpWeightsScaleRcMixin<" + ctx.glue_cpp_name + ">"
    return cpp

###########################################
# Parser: summary (used by Delphic modules)
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string for mean weights summary
def parse_summary_mean_w(ctx):
    '''Summary: mean weight'''
    # Add headers
    ctx.headers.add("uSisQcSummaryMeanWeight.h")
    # Cpp code
    cpp = "uSisQcSummaryMeanWeight"
    return cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for median weights summary
def parse_summary_median_w(ctx):
    '''Summary: median weight'''
    # Add headers
    ctx.headers.add("uSisQcSummaryMedianWeight.h")
    # Cpp code
    cpp = "uSisQcSummaryMedianWeight"
    return cpp

###########################################
# Parser: growth
###########################################

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_grow_mloc(ctx):
    '''Multiple locii growth'''
    # Add headers
    ctx.headers.add("uSisSeoMlocGrowthMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    grow_sim_cpp = ""
    if use_threaded:
        grow_sim_cpp = "uSisSeoMloc::GrowthSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        grow_sim_cpp = "uSisSeoMloc::GrowthSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    grow_cpp = "uSisSeoMloc::GrowthMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=grow_sim_cpp, sam=grow_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_grow_sloc(ctx):
    '''Single locus growth'''
    # Add headers
    ctx.headers.add("uSisSeoSlocGrowthMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    grow_sim_cpp = ""
    if use_threaded:
        grow_sim_cpp = "uSisSeoSloc::GrowthSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        grow_sim_cpp = "uSisSeoSloc::GrowthSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    grow_cpp = "uSisSeoSloc::GrowthMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=grow_sim_cpp, sam=grow_cpp)

###########################################
# Parser: diameter
###########################################

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_diam_hetr(ctx):
    '''Heterogeneous diameter nodes'''
    # Add headers
    ctx.headers.add("uSisHetrNodeRadiusMixin.h")
    # Cpp code for simulation
    radius_sim_cpp = "uSisHetrNodeRadiusSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    radius_cpp = "uSisHetrNodeRadiusMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=radius_sim_cpp, sam=radius_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_diam_homg(ctx):
    '''Homogeneous diameter nodes'''
    # Add headers
    ctx.headers.add("uSisHomgNodeRadiusMixin.h")
    # Cpp code for simulation
    radius_sim_cpp = "uSisHomgNodeRadiusSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    radius_cpp = "uSisHomgNodeRadiusMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=radius_sim_cpp, sam=radius_cpp)

###########################################
# Parser: energy
###########################################

# @return Default Parseable for energy
def defcb_ener():
    '''Default callback for energy'''
    tok_str = get_tok_str(T_UNIF)
    print "[json2cpp] Defaulting energy to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @param weight_policy - policy for updating importance weight
# @return MixinInfo named tuple
def parse_ener_bend(ctx, weight_policy="uSisWeightPolicyBoltz2Boltz"):
    '''Boltzmann bend energy distribution'''
    # Add headers
    ctx.headers.add("uSisSeoBendEnergyMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    ener_sim_cpp = ""
    if use_threaded:
        ener_sim_cpp = "uSisSeoBendEnergySimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        ener_sim_cpp = "uSisSeoBendEnergySimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    ener_cpp = "uSisSeoBendEnergyMixin<" + ctx.glue_cpp_name + ", " + weight_policy + ">"
    return MixinInfo(sim=ener_sim_cpp, sam=ener_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_ener_intr_fail(ctx):
    '''Boltzmann interaction failure fraction energy distribution'''
    # Add headers
    ctx.headers.add("uSisIntrFailEnergyMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    ener_sim_cpp = ""
    if use_threaded:
        ener_sim_cpp = "uSisIntrFailEnergySimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        ener_sim_cpp = "uSisIntrFailEnergySimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    ener_cpp = "uSisIntrFailEnergyMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=ener_sim_cpp, sam=ener_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_ener_unif(ctx):
    '''Uniform energy distribution'''
    # Add headers
    ctx.headers.add("uSisUnifEnergyMixin.h")
    # Cpp code for simulation
    ener_sim_cpp = "uSisUnifEnergySimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    ener_cpp = "uSisUnifEnergyMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=ener_sim_cpp, sam=ener_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_ener_rigid_unif(ctx):
    '''Sample from biased persistence length Boltzmann with uniform target'''
    return parse_ener_bend(ctx=ctx,
                           weight_policy="uSisWeightPolicyBoltz2Unif")

# @TODO - create parse_ener_add(ctx) - a general purpose additive energy model

###########################################
# Parser: chr-chr interaction
###########################################

# @return Default Parseable for chr-chr interaction
def defcb_intr_chr():
    '''Default callback for chr-chr interaction'''
    tok_str = get_tok_str(T_NULL)
    print "[json2cpp] Defaulting chromatin-chromatin interactions to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_chr_null(ctx):
    '''Null chr-chr interaction'''
    # Add headers
    ctx.headers.add("uSisNullIntrChrMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisNullIntrChrSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisNullIntrChrMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_chr_prox(ctx):
    '''Knock-in, knock-out proximity chr-chr interactions'''
    # Add headers
    ctx.headers.add("uSisMkiMkoSeoMlocIntrChrMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisMkiMkoSeoMlocIntrChrSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisMkiMkoSeoMlocIntrChrMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_chr_prox_budg(ctx):
    '''Budgeted knock-in, knock-out proximity chr-chr interactions'''
    # Add headers
    ctx.headers.add("uSisMkiMkoSeoMlocBudgIntrChrMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    intr_sim_cpp = ""
    if use_threaded:
        intr_sim_cpp = "uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        intr_sim_cpp = "uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisMkiMkoSeoMlocBudgIntrChrMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

###########################################
# Parser: lamina interaction
###########################################

# @return Default Parseable for lamina interaction
def defcb_intr_lam():
    '''Default callback for lamina interaction'''
    tok_str = get_tok_str(T_NULL)
    print "[json2cpp] Defaulting lamina interactions to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_null(ctx):
    '''Null lamina interaction'''
    # Add headers
    ctx.headers.add("uSisNullIntrLamMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisNullIntrLamSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisNullIntrLamMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox_sphere(ctx):
    '''Knock-in, knock-out lamina interactions for spherical nucleus'''
    # Add headers
    ctx.headers.add("uSisSeoSphereIntrLamMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisSeoSphereIntrLamSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoSphereIntrLamMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox_budg_sphere(ctx):
    '''Budgeted knock-in|knock-out lamina interactions for spherical nucleus'''
    # Add headers
    ctx.headers.add("uSisSeoSphereBudgIntrLamMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    intr_sim_cpp = ""
    if use_threaded:
        intr_sim_cpp = "uSisSeoSphereBudgIntrLamSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        intr_sim_cpp = "uSisSeoSphereBudgIntrLamSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoSphereBudgIntrLamMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)
    
# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox_ellip(ctx):
    '''Knock-in, knock-out lamina interactions for ellipsoid nucleus'''
    # Add headers
    ctx.headers.add("uSisSeoEllipsoidIntrLamMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisSeoEllipsoidIntrLamSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoEllipsoidIntrLamMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

    # @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox_budg_ellip(ctx):
    '''Budgeted knock-in|knock-out lamina interactions for ellipsoid nucleus'''
    # Add headers
    ctx.headers.add("uSisSeoEllipsoidBudgIntrLamMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    intr_sim_cpp = ""
    if use_threaded:
        intr_sim_cpp = "uSisSeoEllipsoidBudgIntrLamSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        intr_sim_cpp = "uSisSeoEllipsoidBudgIntrLamSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoEllipsoidBudgIntrLamMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox(ctx):
    '''Knock-in, knock-out proximity lamina interactions'''
    # Nuclear field is to emit lamina interaction callback
    if ctx.cbfunc_intr_lam_prox is not None:
        return ctx.cbfunc_intr_lam_prox(ctx)
    fail_resolve(ctx, T_INTR_LAM)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_lam_prox_budg(ctx):
    '''Budgeted knock-in, knock-out proximity lamina interactions'''
    # Nuclear field is to emit lamina interaction callback
    if ctx.cbfunc_intr_lam_prox_budg is not None:
        return ctx.cbfunc_intr_lam_prox_budg(ctx)
    fail_resolve(ctx, T_INTR_LAM)

###########################################
# Parser: Nuclear body interaction
###########################################

# @return Default Parseable for nuclear body interaction
def defcb_intr_nucb():
    '''Default callback for nuclear body interaction'''
    tok_str = get_tok_str(T_NULL)
    print "[json2cpp] Defaulting nuclear body interactions to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_nucb_null(ctx):
    '''Null nuclear body interaction'''
    # Add headers
    ctx.headers.add("uSisNullIntrNucbMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisNullIntrNucbSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisNullIntrNucbMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_nucb_prox(ctx):
    '''Knock-in, knock-out proximity nuclear body interactions'''
    # Add headers
    ctx.headers.add("uSisSeoSphereIntrNucbMixin.h")
    # Cpp code for simulation
    intr_sim_cpp = "uSisSeoSphereIntrNucbSimLevelMixin<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoSphereIntrNucbMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_intr_nucb_prox_budg(ctx):
    '''Budgeted knock-in, knock-out proximity nuclear body interactions'''
    # Add headers
    ctx.headers.add("uSisSeoSphereBudgIntrNucbMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    intr_sim_cpp = ""
    if use_threaded:
        intr_sim_cpp = "uSisSeoSphereBudgIntrNucbSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        intr_sim_cpp = "uSisSeoSphereBudgIntrNucbSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    intr_cpp = "uSisSeoSphereBudgIntrNucbMixin<" + ctx.glue_cpp_name + ">"
    return MixinInfo(sim=intr_sim_cpp, sam=intr_cpp)

###########################################
# Parser: Collision
###########################################

# @return Default Parseable for collision support
def defcb_collis():
    '''Default callback for collision mixin'''
    tok_str = get_tok_str(T_HARD_SHELL)
    print "[json2cpp] Defaulting collision to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_collis_hard_shell(ctx):
    '''Returns MixinInfo named tuple for hard shell collisions'''
    # Add headers
    ctx.headers.add("uBroadPhase.h")
    ctx.headers.add("uSisHardShellCollisionMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    collis_sim_cpp = ""
    if use_threaded:
        collis_sim_cpp = "uSisHardShellCollisionSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        collis_sim_cpp = "uSisHardShellCollisionSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    collis_cpp = "uSisHardShellCollisionMixin<" + \
        ctx.glue_cpp_name + ", uBroadPhase_t>"
    return MixinInfo(sim=collis_sim_cpp, sam=collis_cpp)

# @param ctx - Ctx named tuple
# @return MixinInfo named tuple
def parse_collis_ofa(ctx):
    '''Returns MixinInfo named tuple for overlap factor (ofa) collisions'''
    # Add headers
    ctx.headers.add("uBroadPhase.h")
    ctx.headers.add("uSisOfaCollisionMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    collis_sim_cpp = ""
    if use_threaded:
        collis_sim_cpp = "uSisOfaCollisionSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        collis_sim_cpp = "uSisOfaCollisionSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    collis_cpp = "uSisOfaCollisionMixin<" + \
        ctx.glue_cpp_name + ", uBroadPhase_t>"
    return MixinInfo(sim=collis_sim_cpp, sam=collis_cpp)

###########################################
# Parser: Nucleus
###########################################

# @return Default Parseable for nuclear confinement support
def defcb_nuc():
    '''Default callback for nuclear mixin'''
    tok_str = get_tok_str(T_SPHERE)
    print "[json2cpp] Defaulting nucleus to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return NucInfo named tuple
def parse_nuc_sphere(ctx):
    '''Returns MixinInfo named tuple for spherical nuclear mixins'''
    # Add headers
    ctx.headers.add("uSisSphereNuclearMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    nuc_sim_cpp = ""
    if use_threaded:
        nuc_sim_cpp = "uSisSphereNuclearSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        nuc_sim_cpp = "uSisSphereNuclearSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    nuc_cpp = "uSisSphereNuclearMixin<" + ctx.glue_cpp_name + ">"
    return NucInfo(sim=nuc_sim_cpp, sam=nuc_cpp,
                   cbfunc_intr_lam_prox=parse_intr_lam_prox_sphere,
                   cbfunc_intr_lam_prox_budg=parse_intr_lam_prox_budg_sphere)

# @param ctx - Ctx named tuple
# @return NucInfo named tuple
def parse_nuc_ellip(ctx):
    '''Returns MixinInfo named tuple for ellipsoid nuclear mixins'''
    # Add headers
    ctx.headers.add("uSisEllipsoidNuclearMixin.h")
    # Cpp code for simulation
    use_threaded = is_tr_canon_top(ctx)
    nuc_sim_cpp = ""
    if use_threaded:
        nuc_sim_cpp = "uSisEllipsoidNuclearSimLevelMixin_threaded<" + ctx.glue_cpp_name + ">"
    else:
        nuc_sim_cpp = "uSisEllipsoidNuclearSimLevelMixin_serial<" + ctx.glue_cpp_name + ">"
    # Cpp code for sample
    nuc_cpp = "uSisEllipsoidNuclearMixin<" + ctx.glue_cpp_name + ">"
    return NucInfo(sim=nuc_sim_cpp, sam=nuc_cpp,
                   cbfunc_intr_lam_prox=parse_intr_lam_prox_ellip,
                   cbfunc_intr_lam_prox_budg=parse_intr_lam_prox_budg_ellip)

###########################################
# Parser: Seed
###########################################

# @return Default Parseable for seed mixin support
def defcb_seed():
    '''Default callback for seed mixin'''
    tok_str = get_tok_str(T_UNIF_CUBE)
    print "[json2cpp] Defaulting seed to " + tok_str
    return Parseable(key=tok_str, rhs=tok_str)

# @param ctx - Ctx named tuple
# @return C++ string for simulation seed mixin
def parse_seed_unif_cube(ctx):
    '''Returns MixinInfo named tuple for uniform cube seed mixin'''
    # Add headers
    ctx.headers.add("uSisSeoUnifCubeSeedMixin.h")
    seed_cpp = "uSisSeoUnifCubeSeedMixin<" + ctx.glue_cpp_name + ">"
    return seed_cpp

###########################################
# Parser: callback tables
###########################################

# @param tab - Parser table: a callback dictionary
# @param tok - string key or list of string keys
# @param par - parser function
# Registers all tokens to parser table with same parser
def reg_par(tab, tok, par):
    tok_ls = tok if isinstance(tok, list) else [tok]
    for k in tok_ls:
        tab[k] = par

# @return Parsers named tuple of registered callback tables
def init_pars():
    '''Register all parsers with callback tables'''
    # Parser table for 'collision' mixins
    PARS_COLLIS = {}
    reg_par(PARS_COLLIS, T_HARD_SHELL, parse_collis_hard_shell)
    reg_par(PARS_COLLIS, T_OFA, parse_collis_ofa)

    # Parser table for 'node radius' mixins
    PARS_DIAM = {}
    reg_par(PARS_DIAM, T_HETR, parse_diam_hetr)
    reg_par(PARS_DIAM, T_HOMG, parse_diam_homg)

    # Parser table for 'energy' mixins
    PARS_ENER = {}
    reg_par(PARS_ENER, T_BEND, parse_ener_bend)
    reg_par(PARS_ENER, T_INTR_FAIL, parse_ener_intr_fail)
    reg_par(PARS_ENER, T_UNIF, parse_ener_unif)
    reg_par(PARS_ENER, T_RIGID_UNIF, parse_ener_rigid_unif)

    # Parser table for 'growth' mixins
    PARS_GROW = {}
    reg_par(PARS_GROW, T_MLOC, parse_grow_mloc)
    reg_par(PARS_GROW, T_SLOC, parse_grow_sloc)

    # Parser table for 'chr-chr interaction' mixins
    PARS_INTR_CHR = {}
    reg_par(PARS_INTR_CHR, T_NULL, parse_intr_chr_null)
    reg_par(PARS_INTR_CHR, T_PROX, parse_intr_chr_prox)
    reg_par(PARS_INTR_CHR, T_PROX_BUDG, parse_intr_chr_prox_budg)

    # Parser table for 'lamina interaction' mixins
    PARS_INTR_LAM = {}
    reg_par(PARS_INTR_LAM, T_NULL, parse_intr_lam_null)
    reg_par(PARS_INTR_LAM, T_PROX, parse_intr_lam_prox)
    reg_par(PARS_INTR_LAM, T_PROX_BUDG, parse_intr_lam_prox_budg)

    # Parser table for 'nuclear body interaction' mixins
    PARS_INTR_NUCB = {}
    reg_par(PARS_INTR_NUCB, T_NULL, parse_intr_nucb_null)
    reg_par(PARS_INTR_NUCB, T_PROX, parse_intr_nucb_prox)
    reg_par(PARS_INTR_NUCB, T_PROX_BUDG, parse_intr_nucb_prox_budg)

    # Parser table for 'nuclear' mixin
    PARS_NUC = {}
    reg_par(PARS_NUC, T_SPHERE, parse_nuc_sphere)
    reg_par(PARS_NUC, T_ELLIP, parse_nuc_ellip)

    # Parser table for 'quality control' mixins
    PARS_QC = {}
    reg_par(PARS_QC, T_NULL, parse_qc_null)
    reg_par(PARS_QC, T_RC, parse_qc_rc)
    reg_par(PARS_QC, T_RCPART, parse_qc_rcpart)
    reg_par(PARS_QC, T_RS, parse_qc_rs)

    # Parser table for 'rejection-control scale' mixins
    PARS_RC_SCALE = {}
    reg_par(PARS_RC_SCALE, T_FIXED_QUANT, parse_rc_scale_fixed_quant)
    reg_par(PARS_RC_SCALE, T_INTERP_WEIGHTS, parse_rc_scale_interp_weights)

    # Parser table for 'resampling resampler' mixins
    PARS_RS_SAMPLER = {}
    reg_par(PARS_RS_SAMPLER, T_POW, parse_rs_sampler_power)
    reg_par(PARS_RS_SAMPLER, T_RESID, parse_rs_sampler_residual)
    reg_par(PARS_RS_SAMPLER, T_DEL_POW, parse_rs_sampler_del_pow)

    # Parser table for 'schedule' mixins
    PARS_SCHED = {}
    reg_par(PARS_SCHED, T_LAG_HETR, parse_sched_lag_hetr)
    reg_par(PARS_SCHED, T_LAG_HOMG, parse_sched_lag_homg)
    reg_par(PARS_SCHED, T_LAG_HOMG_UNIF, parse_sched_lag_homg_unif)
    reg_par(PARS_SCHED, T_DYN_ESS, parse_sched_dyn_ess)

    # Parser table for 'seed' mixins
    PARS_SEED = {}
    reg_par(PARS_SEED, T_UNIF_CUBE, parse_seed_unif_cube)

    # Parser table for 'select' mixins
    PARS_SEL = {}
    reg_par(PARS_SEL, T_POW, parse_sel_pow)
    reg_par(PARS_SEL, T_UNIF, parse_sel_unif)
    reg_par(PARS_SEL, T_DEL_POW, parse_sel_del_pow)

    # Parser table for 'summary' components used by Delphic modules
    PARS_SUMMARY = {}
    reg_par(PARS_SUMMARY, T_MEAN_W, parse_summary_mean_w)
    reg_par(PARS_SUMMARY, T_MEDIAN_W, parse_summary_median_w)

    # Parser table for 'trial runner' mixins
    PARS_TR = {}
    reg_par(PARS_TR, T_CANON, parse_tr_canon)
    reg_par(PARS_TR, T_LMRK, parse_tr_lmrk)

    return Parsers(COLLIS=PARS_COLLIS, DIAM=PARS_DIAM, ENER=PARS_ENER,
                   GROW=PARS_GROW, INTR_CHR=PARS_INTR_CHR,
                   INTR_LAM=PARS_INTR_LAM, INTR_NUCB=PARS_INTR_NUCB,
                   NUC=PARS_NUC, QC=PARS_QC, RC_SCALE=PARS_RC_SCALE,
                   RS_SAMPLER=PARS_RS_SAMPLER, SCHED=PARS_SCHED,
                   SEED=PARS_SEED, SEL=PARS_SEL, TR=PARS_TR,
                   SUMMARY=PARS_SUMMARY)

###########################################
# Non-configurable mixins
###########################################

# @param ctx - Ctx named tuple
# @return C++ atom string, also modifies ctx.headers
def get_sam(ctx):
    '''C++ atom string for sample type'''
    # Add headers
    ctx.headers.add("uSisSample.h")
    # Cpp code
    sam_cpp = "uSisSample<" + ctx.glue_cpp_name + ">"
    return sam_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string, also modifies ctx.headers
def get_sim(ctx):
    '''C++ atom string for simulation type'''
    # Add headers
    ctx.headers.add("uSisSim.h")
    # Cpp code
    sim_cpp = "uSisSim<" + ctx.glue_cpp_name + ">"
    return sim_cpp

# @param ctx - Ctx named tuple
# @return C++ atom string for regrowth rc mixin, also modifies ctx.headers
def get_rc_regrow(ctx):
    '''Rejection control regrowth'''
    # Handle parallel vs serial
    regrow_cpp = ""
    if ctx.is_top:
        ctx.headers.add("uSisParallelRegrowthRcMixin.h")
        regrow_cpp = "uSisParallelRegrowthRcMixin<" + ctx.glue_cpp_name + ">"
    else:
        ctx.headers.add("uSisSerialRegrowthRcMixin.h")
        regrow_cpp = "uSisSerialRegrowthRcMixin<" + ctx.glue_cpp_name + ">"
    return regrow_cpp

###########################################
# json2cpp
###########################################

# Currently, archetypes values must be a key into the global js dictionary
# like so:
#   {
#       <arch_name> : {<arch_definition>}
#       <glue> { arch : <arch_name> }
#   }
# It is okay for a glue dictionary to not have an archetype
# @param js - global JSON dictionary
# @param glue_rhs - glue dictionary
def get_arch(js, glue_rhs):
    '''Find archetype dictionary'''
    assert isinstance(glue_rhs, dict)
    arch = get_rhs_first(glue_rhs, T_ARCH)
    if not arch:
        # No archetype found
        return None
    # Within glue, archetype value must be string
    if not isinstance(arch, str):
        print "Error, malformed archetype value:\n\t" + str(arch)
        print "Exiting."
        sys.exit(1)
    # Glue's archetype value must be key in global JSON dictionary
    if arch not in js:
        print "Error, archetype not found in global JSON: " + arch
        print "Exiting."
        sys.exit(1)
    arch = js[arch]
    # The subsequent look-up into the global JSON dictionary must return
    # a dictionary value
    if not isinstance(arch, dict):
        print "Error, archetype value is not dictionary:\n\t" + str(arch)
        print "Exiting."
        sys.exit(1)
    return arch

# Utility for applying typedef to C++ glue members
# @param g - GlueCpp named tuple
# @return new instance of GlueCpp tuple with C++ typedefs
def typedef_glue(g):
    '''Typedef glue member classes'''
    tydf = lambda src, dst : "typedef " + src + " " + dst
    return g._replace(sim=tydf(g.sim, "sim_t"),
                      sample=tydf(g.sample, "sample_t"),
                      trial_runner=tydf(g.trial_runner, "trial_runner_mixin_t"),
                      quality_control=tydf(g.quality_control, "sim_level_qc_mixin_t"),
                      radius_sim=tydf(g.radius_sim, "sim_level_node_radius_mixin_t"),
                      radius=tydf(g.radius, "node_radius_mixin_t"),
                      growth_sim=tydf(g.growth_sim, "sim_level_growth_mixin_t"),
                      growth=tydf(g.growth, "growth_mixin_t"),
                      nuclear_sim=tydf(g.nuclear_sim, "sim_level_nuclear_mixin_t"),
                      nuclear=tydf(g.nuclear, "nuclear_mixin_t"),
                      collision_sim=tydf(g.collision_sim, "sim_level_collision_mixin_t"),
                      collision=tydf(g.collision, "collision_mixin_t"),
                      intr_chr_sim=tydf(g.intr_chr_sim, "sim_level_intr_chr_mixin_t"),
                      intr_chr=tydf(g.intr_chr, "intr_chr_mixin_t"),
                      intr_lam_sim=tydf(g.intr_lam_sim, "sim_level_intr_lam_mixin_t"),
                      intr_lam=tydf(g.intr_lam, "intr_lam_mixin_t"),
                      intr_nucb_sim=tydf(g.intr_nucb_sim, "sim_level_intr_nucb_mixin_t"),
                      intr_nucb=tydf(g.intr_nucb, "intr_nucb_mixin_t"),
                      energy_sim=tydf(g.energy_sim, "sim_level_energy_mixin_t"),
                      energy=tydf(g.energy, "energy_mixin_t"),
                      seed=tydf(g.seed, 'seed_mixin_t'))

# @param js - JSON dictionary
# @param glue_json_name - key must map to dict in js
# @param is_top - True if top level glue, False o/w
# @param headers - Set of C++ headers, may be empty
# @param tabs - Parsers named tuple of callback parser tables
# @return GlueInfo tuple
def get_glue(js, glue_json_name, is_top, headers, tabs):
    '''Parses each glue mixin type from JSON'''
    # Pre-conditions
    assert js
    assert isinstance(js, dict)
    assert glue_json_name
    assert isinstance(glue_json_name, str)
    # Fatal error if glue key does not exist
    if glue_json_name not in js:
        print "Error: glue name " + glue_json_name + " not found. Exiting."
        sys.exit(1)
    glue_rhs = js[glue_json_name]
    # Fatal error if mapped glue value is not dictionary
    if not isinstance(glue_rhs, dict):
        print "Error: glue value for " + glue_json_name + " is not dict."
        print "Exiting."
        sys.exit(1)
    # Create Ctx for parsing
    glue_cpp_name = get_glue_cpp_name(glue_json_name)
    ctx = Ctx(js=js,
              glue_cpp_name=glue_cpp_name,
              arch=get_arch(js, glue_rhs),
              rhs=glue_rhs,
              is_top=is_top,
              headers=headers,
              tabs=tabs,
              tr_info=None,
              cbfunc_intr_lam_prox=None,
              cbfunc_intr_lam_prox_budg=None)
    # Put together C++ atom string components
    tr_info = get_cpp(ctx=ctx, tok=T_TR, tab=tabs.TR, defcb=defcb_tr)
    ctx = ctx._replace(tr_info=tr_info)
    qc_sub = None
    qc_cpp = get_cpp(ctx=ctx, tok=T_QC, tab=tabs.QC, defcb=defcb_qc)
    if isinstance(qc_cpp, CppWithSubInfo):
        qc_sub = qc_cpp.sub
        qc_cpp = qc_cpp.cpp
    assert isinstance(qc_cpp, str)
    radius_info = get_cpp(ctx, T_DIAM, tabs.DIAM)
    grow_info = get_cpp(ctx, T_GROW, tabs.GROW)
    nuc_info = get_cpp(ctx=ctx, tok=T_NUC, tab=tabs.NUC, defcb=defcb_nuc)
    # Swap lamina interaction callbacks
    ctx = ctx._replace(cbfunc_intr_lam_prox=nuc_info.cbfunc_intr_lam_prox,
                       cbfunc_intr_lam_prox_budg=nuc_info.cbfunc_intr_lam_prox_budg)
    collis_info = get_cpp(ctx=ctx, tok=T_COLLIS, tab=tabs.COLLIS,
                          defcb=defcb_collis)
    intr_chr_info = get_cpp(ctx=ctx, tok=T_INTR_CHR, tab=tabs.INTR_CHR,
                            defcb=defcb_intr_chr)
    intr_lam_info = get_cpp(ctx=ctx, tok=T_INTR_LAM, tab=tabs.INTR_LAM,
                            defcb=defcb_intr_lam)
    intr_nucb_info = get_cpp(ctx=ctx, tok=T_INTR_NUCB, tab=tabs.INTR_NUCB,
                             defcb=defcb_intr_nucb)
    ener_info = get_cpp(ctx=ctx, tok=T_ENER, tab=tabs.ENER, defcb=defcb_ener)
    seed_cpp = get_cpp(ctx=ctx, tok=T_SEED, tab=tabs.SEED, defcb=defcb_seed)
    sam_cpp = get_sam(ctx)
    sim_cpp = get_sim(ctx)
    # Collate GlueCpp
    glue_cpp = GlueCpp(name=glue_cpp_name, sample=sam_cpp, sim=sim_cpp,
                       trial_runner=tr_info.cpp, quality_control=qc_cpp,
                       radius=radius_info.sam, radius_sim=radius_info.sim,
                       growth=grow_info.sam, growth_sim=grow_info.sim,
                       nuclear=nuc_info.sam, nuclear_sim=nuc_info.sim,
                       collision=collis_info.sam,
                       collision_sim=collis_info.sim,
                       intr_chr=intr_chr_info.sam,
                       intr_chr_sim=intr_chr_info.sim,
                       intr_lam=intr_lam_info.sam,
                       intr_lam_sim=intr_lam_info.sim,
                       intr_nucb=intr_nucb_info.sam,
                       intr_nucb_sim=intr_nucb_info.sim,
                       energy=ener_info.sam,
                       energy_sim=ener_info.sim,
                       seed=seed_cpp)
    # Apply typedefs
    glue_cpp = typedef_glue(glue_cpp)
    # Add JSON key to any sub-simulation glue
    return GlueInfo(tup=glue_cpp, sub_tr=tr_info.sub_tr,
                    sub_tr_sel=tr_info.sub_tr_sel, sub_qc=qc_sub)

# @param cpp_info - CppInfo named tuple
def dump_cpp_info(cpp_info):
    '''For debugging, dumps parsed CppInfo tuples to stdout'''
    # Print headers
    print "HEADERS:"
    for h in cpp_info.headers:
        print h
    # Print glues
    print "GLUES:"
    for g in cpp_info.glues:
        print "************************************"
        print str(g)

# Main utility method, converts JSON specification into collections of C++
# atom strings. Also captures required headers.
# @param json_path - file path to JSON model specification
def json2cpp(json_fpath):
    '''Generates C++ code from JSON specification'''
    print "[json2cpp] Processing: " + json_fpath
    js = load_json(json_fpath)
    assert(isinstance(js, dict))
    model_name = get_rhs_first(js, T_NAME)
    if not model_name or not isinstance(model_name, str):
        print "Error: JSON missing model 'name'. Exiting."
        sys.exit(1)
    glue_json_name = get_rhs_first(js, T_TOP)
    if not glue_json_name:
        print "Error: JSON missing top-level specification. Exiting."
        sys.exit(1)
    assert isinstance(glue_json_name, str)
    tabs = init_pars()
    glue_cpp_tups = []
    visited = set()
    headers = set()
    depth = 0
    IX_DEPTH = 0
    IX_JSON = 1
    IX_TYPE = 2
    TYPE_TOP = 0
    TYPE_TR = 1
    TYPE_TR_SEL = 2
    TYPE_QC = 3
    glue_json_stack = [(depth, glue_json_name, TYPE_TOP)]
    while glue_json_stack:
        # Process current glue
        tup = glue_json_stack.pop()
        sim_type = tup[IX_TYPE]
        depth = tup[IX_DEPTH]
        if depth > MAX_RECUR_DEPTH:
            print "Error: maximum recursion depth exceeded. Exiting."
            sys.exit(1)
        glue_json_name = tup[IX_JSON]
        assert glue_json_name
        if glue_json_name in visited:
            print "Warning: already visited sub: " + glue_json_name
            continue
        visited.add(glue_json_name)
        is_top = ((depth == 0) or ((depth == 1) and (sim_type == TYPE_QC)))
        glue_info = get_glue(js, glue_json_name, is_top, headers, tabs)
        glue_cpp_tups.append(glue_info.tup)
        # Process child sub-simulations
        next_depth = depth + 1
        if glue_info.sub_tr:
            glue_json_stack.append((next_depth, glue_info.sub_tr, TYPE_TR))
        if glue_info.sub_tr_sel:
            glue_json_stack.append((next_depth, glue_info.sub_tr_sel, TYPE_TR_SEL))
        if glue_info.sub_qc:
            glue_json_stack.append((next_depth, glue_info.sub_qc, TYPE_QC))
    return CppInfo(name=model_name, headers=headers, glues=glue_cpp_tups)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= JSON-2-CPP ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        help='Path to JSON model specification')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t-f = ' + args.file
    # Transform JSON to C++
    cpp_info = json2cpp(args.file)
    # Dump to stdout
    dump_cpp_info(cpp_info)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
