#!/usr/bin/python
# -*- coding: utf-8 -*-

##############################################################################
# Sets up local workspace by copying template scripts and configurations into
# a local directory which is ignored by source control.
##############################################################################

import os
import shutil
import sys

# @param silent_fail - If True, any errors will not be reported to console
# @param force - If True, will remove any existing local folder
def create_local(silent_fail=False, force=False):
    '''Create local script folder ignored by source control'''
    # Ignore our own script
    name_of_this_script = os.path.basename(sys.argv[0])

    # Obtain path to template directory
    src_template_dir_path = os.path.dirname(os.path.realpath(sys.argv[0]))

    # Will create a path to a local workspace directory in parent folder
    dst_local_workspace_dir_path = os.path.join(
        src_template_dir_path, '..', 'Local')
    
    # Check if local workspace already exists
    if os.path.exists(dst_local_workspace_dir_path):
        if force:
            # Remove existing local workspace
            shutil.rmtree(dst_local_workspace_dir_path)
        else:
            if not silent_fail:
                print 'Error: Path to local workspace exists already:\n\t' \
                    + dst_local_workspace_dir_path
                print 'Please remove this existing workspace prior to running this script.'
            return

    # Copy template directory tree to local workspace
    print 'Creating local workspace.'
    print 'Copying: ' + src_template_dir_path
    print '\tTo: ' + dst_local_workspace_dir_path
    shutil.copytree(src_template_dir_path, dst_local_workspace_dir_path)

    # Remove this script from workspace
    os.remove(os.path.join(dst_local_workspace_dir_path,
                           name_of_this_script))

    print 'Local workspace successfully created.'

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    create_local(silent_fail=False,
                 force=False)

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
