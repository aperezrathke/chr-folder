//****************************************************************************
// uBlb.h
//****************************************************************************

/**
 * @brief Bag of little bootstraps
 *
 *  Kleiner, A., Talwalkar, A., Sarkar, P., & Jordan, M. I. (2014). A scalable
 *      bootstrap for massive data. Journal of the Royal Statistical Society:
 *      Series B (Statistical Methodology), 76(4), 795-816.
 */

#ifndef uBlb_h
#define uBlb_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uGlobals.h"
#include "uRand.h"
#include "uShufflerFisherYates.h"
#include "uThread.h"
#include "uTypes.h"

#include <math.h>

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Default amount of work each BLB worker thread grabs
 */
#define U_BLB_DEFAULT_CHUNK_SIZE U_TO_UINT(1)

//****************************************************************************
// uBlb
//****************************************************************************

/**
 * Bag of little bootstraps, template member methods use the following
 *  policies:
 *
 *      stato_t: Outer replicate statistic policy, defines the statistic
 *          computed over the inner replicates. In BLB paper, this corresponds
 *          to the assessment of the quality of the estimator such as a
 *          confidence interval. Expected interface:
 *          ::init(repo) - initialize given the positive integer number of
 *              outer replicates, called once before any replicates generated
 *          ::start() - for symmetry with stati_t, called once immediately
 *              after init(...) to indicate start of statistical capture
 *          ::update(uUInt i, stati) - called after the i-th outer replicate
 *              has completed, intended to update (i.e. capture) outer
 *              estimator based on inner result
 *          ::finish() - called once after all outer replicates finished, may
 *              be used to normalize capture, export capture, etc.
 *
 *      stati_t: Inner replicate statistic policy, defines the statistic
 *          computed within each inner replicate. Expected interface:
 *          ::init(repi) - initialize given the positive number of inner
 *              replicates, called once before any outer replicates generated
 *          ::start() - called once for every *outer* replicate before (re)-
 *              generating inner replicates, intended to reset capture state
 *          ::update(ii, indices, counts, cache THREAD_ID) - called for each
 *              inner replicate 'ii', intended to update (i.e. capture) inner
 *              estimator where indices is vector specifying which data
 *              samples are to be accessed, counts is vector parallel to
 *              indices and is the multiplicity of each data sample, and cache
 *              implicitly provides an interface for accessing data samples at
 *              indices
 *          ::finish() - called once for each outer replicate, may be used to
 *              normalize capture, export capture, etc.
 *
 *      cache_t: Cache policy, intended to manage loading data samples from
 *          hard disk into RAM. Implicitly defines an interface for accessing
 *          resident data samples to stati_t, but the exact interface is left
 *          to client. Expected interface:
 *          ::update_first(indices) - Primes cache with first outer replicate
 *          ::update(i, indices) - Update cache such that data samples
 *              corresponding to indices are accessible
 *
 *  Here is pseudo-code outline of when template methods are called:
 *
 *  BLB(repo, repi, stato, stati, cache)
 *      // repo - integer number of outer replicates > 0
 *      // repi - integer number of inner replicates > 0
 *      // stato - handle to outer statistic policy
 *      // stati - handle to inner statistic policy
 *      // cache - handle to cache policy
 *      ...
 *      stato.init(repo);
 *      stati.init(repi);
 *      stato.start();
 *      // First outer replicate
 *      shuffle indices;
 *      cache.update_first(indices);
 *      stati.start();
 *      // Inner replicates
 *      for each ii in repi:
 *          compute counts
 *          stati.update(ii, indices, counts, cache);
 *      end for
 *      stati.finish();
 *      stato.update(0, stati);
 *      ...
 *      // Remaining outer replicates
 *      for i=1, i<repo; ++i:
 *          shuffle indices;
 *          cache.update(i, indices)
 *          stati.start()
 *          // Inner replicates
 *          for each ii in repi:
 *              compute counts;
 *              stati.update(ii, indices, counts, cache THREAD_ID);
 *          end for // Inner replicates
 *          ...
 *          stati.finish();
 *          stato.update(i, stati);
 *      end for // Outer replicates
 *      stato.finish();
 *  end // BLB
 */
class uBlb {
public:
    /**
     * @param n - Total number of data points
     * @param nexp - Exponent in [0,1], the number of unique samples per
     *      outer replicate is n^nexp
     * @return number of unique samples per outer replicate
     */
    inline static uUInt get_nun(const uUInt n, const uReal nexp) {
        // Compute number of unique samples per outer replicate
        uUInt nun = U_TO_UINT(pow(U_TO_REAL(n), nexp));
        nun = U_CLIP(nun, U_TO_UINT(1), n);
        return nun;
    }

    /**
     * Performs bag of little bootstraps
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * @param repo - Number of outer replicates, sampled w/o replacement
     * @param repi - Number of inner replicates, sampled w/ replacement
     * @param n - Total number of data points
     * @param nexp - Exponent in [0,1], the number of unique samples per
     *      outer replicate is n^nexp
     * @param stato - Outer replicate statistic policy
     * @param stati - Inner replicate statistic policy
     * @param cache - Cache policy
     * @param chunk_size - Only used for threaded builds, defines amount of
     *      work each thread grabs per work request
     * @return true if bootstrap okay, false o/w
     */
    template <typename stato_t, typename stati_t, typename cache_t>
    static bool boot(const uUInt repo,
                     const uUInt repi,
                     const uUInt n,
                     const uReal nexp,
                     stato_t& stato,
                     stati_t& stati,
                     cache_t& cache,
                     const uUInt chunk_size = U_BLB_DEFAULT_CHUNK_SIZE) {
        // Check if should early out
        if (early_out(repo, repi, n, nexp)) {
            return false;
        }
        // Compute number of unique samples per outer replicate
        const uUInt nun = get_nun(n, nexp);
        // Defer to engine
        return booteng(repo,
                       repi,
                       n,
                       nun,
                       stato,
                       stati,
                       cache,
                       chunk_size U_MAIN_THREAD_ID_ARG);
    }

    /**
     * Performs bag of little bootstraps
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * @param repo - Number of outer replicates, sampled w/o replacement
     * @param repi - Number of inner replicates, sampled w/ replacement
     * @param n - Total number of data points
     * @param nun - nun <= n, the number of unique samples per outer replicate
     * @param stato - Outer replicate statistic policy
     * @param stati - Inner replicate statistic policy
     * @param cache - Cache policy
     * @param chunk_size - Only used for threaded builds, defines amount of
     *      work each thread grabs per work request
     * @return true if bootstrap okay, false o/w
     */
    template <typename stato_t, typename stati_t, typename cache_t>
    static bool bootnun(const uUInt repo,
                        const uUInt repi,
                        const uUInt n,
                        const uUInt nun,
                        stato_t& stato,
                        stati_t& stati,
                        cache_t& cache,
                        const uUInt chunk_size = U_BLB_DEFAULT_CHUNK_SIZE) {
        // Check if should early out
        if (early_outnun(repo, repi, n, nun)) {
            return false;
        }
        // Defer to engine
        return booteng(repo,
                       repi,
                       n,
                       U_CLIP(nun, U_TO_UINT(1), n),
                       stato,
                       stati,
                       cache,
                       chunk_size U_MAIN_THREAD_ID_ARG);
    }

private:
    /**
     * Batch work utility for performing inner replicates in chunks
     */
    template <typename stati_t, typename cache_t>
    class batch_work
#ifdef U_BUILD_ENABLE_THREADS
        : public uMappableWork
#endif  // U_BUILD_ENABLE_THREADS
    {
    public:
        /**
         * Constructor
         * @param indices - Vector of unique indices, size is [1,n] and each
         *      element index is in integer range [0,n-1]
         * @param repi - Number of inner replicates, sampled w/ replacement
         * @param n - Total number of data points
         * @param stati - Inner replicate statistic policy
         * @param cache - Cache policy
         */
        batch_work(const uUIVecCol& indices,
                   const uUInt repi,
                   const uUInt n,
                   stati_t& stati,
                   const cache_t& cache)
            : m_indices(indices),
              m_repi(repi),
              m_n(n),
              m_stati(stati),
              m_cache(cache) {
            U_INIT_TLS_DATA_FOR_EACH_1_PARAM(m_counts, zeros, m_indices.n_elem);
        }

        /**
         * Perform inner replicates [ix_start, min(ix_start+count, repi))
         */
#ifdef U_BUILD_ENABLE_THREADS
        virtual
#endif  // U_BUILD_ENABLE_THREADS
            uBool
            do_range(const uUInt ix_start,
                     const uUInt count U_THREAD_ID_PARAM) {
            // Compute one past last inner replicate to process
            const uUInt end = std::min(ix_start + count, m_repi);
            // Random number generator
            uRand& rng = uRng;
            // Buffer for storing multinomial counts
            uUIVecCol& counts_buff = U_ACCESS_TLS_DATA(m_counts);
            // Perform inner replicate chunk
            for (uUInt i_repi = ix_start; i_repi < end; ++i_repi) {
                // Multinomial sample (with replacement)
                sample_counts(m_n, counts_buff, rng);
                // Update inner replicate statistic
                m_stati.update(
                    i_repi, m_indices, counts_buff, m_cache U_THREAD_ID_ARG);
            }
            // Check if we've processed last inner replicate
            return (end < m_repi);
        }

    private:
        /**
         * Utility performs uniform multinomial sampling
         * @param n - Total sum of counts
         * @param counts_buff - Buffer for storing counts at each sample
         * @param rng - Random number generator
         */
        static void sample_counts(const uUInt n,
                                  uUIVecCol& counts_buff,
                                  uRand& rng) {
            // @TODO - consider replacing with GNU Scientific Library
            //  multinomial implementation:
            //      https://www.gnu.org/software/gsl/
            //  which may be able to perform big-O(counts_buff.n_elem)
            //  random.sample() calls instead of big-O(n >> counts_buf.n_elem)
            uAssert(counts_buff.n_elem > U_TO_MAT_SZ_T(0));
            uAssert(counts_buff.n_elem <= U_TO_MAT_SZ_T(n));
            uRandom::uniform_int_distribution<uMatSz_t> dist(
                U_TO_MAT_SZ_T(0), counts_buff.n_elem - U_TO_MAT_SZ_T(1));
            counts_buff.zeros();
            for (uUInt i = U_TO_UINT(0); i < n; ++i) {
                const uMatSz_t index = rng.sample(dist);
                uAssertBounds(index, U_TO_MAT_SZ_T(0), counts_buff.n_elem);
                ++(counts_buff.at(index));
            }
        }

        /**
         * Sub-sampled indices vector
         */
        const uUIVecCol& m_indices;
        /**
         * Number of inner replicates
         */
        const uUInt m_repi;
        /**
         * Total number of data points
         */
        const uUInt m_n;
        /**
         * Inner replicate statistic policy
         */
        stati_t& m_stati;
        /**
         * Cache policy
         */
        const cache_t& m_cache;
        /**
         * Thread-local multinomial count buffers
         */
        U_DECLARE_TLS_DATA(uUIVecCol, m_counts);
    };

    /**
     * Engine performs bag of little bootstraps
     * @param repo - Number of outer replicates, sampled w/o replacement
     * @param repi - Number of inner replicates, sampled w/ replacement
     * @param n - Total number of data points
     * @param nun - nun <= n, the number of unique samples per outer replicate
     * @param stato - Outer replicate statistic policy
     * @param stati - Inner replicate statistic policy
     * @param cache - Cache policy
     * @param chunk_size - Only used for threaded builds, defines amount of
     *      work each thread grabs per work request
     * @return true if bootstrap okay, false o/w
     */
    template <typename stato_t, typename stati_t, typename cache_t>
    static bool booteng(const uUInt repo,
                        const uUInt repi,
                        const uUInt n,
                        const uUInt nun,
                        stato_t& stato,
                        stati_t& stati,
                        cache_t& cache,
                        const uUInt chunk_size U_THREAD_ID_PARAM) {
#ifdef U_BUILD_ENABLE_THREADS
        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);
#endif  // U_BUILD_ENABLE_THREADS
        // Pre-conditions
        uAssert(!early_out(repo, repi, n));
        uAssertBoundsInc(nun, U_TO_UINT(1), n);
        // Fisher-Yates shuffler
        uShufflerFisherYates<> fishy(n);
        // Batch work object
        typedef batch_work<stati_t, cache_t> batch_work_t;
        batch_work_t work(fishy.get_view(nun), repi, n, stati, cache);
        // Initialize statistics
        stato.init(repo);
        stati.init(repi);
        // Start outer statistic
        stato.start();
        // Special case first outer replicate to allow cache priming
        // Shuffle indices and update cache (sample w/o replacement)
        cache.update_first(fishy.shuffle(nun, uRng));
        // Start inner statistic
        stati.start();
        // Process inner replicate batch
        process_inner_batch(work, repi, chunk_size U_THREAD_ID_ARG);
        // Finish inner statistic
        stati.finish();
        // Update outer statistic
        stato.update(U_TO_UINT(0), stati);
        // Iterate over remaining outer replicates
        for (uUInt i_repo = 1; i_repo < repo; ++i_repo) {
            // Shuffle indices and update cache (sample w/o replacement)
            cache.update(i_repo, fishy.shuffle(nun, uRng));
            // Start inner statistic
            stati.start();
            // Process inner replicate batch
            process_inner_batch(work, repi, chunk_size U_THREAD_ID_ARG);
            // Finish inner statistic
            stati.finish();
            // Update outer statistic
            stato.update(i_repo, stati);
        }  // end outer replicate loop
        // Finish outer statistic
        stato.finish();
        return true;
    }

    /**
     * Performs (queues) inner replicate batch work
     * @param batch_work - batch work object
     * @param repi - Number of inner replicates, sampled w/ replacement
     * @param chunk_size - Only used for threaded builds, defines amount of
     *      work each thread grabs per work request
     */
    template <typename batch_work_t>
    static void process_inner_batch(batch_work_t& work,
                                    const uUInt repi,
                                    const uUInt chunk_size U_THREAD_ID_PARAM) {
        // Perform inner replicates
#ifdef U_BUILD_ENABLE_THREADS
        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);
        // Chunk size must be positive
        uAssert(chunk_size > U_TO_UINT(0));
        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will
        // likely deadlock as we only have a single, global parallel mapper.
        uParMap.wait_until_work_finished(work, chunk_size);
#else
        // Else perform serial call! Note, do_range() returns FALSE if all
        // work processed
        uVerify(!work.do_range(U_TO_UINT(0), repi U_THREAD_ID_ARG));
#endif  // U_BUILD_ENABLE_THREADS
    }

    /**
     * @return true if bootstrap is not needed or not configured properly
     */
    static bool early_out(const uUInt repo, const uUInt repi, const uUInt n) {
        return (repo < U_TO_UINT(1)) || (repi < U_TO_UINT(1)) ||
               (n <= U_TO_UINT(1));
    }

    /**
     * @return true if bootstrap is not needed or not configured properly
     */
    static bool early_out(const uUInt repo,
                          const uUInt repi,
                          const uUInt n,
                          const uReal nexp) {
        return early_out(repo, repi, n) || (nexp < U_TO_REAL(0.0)) ||
               (nexp > U_TO_REAL(1.0));
    }

    /**
     * @return true if bootstrap is not needed or not configured properly
     */
    static bool early_outnun(const uUInt repo,
                             const uUInt repi,
                             const uUInt n,
                             const uUInt nun) {
        return early_out(repo, repi, n) || (nun < U_TO_UINT(1));
    }

    // Disallow instantiation, copy, and assignment
    uBlb();
    uBlb(const uBlb&);
    uBlb& operator=(const uBlb&);
};

#endif  // uBlb_h
