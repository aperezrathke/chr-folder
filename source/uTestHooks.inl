//****************************************************************************
// uTestHooks.inl
//****************************************************************************

/**
 * General work flow for adding a test commandlet:
 * 1) Create a new commandlet header file uTest<name>.h
 * 2) Within test commandlet header file, follow header guard and build flags
 *      policy as in other test commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef U_BUILD_ENABLE_TESTS
 * 3) Within test commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within test commandlet header file, define uTest<name>Main(opts)
 *      method (outside of anonymous namespace)
 * 5) In uOpts.h, within the test commandlet options demarcated by build flag,
 *      add uOpt_test_<name> to the set of enums
 * 6) In uOpts.cpp, in the section of U_OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("test_<name>", "<description>").
 *      - make sure this is in same order as enum structure in uOpts.h
 * 7.) In uTestHooks.inl, include test commandlet header file
 * 8.) In uTestHooks.inl, add to the GAppInfo structure the test commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef uTestHooks_inl
#   error "Test Hooks included multiple times!"
#endif  // uTestHooks_inl
#define uTestHooks_inl

// Implementation of test commandlet hooks extracted to separate file to avoid
// cluttering uMain.cpp

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uAppHooks.h"
#include "uExitCodes.h"

// Include additional tests here
#include "uTestCsppline.h"
#include "uTestEbrTuner.h"
#include "uTestExport.h"
#include "uTestHash.h"
#include "uTestLigcCalc.h"
#include "uTestLigcPopd.h"
#ifdef U_BUILD_ENABLE_THREADS
#   include "uTestParallelMapper.h"
#endif  // U_BUILD_ENABLE_THREADS
#include "uTestRng.h"
#include "uTestSisDelphicPowerResamplingQc.h"
#include "uTestSisEllipsoidalNucleus.h"
#include "uTestSisHomgUnifLagQcSchedule.h"
#include "uTestSisHomgUnifLagTrSchedule.h"
#include "uTestSisIntrFailEnerChr.h"
#include "uTestSisIntrFailEnerLamSph.h"
#include "uTestSisIntrFailEnerNucbSph.h"
#include "uTestSisLmrkRc.h"
#include "uTestSisLmrkRs.h"
#include "uTestSisLmrkRsRc.h"
#include "uTestSisLmrkRsRs.h"
#include "uTestSisLmrkSelDelPow.h"
#include "uTestSisMkiMkoSeoMlocIntrChr.h"
#include "uTestSisMlocGrowth.h"
#include "uTestSisOfaCollision.h"
#include "uTestSisPartialRejectionControlQc.h"
#include "uTestSisPowerResamplingQc.h"
#include "uTestSisRejectionControlQc.h"
#include "uTestSisResidualResamplingQc.h"
#include "uTestSisSeoEllipsoidIntrLam.h"
#include "uTestSisSeoSphereIntrLam.h"
#include "uTestSisSeoSphereIntrNucb.h"
#include "uTestSisSkiSeoSlocIntrChr.h"
#include "uTestZstr.h"

// Register test commandlets here
static struct uAppInfo GTests[] = {
#ifdef U_BUILD_ENABLE_COMMANDLETS
    {uOpt_get_cmd_switch(uOpt_test_ligc_calc), &uTestLigcCalcMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_popd), &uTestLigcPopdMain},
#endif  // U_BUILD_ENABLE_COMMANDLETS
    {uOpt_get_cmd_switch(uOpt_test_csppline), &uTestCspplineMain},
    {uOpt_get_cmd_switch(uOpt_test_csv), &uTestCsvMain},
    {uOpt_get_cmd_switch(uOpt_test_csv_gz), &uTestCsvGzMain},
    {uOpt_get_cmd_switch(uOpt_test_csv_bulk), &uTestCsvBulkMain},
    {uOpt_get_cmd_switch(uOpt_test_csv_bulk_gz), &uTestCsvBulkGzMain},
    {uOpt_get_cmd_switch(uOpt_test_ebr_tuner), &uTestEbrTunerMain},
    {uOpt_get_cmd_switch(uOpt_test_hash), &uTestHashMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_ellipnuc),
     &uTestSisEllipsoidalNucleusMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_ofa_collision),
     &uTestSisOfaCollisionMain},
#ifdef U_BUILD_ENABLE_THREADS
    {uOpt_get_cmd_switch(uOpt_test_parallel_mapper), &uTestParallelMapperMain},
#endif  // U_BUILD_ENABLE_THREADS
    {uOpt_get_cmd_switch(uOpt_test_pdb), &uTestPdbMain},
    {uOpt_get_cmd_switch(uOpt_test_pdb_gz), &uTestPdbGzMain},
    {uOpt_get_cmd_switch(uOpt_test_pdb_bulk), &uTestPdbBulkMain},
    {uOpt_get_cmd_switch(uOpt_test_pdb_bulk_gz), &uTestPdbBulkGzMain},
    {uOpt_get_cmd_switch(uOpt_test_pml), &uTestPmlMain},
    {uOpt_get_cmd_switch(uOpt_test_pml_gz), &uTestPmlGzMain},
    {uOpt_get_cmd_switch(uOpt_test_pml_bulk), &uTestPmlBulkMain},
    {uOpt_get_cmd_switch(uOpt_test_pml_bulk_gz), &uTestPmlBulkGzMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_csv), &uTestLigcCsvMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_csv_gz), &uTestLigcCsvGzMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_csv_bulk), &uTestLigcCsvBulkMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_csv_bulk_gz), &uTestLigcCsvBulkGzMain},
    {uOpt_get_cmd_switch(uOpt_test_ligc_bin), &uTestLigcBinMain},
    {uOpt_get_cmd_switch(uOpt_test_rng), &uTestRngMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_sloc_hetr),
     &uTestSisMkiMkoSeoIntrChrSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_mloc_hetr),
     &uTestSisMkiMkoSeoIntrChrMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_sloc_homg),
     &uTestSisMkiMkoSeoIntrChrSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_mloc_homg),
     &uTestSisMkiMkoSeoIntrChrMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_sloc_hetr_budg),
     &uTestSisMkiMkoSeoIntrChrSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_mloc_hetr_budg),
     &uTestSisMkiMkoSeoIntrChrMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_sloc_homg_budg),
     &uTestSisMkiMkoSeoIntrChrSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mki_mko_seo_intr_chr_mloc_homg_budg),
     &uTestSisMkiMkoSeoIntrChrMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_sloc_hetr),
     &uTestSisIntrFailEnerChrSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_mloc_hetr),
     &uTestSisIntrFailEnerChrMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_sloc_homg),
     &uTestSisIntrFailEnerChrSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_mloc_homg),
     &uTestSisIntrFailEnerChrMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_sloc_hetr_budg),
     &uTestSisIntrFailEnerChrSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_mloc_hetr_budg),
     &uTestSisIntrFailEnerChrMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_sloc_homg_budg),
     &uTestSisIntrFailEnerChrSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_chr_mloc_homg_budg),
     &uTestSisIntrFailEnerChrMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_sloc_hetr),
     &uTestSisSeoSphereIntrLamSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_mloc_hetr),
     &uTestSisSeoSphereIntrLamMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_sloc_homg),
     &uTestSisSeoSphereIntrLamSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_mloc_homg),
     &uTestSisSeoSphereIntrLamMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_sloc_hetr_budg),
     &uTestSisSeoSphereIntrLamSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_mloc_hetr_budg),
     &uTestSisSeoSphereIntrLamMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_sloc_homg_budg),
     &uTestSisSeoSphereIntrLamSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_lam_mloc_homg_budg),
     &uTestSisSeoSphereIntrLamMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_sloc_hetr),
     &uTestSisIntrFailEnerLamSphSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_mloc_hetr),
     &uTestSisIntrFailEnerLamSphMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_sloc_homg),
     &uTestSisIntrFailEnerLamSphSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_mloc_homg),
     &uTestSisIntrFailEnerLamSphMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_sloc_hetr_budg),
     &uTestSisIntrFailEnerLamSphSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_mloc_hetr_budg),
     &uTestSisIntrFailEnerLamSphMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_sloc_homg_budg),
     &uTestSisIntrFailEnerLamSphSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_lam_sph_mloc_homg_budg),
     &uTestSisIntrFailEnerLamSphMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_sloc_hetr),
     &uTestSisSeoEllipsoidIntrLamSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_mloc_hetr),
     &uTestSisSeoEllipsoidIntrLamMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_sloc_homg),
     &uTestSisSeoEllipsoidIntrLamSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_mloc_homg),
     &uTestSisSeoEllipsoidIntrLamMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_sloc_hetr_budg),
     &uTestSisSeoEllipsoidIntrLamSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_mloc_hetr_budg),
     &uTestSisSeoEllipsoidIntrLamMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_sloc_homg_budg),
     &uTestSisSeoEllipsoidIntrLamSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_ellip_intr_lam_mloc_homg_budg),
     &uTestSisSeoEllipsoidIntrLamMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_sloc_hetr),
     &uTestSisSeoSphereIntrNucbSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_mloc_hetr),
     &uTestSisSeoSphereIntrNucbMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_sloc_homg),
     &uTestSisSeoSphereIntrNucbSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_mloc_homg),
     &uTestSisSeoSphereIntrNucbMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_sloc_hetr_budg),
     &uTestSisSeoSphereIntrNucbSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_mloc_hetr_budg),
     &uTestSisSeoSphereIntrNucbMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_sloc_homg_budg),
     &uTestSisSeoSphereIntrNucbSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_seo_sphere_intr_nucb_mloc_homg_budg),
     &uTestSisSeoSphereIntrNucbMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_hetr),
     &uTestSisIntrFailEnerNucbSphSlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_hetr),
     &uTestSisIntrFailEnerNucbSphMlocHetrMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_homg),
     &uTestSisIntrFailEnerNucbSphSlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_homg),
     &uTestSisIntrFailEnerNucbSphMlocHomgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_hetr_budg),
     &uTestSisIntrFailEnerNucbSphSlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_hetr_budg),
     &uTestSisIntrFailEnerNucbSphMlocHetrBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_homg_budg),
     &uTestSisIntrFailEnerNucbSphSlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_homg_budg),
     &uTestSisIntrFailEnerNucbSphMlocHomgBudgMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_mloc_growth), &uTestSisMlocGrowthMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_homg_unif_lag_qc_sched),
     &uTestSisHomgUnifLagQcSchedMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_homg_unif_lag_tr_sched),
     &uTestSisHomgUnifLagTrSchedMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_lmrk_rc), &uTestSisLmrkRcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_lmrk_rs), &uTestSisLmrkRsMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_lmrk_rs_rc), &uTestSisLmrkRsRcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_lmrk_rs_rs), &uTestSisLmrkRsRsMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_lmrk_sel_del_pow),
     &uTestSisLmrkSelDelPowMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_partial_rejection_control_qc),
     &uTestSisPartialRejectionControlQcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_power_resampling_qc),
     &uTestSisPowerResamplingQcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_rejection_control_qc),
     &uTestSisRejectionControlQcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_residual_resampling_qc),
     &uTestSisResidualResamplingQcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_delphic_power_resampling_qc),
     &uTestSisDelphicPowerResamplingQcMain},
    {uOpt_get_cmd_switch(uOpt_test_sis_ski_seo_sloc_intr_chr),
     &uTestSisSkiSeoSlocIntrChrMain},
    {uOpt_get_cmd_switch(uOpt_test_zstr), &uTestZstrMain}};

// Tests are enabled, route to the proper hook:
// Parse command line and run any tests specified
#define U_CONDITIONAL_RUN_TESTS(cmd_opts) \
    U_CONDITIONAL_RUN_APPS(cmd_opts, GTests)

#else

// Tests are not enabled, strip away call to hook
#define U_CONDITIONAL_RUN_TESTS(cmd_opts) ((void)0)

#endif  // U_BUILD_ENABLE_TESTS
