//****************************************************************************
// uSisIntrLamCommonBatch.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utilities for lamina interaction batch tests
 */

#ifndef uSisIntrLamCommonBatch_h
#define uSisIntrLamCommonBatch_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibDefs.h"
#include "uSisIntrLamCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Typedefs and structs
//****************************************************************************

/**
 * Collection of loci tables
 */
typedef struct {
    /**
     * Table of lamina knock-in interactions partitioned by locus
     */
    uSisIntrLociTable_t loci_kin;

    /**
     * Table of lamina knock-out interactions partitioned by locus
     */
    uSisIntrLociTable_t loci_ko;
} uSisIntrLamLociTables_t;

/**
 * Core arguments for testing if seeded nodes can satisfy lamina interactions
 */
typedef struct {
    /**
     * Table of interaction identifiers partitioned by loci
     */
    const uSisIntrLociTable_t* p_intr_loci;
    /**
     * Vector of interactions, each element is column index into frags matrix
     */
    const uUIVecCol* p_intr;
    /**
     * 2 x M matrix where M is number of fragments, defines the node spans
     * constituting a chromatin fragment
     */
    const uUIMatrix* p_frags;
} uSisIntrLamBatchIsSeedOkayCore_t;

/**
 * Core arguments needed for batch marking candidates which violate
 * interaction constraints
 */
typedef struct {
    /**
     * Matrix containing candidate positions to test for interaction
     * constraint violations.
     */
    const uMatrix* p_candidate_centers;
    /**
     * The radius of the candidate node (assumes all have same radius)
     */
    uReal candidate_radius;
    /**
     * The node identifier at which candidate will be placed
     */
    uUInt candidate_nid;
    /**
     * Locus containing candidate node
     */
    uUInt candidate_lid;
    /**
     * Table of interaction identifiers partitioned by loci
     */
    const uSisIntrLociTable_t* p_intr_loci;
    /**
     * Vector of interactions, each element is column index into frags matrix
     */
    const uUIMatrix* p_intr;
    /**
     * 2 x M matrix where M is number of fragments, defines the node spans
     * constituting a chromatin fragment
     */
    const uUIMatrix* p_frags;
} uSisIntrLamBatchMarkViolCore_t;

/**
 * Core arguments needed for batch checking if any interaction constraints
 * have been satisfied
 */
typedef struct {
    /**
     * Unique chromatin node identifier for latest node added to sample
     */
    uUInt nid;
    /**
     * Locus identifier corresponding to 'nid'
     */
    uUInt lid;
    /**
     * The radius of the added node at 'nid'
     */
    uReal radius;
    /**
     * 3D coordinates of node centroid at 'nid'
     */
    const uReal* B;
    /**
     * Vector of interactions, each element is column index into frags matrix
     */
    const uUIMatrix* p_intr;
    /**
     * 2 x M matrix where M is number of fragments, defines the node spans
     * constituting a chromatin fragment
     */
    const uUIMatrix* p_frags;
} uSisIntrLamBatchIsSatisfCore_t;

//****************************************************************************
// uSisIntrLamCommonBatch
//****************************************************************************

/**
 * Effectively namespace for shared lamina interaction batch utilities
 */
template <typename t_SisGlue>
class uSisIntrLamCommonBatch {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrLamCommonFilt<glue_t> intr_lam_common_filt_t;

    /**
     * Utility to clear all lamina loci interaction tables
     */
    static void clear(uSisIntrLamLociTables_t& loci_tables) {
        loci_tables.loci_kin.clear();
        loci_tables.loci_ko.clear();
    }

    /**
     * Partitions interactions by involved loci
     * @param loci - Output interaction table, stored elements correspond
     *  to indices into 'intr' array
     * @param intr - Vector of interactions, each element is column index into
     *  'frags' matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param sim - Parent simulation
     */
    static void partition_by_loci(uSisIntrLociTable_t& loci,
                                  const uUIVecCol& intr,
                                  const uUIMatrix& frags,
                                  const sim_t& sim) {
        // Reset loci table
        loci.clear();
        // Early out if no interactions
        if (intr.is_empty()) {
            return;
        }
        // If this assert trips, then index type is insufficient number of
        // bits to represent the number of interactions
        uAssert(
            static_cast<size_t>(intr.n_elem) <=
            static_cast<size_t>(
                std::numeric_limits<uSisIntrIndexVec_t::value_type>::max()));
        // Resize loci tables
        const uUInt num_loci = sim.get_num_loci();
        uAssert(num_loci > U_TO_UINT(0));
        loci.resize(num_loci);
        // Partition interactions
        uAssertPosEq(frags.n_rows, uIntrLibPairIxNum);
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < intr.n_elem; ++i) {
            // Determine locus involved for this interaction
            const uUInt frag = intr.at(i);
            uAssertBounds(frag, U_TO_UINT(0), frags.n_cols);
            const uUInt nid_lo = frags.at(uIntrLibPairIxMin, frag);
            const uUInt nid_hi = frags.at(uIntrLibPairIxMax, frag);
            uAssertBoundsInc(nid_lo, U_TO_UINT(0), nid_hi);
            uAssert(nid_hi < sim.get_max_total_num_nodes());
            const uUInt lid_lo = sim.get_growth_lid(nid_lo);
            const uUInt lid_hi = sim.get_growth_lid(nid_hi);
            uAssertBounds(lid_lo, U_TO_UINT(0), num_loci);
            if (lid_lo != lid_hi) {
                uLogf(
                    "Error, lamina interaction fragment (0-index=%d) overlaps "
                    "multiple loci. Exiting.\n",
                    (int)frag);
                exit(uExitCode_error);
            }
            loci[lid_lo].push_back(i);
        }
    }

    /**
     * Determines if seeded node can satisfy associated lamina interaction
     *  constraints. The template arguments filt_kin_t and filt_ko_t provide a
     *  stateless interface for testing constraints; the template arguments
     *  policy_t and policy_args_t provide callbacks (and the corresponding
     *  arguments) for handling events such as constraint failure.
     * @param policy - Callback policy for seed failure
     * @param p_policy_args - Pointer to additional policy arguments
     * @param loci_tables - Active loci interactions tables
     * @param nfo - Common seed information
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_kin_t,
              typename filt_ko_t,
              typename policy_t,
              typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrLamLociTables_t& loci_tables,
                              const uSisUtils::seed_info_t& nfo,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        // Initialize core arguments
        uSisIntrLamBatchIsSeedOkayCore_t isokc /* = {0}*/;
        isokc.p_frags = &(sim.get_intr_lam_frags());

        // Master interaction lists
        const uUIVecCol& intr_kin = sim.get_intr_lam_kin();
        const uUIVecCol& intr_ko = sim.get_intr_lam_ko();

        // Knock-in interactions
        isokc.p_intr_loci = &loci_tables.loci_kin;
        isokc.p_intr = &intr_kin;
        if (!is_seed_okay_<filt_kin_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                isokc,
                nfo,
                sample,
                sim U_THREAD_ID_ARG)) {
            // Knock-in interaction cannot be satisfied by seed
            return uFALSE;
        }

        // Knock-out interactions
        isokc.p_intr_loci = &loci_tables.loci_ko;
        isokc.p_intr = &intr_ko;
        if (!is_seed_okay_<filt_ko_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                isokc,
                nfo,
                sample,
                sim U_THREAD_ID_ARG)) {
            // Knock-out interaction cannot be satisfied by seed
            return uFALSE;
        }

        // All lamina interactions potentially satisfiable
        return uTRUE;
    }

    /**
     * ASSUMES sequential growth from first to last node. Determines which
     * candidate positions violate (i.e. cannot possibly satisfy) lamina
     * interaction constraints. The template arguments filt_kin_t and
     * filt_ko_t provide a stateless interface for testing constraints; the
     * template arguments policy_t and policy_args_t provide callbacks (and
     * the corresponding arguments) for handling events such as constraint
     * failure.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param loci_tables - Active loci interactions tables
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - Radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - Node identifier from which candidates positions
     *  were generated
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_kin_t,
              typename filt_ko_t,
              typename policy_t,
              typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrLamLociTables_t& loci_tables,
                          const uMatrix& candidate_centers,
                          const uReal candidate_radius,
                          const uUInt parent_node_id,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // Initialize core arguments
        uSisIntrLamBatchMarkViolCore_t mvc /* = {0}*/;
        mvc.p_candidate_centers = &candidate_centers;
        mvc.candidate_radius = candidate_radius;
        // @WARNING - assumes candidate node id is +1 from parent node id
        mvc.candidate_nid = parent_node_id + 1;
        uAssertBounds(
            mvc.candidate_nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
        mvc.candidate_lid = sim.get_growth_lid(mvc.candidate_nid);
        uAssertBounds(mvc.candidate_lid, U_TO_UINT(0), sim.get_num_loci());
        mvc.p_frags = &(sim.get_intr_lam_frags());

        // Master interaction lists
        const uUIVecCol& intr_kin = sim.get_intr_lam_kin();
        const uUIVecCol& intr_ko = sim.get_intr_lam_ko();

        // Knock-in interactions
        mvc.p_intr_loci = &loci_tables.loci_kin;
        mvc.p_intr = &intr_kin;
        mark_viol_<filt_kin_t, policy_t, policy_args_t>(
            policy,
            p_policy_args,
            out_legal_candidates_primed,
            mvc,
            sample,
            sim U_THREAD_ID_ARG);

        // Knock-out interactions
        mvc.p_intr_loci = &loci_tables.loci_ko;
        mvc.p_intr = &intr_ko;
        mark_viol_<filt_ko_t, policy_t, policy_args_t>(
            policy,
            p_policy_args,
            out_legal_candidates_primed,
            mvc,
            sample,
            sim U_THREAD_ID_ARG);
    }

    /**
     * Updates loci interaction tables by removing any satisfied interactions;
     * The template arguments filt_kin_t and filt_ko_t provide a stateless
     * interface for testing constraints satisfaction
     * @param loci_tables - Mutable active loci interactions tables, any
     *  satisfied interactions will be removed
     * @param node_id - Unique chromatin node identifier
     * @param B - 3D coordinates of node centroid
     * @param radius - Radius of the node to add
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation of parameter sample
     */
    template <typename filt_kin_t, typename filt_ko_t>
    static void remove_satisf(uSisIntrLamLociTables_t& loci_tables,
                              const uUInt node_id,
                              const uReal* B,
                              const uReal radius,
                              const uUInt candidate_id,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        // Initialize core arguments
        uSisIntrLamBatchIsSatisfCore_t isc /* = {0}*/;
        isc.nid = node_id;
        uAssertBounds(isc.nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
        isc.lid = sim.get_growth_lid(node_id);
        uAssertBounds(isc.lid, U_TO_UINT(0), sim.get_num_loci());
        isc.radius = radius;
        isc.B = B;
        isc.p_frags = &(sim.get_intr_lam_frags());

        // Master interaction lists
        const uUIVecCol& intr_kin = sim.get_intr_lam_kin();
        const uUIVecCol& intr_ko = sim.get_intr_lam_ko();

        // Knock-in interactions
        isc.p_intr = &intr_kin;
        remove_satisf_<filt_kin_t>(loci_tables.loci_kin,
                                   isc,
                                   candidate_id,
                                   sample,
                                   sim U_THREAD_ID_ARG);

        // Knock-out interactions
        isc.p_intr = &intr_ko;
        remove_satisf_<filt_ko_t>(loci_tables.loci_ko,
                                  isc,
                                  candidate_id,
                                  sample,
                                  sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Determines if seed candidate can satisfy lamina constraints. The
     *  template argument 'filt_t' provides a stateless interface for checking
     *  if a single interaction has been violated; the template arguments
     *  policy_t and policy_args_t provide callbacks (and the corresponding
     *  arguments) for handling events like constraint failure.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param isokc - Core arguments for seed constraint violations
     * @param nfo - Common seed information
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if lamina constraints can be satisfied, uFALSE o/w
     */
    template <typename filt_t, typename policy_t, typename policy_args_t>
    static uBool is_seed_okay_(policy_t& policy,
                               policy_args_t* const p_policy_args,
                               const uSisIntrLamBatchIsSeedOkayCore_t& isokc,
                               const uSisUtils::seed_info_t& nfo,
                               const sample_t& sample,
                               const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(nfo.p_node_center);
        // Verify bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBounds(nfo.nid, U_TO_UINT(0), max_nodes);
        uAssertBounds(nfo.lid, U_TO_UINT(0), sim.get_num_loci());

        // Use references
        const uSisIntrLociTable_t& intr_loci = *isokc.p_intr_loci;
        const uUIVecCol& intr = *isokc.p_intr;
        const uUIMatrix& frags = *isokc.p_frags;

        // Early out if interaction table is empty
        if (intr_loci.empty()) {
            return uTRUE;
        }

        // Early out if no interactions involving this locus
        uAssertPosEq(U_TO_UINT(intr_loci.size()), sim.get_num_loci());
        const uSisIntrIndexVec_t& intr_locus = intr_loci[nfo.lid];
        if (intr_locus.empty()) {
            return uTRUE;
        }

        // Initialize core arguments to filter
        uSisIntrLamFilterCore_t fc /* = {0}*/;
        fc.node_nid = nfo.nid;
        fc.node_lid = nfo.lid;
        fc.node_radius = nfo.node_radius;

        // Apply interaction filter to seed candidate
        const uVecCol& node_center = *(nfo.p_node_center);
        const size_t intr_locus_num = intr_locus.size();
        for (size_t i = 0; i < intr_locus_num; ++i) {
            fc.intr_id = intr_locus[i];
            get_frag_range_(fc, intr, frags, max_nodes);
            uAssert(intr_lam_common_filt_t::check_format(fc, sim));
            if (!filt_t::is_seed_okay(policy,
                                      p_policy_args,
                                      fc,
                                      node_center,
                                      sample,
                                      sim U_THREAD_ID_ARG)) {
                // Early out, constraint was violated
                return uFALSE;
            }
        }

        // All constraints are potentially satisfiable
        return uTRUE;
    }

    /**
     * Determines if any candidates violate lamina interaction constraints.
     *  The template argument 'filt_t' provides a stateless interface for
     *  checking if a single interaction has been violated; the template
     *  arguments policy_t and policy_args_t provide callbacks (and the
     *  corresponding arguments) for handling events like constraint failure.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param mvc - Core arguments for batch marking constraint violations
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_t, typename policy_t, typename policy_args_t>
    static void mark_viol_(policy_t& policy,
                           policy_args_t* const p_policy_args,
                           uBoolVecCol& out_legal_candidates_primed,
                           const uSisIntrLamBatchMarkViolCore_t& mvc,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        // Verify bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssert(mvc.p_candidate_centers);
        uAssertBounds(mvc.candidate_nid, U_TO_UINT(0), max_nodes);
        uAssertBounds(mvc.candidate_lid, U_TO_UINT(0), sim.get_num_loci());

        // Use references
        const uSisIntrLociTable_t& intr_loci = *mvc.p_intr_loci;
        const uUIVecCol& intr = *mvc.p_intr;
        const uUIMatrix& frags = *mvc.p_frags;

        // Early out if interaction table is empty
        if (intr_loci.empty()) {
            return;
        }

        // Early out if no interactions involving this locus
        uAssertPosEq(U_TO_UINT(intr_loci.size()), sim.get_num_loci());
        const uSisIntrIndexVec_t& intr_locus = intr_loci[mvc.candidate_lid];
        if (intr_locus.empty()) {
            return;
        }

        // Initialize core arguments to filter
        uSisIntrLamFilterCore_t fc /* = {0}*/;
        fc.node_nid = mvc.candidate_nid;
        fc.node_lid = mvc.candidate_lid;
        fc.node_radius = mvc.candidate_radius;

        // Apply interaction filter to candidates
        const size_t intr_locus_num = intr_locus.size();
        for (size_t i = 0; i < intr_locus_num; ++i) {
            fc.intr_id = intr_locus[i];
            get_frag_range_(fc, intr, frags, max_nodes);
            uAssert(intr_lam_common_filt_t::check_format(fc, sim));
            filt_t::mark_viol(policy,
                              p_policy_args,
                              out_legal_candidates_primed,
                              fc,
                              *(mvc.p_candidate_centers),
                              sample,
                              sim U_THREAD_ID_ARG);
        }
    }

    /**
     * Determines if any lamina interaction constraints have been satisfied
     *  and updates loci interaction lists so that they are no longer checked
     *  during the growth phase. The template argument 'filt_t' provides a
     *  stateless interface for checking if a single interaction has been
     *  satisfied.
     * @param intr_loci - Output table of active interactions (partitioned by
     *  loci); satisfied constraints are removed
     * @param isc - Core arguments for batch checking constraint satisfaction
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_t>
    static void remove_satisf_(uSisIntrLociTable_t& intr_loci,
                               const uSisIntrLamBatchIsSatisfCore_t& isc,
                               const uUInt candidate_id,
                               const sample_t& sample,
                               const sim_t& sim U_THREAD_ID_PARAM) {
        // Verify bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBounds(isc.nid, U_TO_UINT(0), max_nodes);
        uAssertBounds(isc.lid, U_TO_UINT(0), sim.get_num_loci());

        // Early out if interaction table is empty
        if (intr_loci.empty()) {
            return;
        }

        // Use references
        const uUIVecCol& intr = *isc.p_intr;
        const uUIMatrix& frags = *isc.p_frags;

        // Early out if no interactions involving this locus
        uAssertPosEq(U_TO_UINT(intr_loci.size()), sim.get_num_loci());
        uSisIntrIndexVec_t& intr_locus = intr_loci[isc.lid];
        if (intr_locus.empty()) {
            return;
        }

        // Initialize core arguments to filter
        uSisIntrLamFilterCore_t fc /* = {0}*/;
        fc.node_nid = isc.nid;
        fc.node_lid = isc.lid;
        fc.node_radius = isc.radius;

        // Update satisfied interactions
        const int intr_locus_num = static_cast<int>(intr_locus.size());
        for (int i = intr_locus_num - 1; i >= 0; --i) {
            fc.intr_id = intr_locus[i];
            get_frag_range_(fc, intr, frags, max_nodes);
            uAssert(intr_lam_common_filt_t::check_format(fc, sim));
            if (filt_t::is_satisf(
                    fc, isc.B, candidate_id, sample, sim U_THREAD_ID_ARG)) {
                // Remove satisfied constraint from further testing
                std::swap(intr_locus[i], intr_locus.back());
                uAssert(intr_locus.back() == fc.intr_id);
                intr_locus.pop_back();
            }
        }
    }

    /**
     * Obtain node span for interacting fragment regions
     * @param fc - Core arguments to filter, MUST HAVE .intr_id SET! The
     *  fragment span (lo, hi) is written to corresponding data members
     * @param intr - Vector of interactions, each element is column index into
     *  frags matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param max_nodes - Maximum node identifier, used for bounds checking
     *  only if asserts are enabled
     */
    inline static void get_frag_range_(uSisIntrLamFilterCore_t& fc,
                                       const uUIVecCol& intr,
                                       const uUIMatrix& frags,
                                       const uUInt max_nodes) {
        uAssertBounds(fc.intr_id, U_TO_UINT(0), intr.n_elem);
        const uUInt frag_id = intr.at(fc.intr_id);
        uAssertBounds(frag_id, U_TO_UINT(0), U_TO_UINT(frags.n_cols));
        uAssertPosEq(frags.n_rows, uIntrLibPairIxNum);
        fc.frag_nid_lo = frags.at(uIntrLibPairIxMin, frag_id);
        fc.frag_nid_hi = frags.at(uIntrLibPairIxMax, frag_id);
        uAssertBoundsInc(fc.frag_nid_lo, U_TO_UINT(0), fc.frag_nid_hi);
        uAssertBounds(fc.frag_nid_hi, fc.frag_nid_lo, max_nodes);
    }

    // Disallow any form of instantiation
    uSisIntrLamCommonBatch(const uSisIntrLamCommonBatch&);
    uSisIntrLamCommonBatch& operator=(const uSisIntrLamCommonBatch&);
};

#endif  // uSisIntrLamCommonBatch_h
