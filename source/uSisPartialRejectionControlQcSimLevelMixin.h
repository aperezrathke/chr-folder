//****************************************************************************
// uSisPartialRejectionControlQcSimLevelMixin.h
//****************************************************************************

/**
 * QcMixin = A mixin that performs Quality Control to minimize distance
 *  between target and trial sampling distributions. Examples of algorithms
 *  intended to achieve this include Rejection Control and Resampling. See:
 *
 * Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science
 *   & Business Media, 2008.
 *
 * This is a mixin that performs partial rejection control. This algorithm is
 * documented in (Liu 2008) and also described in:
 *
 * Smith, Adrian. Sequential Monte Carlo methods in practice. Eds.
 *   Arnaud Doucet, Nando de Freitas, and Neil Gordon. Springer Science &
 *   Business Media, 2013.
 */

#ifndef uSisPartialRejectionControlQcSimLevelMixin_h
#define uSisPartialRejectionControlQcSimLevelMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uHashMap.h"
#include "uRand.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * This class defines on_grow_phase_finish() to perform partial rejection
 * control filtering of the current set of samples. Samples that fail the
 * rejection control step must be resampled from the prior checkpoint and then
 * regrown back to the current checkpoint.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // Determines schedule of when to run rejection control
    typename t_RcSched,
    // Determines weight scaling during rejection control steps
    typename t_RcScale,
    // Determines the regrowth procedure
    typename t_RcRegrowth>
class uSisPartialRejectionControlQcSimLevelMixin : public t_RcSched,
                                                   public t_RcScale,
                                                   public t_RcRegrowth {
private:
    /**
     * Scheduling type
     */
    typedef t_RcSched uRcSched_t;

    /**
     * Scaling type
     */
    typedef t_RcScale uRcScale_t;

    /**
     * Regrowth type
     */
    typedef t_RcRegrowth uRcRegrowth_t;

    /**
     * Allow it to call regrowth()
     */
    friend uRcRegrowth_t;

    // Flip switch to enable logging whenever a checkpoint is reached
    enum { rc_heartbeat = true };

public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisPartialRejectionControlQcSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Assert that we are growing at least 1 sample
        uAssert(sim.get_target_total_num_samples() > 0);

        // Clear any previous state
        this->clear();

        // We are enabled for the first trial only
        // (too memory intensive to store all intermediate sample pools)
        this->b_enabled = uTRUE;

        // Allocate previous checkpoint selection probability buffer
        this->prev_checkpoint_p_select.zeros(
            sim.get_target_total_num_samples());

        // Allocate previous checkpoint samples buffer
        this->prev_checkpoint_samples.resize(
            sim.get_target_total_num_samples());

        // Allocate exponential weights buffer
        this->curr_checkpoint_norm_exp_weights.zeros(
            sim.get_target_total_num_samples());

        // Allocate should regrow buffer
        this->curr_checkpoint_should_regrow.resize(
            sim.get_target_total_num_samples());

        // Initialize schedule
        uRcSched_t::init(sim, config, false /*is_tr*/ U_THREAD_ID_ARG);

        // Initialize threshold
        uRcScale_t::init(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->init(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Resets to default state
     */
    inline void clear() {
        this->b_enabled = uFALSE;

        this->prev_checkpoint_grow_step = 0;
        this->prev_checkpoint_mean_log_weight = U_TO_REAL(0.0);
        this->prev_checkpoint_p_select.clear();
        this->prev_checkpoint_samples.clear();

        this->curr_checkpoint_grow_step = 0;
        this->curr_checkpoint_max_log_weight = U_TO_REAL(0.0);
        this->curr_checkpoint_scale = U_TO_REAL(0.0);

        this->curr_checkpoint_norm_exp_weights.clear();
        this->curr_checkpoint_should_regrow.clear();

        this->uRcSched_t::clear();
        this->uRcScale_t::clear();
    }

    /**
     * Preps for next trial
     * @Warning - It is too memory intensive to keep track of all samples at
     * all checkpoints. Therefore, any sample that needs to be regrown in any
     * trial beyond the first will *NOT* be quality controlled.
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
        // We are not enabled for any trials beyond the first as it's too
        // memory intensive to store all intermediate sample pools.
        uAssert(this->b_enabled == uFALSE);
    }

    /**
     * Callback for when all samples have finished a single grow phase
     * @param status - The current status of each trial sample - 1 means
     *  chain is still growing, 0 means chain is no longer growing. May be
     *  modified by this function call.
     * @param log_weights - The current log weights of each sample. May be
     *  modified by this function call.
     * @param samples - the current set of trial samples. May be modified by
     *  this function call.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     */
    inline void on_grow_phase_finish(
        uWrappedBitset& status,
        uVecCol& log_weights,
        std::vector<sample_t>& samples,
        const uUInt num_completed_grow_steps_per_sample,
        sim_t& sim U_THREAD_ID_PARAM) {
        // If we're beyond first trial, then early out
        if (!this->b_enabled) {
            return;
        }

        // Should not be called if all samples are dead
        uAssert(!uSisUtilsQc::are_all_samples_dead(samples));

        // Should only be called after sample_t::grow_*()
        uAssert(num_completed_grow_steps_per_sample > 1);

        // Verify parallel data structures
        uAssertPosEq(status.size(), log_weights.size());
        uAssert(status.size() == samples.size());
        uAssert(status.size() == this->curr_checkpoint_norm_exp_weights.size());
        uAssert(status.size() == this->curr_checkpoint_should_regrow.size());

        // See if we are at a checkpoint
        if (!rc_prime_for_checkpoint(
                log_weights, num_completed_grow_steps_per_sample, sim)) {
            // Early out if no checkpoint reached
            return;
        }

        if (rc_heartbeat) {
            uLogf(
                "-Partial rejection control checkpoint reached at growth step "
                "%u.\n",
                (unsigned int)num_completed_grow_steps_per_sample);
        }

        U_SCOPED_INTERVAL_LOGGER(uSTAT_QcCheckpointInterval);

        // Make sure checkpoint data is valid
        uAssert(this->curr_checkpoint_max_log_weight >= U_TO_REAL(0.0));
        uAssert(this->curr_checkpoint_scale > U_TO_REAL(0.0));
        uAssert(this->curr_checkpoint_grow_step >
                this->prev_checkpoint_grow_step);

        // Determine if samples need to be regrown
        const uUInt n_samples = log_weights.size();
        uReal log_keep_weight_unused = U_TO_REAL(0.0);
        for (uUInt i = 0; i < n_samples; ++i) {
            // Mark all samples which don't pass checkpoint
            this->curr_checkpoint_should_regrow.set(
                i,
                uSisUtilsRc::checkpoint_filter(
                    log_weights[i],
                    log_keep_weight_unused,
                    this->curr_checkpoint_norm_exp_weights[i],
                    this->curr_checkpoint_scale U_THREAD_ID_ARG));
        }

        // Wrap bit set for faster iteration
        const uWrappedBitset wrapped_should_regrow(
            U_WRAP_BITSET(this->curr_checkpoint_should_regrow));

        // Early out if nothing needs to be regrown!
        if (!wrapped_should_regrow.any()) {
            return;
        }

        // Regrow selected samples
        {
            U_SCOPED_STAT_TIMER(uSTAT_RcRegrowthTime);

            this->uRcRegrowth_t::rc_batch_regrow(
                status,
                samples,
                log_weights,
                *this,
                sim,
                wrapped_should_regrow,
                num_completed_grow_steps_per_sample U_THREAD_ID_ARG);
        }  // end timer scope

        // Replace previous checkpoint with current checkpoint
        uAssert(this->curr_checkpoint_grow_step >
                this->prev_checkpoint_grow_step);
        this->prev_checkpoint_grow_step = this->curr_checkpoint_grow_step;
        this->prev_checkpoint_mean_log_weight =
            uSisUtilsQc::calc_mean_log_weight(
                this->curr_checkpoint_norm_exp_weights, log_weights);
        this->prev_checkpoint_p_select =
            this->curr_checkpoint_norm_exp_weights /
            uMatrixUtils::sum(this->curr_checkpoint_norm_exp_weights);
        this->prev_checkpoint_samples = samples;

    }  // end on_grow_phase_finish()

    // BEGIN REGROW INTERFACE
    // Interface used by trial runner when regrowing samples

    /**
     * Called by trial runner when prepping a sample to be regrown to a
     * certain length. Default simply forwards to mixins.
     */
    void reset_for_regrowth(const sample_t& sample,
                            const uUInt num_completed_grow_steps,
                            const sim_t& sim,
                            uSpConstConfig_t config) {
        this->uRcSched_t::reset_for_regrowth(
            *this, sample, num_completed_grow_steps, sim, config);
        this->uRcScale_t::reset_for_regrowth(
            *this, sample, num_completed_grow_steps, sim, config);
    }

    /**
     * Called by trial runner when the sample has finished being seeded during
     * the regrowth procedure. Default simply forwards to mixins.
     * @WARNING - Assuming no checkpoint will occur during seeding
     */
    void on_reseed_finish(const sample_t& sample,
                          uReal& out_log_weight,
                          bool& status,
                          const sim_t& sim) {
        this->uRcSched_t::on_reseed_finish(*this, sample, sim);
        this->uRcScale_t::on_reseed_finish(*this, sample, sim);
    }

    /**
     * Called by trial runner when sample has finished a grow_*() call
     * during the regrowth procedure. Determines if a checkpoint has been
     * reached. Samples which do not pass the checkpoint are marked dead.
     */
    void on_regrow_step_finish(sample_t& sample,
                               uReal& out_log_weight,
                               bool& status,
                               const uUInt num_completed_grow_steps,
                               const uUInt sample_id,
                               const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(!sample.is_dead());

        // Let mixins update their state first
        this->uRcSched_t::on_regrow_step_finish(
            *this, sample, status, num_completed_grow_steps, sim);
        this->uRcScale_t::on_regrow_step_finish(
            *this, sample, status, num_completed_grow_steps, sim);

        // See if we've reached the target checkpoint
        if (num_completed_grow_steps == this->curr_checkpoint_grow_step) {
            // Normalize log weight relative to max log weight at time
            // of checkpoint
            const uReal norm_exp_weight =
                exp(out_log_weight - this->curr_checkpoint_max_log_weight);

            // See if we pass checkpoint
            uReal log_keep_weight_unused = U_TO_REAL(0.0);
            if (uSisUtilsRc::checkpoint_filter(
                    out_log_weight,
                    log_keep_weight_unused,
                    norm_exp_weight,
                    this->curr_checkpoint_scale U_THREAD_ID_ARG)) {
                U_INC_STAT_COUNTER(uSTAT_RcReRegrowthCount);

                // We got filtered -> we need to regrow
                status = false;
                sample.mark_dead(uSisFail_QC, out_log_weight);
            }
        }
    }

    // END REGROW INTEFACE

    U_SIS_DECLARE_MIXIN_NULL_TRIAL_FINISH_INTERFACE

    U_SIS_DECLARE_MIXIN_NULL_RUN_FINISH_INTERFACE

private:
    /**
     * Utility function for use in on_grow_phase_finish(). Determines if we've
     * encountered a checkpoint based on scheduling policy. If checkpoint is
     * encountered: norm exp weights will be initialized, and current check-
     * point growth phase, scale, and max log weight will be updated
     * @param log_weights - The current log weights of each sample
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     * @return TRUE if new checkpoint encountered, FALSE otherwise
     */
    inline uBool rc_prime_for_checkpoint(
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_t& sim) {
        uAssert(num_completed_grow_steps_per_sample > 1);
        uAssertPosEq(this->curr_checkpoint_norm_exp_weights.size(),
                     log_weights.size());

        uBool b_checkpoint_encountered = uFALSE;

        // Determine if we should perform rejection control
        if (uRcSched_t::qc_checkpoint_reached(
                this->curr_checkpoint_norm_exp_weights,
                this->curr_checkpoint_max_log_weight,
                log_weights,
                num_completed_grow_steps_per_sample,
                *this,
                sim)) {
            // Determine exponential weights
            if (!uRcSched_t::calculates_norm_exp_weights) {
                uSisUtilsQc::calc_norm_exp_weights(
                    this->curr_checkpoint_norm_exp_weights,
                    this->curr_checkpoint_max_log_weight,
                    log_weights);
            }

            // Determine scaling factor
            this->curr_checkpoint_scale =
                uRcScale_t::rc_get_scale(this->curr_checkpoint_norm_exp_weights,
                                         this->curr_checkpoint_max_log_weight,
                                         num_completed_grow_steps_per_sample,
                                         *this,
                                         sim);

            // Update growth phase marker
            this->curr_checkpoint_grow_step =
                num_completed_grow_steps_per_sample;

            // Flag that checkpoint has been encountered
            b_checkpoint_encountered = uTRUE;
        }

        return b_checkpoint_encountered;
    }

    /**
     * The workhorse method of partial rejection control. Samples that do not
     * pass a checkpoint must be regrown from a previous checkpoint.
     *
     * @param out_sample - The sample to be regrown
     * @param out_log_weight - The final log weight of the sample after
     *  checkpoint correction
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the number of grow steps to complete
     * @param sample_id - The sample identifier (index into samples array)
     * @return final status of last grow_*() call -> TRUE if sample needs
     *  continue growing, FALSE if finished growing. We should not return any
     *  dead chains. Currently, method will infinite loop until chain is no
     *  longer dead.
     */
    bool rc_regrow(sample_t& out_sample,
                   uReal& out_log_weight,
                   sim_t& sim,
                   const uUInt target_num_grow_steps,
                   const uUInt sample_id U_THREAD_ID_PARAM) {
        U_INC_STAT_COUNTER(uSTAT_RcRegrowthCount);
        uAssert(target_num_grow_steps == this->curr_checkpoint_grow_step);

        // Used for storing status after each grow step
        bool status = false;

        // If this is the first encountered checkpoint, then we need to
        // reset and reseed each sample
        if (this->prev_checkpoint_grow_step == 0) {
            status = uSisUtilsRc::regrow_force(out_sample,
                                               out_log_weight,
                                               sim,
                                               target_num_grow_steps,
                                               sample_id U_THREAD_ID_ARG);
        }
        // Else, resample from a previous checkpoint
        else {
            do {
                uAssert(this->prev_checkpoint_grow_step > 1);
                uAssertPosEq(this->prev_checkpoint_p_select.size(),
                             this->prev_checkpoint_samples.size());
                uAssertRealEq(uMatrixUtils::sum(this->prev_checkpoint_p_select),
                              U_TO_REAL(1.0));

                // Select a previous checkpoint sample
                const uUInt idx = uSisUtilsQc::rand_select_index(
                    this->prev_checkpoint_p_select U_THREAD_ID_ARG);
                uAssertBounds(idx, 0, this->prev_checkpoint_samples.size());

                status = sim.regrow_attempt_from_template(
                    out_sample,
                    out_log_weight,
                    sim,
                    target_num_grow_steps,
                    sample_id,
                    this->prev_checkpoint_samples[idx],
                    this->prev_checkpoint_grow_step,
                    this->prev_checkpoint_mean_log_weight U_THREAD_ID_ARG);

                // @WARNING - THIS COULD INFINITE LOOP!
            } while (out_sample.is_dead());
        }

        uAssert(!out_sample.is_dead());
        uAssert(out_log_weight > U_TO_REAL(0.0));
        return status;
    }

    /**
     * If TRUE, quality control will proceed using partial rejection control
     * algorithm. Else, quality control will be disabled. Disabling occurs if
     * we have to regrow dead chains from the first trial. This is because
     * keeping track of all intermediate sample pools is too memory intensive.
     * Therefore, we just default to the non-quality control method for the
     * remaining samples.
     */
    uBool b_enabled;

    /**
     * The growth step count at which the previous checkpoint was
     * encountered. If this is 0, then no previous checkpoint was encountered.
     */
    uUInt prev_checkpoint_grow_step;

    /**
     * The mean log weight at this checkpoint, samples that are regrown from
     * this checkpoint have their weight initialized to the mean log weight of
     * samples which passed the checkpoint
     */
    uReal prev_checkpoint_mean_log_weight;

    /**
     * Probabilities to select each previous checkpoint sample when regrowing
     */
    uVecCol prev_checkpoint_p_select;

    /**
     * The pool of samples from last checkpoint at which to begin regrowth
     */
    std::vector<sample_t> prev_checkpoint_samples;

    /**
     * The grow step at which the current checkpoint was encountered.
     */
    uUInt curr_checkpoint_grow_step;

    /**
     * The maximum log weight at this checkpoint, needed for normalizing
     * log weights prior to exp transform (to avoid precision issues)
     */
    uReal curr_checkpoint_max_log_weight;

    /**
     * All weights at checkpoint are scaled by this factor. A sample is
     * kept with probability = min(1.0, w_i / scale) where w_i is weight
     * of i-th sample.
     */
    uReal curr_checkpoint_scale;

    /**
     * Internal buffer of exponential weights (derived from log_weights).
     * These weights are normalized relative to a maximum log weight.
     */
    uVecCol curr_checkpoint_norm_exp_weights;

    /**
     * Internal bit set buffer for keeping track of which samples to regrow.
     * i-th bit is 1 if sample should be regrown, 0 otherwise
     *
     * Collecting all samples that need to be grown so that they can be more
     * efficiently batch queued into worker threads if a threaded
     * implementation is available.
     */
    uBitset curr_checkpoint_should_regrow;
};

#endif  // uSisPartialRejectionControlQcSimLevelMixin_h
