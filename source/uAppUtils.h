//****************************************************************************
// uAppUtils.h
//****************************************************************************

#ifndef uAppUtils_h
#define uAppUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uGlobals.h"
#include "uOpts.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypes.h"

#include <iostream>
#include <string>

//****************************************************************************
// uAppUtils
//****************************************************************************

class uAppUtils {
public:
    /**
     * Check if exporting has been configured
     */
    static inline void print_warn_null_export(uSpConstConfig_t config) {
        if (0 == config->export_flags) {
            std::cout << "Warning, no export flags detected.\n"
                      << "\tSee command line options:\n"
                      << "\t\t" << uOpt_get_cmd_switch(uOpt_export_csv_unary)
                      << std::endl
                      << "\t\t" << uOpt_get_cmd_switch(uOpt_export_pdb_unary)
                      << std::endl
                      << "\t\t" << uOpt_get_cmd_switch(uOpt_export_pml_unary)
                      << std::endl
                      << "\t\t"
                      << uOpt_get_cmd_switch(uOpt_export_log_weights_unary)
                      << std::endl
                      << "\t\t" << uOpt_get_cmd_switch(uOpt_help) << std::endl;
        }
    }

    /**
     * Utility prints warning if configuration has no child config
     * @param config - Parent configuration
     * @return TRUE if no child configuration, FALSE o/w
     */
    static inline void print_warn_no_child_config(uSpConstConfig_t config) {
        // Warn if child configuration not found
        if (!config->get_child_tr()) {
            std::cout
                << "Warning, no child configuration found for trial runner "
                   "sub-simulation(s)."
                << std::endl
                << "\tWill default to parent configuration. See ini option: "
                << uOpt_get_ini_key(uOpt_conf_child_tr) << std::endl;
        }
    }

    /**
     * Prints configuration parameters to stdout
     */
    static inline void print_config(uSpConstConfig_t config) {
        config->print();

        // Check parent export status
        print_warn_null_export(config);

        // Check parent for child config
        print_warn_no_child_config(config);
    }

    /**
     * @return true if configured to report percentage of unique monomers at
     *each node position, false o/w
     */
    static inline bool should_report_perc_uniq(uSpConstConfig_t config) {
        // Check if user wants to report percent unique at each monomer
        uBool b_report_perc_uniq = uFALSE;
        config->read_into(b_report_perc_uniq, uOpt_report_perc_uniq);
        return b_report_perc_uniq == uTRUE;
    }

    /**
     * Default main for a templated simulation type.
     * @param cmd_opts - Command line options
     * @param announce - Console output announcing app is being run
     */
    template <typename sim_t>
    static int default_main(const uCmdOptsMap& cmd_opts,
                            const std::string& announce) {
        // Create configuration
        uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
        // Parse command line and any INI configuration (override defaults)
        config->init(cmd_opts);

        // Output configuration
        std::cout << announce << std::endl;
        print_config(config);

        // Initialize globals
        uG::init(config);

        U_LOG_NUM_WORKER_THREADS;

        // Feed configuration to simulation
        sim_t sim(config U_MAIN_THREAD_ID_ARG);

        // Run simulation!
        const uBool result = sim.run();

        // Compute effective sample size
        const uReal ess = uSisUtilsQc::calc_ess_cv_from_log(
            sim.get_completed_log_weights_view());

        std::cout << "SIS simulation completed with status: " << result
                  << std::endl;
        std::cout << "SIS effective sample size: " << ess << std::endl;

        // Check if user wants to report percent unique at each monomer
        if (should_report_perc_uniq(config)) {
            uSisUtilsQc::report_percent_unique_monomers(
                sim.get_completed_samples());
        }

        U_STATS_REPORT;

        // Export!
        uSisUtils::export_sim(sim);

        // Destroy globals
        uG::teardown();

        return result ? uExitCode_normal : uExitCode_error_no_fold;
    }

};  // uAppUtils

#endif  // uAppUtils_h
