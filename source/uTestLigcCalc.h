//****************************************************************************
// uTestLigcCalc.h
//****************************************************************************

/**
 * Commandlet for testing ligation contact calculations
 *
 * Usage:
 * -test_ligc_calc [--conf <conf_path>]
 */
#ifdef uTestLigcCalc_h
#   error "Test Ligc Calc included multiple times!"
#endif  // uTestLigcCalc_h
#define uTestLigcCalc_h

#include "uBuild.h"

#if (defined(U_BUILD_ENABLE_TESTS) && defined(U_BUILD_ENABLE_COMMANDLETS))

#include "uCmdOptsMap.h"
#include "uExitCodes.h"
#include "uLogf.h"

/**
 * Entry point for test
 */
int uTestLigcCalcMain(const uCmdOptsMap& cmd_opts_) {
    uLogf("Running Ligc Calc Test.\n");
    uCmdOptsMap cmd_opts(cmd_opts_);
    // Check if we should set default configuration path
    const std::string DEFAULT_CONF_PATH = "../tests/test_ligc_calc/ligc.ini";
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_conf),
                                  DEFAULT_CONF_PATH);
    // Smoke test
    return uCmdletLigcCalcMain(cmd_opts);
}

#endif  // (defined(U_BUILD_ENABLE_TESTS) &&
        // defined(U_BUILD_ENABLE_COMMANDLETS))
