//****************************************************************************
// uSisIntrNucbCommonFilt.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utilities for nuclear body interaction
 *  filtering
 */

#ifndef uSisIntrNucbCommonFilt_h
#define uSisIntrNucbCommonFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Structs
//****************************************************************************

/**
 * Core arguments used by nuclear body interaction filter utilities
 */
typedef struct {
    /**
     * Interaction index
     */
    uUInt intr_id;
    /**
     * Lower (min) node identifier for interacting fragment; note lo <= hi
     */
    uUInt frag_nid_lo;
    /**
     * Upper (max) node identifier for interacting fragment
     */
    uUInt frag_nid_hi;
    /**
     * Node identifier for either a candidate node (if marking violators) or a
     *  recently placed node (if checking constraint satisfaction)
     */
    uUInt node_nid;
    /**
     * The locus identifier corresponding to 'node_nid'
     */
    uUInt node_lid;
    /**
     * The node radius corresponding to 'node_nid'; note all candidate nodes
     *  with the same node identifier are assumed to have same radius
     */
    uReal node_radius;
    /**
     * Center (x,y,z) of nuclear body
     */
    const uReal* p_body_center;
} uSisIntrNucbFilterCore_t;

//****************************************************************************
// uSisIntrNucbCommonFilt
//****************************************************************************

/**
 * Effectively a namespace for shared interaction filter utilities
 */
template <typename t_SisGlue>
class uSisIntrNucbCommonFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * @param nid - Node identifier
     * @param frag_nid_lo - Minimum fragment node identifier
     * @param frag_nid_hi - Maximum fragment node identifier
     * @return uTRUE if node at nid is within fragment, uFALSE o/w
     */
    inline static uBool is_in_frag(const uUInt nid,
                                   const uUInt frag_nid_lo,
                                   const uUInt frag_nid_hi) {
        // Under SEO growth, we should not call this method with fragment that
        // has already been grown!
        uAssert(nid <= frag_nid_hi);
        uAssert(frag_nid_lo <= frag_nid_hi);
        // Early out if we're not in a relevant region
        return ((nid >= frag_nid_lo) && (nid <= frag_nid_hi));
    }

    /**
     * Asserts if interaction is not formatted properly
     * @WARNING - ASSUMES SEO GROWTH!
     * @param c - Core filter arguments
     * @param sim - Outer simulation containing global sample data
     */
    static uBool check_format(const uSisIntrNucbFilterCore_t& c,
                              const sim_t& sim) {
        // Assume SEO growth, constraint testing should be avoided after we've
        // grown beyond interacting fragment
        uAssert(c.node_nid <= c.frag_nid_hi);
        // Assume valid bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBoundsInc(c.frag_nid_lo, U_TO_UINT(0), c.frag_nid_hi);
        uAssertBounds(c.frag_nid_hi, c.frag_nid_lo, max_nodes);
        uAssertBounds(c.node_nid, U_TO_UINT(0), max_nodes);
        // Assume single locus
        const uUInt lid = sim.get_growth_lid(c.frag_nid_lo);
        uAssertBounds(lid, U_TO_UINT(0), sim.get_num_loci());
        uAssert(lid == sim.get_growth_lid(c.frag_nid_lo));
        uAssert(lid == sim.get_growth_lid(c.frag_nid_hi));
        uAssert(lid == sim.get_growth_lid(c.node_nid));
        uAssert(lid == c.node_lid);
        // Assume positive radius
        uAssert(c.node_radius > U_TO_REAL(0.0));
        // Assume valid pointer
        uAssert(c.p_body_center);
        return uTRUE;
    }

private:
    // Disallow any form of instantiation
    uSisIntrNucbCommonFilt(const uSisIntrNucbCommonFilt&);
    uSisIntrNucbCommonFilt& operator=(const uSisIntrNucbCommonFilt&);
};

#endif  // uSisIntrNucbCommonFilt_h
