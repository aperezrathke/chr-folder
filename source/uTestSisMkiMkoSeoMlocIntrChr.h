//****************************************************************************
// uTestSisMkiMkoSeoMlocIntrChr.h
//****************************************************************************

/**
 * Commandlet for testing multiple knock-ins, knock-outs constrained growth;
 * will output resulting PDB samples for later visualization.
 *
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Sloc: Single locus
 * Mloc: Multiple loci
 *
 * Also:
 *
 *  hetr: Heterogeneous radius nodes
 *  homg: Homogeneous radius nodes
 *
 * Usage for heterogeneous radius:
 *
 *  (single locus)
 * -test_sis_mki_mko_seo_intr_chr_sloc_hetr --conf <conf_path>
 *
 *  (multiple loci)
 * -test_sis_mki_mko_seo_intr_chr_mloc_hetr --conf <conf_path>

 * Usage for homogeneous radius:
 *
 *  (single locus)
 * -test_sis_mki_mko_seo_intr_chr_sloc_homg --conf <conf_path>
 *
 *  (multiple loci)
 * -test_sis_mki_mko_seo_intr_chr_mloc_homg --conf <conf_path>
 *
 * or, if defaults are okay to use, then respectively:
 *
 * -test_sis_mki_mko_seo_intr_chr_sloc_hetr
 * -test_sis_mki_mko_seo_intr_chr_mloc_hetr
 * -test_sis_mki_mko_seo_intr_chr_sloc_homg
 * -test_sis_mki_mko_seo_intr_chr_mloc_homg
 *
 *  Tests also available for budgeted interactions:
 *
 * -test_sis_mki_mko_seo_intr_chr_sloc_hetr_budg [--conf <path>]
 * -test_sis_mki_mko_seo_intr_chr_mloc_hetr_budg [--conf <path>]
 * -test_sis_mki_mko_seo_intr_chr_sloc_homg_budg [--conf <path>]
 * -test_sis_mki_mko_seo_intr_chr_mloc_homg_budg [--conf <path>]
 *
 * Currently, budgeted interaction tests are over hard-coded budget profile
 *  and, therefore, setting them in INI will have no effect
 */

#ifdef uTestSisMkiMkoSeoMlocIntrChr_h
#   error "Test Sis Mki Mki Seo Mloc Intr Chr included multiple times!"
#endif  // uTestSisMkiMkoSeoMlocIntrChr_h
#define uTestSisMkiMkoSeoMlocIntrChr_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uDistUtils.h"
#include "uExitCodes.h"
#include "uFilesystem.h"
#include "uLogf.h"
#include "uMockUpUtils.h"
#include "uSisIntrChrCommonFilt.h"
#include "uSisMkiMkoSeoMlocBudgIntrChrMixin.h"
#include "uSisMkiMkoSeoMlocIntrChrMixin.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypes.h"

#include <string.h>
#include <string>

namespace uTestSisMkiMkoSeoMlocIntrChr {

/**
 * A library glue for multi-locus, SEO growth with KI and KO constraints and
 * heterogeneous radius nodes
 */
U_MOCKUP_DECLARE_NO_QC_GLUE(HetrGlue,
                            uSisSeoMloc::GrowthMixin,
                            uSisSeoMloc::GrowthSimLevelMixin_threaded,
                            uSisHetrNodeRadiusMixin,
                            uSisHetrNodeRadiusSimLevelMixin,
                            uSisMkiMkoSeoMlocIntrChrMixin,
                            uSisMkiMkoSeoMlocIntrChrSimLevelMixin,
                            uSisNullIntrLamMixin,
                            uSisNullIntrLamSimLevelMixin,
                            uSisNullIntrNucbMixin,
                            uSisNullIntrNucbSimLevelMixin,
                            uSisUnifEnergyMixin,
                            uSisUnifEnergySimLevelMixin);

/**
 * Budgeted, heterogeneous glue
 */
U_MOCKUP_DECLARE_NO_QC_GLUE(HetrBudgGlue,
                            uSisSeoMloc::GrowthMixin,
                            uSisSeoMloc::GrowthSimLevelMixin_threaded,
                            uSisHetrNodeRadiusMixin,
                            uSisHetrNodeRadiusSimLevelMixin,
                            uSisMkiMkoSeoMlocBudgIntrChrMixin,
                            uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_threaded,
                            uSisNullIntrLamMixin,
                            uSisNullIntrLamSimLevelMixin,
                            uSisNullIntrNucbMixin,
                            uSisNullIntrNucbSimLevelMixin,
                            uSisUnifEnergyMixin,
                            uSisUnifEnergySimLevelMixin);

/**
 * A library glue for multi-locus, SEO growth with KI and KO constraints and
 * homogeneous radius nodes
 */
U_MOCKUP_DECLARE_NO_QC_GLUE(HomgGlue,
                            uSisSeoMloc::GrowthMixin,
                            uSisSeoMloc::GrowthSimLevelMixin_threaded,
                            uSisHomgNodeRadiusMixin,
                            uSisHomgNodeRadiusSimLevelMixin,
                            uSisMkiMkoSeoMlocIntrChrMixin,
                            uSisMkiMkoSeoMlocIntrChrSimLevelMixin,
                            uSisNullIntrLamMixin,
                            uSisNullIntrLamSimLevelMixin,
                            uSisNullIntrNucbMixin,
                            uSisNullIntrNucbSimLevelMixin,
                            uSisUnifEnergyMixin,
                            uSisUnifEnergySimLevelMixin);

/**
 * Budgeted, homogeneous glue
 */
U_MOCKUP_DECLARE_NO_QC_GLUE(HomgBudgGlue,
                            uSisSeoMloc::GrowthMixin,
                            uSisSeoMloc::GrowthSimLevelMixin_threaded,
                            uSisHomgNodeRadiusMixin,
                            uSisHomgNodeRadiusSimLevelMixin,
                            uSisMkiMkoSeoMlocBudgIntrChrMixin,
                            uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_threaded,
                            uSisNullIntrLamMixin,
                            uSisNullIntrLamSimLevelMixin,
                            uSisNullIntrNucbMixin,
                            uSisNullIntrNucbSimLevelMixin,
                            uSisUnifEnergyMixin,
                            uSisUnifEnergySimLevelMixin);

/**
 * Single interaction information used for verification
 */
typedef struct {
    /**
     * Lower (min) node identifier for 1st fragment in interaction pair; note
     *  lo <= hi
     */
    uUInt frag_a_nid_lo;
    /**
     * Upper (max) node identifier for 1st fragment in interaction pair
     */
    uUInt frag_a_nid_hi;
    /**
     * Locus identifier associated with fragment A
     */
    uUInt frag_a_lid;
    /**
     * Lower (min) node identifier for 2nd fragment in interaction pair
     */
    uUInt frag_b_nid_lo;
    /**
     * Upper (max) node identifier for 2nd fragment in interaction pair
     */
    uUInt frag_b_nid_hi;
    /**
     * Locus identifier associated with fragment B
     */
    uUInt frag_b_lid;
} intr_chr_info_t;

/**
 * Populate interaction info
 */
template <typename sim_t>
void get_info(intr_chr_info_t& nfo,
              const uUInt i,
              const uUIMatrix& intrs,
              const sim_t& sim) {
    // Clear old info
    memset(&nfo, 0, sizeof(intr_chr_info_t));
    const uUIMatrix& frags = sim.get_intr_chr_frags();
    uAssertBounds(i, U_TO_UINT(0), intrs.n_cols);
    // Fragment A
    const uUInt frag_a = intrs.at(uIntrLibPairIxMin, i);
    uAssertBounds(frag_a, U_TO_UINT(0), frags.n_cols);
    nfo.frag_a_nid_lo = frags.at(uIntrLibPairIxMin, frag_a);
    uAssertBounds(
        nfo.frag_a_nid_lo, U_TO_UINT(0), sim.get_max_total_num_nodes());
    nfo.frag_a_nid_hi = frags.at(uIntrLibPairIxMax, frag_a);
    uAssertBounds(
        nfo.frag_a_nid_hi, U_TO_UINT(0), sim.get_max_total_num_nodes());
    nfo.frag_a_lid = sim.get_growth_lid(nfo.frag_a_nid_lo);
    uAssert(nfo.frag_a_lid == sim.get_growth_lid(nfo.frag_a_nid_hi));
    uAssertBounds(nfo.frag_a_lid, U_TO_UINT(0), sim.get_num_loci());
    // Fragment B
    const uUInt frag_b = intrs.at(uIntrLibPairIxMax, i);
    uAssertBounds(frag_b, U_TO_UINT(0), frags.n_cols);
    nfo.frag_b_nid_lo = frags.at(uIntrLibPairIxMin, frag_b);
    uAssertBounds(
        nfo.frag_b_nid_lo, U_TO_UINT(0), sim.get_max_total_num_nodes());
    nfo.frag_b_nid_hi = frags.at(uIntrLibPairIxMax, frag_b);
    nfo.frag_b_lid = sim.get_growth_lid(nfo.frag_b_nid_lo);
    uAssert(nfo.frag_b_lid == sim.get_growth_lid(nfo.frag_b_nid_hi));
    uAssertBounds(nfo.frag_b_lid, U_TO_UINT(0), sim.get_num_loci());
}

/**
 * Utility verifies that all samples within simulation have satisfied knock-in
 * constraints
 */
template <typename sim_t>
uBool verify_ki(const sim_t& sim) {
    // Completed samples
    typedef typename sim_t::sample_t sample_t;
    const std::vector<sample_t>& samples = sim.get_completed_samples();
    const size_t num_samples = samples.size();
    if (num_samples < 1) {
        // Early out if no samples
        return uTRUE;
    }
    // Interactions
    const uUIMatrix& intrs = sim.get_intr_chr_kin();
    if (intrs.empty()) {
        // Early out if no interactions
        return uTRUE;
    }
    // Maximum number of failures
    const uUInt max_fail = sim.get_intr_chr_kin_max_fail();
    uAssertBoundsInc(max_fail, U_TO_UINT(0), U_TO_UINT(intrs.n_cols));
    // Detailed interaction ranges
    intr_chr_info_t nfo;
    // Buffer for computing squared distance
    uReal dist2[uDim_num];
    // Total number of failed interactions among all samples
    uUInt total_fail = U_TO_UINT(0);

    // Iterate over samples
    for (size_t s = 0; s < num_samples; ++s) {
        const sample_t& samp = samples[s];
        const uMatrix& node_positions = samp.get_node_positions();
        // Number of encountered failures
        uUInt num_fail = U_TO_UINT(0);

        // Iterate over constraints
        for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
            // Determine fragment node spans
            get_info(nfo, i, intrs, sim);

            // Check if any node in fragment A is within proximity distance to
            // fragment B
            uReal min_dist2 = U_REAL_MAX;
            uMatSz_t min_a = nfo.frag_a_nid_lo;
            uMatSz_t min_b = nfo.frag_b_nid_lo;
            uBool is_ok = uFALSE;
            for (uMatSz_t a = nfo.frag_a_nid_lo; a <= nfo.frag_a_nid_hi; ++a) {
                uAssertBounds(a, U_TO_MAT_SZ_T(0), node_positions.n_cols);
                const uReal* const A = node_positions.colptr(a);
                // Iterate over fragment B
                for (uMatSz_t b = nfo.frag_b_nid_lo; b <= nfo.frag_b_nid_hi;
                     ++b) {
                    uAssertBounds(b, U_TO_MAT_SZ_T(0), node_positions.n_cols);
                    const uReal* const B = node_positions.colptr(b);
                    const uReal A_radius = samp.get_node_radius(a, sim);
                    const uReal B_radius = samp.get_node_radius(b, sim);
                    // Compute squared distance between (A,B)
                    U_DIST2(dist2, A, B);
                    if (dist2[uDim_dist] < min_dist2) {
                        min_dist2 = dist2[uDim_dist];
                        min_a = a;
                        min_b = b;
                    }
                    if (sample_t::is_homg_radius) {
                        const uReal dist2_check = sim.get_intr_chr_kin_dist2(i);
                        is_ok |= (dist2[uDim_dist] <= dist2_check);
                    } else {
                        const uReal dist_check =
                            sim.get_intr_chr_kin_dist(i) + A_radius + B_radius;
                        const uReal dist2_check = dist_check * dist_check;
                        is_ok |= (dist2[uDim_dist] <= dist2_check);
                    }  // end check if homogeneous radius
                    if (is_ok) {
                        const uReal dist = sqrt(dist2[uDim_dist]);
                        const uReal thresh =
                            (sample_t::is_homg_radius)
                                ? sim.get_intr_chr_kin_dist(i)
                                : (sim.get_intr_chr_kin_dist(i) + A_radius +
                                   B_radius);
                        // Passed! Report passed test
                        uLogf(
                            "\tsample %d:KI INTR CHR "
                            "lid:[nid_lo,nid_hi]:\n\t\t%d:[%d,%d]->%d:[%d,%d] "
                            "PASSED at %d->%d\n\t\twith dist %.1f <= pad "
                            "thresh "
                            "%.1f (raw %.1f) A.\n",
                            (int)s,
                            (int)nfo.frag_a_lid,
                            (int)nfo.frag_a_nid_lo,
                            (int)nfo.frag_a_nid_hi,
                            (int)nfo.frag_b_lid,
                            (int)nfo.frag_b_nid_lo,
                            (int)nfo.frag_b_nid_hi,
                            (int)a,
                            (int)b,
                            (double)dist,
                            (double)thresh,
                            (double)sim.get_intr_chr_kin_dist(i));
                        // Avoid testing any more fragment B monomers if
                        // satisfied
                        break;
                    }  // end check if A-B interaction satisfied
                }      // end iteration over fragment B monomers
                if (is_ok) {
                    // Avoid testing any more fragment A monomers if satisfied
                    break;
                }  // end check for early out at fragment A
            }      // end iteration over fragment A monomers

            if (!is_ok) {
                // Failed! Report failed test
                ++num_fail;
                const uReal dist = (uReal)sqrt(min_dist2);
                const uReal A_radius = samp.get_node_radius(min_a, sim);
                const uReal B_radius = samp.get_node_radius(min_b, sim);
                const uReal thresh =
                    (sample_t::is_homg_radius)
                        ? sim.get_intr_chr_kin_dist(i)
                        : (sim.get_intr_chr_kin_dist(i) + A_radius + B_radius);
                uLogf(
                    "\tsample %d:KI INTR CHR "
                    "lid:[nid_lo,nid_hi]:\n\t\t%d:[%d,%d]->%d:[%d,%d] FAILED "
                    "with closest nodes at %d->%d\n\t\twith dist %.1f > pad "
                    "thresh "
                    "%.1f (raw %.1f) A.\n",
                    (int)s,
                    (int)nfo.frag_a_lid,
                    (int)nfo.frag_a_nid_lo,
                    (int)nfo.frag_a_nid_hi,
                    (int)nfo.frag_b_lid,
                    (int)nfo.frag_b_nid_lo,
                    (int)nfo.frag_b_nid_hi,
                    (int)min_a,
                    (int)min_b,
                    (double)dist,
                    (double)thresh,
                    (double)sim.get_intr_chr_kin_dist(i));
                // Check if fail budget exceeded
                if (num_fail > max_fail) {
                    uLogf("FAILED KI INTR CHR BUDGET (max fail=%d)\n",
                          (int)max_fail);
                    uAssert(false);
                    return uFALSE;
                }  // end check if fail budget exceeded
            }      // end check if interaction failed at sample
        }          // end iteration over interactions

        // If we reached here, sample passed
        uAssert(num_fail <= max_fail);
        uLogf("\tsample %d: KI INTR CHR VERIFIED WITH FAIL COUNT %d <= %d\n",
              (int)s,
              (int)num_fail,
              (int)max_fail);
        total_fail += num_fail;
    }  // end iteration over samples

    // Total number of interactions over all samples
    const uUInt total_intr = U_TO_UINT(num_samples) * U_TO_UINT(intrs.n_cols);
    // Compute proportion of failed interactions
    const uReal fail_rate = U_TO_REAL(total_fail) / U_TO_REAL(total_intr);
    uLogf("\t#########\n");
    uLogf("\tFAIL RATE: %f\n", (double)fail_rate);
    uLogf("\t#########\n");

    // If we reached here, all tests pass
    return uTRUE;
}

/**
 * Utility verifies that all samples within simulation have satisfied knock-out
 * constraints
 */
template <typename sim_t>
uBool verify_ko(const sim_t& sim) {
    // Completed samples
    typedef typename sim_t::sample_t sample_t;
    const std::vector<sample_t>& samples = sim.get_completed_samples();
    const size_t num_samples = samples.size();
    if (num_samples < 1) {
        // Early out if no samples
        return uTRUE;
    }
    // Interactions
    const uUIMatrix& intrs = sim.get_intr_chr_ko();
    if (intrs.empty()) {
        // Early out if no interactions
        return uTRUE;
    }
    // Maximum number of failures
    const uUInt max_fail = sim.get_intr_chr_ko_max_fail();
    uAssertBoundsInc(max_fail, U_TO_UINT(0), U_TO_UINT(intrs.n_cols));
    // Detailed interaction ranges
    intr_chr_info_t nfo;
    // Buffer for computing squared distance
    uReal dist2[uDim_num];
    // Total number of failed interactions among all samples
    uUInt total_fail = U_TO_UINT(0);

    // Iterate over samples
    for (size_t s = 0; s < num_samples; ++s) {
        const sample_t& samp = samples[s];
        const uMatrix& node_positions = samp.get_node_positions();
        // Number of encountered failures
        uUInt num_fail = U_TO_UINT(0);

        // Iterate over constraints
        for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
            // Determine fragment node spans
            get_info(nfo, i, intrs, sim);

            // Check if all nodes in fragment A are beyond proximity distance to
            // fragment B
            uReal min_dist2 = U_REAL_MAX;
            uMatSz_t min_a = nfo.frag_a_nid_lo;
            uMatSz_t min_b = nfo.frag_b_nid_lo;
            uBool is_ok = uTRUE;
            for (uMatSz_t a = nfo.frag_a_nid_lo; a <= nfo.frag_a_nid_hi; ++a) {
                uAssertBounds(a, U_TO_MAT_SZ_T(0), node_positions.n_cols);
                const uReal* const A = node_positions.colptr(a);
                // Iterate over fragment B
                for (uMatSz_t b = nfo.frag_b_nid_lo; b <= nfo.frag_b_nid_hi;
                     ++b) {
                    uAssertBounds(b, U_TO_MAT_SZ_T(0), node_positions.n_cols);
                    const uReal* const B = node_positions.colptr(b);
                    const uReal A_radius = samp.get_node_radius(a, sim);
                    const uReal B_radius = samp.get_node_radius(b, sim);
                    // Compute squared distance between (A,B)
                    U_DIST2(dist2, A, B);
                    if (dist2[uDim_dist] < min_dist2) {
                        min_dist2 = dist2[uDim_dist];
                        min_a = a;
                        min_b = b;
                    }
                    if (sample_t::is_homg_radius) {
                        const uReal dist2_check = sim.get_intr_chr_ko_dist2(i);
                        is_ok = (dist2[uDim_dist] > dist2_check);
                    } else {
                        const uReal dist_check =
                            sim.get_intr_chr_ko_dist(i) + A_radius + B_radius;
                        const uReal dist2_check = dist_check * dist_check;
                        is_ok = (dist2[uDim_dist] > dist2_check);
                    }  // end check if homogeneous radius
                    if (!is_ok) {
                        const uReal dist = (uReal)sqrt(dist2[uDim_dist]);
                        const uReal thresh =
                            (sample_t::is_homg_radius)
                                ? sim.get_intr_chr_ko_dist(i)
                                : (sim.get_intr_chr_ko_dist(i) + A_radius +
                                   B_radius);
                        // Failed! Report failed test
                        ++num_fail;
                        uLogf(
                            "\tsample %d:KO INTR CHR "
                            "lid:([nid_lo,nid_hi]:\n\t\t%d:[%d,%d]->%d:[%d,%d] "
                            "FAILED (%d) at %d->%d\n\t\twith dist %.1f <= pad "
                            "thresh %.1f (raw %.1f) A.\n",
                            (int)s,
                            (int)nfo.frag_a_lid,
                            (int)nfo.frag_a_nid_lo,
                            (int)nfo.frag_a_nid_hi,
                            (int)nfo.frag_b_lid,
                            (int)nfo.frag_b_nid_lo,
                            (int)nfo.frag_b_nid_hi,
                            (int)num_fail,
                            (int)a,
                            (int)b,
                            (double)dist,
                            (double)thresh,
                            (double)sim.get_intr_chr_ko_dist(i));
                        // Check if fail budget exceeded
                        if (num_fail > max_fail) {
                            uLogf("FAILED KO INTR CHR BUDGET (max fail=%d)\n",
                                  (int)max_fail);
                            uAssert(false);
                            return uFALSE;
                        }  // end check if fail budget exceeded

                        // Avoid testing any more fragment B monomers if
                        // not satisfied
                        break;
                    }  // end check if A-B interaction failed
                }      // end iteration over fragment B monomers

                if (!is_ok) {
                    // Avoid testing any more fragment A monomers if not
                    // satisfied
                    break;
                }  // end check for early out at fragment A
            }      // end iteration over fragment A monomers

            // Passed! Report passed test
            if (is_ok) {
                const uReal dist = (uReal)sqrt(min_dist2);
                const uReal A_radius = samp.get_node_radius(min_a, sim);
                const uReal B_radius = samp.get_node_radius(min_b, sim);
                const uReal thresh =
                    (sample_t::is_homg_radius)
                        ? sim.get_intr_chr_ko_dist(i)
                        : (sim.get_intr_chr_ko_dist(i) + A_radius + B_radius);
                uLogf(
                    "\tsample %d:KO INTR CHR "
                    "lid:([nid_lo,nid_hi]:\n\t\t%d:[%d,%d]->%d:[%d,%d] PASSED "
                    "(%d) with min at %d->%d\n\t\twith dist %.1f > pad thresh "
                    "%.1f "
                    "(raw %.1f) A.\n",
                    (int)s,
                    (int)nfo.frag_a_lid,
                    (int)nfo.frag_a_nid_lo,
                    (int)nfo.frag_a_nid_hi,
                    (int)nfo.frag_b_lid,
                    (int)nfo.frag_b_nid_lo,
                    (int)nfo.frag_b_nid_hi,
                    (int)num_fail,
                    (int)min_a,
                    (int)min_b,
                    (double)dist,
                    (double)thresh,
                    (double)sim.get_intr_chr_ko_dist(i));
            }
        }  // end iteration over interactions

        // If we reached here, sample passed
        uAssert(num_fail <= max_fail);
        uLogf("\tsample %d: KO INTR CHR VERIFIED WITH FAIL COUNT %d <= %d\n",
              (int)s,
              (int)num_fail,
              (int)max_fail);
        total_fail += num_fail;
    }  // end iteration over samples

    // Total number of interactions over all samples
    const uUInt total_intr = U_TO_UINT(num_samples) * U_TO_UINT(intrs.n_cols);
    // Compute proportion of failed interactions
    const uReal fail_rate = U_TO_REAL(total_fail) / U_TO_REAL(total_intr);
    uLogf("\t#########\n");
    uLogf("\tFAIL RATE: %f\n", (double)fail_rate);
    uLogf("\t#########\n");

    // If we reached here, all tests pass
    return uTRUE;
}

/**
 * Common utility to run a single test simulation
 */
template <typename sim_t>
int run_sim(const uSpConstConfig_t config,
            const std::string& announce,
            const uUInt test_id) {
    // Output configuration
    uLogf("%d: %s\n", (int)test_id, announce.c_str());
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Export!
    uSisUtils::export_sim(sim);

    // Destroy globals
    uG::teardown();

    // Verify interactions
    uLogf("%d: TEST knock-in chr-chr interactions...\n", (int)test_id);
    const uBool ki_pass = verify_ki(sim);
    uLogf("%d: TEST knock-in chr-chr interactions %s\n",
          (int)test_id,
          (ki_pass ? "PASS" : "FAIL"));
    uLogf("%d: TEST knock-out chr-chr interactions...\n", (int)test_id);
    const uBool ko_pass = verify_ko(sim);
    uLogf("%d: TEST knock-out chr-chr interactions %s\n",
          (int)test_id,
          (ko_pass ? "PASS" : "FAIL"));

    // Return normal if all tests passed, else error
    return (ki_pass && ko_pass) ? uExitCode_normal : uExitCode_error;
}

/**
 * Run a suite of tests
 */
template <typename sim_t>
int run_test_suite(const uCmdOptsMap& cmd_opts,
                   const std::string& announce,
                   const std::string& subdir_name) {
    // Create a base configuration
    uSpConfig_t template_config = uSmartPtr::make_shared<uConfig>();

    // Override from command line
    template_config->init(cmd_opts);

    // Output exit status
    int ec = uExitCode_normal;

    // Process child test configurations
    const uUInt MAX_TESTS = 1000;
    for (uUInt off = 0; off <= MAX_TESTS; ++off) {
        std::string test_child_conf_path;
        if (!template_config->resolve_path_vec(
                test_child_conf_path, uOpt_test_child_conf, off)) {
            // No more child tests!
            break;
        }
        // Configure test
        uCmdOptsMap job_cmd = cmd_opts;
        job_cmd.set_option(uOpt_get_cmd_switch(uOpt_conf_child_tr),
                           test_child_conf_path);
        uSpConfig_t job_config = uSmartPtr::make_shared<uConfig>();
        job_config->init(job_cmd);
        if (!job_config->get_child_tr()) {
            uLogf(
                "Error: test suite child configuration %d not found. "
                "Exiting.\n",
                (int)off);
            exit(uExitCode_error);
        }

        // Configure export flags
        job_config->export_flags =
            uExportCsv | uExportPml | uExportExtended | uExportIntrChr;
        job_config->get_child_tr()->export_flags = job_config->export_flags;

        // Determine output folder
        if (!subdir_name.empty()) {
            job_config->output_dir =
                (uFs::path(job_config->output_dir) / uFs::path(subdir_name))
                    .string();
            job_config->get_child_tr()->output_dir = job_config->output_dir;
        }

        uLogf(
            "\n**************************************\nTEST "
            "%d\n**************************************\n\n",
            (int)off);
        if (!run_sim<sim_t>(job_config->get_child_tr(), announce, off) ==
            uExitCode_normal) {
            // Test failed
            ec = uExitCode_error;
            break;
        }
    }

    // Return exit status
    return ec;
}

/**
 * Runs test suite for parameter budget
 */
template <typename sim_t>
int run_test_suite_budg_at(const uCmdOptsMap& cmd_opts_,
                           const std::string& announce_no_dot_or_newline,
                           const std::string& subdir_name_,
                           const uReal kin_fail_perc,
                           const uReal ko_fail_perc) {
    // Assume budgets are in [0,1]
    uAssertRealBoundsInc(kin_fail_perc, U_TO_REAL(0.0), U_TO_REAL(1.0));
    uAssertRealBoundsInc(ko_fail_perc, U_TO_REAL(0.0), U_TO_REAL(1.0));
    // Override command-line budgets
    uCmdOptsMap cmd_opts = cmd_opts_;
    const std::string kin_fail_key(
        uOpt_get_cmd_switch(uOpt_chr_knock_in_fail_budget));
    const std::string ko_fail_key(
        uOpt_get_cmd_switch(uOpt_chr_knock_out_fail_budget));
    const std::string str_kin_fail_perc = u2Str(kin_fail_perc);
    const std::string str_ko_fail_perc = u2Str(ko_fail_perc);
    cmd_opts.set_option(kin_fail_key, str_kin_fail_perc.c_str());
    cmd_opts.set_option(ko_fail_key, str_ko_fail_perc.c_str());
    // Resolve announce string
    const std::string announce =
        announce_no_dot_or_newline + std::string("\n\t-kin_budg = ") +
        str_kin_fail_perc + std::string("\n\t-ko_budg = ") + str_ko_fail_perc +
        std::string("\n");
    // Resolve output subdirectory
    const std::string subdir_name = subdir_name_ + std::string(".kinb.") +
                                    str_kin_fail_perc + std::string(".kob.") +
                                    str_ko_fail_perc;
    // Launch test suite
    return run_test_suite<sim_t>(cmd_opts, announce, subdir_name);
}

/**
 * Run test suites for hard-coded budget profile
 */
template <typename sim_t>
int run_test_suite_budg_prof(const uCmdOptsMap& cmd_opts,
                             const std::string& announce_no_dot_or_newline,
                             const std::string& subdir_name) {

    // Homogeneous fail budget profile (KIN fail budget == KO fail budget)
    const uReal homg_fail_budg_prof[] = {U_TO_REAL(0.0),
                                         U_TO_REAL(0.25),
                                         U_TO_REAL(0.5),
                                         U_TO_REAL(0.75),
                                         U_TO_REAL(1.0)};
    // Determine number of elements in profile
    size_t n_prof =
        sizeof(homg_fail_budg_prof) / sizeof(homg_fail_budg_prof[0]);
    // Process profile
    for (size_t i = 0; i < n_prof; ++i) {
        const uReal fail_budg = homg_fail_budg_prof[i];
        const int ec = run_test_suite_budg_at<sim_t>(cmd_opts,
                                                     announce_no_dot_or_newline,
                                                     subdir_name,
                                                     fail_budg,
                                                     fail_budg);
        if (ec != uExitCode_normal) {
            // Early out if suite failed
            return ec;
        }
    }

    // If we reached here, all tests passed!
    return uExitCode_normal;
}

/**
 * Initialize command line options, defer to default path if not specified
 */
void init_cmd(uCmdOptsMap& out_cmd,
              const uCmdOptsMap& in_cmd,
              const std::string& ini_name) {
    out_cmd = in_cmd;
    // Default INI path
    const std::string ini_path =
        "../tests/test_sis_mki_mko_seo_mloc_intr_chr/" + ini_name;
    // Check if key present, set to default if not specified
    const std::string conf_key(uOpt_get_cmd_switch(uOpt_conf));
    out_cmd.set_option_if_absent(conf_key, ini_path);
}

}  // namespace uTestSisMkiMkoSeoMlocIntrChr

/**
 * Entry point for SLOC commandlet test with heterogeneous radius nodes
 */
int uTestSisMkiMkoSeoIntrChrSlocHetrMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HetrGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.hetr");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Sloc Hetr.\n",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with heterogeneous radius nodes and
 *  failure budget
 */
int uTestSisMkiMkoSeoIntrChrSlocHetrBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HetrBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.hetr.budg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Sloc Hetr Budg",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with heterogeneous radius nodes
 */
int uTestSisMkiMkoSeoIntrChrMlocHetrMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HetrGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.hetr");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Mloc Hetr.\n",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with heterogeneous radius nodes and
 *  failure budget
 */
int uTestSisMkiMkoSeoIntrChrMlocHetrBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HetrBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.hetr.budg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Mloc Hetr Budg",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with homogeneous radius nodes
 */
int uTestSisMkiMkoSeoIntrChrSlocHomgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HomgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.homg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Sloc Homg.\n",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with homogeneous radius nodes and
 *  failure budget
 */
int uTestSisMkiMkoSeoIntrChrSlocHomgBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HomgBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.homg.budg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Sloc Homg Budg",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with homogeneous radius nodes
 */
int uTestSisMkiMkoSeoIntrChrMlocHomgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HomgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.homg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Mloc Homg.\n",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with homogeneous radius nodes and
 *  failure budget
 */
int uTestSisMkiMkoSeoIntrChrMlocHomgBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisMkiMkoSeoMlocIntrChr::HomgBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisMkiMkoSeoMlocIntrChr::init_cmd(
        cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.homg.budg");
    return uTestSisMkiMkoSeoMlocIntrChr::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Mki Mko Seo Intr Chr Mloc Homg Budg",
        subdir_name);
}

#endif  // U_BUILD_ENABLE_TESTS
