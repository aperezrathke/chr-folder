//****************************************************************************
// uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin.inl
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Mloc: Multiple loci
 * Budg: Interaction constraint failures allowed up to budget
 *
 * Defines a budgeted chrome-to-chrome interaction mixin supporting knock-in
 * and knock-out constraints over multiple (or single) loci
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * and/or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Budgeted chr-chr interaction simulation mixin
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_threaded
#else
class uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
    // @HACK - Inheritance for composition purposes (no virtual methods)
    : public uSisMkiMkoSeoMlocIntrChrSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Parent type
     */
    typedef uSisMkiMkoSeoMlocIntrChrSimLevelMixin<glue_t>
        parent_sim_level_intr_chr_mixin_t;

    /**
     * Default constructor
     */
#ifdef U_INJECT_THREADED_TLS
    uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_threaded()
#else
    uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_serial()
#endif  // U_INJECT_THREADED_TLS
    {
        clear();
    }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Reset state
        this->clear();
        // Initialize parent
        parent_sim_level_intr_chr_mixin_t::init(sim, config U_THREAD_ID_ARG);
        // Initialize failure budgets
        uIntrLibConfig::load_fail_budg(
            m_intr_chr_kin_max_fail,
            config,
            uOpt_chr_knock_in_fail_budget,
            uIntrChrDefaultKnockInFailBudgFrac,
            U_TO_UINT(this->get_intr_chr_kin().n_cols),
            uIntrChrKnockInDataName);
        uIntrLibConfig::load_fail_budg(
            m_intr_chr_ko_max_fail,
            config,
            uOpt_chr_knock_out_fail_budget,
            uIntrChrDefaultKnockOutFailBudgFrac,
            U_TO_UINT(this->get_intr_chr_ko().n_cols),
            uIntrChrKnockOutDataName);

        // Initialize TLS
        const uUInt num_candidates = sim.get_num_unit_sphere_sample_points();
        uAssert(num_candidates > U_TO_UINT(0));
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_chr_kin_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_chr_ko_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_chr_kin_fail_table, resize, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_chr_ko_fail_table, resize, num_candidates);
    }

    /**
     * Resets to default state
     */
    void clear() {
        parent_sim_level_intr_chr_mixin_t::clear();
        m_intr_chr_kin_max_fail = U_TO_UINT(0);
        m_intr_chr_ko_max_fail = U_TO_UINT(0);
        m_intr_chr_kin_delta_fails.clear();
        m_intr_chr_ko_delta_fails.clear();
        m_intr_chr_kin_fail_table.clear();
        m_intr_chr_ko_fail_table.clear();
    }

    // Accessors

    /**
     * @return Chr-chr knock-in interaction failure budget
     */
    inline uUInt get_intr_chr_kin_max_fail() const {
        uAssertBoundsInc(m_intr_chr_kin_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_chr_kin().n_cols));
        return m_intr_chr_kin_max_fail;
    }

    /**
     * @return Chr-chr knock-out interaction failure budget
     */
    inline uUInt get_intr_chr_ko_max_fail() const {
        uAssertBoundsInc(m_intr_chr_ko_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_chr_ko().n_cols));
        return m_intr_chr_ko_max_fail;
    }

    // Note: parent simulation is likely to be const, therefore making buffers
    // mutable objects
    inline uUIVecCol& get_intr_chr_kin_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_chr_kin_delta_fails);
    }
    inline uUIVecCol& get_intr_chr_ko_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_chr_ko_delta_fails);
    }
    inline uSisIntrChrBudgFailTable_t& get_intr_chr_kin_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_chr_kin_fail_table);
    }
    inline uSisIntrChrBudgFailTable_t& get_intr_chr_ko_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_chr_ko_fail_table);
    }

    // @return TLS handles for budget chr-chr interaction failure policy
    inline uSisIntrChrBudgFailPolicyArgs_t get_intr_chr_budg_fail_policy_args(
        U_THREAD_ID_0_PARAM) const {
        uSisIntrChrBudgFailPolicyArgs_t policy_args = {
            this->get_intr_chr_kin_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_chr_ko_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_chr_kin_fail_table(U_THREAD_ID_0_ARG),
            this->get_intr_chr_ko_fail_table(U_THREAD_ID_0_ARG)};
        return policy_args;
    }

private:
    /**
     * Maximum number of chr-chr knock-in interactions that can fail
     */
    uUInt m_intr_chr_kin_max_fail;

    /**
     * Maximum number of chr-chr knock-out interactions that can fail
     */
    uUInt m_intr_chr_ko_max_fail;

    /**
     * Scratch buffer for storing incremental (delta) knock-in failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_chr_kin_delta_fails);

    /**
     * Scratch buffer for storing incremental (delta) knock-out failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_chr_ko_delta_fails);

    // Note, the size of each candidate's fail list should be same as the
    //  corresponding count in the delta fail vector. Though this information
    //  is redundant, am still retaining separate 'delta' vector for failed
    //  counts at each candidate, as this makes it easier to implement
    //  energy selection models based on failure counts.

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-in interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrChrBudgFailTable_t,
                               m_intr_chr_kin_fail_table);

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-out interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrChrBudgFailTable_t,
                               m_intr_chr_ko_fail_table);
};
