//****************************************************************************
// uSisIntrChrBudgUtil.h
//****************************************************************************

/**
 * Utility header for budgeted chr-chr interactions
 */

#ifndef uSisIntrChrBudgUtil_h
#define uSisIntrChrBudgUtil_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// Typedefs and structs
//****************************************************************************

/**
 * Information needed for failed interaction removal at selected candidate
 */
typedef struct {
    /**
     * Interaction column index
     */
    uUInt intr_id;
    /**
     * Locus identifier corresponding to fragment 'A'
     */
    uUInt frag_a_lid;
    /**
     * Locus identifier corresponding to fragment 'B'
     */
    uUInt frag_b_lid;
} uSisIntrChrBudgFail_t;

/**
 * List of failed interactions at a single candidate
 */
typedef std::vector<uSisIntrChrBudgFail_t> uSisIntrChrBudgFailList_t;

/**
 * Table of failed interactions for all candidates, where each row is list of
 *  failed interactions at a single candidate
 */
typedef std::vector<uSisIntrChrBudgFailList_t> uSisIntrChrBudgFailTable_t;

/**
 * Budgeted chr-chr interaction failure policy arguments, stores scratch
 *  buffers for tracking failed interactions at all candidates
 * @WARNING - DO NOT CHANGE THE ORDER OF THESE DATA MEMBERS WITHOUT ALSO
 *  CHANGING ALL INSTANCES WHERE THIS STRUCTURE IS INITIALIZED!
 */
typedef struct {
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-in, chr-chr interaction failures at each candidate position
     */
    uUIVecCol& kin_delta_fails;
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-out, chr-chr interaction failures at each candidate position
     */
    uUIVecCol& ko_delta_fails;
    /**
     * Handle to scratch table for storing failed knock-in, chr-chr
     *  interactions at each candidate position
     */
    uSisIntrChrBudgFailTable_t& kin_fail_table;
    /**
     * Handle to scratch table for storing failed knock-in, chr-chr
     *  interactions at each candidate position
     */
    uSisIntrChrBudgFailTable_t& ko_fail_table;
} uSisIntrChrBudgFailPolicyArgs_t;

/**
 * Chr-chr knock-in budget access policy
 */
template <typename t_SisGlue>
class uSisIntrChrBudgAccKin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_chr_kin_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

/**
 * Chr-chr knock-out budget access policy
 */
template <typename t_SisGlue>
class uSisIntrChrBudgAccKo {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_chr_ko_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

#endif  // uSisIntrChrBudgUtil_h
