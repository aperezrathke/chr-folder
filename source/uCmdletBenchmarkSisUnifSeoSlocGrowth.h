//****************************************************************************
// uCmdletBenchmarkSisUnifSeoSlocGrowth.h
//****************************************************************************

/**
 * Commandlet for benchmarking growth of a chromatin chain (single locus)
 * with uniform sampling
 *
 * Usage:
 * -cmdlet_benchmark_sis_unif_seo_sloc_growth
 */

#ifdef uCmdletBenchmarkSisUnifSeoSlocGrowth_h
#   error "Commandlet Benchmark Sis Unif Seo Sloc Growth included multiple times!"
#endif  // uCmdletBenchmarkSisUnifSeoSlocGrowth_h
#define uCmdletBenchmarkSisUnifSeoSlocGrowth_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uMockUpUtils.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uTypes.h"

#include <iostream>
#include <string>

/**
 * Entry point for commandlet
 */
int uCmdletBenchmarkSisUnifSeoSlocGrowthMain(const uCmdOptsMap& cmd_opts) {
    // Expose final simulation
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t sim_t;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 1000;
    config->ensemble_size = 75;
    config->set_option(uOpt_max_node_diameter, U_TO_REAL(110.0));
    config->set_option(uOpt_nuclear_diameter, U_TO_REAL(4450.3264));
    config->num_nodes = std::vector<uUInt>(1 /*n_chains*/, 3300 /*n_nodes*/);
    config->num_unit_sphere_sample_points = 64;

    // Output configuration
    std::cout << "Running Commandlet Benchmark Sis Unif Seo Sloc Growth."
              << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_COMMANDLETS
