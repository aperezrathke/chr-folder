//****************************************************************************
// uGlobals.h
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#ifndef uGlobals_h
#define uGlobals_h

#include "uBuild.h"
#include "uConfig.h"
#include "uRand.h"
#include "uThread.h"

/**
 * Namespace to house global data
 */
namespace uG {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * uG::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
U_EXTERN_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: uG:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
U_EXTERN_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
extern U_DECLARE_TLS_DATA(uRand, rng);

/**
 * Initialize global data from runtime configuration
 */
extern void init(uSpConstConfig_t cfg);

/**
 * Initializes global data to default values
 */
extern void default_init();

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
extern void teardown();

}  // namespace uG

/**
 * Macro for accessing the global random number generator
 */
#define uRng U_ACCESS_TLS_DATA(uG::rng)

#endif  // uGlobals_h
