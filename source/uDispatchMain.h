//****************************************************************************
// uDispatchMain.h
//****************************************************************************

/**
 * Dispatches to user-generated main models
 */

#ifndef uDispatchMain_h
#define uDispatchMain_h

#include "uBuild.h"
#include "uCanvasMain.h"
#include "uCmdOptsMap.h"
#include "uLogf.h"
#include "uOpts.h"

//****************************************************************************
// Main
//****************************************************************************

/**
 * The main dispatcher! Usage:
 *
 *  --main_dispatch <cmdkey>
 */
class uDispatchMain {
public:
    /**
     * Entry point for app
     */
    static int main(const uCmdOptsMap& cmd_opts) {
        std::string cmd_key;
        cmd_opts.read_into(cmd_key, uOpt_get_cmd_switch(uOpt_main_dispatch));
        uLogf("Running main dispatcher for: %s\n", cmd_key.c_str());
        return uSisMainApps::dispatch(cmd_opts, cmd_key);
    }

private:
    // Disallow construction and assignment
    uDispatchMain();
    uDispatchMain(const uDispatchMain&);
    uDispatchMain& operator=(const uDispatchMain&);
};

#endif  // uDispatchMain_h
