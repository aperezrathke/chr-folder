//****************************************************************************
// uChromatinExportLigc.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportLigc.h"
#include "uAssert.h"
#include "uFilesystem.h"
#include "uGlobals.h"
#include "uLigcCore.h"
#include "uSisUtilsCollision.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsExportPath.h"
#include "uStreamUtils.h"
#include "uStringUtils.h"
#include "uThread.h"

#include <fstream>
#include <sstream>
#include <zstr.hpp>

//****************************************************************************
// Utilities
//****************************************************************************

namespace {
/**
 * Batch work object
 */
class uChromatinExportLigcBatchWork
#ifdef U_BUILD_ENABLE_THREADS
    : public uMappableWork
#endif  // U_BUILD_ENABLE_THREADS
{
public:
    /**
     * Batch export information type
     */
    typedef uChromatinExportLigc::info_t info_t;

    /**
     * Broad phase element identifier
     */
    typedef uLigcCore::elem_id_t elem_id_t;

    /**
     * Contact tuple (index of first monomer, index of second monomer)
     */
    typedef uLigcCore::contact_t contact_t;

    /**
     * Set of contact tuples within a polymer
     */
    typedef uLigcCore::profile_t profile_t;

    /**
     * Constructor
     */
    explicit uChromatinExportLigcBatchWork(const info_t& info)
        : m_info(info),
          m_num_samples(info.m_node_positions.size()),
          m_export_dir_csv(
              uLigcCore::get_format_dir(info.m_output_dir,
                                        uLigcCore::get_format_csv())),
          m_export_dir_csv_gz(
              uLigcCore::get_format_dir(info.m_output_dir,
                                        uLigcCore::get_format_csv_gz())),
          m_export_dir_csv_bulk(
              uLigcCore::get_format_dir(info.m_output_dir,
                                        uLigcCore::get_format_csv_bulk())),
          m_export_dir_csv_bulk_gz(
              uLigcCore::get_format_dir(info.m_output_dir,
                                        uLigcCore::get_format_csv_bulk_gz())),
          m_export_dir_bin(
              uLigcCore::get_format_dir(info.m_output_dir,
                                        uLigcCore::get_format_bin())),
          mp_fbulk(NULL),
          mp_fbulkgz(NULL) {
        uAssert(check_info(m_info));
        // Allocate thread-local buffers
        m_mask.init(U_TO_UINT(info.mp_node_radii->n_elem));
        U_INIT_TLS_DATA_0_PARAM(m_profile);
    }

    /**
     * Process all sample geometry data in streaming fashion
     * ASSUMED TO BE CALLED FROM MAIN THREAD!
     */
    void process(U_THREAD_ID_0_PARAM) {
        // Pre-conditions
        uAssert(check_info(m_info));
        uAssert(m_num_samples == m_info.m_node_positions.size());
        uAssert(m_num_samples == ((size_t)(m_info.mp_log_weights->n_elem)));
        // Early out if no work
        if (m_num_samples < U_TO_UINT(1)) {
            return;
        }
        // Make sure export directories exist
        this->create_export_dirs();
        // Initialize resources for capture CSV bulk data
        this->attempt_init_capture_csv_bulk();
#ifdef U_BUILD_ENABLE_THREADS
        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);

        // Chunk size must be positive
        uAssert(m_info.m_chunk_size > U_TO_UINT(0));

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        uParMap.wait_until_work_finished(*this, m_info.m_chunk_size);
#else
        // Else perform serial call! Note, do_range() returns FALSE if all
        // work processed
        uVerify(!this->do_range(U_TO_UINT(0),
                                U_TO_UINT(m_num_samples) U_THREAD_ID_ARG));
#endif  // U_BUILD_ENABLE_THREADS
        // Determine if any bulk CSV export
        this->attempt_export_csv_bulk();
        // Determine if any compressed bulk CSV export
        this->attempt_export_csv_bulk_gz();
        // Release resources for capturing CSV bulk data
        this->attempt_teardown_capture_csv_bulk();
    }

    /**
     * Process stream of sample over parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at
     *  ix_start
     * @return TRUE if work still to be done, FALSE if finished
     */
#ifdef U_BUILD_ENABLE_THREADS
    virtual
#endif  // U_BUILD_ENABLE_THREADS
        uBool
        do_range(const uUInt ix_start, const uUInt count U_THREAD_ID_PARAM) {
        const size_t end =
            std::min(static_cast<size_t>(ix_start + count), m_num_samples);

        // Determine if bonded interactions should be exported
        const uBool b_export_bonded = m_info.m_export & uExportLigcBonded;
        // Determine if CSV format should be exported
        const uBool b_export_csv = m_info.m_export & uExportLigcCsv;
        // Determine if compressed CSV format should be exported
        const uBool b_export_csv_gz = m_info.m_export & uExportLigcCsvGz;
        // Determine if bulk CSV format should be captured
        const uBool b_capture_csv_bulk = this->should_capture_csv_bulk();
        // Determine if binary format should be exported
        const uBool b_export_bin = m_info.m_export & uExportLigcBin;

        // Const handle to node radii
        const uVecCol& node_radii = *(m_info.mp_node_radii);
        // Const handle to log weights
        const uVecCol& log_weights = *(m_info.mp_log_weights);

        // Handle to TLS profile buffer
        profile_t& profile = this->get_profile(U_THREAD_ID_0_ARG);
        // Handle to TLS mask buffer
        uBitset& mask = this->get_broad_phase_mask(U_THREAD_ID_0_ARG);

        // Stream over samples
        for (size_t i = ix_start; i < end; ++i) {
            // Const handle to node positions
            uAssertBounds(i, 0, m_info.m_node_positions.size());
            const uMatrix& node_positions = *(m_info.m_node_positions[i]);
            // Const handle to broad phase
            uAssertBounds(i, 0, m_info.m_broad_phases.size());
            const uBroadPhase_t& broad_phase = *(m_info.m_broad_phases[i]);
            // Compute ligation contacts
            uLigcCore::proximity(profile,
                                 mask,
                                 m_info.m_ki_dist,
                                 node_radii,
                                 node_positions,
                                 broad_phase,
                                 m_info.m_broad_cell_length,
                                 m_info.m_broad_radius,
                                 b_export_bonded);
            // Extract log weight
            const uReal log_weight = log_weights.at(U_TO_MAT_SZ_T(i));
            // File identifier
            const std::string file_id(u2Str(i));
            // Export CSV format ligations
            if (b_export_csv) {
                this->export_csv(profile, log_weight, file_id);
            }
            // Export compressed CSV format ligations
            if (b_export_csv_gz) {
                this->export_csv_gz(profile, log_weight, file_id);
            }
            // Capture bulk CSV ligations
            if (b_capture_csv_bulk) {
                this->capture_csv_bulk(profile, log_weight U_THREAD_ID_ARG);
            }
            // Export binary format ligations
            if (b_export_bin) {
                this->export_bin(profile, log_weight, file_id);
            }
        }

        // Check if we've processed last element
        return (end < m_num_samples);
    }

private:
    // Disallow copy and assignment
    uChromatinExportLigcBatchWork(const uChromatinExportLigcBatchWork&);
    uChromatinExportLigcBatchWork& operator=(
        const uChromatinExportLigcBatchWork&);

#ifdef U_BUILD_ENABLE_ASSERT
    /**
     * Check export info makes sense!
     */
    static bool check_info(const info_t& info) {
        uAssert(info.m_broad_cell_length > U_TO_REAL(0.0));
        uAssert(info.m_broad_radius > U_TO_REAL(0.0));
        uAssert(info.m_ki_dist >= U_TO_REAL(0.0));
        uAssert(info.mp_log_weights);
        uAssert(info.mp_node_radii);
        uAssert(info.mp_node_radii->n_elem > U_TO_MAT_SZ_T(0));
        uAssert(info.mp_log_weights->n_elem ==
                U_TO_MAT_SZ_T(info.m_node_positions.size()));
        uAssert(info.m_node_positions.size() == info.m_broad_phases.size());
#ifdef U_BUILD_ENABLE_THREADS
        uAssert(info.m_chunk_size > U_TO_UINT(0));
#endif  // U_BUILD_ENABLE_THREADS
        return true;
    }
#endif  // U_BUILD_ENABLE_ASSERT

    /**
     * @return ligc format export path
     */
    std::string get_export_path(const uFs::path& export_dir,
                                const std::string& file_id,
                                const std::string& ligc_format) const {
        // File name
        const std::string fname(
            uSisUtils::get_export_fname(m_info.m_job_id, file_id, ligc_format));
        // File path
        return (export_dir / uFs::path(fname)).string();
    }

    /**
     * Create all necessary export directories
     */
    void create_export_dirs() const {
        // CSV export
        if (m_info.m_export & uExportLigcCsv) {
            uFs_create_dirs(m_export_dir_csv);
        }
        // compressed CSV export
        if (m_info.m_export & uExportLigcCsvGz) {
            uFs_create_dirs(m_export_dir_csv_gz);
        }
        // bulk CSV export
        if (m_info.m_export & uExportLigcCsvBulk) {
            uFs_create_dirs(m_export_dir_csv_bulk);
        }
        // bulk compressed CSV export
        if (m_info.m_export & uExportLigcCsvBulkGz) {
            uFs_create_dirs(m_export_dir_csv_bulk_gz);
        }
        // Binary export
        if (m_info.m_export & uExportLigcBin) {
            uFs_create_dirs(m_export_dir_bin);
        }
    }

    /**
     * CSV export ligation profile
     */
    void export_csv(const profile_t& profile,
                    const uReal log_weight,
                    const std::string& file_id) const {
        // File path
        const std::string fpath(this->get_export_path(
            m_export_dir_csv, file_id, uLigcCore::get_format_csv()));
        // Open file handle
        std::ofstream fout(fpath.c_str(), std::ofstream::out);
        if (!uLigcCore::export_csv(fout, profile, log_weight)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
        }
        // Close file handle
        fout.close();
    }

    /**
     * Compressed CSV export ligation profile
     */
    void export_csv_gz(const profile_t& profile,
                       const uReal log_weight,
                       const std::string& file_id) const {
        // File path
        const std::string fpath(this->get_export_path(
            m_export_dir_csv_gz, file_id, uLigcCore::get_format_csv_gz()));
        // Open file handle
        zstr::ofstream fout(fpath.c_str());
        if (!uLigcCore::export_csv_gz(fout, profile, log_weight)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
        }
        // WARNING - zstr::ofstream does not have close() method!
    }

    /**
     * Binary export ligation profile
     */
    void export_bin(const profile_t& profile,
                    const uReal log_weight,
                    const std::string& file_id) const {
        // File path
        const std::string fpath(this->get_export_path(
            m_export_dir_bin, file_id, uLigcCore::get_format_bin()));
        // Open file handle
        std::ofstream fout(fpath.c_str(),
                           std::ofstream::out | std::ios::binary);
        if (!uLigcCore::export_bin(fout, profile, log_weight)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
        }
        // Close file handle
        fout.close();
    }

    /**
     * Export bulk CSV data if enabled
     */
    void attempt_export_csv_bulk() {
        if (m_info.m_export & uExportLigcCsvBulk) {
            // File path
            const std::string fpath(
                this->get_export_path(m_export_dir_csv_bulk,
                                      std::string(U_SIS_BULK_EXPORT_FID),
                                      uLigcCore::get_format_csv_bulk()));
            // Open file handle
            uAssert(NULL == mp_fbulk);
            mp_fbulk = new std::ofstream(fpath.c_str(), std::ofstream::out);
            uAssert(mp_fbulk);
            if (*mp_fbulk) {
                // Append TLS captured CSV bulk data
                U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(mp_ss, this->cat_csv_bulk);
                mp_fbulk->close();
            } else {
                uLogf("Warning, unable to export to path: %s.\n",
                      fpath.c_str());
            }
            delete mp_fbulk;
            mp_fbulk = NULL;
        }
    }

    /**
     * Export compressed bulk CSV data if enabled
     */
    void attempt_export_csv_bulk_gz() {
        // Determine if any bulk compressed CSV export
        if (m_info.m_export & uExportLigcCsvBulkGz) {
            // File path
            const std::string fpath(
                this->get_export_path(m_export_dir_csv_bulk_gz,
                                      std::string(U_SIS_BULK_EXPORT_FID),
                                      uLigcCore::get_format_csv_bulk_gz()));
            // Open file handle
            uAssert(NULL == mp_fbulkgz);
            mp_fbulkgz = new zstr::ofstream(fpath.c_str());
            uAssert(mp_fbulkgz);
            if (*mp_fbulkgz) {
                // Append TLS captured CSV bulk data
                U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(mp_ss,
                                                    this->cat_csv_bulk_gz);
                // zstr::ofstream has no close() method!
            } else {
                uLogf("Warning, unable to export to path: %s.\n",
                      fpath.c_str());
            }
            delete mp_fbulkgz;
            mp_fbulkgz = NULL;
        }
    }

    /**
     * bulk CSV append stream
     */
    void cat_csv_bulk(const std::stringstream* const p_ss) {
        (*mp_fbulk) << p_ss->rdbuf();
    }

    /**
     * compressed bulk CSV append stream
     */
    void cat_csv_bulk_gz(std::stringstream* const p_ss) {
        p_ss->seekg(0, std::ios::beg);
        uStreamUtils::cat(*p_ss, *mp_fbulkgz);
    }

    /**
     * Allocate bulk CSV string stream buffers
     */
    void alloc_capture_csv_bulk(std::stringstream*& p_ss) {
        p_ss = new std::stringstream();
        uAssert(NULL != p_ss);
    }

    /**
     * Free bulk CSV string stream buffers
     */
    void free_capture_csv_bulk(std::stringstream*& p_ss) {
        delete p_ss;
        p_ss = NULL;
    }

    /**
     * Initialize (allocate) all TLS buffers for capture CSV bulk
     */
    void attempt_init_capture_csv_bulk() {
        if (this->should_capture_csv_bulk()) {
            U_INIT_TLS_DATA_0_PARAM(mp_ss);
            U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(mp_ss,
                                                this->alloc_capture_csv_bulk);
        }
    }

    /**
     * Release (free) all TLS buffers for capture CSV bulk
     */
    void attempt_teardown_capture_csv_bulk() {
        if (this->should_capture_csv_bulk()) {
            U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(mp_ss,
                                                this->free_capture_csv_bulk);
        }
    }

    /**
     * Thread local CSV capture ligation profile for bulk exporting
     */
    void capture_csv_bulk(const profile_t& profile,
                          const uReal log_weight U_THREAD_ID_PARAM) {
        if (!uLigcCore::export_csv(
                *U_ACCESS_TLS_DATA(mp_ss), profile, log_weight)) {
            uLogf("Warning, unable to capture ligc for bulk export!");
        }
    }

    /**
     * @return integer > 0 if capturing bulk CSV data, 0 o/w
     */
    inline uBool should_capture_csv_bulk() const {
        return (m_info.m_export & uExportLigcCsvBulk) |
               (m_info.m_export & uExportLigcCsvBulkGz);
    }

    /**
     * @return Bit set buffer for broad phase collision results
     */
    inline uBitset& get_broad_phase_mask(U_THREAD_ID_0_PARAM) {
        return m_mask.get_mask(U_THREAD_ID_0_ARG);
    }

    /**
     * @return Handle to thread-local buffer for storing ligation tuples
     */
    inline profile_t& get_profile(U_THREAD_ID_0_PARAM) {
        return U_ACCESS_TLS_DATA(m_profile);
    }

    /**
     * Batch export information
     */
    const info_t& m_info;

    /**
     * Cached number of samples to export
     */
    const size_t m_num_samples;

    /**
     * Export directory for CSV ligation tuples
     */
    const uFs::path m_export_dir_csv;

    /**
     * Export directory for compressed CSV ligation tuples
     */
    const uFs::path m_export_dir_csv_gz;

    /**
     * Export directory for bulk CSV ligation tuples
     */
    const uFs::path m_export_dir_csv_bulk;

    /**
     * Export directory for bulk compressed CSV ligation tuples
     */
    const uFs::path m_export_dir_csv_bulk_gz;

    /**
     * Export directory for binary ligation tuples
     */
    const uFs::path m_export_dir_bin;

    /**
     * Dynamic bit set, 1 if element needs narrow phase checking, 0 otherwise
     */
    uSisUtils::BroadPhaseCollisionMask m_mask;

    /**
     * Thread-local profile of ligation tuples
     */
    U_DECLARE_TLS_DATA(profile_t, m_profile);

    /**
     * Thread-local stream buffers for capturing bulk CSV data
     */
    U_DECLARE_TLS_DATA(std::stringstream*, mp_ss);

    /**
     * Output stream for bulk CSV data
     */
    std::ofstream* mp_fbulk;

    /**
     * Compressed output stream for bulk CSV data
     */
    zstr::ofstream* mp_fbulkgz;
};

}  // namespace

//****************************************************************************
// uChromatinExportLigc
//****************************************************************************

/**
 * @return uTRUE if ligc export indicated, uFALSE o/w
 */
uBool uChromatinExportLigc::should_export(const uUInt export_flags) {
    // Defer to core
    return uLigcCore::should_export(export_flags);
}

/**
 * Batch export ligc tuples for all samples
 * @WARNING - CAN ONLY BE CALLED FROM MAIN THREAD!
 * @param info - collated information needed to export ligc data
 */
void uChromatinExportLigc::export_ligc(const info_t& info) {
    // Create and process work
    uChromatinExportLigcBatchWork work(info);
    work.process(U_MAIN_THREAD_ID_0_ARG);
}
