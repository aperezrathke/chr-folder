//****************************************************************************
// uIntervalLogger.h
//****************************************************************************

/**
 * @brief Simple utility for reporting time taken by a code region for each
 *   execution of that code.
 */

#ifndef uIntervalLogger_h
#define uIntervalLogger_h

#include "uBuild.h"
#include "uAutoTimer.h"

#ifdef U_BUILD_ENABLE_INTERVAL_LOGGING

#include "uLogf.h"

/**
 * Enumerated unique identifiers for all interval logger stats
 */
enum uAllIntervalLoggerStats {
    // Total execution time
    uSTAT_TotalExecInterval = 0,
    // Last completed interval time
    uSTAT_TrialInterval,
    // Last completed quality control checkpoint time
    uSTAT_QcCheckpointInterval,
    uSTAT_IntervalLoggerMax
};

/**
 * Conditional macro for non-forced interval loggers
 */
#define U_SCOPED_INTERVAL_LOGGER(stat_id) \
    uStats::ScopedStatIntervalLogger ScopedLogger_##stat_id(stat_id)

/**
 * Macro defers to stats version if interval logging enabled, else
 * defers to a simple auto timer
 */
#define U_FORCE_SCOPED_INTERVAL_LOGGER(stat_id) \
    U_SCOPED_INTERVAL_LOGGER(stat_id)

namespace uStats {
/**
 * The static descriptions used for interval logging
 */
extern const char* GCounterIntervalLoggerDescs[uSTAT_IntervalLoggerMax];

/**
 * A synchronous timer for a single interval. Interval time span is
 * reported on destruction
 */
class ScopedStatIntervalLogger {
public:
    explicit ScopedStatIntervalLogger(const uAllIntervalLoggerStats stat_id)
        : m_stat_id(stat_id) {}

    ~ScopedStatIntervalLogger() {
        uLogf("%s\n", GCounterIntervalLoggerDescs[m_stat_id]);
    }

private:
    const uAllIntervalLoggerStats m_stat_id;
    uAutoTimer m_timer;
};
}  // namespace uStats

#else

#define U_SCOPED_INTERVAL_LOGGER(stat_id) ((void)0)

/**
 * Macro defers to stats version if interval logging enabled, else
 * defers to a simple auto timer
 */
#define U_FORCE_SCOPED_INTERVAL_LOGGER(stat_id) \
    uAutoTimer auto_timer_##stat_id

#endif  // U_BUILD_ENABLE_INTERVAL_LOGGING

#endif  // uIntervalLogger_h
