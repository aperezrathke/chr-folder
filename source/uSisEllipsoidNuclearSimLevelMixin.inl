//****************************************************************************
// uSisEllipsoidNuclearSimLevelMixin.inl
//****************************************************************************

/**
 * A nuclear mixin manages the details for checking candidate nodes to see if
 * they lie within the nuclear volume; this mixing enforces an ellipsoidal
 * nuclear confinement.
 *
 * An accessory structure is the simulation level nuclear mixin which defines
 * global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 */

/**
 * Mixin encapsulating global (sample independent) data and utilities. In
 * this case, the simulation level nuclear mixin manages the pre-allocated
 * scratch buffers used in nuclear constraint checking. Does not depend on
 * node homogeneity.
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisEllipsoidNuclearSimLevelMixin_threaded
#else
class uSisEllipsoidNuclearSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
{
private:
    /**
     * 3-D vector type
     */
    typedef typename uVecCol::fixed<uDim_num> uVec3d_t;

public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag that nucleus may be non-spherical
     */
    enum { is_sphere_nucleus = false };

    /**
     * Initializes simulation level nuclear mixin by pre-allocating scratch
     * buffers used for nuclear constraint checking.
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        // Initialize default nuclear diameter
        uReal def_nuc_diam = U_TO_REAL(0.0);
        config->read_into(def_nuc_diam, uOpt_nuclear_diameter);
        // Initialize x-axis
        this->init_nuc_axis(
            uDim_X, uOpt_nuclear_diameter_x, def_nuc_diam, sim, config);
        // Initialize y-axis
        this->init_nuc_axis(
            uDim_Y, uOpt_nuclear_diameter_y, def_nuc_diam, sim, config);
        // Initialize z-axis
        this->init_nuc_axis(
            uDim_Z, uOpt_nuclear_diameter_z, def_nuc_diam, sim, config);
        // Initialize min principal axis
        m_nuc_min_radius = m_nuc_radius.min();
        uAssert(m_nuc_min_radius > U_TO_REAL(0.0));
        // This assumes sphere sample points have been initialized!
        const uMatrix& unit_sphere_sample_points =
            sim.get_unit_sphere_sample_points();
        uAssert(unit_sphere_sample_points.size() > 0);
        // Assuming each row is a sample position (row-major)
        uAssert(unit_sphere_sample_points.n_cols == uDim_num);
        // Initialize TLS
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(m_nuc_schur_buffer,
                                         zeros,
                                         unit_sphere_sample_points.n_rows,
                                         unit_sphere_sample_points.n_cols);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_nuc_dot_buffer, zeros, unit_sphere_sample_points.n_rows);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_nuc_min_radius = U_TO_REAL(0.0);
        m_nuc_max_radius = U_TO_REAL(0.0);
        m_nuc_diameter.zeros();
        m_nuc_radius.zeros();
        m_nuc_homg_allowed_radius.zeros();
        m_nuc_homg_allowed_inv_sq_radius.zeros();
        m_nuc_schur_buffer.clear();
        m_nuc_dot_buffer.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Nuclear radius along parameter principal axis
     */
    inline uReal get_nuclear_radius(const enum uDim axis) const {
        uAssertBounds(
            U_TO_MAT_SZ_T(axis), U_TO_MAT_SZ_T(0), m_nuc_radius.n_elem);
        return m_nuc_radius.at(axis);
    }

    /**
     * @return Minimum nuclear radius among principal radii
     */
    inline uReal get_min_nuclear_radius() const {
        uAssertRealPosEq(m_nuc_min_radius, m_nuc_radius.min());
        return m_nuc_min_radius;
    }

    /**
     * Nodes (monomers) are rejected if they do not fit within nucleus. Since
     * all chromatin samples have the same nuclear dimensions, we are making
     * this a property of the simulation. This method has 'max' to allow for
     * different shapes such as triaxial ellipsoids which are specified by 3
     * radii, this makes it clear we simply want the largest radius.
     * @return Largest nuclear radius
     */
    inline uReal get_max_nuclear_radius() const {
        uAssertRealPosEq(m_nuc_max_radius, m_nuc_radius.max());
        return m_nuc_max_radius;
    }

    /**
     * Support more general ellipsoid interface, allow axial dimension to be
     * specified.
     * @WARNING - MUST ONLY BE CALLED USING RADIUS OF AN ACTUAL NODE!
     * @param axis - Axial dimension to query
     * @param node_radius - Radius of element to fit within nucleus
     * @param sim - Parent simulation
     * @return Allowed nuclear radius for the node to fit within along the
     *  parameter axis
     */
    inline uReal get_allowed_nuclear_radius(const enum uDim axis,
                                            const uReal node_radius,
                                            const sim_t& sim) const {
        uAssertBounds(
            U_TO_MAT_SZ_T(axis), U_TO_MAT_SZ_T(0), m_nuc_radius.n_elem);
        uAssertPosEq(m_nuc_radius.n_elem, uDim_num);
        if (sim_t::is_homg_radius) {
            // CASE: HOMOGENEOUS NODES
            uAssertPosEq(m_nuc_radius.n_elem, m_nuc_homg_allowed_radius.n_elem);
            uAssertPosEq(m_nuc_radius.n_elem,
                         m_nuc_homg_allowed_inv_sq_radius.n_elem);
            uAssertRealEq((m_nuc_radius.at(axis) - node_radius),
                          m_nuc_homg_allowed_radius.at(axis));
            uAssertRealPosEq(
                m_nuc_homg_allowed_radius.at(axis) *
                    m_nuc_homg_allowed_radius.at(axis),
                U_TO_REAL(1.0) / m_nuc_homg_allowed_inv_sq_radius.at(axis));
            return m_nuc_homg_allowed_radius.at(axis);
        }
        // ELSE: HETEROGENEOUS NODES
        uAssert(node_radius < m_nuc_radius.at(axis));
        return m_nuc_radius.at(axis) - node_radius;
    }

    /**
     * Nodes (monomers) are rejected if they do not fit within nucleus. Since
     * all chromatin samples have the same nuclear dimensions, we are making
     * this a property of the simulation.
     * @WARNING - MUST ONLY BE CALLED USING RADIUS OF AN ACTUAL NODE!
     * @param axis - Axial dimension to query
     * @param node_radius - Radius of element to fit within nucleus
     * @param sim - Parent simulation
     * @return Inverse of square of the allowed nuclear radius along parameter
     *  axis
     */
    inline uReal get_allowed_nuclear_inv_sq_radius(const enum uDim axis,
                                                   const uReal node_radius,
                                                   const sim_t& sim) const {
        uAssertBounds(
            U_TO_MAT_SZ_T(axis), U_TO_MAT_SZ_T(0), m_nuc_radius.n_elem);
        uAssertPosEq(m_nuc_radius.n_elem, uDim_num);
        if (sim_t::is_homg_radius) {
            // CASE: HOMOGENEOUS NODES
            uAssertPosEq(m_nuc_radius.n_elem, m_nuc_homg_allowed_radius.n_elem);
            uAssertPosEq(m_nuc_radius.n_elem,
                         m_nuc_homg_allowed_inv_sq_radius.n_elem);
            uAssertRealEq((m_nuc_radius.at(axis) - node_radius),
                          m_nuc_homg_allowed_radius.at(axis));
            uAssertRealPosEq(
                m_nuc_homg_allowed_radius.at(axis) *
                    m_nuc_homg_allowed_radius.at(axis),
                U_TO_REAL(1.0) / m_nuc_homg_allowed_inv_sq_radius.at(axis));
            return m_nuc_homg_allowed_inv_sq_radius.at(axis);
        }
        // ELSE: HETEROGENEOUS NODES
        const uReal allowed_radius =
            this->get_allowed_nuclear_radius(axis, node_radius, sim);
        uAssert(allowed_radius > U_TO_REAL(0.0));
        return U_TO_REAL(1.0) / (allowed_radius * allowed_radius);
    }

    // Note: parent simulation is likely to be const, therefore making buffers
    // mutable objects
    inline uMatrix& get_nuc_schur_buffer(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_nuc_schur_buffer);
    }
    inline uVecCol& get_nuc_dot_buffer(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_nuc_dot_buffer);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Initialize nuclear diameter and radius along parameter principal axis.
     * WILL EXIT WITH ERROR CODE IF NON-POSITIVE VALUE ENCOUNTERED!
     * @param axis - Principal axis to initialize
     * @param opt - Option key
     * @param def_diam - Default nuclear diameter
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration
     */
    void init_nuc_axis(const enum uDim axis,
                       const enum uOptE opt_axis,
                       const uReal def_diam,
                       const sim_t& sim,
                       uSpConstConfig_t config) {
        uAssertBounds(
            U_TO_MAT_SZ_T(axis), U_TO_MAT_SZ_T(0), m_nuc_diameter.n_elem);
        uAssertPosEq(m_nuc_diameter.n_elem, U_TO_MAT_SZ_T(uDim_num));
        uAssertPosEq(m_nuc_diameter.n_elem, m_nuc_radius.n_elem);
        uAssertPosEq(m_nuc_diameter.n_elem, m_nuc_homg_allowed_radius.n_elem);
        uAssertPosEq(m_nuc_diameter.n_elem,
                     m_nuc_homg_allowed_inv_sq_radius.n_elem);
        uReal& out_diam = m_nuc_diameter.at(axis);
        uReal& out_rad = m_nuc_radius.at(axis);
        uReal& out_homg_allowed_rad = m_nuc_homg_allowed_radius.at(axis);
        // Note, this is initialized to square radius, we must invert it after
        // function call finishes if homogeneous simulation
        uReal& out_homg_allowed_inv_sq_rad =
            m_nuc_homg_allowed_inv_sq_radius.at(axis);
        uSisUtils::NucConfig::init_axis(out_diam,
                                        out_rad,
                                        out_homg_allowed_rad,
                                        out_homg_allowed_inv_sq_rad,
                                        opt_axis,
                                        def_diam,
                                        sim,
                                        config);
        // Check if new max encountered
        m_nuc_max_radius = std::max(m_nuc_max_radius, out_rad);
        // Post-condition(s)
        uAssert(out_diam > U_TO_REAL(0.0));
        uAssertRealPosEq(out_rad, (U_TO_REAL(0.5) * out_diam));
        uAssert(out_homg_allowed_rad < out_rad);
        if (sim_t::is_homg_radius) {
            // Invert allowed square radius
            uAssertRealPosEq((out_homg_allowed_rad * out_homg_allowed_rad),
                             out_homg_allowed_inv_sq_rad);
            out_homg_allowed_inv_sq_rad =
                U_TO_REAL(1.0) / out_homg_allowed_inv_sq_rad;
        }
    }

    /**
     * Cached minimum principal nuclear radius
     */
    uReal m_nuc_min_radius;

    /**
     * Cached maximum principal nuclear radius
     */
    uReal m_nuc_max_radius;

    /**
     * Nuclear diameter for each principal axis in Angstroms
     */
    uVec3d_t m_nuc_diameter;

    /**
     * Nuclear radius for each principal axis in Angstroms
     */
    uVec3d_t m_nuc_radius;

    /**
     * Used for determining portion of nuclear volume that a chromatin node
     * center may reside: allowed_r[dim] = (nuclear_radius[dim] - node_radius)
     *
     * Assumed to be same for all nodes in simulation
     * @WARNING - ONLY VALID FOR HOMOGENEOUS NODE SIMULATIONS!
     */
    uVec3d_t m_nuc_homg_allowed_radius;

    /**
     * The allowed inverse squared radius of the nuclear sphere along each
     * principal axis. The radius is corrected by the node radius.
     *
     * inv_allowed_r[dim]^2 = 1.0 / (nuclear_radius[dim] - node_radius)^2
     *
     * Assumed to be same for all nodes in simulation.
     * @WARNING - ONLY VALID FOR HOMOGENEOUS NODE SIMULATIONS!
     */
    uVec3d_t m_nuc_homg_allowed_inv_sq_radius;

    /**
     * Buffer to store element-wise multiplication
     */
    mutable U_DECLARE_TLS_DATA(uMatrix, m_nuc_schur_buffer);

    /**
     * Stores final dot product of candidate positions
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_nuc_dot_buffer);
};
