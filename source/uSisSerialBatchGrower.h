//****************************************************************************
// uSisSerialBatchGrower.h
//****************************************************************************

#ifndef uSisSerialBatchGrower_h
#define uSisSerialBatchGrower_h

#include "uBuild.h"
#include "uAssert.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Performs batch regrowth operations
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisSerialBatchGrower {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Performs the first growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  complete for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     */
    inline static void batch_grow_1st(uWrappedBitset& status,
                                      std::vector<sample_t>& samples,
                                      uVecCol& log_weights,
                                      trial_runner_mixin_t& tr,
                                      sim_t& sim,
                                      const uUInt target_num_grow_steps
                                          U_THREAD_ID_PARAM) {
        // Verify positive grow step count
        uAssert(target_num_grow_steps > 0);

        // Cache number of samples to grow
        const size_t n_samples = samples.size();

        // Verify parallel sets
        uAssertPosEq(n_samples, log_weights.size());
        uAssert(n_samples == status.size());

        // Grow samples through first phase
        bool result = false;
        for (size_t i = 0; i < n_samples; ++i) {
            sample_t& sample(samples[i]);
            uReal& log_weight = log_weights.at(U_TO_MAT_SZ_T(i));
            result = tr.grow_1st(
                target_num_grow_steps, sample, log_weight, sim U_THREAD_ID_ARG);
            status.set(i, result);
        }
    }

    /**
     * Performs second growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  grow to for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     * @param num_completed_grow_steps_per_sample - The number of grow steps
     *  each sample has already completed
     */
    inline static void batch_grow_2nd(
        uWrappedBitset& status,
        std::vector<sample_t>& samples,
        uVecCol& log_weights,
        trial_runner_mixin_t& tr,
        sim_t& sim,
        const uUInt target_num_grow_steps,
        const uUInt num_completed_grow_steps_per_sample U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps_per_sample < target_num_grow_steps);
        bool result = false;
        for (size_t i = status.find_first(); i != status.npos;
             i = status.find_next(i)) {
            sample_t& sample(samples[i]);
            uReal& log_weight = log_weights.at(U_TO_MAT_SZ_T(i));
            result = tr.grow_2nd(target_num_grow_steps,
                                 sample,
                                 num_completed_grow_steps_per_sample,
                                 log_weight,
                                 sim U_THREAD_ID_ARG);
            status.set(i, result);
        }
    }

    /**
     * Performs the next growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  grow to for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     * @param num_completed_grow_steps_per_sample - The number of grow steps
     *  each sample has already completed
     */
    inline static void batch_grow_nth(
        uWrappedBitset& status,
        std::vector<sample_t>& samples,
        uVecCol& log_weights,
        trial_runner_mixin_t& tr,
        sim_t& sim,
        const uUInt target_num_grow_steps,
        const uUInt num_completed_grow_steps_per_sample U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps_per_sample < target_num_grow_steps);
        bool result = false;
        for (size_t i = status.find_first(); i != status.npos;
             i = status.find_next(i)) {
            sample_t& sample(samples[i]);
            uReal& log_weight = log_weights.at(U_TO_MAT_SZ_T(i));
            result = tr.grow_nth(target_num_grow_steps,
                                 sample,
                                 num_completed_grow_steps_per_sample,
                                 log_weight,
                                 sim U_THREAD_ID_ARG);
            status.set(i, result);
        }
    }
};

#endif  // uSisSerialBatchGrower_h
