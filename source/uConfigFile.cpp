// ConfigFile.cpp

#include "uConfigFile.h"
#include "uFilesystem.h"
#include "uLogf.h"

using std::string;

uConfigFile::uConfigFile(const string filename,
                         const string delimiter,
                         const string comment,
                         const string sentry)
    : myDelimiter(delimiter), myComment(comment), mySentry(sentry) {
    // Construct a ConfigFile, getting keys and values from given file

    uLogf("Reading configuration file at: %s\n", filename.c_str());
    std::ifstream in(filename.c_str());

    if (!in)
        throw file_not_found(filename);

    in >> (*this);
    in.close();

    // Determine configuration directory for resolving paths
    myDir = uFs::canonical(uFs::path(filename)).parent_path().string();
}

uConfigFile::uConfigFile()
    : myDelimiter(string(1, '=')), myComment(string(1, '#')) {
    // Construct a ConfigFile without a file; empty
}

void uConfigFile::remove(const string& key) {
    // Remove key and its value
    myContents.erase(myContents.find(key));
    return;
}

bool uConfigFile::key_exists(const string& key) const {
    // Indicate whether key is found
    mapci p = myContents.find(key);
    return (p != myContents.end());
}

// Obtain all registered keys
void uConfigFile::get_keys(std::vector<string>& keys) const {
    for (mapci it = myContents.begin(); it != myContents.end(); ++it) {
        keys.push_back(it->first);
    }
}

/* static */
void uConfigFile::trim(string& s) {
    // Remove leading and trailing whitespace
    static const char whitespace[] = " \n\t\v\r\f";
    s.erase(0, s.find_first_not_of(whitespace));
    s.erase(s.find_last_not_of(whitespace) + 1U);
}

std::ostream& operator<<(std::ostream& os, const uConfigFile& cf) {
    // Save a ConfigFile to os
    for (uConfigFile::mapci p = cf.myContents.begin(); p != cf.myContents.end();
         ++p) {
        os << p->first << " " << cf.myDelimiter << " ";
        os << p->second << std::endl;
    }
    return os;
}

std::istream& operator>>(std::istream& is, uConfigFile& cf) {
    // Load a ConfigFile from is
    // Read in keys and values, keeping internal whitespace
    typedef string::size_type pos;
    const string& delim = cf.myDelimiter;  // separator
    const string& comm = cf.myComment;     // comment
    const string& sentry = cf.mySentry;    // end of file sentry
    const pos skip = delim.length();       // length of separator

    string nextline = "";  // might need to read ahead to see where value ends

    while (is || nextline.length() > 0) {
        // Read an entire line at a time
        string line;
        if (nextline.length() > 0) {
            line = nextline;  // we read ahead; use it now
            nextline = "";
        } else {
            std::getline(is, line);
        }

        // Ignore comments
        line = line.substr(0, line.find(comm));

        // Check for end of file sentry
        if (sentry != "" && line.find(sentry) != string::npos)
            return is;

        // Parse the line if it contains a delimiter
        pos delimPos = line.find(delim);
        if (delimPos < string::npos) {
            // Extract the key
            string key = line.substr(0, delimPos);
            line.replace(0, delimPos + skip, "");

            // See if value continues on the next line
            // Stop at blank line, next line with a key, end of stream,
            // or end of file sentry
            bool terminate = false;
            while (!terminate && is) {
                std::getline(is, nextline);
                terminate = true;

                string nlcopy = nextline;
                uConfigFile::trim(nlcopy);
                if (nlcopy == "")
                    continue;

                nextline = nextline.substr(0, nextline.find(comm));
                if (nextline.find(delim) != string::npos)
                    continue;
                if (sentry != "" && nextline.find(sentry) != string::npos)
                    continue;

                nlcopy = nextline;
                uConfigFile::trim(nlcopy);
                if (nlcopy != "")
                    line += "\n";
                line += nextline;
                terminate = false;
            }

            // Store key and value
            uConfigFile::trim(key);
            uConfigFile::trim(line);
            cf.myContents[key] = line;  // overwrites if key is repeated
        }
    }

    return is;
}

/**
 * @return true if key exists, false otherwise. If key exists, output path
 *  argument is always absolute. However, if user value is a relative path,
 *  the output absolute path is relative to the location of this
 *  configuration file. If the user value was already an absolute path, it
 *  is returned unmodified. If key does not exist, path argument is
 *  unmodified.
 */
bool uConfigFile::resolve_path(std::string& strpath,
                               const std::string& key) const {
    // Get the value corresponding to key and store in var
    // Return true if key is found
    // Otherwise leave var untouched
    mapci p = myContents.find(key);
    bool found = (p != myContents.end());
    if (found) {
        strpath = p->second;
        uFs::path fspath(strpath);
        // For relative paths, resolve relative to configuration directory
        if (fspath.is_relative()) {
            strpath = (myDir / fspath).string();
        }
    }
    return found;
}
