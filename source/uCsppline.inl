//****************************************************************************
// uCsppline.inl
//****************************************************************************

/**
 * Implementation file for uCsspline - do not include this file directly!
 */

/**
 * Header guard - file should only be included by uCsspline.h
 */
#ifndef uCsspline_inl
#define uCsspline_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uCsppline.h"
#include "uIsFinite.h"
#include "uStaticAssert.h"
#include "uTypes.h"

#include <algorithm>
#include <boost/math/tools/minima.hpp>

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Private utilities accessible to uCsppline
 */
template <typename t_real, typename t_driver>
class uCspplinePutils {
private:
    ////////////////////////////////////////////////////////////////
    // Typedefs

    /**
     * Corresponding spline type
     */
    typedef uCsppline<t_real, t_driver> spline_t;

    /**
     * Vector type
     */
    typedef typename spline_t::vec_t vec_t;

    /**
     * Real-valued scalar type
     */
    typedef typename spline_t::real_t real_t;

    /**
     * Driver type
     */
    typedef typename spline_t::driver_t driver_t;

    /**
     * Only corresponding spline and driver may access these utilities
     */
    friend spline_t;
    friend driver_t;

    ////////////////////////////////////////////////////////////////
    // Shifted vector and matrix views

    /**
     * Green & Silverman text uses non-standard indexing for certain vectors
     * and matrices. These utility classes are to make indexing consistent
     * between standard and non-standard indexing use cases.
     */

    /**
     * Right-shifted real-valued vector view (non-const)
     */
    template <uMatSz_t rshift_>
    class vec_rshift_view {
    private:
        enum { rshift = rshift_ };

    public:
        explicit vec_rshift_view(vec_t& v) : m_v(v) {}
        inline real_t& at(const uMatSz_t i) {
            uAssertBounds(i, rshift, m_v.n_elem + rshift);
            return m_v.at(i - rshift);
        }
        inline real_t at(const uMatSz_t i) const {
            uAssertBounds(i, rshift, m_v.n_elem + rshift);
            return m_v.at(i - rshift);
        }

    private:
        vec_t& m_v;
    };

    /**
     * Right-shifted real-valued vector view (const)
     */
    template <uMatSz_t rshift_>
    class vec_const_rshift_view {
    private:
        enum { rshift = rshift_ };

    public:
        explicit vec_const_rshift_view(const vec_t& v) : m_v(v) {}
        inline real_t at(const uMatSz_t i) const {
            uAssertBounds(i, rshift, m_v.n_elem + rshift);
            return m_v.at(i - rshift);
        }

    private:
        const vec_t& m_v;
    };

    /**
     * Right-shifted real-valued matrix view - applies to row & column indices
     */
    template <uMatSz_t rshift_row_, uMatSz_t rshift_col_, typename t_mat>
    class mat_rshift_view {
    private:
        enum { rshift_row = rshift_row_, rshift_col = rshift_col_ };

    public:
        typedef t_mat mat_t;
        explicit mat_rshift_view(mat_t& m) : m_m(m) {}
        inline real_t& at(const uMatSz_t i, const uMatSz_t j) {
            uAssertBounds(i, rshift_row, m_m.n_rows + rshift_row);
            uAssertBounds(j, rshift_col, m_m.n_cols + rshift_col);
            return m_m.at(i - rshift_row, j - rshift_col);
        }
        inline real_t at(const uMatSz_t i, const uMatSz_t j) const {
            uAssertBounds(i, rshift_row, m_m.n_rows + rshift_row);
            uAssertBounds(j, rshift_col, m_m.n_cols + rshift_col);
            return m_m.at(i - rshift_row, j - rshift_col);
        }

    private:
        mat_t& m_m;
    };

    /**
     * Right-shifted real-valued matrix view - applies to row & column indices
     * (const)
     */
    template <uMatSz_t rshift_row_, uMatSz_t rshift_col_, typename t_mat>
    class mat_const_rshift_view {
    private:
        enum { rshift_row = rshift_row_, rshift_col = rshift_col_ };

    public:
        typedef t_mat mat_t;
        explicit mat_const_rshift_view(const mat_t& m) : m_m(m) {}
        inline real_t at(const uMatSz_t i, const uMatSz_t j) const {
            uAssertBounds(i, rshift_row, m_m.n_rows + rshift_row);
            uAssertBounds(j, rshift_col, m_m.n_cols + rshift_col);
            return m_m.at(i - rshift_row, j - rshift_col);
        }

    private:
        const mat_t& m_m;
    };

    ////////////////////////////////////////////////////////////////
    // Auto-smoothing

    /**
     * Read-only specification to auto-smoother
     */
    typedef struct {
        // Filtered y-coordinates
        vec_t y_filt;
        // Filtered weights
        vec_t w_filt;
        // Maximum number of iterations to search for local minima
        unsigned int max_iters;
        // Effective degrees of freedom, may be unused
        real_t target_edf;
        // Minimum value of roughness penalty alpha
        real_t min_alpha;
        // Maximum value of roughness penalty alpha
        real_t max_alpha;
        // Selected auto-smooth type
        enum uCspplineAutoSmooth e_smooth;
    } auto_smooth_spec_t;

    /**
     * Writable data buffers needed by auto-smoother
     */
    typedef struct {
        // Vector for storing band matrix diagonals of linear smooth operator
        // (aka hat matrix from Green & Silverman, section 3.2.2 and 3.5.3)
        vec_t a_diag;
        // Linear smooth operator at minimum criterion score
        vec_t min_a_diag;
        // Y-coordinates at minimum knot points
        vec_t min_y;
        // Second derivatives at minimum knot points
        vec_t min_dd;
        // Minimum encountered score
        real_t min_score;
    } auto_smooth_state_t;

    /**
     * Auto-smooths spline roughness penalty alpha by searching for a local
     * minima between [min_alpha, max_alpha] according to template scorer type
     */
    template <typename t_scorer>
    class auto_smoother {
    public:
        /**
         * Score type, e.g. computes cross-validation or GCV score
         */
        typedef t_scorer scorer_t;

        /**
         * Constructor
         * @param spline - output spline to fit to design points
         * @param state - writable data buffers needed by smoother
         * @param config - read-only configuration
         */
        auto_smoother(spline_t& spline,
                      auto_smooth_state_t& state,
                      const auto_smooth_spec_t& spec)
            : m_spline(spline), m_state(state), m_spec(spec) {
            uAssert(uIsFinite(m_spec.min_alpha));
            uAssert(uIsFinite(m_spec.max_alpha));
            uAssertBoundsInc(
                m_spec.min_alpha, static_cast<real_t>(0.0), m_spec.max_alpha);
            uAssert(m_spec.max_iters > 0);
        }

        /**
         * Uses Brent's algorithm to minimize score criterion as a function of
         * roughness penalty
         */
        bool run(typename spline_t::auto_smooth_info_t* const info) {
            // Number of bits of precision
            const int bits = std::numeric_limits<real_t>::digits;
            // Reset minimum score
            m_state.min_score = std::numeric_limits<real_t>::max();
            // Prime driver
            if (!m_spline.m_driver.prime(m_spline.m_x,
                                         m_spec.y_filt,
                                         m_spec.w_filt,
                                         m_spec.e_smooth)) {
                return false;
            }
            // Run Brent's algorithm
            boost::uintmax_t max_iters =
                static_cast<boost::uintmax_t>(m_spec.max_iters);
            const std::pair<real_t, real_t> result =
                boost::math::tools::brent_find_minima(
                    *this, m_spec.min_alpha, m_spec.max_alpha, bits, max_iters);
            uAssert(result.second == m_state.min_score);
            // Output spline corresponding to local optimal roughness penalty
            m_spline.m_y = m_state.min_y;
            m_spline.m_dd = m_state.min_dd;
            // Allow driver to clean its state
            m_spline.m_driver.post();
            // Capture detailed results
            if (info) {
                info->alpha = alpha_inv_trans(result.first);
                info->edf = uMatrixUtils::sum(m_state.min_a_diag);
                info->num_iters = static_cast<unsigned int>(max_iters);
                info->score = result.second;
            }
            return (result.second != std::numeric_limits<real_t>::max()) &&
                   (m_spline.size() > 0);
        }

        /**
         * Callback used by Brent's algorithm minimizer
         * @param alpha - candidate roughness penalty in [0,1)
         * @return score at candidate roughness penalty alpha
         */
        real_t operator()(const real_t alpha) {
            // Build spline with roughness penalty alpha
            const bool success = m_spline.m_driver.fit(m_spline.m_y,
                                                       m_spline.m_dd,
                                                       m_state.a_diag,
                                                       m_spline.m_x,
                                                       m_spec.y_filt,
                                                       m_spec.w_filt,
                                                       alpha);
            if (success) {
                uAssert(m_spline.size() > 0);
                uAssertPosEq(m_spline.m_y.n_elem, m_state.a_diag.n_elem);
                uAssertPosEq(m_spline.m_y.n_elem, m_spec.w_filt.n_elem);
                // Compute fitness score
                const real_t score = scorer_t::get_score(*this);
                // Check if we found new minimum
                if (score < m_state.min_score) {
                    m_state.min_score = score;
                    // @TODO - storing a_diag is only needed for detailed info
                    // on the found effective degrees of freedom, if this has
                    // non-negligible performance impact, then consider removing
                    m_state.min_a_diag = m_state.a_diag;
                    m_state.min_y = m_spline.m_y;
                    m_state.min_dd = m_spline.m_dd;
                }
                return score;
            }
            // If we reach here, then spline was not fit, return maximum score
            return std::numeric_limits<real_t>::max();
        }

        spline_t& m_spline;
        auto_smooth_state_t& m_state;
        const auto_smooth_spec_t& m_spec;
    };

    /**
     * Leave-one-out cross-validation score for roughness penalty 'alpha'
     */
    class scorer_cv {
    public:
        /**
         * @return cross-validation score
         */
        static real_t get_score(const auto_smoother<scorer_cv>& s) {
            uAssert(s.m_spec.y_filt.is_finite());
            uAssert(s.m_spec.w_filt.is_finite());
            uAssert(s.m_spline.m_y.is_finite());
            uAssert(s.m_state.a_diag.is_finite());
            const uMatSz_t N_ELEM = s.m_spline.m_x.n_elem;
            real_t score = static_cast<real_t>(0.0);
            real_t term;
            for (uMatSz_t i = 0; i < N_ELEM; ++i) {
                uAssert(check_bounds_3v(
                    i, s.m_spec.y_filt, s.m_spline.m_y, s.m_state.a_diag));
                // Add small epsilon to avoid divide-by-zero
                const real_t denom =
                    (static_cast<real_t>(1.0) - s.m_state.a_diag.at(i)) +
                    std::numeric_limits<real_t>::epsilon();
                uAssert(denom != static_cast<real_t>(0.0));
                term = (s.m_spec.y_filt.at(i) - s.m_spline.m_y.at(i)) / denom;
                uAssertBounds(i, 0, s.m_spec.w_filt.n_elem);
                score += s.m_spec.w_filt.at(i) * term * term;
            }
            score /= static_cast<real_t>(N_ELEM);
            return score;
        }
    };

    /**
     * Generalized cross-validation (GCV) score for roughness penalty 'alpha'
     */
    class scorer_gcv {
    public:
        /**
         * @return GCV score
         */
        static real_t get_score(const auto_smoother<scorer_gcv>& s) {
            uAssert(s.m_spec.y_filt.is_finite());
            uAssert(s.m_spec.w_filt.is_finite());
            uAssert(s.m_spline.m_y.is_finite());
            uAssert(s.m_state.a_diag.is_finite());
            const uMatSz_t N_ELEM = s.m_spline.m_x.n_elem;
            real_t denom =
                static_cast<real_t>(1.0) - uMatrixUtils::mean(s.m_state.a_diag);
            denom *= denom * static_cast<real_t>(N_ELEM);
            real_t score = static_cast<real_t>(0.0);
            real_t term;
            for (uMatSz_t i = 0; i < N_ELEM; ++i) {
                uAssert(check_bounds_3v(
                    i, s.m_spec.y_filt, s.m_spline.m_y, s.m_spec.w_filt));
                term = (s.m_spec.y_filt.at(i) - s.m_spline.m_y.at(i));
                score += s.m_spec.w_filt.at(i) * term * term;
            }
            // Add small epsilon to avoid divide-by-zero
            score /= (denom + std::numeric_limits<real_t>::epsilon());
            return score;
        }
    };

    /**
     * Effective degrees of freedom score for roughness penalty 'alpha'
     */
    class scorer_edf {
    public:
        /**
         * @return divergence from target effective degrees of freedom
         */
        static real_t get_score(const auto_smoother<scorer_edf>& s) {
            // If smoothing by effective degrees of freedom, then edf must be
            // in [1, n] where n is number of knots.
            uAssertBoundsInc(s.m_spec.target_edf,
                             static_cast<real_t>(1.0),
                             static_cast<real_t>(s.m_spline.m_x.n_elem));
            // Trace of linear smooth operator is defined as effective degrees
            // of freedom according to Hastie & Tibshirani, Generalized
            // Additive Models, Section 3.5
            real_t score =
                uMatrixUtils::sum(s.m_state.a_diag) - s.m_spec.target_edf;
            // Define divergence as squared distance from target edf
            score *= score;
            return score;
        }
    };

    ////////////////////////////////////////////////////////////////
    // Handle case with only 1 or 2 knots

    /**
     * Assumes spline has been clear()'d already
     * @return true if spline is build for only 1 or 2 knots
     */
    static bool fit_linear(
        spline_t& sp,
        vec_t& y_filt,
        typename spline_t::auto_smooth_info_t* const info = NULL) {
        uAssertPosEq(y_filt.n_elem, sp.m_x.n_elem);
        if (y_filt.n_elem < 3) {
            sp.m_y = y_filt;
            sp.m_dd.clear();
            if (info) {
                info->alpha = static_cast<real_t>(1.0);
                info->edf = static_cast<real_t>(y_filt.n_elem);
                info->num_iters = 0;
                info->score = static_cast<real_t>(0.0);
            }
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////
    // Filter tied design points

    // Compare functor returns i < j iff x[i] < x[j]
    class compare_lt {
    public:
        explicit compare_lt(const vec_t& x_) : m_x(x_) {}
        bool operator()(const uMatSz_t i, const uMatSz_t j) const {
            uAssertBounds(i, 0, m_x.n_elem);
            uAssertBounds(j, 0, m_x.n_elem);
            return m_x.at(i) < m_x.at(j);
        }

    private:
        const vec_t& m_x;
    };

    /**
     * Determine sort order of x, which can be used to sort other parallel
     * arrays
     * @param order - output index such that x[order[i]] gives i-th sorted
     *  element of x in ascending order
     * @param x - vector to determine sort order
     */
    static void get_sort_order(uMatSzVecCol& order, const vec_t& x) {
        // Initialize order as vector with consecutive elements 1 ... N
        order.set_size(x.n_elem);
        for (uMatSz_t i = 0; i < order.n_elem; ++i) {
            order.at(i) = i;
        }
        // Determine index permutation such that x[i] <= x[j]
        std::sort(order.begin(), order.end(), compare_lt(x));
    }

    // Compare functor returns i == j iff x[i] == x[j]
    class compare_eq {
    public:
        explicit compare_eq(const vec_t& x_) : m_x(x_) {}
        bool operator()(const uMatSz_t i, const uMatSz_t j) const {
            uAssertBounds(i, 0, m_x.n_elem);
            uAssertBounds(j, 0, m_x.n_elem);
            return m_x.at(i) == m_x.at(j);
        }

    private:
        const vec_t& m_x;
    };

    /**
     * Sorts by x-coordinate. Replaces all tied design points with corresponding
     *  weighted mean y-value. Note, points (x1,y1) and (x2, y2) are considered
     *  'tied' if x1 == x2. Also removes any design points with zero weight.
     * @param x_filt - sorted x-coordinates, all elements unique
     * @param y_filt - y-coordinates corresponding to 'x_filt', all elements at
     *  duplicate x-values are replaced by weighted mean y-value
     * @param w_filt - weights corresponding to 'x_filt'
     * @param x - unsorted x-coordinates which may contain duplicates
     * @param y - y-coordinates corresponding to 'x'
     * @param w - weights corresponding to 'x'
     * @return true if knots remain after filtering, false o/w
     */
    static bool filter_knots(vec_t& x_filt,
                             vec_t& y_filt,
                             vec_t& w_filt,
                             const vec_t& x,
                             const vec_t& y,
                             const vec_t& w) {
        // Assume x, y, w are parallel arrays of same length
        const uMatSz_t N_ELEM = x.n_elem;
        uAssertPosEq(N_ELEM, y.n_elem);
        uAssertPosEq(N_ELEM, w.n_elem);
        uMatSzVecCol order;
        // Determine permutation order such that x[i] < x[j]
        get_sort_order(order, x);
        uAssertPosEq(N_ELEM, order.n_elem);

        // Allocate filtered arrays for storing unique design points
        x_filt.set_size(N_ELEM);
        y_filt.set_size(N_ELEM);
        w_filt.set_size(N_ELEM);

        uMatSzVecCol::const_iterator it_cpy = order.cbegin();
        uMatSzVecCol::const_iterator it_dup;
        uMatSz_t i_src;
        uMatSz_t i_dst = 0;
        const real_t ZERO_R = static_cast<real_t>(0.0);

        do {
            // Get start of tied segment
            it_dup = std::adjacent_find(it_cpy, order.cend(), compare_eq(x));

            // Copy prior unique segment
            while (it_cpy != it_dup) {
                i_src = *it_cpy;
                uAssert(check_bounds_3v(i_src, x, y, w));
                uAssert(check_bounds_3v(i_dst, x_filt, y_filt, w_filt));
                x_filt.at(i_dst) = x.at(i_src);
                y_filt.at(i_dst) = y.at(i_src);
                w_filt.at(i_dst) = w.at(i_src);
                ++i_dst;
                ++it_cpy;
            }

            // Replace tied design points with weighted average
            if (it_dup != order.cend()) {
                i_src = *it_cpy;
                uAssert(check_bounds_3v(i_src, x, y, w));
                const real_t x_dup = x.at(i_src);
                real_t y_avg = ZERO_R;
                real_t w_sum = ZERO_R;
                real_t x_cpy;
                do {
                    i_src = *it_cpy;
                    uAssert(check_bounds_3v(i_src, x, y, w));
                    x_cpy = x.at(i_src);
                    if (x_cpy != x_dup) {
                        break;
                    }
                    y_avg += w.at(i_src) * y.at(i_src);
                    w_sum += w.at(i_src);
                    ++it_cpy;
                } while (it_cpy != order.cend());
                // Filter points with zero weight
                if (w_sum != ZERO_R) {
                    y_avg /= w_sum;
                    uAssert(check_bounds_3v(i_dst, x_filt, y_filt, w_filt));
                    x_filt.at(i_dst) = x_dup;
                    y_filt.at(i_dst) = y_avg;
                    w_filt.at(i_dst) = w_sum;
                    ++i_dst;
                }
            }

            it_dup = it_cpy;
        } while (it_dup != order.cend());

        // Remove knots with zero weight
        uAssert(check_bounds_inc_3v(i_dst, x_filt, y_filt, w_filt));
        const uMatSz_t N_ELEM_PRE_WEIGHT_FILTER = i_dst;
        i_dst = 0;
        for (i_src = 0; i_src < N_ELEM_PRE_WEIGHT_FILTER; ++i_src) {
            if (w_filt.at(i_src) != ZERO_R) {
                x_filt.at(i_dst) = x_filt.at(i_src);
                y_filt.at(i_dst) = y_filt.at(i_src);
                w_filt.at(i_dst) = w_filt.at(i_src);
                ++i_dst;
            }
        }

        // Resize arrays
        uAssert(check_bounds_inc_3v(i_dst, x_filt, y_filt, w_filt));
        x_filt.resize(i_dst);
        uAssert(uMatrixUtils::all(x_filt == uMatrixUtils::unique(x_filt)));
        uAssert(x_filt.is_sorted());
        uAssert(x_filt.is_finite());
        y_filt.resize(i_dst);
        uAssert(y_filt.is_finite());
        w_filt.resize(i_dst);
        uAssert(w_filt.is_finite());
        return x_filt.n_elem > 0;
    }

    /**
     * @return average spacing between knots
     */
    static real_t get_mean_spacing(const vec_t& x) {
        // Assuming x has been filtered for duplicates and is sorted
        uAssert(uMatrixUtils::all(x == uMatrixUtils::unique(x)));
        uAssert(x.is_sorted());
        real_t h = static_cast<real_t>(0.0);
        const uMatSz_t N_ELEM = x.n_elem;
        uAssert(N_ELEM >= 2);
        for (uMatSz_t i = 1; i < N_ELEM; ++i) {
            h += x.at(i) - x.at(i - 1);
        }
        h /= (N_ELEM > 1) ? static_cast<real_t>(N_ELEM - 1)
                          : static_cast<real_t>(1.0);
        return h;
    }

    ////////////////////////////////////////////////////////////////
    // Alpha

    /**
     * Converts alpha in [0, 1] to roughness penalty in [0, +inf) for use by
     * driver.
     */
    static real_t alpha_transform(const real_t alpha) {
        const real_t ONE_R = static_cast<real_t>(1.0);
        uAssertBoundsInc(alpha, static_cast<real_t>(0.0), ONE_R);
        // Add small epsilon to avoid divide-by-zero
        return alpha / (ONE_R - alpha + std::numeric_limits<real_t>::epsilon());
    }

    /**
     * Converts alpha in [0, +inf) to [0, 1]
     */
    static real_t alpha_inv_trans(const real_t alpha) {
        uAssert(alpha >= static_cast<real_t>(0.0));
        uAssert(uIsFinite(alpha));
        const real_t ONE_R = static_cast<real_t>(1.0);
        const real_t result =
            (alpha * (ONE_R + std::numeric_limits<real_t>::epsilon())) /
            (alpha + ONE_R);
        uAssertBoundsInc(result, static_cast<real_t>(0.0), ONE_R);
        return result;
    }

    ////////////////////////////////////////////////////////////////
    // Conditional checking

    /**
     * Asserts index i is in [0, min(a.n_elem, b.n_elem, c.n_elem))
     */
    static bool check_bounds_3v(const uMatSz_t i,
                                const vec_t& a,
                                const vec_t& b,
                                const vec_t& c) {
        uAssertBounds(i, 0, a.n_elem);
        uAssertBounds(i, 0, b.n_elem);
        uAssertBounds(i, 0, c.n_elem);
        return true;
    }

    /**
     * Asserts index i is in [0, min(vx.n_elem, vy.n_elem, vw.n_elem)]
     */
    static bool check_bounds_inc_3v(const uMatSz_t i,
                                    const vec_t& a,
                                    const vec_t& b,
                                    const vec_t& c) {
        uAssertBoundsInc(i, 0, a.n_elem);
        uAssertBoundsInc(i, 0, b.n_elem);
        uAssertBoundsInc(i, 0, c.n_elem);
        return true;
    }

    /**
     * @return true if user arguments are valid, false o/w
     */
    static bool args_valid(const vec_t& x, const vec_t& y, const vec_t& w) {
        bool valid = x.n_elem == y.n_elem;
        uAssert(valid);
        valid &= x.n_elem == w.n_elem;
        uAssert(valid);
        valid &= (x.n_elem > 0);
        uAssert(valid);
        return valid;
    }

    /**
     * @return true if additional auto-smooth configuration is also valid
     */
    static bool args_valid(const vec_t& x,
                           const vec_t& y,
                           const vec_t& w,
                           const unsigned int max_iters,
                           const real_t min_alpha,
                           const real_t max_alpha) {
        bool valid = args_valid(x, y, w);
        uAssert(valid);
        valid &= (max_iters > 0);
        uAssert(valid);
        valid &= min_alpha < max_alpha;
        uAssert(valid);
        valid &= min_alpha >= static_cast<real_t>(0.0);
        uAssert(valid);
        valid &= max_alpha <= static_cast<real_t>(1.0);
        uAssert(valid);
        return valid;
    }

    ////////////////////////////////////////////////////////////////
    // Spline evaluators

    /**
     * Utility for evaluating a linear spline (# of knots < 3) at a
     * set of input x-coordinate abscissae defined by range [beg, end)
     * @return true if spline is linear, false otherwise
     */
    template <typename FwdWriteIt, typename FwdReadIt>
    static bool eval_linear(const spline_t& s,
                            FwdWriteIt out,
                            FwdReadIt beg,
                            const FwdReadIt end) {
        const unsigned int NUM_KNOTS = s.size();
        if (NUM_KNOTS < 3) {
            // Case: 0 knots, f(x) = 0.0
            if (NUM_KNOTS == 0) {
                // Output is constant 0
                while (beg != end) {
                    *out = static_cast<real_t>(0.0);
                    ++out;
                    ++beg;
                }
                return true;
            }

            // Case: 1 knot, f(x) = constant
            if (NUM_KNOTS == 1) {
                // Output is simply a constant at first knot
                while (beg != end) {
                    *out = s.m_y.at(0);
                    ++out;
                    ++beg;
                }
                return true;
            }

            // Case: 2 knots, f(x) = m*x + b
            if (NUM_KNOTS == 2) {
                // Output is simply a straight line
                const real_t x0 = s.m_x.at(0);
                const real_t x1 = s.m_x.at(1);
                uAssert(x0 < x1);
                const real_t y0 = s.m_y.at(0);
                const real_t y1 = s.m_y.at(1);
                const real_t slope = (y1 - y0) / (x1 - x0);
                const real_t ycept = y1 - (slope * x1);
                while (beg != end) {
                    const real_t x = *beg;
                    *out = (slope * x) + ycept;
                    ++out;
                    ++beg;
                }
                return true;
            }
        }
        // Spline is not linear
        return false;
    }

    /**
     * Utility for evaluating a true cubic spline (# of knots >= 3) at a
     * single x-coordinate abscissa defined by x and knot region defined by it.
     * @return scalar value of cubic spline at x
     */
    static real_t eval_cubic(const spline_t& s,
                             const real_t x,
                             const real_t* it) {
        // Spline evaluated according to Green, section 2.4.2
        // Right-shifted view for vector of second derivatives
        // (gamma vector in Green & Silverman text, section 2.1.2)
        const vec_const_rshift_view<1> ddview(s.m_dd);

        // Check edge case x <= x0 knot
        if (it == s.m_x.cbegin()) {
            // Linear case
            const real_t knot = *it;
            uAssert(x <= knot);
            const real_t x0 = s.m_x.at(0);
            const real_t x1 = s.m_x.at(1);
            const real_t y0 = s.m_y.at(0);
            const real_t y1 = s.m_y.at(1);
            const real_t dd1 = ddview.at(1);

            // First derivative at knot 0
            const real_t d0 =
                ((y1 - y0) / (x1 - x0)) -
                ((static_cast<real_t>(1.0) / static_cast<real_t>(6.0)) *
                 (x1 - x0) * dd1);
            // Compute extrapolated value
            const real_t yx = y0 - ((x0 - x) * d0);
            return yx;
        }

        // Check edge case x > xN knot
        if (it == s.m_x.cend()) {
            // Linear case
            const real_t knot = *it;
            uAssert(x > knot);
            const uMatrixUtils::uword n = s.m_x.n_elem - 1;
            uAssertBounds(n, 1, s.m_x.n_elem);
            const real_t xn_1 = s.m_x.at(n - 1);
            const real_t xn = s.m_x.at(n);
            const real_t yn_1 = s.m_y.at(n - 1);
            const real_t yn = s.m_y.at(n);
            const real_t ddn_1 = ddview.at(n - 1);

            // First derivative at knot N
            const real_t dN =
                ((yn - yn_1) / (xn - xn_1)) +
                ((static_cast<real_t>(1.0) / static_cast<real_t>(6.0)) *
                 (xn - xn_1) * ddn_1);
            // Compute extrapolated value
            const real_t yx = yn + ((x - xn) * dN);
            return yx;
        }

        // Cubic case
        // Set to greatest lower bound (infimum) knot
        --it;
        const real_t knot = *it;
        uAssert(x > knot);
        const uMatrixUtils::uword i =
            static_cast<uMatrixUtils::uword>(it - s.m_x.begin());
        uAssertBounds(i, 0, s.m_x.n_elem - 1);
        const real_t xi = s.m_x.at(i);
        const real_t xi1 = s.m_x.at(i + 1);
        const real_t yi = s.m_y.at(i);
        const real_t yi1 = s.m_y.at(i + 1);
        const real_t ddi = (i != 0) ? ddview.at(i) : static_cast<real_t>(0.0);
        const real_t ddi1 = ((i + 1) != (s.m_x.n_elem - 1))
                                ? ddview.at(i + 1)
                                : static_cast<real_t>(0.0);

        uAssert((xi1 - xi) > static_cast<real_t>(0.0));
        const real_t inv_x_delta = static_cast<real_t>(1.0) / (xi1 - xi);
        // Compute interpolated value
        const real_t yx =
            ((((x - xi) * yi1) + ((xi1 - x) * yi)) * inv_x_delta) -
            ((static_cast<real_t>(1.0) / static_cast<real_t>(6.0)) * (x - xi) *
             (xi1 - x) *
             (((static_cast<real_t>(1.0) + ((x - xi) * inv_x_delta)) * ddi1) +
              ((static_cast<real_t>(1.0) + ((xi1 - x) * inv_x_delta)) * ddi)));
        return yx;
    }
};  // end of uCspplinePutils

//****************************************************************************
// uCspplineSparseDriver
//****************************************************************************

/**
 * Sparse-matrix representation for O(n) memory and computation
 */
template <typename t_real>
class uCspplineSparseDriver {
public:
    // Real-valued scalar type
    typedef t_real real_t;

    // Real-valued vector type
    typedef uMatrixUtils::Col<real_t> vec_t;

    /**
     * Initialize driver for manual or auto-smoothing
     */
    bool prime(const vec_t& x_filt,
               const vec_t& y_filt,
               const vec_t& w_filt,
               const uCspplineAutoSmooth e_smooth = uCspplineAutoSmoothCv);

    /**
     * Hook for clearing transient state used during fitting
     */
    void post();

    /**
     * Hook for clearing any persistent state
     */
    void clear();

    /**
     * Build spline with user-specified roughness penalty alpha
     */
    bool fit(vec_t& y,
             vec_t& dd,
             const vec_t& x_filt,
             const vec_t& y_filt,
             const vec_t& w_filt,
             const real_t alpha);

    /**
     * Build spline and capture diagonals of linear smooth operator
     */
    bool fit(vec_t& y,
             vec_t& dd,
             vec_t& a_diag,
             const vec_t& x_filt,
             const vec_t& y_filt,
             const vec_t& w_filt,
             const real_t alpha);

private:
    ////////////////////////////////////////////////////////////////
    // Banded matrix

    /**
     * Banded matrix compatible with LAPACK formats:
     *  PB : symmetric positive definite band matrix
     *  SB : (real) symmetric band matrix
     *
     * Representation could also represent lower triangle (optional diagonal)
     * of a general banded matrix; however, this format itself is not
     * compatible with LAPACK GB (general band matrix) functions.
     *
     * For LAPACK band storage and matrix formats, see:
     *  http://www.netlib.org/lapack/lug/node124.html
     *  http://www.netlib.org/lapack/lug/node24.html
     *
     * @WARNING: ASSUMES ONLY LOWER TRIANGLE BANDS ARE ACCESSED!
     *
     * For band matrix A with number of rows = number of columns = 5 and
     * number of bands below the diagonal (i.e. sub-diagonals) = 2:
     *
     *    a00  a01  a02  a03  a04
     *    a10  a11  a12  a13  a14
     *    a20  a21  a22  a23  a24
     *    a30  a31  a32  a33  a34
     *    a40  a41  a42  a43  a44
     *
     * The lower triangle with diagonal containing the bands will be stored in
     * column-major format according to:
     *
     *    a00  a11  a22  a33  a44
     *    a10  a21  a32  a43   *
     *    a20  a31  a42   *    *
     *
     * Basically, each row represents a band with '*' padding for elements
     * outside matrix range (it is an error to access these elements).
     *
     * Template argument no_diag == 1 if matrix is strictly lower triangle
     * without main diagonal, no_diag == 0 if main diagonal is present.
     */
    template <uMatSz_t no_diag = 0>
    class mat_band_ltri {
    public:
        /**
         * Default constructor
         */
        mat_band_ltri() : n_rows(0), n_cols(0), n_subdiags(0) {
            U_STATIC_ASSERT((no_diag == 0) || (no_diag == 1));
        }

        /**
         * @param n_rows_ - total number of rows of full, dense matrix
         * @param n_cols_ - total number of columns of full, dense matrix
         * @param n_subdiags_ - requested number of sub-diagonals
         */
        void set_size(const uMatSz_t n_rows_,
                      const uMatSz_t n_cols_,
                      const uMatSz_t max_n_subdiags_) {
            uAssert(n_rows_ > 0);
            uAssert(n_cols_ > 0);
            const_cast<uMatSz_t&>(this->n_rows) = n_rows_;
            const_cast<uMatSz_t&>(this->n_cols) = n_cols_;
            this->mat.set_size(std::min(n_rows_, 1 - no_diag + max_n_subdiags_),
                               std::min(n_rows_, n_cols_));
            uAssertBoundsInc(this->mat.n_rows, 1, n_rows_);
            uAssertBoundsInc(this->mat.n_cols, 1, n_cols_);
            const_cast<uMatSz_t&>(this->n_subdiags) =
                this->mat.n_rows - 1 + no_diag;
        }

        /**
         * Reset internal buffers
         */
        void clear() {
            const_cast<uMatSz_t&>(this->n_rows) = 0;
            const_cast<uMatSz_t&>(this->n_cols) = 0;
            const_cast<uMatSz_t&>(this->n_subdiags) = 0;
            this->mat.clear();
        }

        /**
         * Access is assumed to be restricted to only band elements within the
         * lower triangle:
         *  i <= # of sub-diagonals, j < min(i+1, # of cols - (i - j))
         * @param i - row index
         * @param j - column index
         * @return band element at i,j
         */
        inline real_t& at(const uMatSz_t i, const uMatSz_t j) {
            uAssert(this->check_bounds(i, j));
            return this->mat.at(i - j - no_diag, j);
        }

        /**
         * Access is assumed to be restricted to only band elements within the
         * lower triangle:
         *  i <= # of sub-diagonals, j < min(i+1, # of cols - (i - j))
         * @param i - row index
         * @param j - column index
         * @return band element at i,j
         */
        inline real_t at(const uMatSz_t i, const uMatSz_t j) const {
            uAssert(this->check_bounds(i, j));
            return this->mat.at(i - j - no_diag, j);
        }

        // Number of rows - exposed to be consistent with right-shift views
        const uMatSz_t n_rows;
        // Number of columns - exposed to be consistent with right-shift views
        const uMatSz_t n_cols;
        // Number of sub-diagonals
        const uMatSz_t n_subdiags;

    private:
        /**
         * @return true if (i,j) is accessing a band element of the lower
         * triangle
         */
        bool check_bounds(const uMatSz_t i, const uMatSz_t j) const {
            // Only lower triangle may be accessed
            uAssert((j + no_diag) <= i);
            // # of rows is # of sub-diagonals + 1 - no_diag
            uAssertBoundsInc((i - j), no_diag, this->n_subdiags);
            // Bounds check internal matrix access
            uAssertBounds((i - j - no_diag), 0, this->mat.n_rows);
            // Also check '*' elements are not accessed
            uAssertBounds(
                j, 0, std::min(n_rows - (i - j - no_diag), this->mat.n_cols));
            return true;
        }

        // Implement internally using dense matrix type
        typedef uMatrixUtils::Mat<real_t> mat_t;
        mat_t mat;
    };

    enum { e_has_diag = 0, e_no_diag = 1 };

    // See Green & Silverman, section 3.5.2
    mat_band_ltri<e_has_diag> m_Q;
    mat_band_ltri<e_has_diag> m_R;
    // m_QtY = transpose(Q) * Y
    vec_t m_QtY;
    // m_WinvQ = inverse(W) * Q
    mat_band_ltri<e_has_diag> m_WinvQ;
    // m_QtWinvQ = transpose(Q) * inverse(W) * Q
    mat_band_ltri<e_has_diag> m_QtWinvQ;
    // m_B = R + alpha * trans(Q) * inverse(W) * Q
    mat_band_ltri<e_has_diag> m_B;
    // m_L is from LDLt Cholesky decomposition of B
    mat_band_ltri<e_no_diag> m_L;
    // m_D is from LDLt Cholesky decomposition of transpose(Q)*Y
    vec_t m_D;
};

/**
 * Initialize driver for auto-smoothing
 */
template <typename t_real>
bool uCspplineSparseDriver<t_real>::prime(
    const typename uCspplineSparseDriver<t_real>::vec_t& x_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& y_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& w_filt,
    const enum uCspplineAutoSmooth e_smooth) {
    const uMatSz_t N_ELEM = x_filt.n_elem;
    uAssert(N_ELEM > 2);
    uAssertPosEq(N_ELEM, y_filt.n_elem);
    uAssertPosEq(N_ELEM, w_filt.n_elem);

    // See Green & Silverman, section 2.1.2

    // Initialize vector of second differences
    const real_t ZERO_R = static_cast<real_t>(0.0);
    vec_t h(N_ELEM - 1);
    for (uMatSz_t i = 0; i < (N_ELEM - 1); ++i) {
        uAssertBounds(i, 0, h.n_elem);
        uAssertBounds(i + 1, 0, x_filt.n_elem);
        h.at(i) = x_filt.at(i + 1) - x_filt.at(i);
        uAssert(h.at(i) > ZERO_R);
    }

    // Initialize Q band matrix
    m_Q.set_size(N_ELEM, N_ELEM - 2, 2 /*n_subdiags*/);
    uAssert(m_Q.n_subdiags == 2);
    typedef uCspplinePutils<real_t, uCspplineSparseDriver<real_t> > putils_t;
    typedef typename putils_t::template mat_rshift_view<0, 1, mat_band_ltri<0> >
        Q_shift_t;
    Q_shift_t Q_shift(m_Q);
    const real_t ONE_R = static_cast<real_t>(1.0);
    uMatSz_t i, j;
    for (j = 1; j < (N_ELEM - 1); ++j) {
        uAssertBounds(j, 1, h.n_elem);
        uAssertBounds(j + 1, 2, m_Q.n_rows);
        uAssertBoundsInc(j, 1, m_Q.n_cols);
        const real_t h_inv_j_1 = ONE_R / h.at(j - 1);
        const real_t h_inv_j = ONE_R / h.at(j);
        Q_shift.at(j - 1, j) = h_inv_j_1;
        Q_shift.at(j, j) = -h_inv_j_1 - h_inv_j;
        Q_shift.at(j + 1, j) = h_inv_j;
    }

    // Initialize QtY - use Green & Silverman, equation 2.7
    m_QtY.set_size(N_ELEM - 2);
    typedef typename putils_t::template vec_rshift_view<1> QtY_shift_t;
    QtY_shift_t QtY_shift(m_QtY);
    for (i = 1; i < (N_ELEM - 1); ++i) {
        uAssertBounds(i + 1, 2, y_filt.n_elem);
        uAssertBoundsInc(i, 1, m_Q.n_cols);
        uAssertBounds(i + 1, 2, m_Q.n_rows);
        const real_t h_inv_i_1 = Q_shift.at(i - 1, i);
        const real_t h_inv_i = Q_shift.at(i + 1, i);
        const real_t y_i_1 = y_filt.at(i - 1);
        const real_t y_i = y_filt.at(i);
        const real_t y_i1 = y_filt.at(i + 1);
        QtY_shift.at(i) =
            ((y_i1 - y_i) * h_inv_i) - ((y_i - y_i_1) * h_inv_i_1);
    }

    // Initialize inverse(W) * Q
    m_WinvQ.set_size(N_ELEM, N_ELEM - 2, 2 /*n_subdiags*/);
    uAssert(m_WinvQ.n_subdiags == 2);
    Q_shift_t WinvQ_shift(m_WinvQ);
    i = 0;
    for (j = 1; j < (N_ELEM - 1); ++j) {
        uAssertBounds(i + 2, 2, w_filt.n_elem);
        uAssertBounds(i + 2, 2, m_Q.n_rows);
        uAssertBounds(i + 2, 2, m_WinvQ.n_rows);
        uAssertBoundsInc(j, 1, m_Q.n_cols);
        uAssertBoundsInc(j, 1, m_WinvQ.n_cols);
        uAssert(w_filt.at(i) != ZERO_R);
        uAssert(w_filt.at(i + 1) != ZERO_R);
        uAssert(w_filt.at(i + 2) != ZERO_R);
        WinvQ_shift.at(i, j) = Q_shift.at(i, j) / w_filt.at(i);
        WinvQ_shift.at(i + 1, j) = Q_shift.at(i + 1, j) / w_filt.at(i + 1);
        WinvQ_shift.at(i + 2, j) = Q_shift.at(i + 2, j) / w_filt.at(i + 2);
        ++i;
    }

    // Initialize transpose(Q) * inverse(W) * Q
    // Matrix is symmetric, only compute banded elements along lower triangle
    m_QtWinvQ.set_size(N_ELEM - 2, N_ELEM - 2, 2 /*n_subdiags*/);
    j = 0;
    if (N_ELEM > 4) {
        // Compute 3 output elements: 1 diagonal + 2 sub-diagonals
        uAssert(m_QtWinvQ.n_cols >= 3);
        for (; j < N_ELEM - 4; ++j) {
            uAssertBounds(j + 2, 2, m_WinvQ.n_rows);
            uAssertBounds(j, 0, m_WinvQ.n_cols);
            uAssertBounds(j + 2, 2, m_Q.n_rows);
            uAssertBounds(j + 2, 2, m_Q.n_cols);
            uAssertBounds(j + 2, 2, m_QtWinvQ.n_rows);
            uAssertBounds(j, 0, m_QtWinvQ.n_cols);
            // Elements from right matrix: inverse(W) * Q
            const real_t rjj = m_WinvQ.at(j, j);
            const real_t rj1j = m_WinvQ.at(j + 1, j);
            const real_t rj2j = m_WinvQ.at(j + 2, j);
            // Elements from left matrix: transpose(Q)
            // Note, left-hand side variable names indicate the subscript
            // access within the transposed matrix; however am flipping (row,
            // col) subscripts on right-hand side as transpose(Q) is not
            // actually available, only Q is available.
            const real_t ljj = m_Q.at(j, j);
            const real_t ljj1 = m_Q.at(j + 1, j);
            const real_t ljj2 = m_Q.at(j + 2, j);
            const real_t lj1j1 = m_Q.at(j + 1, j + 1);
            const real_t lj1j2 = m_Q.at(j + 2, j + 1);
            const real_t lj2j2 = m_Q.at(j + 2, j + 2);
            // Output column results
            const real_t ojj = (rjj * ljj) + (rj1j * ljj1) + (rj2j * ljj2);
            const real_t oj1j = (rj1j * lj1j1) + (rj2j * lj1j2);
            const real_t oj2j = (rj2j * lj2j2);
            m_QtWinvQ.at(j, j) = ojj;
            m_QtWinvQ.at(j + 1, j) = oj1j;
            m_QtWinvQ.at(j + 2, j) = oj2j;
        }
    }
    if (N_ELEM > 3) {
        // Second to last column has 2 outputs: 1 diagonal + 1 sub-diagonal
        uAssert(m_QtWinvQ.n_cols >= 2);
        uAssert(j == (N_ELEM - 4));
        uAssert((j + 1) < m_QtWinvQ.n_cols);
        uAssert((j + 2) == m_QtWinvQ.n_rows);
        uAssertBounds(j + 2, 2, m_WinvQ.n_rows);
        uAssertBounds(j, 0, m_WinvQ.n_cols);
        uAssertBounds(j + 2, 2, m_Q.n_rows);
        uAssertBounds(j + 1, 1, m_Q.n_cols);
        uAssertBounds(j + 1, 1, m_QtWinvQ.n_rows);
        uAssertBounds(j, 0, m_QtWinvQ.n_cols);
        // Elements from right matrix: inverse(W) * Q
        const real_t rjj = m_WinvQ.at(j, j);
        const real_t rj1j = m_WinvQ.at(j + 1, j);
        const real_t rj2j = m_WinvQ.at(j + 2, j);
        // Elements from left matrix: transpose(Q)
        const real_t ljj = m_Q.at(j, j);
        const real_t ljj1 = m_Q.at(j + 1, j);
        const real_t ljj2 = m_Q.at(j + 2, j);
        const real_t lj1j1 = m_Q.at(j + 1, j + 1);
        const real_t lj1j2 = m_Q.at(j + 2, j + 1);
        // Output column results
        const real_t ojj = (rjj * ljj) + (rj1j * ljj1) + (rj2j * ljj2);
        const real_t oj1j = (rj1j * lj1j1) + (rj2j * lj1j2);
        m_QtWinvQ.at(j, j) = ojj;
        m_QtWinvQ.at(j + 1, j) = oj1j;
        ++j;
    }
    {
        // Last column has 1 output: 1 diagonal
        uAssert((j + 1) == m_QtWinvQ.n_cols);
        uAssertBounds(j + 2, 2, m_WinvQ.n_rows);
        uAssertBounds(j, 0, m_WinvQ.n_cols);
        uAssertBounds(j + 2, 2, m_Q.n_rows);
        uAssertBounds(j, 0, m_Q.n_cols);
        uAssertBounds(j, 0, m_QtWinvQ.n_rows);
        uAssertBounds(j, 0, m_QtWinvQ.n_cols);
        // Elements from right matrix: inverse(W) * Q
        const real_t rjj = m_WinvQ.at(j, j);
        const real_t rj1j = m_WinvQ.at(j + 1, j);
        const real_t rj2j = m_WinvQ.at(j + 2, j);
        // Elements from left matrix: transpose(Q)
        const real_t ljj = m_Q.at(j, j);
        const real_t ljj1 = m_Q.at(j + 1, j);
        const real_t ljj2 = m_Q.at(j + 2, j);
        // Output column results
        const real_t ojj = (rjj * ljj) + (rj1j * ljj1) + (rj2j * ljj2);
        m_QtWinvQ.at(j, j) = ojj;
    }
    uAssert((j + 1) == m_QtWinvQ.n_cols);

    // Initialize symmetric R band matrix (only fill diagonal + lower band)
    m_R.set_size(N_ELEM - 2, N_ELEM - 2, 1 /*n_subdiag*/);
    typedef typename putils_t::template mat_rshift_view<1, 1, mat_band_ltri<0> >
        R_shift_t;
    R_shift_t R_shift(m_R);
    const real_t ONE_THIRD_R = static_cast<real_t>(1.0 / 3.0);
    const real_t ONE_SIXTH_R = static_cast<real_t>(1.0 / 6.0);
    for (uMatSz_t i = 1; i < (N_ELEM - 2); ++i) {
        uAssertBounds(i, 1, h.n_elem);
        const real_t h_i = h.at(i);
        const real_t h_i_1 = h.at(i - 1);
        R_shift.at(i, i) = ONE_THIRD_R * (h_i + h_i_1);
        R_shift.at(i + 1, i) = ONE_SIXTH_R * h_i;
    }
    R_shift.at(N_ELEM - 2, N_ELEM - 2) =
        ONE_THIRD_R * (h.at(N_ELEM - 3) + h.at(N_ELEM - 2));

    // Allocate m_B - ultimately will store:
    //  R + alpha * transpose(Q) * inverse(W) * Q
    m_B.set_size(N_ELEM - 2, N_ELEM - 2, 2 /*n_subdiag*/);
    // Allocate m_L - note, according to Section 2.3.3 of Green & Silverman,
    // the diagonal of L is always 1. However, it never appears to be accessed
    // when computing the O(n) Cholesky decomposition of section 2.6.1 or
    // solving the corresponding linear system of section 2.6.2, so leaving
    // uninitialized for now.
    m_L.set_size(N_ELEM - 2, N_ELEM - 2, 2 /*n_subdiag*/);
    // Allocate m_D
    m_D.set_size(N_ELEM - 2);

    // Prime successful
    return true;
}

/**
 * Hook for clearing transient state used during fitting
 */
template <typename t_real>
void uCspplineSparseDriver<t_real>::post() {
    this->clear();
}

/**
 * Hook for clearing any persistent state
 */
template <typename t_real>
void uCspplineSparseDriver<t_real>::clear() {
    m_Q.clear();
    m_R.clear();
    m_QtY.clear();
    m_WinvQ.clear();
    m_QtWinvQ.clear();
    m_B.clear();
    m_L.clear();
    m_D.clear();
}

/**
 * Build spline with user-specified roughness penalty alpha
 */
template <typename t_real>
bool uCspplineSparseDriver<t_real>::fit(
    typename uCspplineSparseDriver<t_real>::vec_t& y,
    typename uCspplineSparseDriver<t_real>::vec_t& dd,
    const typename uCspplineSparseDriver<t_real>::vec_t& x_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& y_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& w_filt,
    const t_real alpha) {
    // Assumes prime() has been called!
    const uMatSz_t N_ELEM = x_filt.n_elem;
    uAssert(N_ELEM > 2);
    uAssertPosEq(N_ELEM, y_filt.n_elem);
    uAssertPosEq(N_ELEM, w_filt.n_elem);

    // Based on Reinsch algorithm for weighted smoothing as related in Green &
    // Silverman text, section 3.5.2

    //////////////////////////////////////////////////////////////////////////
    // Step 1 - Evaluatation of vector transpose(Q) * Y has been assumed
    // completed in prime() call and exists within m_QtY member variable
    uAssert((m_QtY.n_elem + 2) == N_ELEM);

    //////////////////////////////////////////////////////////////////////////
    // Step 2 - Find non-zero diagonals of:
    //  B = R + alpha * transpose(Q) * inverse(W) * Q
    // and its Cholesky decompositions factors L and D

    // Compute B = R + alpha * transpose(Q) * inverse(W) * Q
    // Assumptions from prime() call:
    // - m_QtWinvQ contains value: transpose(Q) * inverse(W) * Q
    // - m_B = B has been allocated
    // - m_R = R is assumed to have been computed in prime().
    uAssert(m_B.n_cols == (N_ELEM - 2));
    uAssertPosEq(m_B.n_rows, m_R.n_rows);
    uAssertPosEq(m_B.n_cols, m_R.n_cols);
    uAssertPosEq(m_B.n_rows, m_QtWinvQ.n_rows);
    uAssertPosEq(m_B.n_cols, m_QtWinvQ.n_cols);
    uMatSz_t j = 0;
    if (N_ELEM > 4) {
        uAssert(m_QtWinvQ.n_cols >= 3);
        for (; j < N_ELEM - 4; ++j) {
            uAssertBounds(j + 2, 2, m_QtWinvQ.n_rows);
            uAssertBounds(j + 1, 1, m_R.n_rows);
            uAssertBounds(j + 2, 2, m_B.n_rows);
            uAssertBounds(j, 0, m_QtWinvQ.n_cols);
            uAssertBounds(j, 0, m_R.n_cols);
            uAssertBounds(j, 0, m_B.n_cols);
            // R matrix inputs - only has 1 sub-diagonal
            const real_t Rjj = m_R.at(j, j);
            const real_t Rj1j = m_R.at(j + 1, j);
            // QtWinvQ matrix inputs - has 2 sub-diagonals
            const real_t QtWinvQjj = m_QtWinvQ.at(j, j);
            const real_t QtWinvQj1j = m_QtWinvQ.at(j + 1, j);
            const real_t QtWinvQj2j = m_QtWinvQ.at(j + 2, j);
            // B outputs - note R(j + 2, j) == 0
            const real_t Bjj = Rjj + (alpha * QtWinvQjj);
            const real_t Bj1j = Rj1j + (alpha * QtWinvQj1j);
            const real_t Bj2j = alpha * QtWinvQj2j;
            m_B.at(j, j) = Bjj;
            m_B.at(j + 1, j) = Bj1j;
            m_B.at(j + 2, j) = Bj2j;
        }
    }
    if (N_ELEM > 3) {
        // Second to last column has 1 less sub-diagonal along m_QtWinvQ
        uAssert(m_QtWinvQ.n_cols >= 2);
        uAssert(j == (N_ELEM - 4));
        uAssert((j + 1) < m_QtWinvQ.n_cols);
        uAssert((j + 2) == m_QtWinvQ.n_rows);
        uAssertBounds(j + 1, 1, m_QtWinvQ.n_rows);
        uAssertBounds(j + 1, 1, m_R.n_rows);
        uAssertBounds(j + 1, 1, m_B.n_rows);
        uAssertBounds(j, 0, m_QtWinvQ.n_cols);
        uAssertBounds(j, 0, m_R.n_cols);
        uAssertBounds(j, 0, m_B.n_cols);
        // R matrix inputs - only has 1 sub-diagonal
        const real_t Rjj = m_R.at(j, j);
        const real_t Rj1j = m_R.at(j + 1, j);
        // QtWinvQ matrix inputs - now only has *1* sub-diagonal
        const real_t QtWinvQjj = m_QtWinvQ.at(j, j);
        const real_t QtWinvQj1j = m_QtWinvQ.at(j + 1, j);
        // B outputs
        const real_t Bjj = Rjj + (alpha * QtWinvQjj);
        const real_t Bj1j = Rj1j + (alpha * QtWinvQj1j);
        m_B.at(j, j) = Bjj;
        m_B.at(j + 1, j) = Bj1j;
        ++j;
    }
    {
        // Last column has no sub-diagonals along R and QtWinvQ
        uAssertPosEq((j + 1), m_R.n_cols);
        uAssertPosEq((j + 1), m_QtWinvQ.n_cols);
        uAssertPosEq((j + 1), m_B.n_cols);
        // R matrix input - only has diagonal
        const real_t Rjj = m_R.at(j, j);
        // QtWinvQ matrix inputs - only has diagonal
        const real_t QtWinvQjj = m_QtWinvQ.at(j, j);
        // B output
        const real_t Bjj = Rjj + (alpha * QtWinvQjj);
        m_B.at(j, j) = Bjj;
    }
    uAssert((j + 1) == m_B.n_cols);

    // Compute LDLt Cholesky decomposition of B matrix, see Green & Silverman,
    // section 2.6.1. Assumptions from prime() call:
    //  - L matrix and D "matrix" (represented as vector) have been allocated
    // Note that diagonal of L matrix is all 1's; however these values are
    // never accessed and hence remain uninitialized.
    uAssertPosEq(m_L.n_cols, (N_ELEM - 2));
    uAssertPosEq(m_L.n_rows, (N_ELEM - 2));
    uAssertPosEq(m_D.n_elem, (N_ELEM - 2));
    // Compute first element of D
    m_D.at(0) = m_B.at(0, 0);
    if (N_ELEM > 3) {
        uAssert(m_L.n_cols >= 2);
        // Compute second set of elements
        {
            uAssertBounds(1, 1, m_D.n_elem);
            uAssertBounds(1, 1, m_B.n_rows);
            uAssertBounds(1, 1, m_B.n_cols);
            uAssertBounds(1, 1, m_L.n_rows);
            uAssertBounds(0, 0, m_L.n_cols);
            const real_t D0 = m_D.at(0);
            const real_t B10 = m_B.at(1, 0);
            const real_t B11 = m_B.at(1, 1);
            uAssert(D0 != static_cast<real_t>(0.0));
            const real_t L10 = B10 / D0;
            const real_t D1 = B11 - (L10 * L10 * D0);
            m_L.at(1, 0) = L10;
            m_D.at(1) = D1;
        }
        // Compute remaining elements
        for (j = 2; j < (N_ELEM - 2); ++j) {
            uAssertBounds(j, 2, m_L.n_rows);
            uAssertBounds(j - 1, 1, m_L.n_cols);
            uAssertBounds(j, 2, m_B.n_rows);
            uAssertBounds(j, 2, m_B.n_cols);
            uAssertBounds(j, 2, m_D.n_elem);
            // Load inputs
            const real_t Bjj_2 = m_B.at(j, j - 2);
            const real_t Bjj_1 = m_B.at(j, j - 1);
            const real_t Bjj = m_B.at(j, j);
            const real_t Dj_2 = m_D.at(j - 2);
            const real_t Dj_1 = m_D.at(j - 1);
            const real_t Lj_1j_2 = m_L.at(j - 1, j - 2);
            // Compute outputs
            uAssert(Dj_2 != static_cast<real_t>(0.0));
            const real_t Ljj_2 = Bjj_2 / Dj_2;
            uAssert(Dj_1 != static_cast<real_t>(0.0));
            const real_t Ljj_1 = (Bjj_1 - (Lj_1j_2 * Ljj_2 * Dj_2)) / Dj_1;
            const real_t Dj =
                Bjj - (Ljj_1 * Ljj_1 * Dj_1) - (Ljj_2 * Ljj_2 * Dj_2);
            // Store outputs
            m_L.at(j, j - 2) = Ljj_2;
            m_L.at(j, j - 1) = Ljj_1;
            m_D.at(j) = Dj;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Step 3 - Solve LDLt * dd = Qt * Y

    // See Green & Silverman, section 2.6.2 for solving linear equations
    // Assumptions from prime() are:
    //  - m_QtY = transpose(Q) * Y has been computed

    // Forward phase: compute "u" vector and store in output dd
    dd.set_size(N_ELEM - 2);
    uAssertPosEq(dd.n_elem, N_ELEM - 2);
    uAssertPosEq(dd.n_elem, m_QtY.n_elem);
    // Compute first element of "u"
    dd.at(0) = m_QtY.at(0);
    if (N_ELEM > 3) {
        // Compute second element of "u"
        {
            uAssertBounds(1, 1, dd.n_elem);
            uAssertBounds(1, 1, m_QtY.n_elem);
            uAssertBounds(1, 1, m_L.n_rows);
            uAssertBounds(0, 0, m_L.n_cols);
            const real_t z1 = m_QtY.at(1);
            const real_t L10 = m_L.at(1, 0);
            const real_t u0 = dd.at(0);
            const real_t u1 = z1 - (L10 * u0);
            dd.at(1) = u1;
        }
        // Compute remaining elements of "u"
        for (j = 2; j < (N_ELEM - 2); ++j) {
            uAssertBounds(j, 2, dd.n_elem);
            uAssertBounds(j, 2, m_QtY.n_elem);
            uAssertBounds(j, 2, m_L.n_rows);
            uAssertBounds(j - 1, 1, m_L.n_cols);
            // Load
            const real_t zj = m_QtY.at(j);
            const real_t Ljj_1 = m_L.at(j, j - 1);
            const real_t Ljj_2 = m_L.at(j, j - 2);
            const real_t uj_1 = dd.at(j - 1);
            const real_t uj_2 = dd.at(j - 2);
            // Compute
            const real_t uj = zj - (Ljj_1 * uj_1) - (Ljj_2 * uj_2);
            // Store
            dd.at(j) = uj;
        }

        // Forward phase: compute "v" vector and store in output dd
        // (arbitrarily doing in reverse order)
        for (j = (N_ELEM - 3); j >= 2; --j) {
            uAssertBounds(j, 2, m_D.n_elem);
            uAssertBounds(j, 2, dd.n_elem);
            uAssert(m_D.at(j) != static_cast<real_t>(0.0));
            dd.at(j) /= m_D.at(j);
        }
        // Compute second element of "v"
        {
            uAssert(m_D.at(1) != static_cast<real_t>(0.0));
            dd.at(1) /= m_D.at(1);
        }
    }
    // Compute first element of "v"
    uAssert(m_D.at(0) != static_cast<real_t>(0.0));
    dd.at(0) /= m_D.at(0);

    // Reverse phase: compute "x" vector and store in output dd
    // Note, x_n = v_n is already computed
    if (N_ELEM > 3) {
        // Compute 2nd to last element of "x" - which may also be first
        // element if there are only 4 elements total
        {
            uAssertBounds(N_ELEM - 3, 1, dd.n_elem);
            uAssertBounds(N_ELEM - 3, 1, m_L.n_rows);
            uAssertBounds(N_ELEM - 4, 0, m_L.n_cols);
            const real_t xn = dd.at(N_ELEM - 3);
            const real_t vn_1 = dd.at(N_ELEM - 4);
            const real_t Lnn_1 = m_L.at(N_ELEM - 3, N_ELEM - 4);
            const real_t xn_1 = vn_1 - (Lnn_1 * xn);
            dd.at(N_ELEM - 4) = xn_1;
        }
        if (N_ELEM > 4) {
            // Compute remaining elements of "x" in reverse order
            for (j = (N_ELEM - 5); j >= 1; --j) {
                uAssertBounds(j + 2, 3, dd.n_elem);
                uAssertBounds(j + 2, 3, m_L.n_rows);
                uAssertBounds(j, 1, m_L.n_cols);
                const real_t xj2 = dd.at(j + 2);
                const real_t xj1 = dd.at(j + 1);
                const real_t vj = dd.at(j);
                const real_t Lj1j = m_L.at(j + 1, j);
                const real_t Lj2j = m_L.at(j + 2, j);
                const real_t xj = vj - (Lj1j * xj1) - (Lj2j * xj2);
                dd.at(j) = xj;
            }
            // Unroll last element, also j is unsigned so is always >= 0
            uAssert(j == 0);
            uAssertBounds(2, 2, dd.n_elem);
            uAssertBounds(2, 2, m_L.n_rows);
            uAssertBounds(0, 0, m_L.n_cols);
            const real_t x2 = dd.at(2);
            const real_t x1 = dd.at(1);
            const real_t v0 = dd.at(0);
            const real_t L10 = m_L.at(1, 0);
            const real_t L20 = m_L.at(2, 0);
            const real_t x0 = v0 - (L10 * x1) - (L20 * x2);
            dd.at(0) = x0;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Step 4 - Solve for y-coordinates at each knot

    // See Green & Silverman, section 3.5.2
    // Assumptions from prime() are:
    //  - m_WinvQ = inverse(W) * Q has been computed
    uAssertPosEq(m_WinvQ.n_rows, N_ELEM);
    uAssertPosEq(m_WinvQ.n_cols + 2, N_ELEM);
    uAssertPosEq(m_WinvQ.n_cols, dd.n_elem);

    // Allocate output vector of knot y-coordinates. Note, vector y is 'g'
    // vector from Green & Silverman. The equation to solve is:
    //  g = Y - alpha * inverse(W) * Q * gamma
    // where gamma has just been computed as output 'dd' vector.
    y.zeros(N_ELEM);

    // Iterate over columns of m_WinvQ
    for (j = 0; j < (N_ELEM - 2); ++j) {
        uAssertBounds(j + 2, 2, m_WinvQ.n_rows);
        uAssertBounds(j, 0, m_WinvQ.n_cols);
        uAssertBounds(j, 0, dd.n_elem);
        // Load
        const real_t WinvQj0j0 = m_WinvQ.at(j, j);
        const real_t WinvQj1j0 = m_WinvQ.at(j + 1, j);
        const real_t WinvQj2j0 = m_WinvQ.at(j + 2, j);
        const real_t ddj0 = dd.at(j);
        // Compute
        const real_t dgj0 = alpha * WinvQj0j0 * ddj0;
        const real_t dgj1 = alpha * WinvQj1j0 * ddj0;
        const real_t dgj2 = alpha * WinvQj2j0 * ddj0;
        // Store
        y.at(j) += dgj0;
        y.at(j + 1) += dgj1;
        y.at(j + 2) += dgj2;
    }
    y = y_filt - y;

    // Finished Reinsch algorithm
    return true;
}

/**
 * Build spline and capture diagonals of linear smooth operator
 */
template <typename t_real>
bool uCspplineSparseDriver<t_real>::fit(
    typename uCspplineSparseDriver<t_real>::vec_t& y,
    typename uCspplineSparseDriver<t_real>::vec_t& dd,
    typename uCspplineSparseDriver<t_real>::vec_t& a_diag,
    const typename uCspplineSparseDriver<t_real>::vec_t& x_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& y_filt,
    const typename uCspplineSparseDriver<t_real>::vec_t& w_filt,
    const t_real alpha) {
    // Assumes prime() has been called by auto-smoother!
    const uMatSz_t N_ELEM = x_filt.n_elem;
    uAssert(N_ELEM > 2);
    uAssertPosEq(N_ELEM, y_filt.n_elem);
    uAssertPosEq(N_ELEM, w_filt.n_elem);
    // Determine y, dd, and LDLt decompositions of B from Reinsch algorithm
    uVerify(this->fit(y, dd, x_filt, y_filt, w_filt, alpha));

    // See Green & Silverman section 3.2.2, only slight modification is
    // to compute the diagonals of the lower triangle of the hat matrix
    // instead of the upper triangle.
    // m_B will now be reused to store the central diagonals of inverse(B)
    uAssert(m_B.n_rows + 2 == N_ELEM);
    uAssert(m_B.n_cols + 2 == N_ELEM);
    uAssert(m_L.n_rows + 2 == N_ELEM);
    uAssert(m_L.n_cols + 2 == N_ELEM);
    uAssert(m_D.n_elem + 2 == N_ELEM);

    // Initialize last band element "b_inv(n,n)" along main diagonal
    const real_t ONE_R = static_cast<real_t>(1.0);
    uMatSz_t j = N_ELEM - 3;
    // Note, Reinsch algorithm should have already tripped asserts if any
    // elements of D are 0.
    m_B.at(j, j) = ONE_R / m_D.at(j);

    if (N_ELEM > 3) {
        uAssert(j >= 1);
        {
            uAssertBounds(j, 1, m_B.n_rows);
            uAssertBounds(j, 1, m_B.n_cols);
            uAssertBounds(j, 1, m_L.n_rows);
            uAssertBounds(j - 1, 0, m_L.n_cols);
            uAssertBounds(j - 1, 0, m_D.n_elem);
            // Load
            const real_t Lnn_1 = m_L.at(j, j - 1);
            const real_t dn_1 = m_D.at(j - 1);
            const real_t binv_nn = m_B.at(j, j);
            // Compute "b_inv(n,n-1)" and "b_inv(n-1, n-1)"
            const real_t binv_nn_1 = -Lnn_1 * binv_nn;
            const real_t binv_n_1n_1 = (ONE_R / dn_1) - (Lnn_1 * binv_nn_1);
            // Store
            m_B.at(j, j - 1) = binv_nn_1;
            m_B.at(j - 1, j - 1) = binv_n_1n_1;
        }

        // Determine remaining diagonals, note equations for binv_j2j0 and
        // binv_j1j0 are based on errata found on Peter Green's website:
        // "We are grateful to Carl de Boor(Madison) for pointing this out.
        //
        //  p.34: The second and third displayed equations are incorrect:
        //        They should read(using TeX style formatting) :
        //
        //  \bar{b}{i,i+1} = -L_{i+1,i}\bar{b}{i+1,i+1}
        //                          - L_{i+2,i}\bar{b}{i+1,i+2}
        //  \bar{b}{i,i+2} = -L_{i+1,i}\bar{b}{i+1,i+2}
        //                          - L_{i+2,i}\bar{b}{i+2,i+2}"
        if (N_ELEM > 4) {
            uAssert(j >= 2);
            for (j -= 2; j > 0; --j) {
                uAssertBounds(j + 2, 3, m_L.n_rows);
                uAssertBounds(j + 1, 2, m_L.n_cols);
                uAssertBounds(j, 1, m_D.n_elem);
                uAssertBounds(j + 2, 3, m_B.n_rows);
                uAssertBounds(j + 2, 3, m_B.n_cols);
                // Load
                const real_t Lj1j0 = m_L.at(j + 1, j);
                const real_t Lj2j0 = m_L.at(j + 2, j);
                const real_t dj0 = m_D.at(j);
                const real_t binv_j1j1 = m_B.at(j + 1, j + 1);
                const real_t binv_j2j1 = m_B.at(j + 2, j + 1);
                const real_t binv_j2j2 = m_B.at(j + 2, j + 2);
                // Compute
                const real_t binv_j2j0 =
                    (-Lj1j0 * binv_j2j1) - (Lj2j0 * binv_j2j2);
                const real_t binv_j1j0 =
                    (-Lj1j0 * binv_j1j1) - (Lj2j0 * binv_j2j1);
                const real_t binv_j0j0 =
                    (ONE_R / dj0) - (Lj1j0 * binv_j1j0) - (Lj2j0 * binv_j2j0);
                // Store
                m_B.at(j, j) = binv_j0j0;
                m_B.at(j + 1, j) = binv_j1j0;
                m_B.at(j + 2, j) = binv_j2j0;
            }
            uAssert(j == 0);
            // Unroll case j=0
            {
                uAssertBounds(2, 2, m_L.n_rows);
                uAssertBounds(1, 1, m_L.n_cols);
                uAssertBounds(0, 0, m_D.n_elem);
                uAssertBounds(2, 2, m_B.n_rows);
                uAssertBounds(2, 2, m_B.n_cols);
                // Load
                const real_t L10 = m_L.at(1, 0);
                const real_t L20 = m_L.at(2, 0);
                const real_t d0 = m_D.at(0);
                const real_t binv_11 = m_B.at(1, 1);
                const real_t binv_21 = m_B.at(2, 1);
                const real_t binv_22 = m_B.at(2, 2);
                // Compute
                const real_t binv_20 = (-L10 * binv_21) - (L20 * binv_22);
                const real_t binv_10 = (-L10 * binv_11) - (L20 * binv_21);
                const real_t binv_00 =
                    (ONE_R / d0) - (L10 * binv_10) - (L20 * binv_20);
                // Store
                m_B.at(0, 0) = binv_00;
                m_B.at(1, 0) = binv_10;
                m_B.at(2, 0) = binv_20;
            }
        }
    }

    // First diagonal is always same
    a_diag.set_size(N_ELEM);
    const real_t b00 = m_B.at(0, 0);
    const real_t q00 = m_Q.at(0, 0);
    const real_t w0 = w_filt.at(0);
    a_diag.at(0) = ONE_R - (alpha * b00 * q00 * q00 / w0);

    // Second diagonal: compute common term
    const real_t q10 = m_Q.at(1, 0);
    a_diag.at(1) = b00 * q10 * q10;

    if (N_ELEM > 3) {
        // Second diagonal: add term present if N_ELEM > 3
        real_t cache;
        {
            const real_t q11 = m_Q.at(1, 1);
            const real_t b10 = m_B.at(1, 0);
            const real_t b11 = m_B.at(1, 1);
            // This term is used by next diagonal
            cache = b10 + b10;
            // Note, second diagonal will be translated and scaled later
            a_diag.at(1) += (b11 * q11 * q11) + (cache * q11 * q10);
        }

        // Compute 3rd to 3rd to last diagonals
        for (j = 2; j < (N_ELEM - 2); ++j) {
            uAssertBounds(j, 2, a_diag.n_elem);
            uAssertBounds(j, 2, w_filt.n_elem);
            uAssertBounds(j, 2, m_Q.n_rows);
            uAssertBounds(j, 2, m_Q.n_cols);
            uAssertBounds(j, 2, m_B.n_rows);
            uAssertBounds(j, 2, m_B.n_cols);
            const real_t wj = w_filt.at(j);
            // Q row j
            const real_t qjj2 = m_Q.at(j, j - 2);
            const real_t qjj1 = m_Q.at(j, j - 1);
            const real_t qjj = m_Q.at(j, j);
            // Note, B is symmetric, only accessing lower triangle
            // B col j-2
            const real_t bj2j2 = m_B.at(j - 2, j - 2);
            const real_t bjj2 = m_B.at(j, j - 2);
            // B col j-1
            const real_t bj1j1 = m_B.at(j - 1, j - 1);
            const real_t bjj1 = m_B.at(j, j - 1);
            // B col j
            const real_t bjj = m_B.at(j, j);

            real_t term = (bjj * qjj * qjj) + (bj1j1 * qjj1 * qjj1) +
                          (bj2j2 * qjj2 * qjj2);
            term += ((cache * qjj1) + ((bjj2 + bjj2) * qjj)) * qjj2;
            cache = bjj1 + bjj1;
            term += cache * qjj * qjj1;
            a_diag.at(j) = ONE_R - (alpha * term / wj);
        }

        // Second to last diagonal is always same
        {
            const real_t wn2 = w_filt.at(N_ELEM - 2);
            const real_t qn2n3 = m_Q.at(N_ELEM - 2, N_ELEM - 3);
            const real_t qn2n4 = m_Q.at(N_ELEM - 2, N_ELEM - 4);
            const real_t bn3n3 = m_B.at(N_ELEM - 3, N_ELEM - 3);
            const real_t bn4n4 = m_B.at(N_ELEM - 4, N_ELEM - 4);
            const real_t term = (bn3n3 * qn2n3 * qn2n3) +
                                (bn4n4 * qn2n4 * qn2n4) +
                                (cache * qn2n3 * qn2n4);
            a_diag.at(N_ELEM - 2) = ONE_R - (alpha * term / wn2);
        }
    }

    // Second diagonal: finish scaling and translating
    const real_t w1 = w_filt.at(1);
    a_diag.at(1) = ONE_R - (alpha * a_diag.at(1) / w1);

    // Last diagonal is always same
    uAssertBounds(N_ELEM - 1, N_ELEM - 1, a_diag.n_elem);
    uAssertBounds(N_ELEM - 1, N_ELEM - 1, w_filt.n_elem);
    uAssertBounds(N_ELEM - 1, N_ELEM - 1, m_Q.n_rows);
    uAssertBounds(N_ELEM - 3, N_ELEM - 3, m_Q.n_cols);
    uAssertBounds(N_ELEM - 3, N_ELEM - 3, m_B.n_rows);
    uAssertBounds(N_ELEM - 3, N_ELEM - 3, m_B.n_cols);
    const real_t wn1 = w_filt.at(N_ELEM - 1);
    const real_t qn1n3 = m_Q.at(N_ELEM - 1, N_ELEM - 3);
    const real_t bn3n3 = m_B.at(N_ELEM - 3, N_ELEM - 3);
    a_diag.at(N_ELEM - 1) = ONE_R - (alpha * bn3n3 * qn1n3 * qn1n3 / wn1);

    return true;
}

//****************************************************************************
// uCsspline
//****************************************************************************

/**
 * Evaluates spline at input x
 * @param x - abscissa coordinate to evaluate spline at
 * @return output value of spline at coordinate x with default 0 if spline
 *  has not yet been built
 */
template <typename t_real, typename t_driver>
t_real uCsppline<t_real, t_driver>::operator()(const t_real x) const {
    // Check if spline is linear
    const unsigned int NUM_KNOTS = this->size();
    if (NUM_KNOTS < 3) {
        // Case: 0 knots, f(x) = 0.0
        if (NUM_KNOTS == 0) {
            return static_cast<real_t>(0.0);
        }

        // Case: 1 knot, f(x) = constant
        if (NUM_KNOTS == 1) {
            // Output is simply a constant
            return m_y.at(0);
        }

        // Case: 2 knots, f(x) = m*x + b
        if (NUM_KNOTS == 2) {
            // Output is simply a straight line
            const real_t x0 = m_x.at(0);
            const real_t x1 = m_x.at(1);
            uAssert(x0 < x1);
            const real_t y0 = m_y.at(0);
            const real_t y1 = m_y.at(1);
            const real_t slope = (y1 - y0) / (x1 - x0);
            const real_t ycept = y1 - (slope * x1);
            return (slope * x) + ycept;
        }
    }

    // Case: 3 or more knots
    // Get first knot greater than or equal to x
    const real_t* it = std::lower_bound(m_x.cbegin(), m_x.cend(), x);
    return putils_t::eval_cubic(*this, x, it);
}

/**
 * Evaluate spline at set of input abscissae defined by range [beg, end).
 * The outputs are written to the consecutive locations defined by out.
 * If spline has not yet been fit to data (size() == 0), then a default
 * value of 0 will be written to all output locations. Method *does not*
 * assume that x-coordinate abscissae are sorted in ascending order. If
 * abscissae are in fact ascending sorted, then pass additional argument
 * 'eCspplineSortedXasc' for more efficient spline evaluation.
 * @param out - iterator to first write-able location for storing results
 *  of spline evaluation in abscissae defined by read iterators beg, end
 * @param beg - iterator to first abscissa (x-coordinate) to evaluate
 * @param end - iterator such that spline evaluation is terminated when
 *  encountered
 */
template <typename t_real, typename t_driver>
template <typename FwdWriteIt, typename FwdReadIt>
void uCsppline<t_real, t_driver>::operator()(FwdWriteIt out,
                                             FwdReadIt beg,
                                             const FwdReadIt end) const {
    // Check if spline is linear
    if (putils_t::eval_linear(*this, out, beg, end)) {
        return;
    }

    // Else, spline is cubic (has 3 or more knots)
    uAssert(this->size() >= 3);
    while (beg != end) {
        const real_t x = *beg;
        // Get first knot greater than or equal to x
        const real_t* it = std::lower_bound(m_x.cbegin(), m_x.cend(), x);
        *out = putils_t::eval_cubic(*this, x, it);
        ++out;
        ++beg;
    }
}

/**
 * ASSUMES X-COORDINATES ENCOUNTERED BY ITERATING RANGE [beg, end) ARE
 * SORTED IN ASCENDING ORDER. DOES NOT CHECK THAT RANGE IS SORTED!
 * Evaluate spline at set of input abscissae defined by range [beg, end).
 * The outputs are written to the consecutive locations defined by out.
 * If spline has not yet been fit to data (size() == 0), then a default
 * value of 0 will be written to all output locations. Assumes abscissae
 * are sorted in ascending order which may lead to more efficient spline
 * evaluation.
 * @param out - iterator to first write-able location for storing results
 *  of spline evaluation in abscissae defined by read iterators beg, end
 * @param beg - iterator to first abscissa (x-coordinate) to evaluate
 * @param end - iterator such that spline evaluation is terminated when
 *  encountered
 */
template <typename t_real, typename t_driver>
template <typename FwdWriteIt, typename FwdReadIt>
void uCsppline<t_real, t_driver>::operator()(
    FwdWriteIt out,
    FwdReadIt beg,
    const FwdReadIt end,
    const uCspplineSortedX x_asc) const {
    // Check if spline is linear
    if (putils_t::eval_linear(*this, out, beg, end)) {
        return;
    }

    // Else, spline is cubic (has 3 or more knots)
    uAssert(this->size() >= 3);
    typename vec_t::const_iterator it = m_x.cbegin();
    while (beg != end) {
        const real_t x = *beg;
        // Get first knot greater than or equal to x, use hint that abscissae
        // are sorted by culling range of knot search according to prior knot
        it = std::lower_bound(it, m_x.cend(), x);
        *out = putils_t::eval_cubic(*this, x, it);
        ++out;
        ++beg;
    }
}

/**
 * @return number of knots within spline
 */
template <typename t_real, typename t_driver>
unsigned int uCsppline<t_real, t_driver>::size() const {
    uAssert(m_x.n_elem == m_y.n_elem);
    uAssert((m_x.n_elem <= 2) || (m_dd.n_elem + 2 == m_x.n_elem));
    uAssert(m_x.is_finite());
    uAssert(m_y.is_finite());
    uAssert(m_dd.is_finite());
    uAssert(m_x.is_sorted());
    return static_cast<unsigned int>(m_x.n_elem);
}

/**
 * Manual smoothing with user-specified roughness penalty alpha
 *
 * Builds a smoothed natural cubic spline which minimizes criterion:
 *      criterion = (1-alpha) * RSS +  alpha * roughness
 *  where RSS is the residual sum of squares and roughness is the integral
 *  of the squared second derivative (see Green, section 1.2.3).
 *
 * All design points with same 'x' coordinate will be replaced by a single
 * weighted design point (x, y_bar) where y_bar is the average value at x;
 * the corresponding RSS term will have weight 'm' where 'm' is the number
 * of tied design points. Note: all unique design points will be weight 1.
 *
 * @param x - input abscissae
 * @param y - observed y values at input x, must be same length as x
 * @param alpha - roughness penalty in [0,1]. If 0, resulting spline will
 *  have no roughness penalty and will interpolate all points perfectly
 *  with low bias but high variance. If 1, resulting spline will have an
 *  infinite roughness penalty and produce a linear least-squares fit with
 *  high bias but low variance. If alpha < 0 or alpha > 1, then alpha will
 *  be set to a default value of (1 - 1/(1 + (h^3)/0.6)) where h is the
 *  average knot spacing; this is similar to default value described in
 *  Matlab's csaps() documentation.
 * @return true if spline build successful, false o/w
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const typename uCsppline<t_real, t_driver>::vec_t& x,
    const typename uCsppline<t_real, t_driver>::vec_t& y,
    const t_real alpha) {
    const vec_t w(x.n_elem, uMatrixUtils::fill::ones);
    return this->fit(x, y, w, alpha);
}

/**
 * Manual smoothing interface for std::vector (default weights)
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(const std::vector<real_t>& x_,
                                      const std::vector<real_t>& y_,
                                      const t_real alpha) {
    const vec_t x(x_);
    const vec_t y(y_);
    return this->fit(x, y, alpha);
}

/**
 * Manual smoothing interface for std::vector (user-specified weights)
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(const std::vector<real_t>& x_,
                                      const std::vector<real_t>& y_,
                                      const std::vector<real_t>& w_,
                                      const t_real alpha) {
    const vec_t x(x_);
    const vec_t y(y_);
    const vec_t w(w_);
    return this->fit(x, y, w, alpha);
}

/**
 * Manual smoothing interface for arbitrary containers all assumed to be
 *  at least size 'n'
 * @param x - forward iterator to x-coordinates
 * @param y - forward iterator to y-coordinates
 * @param n - input number of design points
 * @param alpha - smoothing parameter in [0,1]
 * @return true if spline build successful, false otherwise
 */
template <typename t_real, typename t_driver>
template <typename FwdItX, typename FwdItY>
bool uCsppline<t_real, t_driver>::fit(FwdItX x_,
                                      FwdItY y_,
                                      const unsigned int n,
                                      const t_real alpha) {
    vec_t x(n);
    vec_t y(n);
    for (unsigned int i = 0; i < n; ++i) {
        x.at(i) = *x_;
        y.at(i) = *y_;
        ++x_;
        ++y_;
    }
    return this->fit(x, y, alpha);
}

/**
 * Manual smoothing interface for arbitrary containers all assumed to be
 *  at least size 'n'
 * @param x - forward iterator to x-coordinates
 * @param y - forward iterator to y-coordinates
 * @param w - forward iterator to weights, will default to 1 if not
 *  specified by user
 * @param n - input number of design points
 * @param alpha - smoothing parameter in [0,1]
 * @return true if spline build successful, false otherwise
 */
template <typename t_real, typename t_driver>
template <typename FwdItX, typename FwdItY, typename FwdItW>
bool uCsppline<t_real, t_driver>::fit(FwdItX x_,
                                      FwdItY y_,
                                      FwdItW w_,
                                      const unsigned int n,
                                      const t_real alpha) {
    vec_t x(n);
    vec_t y(n);
    vec_t w(n);
    for (unsigned int i = 0; i < n; ++i) {
        x.at(i) = *x_;
        y.at(i) = *y_;
        w.at(i) = *w_;
        ++x_;
        ++y_;
        ++w_;
    }
    return this->fit(x, y, w, alpha);
}

/**
 * Manual smoothing with user-specified roughness penalty alpha
 *
 * Builds a weighted, smoothed natural cubic spline minimizing criterion:
 *      criterion = (1-alpha) * RSS(w) +  alpha * roughness
 *  where RSS(w) is the weighted residual sum of squares where each
 *  individual squared residual has magnitude w_i * (y_i - s(x_i))^2 and
 *  roughness is the integral of the squared second derivative (see Green,
 *  section 3.5.4).
 *
 * All design points with same 'x' coordinate will be replaced by a single
 * weighted design point (x, y_bar) where y_bar is the weighted average
 * value at x; the corresponding RSS term will have weight 'm' where 'm'
 * is the sum of the weights for all design points at x.
 *
 * @param x - input abscissae
 * @param y - observed y values at input x, must be same length as x
 * @param w - non-negative weights at each design point, typically set to
 *  values inversely proportional to the variance at each point
 * @param alpha - roughness penalty in [0,1]. If 0, resulting spline will
 *  have no roughness penalty and will interpolate all points perfectly
 *  with low bias but high variance. If 1, resulting spline will have an
 *  infinite roughness penalty and produce a linear least-squares fit with
 *  high bias but low variance. If alpha < 0 or alpha > 1, then alpha will
 *  be set to a default value of (1 - 1/(1 + (h^3)/0.6)) where h is the
 *  average knot spacing; this is similar to default value described in
 *  Matlab's csaps() documentation.
 * @return true if spline build successful, false o/w
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const typename uCsppline<t_real, t_driver>::vec_t& x,
    const typename uCsppline<t_real, t_driver>::vec_t& y,
    const typename uCsppline<t_real, t_driver>::vec_t& w,
    const t_real alpha_) {

    // Reset internal state
    this->clear();

    // Make sure arguments are valid
    if (!putils_t::args_valid(x, y, w)) {
        return false;
    }

    // Replace any tied design points with weighted average. See Green &
    // Silverman text, section 3.5.4
    vec_t y_filt, w_filt;
    if (!putils_t::filter_knots(m_x, y_filt, w_filt, x, y, w)) {
        // All knots were removed!
        uAssert(m_x.n_elem == 0);
        return false;
    }

    // Handle edge cases with only 1 or 2 knots
    if (putils_t::fit_linear(*this, y_filt)) {
        // Succeeded in fitting linear spline
        return true;
    }

    // Ensure alpha is in [0, 1)
    real_t alpha = alpha_;
    const real_t one_r = static_cast<real_t>(1.0);
    if ((alpha < static_cast<real_t>(0.0)) || (alpha > one_r)) {
        // Alpha is out of range, set to default. Equation is based on
        // documentation for Matlab's csaps().
        // @TODO - should this be weighted?
        const real_t h = putils_t::get_mean_spacing(m_x);
        // According to csaps(), for uniformly spaced data, base_r = 60 will
        // give very small residual error and base_r = 0.6 will result in
        // "satisfactory" smoothing. The "interesting range" appears to be
        // around base_r = 6.0. However, to help avoid over-fitting, we use
        // the more conservative base_r = 0.6. Also, note that our alpha is
        // 1 - p where p is the csaps() smoothing parameter.
        const real_t base_r = static_cast<real_t>(0.6);
        alpha = one_r - (one_r / (one_r + ((h * h * h) / base_r)));
    }

    // Convert to roughness penalty in (0, +inf) for use by driver
    alpha = putils_t::alpha_transform(alpha);

    // Defer to driver to fit spline parameters
    if (!m_driver.prime(m_x, y_filt, w_filt)) {
        this->clear();
        // Failed to prime driver
        return false;
    }
    if (!m_driver.fit(m_y, m_dd, m_x, y_filt, w_filt, alpha)) {
        this->clear();
        // Failed to fit cubic spline
        return false;
    }
    // Clear any transient driver state
    m_driver.post();
    // Succeeded in fitting cubic spline
    return true;
}

/**
 * Auto-smoothing is used to determine the roughness penalty alpha.
 *
 * Builds a smoothed natural cubic spline which minimizes criterion:
 *      criterion = (1-alpha) * RSS +  alpha * roughness
 *  where RSS is the residual sum of squares and roughness is the integral
 *  of the squared second derivative (see Green, section 1.2.3).
 *
 * All design points with same 'x' coordinate will be replaced by a single
 * weighted design point (x, y_bar) where y_bar is the average value at x;
 * the corresponding RSS term will have weight 'm' where 'm' is the number
 * of tied design points. Note: all unique design points will be weight 1.
 *
 * @param x - input abscissae
 * @param y - observed y values at input x, must be same length as x
 * @param type - specifies auto smooth algorithm:
 *      auto_smooth_cv: leave-one-out cross validation (recommended)
 *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
 *      auto_smooth_edf: effective degrees of freedom
 *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
 *      validation and generalized cross-validation.
 *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
 *      and James, Witten, Hastie, & Tibshirani, An Introduction to
 *      Statistical Learning, section 7.5.2 for description of effective
 *      degrees of freedom.
 * @param info - optional pointer to info structure for detailed results
 * @param edf - effective degrees of freedom, must be in range [1, n]
 *  where n is number of unique elements in parameter 'x', only used if
 *  parameter 'type' is 'auto_smooth_edf'.
 * @param config - optional pointer to custom auto-smoother configuration,
 *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
 * @return true if spline build successful, false o/w
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const typename uCsppline<t_real, t_driver>::vec_t& x,
    const typename uCsppline<t_real, t_driver>::vec_t& y,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {
    const vec_t w(x.n_elem, uMatrixUtils::fill::ones);
    return this->fit(x, y, w, type, info, edf, config);
}

/**
 * Auto smoothing interface for std::vector (default weights)
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const std::vector<real_t>& x_,
    const std::vector<real_t>& y_,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {
    const vec_t x(x_);
    const vec_t y(y_);
    const vec_t w(x.n_elem, uMatrixUtils::fill::ones);
    return this->fit(x, y, w, type, info, edf, config);
}

/**
 * Auto smoothing interface for std::vector (user-specified weights)
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const std::vector<real_t>& x_,
    const std::vector<real_t>& y_,
    const std::vector<real_t>& w_,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {
    const vec_t x(x_);
    const vec_t y(y_);
    const vec_t w(w_);
    return this->fit(x, y, w, type, info, edf, config);
}

/**
 * Auto-smoothing interface for arbitrary containers all assumed to be at
 *  least size 'n'
 * @param x - forward iterator to x-coordinates
 * @param y - forward iterator to y-coordinates
 * @param n - input number of design points
 * @param type - specifies auto smooth algorithm:
 *      auto_smooth_cv: leave-one-out cross validation (recommended)
 *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
 *      auto_smooth_edf: effective degrees of freedom
 *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
 *      validation and generalized cross-validation.
 *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
 *      and James, Witten, Hastie, & Tibshirani, An Introduction to
 *      Statistical Learning, section 7.5.2 for description of effective
 *      degrees of freedom.
 * @param info - optional pointer to info structure for detailed results
 * @param edf - effective degrees of freedom, must be in range [1, n]
 *  where n is number of unique elements in parameter 'x', only used if
 *  parameter 'type' is 'auto_smooth_edf'.
 * @param config - optional pointer to custom auto-smoother configuration,
 *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
 * @return true if spline fit successful, false otherwise
 */
template <typename t_real, typename t_driver>
template <typename FwdItX, typename FwdItY>
bool uCsppline<t_real, t_driver>::fit(
    FwdItX x_,
    FwdItY y_,
    const unsigned int n,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {
    vec_t x(n);
    vec_t y(n);
    for (unsigned int i = 0; i < n; ++i) {
        x.at(i) = *x_;
        y.at(i) = *y_;
        ++x_;
        ++y_;
    }
    return this->fit(x, y, type, info, edf, config);
}

/**
 * Auto-smoothing interface for arbitrary containers all assumed to be at
 *  least size 'n'
 * @param x - forward iterator to x-coordinates
 * @param y - forward iterator to y-coordinates
 * @param w - forward iterator to weights, will default to 1 if not
 *  specified by user
 * @param n - input number of design points
 * @param type - specifies auto smooth algorithm:
 *      auto_smooth_cv: leave-one-out cross validation (recommended)
 *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
 *      auto_smooth_edf: effective degrees of freedom
 *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
 *      validation and generalized cross-validation.
 *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
 *      and James, Witten, Hastie, & Tibshirani, An Introduction to
 *      Statistical Learning, section 7.5.2 for description of effective
 *      degrees of freedom.
 * @param info - optional pointer to info structure for detailed results
 * @param edf - effective degrees of freedom, must be in range [1, n]
 *  where n is number of unique elements in parameter 'x', only used if
 *  parameter 'type' is 'auto_smooth_edf'.
 * @param config - optional pointer to custom auto-smoother configuration,
 *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
 * @return true if spline fit successful, false otherwise
 */
template <typename t_real, typename t_driver>
template <typename FwdItX, typename FwdItY, typename FwdItW>
bool uCsppline<t_real, t_driver>::fit(
    FwdItX x_,
    FwdItY y_,
    FwdItW w_,
    const unsigned int n,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {
    vec_t x(n);
    vec_t y(n);
    vec_t w(n);
    for (unsigned int i = 0; i < n; ++i) {
        x.at(i) = *x_;
        y.at(i) = *y_;
        w.at(i) = *w_;
        ++x_;
        ++y_;
        ++w_;
    }
    return this->fit(x, y, w, type, info, edf, config);
}

/**
 * Auto-smoothing is used to determine the roughness penalty alpha.
 *
 * Builds a weighted, smoothed natural cubic spline minimizing criterion:
 *      criterion = (1-alpha) * RSS(w) +  alpha * roughness
 *  where RSS(w) is the weighted residual sum of squares where each
 *  individual squared residual has magnitude w_i * (y_i - s(x_i))^2 and
 *  roughness is the integral of the squared second derivative (see Green,
 *  section 3.5.4).
 *
 * All design points with same 'x' coordinate will be replaced by a single
 * weighted design point (x, y_bar) where y_bar is the weighted average
 * value at x; the corresponding RSS term will have weight 'm' where 'm'
 * is the sum of the weights for all design points at x.
 *
 * @param x - input abscissae
 * @param y - observed y values at input x, must be same length as x
 * @param w - non-negative weights at each design point, typically set to
 *  values inversely proportional to the variance at each point
 * @param type - specifies auto smooth algorithm:
 *      auto_smooth_cv: leave-one-out cross validation (recommended)
 *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
 *      auto_smooth_edf: effective degrees of freedom
 *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
 *      validation and generalized cross-validation.
 *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
 *      and James, Witten, Hastie, & Tibshirani, An Introduction to
 *      Statistical Learning, section 7.5.2 for description of effective
 *      degrees of freedom.
 * @param info - optional pointer to info structure for detailed results
 * @param edf - effective degrees of freedom, must be in range [1, n]
 *  where n is number of unique elements in parameter 'x', only used if
 *  parameter 'type' is 'auto_smooth_edf'.
 * @param config - optional pointer to custom auto-smoother configuration,
 *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
 * @return true if spline build successful, false o/w
 */
template <typename t_real, typename t_driver>
bool uCsppline<t_real, t_driver>::fit(
    const typename uCsppline<t_real, t_driver>::vec_t& x,
    const typename uCsppline<t_real, t_driver>::vec_t& y,
    const typename uCsppline<t_real, t_driver>::vec_t& w,
    const uCspplineAutoSmooth type,
    typename uCsppline<t_real, t_driver>::auto_smooth_info_t* const info,
    const t_real edf,
    typename uCsppline<t_real, t_driver>::auto_smooth_config_t* const config) {

    // Determine user auto-smooth configuration
    const unsigned int max_iters =
        config ? config->max_iters : auto_smooth_defaults_t::max_iters();
    const real_t min_alpha =
        config ? config->min_alpha : auto_smooth_defaults_t::min_alpha();
    const real_t max_alpha =
        config ? config->max_alpha : auto_smooth_defaults_t::max_alpha();
    uAssert(max_iters > 0);
    uAssertBounds(min_alpha, static_cast<real_t>(0.0), max_alpha);
    uAssert(max_alpha <= static_cast<real_t>(1.0));

    // Reset internal state
    this->clear();

    // Make sure arguments are valid
    if (!putils_t::args_valid(x, y, w, max_iters, min_alpha, max_alpha)) {
        return false;
    }

    // Replace any tied design points with weighted average. See Green &
    // Silverman text, section 3.5.4
    typename putils_t::auto_smooth_spec_t sm_spc;
    if (!putils_t::filter_knots(m_x, sm_spc.y_filt, sm_spc.w_filt, x, y, w)) {
        // All knots were removed!
        uAssert(m_x.n_elem == 0);
        return false;
    }

    // Handle edge cases with only 1 or 2 knots
    if (putils_t::fit_linear(*this, sm_spc.y_filt, info)) {
        return true;
    }

    // Finish initializing configuration for auto-smoother
    sm_spc.max_iters = max_iters;
    // Make sure target edf is [1, n]
    sm_spc.target_edf = std::min(std::max(edf, static_cast<real_t>(1.0)),
                                 static_cast<real_t>(m_x.n_elem));
    sm_spc.min_alpha = putils_t::alpha_transform(min_alpha);
    sm_spc.max_alpha = putils_t::alpha_transform(max_alpha);
    sm_spc.e_smooth = type;

    // Auto-smoother buffers
    typename putils_t::auto_smooth_state_t sm_st;
    bool result = false;

    switch (type) {
        case uCspplineAutoSmoothCv: {
            typedef typename putils_t::scorer_cv scorer_t;
            typedef typename putils_t::template auto_smoother<scorer_t>
                auto_smoother_t;
            auto_smoother_t s(*this, sm_st, sm_spc);
            result = s.run(info);
        } break;
        case uCspplineAutoSmoothGcv: {
            typedef typename putils_t::scorer_gcv scorer_t;
            typedef typename putils_t::template auto_smoother<scorer_t>
                auto_smoother_t;
            auto_smoother_t s(*this, sm_st, sm_spc);
            result = s.run(info);
        } break;
        case uCspplineAutoSmoothEdf: {
            typedef typename putils_t::scorer_edf scorer_t;
            typedef typename putils_t::template auto_smoother<scorer_t>
                auto_smoother_t;
            auto_smoother_t s(*this, sm_st, sm_spc);
            result = s.run(info);
        } break;
        default:
            uAssert(false && "Unrecognized auto-smooth type!");
    }

    // Check if auto-smoother was successful
    if (!result) {
        // Auto-smooth failed, clear state
        this->clear();
    }

    return result;
}

/**
 * Resets internal state
 */
template <typename t_real, typename t_driver>
void uCsppline<t_real, t_driver>::clear() {
    m_x.clear();
    m_y.clear();
    m_dd.clear();
    m_driver.clear();
}

#endif  // uCsspline_inl
