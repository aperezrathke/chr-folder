//****************************************************************************
// uSisPowerSamplerRsMixin.h
//****************************************************************************

/**
 * SamplerRsMixin = A mixin that defines how samples are chosen when a resample
 * checkpoint is encountered
 */

#ifndef uSisPowerSamplerRsMixin_h
#define uSisPowerSamplerRsMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * All sample weights are raised to a power 'alpha' and then selected with
 * probability proportional to transformed weight.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing from configuration file
    // e.g uOpt_qc_sampler_power_alpha_slot0
    enum uOptE e_Key>
class uSisPowerSamplerRsMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin ensures the parameter
     * norm exponential weights are valid
     */
    enum { needs_norm_exp_weights = true };

    /**
     * Default constructor
     */
    uSisPowerSamplerRsMixin() { clear(); }

    /**
     * Initializes sampler
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
        // Check if option exists in config
        config->read_into(this->alpha, e_Key);
        if (this->alpha < U_TO_REAL(0.0)) {
            uLogf("Error: power resampler exponent alpha %f < 0. Exiting.\n",
                  (double)this->alpha);
            exit(uExitCode_error);
        }
        // We require that our threshold value be non-negative
        uAssert(this->alpha >= U_TO_REAL(0.0));
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    inline void clear() { this->alpha = U_SIS_QC_SAMPLER_POWER_DEFAULT_ALPHA; }

    /**
     * Performs resampling and modifies status and log weights
     * @param out_status - 1 if sample is still able to grow, 0 otherwise
     * @param out_log_weights - The current log weight of each sample, will
     *  be replaced by modified weight of chosen sample
     * @param out_samples - The current set of samples to resample from. The
     *  final set of selected samples will overwrite this vector.
     * @param norm_exp_weights - The vector of exp transformed weights
     *  normalized by the max weight - should be in (0,1]. Weights will no
     *  longer be representative of sample after this function finishes.
     * @param max_log_weight - The unnormalized maximal log weight. Weight
     *  will no longer be representative of sample after this function
     *  finishes.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - The parent quality control mixin
     * @param sim - The parent simulation
     */
    void qc_resample(uWrappedBitset& out_status,
                     uVecCol& out_log_weights,
                     std::vector<sample_t>& out_samples,
                     const uVecCol& norm_exp_weights,
                     const uReal max_log_weight,
                     const uUInt num_completed_grow_steps_per_sample,
                     const sim_level_qc_mixin_t qc,
                     const sim_t& sim U_THREAD_ID_PARAM) {
        uAssertPosEq(out_status.size(), out_log_weights.size());
        uAssert(out_status.size() == out_samples.size());
        uAssert(out_status.size() == norm_exp_weights.size());

        // Compute smoothed probabilities by raising weights to the alpha
        // power. Samples are chosen based on this smoothed weight.
        uVecCol smoothed_probabilities;
        uSisUtilsQc::calc_power_smoothed_probabilities(
            smoothed_probabilities, norm_exp_weights, this->alpha);

        // Compute log of each selection probability for use in weight
        // correction
        const uVecCol log_smoothed_probabilities =
            uMatrixUtils::log(smoothed_probabilities);
        uAssert(out_status.size() == smoothed_probabilities.size());
        uAssert(out_status.size() == log_smoothed_probabilities.size());

        // Create the pool of samples to select from
        uBitset status_pool;
        out_status.to_hard_bitset(status_pool);
        uAssert(status_pool.size() == out_status.size());
        const uVecCol log_weights_pool = out_log_weights;
        const std::vector<sample_t> samples_pool = out_samples;

        // Resample with replacement
        const uUInt n_samples = U_TO_UINT(out_samples.size());
        uUInt i_keep = 0;
        for (uUInt i_replace = 0; i_replace < n_samples; ++i_replace) {
            // Select a sample from pool
            i_keep = uSisUtilsQc::rand_select_index(
                smoothed_probabilities U_THREAD_ID_ARG);
            uAssertBounds(i_keep, 0, n_samples);
            // Replace sample status
            uAssert(status_pool[i_keep]);
            out_status.set(i_replace, status_pool.test(i_keep));
            // Replace log weight
            out_log_weights[i_replace] =
                log_weights_pool[i_keep] - log_smoothed_probabilities[i_keep];
            // Replace sample
            out_samples[i_replace] = samples_pool[i_keep];
        }
    }

private:
    /**
     * All sample weights are raised to this power. Each sample then has
     * probability proportional to raised weight of being selected. For values
     * of alpha < 1.0, this has the effect of smoothing out the weight
     * distribution and (ideally) leading to more diversity. For value of
     * alpha > 1.0, this has the effect of boosting the highest weights
     * (and less diversity).
     */
    uReal alpha;
};

#endif  // uSisPowerSamplerRsMixin_h
