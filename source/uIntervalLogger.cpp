//****************************************************************************
// uIntervalLogger.cpp
//****************************************************************************

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_INTERVAL_LOGGING

#include "uIntervalLogger.h"

namespace uStats {
    /**
     * The static descriptions used for interval logging
     */
    const char* GCounterIntervalLoggerDescs[uSTAT_IntervalLoggerMax] = {
        "Total execution time:",
        "Trial time:",
        "QC checkpoint time:"
    };

}  // namespace uStats

#endif  // U_BUILD_ENABLE_INTERVAL_LOGGING
