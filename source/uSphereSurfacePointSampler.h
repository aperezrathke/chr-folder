//****************************************************************************
// uSphereSurfacePointSampler.h
//****************************************************************************

/**
 * Utility for obtaining points "evenly" distributed on the surface of a
 * unit sphere.
 *
 * Code uses source from: http://rotations.mitchell-lab.org/
 * A. Yershova, S. Jain, S. M. LaValle, and J. C. Mitchell. Generating Uniform
 * Incremental Grids on SO(3) Using the Hopf Fibration International Journal
 * of Robotics Research, November 2009.
 */

#ifndef uSphereSurfacePointSampler_h
#define uSphereSurfacePointSampler_h

#include "uBuild.h"
#include "uTypes.h"

namespace uSphereSurfacePointSampler {
    extern void get_points(uMatrix& mat, const unsigned int num_points);
}

#endif  // uSphereSurfacePointSampler_h
