//****************************************************************************
// uSisUtilsExport.cpp
//****************************************************************************

#include "uBuild.h"
#include "uSisUtilsExport.h"
#include "uFilesystem.h"
#include "uSisUtilsExportPath.h"

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Configure default CSV exporter precision
 */
#if defined(U_BUILD_ENABLE_BIG_GRID_COLLISION64) || \
    defined(U_BUILD_ENABLE_BIG_GRID_COLLISION128)
#       define U_BUILD_CSV_EXPORT_DEFAULT_PRECISION 64
#else
#       define U_BUILD_CSV_EXPORT_DEFAULT_PRECISION 32
#endif // defined(U_BUILD_ENABLE_BIG_GRID_COLLISION64|128)

//****************************************************************************
// Export utilities
//****************************************************************************

namespace uSisUtils {

/**
 * Utility to obtain file name from arguments
 * @param job_id - Job identifier prefix
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export file name for a single export file
 */
std::string get_export_fname(const std::string& job_id,
                             const std::string& file_id,
                             const std::string& ext) {
    const std::string fname = job_id + "." + file_id + "." + ext;
    return fname;
}

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
std::string get_export_fpath(uSpConstConfig_t config,
                             const std::string& subdir,
                             const std::string& file_id,
                             const std::string& ext) {
    const std::string fname(get_export_fname(config->job_id, file_id, ext));

    const uFs::path outd(config->output_dir);
    const uFs::path f(fname);

    if (!subdir.empty()) {
        return (outd / uFs::path(subdir) / f).string();
    }

    return (outd / f).string();
}

/**
 * For PDB-based exporters, have fixed number of characters to represent 3-D
 * positions. Therefore, we scale positions in order to help mitigate issues
 * due to having a fixed number of columns to represent each position.
 * @return scale factor for PDB-based formats
 */
uReal get_export_pdb_scale(const uVecCol& node_radii) {
    uAssert(node_radii.n_elem > 0);
    // @HACK - scale to size of alpha carbon as otherwise the
    // the units may not fit in PDB format
    const uReal CARBON_RADIUS = U_TO_REAL(1.7);
    const uReal min_radius = uMatrixUtils::min(node_radii);
    uAssert(min_radius > U_TO_REAL(0.0));
    const uReal scale = CARBON_RADIUS / min_radius;
    return scale;
}

/**
 * @param config - User configuration
 * @return CSV export precision.
 * @WARNING - THIS VALUE IS IGNORED IF LOWER THAN STREAM DEFAULT!
 */
int get_export_csv_precision(uSpConstConfig_t config) {
    int p = U_BUILD_CSV_EXPORT_DEFAULT_PRECISION;
    config->read_into(p, uOpt_export_csv_precision);
    p = std::max(p, 0);
    return p;
}

/**
 * Utility exports all log weights to single file
 * @param log_weights - The log weights to export
 */
void export_log_weights(const uVecCol& log_weights, uSpConstConfig_t config) {
    // Early out if no weights
    if (log_weights.n_elem == 0) {
        return;
    }

    // Obtain export path
    const std::string fpath = get_export_fpath(config,
                                               "log_weights", /*subdir*/
                                               "log_weights", /*file_id*/
                                               "csv");        /*file_ext*/

    uFs_create_parent_dirs(fpath);
    log_weights.save(fpath, uMatrixUtils::csv_ascii);
}

}  // namespace uSisUtils
