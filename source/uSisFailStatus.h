//****************************************************************************
// uSisFailStatus.h
//****************************************************************************

#ifndef uSisFailStatus_h
#define uSisFailStatus_h

/**
 * Failure status bits - represent the various reasons that a sample may have
 * failed a grow_*() call. Am representing as bit flags as it may be the
 * case that these fields are not mutually exclusive.
 */
enum uSisSampleFailStatus {
    /**
     * Sample could not be grown due to constraint violations
     */
    uSisFail_CONS = 0x1
    /**
     * Sample failed due quality control (e.g. didn't pass checkpoint)
     */
    ,
    uSisFail_QC = 0x1 << 1
};

#endif  // uSisFailStatus_h
