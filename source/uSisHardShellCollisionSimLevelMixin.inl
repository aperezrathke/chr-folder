//****************************************************************************
// uSisHardShellCollisionSimLevelMixin.inl
//****************************************************************************

/**
 * A collision mixin manages the details for checking candidate nodes to see if
 * the satisfy self-avoidance (do not collide with any existing nodes)
 *
 * An accessory structure is the simulation level collision mixin which
 * define global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 * HardShell - Uses broad phase filtering and then applies a narrow phase hard
 *  shell (i.e. no overlap) collision model to enforce 'self-avoidance'
 */

/**
 * Mixin encapsulating global (sample independent) data and utilities. In
 * this case, the simulation level collision mixin manages the pre-allocated
 * scratch buffers used in collision checking.
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisHardShellCollisionSimLevelMixin_threaded
#else
class uSisHardShellCollisionSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
{
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Broad phase results type
     */
    typedef std::vector<uUInt> broad_phase_results_t;

    /**
     * Initializes simulation level collision mixin by pre-allocating scratch
     * buffers used for collision checking.
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        uAssert(sim.get_max_total_num_nodes() > 0);
        // Initialize grid cell length
        this->collision_grid_cell_length =
            uSisUtils::BroadPhaseCollisionConfig::get_grid_cell_length(
                config, sim.get_node_radii());

        // Initialize grid bounding radius
        this->collision_bounding_radius =
            uSisUtils::BroadPhaseCollisionConfig::get_bounding_radius(
                sim.get_max_nuclear_radius(),
                sim.get_max_node_diameter(),
                uSisIntrChrConfig::get_knock_in_dist(config),
                config->export_flags);
        uAssert(this->collision_bounding_radius > U_TO_REAL(0.0));

        // Initialize TLS mask buffer
        this->collision_broad_phase_mask.init(sim.get_max_total_num_nodes());

        /**
         * @WARNING:
         *  THIS TABLE HAS SIZE == 4 if SSE collisions are *enabled* to allow
         *      direct loading of positions to SSE registers.
         *  THIS TABLE HAS SIZE == 3 if SSE collisions are *disabled* and
         *      nodes are *homogeneous* for better cache and memory usage.
         *  THIS TABLE HAS SIZE == 4 if SSE *disabled* and nodes are
         *      *inhomogeneous* to allow encoding radius in last element.
         */
        {
#ifdef U_BUILD_ENABLE_SSE_COLLISION
            const size_t n_rows = U_SSE_VEC_DIM; /* quadword */
#else
            // If non-homogeneous nodes, then allocate extra dimension to store
            // radius
            const size_t n_rows = sample_t::is_homg_radius
                                      ? uDim_num           /* triword */
                                      : uDim_num_extended; /* quadword */
#endif
            U_INIT_TLS_DATA_FOR_EACH_2_PARAM(
                this->collision_broad_phase_positions,
                zeros,
                n_rows,
                sim.get_max_total_num_nodes()); /* n_cols */
        }
        // Determine if we can cache minimum node-to-node (n2n) squared
        // distance, only valid for homogeneous node simulations (all nodes
        // have same radii)
        if (sim_t::is_homg_radius) {
            // Initialize global minimum node to node squared distance
            this->collision_homg_min_n2n_sq_dist =
                this->calc_collision_homg_min_n2n_sq_dist(
                    sim.get_unit_sphere_sample_points(),
                    sim.get_max_node_diameter());
            uAssert(this->collision_homg_min_n2n_sq_dist >= U_TO_REAL(0.0));
        }
    }

    /**
     * Resets to default state
     */
    void clear() {
        this->collision_homg_min_n2n_sq_dist = U_TO_REAL(0.0);
        this->collision_grid_cell_length = U_TO_REAL(0.0);
        this->collision_bounding_radius = U_TO_REAL(0.0);
        this->collision_broad_phase_mask.clear();
        this->collision_broad_phase_positions.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * @return Minimum squared distance collision threshold for heterogeneous
     *  nodes
     */
    inline static uReal calc_collision_hetr_min_n2n_sq_dist(
        const uReal radius_a,
        const uReal radius_b) {
        uAssert(radius_a > U_TO_REAL(0.0));
        uAssert(radius_b > U_TO_REAL(0.0));
        uReal min_n2n_sq_dist = radius_a + radius_b;
        min_n2n_sq_dist *= min_n2n_sq_dist;
        min_n2n_sq_dist -= U_REAL_EPS;
        return min_n2n_sq_dist;
    }

    // Accessors

    /**
     * @return minimum node-to-node squared distance threshold, only valid if
     *  simulation is over homogeneous nodes (all have same radii)
     */
    inline uReal get_collision_homg_min_n2n_sq_dist() const {
        uAssert(sim_t::is_homg_radius);
        uAssert(collision_homg_min_n2n_sq_dist >= U_TO_REAL(0.0));
        return this->collision_homg_min_n2n_sq_dist;
    }

    /**
     * Grid cells have dimension L x L x L
     * @return L (length of one dimension)
     */
    inline uReal get_collision_grid_cell_length() const {
        uAssert(collision_grid_cell_length > U_TO_REAL(0.0));
        return this->collision_grid_cell_length;
    }

    /**
     * @return the simulation bounding radius
     */
    inline uReal get_collision_bounding_radius() const {
        uAssert(collision_bounding_radius > U_TO_REAL(0.0));
        return this->collision_bounding_radius;
    }

    // Note: parent simulation is likely to be const, therefore making TLS
    // mutable objects

    /**
     * @return Bit set buffer for intermediate broad phase collision results
     */
    inline uWrappedBitset get_collision_broad_phase_mask(
        U_THREAD_ID_0_PARAM) const {
        return U_WRAP_BITSET(
            collision_broad_phase_mask.get_mask(U_THREAD_ID_0_ARG));
    }

    /**
     * @return Fixed size positions buffer for better cache usage
     */
    inline uSseTable& get_collision_broad_phase_positions(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(collision_broad_phase_positions);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

protected:
    /**
     * @HACK - Allow child classes to mutate this homogeneous node-to-node
     *  (n2n) collision threshold
     * @param sq_dist - New squared distanced threshold for homogeneous node
     *  simulations (all nodes have same radii)
     */
    void set_collision_homg_min_n2n_sq_dist(const uReal sq_dist) {
        uAssert(sim_t::is_homg_radius);
        collision_homg_min_n2n_sq_dist = sq_dist;
    }

private:
    /**
     * Utility for initializing the minimum node to node (n2n) squared
     * distance. This squared distance is used when calculating if two
     * nodes collide and is exactly (node_diameter * node_diameter) (assuming
     * all nodes have same diameter). However, there are both issues with
     * floating point precision and with sampling of the unit sphere that, in
     * order to avoid unwanted collisions between candidate nodes and parent
     * nodes, we must take into account. The basic idea is to check the
     * squared distances of all candidate node centers relative to the origin.
     * We then return the minimum of these values minus a small epsilon.
     * @param unit_sphere_sample_points - the basis sample points for
     *  generating new candidates chromatin nodes
     * @param node_diameter - the diameter of a chromatin node
     * @return The minimum squared distance between two nodes at which they
     *  are not considered colliding
     */
    static inline uReal calc_collision_homg_min_n2n_sq_dist(
        const uMatrix& unit_sphere_sample_points,
        const uReal node_diameter) {
        // Assuming each row defines a single point
        uAssert(unit_sphere_sample_points.size() != 0);
        uAssert(unit_sphere_sample_points.n_cols == uDim_num);
        uAssert(unit_sphere_sample_points.n_rows > 0);
        uAssert(node_diameter > U_TO_REAL(0.0));

        // Basic idea is that we don't want the sample points to trigger
        // narrow-phase collisions with the parent node that generated them.
        // Instead of keeping track of which node is the parent, lets
        // try finding the distance threshold at the origin that would prevent
        // this from happening in the first place.

        // Scale each sample point by node diameter to obtain a candidate
        // node's center position
        uMatrix scaled_points = unit_sphere_sample_points * node_diameter;

        // Dot product to find the squared distance from the origin
        scaled_points %= scaled_points;
        uVecCol dot_prod = scaled_points.unsafe_col(uDim_X) +
                           scaled_points.unsafe_col(uDim_Y) +
                           scaled_points.unsafe_col(uDim_Z);

        // Take minimum squared distance and pad by epsilon to accommodate
        // issues with floating point precision
        const uReal min_node_node_sq_dist = dot_prod.min() - U_REAL_EPS;
        uAssert(min_node_node_sq_dist > U_TO_REAL(0.0));
        return min_node_node_sq_dist;
    }

    /**
     * The minimum squared distance between two nodes (node-to-node == n2n) in
     * order to not be colliding. Assumed to be same for all nodes in
     * simulation.
     * @WARNING - ONLY VALID FOR HOMOGENEOUS NODE SIMULATIONS!
     */
    uReal collision_homg_min_n2n_sq_dist;

    /**
     * All grid cells have dimensions L x L x L, this value stores L
     */
    uReal collision_grid_cell_length;

    /**
     * The bounding radius of the entire simulation, it is assumed that no
     * polymer node will be outside this radius
     */
    uReal collision_bounding_radius;

    /**
     * Dynamic bit set, 1 if element needs narrow phase checking, 0 otherwise
     */
    mutable uSisUtils::BroadPhaseCollisionMask collision_broad_phase_mask;

    /**
     * Buffer for storing contiguous broad phase node positions
     */
    mutable U_DECLARE_TLS_DATA(uSseTable, collision_broad_phase_positions);
};
