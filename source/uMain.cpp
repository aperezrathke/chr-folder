//****************************************************************************
// uMain.cpp
//
// Main program entry point
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAppHooks.h"
#include "uApps.h"
#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uIntervalLogger.h"
#include "uOpts.h"

// Commandlet integration
#include "uCommandletHooks.inl"

// Test integration
#include "uTestHooks.inl"

//****************************************************************************
// Anonymous namespace for simple applications and utilities
//****************************************************************************

namespace {

/**
 * Simple app for printing usage information
 */
static int uAppHelpMain(const uCmdOptsMap& cmd_opts) {
    uOpt_print_usage_all();

    // Signify we finished okay
    return uExitCode_normal;
}

}  // end of anonymous namespace

//****************************************************************************
// App registration
//****************************************************************************

// Register applications here
const struct uAppInfo GApps[] = {
    {uOpt_get_cmd_switch(uOpt_null_dispatch), &uDispatchNull::main},
    {uOpt_get_cmd_switch(uOpt_main_dispatch), &uDispatchMain::main},
    {uOpt_get_cmd_switch(uOpt_fea_dispatch), &uDispatchFea::main},
    {uOpt_get_cmd_switch(uOpt_help), &uAppHelpMain},
    {uOpt_get_cmd_switch(uOpt_app_help), &uAppHelpMain},
    {uOpt_get_cmd_switch(uOpt_app_sis_unif_ski_lmrk_rs_rc),
     &uAppSisUnifSkiLmrkRsRcMain}};

/**
 * Wrapper for executing main applications
 */
static int uRunApps(const uCmdOptsMap& cmd_opts) {
    //******************************
    // Main application starts here:
    //******************************
    U_CONDITIONAL_RUN_APPS(cmd_opts, GApps);
    // If we reach here, we didn't find an application to run
    return uExitCode_error;
}

//****************************************************************************
// Main
//****************************************************************************

int main(const int argc, const char** argv) {
    // Force program duration to be reported
    U_FORCE_SCOPED_INTERVAL_LOGGER(uSTAT_TotalExecInterval);

    // Format command line options into <key, value> mappings
    uCmdOptsMap cmd_opts;
    if (!cmd_opts.parse(argc, argv)) {
        printf("Unrecoverable error - unable to parse command line options.\n");
        return uExitCode_error;
    }

    // Commandlets!
    U_CONDITIONAL_RUN_COMMANDLETS(cmd_opts);

    // Tests!
    U_CONDITIONAL_RUN_TESTS(cmd_opts);

    // Run main application(s)
    return uRunApps(cmd_opts);
}
