//****************************************************************************
// uSisEllipsoidNuclearMixin.h
//****************************************************************************

/**
 * A nuclear mixin manages the details for checking candidate nodes to see if
 * they lie within the nuclear volume; this mixing enforces an ellipsoidal
 * nuclear confinement.
 *
 * An accessory structure is the simulation level nuclear mixin which defines
 * global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 */

#ifndef uSisEllipsoidNuclearMixin_h
#define uSisEllipsoidNuclearMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisEllipsoidNuclearSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers.
 */
template <typename t_SisGlue>
class uSisEllipsoidNuclearMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Indicate we are not a spherical nucleus
     */
    enum { is_sphere_nucleus = false };

    /**
     * Initializes nuclear constraint mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Checks if seed can satisfy nuclear confinement constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE if seed can satisfy constraints, uFALSE o/w
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) const {
        uAssert(nfo.p_node_center);

        // Check if point is within ellipsoid volume
        // https://en.wikipedia.org/wiki/Ellipsoid
        const uReal x2 =
            nfo.p_node_center->at(uDim_X) * nfo.p_node_center->at(uDim_X);
        const uReal y2 =
            nfo.p_node_center->at(uDim_Y) * nfo.p_node_center->at(uDim_Y);
        const uReal z2 =
            nfo.p_node_center->at(uDim_Z) * nfo.p_node_center->at(uDim_Z);
        const uReal inv_rsq_x =
            sim.get_allowed_nuclear_inv_sq_radius(uDim_X, nfo.node_radius, sim);
        uAssert(inv_rsq_x >= U_TO_REAL(0.0));
        const uReal inv_rsq_y =
            sim.get_allowed_nuclear_inv_sq_radius(uDim_Y, nfo.node_radius, sim);
        uAssert(inv_rsq_y >= U_TO_REAL(0.0));
        const uReal inv_rsq_z =
            sim.get_allowed_nuclear_inv_sq_radius(uDim_Z, nfo.node_radius, sim);
        uAssert(inv_rsq_z >= U_TO_REAL(0.0));
        const uReal test =
            (x2 * inv_rsq_x) + (y2 * inv_rsq_y) + (z2 * inv_rsq_z);

        // @TODO - THIS ONLY NEEDS TO BE STORED IF LAMINA INTERACTION
        //  CONSTRAINTS NEED TO BE TESTED FOR SATISFACTION, use compile time
        //  enum branch
        uMatrix& schur_buffer = sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(schur_buffer.n_rows));
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_X) = x2;
        schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Y) = y2;
        schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Z) = z2;

        // Implicit surface test
        return test <= U_TO_REAL(1.0);
    }

    /**
     * Determines which positions should be considered for growing chain from
     * parameter node. These positions have not been checked for nuclear
     * constraint satisfaction or self-avoidance.
     * @param out_candidate_positions - Output unfiltered 3-D positions of
     *  each candidate node center
     * @param parent_node_id - Node to extend chromatin chain from
     * @param candidate_radius - Radius of each candidate node, assumes all
     *  candidates have same radius.
     * @param parent_radius - Radius of the parent node
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void calc_candidate_positions(uMatrix& out_candidate_positions,
                                         const uUInt parent_node_id,
                                         const uReal candidate_radius,
                                         const uReal parent_radius,
                                         const sample_t& sample,
                                         const sim_t& sim
                                             U_THREAD_ID_PARAM) const {
        const uMatrix& node_positions = sample.get_node_positions();
        // Assuming column-wise point storage for node positions
        uAssert(node_positions.n_rows == uDim_num);
        uAssert(node_positions.size() > 0);
        uAssertBounds(parent_node_id, 0, node_positions.n_cols);
        uSisUtils::calc_candidate_positions(
            out_candidate_positions,
            node_positions.colptr(parent_node_id),
            candidate_radius,
            parent_radius,
            sim.get_unit_sphere_sample_points());
    }

    /**
     * Determines which positions fit within nuclear volume
     * @param out_legal_candidates - Output integer [0,1] array where i-th
     *  element is 1 if i-th candidate node position would satisfy nuclear
     *  volume constraint and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test if they are within nucleus.
     * @param candidate_radius - Radius of each candidate, assumes all
     *  candidates have same radius.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void nuclear_filter(uBoolVecCol& out_legal_candidates,
                               const uMatrix& candidate_centers,
                               const uReal& candidate_radius,
                               const sample_t& sample,
                               const sim_t& sim U_THREAD_ID_PARAM) const {
        // Assuming non-empty matrix with positions in rows
        uAssert(candidate_centers.size() > 0);
        uAssert(candidate_centers.n_cols == uDim_num);

        // Defer to simulation level mixin for buffers

        // Obtain reference to buffer for storing element-wise multiplication
        uMatrix& schur_buffer = sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssert(schur_buffer.n_cols == candidate_centers.n_cols);
        uAssert(schur_buffer.n_rows == candidate_centers.n_rows);
        uAssert(schur_buffer.size() == candidate_centers.size());
        uAssert(schur_buffer.n_cols == uDim_num);

        // Obtain reference to buffer for storing dot product
        uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssert(dot_buffer.n_rows == schur_buffer.n_rows);
        uAssert(dot_buffer.n_rows == candidate_centers.n_rows);

        // https://en.wikipedia.org/wiki/Ellipsoid
        // Implicit equation of ellipsoid:
        //
        //  f(x,y,z) = x^2/rad_x^2 + y^2/rad_y^2 + z^2/rad_z^2
        //
        // If f(x,y,z) == 1, then point (x,y,z) is on ellipsoid surface
        // If f(x,y,z) < 1, then point (x,y,z) is within interior volume
        // If f(x,y,z) > 1, then point (x,y,z) is exterior to volume
        //
        // We can create a simple test for spherical points representing a
        // monomer node by subtracting the node's radius from each principal
        // radius defining the ellipsoid - we call this the 'allowed nuclear
        // radius' for that principal axis. We can then used these modified
        // radii instead of the original radii in the implicit equation to
        // test if some portion of the monomer sphere is external to ellipsoid
        // volume.

        // Compute the element-wise (Schur) product of candidate positions
        schur_buffer = candidate_centers % candidate_centers;
        // Scale each position coordinate by corresponding inverse square of
        // principal axis radius. Scoping to help prevent errors from
        // accidentally using the wrong temporary variable
        {
            // x-axis
            const uReal inv_rsq_x = sim.get_allowed_nuclear_inv_sq_radius(
                uDim_X, candidate_radius, sim);
            uAssert(inv_rsq_x > U_TO_REAL(0.0));
            dot_buffer = (schur_buffer.unsafe_col(uDim_X) * inv_rsq_x);
        }
        {
            // y-axis
            const uReal inv_rsq_y = sim.get_allowed_nuclear_inv_sq_radius(
                uDim_Y, candidate_radius, sim);
            uAssert(inv_rsq_y > U_TO_REAL(0.0));
            // @TODO - Verify no dynamic allocations
            dot_buffer += (schur_buffer.unsafe_col(uDim_Y) * inv_rsq_y);
        }
        {
            // x-axis
            const uReal inv_rsq_z = sim.get_allowed_nuclear_inv_sq_radius(
                uDim_Z, candidate_radius, sim);
            uAssert(inv_rsq_z > U_TO_REAL(0.0));
            // @TODO - Verify no dynamic allocations
            dot_buffer += (schur_buffer.unsafe_col(uDim_Z) * inv_rsq_z);
        }

        uAssert(out_legal_candidates.n_rows == dot_buffer.n_rows);
        uAssert(out_legal_candidates.size() == dot_buffer.size());
        uAssert(out_legal_candidates.n_elem == dot_buffer.n_elem);

        // Test to see if dot product <= 1; if so, then candidate candidate
        // node is within ellipsoid nuclear volume.
        // @TODO - make sure this gets vectorized, see:
        // https://msdn.microsoft.com/en-us/library/jj614596(v=vs.120).aspx
        const uUInt n_elem = U_TO_UINT(dot_buffer.n_elem);
        uBool* const out_legal_candidates_mem = out_legal_candidates.memptr();
        const uReal* const dot_buffer_mem = dot_buffer.memptr();
        for (uUInt i = 0; i < n_elem; ++i) {
            out_legal_candidates_mem[i] = (dot_buffer_mem[i] <= U_TO_REAL(1.0));
        }
    }
};

#endif  // uSisEllipsoidNuclearMixin_h
