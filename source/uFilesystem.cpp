//****************************************************************************
// uFilesystem.cpp
//****************************************************************************

#include "uBuild.h"
#include "uFilesystem.h"

#include <boost/version.hpp>

/**
 * Creates any directories needed for parameter path
 * @param dir_path - path to directory
 * @return result of uFs::create_directories(...) for dir_path
 */
bool uFs_create_dirs(const uFs::path& dir_path) {
    return uFs::create_directories(dir_path);
}

/**
 * Creates any parent directories needed for parameter file path
 * @param file_path - path to file (not path to directory)
 * @return result of uFs::create_directories(...) for parent directories
 */
bool uFs_create_parent_dirs(const uFs::path& file_path) {
    const uFs::path outdir = uFs::absolute(file_path.parent_path());
    return uFs::create_directories(outdir);
}

/**
 * Creates lexically normal (removes extra path separators and dots) directory
 * path without trailing path separator slash(es)
 * @param dir_path - directory path to normalize
 */
void uFs_normalize_dir_path(uFs::path& dir_path) {
    // @HACK - append non-existing file to path so that we may later resolve
    // normalized directory path using parent_path(). See discussion :
    // https://stackoverflow.com/questions/36941934/parent-path-with-or-without-trailing-slash/50493087#50493087
    dir_path /= "FILE.TXT";
    // Remove unneeded dots and slashes
#if BOOST_VERSION >= 106000
    dir_path = dir_path.lexically_normal();
#else
    // @HACK - THIS IS A DEPRECATED FUNCTION INCLUDED BY DEFAULT. NEEDED FOR
    // HELPING RESOLVE DOTS IN PATH STRING
    dir_path.normalize();
#endif  // BOOST_VERSION >= 106000
    // Remove trailing slash from original path!
    dir_path = dir_path.parent_path();
}

/**
 * Overloaded version to work with string argument
 */
void uFs_normalize_dir_path(std::string& dir_path) {
    uFs::path dir_path_(dir_path);
    uFs_normalize_dir_path(dir_path_);
    dir_path = dir_path_.string();
}
