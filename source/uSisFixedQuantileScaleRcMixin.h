//****************************************************************************
// uSisFixedQuantileScaleRcMixin.h
//****************************************************************************

/**
 * ScaleRcMixin = A mixin that defines the checkpoint scaling thresholds for
 *   rejection control algorithm. See:
 *
 * Liu, Jun S., Rong Chen, and Wing Hung Wong. "Rejection control and
 *  sequential importance sampling." Journal of the American Statistical
 *  Association 93.443 (1998): 1022-1031.
 *
 * A sample passes rejection control checkpoints with probability:
 *
 *  P = min(1, w_i / scale) where w_i is weight of i-th sample.
 *
 * This mixin defines the scale denominator for checkpoint culling.
 */

#ifndef uSisFixedQuantileScaleRcMixin_h
#define uSisFixedQuantileScaleRcMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uTypes.h"

/**
 * Given a quantile in [0,1], sets scale to be weight at associated quantile
 * This essentially means that a fixed proportion (1 - quantile) of the
 * samples automatically pass each rejection control checkpoint.
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisFixedQuantileScaleRcMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisFixedQuantileScaleRcMixin() { clear(); }

    /**
     * Initializes scale mixin
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
        // Read quantile in [0,1] from configuration file
        uReal quantile = 0.0;
        config->read_into<uReal>(quantile, uOpt_rc_scale_fixed_quantile);
        uAssert(quantile >= 0.0);
        uAssert(quantile <= 1.0);
        // Pre-size sorted weights buffer
        this->sorted_weights.zeros(config->ensemble_size);
        // Compute quantile index within sorted buffer
        this->ix_quantile = quantile * U_TO_REAL(config->ensemble_size);
        // Make sure quantile index is in legal range
        uAssert(config->ensemble_size > 0);
        this->ix_quantile = std::min(std::max(this->ix_quantile, U_TO_UINT(0)),
                                     (config->ensemble_size - 1));
    }

    /**
     * Resets to default state
     */
    inline void clear() { this->ix_quantile = U_TO_UINT(0); }

    /**
     * Determines the scale used to determine if a sample should be regrown
     * @param norm_exp_weights - the vector of exp transformed weights
     *   normalized by the max weight - should be in (0,1]
     * @param max_log_weight - the unnormalized maximal log weight
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin (must be a rejection
     *   control mixin)
     * @param sim - parent simulation
     * @return scale factor for accept/reject probability = min(1, weight/scale)
     */
    inline uReal rc_get_scale(const uVecCol& norm_exp_weights,
                              const uReal max_log_weight,
                              const uUInt num_completed_grow_steps_per_sample,
                              const sim_level_qc_mixin_t& qc,
                              const sim_t& sim) {
        // Verify arrays same size and non-empty
        uAssertPosEq(this->sorted_weights.n_elem, norm_exp_weights.n_elem);
        // Sort weights
        // @TODO - verify no dynamic allocations are happening
        this->sorted_weights = uMatrixUtils::sort(norm_exp_weights);
        // Return configured quantile
        uAssertBounds(this->ix_quantile, 0, this->sorted_weights.n_elem);
        return this->sorted_weights[this->ix_quantile];
    }

private:
    /**
     * The index of target quantile in the sorted weights vector
     */
    uUInt ix_quantile;

    /**
     * Buffer for storing sorted weights
     */
    uVecCol sorted_weights;
};

#endif  // uSisFixedQuantileScaleRcMixin_h
