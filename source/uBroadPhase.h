//****************************************************************************
// uBroadPhase.h
//****************************************************************************

/**
 * Collision detection is divided into broad and narrow phases. The purpose of
 * the broad phase is to perform efficient ruling out of large portions of the
 * potential collidable objects. Those objects which are not filtered via the
 * broad phase must then undergo detailed collision checking in the narrow
 * phase.
 *
 * This file defines which broad phase implementation to use. Client code
 * should only include this file.
 */

#ifndef uBroadPhase_h
#define uBroadPhase_h

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uTypes.h"

// Enable implementation where voxels have dynamic number of elements
#include "uBroadPhase_ElemsetGrid.h"

// Broad phase grid ideally using 'small' element identifiers
typedef uBroadPhase::ElemsetGrid<uUElemId_t> uBroadPhase_t;

#endif  // uBroadPhase_h
