//****************************************************************************
// uSisOfaCollisionMixin.h
//****************************************************************************

/**
 * A collision mixin manages the details for checking candidate nodes to see if
 * they satisfy self-avoidance (do not collide with any existing nodes)
 *
 * An accessory structure is the simulation level collision mixin which
 * define global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 * Ofa - Uses broad phase filtering and then applies a narrow phase overlap
 *  factor (allows partial overlap) collision model to enforce self-avoidance
 *  among monomer nodes
 */

#ifndef uSisOfaCollisionMixin_h
#define uSisOfaCollisionMixin_h

/**
 * Overlap factors:
 *
 * If the ratio of distance between monomer node centers to the sum of their
 * radii is less than the overlap factor (ofa) value, then the monomers are
 * colliding. See:
 *
 *  Jacobson, Matthew P., David L.Pincus, Chaya S.Rapp, Tyler JF Day, Barry
 *      Honig, David E.Shaw, and Richard A.Friesner. "A hierarchical approach
 *      to all-atom protein loop prediction." Proteins: Structure, Function,
 *      and Bioinformatics 55, no. 2 (2004) : 351 - 367.
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisOfaCollisionSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * "Typedef" for sample-level mixin
 */
template <typename t_SisGlue, typename t_BroadUtil>
class uSisOfaCollisionMixin
    : public uSisHardShellCollisionMixin<t_SisGlue, t_BroadUtil> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
};

#endif  // uSisOfaCollisionMixin_h
