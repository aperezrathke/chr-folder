//****************************************************************************
// uStreamUtils.cpp
//****************************************************************************

/**
 * Stream utilities
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uStreamUtils.h"

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Based on ZSTR example: cat input stream to output stream
 * @param from - input stream
 * @param to - output stream
 */
void uStreamUtils::cat(std::istream& from, std::ostream& to) {
    // From http://zlib.net/zlib_how.html
    //
    // If the memory is available, buffers sizes on the order of 128K or 256K
    // bytes should be used.
    //
    // Note: 1 << 18 = 256 KB
    const std::streamsize BUFF_SIZE = 1 << 18;
    char BUFF[BUFF_SIZE];
    while (true) {
        from.read(BUFF, BUFF_SIZE);
        const std::streamsize CNT = from.gcount();
        if (CNT == 0) {
            break;
        }
        to.write(BUFF, CNT);
    }
}
