//****************************************************************************
// uSisMkiMkoSeoMlocIntrChrSimLevelMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Mloc: Multiple loci
 *
 * Defines a general chrome-to-chrome interaction mixin supporting knock-in
 * and knock-out constraints over multiple (or single) loci.
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * and/or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisMkiMkoSeoMlocIntrChrSimLevelMixin_h
#define uSisMkiMkoSeoMlocIntrChrSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uExitCodes.h"
#include "uIntrLibDefs.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uSisIntrChrCommonBatch.h"
#include "uSisIntrChrCommonInit.h"
#include "uSisIntrChrConfig.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

#include <limits>

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisMkiMkoSeoMlocIntrChrSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Batch interactions utility
     */
    typedef uSisIntrChrCommonBatch<t_SisGlue> intr_chr_batch_util_t;

    /**
     * Default constructor
     */
    uSisMkiMkoSeoMlocIntrChrSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();

        // Load fragment spans
        uSisIntrChrConfig::load_frag(m_intr_chr_frags,
                                     config,
                                     uFALSE /*no_fail*/,
                                     sim.get_max_total_num_nodes());
        uAssert(m_intr_chr_frags.n_cols >= 1);
        uAssert(m_intr_chr_frags.n_rows == uIntrLibPairIxNum);

        // Initialize knock-in interactions
        init_intrs_chr<uSisIntrChrConfig::ki_cfg_t>(
            m_intr_chr_kin,
            m_intr_chr_loci_tables.loci_kin_intra,
            m_intr_chr_loci_tables.loci_kin_inter,
            m_intr_chr_kin_dist,
            m_intr_chr_kin_dist2,
            config,
            m_intr_chr_frags,
            sim);

        // Initialize knock-out interactions
        init_intrs_chr<uSisIntrChrConfig::ko_cfg_t>(
            m_intr_chr_ko,
            m_intr_chr_loci_tables.loci_ko_intra,
            m_intr_chr_loci_tables.loci_ko_inter,
            m_intr_chr_ko_dist,
            m_intr_chr_ko_dist2,
            config,
            m_intr_chr_frags,
            sim);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_intr_chr_frags.clear();
        m_intr_chr_kin.clear();
        m_intr_chr_ko.clear();
        m_intr_chr_kin_dist.clear();
        m_intr_chr_kin_dist2.clear();
        m_intr_chr_ko_dist.clear();
        m_intr_chr_ko_dist2.clear();
        intr_chr_batch_util_t::clear(m_intr_chr_loci_tables);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Const handle to interaction fragments
     */
    inline const uUIMatrix& get_intr_chr_frags() const {
        return m_intr_chr_frags;
    }

    /**
     * @return Const handle to knock-in interactions
     */
    inline const uUIMatrix& get_intr_chr_kin() const { return m_intr_chr_kin; }

    /**
     * @return Const handle to knock-out interactions
     */
    inline const uUIMatrix& get_intr_chr_ko() const { return m_intr_chr_ko; }

    /**
     * @param i - knock-in interaction index
     * @return [Squared] knock-in Euclidean distance threshold
     */
    inline uReal get_intr_chr_kin_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_chr_kin_dist.n_elem);
        return m_intr_chr_kin_dist.at(i);
    }
    inline uReal get_intr_chr_kin_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_chr_kin_dist2.n_elem);
        return m_intr_chr_kin_dist2.at(i);
    }

    /**
     * @param i - knock-out interaction index
     * @return [Squared] knock-out Euclidean distance threshold
     */
    inline uReal get_intr_chr_ko_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_chr_ko_dist.n_elem);
        return m_intr_chr_ko_dist.at(i);
    }
    inline uReal get_intr_chr_ko_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_chr_ko_dist2.n_elem);
        return m_intr_chr_ko_dist2.at(i);
    }

    /**
     * @return All loci interaction tables
     */
    inline const uSisIntrChrLociTables_t& get_intr_chr_loci_tables() const {
        return m_intr_chr_loci_tables;
    }

    /**
     * @return Table of within-locus knock-in interactions
     */
    inline const uSisIntrLociTable_t& get_intr_chr_loci_kin_intra() const {
        return m_intr_chr_loci_tables.loci_kin_intra;
    }

    /**
     * @return Table of between-locus knock-in interactions
     */
    inline const uSisIntrLociTable_t& get_intr_chr_loci_kin_inter() const {
        return m_intr_chr_loci_tables.loci_kin_inter;
    }

    /**
     * @return Table of within-locus knock-out interactions
     */
    inline const uSisIntrLociTable_t& get_intr_chr_loci_ko_intra() const {
        return m_intr_chr_loci_tables.loci_ko_intra;
    }

    /**
     * @return Table of between-locus knock-out interactions
     */
    inline const uSisIntrLociTable_t& get_intr_chr_loci_ko_inter() const {
        return m_intr_chr_loci_tables.loci_ko_inter;
    }

    /**
     * @return Chr-chr knock-in interaction failure budget
     */
    inline uUInt get_intr_chr_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Chr-chr knock-out interaction failure budget
     */
    inline uUInt get_intr_chr_ko_max_fail() const { return U_TO_UINT(0); }

    // Export interface

    /**
     * Copies chr-chr interaction fragments to 'out'
     */
    void get_intr_chr_frags(uUIMatrix& out) const { out = m_intr_chr_frags; }

    /**
     * Copies knock-in chr-chr interactions to 'out'
     */
    void get_intr_chr_kin(uUIMatrix& out) const { out = m_intr_chr_kin; }

    /**
     * Copies knock-out chr-chr interactions to 'out'
     */
    void get_intr_chr_ko(uUIMatrix& out) const { out = m_intr_chr_ko; }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Validates interaction fragments to make sure no interaction pairs
     * have overlapping fragments and that indices do not validate bounds,
     * exits program if overlap or boundary violation encountered. Also,
     * formats interactions such that first fragment always occurs "before"
     * second interacting fragment under SEO growth model.
     * @param intrs - 2 x N matrix where N is number of interaction pairs,
     *  each column is interaction pair, each row is column index into
     *  fragments (frags) matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param sim - parent simulation
     */
    static void format_intrs_chr_exit_on_fail(uUIMatrix& intrs,
                                              const uUIMatrix& frags,
                                              const sim_t& sim) {
        // Early out if no interactions
        if (intrs.is_empty()) {
            return;
        }
        // Format all interactions
        uAssertPosEq(intrs.n_rows, uIntrLibPairIxNum);
        uAssertPosEq(frags.n_rows, uIntrLibPairIxNum);
        for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
            uUInt& frag_a = intrs.at(uIntrLibPairIxMin, i);
            uUInt& frag_b = intrs.at(uIntrLibPairIxMax, i);
            uSisIntrChrCommonInit::format_intr_exit_on_fail(
                frag_a, frag_b, frags, sim);
        }
    }

    /**
     * Partitions interactions by involved loci
     * @param loci_intra - output interaction table for interactions occurring
     *  entirely within a single locus
     * @param loci_inter - output interaction table for interactions occurring
     *  between two different loci
     * @param intrs - 2 x N matrix where N is number of interaction pairs,
     *  each column is interaction pair, each row is column index into
     *  fragments (frags) matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param sim - parent simulation
     */
    static void partition_intrs_chr_by_loci(uSisIntrLociTable_t& loci_intra,
                                            uSisIntrLociTable_t& loci_inter,
                                            const uUIMatrix& intrs,
                                            const uUIMatrix& frags,
                                            const sim_t& sim) {
        // Reset loci tables
        loci_intra.clear();
        loci_inter.clear();
        // Early out if no interactions
        if (intrs.is_empty()) {
            return;
        }
        // If this assert trips, then index type is insufficient number of
        // bits to represent the number of interactions
        uAssert(
            static_cast<size_t>(intrs.n_cols) <=
            static_cast<size_t>(
                std::numeric_limits<uSisIntrIndexVec_t::value_type>::max()));
        // Resize loci tables
        const uUInt num_loci = sim.get_num_loci();
        uAssert(num_loci > U_TO_UINT(0));
        loci_intra.resize(num_loci);
        loci_inter.resize(num_loci);
        // Partition interactions
        uAssertPosEq(intrs.n_rows, uIntrLibPairIxNum);
        uAssertPosEq(frags.n_rows, uIntrLibPairIxNum);
        uUInt n_intra = U_TO_UINT(0);
        uUInt n_inter = U_TO_UINT(0);
        for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
            // Determine loci involved for this interaction
            const uUInt frag_a = intrs.at(uIntrLibPairIxMin, i);
            uAssertBounds(frag_a, U_TO_UINT(0), frags.n_cols);
            const uUInt frag_b = intrs.at(uIntrLibPairIxMax, i);
            uAssertBounds(frag_b, U_TO_UINT(0), frags.n_cols);
            const uUInt nid_a = frags.at(uIntrLibPairIxMax, frag_a);
            const uUInt nid_b = frags.at(uIntrLibPairIxMax, frag_b);
            const uUInt lid_a = sim.get_growth_lid(nid_a);
            uAssertBounds(lid_a, U_TO_UINT(0), num_loci);
            const uUInt lid_b = sim.get_growth_lid(nid_b);
            uAssertBounds(lid_b, U_TO_UINT(0), num_loci);
            // Case: within-locus interaction
            if (lid_a == lid_b) {
                loci_intra[lid_a].push_back(U_TO_UINT(i));
                ++n_intra;
            }
            // Case: between-loci interaction
            else {
                loci_inter[lid_a].push_back(U_TO_UINT(i));
                loci_inter[lid_b].push_back(U_TO_UINT(i));
                ++n_inter;
            }
        }
        // Clear out any empty loci tables
        if (U_TO_UINT(0) == n_intra) {
            loci_intra.clear();
        }
        if (U_TO_UINT(0) == n_inter) {
            loci_inter.clear();
        }
    }

    /**
     * Extracts masked interaction data
     */
    static void apply_intrs_chr_mask(const uBoolVecCol& mask,
                                     uUIMatrix& intrs,
                                     uVecCol& dist,
                                     uVecCol& dist2) {
        uAssert(mask.n_elem == intrs.n_cols);
        const uMatSz_t n_intrs = uMatrixUtils::sum(mask);
        uAssertBoundsInc(n_intrs, U_TO_MAT_SZ_T(0), intrs.n_cols);
        uUIMatrix temp_intrs;
        uVecCol temp_dist;
        uVecCol temp_dist2;
        // Extract masked data
        if (n_intrs > U_TO_UINT(0)) {
            temp_intrs.set_size(uIntrLibPairIxNum, n_intrs);
            temp_dist.set_size(n_intrs);
            temp_dist2.set_size(n_intrs);
            uUInt dst = U_TO_UINT(0);
            for (uMatSz_t src = 0; src < mask.n_elem; ++src) {
                const uBool bit = mask.at(src);
                if (bit) {
                    uAssertBounds(dst, U_TO_UINT(0), temp_intrs.n_cols);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist.n_elem);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist2.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), intrs.n_cols);
                    uAssertBounds(src, U_TO_UINT(0), dist.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), dist2.n_elem);
                    temp_intrs.at(uIntrLibPairIxMin, dst) =
                        intrs.at(uIntrLibPairIxMin, src);
                    temp_intrs.at(uIntrLibPairIxMax, dst) =
                        intrs.at(uIntrLibPairIxMax, src);
                    temp_dist.at(dst) = dist.at(src);
                    temp_dist2.at(dst) = dist2.at(src);
                    ++dst;
                }
            }
            uAssert(dst == n_intrs);
        }
        // Overwrite buffers
        uAssert(temp_intrs.n_cols <= intrs.n_cols);
        uAssert(temp_intrs.n_rows == intrs.n_rows);
        uAssert(temp_dist.n_elem <= dist.n_elem);
        uAssert(temp_dist2.n_elem <= dist2.n_elem);
        uAssert(temp_dist2.n_elem == temp_dist.n_elem);
        uAssert(temp_intrs.n_cols == temp_dist.n_elem);
        intrs = temp_intrs;
        dist = temp_dist;
        dist2 = temp_dist2;
    }

    /**
     * Initialize all data associated with an interaction type
     */
    template <typename t_cfg>
    static void init_intrs_chr(uUIMatrix& intrs,
                               uSisIntrLociTable_t& loci_intra,
                               uSisIntrLociTable_t& loci_inter,
                               uVecCol& dist,
                               uVecCol& dist2,
                               uSpConstConfig_t config,
                               const uUIMatrix& frags,
                               const sim_t& sim) {
        // This method should not be called if no fragments exist
        uAssert(frags.n_cols >= 1);
        uAssert(frags.n_rows == uIntrLibPairIxNum);

        // Load interactions
        if (!t_cfg::load(intrs, config, uFALSE /*no_fail*/)) {
            uLogf("Warning: no %s chr-chr interactions loaded.\n",
                  t_cfg::get_name());
            intrs.clear();
            return;
        }
        uAssert(intrs.n_cols >= 1);
        uAssert(intrs.n_rows == uIntrLibPairIxNum);

        // Initialize interaction distances
        t_cfg::get_dist(dist, intrs.n_cols, config);
        uAssertPosEq(dist.n_elem, intrs.n_cols);
        dist2.set_size(dist.n_elem);
        for (uMatSz_t i = 0; i < dist.n_elem; ++i) {
            uSisIntrChrCommonInit::init_dists(dist.at(i), dist2.at(i), sim);
        }

        // Ensure interactions are disjoint and formatted properly
        format_intrs_chr_exit_on_fail(intrs, frags, sim);

        // Read interactions mask
        uBoolVecCol mask;
        t_cfg::get_mask(mask, intrs.n_cols, config);
        uAssert(mask.n_elem == intrs.n_cols);

        // Apply mask
        apply_intrs_chr_mask(mask, intrs, dist, dist2);

        // Partition interactions by loci
        partition_intrs_chr_by_loci(loci_intra, loci_inter, intrs, frags, sim);

        // Compute mean interaction constraint distance (avoid divide-by-zero)
        double mean_dist = 0.0;
        if (intrs.n_cols > U_TO_MAT_SZ_T(0)) {
            mean_dist = (double)uMatrixUtils::mean(dist);
        }
        // Report initialized interaction state
        uAssert(mean_dist >= 0.0);
        uAssert(intrs.n_cols == dist.n_elem);
        uAssert(intrs.n_cols == dist2.n_elem);
        uLogf(
            "Initialized %d %s chr-chr interaction constraints.\n\t-<%s dist> "
            "(may be padded): %f.\n",
            (int)intrs.n_cols,
            t_cfg::get_name(),
            t_cfg::get_name(),
            mean_dist);
    }

    /**
     * Fragment spans
     */
    uUIMatrix m_intr_chr_frags;

    /**
     * Knock-in interactions
     */
    uUIMatrix m_intr_chr_kin;

    /**
     * Knock-out interactions
     */
    uUIMatrix m_intr_chr_ko;

    /**
     * At least one node in first fragment can be no farther than this
     * distance from a node in the second fragment for a knock-in interaction.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_chr_kin_dist;

    /**
     * Squared distance proximity threshold.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_chr_kin_dist2;

    /**
     * All nodes in first fragment must be greater than this distance from all
     * nodes in second fragment for a knock-out interaction.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_chr_ko_dist;

    /**
     * Squared knock-out distance threshold.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_chr_ko_dist2;

    /**
     * Collection of interactions partitioned by loci
     */
    uSisIntrChrLociTables_t m_intr_chr_loci_tables;
};

#endif  // uSisMkiMkoSeoMlocIntrChrSimLevelMixin_h
