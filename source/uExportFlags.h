//****************************************************************************
// uExportFlags.h
//****************************************************************************

/**
 * Defines the flags specifying what to export and in what format(s)
 */

#ifndef uExportFlags_h
#define uExportFlags_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

//****************************************************************************
// Export Flags
//****************************************************************************

/**
 * The bits denoting export formats (and or additional data to export)
 * @TODO - consider 'bulk' export flags and/or binary export flags
 */
enum uExportBits {
    uExportBitCsv = 0,
    uExportBitCsvGz,
    uExportBitCsvBulk,
    uExportBitCsvBulkGz,
    uExportBitPdb,
    uExportBitPdbGz,
    uExportBitPdbBulk,
    uExportBitPdbBulkGz,
    uExportBitPml,
    uExportBitPmlGz,
    uExportBitPmlBulk,
    uExportBitPmlBulkGz,
    uExportBitLogWeights,
    uExportBitExtended,
    uExportBitNucleus,
    uExportBitIntrChr,
    uExportBitIntrLam,
    uExportBitIntrNucb,
    uExportBitLigcCsv,
    uExportBitLigcCsvGz,
    uExportBitLigcCsvBulk,
    uExportBitLigcCsvBulkGz,
    uExportBitLigcBin,
    uExportBitLigcBonded,
    uExportBitNum
};

/**
 * The flags to test to see if an export configuration is enabled.
 * e.g. flags | uExportCsv, if this is true, then CSV export is enabled.
 */
enum uExportFlags {
    // Export individual CSV coordinate files for each sample
    uExportCsv = 1 << uExportBitCsv,
    // Export compressed CSV coordinates
    uExportCsvGz = 1 << uExportBitCsvGz,
    // Export uncompressed, bulk CSV coordinate files
    uExportCsvBulk = 1 << uExportBitCsvBulk,
    // Export compressed, bulk CSV coordinate files
    uExportCsvBulkGz = 1 << uExportBitCsvBulkGz,
    // Export individual PDB coordinate files for each sample
    uExportPdb = 1 << uExportBitPdb,
    // Export compressed PDB coordinates
    uExportPdbGz = 1 << uExportBitPdbGz,
    // Export uncompressed, bulk PDB coordinate files
    uExportPdbBulk = 1 << uExportBitPdbBulk,
    // Export compressed, bulk CSV coordinate files
    uExportPdbBulkGz = 1 << uExportBitPdbBulkGz,
    // Export individual PML coordinate files with embedded script for
    // rendering in PyMol
    uExportPml = 1 << uExportBitPml,
    // Export compressed PML coordinates
    uExportPmlGz = 1 << uExportBitPmlGz,
    // Export uncompressed, bulk PML coordinate files
    uExportPmlBulk = 1 << uExportBitPmlBulk,
    // Export compressed, bulk PML coordinate files
    uExportPmlBulkGz = 1 << uExportBitPmlBulkGz,
    // Exports log weights as a separate file (single column)
    uExportLogWeights = 1 << uExportBitLogWeights,
    // Export extended details like radii, chain identifiers
    uExportExtended = 1 << uExportBitExtended,
    // Export nuclear shell
    uExportNucleus = 1 << uExportBitNucleus,
    // Export each sample's chr-chr (knock-in and/or knock-out) interactions
    uExportIntrChr = 1 << uExportBitIntrChr,
    // Export each sample's lam-chr (knock-in and/or knock-out) interactions
    uExportIntrLam = 1 << uExportBitIntrLam,
    // Export each sample's nucb-chr (knock-in and/or knock-out) interactions
    uExportIntrNucb = 1 << uExportBitIntrNucb,
    // Export ligations as CSV tuples
    uExportLigcCsv = 1 << uExportBitLigcCsv,
    // Export ligations as compressed CSV tuples
    uExportLigcCsvGz = 1 << uExportBitLigcCsvGz,
    // Export ligations as bulk CSV tuples
    uExportLigcCsvBulk = 1 << uExportBitLigcCsvBulk,
    // Export ligations as bulk compressed CSV tuples
    uExportLigcCsvBulkGz = 1 << uExportBitLigcCsvBulkGz,
    // Export ligations as binary format
    uExportLigcBin = 1 << uExportBitLigcBin,
    // Export self (i,i) and bonded (i,i+1) ligation tuples
    uExportLigcBonded = 1 << uExportBitLigcBonded
};

#endif  // uExportFlags_h
