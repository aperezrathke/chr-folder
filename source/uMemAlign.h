//****************************************************************************
// uMemAlign.h
//****************************************************************************

/**
 * Portable utilities for (de)allocating aligned memory.
 *
 * Usage:
 *  u_aligned_malloc(...) allocates aligned memory
 *  u_aligned_free(...) deallocates aligned memory
 *
 * Memory allocated with u_aligned_malloc must be freed with u_aligned_free!
 */

#ifndef uMemAlign_h
#define uMemAlign_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"

//****************************************************************************
// Macros
//****************************************************************************

#ifdef U_BUILD_ENABLE_BOOST_MEM_ALIGN
/**
 * Boost alignment routines
 */
#include <boost/align/aligned_alloc.hpp>

    #define u_aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)boost::alignment::aligned_alloc(align__, size__)

    #define u_aligned_free(ptr__) boost::alignment::aligned_free(ptr__)

#elif defined(U_BUILD_COMPILER_MSVC)
    /**
     * MSCV alignment routines
     */
    #include <malloc.h>

    #define u_aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)_aligned_malloc(size__, align__)

    #define u_aligned_free(ptr__) _aligned_free(ptr__)

#elif defined(U_BUILD_COMPILER_ICC)
    /**
     * Intel compiler alignment routines
     */
    #define u_aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)_mm_malloc(size__, align__)

    #define u_aligned_free(ptr__) _mm_free(ptr__)

#elif defined(U_BUILD_COMPILER_GCC)
    /**
     * GCC (POSIX) alignment routines
     */
    #include <stdlib.h>

    #define u_aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        do {                                                           \
            void** pptr__ = (void**)(&(ptr__));                        \
            posix_memalign(pptr__, align__, size__);                   \
        } while (0)

    #define u_aligned_free(ptr__) free(ptr__);

#else
    /**
     * Platform not found!
     */
    #error Unrecognized platform
#endif  // U_BUILD_ENABLE_BOOST_MEM_ALIGN

#define u_is_aligned(ptr__, align__) \
    (((uintptr_t)(const void*)(ptr__)) % (align__) == 0)

/**
 * u_aligned_malloc
 *  @param ptr__ - Pointer to be assigned with allocated, aligned memory
 *  @param size__ - Number of bytes to allocate
 *  @param align__ - Target memory alignment in bytes
 *  @param type__ - The target type that pointer is pointing to
 *
 * Allocated memory must be freed using u_aligned_free(ptr__)
 */
#define u_aligned_malloc(ptr__, size__, align__, type__)            \
    do {                                                            \
        u_aligned_malloc_unchecked(ptr__, size__, align__, type__); \
        uAssert(u_is_aligned(ptr__, align__));                      \
    } while (0)

#endif  // uMemAlign_h
