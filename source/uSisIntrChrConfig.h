//****************************************************************************
// uSisIntrChrConfig.h
//****************************************************************************

/**
 * Utilities for parsing and loading chr-chr interaction constraint config
 */

#ifndef uSisIntrChrConfig_h
#define uSisIntrChrConfig_h

#include "uBuild.h"
#include "uConfig.h"
#include "uIntrLibDefs.h"
#include "uTypes.h"

/**
 * Utilities for querying and loading user configuration for options:
 *  - uOpt_chr_frag: Specifies path to plain-text fragments file. The file
 *      specifies the start and end node indices (0-based) for genomic regions
 *      (i.e. fragments) that may be involved in spatial interactions (either
 *      knock-in or knock-out). The expected format is:
 *
 *          <frag_start_index><sep><frag_end_index>
 *          ...
 *          <frag_start_index><sep><frag_end_index>
 *
 *      where <frag_*index> is a 0-based integer index such that the span:
 *
 *          [start index, end index] with start index <= end index
 *
 *      is inclusive and defines the consecutive nodes that constitute the
 *      fragment. Hence, each line of the file defines the inclusive node span
 *      of a separate fragment (that may overlap and/or differ in length from
 *      other fragments). The <sep> field separates integer indices and can be
 *      any length set of consecutive non-numeric characters. Therefore, <sep>
 *      formats such as comma-separated or white space delimited are both
 *      acceptable. Here is an example file in white space delimited format:
 *
 *          <line 0>: 0 4
 *          <line 1>: 5 8
 *          <line 2>: 9 12
 *          <line 3>: 13 13
 *
 *      Line 0 defines a genomic fragment from polymer nodes 0 to 4; this
 *      fragment is 5 nodes in length and includes nodes 0, 1, 2, 3 and 4. The
 *      next line (1) similarly defines a fragment spanning nodes 5 to 8. The
 *      next line (2) defines a fragment spanning nodes 9 to 12. The last line
 *      (3) defines a short fragment consisting of only node 13. Again, the
 *      fragments do not have to be the same length and they may overlap. This
 *      option is required if either uOpt_chr_knock_in or uOpt_chr_knock_out
 *      arguments are specified.
 *
 * - uOpt_chr_knock_in: Optional path to knock-in constraints file. Knock-in
 *      constraints are pairs of 0-based indices into the fragments file (see
 *      uOpt_chr_frag). Each index represents a line in the fragments file; in
 *      turn each fragments file line defines the nodes in the fragment. Each
 *      pair of indices defines genomic regions (fragments) constrained
 *      (i.e. "knocked-in") to be spatially proximal in each generated polymer
 *      sample. See uOpt_chr_knock_in_dist option for how to specify the max
 *      distance between fragments constrained to be spatially proximal. The
 *      format is the same as the fragments file but instead represents the
 *      line indices into the fragments file rather than node indices. An
 *      example file:
 *
 *          <line 0>: 0 1
 *          <line 1>: 0 2
 *          <line 2>: 1 2
 *
 *      represents the constraints fragment 0 is spatially proximal to
 *      fragment 1 (line 0), fragment 0 is spatially proximal to fragment
 *      2 (line 1), and fragment 1 is spatially proximal to fragment 2. This
 *      option requires uOpt_chr_frag to be specified.
 *
 * - uOpt_chr_knock_out: Optional path to knock-out constraints file. Knock-out
 *      constraints are pairs of 0-based indices into the fragments file (see
 *      uOpt_chr_frag). Each index represents a line in the fragments file; in
 *      turn each fragments file line defines the nodes in the fragment. Each
 *      pair of indices defines genomic regions (fragments) constrained
 *      (i.e. "knocked-out") to be spatially distal in each generated polymer
 *      sample. See uOpt_chr_knock_out_dist option for how to specify the min
 *      distance between fragments constrained to be spatially distal. The
 *      format is the same as the fragments file but instead represents the
 *      line indices into the fragments file rather than node indices. An
 *      example file:
 *
 *          <line 0>: 0 3
 *          <line 1>: 1 3
 *
 *      represents the constraints fragment 0 is spatially distal to fragment
 *      3 (line 0), and fragment 1 is spatially distal to fragment 3 (line 1).
 *      This option requires uOpt_chr_frag to be specified.
 *
 * - uOpt_chr_knock_in_dist: Optional spatial proximity distance in Angstroms.
 *      All knock-in fragment pairs are constrained such that at least two
 *      nodes, one in each knock-in fragment pair, are within this distance
 *      from each other. One possible default is 1000 Angstroms as reported:
 *
 *          Giorgetti, Luca, and Edith Heard. "Closing the loop: 3C versus DNA
 *           FISH." Genome biology 17.1 (2016): 215
 *
 *          NOTE: In above review, Giorgetti actually cites one of his earlier
 *          Cell papers which reports an optimal cross-linking distance of 795
 *          Angstroms, see:
 *
 *          Giorgetti, Luca, et al. "Predictive polymer modeling reveals
 *           coupled fluctuations in chromosome conformation and
 *           transcription." Cell 157.4 (2014): 950-963.
 *
 * - uOpt_chr_knock_in_dist_fpath: If modeling heterogeneous KI constraints
 *      (e.g. 3-D FISH), then this specifies a path to a run-length encoded
 *      file with a distance at each knock-in interaction. If found, then
 *      uOpt_chr_knock_in_dist argument is ignored.
 *
 * - uOpt_chr_knock_out_dist: Optional spatial distance in Angstroms. All
 *      knock-out fragment pairs are constrained such that their minimum node
 *      separation distance (center-to-center) is greater than this value.
 *
 * - uOpt_chr_knock_out_dist_fpath: If modeling heterogeneous KO constraints
 *      (e.g. 3-D FISH), then this specifies a path to a run-length encoded
 *      file with a distance at each knock-out interaction. If found, then
 *      uOpt_chr_knock_out_dist argument is ignored.
 *
 * - uOpt_chr_knock_in_bit_mask, uOpt_chr_knock_in_bit_mask_fpath,
 *      uOpt_chr_knock_in_index_mask, uOpt_chr_knock_in_index_mask_path,
 *      uOpt_chr_knock_out_bit_mask, uOpt_chr_knock_out_bit_mask_fpath,
 *      uOpt_chr_knock_out_index_mask, uOpt_chr_knock_out_index_mask_path:
 *      Various ways of specifying optional masks over the set of interactions.
 *      Only interactions specified by the mask will be enforced. A bit mask is
 *      a 0|1 string (all characters not 0|1 are ignored) which must be same
 *      size as parsed interaction pairs. An index mask is a delimited set
 *      of integer indices into the interaction pairs matrix; the delimiter
 *      is any series of non-integer characters. The priority for which mask
 *      is used when multiple masks are specified is as follows:
 *          bit_mask (cmd) > bit_mask_fpath (cmd) > index_mask (cmd)
 *              > index_mask_fpath (cmd) > bit_mask (INI)
 *                  > bit_mask_fpath (INI) > index_mask (INI)
 *                      > index_mask_fpath (INI)
 */
namespace uSisIntrChrConfig {
/**
 * Loads fragments file from disk. Path to file is set via uOpt_chr_frag user
 *  parameter. Each line of the fragments file specifies the [start, end]
 *  inclusive node index range defining a single chromatin fragment that may
 *  serve as one half of a contact constraint (knock-in or knock-out). If file
 *  not loaded and no_fail is FALSE, then will create a fragment for each
 *  individual monomer.
 * @param fragments - 2 x NUM_FRAGS matrix (method will properly size) for
 *  storing loaded fragments. Each fragment is stored as a column with
 *  lower node index in the uIntrLibPairIxMin row and upper node index
 *  (inclusive) in the uIntrLibPairIxMax row.
 * @WARNING - fragments matrix is cleared even if no fragments loaded
 * @param config - User configuration
 * @param no_fail - If uTRUE, reports error and exits if loading failed
 * @param num_mon - Number of chromatin monomers (nodes) present in simulation
 */
extern void load_frag(uUIMatrix& fragments,
                      uSpConstConfig_t config,
                      const uBool no_fail,
                      const uUInt num_mon);

/**
 * Loads knock-in constraints file from disk. Path to file is set via
 *  uOpt_chr_knock_in user parameter. Each line of the file specifies the
 *  fragments which may be constrained to be within uOpt_chr_knock_in_dist
 *  Angstroms from each other.
 * @param constraints - 2 x NUM_CONSTRAINTS matrix (method will properly
 *  size) for storing loaded constraints. Each constraint is stored as a
 *  column with lesser fragment index in the uIntrLibPairIxMin row and
 *  greater fragment index in the uIntrLibPairIxMax row.
 * @WARNING - constraints matrix is cleared even if nothing loaded
 * @param config - User configuration
 * @param no_fail - If uTRUE, reports error and exits if loading failed
 * @return uTRUE if constraints successfully loaded, uFALSE o/w
 */
extern uBool load_knock_in(uUIMatrix& constraints,
                           uSpConstConfig_t config,
                           const uBool no_fail = uTRUE);

/**
 * Loads knock-out constraints file from disk. Path to file is set via
 *  uOpt_chr_knock_out user parameter. Each line of the file specifies the
 *  fragments which may be constrained to be farther than
 *  uOpt_chr_knock_out_dist Angstroms from each other.
 * @param constraints - 2 x NUM_CONSTRAINTS matrix (method will properly
 *  size) for storing loaded constraints. Each constraint is stored as a
 *  column with lesser fragment index in the uIntrLibPairIxMin row and
 *  greater fragment index in the uIntrLibPairIxMax row.
 * @WARNING - constraints matrix is cleared even if nothing loaded
 * @param config - User configuration
 * @param no_fail - If uTRUE, reports error and exits if loading failed
 * @return uTRUE if constraints successfully loaded, uFALSE o/w
 */
extern uBool load_knock_out(uUIMatrix& constraints,
                            uSpConstConfig_t config,
                            const uBool no_fail = uTRUE);

/**
 * Checks user configuration for RLE encoded knock-in distances at each
 * interaction. If not found, defaults to scalar version of
 * get_knock_in_dist() assigned to each interaction.
 * @param out_dist - Output vector of knock-in distances of size 'num_intr'
 * @param num_intr - Number of knock-in interactions
 * @param config - User configuration
 */
extern void get_knock_in_dist(uVecCol& out_dist,
                              const uMatSz_t num_intr,
                              uSpConstConfig_t config);

/**
 * Checks user configuration for scalar knock-in threshold distance in
 *  Angstroms. Specifies the maximal distance that enforced knock-in fragment
 *  constraints may be from each other.
 * @param config - User configuration
 * @return uOpt_chr_knock_in_dist or default (set in uSisInteractionsDefs.h)
 *  if not specified
 */
extern uReal get_knock_in_dist(uSpConstConfig_t config);

/**
 * Checks user configuration for RLE encoded knock-out distances at each
 * interaction. If not found, defaults to scalar version of
 * get_knock_out_dist() assigned to each interaction.
 * @param out_dist - Output vector of knock-out distances of size 'num_intr'
 * @param num_intr - Number of knock-out interactions
 * @param config - User configuration
 */
extern void get_knock_out_dist(uVecCol& out_dist,
                               const uMatSz_t num_intr,
                               uSpConstConfig_t config);

/**
 * Checks user configuration for scalar knock-out threshold distance in
 *  Angstroms. Enforced knock-out fragment constraints must be separated by
 *  more than this value.
 * @param config - User configuration
 * @return uOpt_chr_knock_out_dist or default (set in uSisInteractionsDefs.h)
 *  if not specified
 */
extern uReal get_knock_out_dist(uSpConstConfig_t config);

/**
 * @param mask - Output boolean mask with i-th entry TRUE if i-th knock-in
 *  interaction should be modeled, FALSE o/w
 * @param num_intr - Number of interactions, output mask size
 * @param config - User configuration
 * @WARNING - mask will always be cleared
 */
extern void get_knock_in_mask(uBoolVecCol& mask,
                              const uMatSz_t num_intr,
                              uSpConstConfig_t config);

/**
 * @param mask - Output boolean mask with i-th entry TRUE if i-th knock-out
 *  interaction should be modeled, FALSE o/w
 * @param num_intr - Number of interactions, output mask size
 * @param config - User configuration
 * @WARNING - mask will always be cleared!
 */
extern void get_knock_out_mask(uBoolVecCol& mask,
                               const uMatSz_t num_intr,
                               uSpConstConfig_t config);

/**
 * Knock-in configuration interface
 */
typedef struct {
    // See load_knock_in(...)
    static uBool load(uUIMatrix& constraints,
                      uSpConstConfig_t config,
                      const uBool no_fail = uTRUE) {
        return load_knock_in(constraints, config, no_fail);
    }
    // See vector get_knock_in_dist(...)
    static void get_dist(uVecCol& out_dist,
                         const uMatSz_t num_intr,
                         uSpConstConfig_t config) {
        get_knock_in_dist(out_dist, num_intr, config);
    }
    // See scalar get_knock_in_dist(...)
    static uReal get_dist(uSpConstConfig_t config) {
        return get_knock_in_dist(config);
    }
    // @return Default scalar distance
    static uReal get_dist_default() { return uIntrChrDefaultKnockInDist; }
    // See get_knock_in_mask(...)
    static void get_mask(uBoolVecCol& mask,
                         const uMatSz_t num_intr,
                         uSpConstConfig_t config) {
        get_knock_in_mask(mask, num_intr, config);
    }
    // @return data name
    static const char* get_name() { return uIntrChrKnockInDataName; }
} ki_cfg_t;

/**
 * Knock-out configuration interface
 */
typedef struct {
    // See load_knock_out(...)
    static uBool load(uUIMatrix& constraints,
                      uSpConstConfig_t config,
                      const uBool no_fail = uTRUE) {
        return load_knock_out(constraints, config, no_fail);
    }
    // See vector get_knock_out_dist(...)
    static void get_dist(uVecCol& out_dist,
                         const uMatSz_t num_intr,
                         uSpConstConfig_t config) {
        get_knock_out_dist(out_dist, num_intr, config);
    }
    // See scalar get_knock_out_dist(...)
    static uReal get_dist(uSpConstConfig_t config) {
        return get_knock_out_dist(config);
    }
    // @return Default scalar distance
    static uReal get_dist_default() {
        return uIntrChrDefaultKnockInDist * uIntrChrDefaultKnockOutDistScale;
    }
    // See get_knock_out_mask(...)
    static void get_mask(uBoolVecCol& mask,
                         const uMatSz_t num_intr,
                         uSpConstConfig_t config) {
        get_knock_out_mask(mask, num_intr, config);
    }
    // @return data name
    static const char* get_name() { return uIntrChrKnockOutDataName; }
} ko_cfg_t;

}  // namespace uSisIntrChrConfig

#endif  // uSisIntrChrConfig_h
