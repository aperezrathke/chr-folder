//****************************************************************************
// uSisSelectDelphicPower.h
//****************************************************************************

#ifndef uSisSelectDelphicPower_h
#define uSisSelectDelphicPower_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uSisSampleCopier_unsafe.h"
#include "uSisSelectPower.h"
#include "uSisSubSimAccess.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

/**
 * Selects a single sample among a set of samples with probability
 * proportional to its relative weight raised to a user-defined power
 * (alpha). Each sample weight is based on the sample's current weight
 * modulated by the estimated summary (e.g. mean or median) weight from a
 * user-defined look-ahead ensemble.
 */
template <
    // Core simulation structures
    typename t_SisGlue,
    // Summarizes log weights at look-ahead ensembles to a scalar
    // e.g. log(mean weight), log(median weight)
    typename t_Summary,
    // Threaded or serial sub-simulation access
    typename t_DelphicSubSimAccess,
    // Defines method to(T1 from_, T2 to_) which
    // converts sample of type T1 to sample of type T2
    // See uSisSampleCopier_unsafe
    typename t_DelphicSampleCopyier,
    // Configuration key for look-ahead steps
    // e.g. uOpt_delphic_steps_slot0
    enum uOptE e_Key_steps,
    // Configuration key for power exponent 'alpha'
    // e.g uOpt_delphic_power_alpha_slot0
    enum uOptE e_Key_alpha>
class uSisSelectDelphicPower {
public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

private:
    /**
     * Typedefs
     */
    typedef t_Summary uDelphicSummary;
    typedef t_DelphicSubSimAccess uDelphicSubSimAccess;
    typedef typename uDelphicSubSimAccess::sim_t uDelphicSubSim;
    typedef typename uDelphicSubSim::sample_t uDelphicSubSample;
    typedef t_DelphicSampleCopyier uDelphicSampleCopyier;
    typedef uSisSelectPower<t_SisGlue, e_Key_alpha> uDelphicPowUtil;

public:
    // Interface expected by landmark trial runner

    /**
     * Initializes sampler from config object
     * @param sim - parent simulation
     * @param cfg - configuration object specifying simulation parameters
     */
    void init(const sim_t& sim, uSpConstConfig_t cfg U_THREAD_ID_PARAM) {
        this->clear();
        ////////////////////////////////////////////////////////////
        // Initialize look-ahead steps
        cfg->read_into(this->m_steps, e_Key_steps);
        // Warn user if Delphic step count is 0
        if (this->m_steps == U_TO_UINT(0)) {
            uLogf("Warning: Delphic power selection steps == 0.\n");
        }
        // Warn user if large Delphic step count used
        else if (this->m_steps > U_SIS_DELPHIC_WARN_STEPS_THRESH) {
            uLogf("Warning: Delphic power selection steps > %d.\n",
                  (int)U_SIS_DELPHIC_WARN_STEPS_THRESH);
        }
        ////////////////////////////////////////////////////////////
        // Initialize alpha exponent (power weight smoothing util)
        m_pow_util.init(sim, cfg U_THREAD_ID_ARG);
        ////////////////////////////////////////////////////////////
        // Handle to sub-sim configuration, initialize according to:
        //      1) trial runner select child configuration
        //      2) else, trial runner child configuration
        //      3) else, default to (parent) parameter configuration
        uSpConstConfig_t sub_sim_cfg = cfg;
        // Check if trail runner select child exists
        if (cfg->get_child_tr_sel()) {
            sub_sim_cfg = cfg->get_child_tr_sel();
        }
        // Else, check if trial runner child exists
        else if (cfg->get_child_tr()) {
            uLogf(
                "Warning: Delphic power selection sub sim deferring to trial "
                "runner child configuraiton\n");
            sub_sim_cfg = cfg->get_child_tr();
        } else {
            uLogf(
                "Warning: Delphic power selection unable to detect suitable "
                "child configuration for sub sim, deferring to parent.\n");
        }
        ////////////////////////////////////////////////////////////
        // Initialize sub-simulation
        m_sub_sim_access.init(sub_sim_cfg U_THREAD_ID_ARG);
        // Assume parent and child sim grow step counts match
        const uDelphicSubSim& sub_sim =
            m_sub_sim_access.get_sub_sim(U_MAIN_THREAD_ID_0_ARG);
        const uUInt MAX_GROW_STEPS_PARENT = sim.get_max_num_grow_steps(sim);
        const uUInt MAX_GROW_STEPS_CHILD =
            sub_sim.get_max_num_grow_steps(sub_sim);
        if (MAX_GROW_STEPS_PARENT != MAX_GROW_STEPS_CHILD) {
            // THIS IS NOT FOOLPROOF AS IT'S THEORETICALLY POSSIBLE (BUT NOT
            // CURRENTLY POSSIBLE) THAT PARENT AND CHILD SIMULATIONS MAY HAVE
            // THE SAME TOTAL NUMBER OF GROW STEPS BUT DIFFERING INTERMEDIATE
            // MONOMER COUNTS. HOWEVER, THIS IS A SIMPLE CHECK THAT WILL
            // PROBABLY TRAP MOST CASES. IN GENERAL, NEED TO STANDARDIZE GROW
            // STEP WITH MONOMER COUNT TO REALLY FIX ANY POSSIBLE DESCREPANCY
            uLogf(
                "Error: Parent and child simulation grow step count mismatch. "
                "Exiting.\n");
            exit(uExitCode_error);
        }
        ////////////////////////////////////////////////////////////
        // Initialize Delphic log weights buffer(s)
        if (cfg->get_child_tr()) {
            uAssert(cfg->get_child_tr()->ensemble_size > U_TO_UINT(0));
            U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
                m_delphic_logw, zeros, cfg->get_child_tr()->ensemble_size);
        } else {
            uLogf(
                "Warning: Delphic power selection did not detect trial runner "
                "child configuration. Skipping TLS log weights buffer "
                "pre-allocation.\n");
        }
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_steps = U_SIS_DELPHIC_DEFAULT_STEPS;
        m_pow_util.clear();
        m_sub_sim_access.clear();
        m_delphic_logw.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t cfg U_THREAD_ID_PARAM) {
        m_pow_util.reset_for_next_trial(sim, cfg U_THREAD_ID_ARG);
        m_sub_sim_access.init(U_THREAD_ID_0_ARG);
    }

    /**
     * Selects among a set of samples with probability proportional to
     * the relative weight raised to a power alpha
     * @param log_weights - log weight at each sample
     *  (all weights are assumed to be valid)
     * @param samples - set of samples to select from
     * @WARNING - samples and log_weights assumed to be parallel (same size)
     *  and non-empty!
     * @param completed_grow_steps - Number of completed grow steps for each
     *  sample in 'samples' set
     * @param log_p_select - the output log of the selection probability
     * @return Unsigned integer index of selected sample
     */
    template <typename t_sample>
    inline uUInt select_index(const uVecCol& log_weights,
                              const std::vector<t_sample>& samples,
                              const uUInt completed_grow_steps,
                              uReal& log_p_select U_THREAD_ID_PARAM) {
        // Assume positive number of samples and parallel log weights
        const size_t NUM_SAMPLES = samples.size();
        uAssertPosEq(log_weights.n_elem, U_TO_MAT_SZ_T(NUM_SAMPLES));
        // Obtain handle to sub-simulation for generating look-ahead ensembles
        uDelphicSubSim& sub_sim =
            m_sub_sim_access.get_sub_sim(U_THREAD_ID_0_ARG);
        // Compute target grow step count
        uAssert(completed_grow_steps <=
                sub_sim.get_max_num_grow_steps(sub_sim));
        const uUInt target_grow_steps =
            std::min(completed_grow_steps + m_steps,
                     sub_sim.get_max_num_grow_steps(sub_sim));
        uAssert(target_grow_steps >= completed_grow_steps);
        // Early out if Delphic look-ahead is 0
        if (completed_grow_steps >= target_grow_steps) {
            return m_pow_util.select_index(log_weights,
                                           samples,
                                           completed_grow_steps,
                                           log_p_select U_THREAD_ID_ARG);
        }
        // Buffer for storing log weights from look-ahead summaries
        uVecCol& delphic_logw = this->get_delphic_logw(U_THREAD_ID_0_ARG);
        delphic_logw.set_size(log_weights.n_elem);
        // Perform Delphic look-ahead summary at each sample
        for (size_t i = 0; i < NUM_SAMPLES; ++i) {
            uAssertBounds(i, 0, samples.size());
            // Get template sample and log weight
            const t_sample& sample = samples[i];
            uDelphicSubSample sub_sample_template;
            m_sample_copier.to(sample /*from*/, sub_sample_template /*to*/);
            uAssertBounds(i, 0, (size_t)log_weights.n_elem);
            const uReal log_weight = log_weights.at(U_TO_MAT_SZ_T(i));
            // Perform Delphic look-ahead simulation
            sub_sim.init(U_THREAD_ID_0_ARG);
            sub_sim.run_from_template_until(target_grow_steps,
                                            sub_sample_template,
                                            completed_grow_steps,
                                            log_weight U_THREAD_ID_ARG);
            const uReal summary = uDelphicSummary::summarize(
                sub_sim.get_completed_log_weights_view());
            delphic_logw.at(i) = summary;
        }
        // Select index based on Delphic look-ahead summaries
        return m_pow_util.select_index(delphic_logw,
                                       samples,
                                       target_grow_steps,
                                       log_p_select U_THREAD_ID_ARG);
    }

private:
    /**
     * @return thread-specific Delphic log weights buffer
     */
    inline uVecCol& get_delphic_logw(U_THREAD_ID_0_PARAM) {
        return U_ACCESS_TLS_DATA(m_delphic_logw);
    }

    /**
     * Number of look-ahead grow steps
     */
    uUInt m_steps;

    /**
     * Utility for power-smoothing weights
     */
    uDelphicPowUtil m_pow_util;

    /**
     * Defines how our sub-simulation objects are accessed in threaded
     * and non-threaded environments
     */
    uDelphicSubSimAccess m_sub_sim_access;

    /**
     * Used for converting between parent and child simulation sample types
     */
    uDelphicSampleCopyier m_sample_copier;

    /**
     * Summarized log weights buffer(s) from Delphic look-ahead
     */
    U_DECLARE_TLS_DATA(uVecCol, m_delphic_logw);
};

#endif  // uSisSelectDelphicPower_h
