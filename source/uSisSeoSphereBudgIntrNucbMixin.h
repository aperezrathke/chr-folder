//****************************************************************************
// uSisSeoSphereBudgIntrNucbMixin.h
//****************************************************************************

/**
 * A nuclear body interaction mixin manages nuclear body association
 *  constraints. Note, this is different from a "nuclear mixin" which is used
 *  for modeling confinement by constraining all monomer beads to reside
 *  within the nuclear volume. In contrast, the nuclear body interaction mixin
 *  enforces polymer fragments to be proximal or distal to a specific
 *  spherical mass (x, y, z) coordinate as configured by the user.
 *  Technically, this mass does not have to reside within nuclear volume.
 *
 * This mixin supports a failure budget and assumes spherical nuclear bodies.
 */

#ifndef uSisSeoSphereBudgIntrNucbMixin_h
#define uSisSeoSphereBudgIntrNucbMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibBudgUtil.h"
#include "uSisIntrNucbBudgUtil.h"
#include "uSisIntrNucbCommonBatch.h"
#include "uSisSeoSphereIntrNucbFilt.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisSeoSphereBudgIntrNucbSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers.
 */
template <typename t_SisGlue>
class uSisSeoSphereBudgIntrNucbMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Nuclear body knock-in budget access policy
     */
    typedef uSisIntrNucbBudgAccKin<glue_t> intr_nucb_budg_acc_kin_t;

    /**
     * Nuclear body knock-out budget access policy
     */
    typedef uSisIntrNucbBudgAccKo<glue_t> intr_nucb_budg_acc_ko_t;

    /**
     * Batch interactions utility
     */
    typedef uSisIntrNucbCommonBatch<glue_t> intr_nucb_batch_util_t;

    /**
     * Nuclear body interaction knock-in filter
     */
    typedef uSisSeoSphereIntrNucbKinFilt<glue_t> intr_nucb_filt_kin_t;

    /**
     * Nuclear body interaction knock-out filter
     */
    typedef uSisSeoSphereIntrNucbKoFilt<glue_t> intr_nucb_filt_ko_t;

    /**
     * Initializes mixin
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        m_intr_nucb_loci_tables = sim.get_intr_nucb_loci_tables();
        // Assume clear() has initialized failure counts
        uAssert(m_intr_nucb_kin_num_fail == U_TO_UINT(0));
        uAssert(m_intr_nucb_ko_num_fail == U_TO_UINT(0));
    }

    /**
     * Resets to default state
     */
    void clear() {
        intr_nucb_batch_util_t::clear(m_intr_nucb_loci_tables);
        m_intr_nucb_kin_num_fail = U_TO_UINT(0);
        m_intr_nucb_ko_num_fail = U_TO_UINT(0);
    }

    /**
     * Checks if seed can satisfy nuclear body interaction constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE if seed can satisfy constraints, uFALSE o/w
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        // Acquire TLS scratch buffers
        uSisIntrNucbBudgFailPolicyArgs_t policy_args =
            sim.get_intr_nucb_budg_fail_policy_args(U_THREAD_ID_0_ARG);
        // @TODO - consider only resetting seed candidate slot
        intr_nucb_reset_tls(policy_args, sim);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.kin_delta_fails.n_elem));
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.ko_delta_fails.n_elem));
        // https://stackoverflow.com/questions/1840253/template-member-function-of-template-class-called-from-template-function
        // When the name of a member template specialization appears after .
        // or -> in a postfix-expression, or after nested-name-specifier in a
        // qualified-id, and the postfix-expression or qualified-id explicitly
        // depends on a template-parameter(14.6.2), the member template name
        // must be prefixed by the keyword template. Otherwise the name is
        // assumed to name a non-template.
        return intr_nucb_batch_util_t::template is_seed_okay<
            intr_nucb_filt_kin_t,
            intr_nucb_filt_ko_t,
            intr_nucb_mixin_t,
            uSisIntrNucbBudgFailPolicyArgs_t>(*this,
                                              &policy_args,
                                              m_intr_nucb_loci_tables,
                                              nfo,
                                              sample,
                                              sim U_THREAD_ID_ARG);
    }

    /**
     * Determines which positions satisfy nuclear body constraints
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for constraint violations.
     * @param candidate_radius - Radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - Node identifier from which candidate positions
     *  were generated
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_nucb_filter(uBoolVecCol& out_legal_candidates_primed,
                          const uMatrix& candidate_centers,
                          const uReal candidate_radius,
                          const uUInt parent_node_id,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // Acquire TLS scratch buffers
        uSisIntrNucbBudgFailPolicyArgs_t policy_args =
            sim.get_intr_nucb_budg_fail_policy_args(U_THREAD_ID_0_ARG);
        intr_nucb_reset_tls(policy_args, sim);
        // Mark candidates that violate constraints
        intr_nucb_batch_util_t::template mark_viol<
            intr_nucb_filt_kin_t,
            intr_nucb_filt_ko_t,
            intr_nucb_mixin_t,
            uSisIntrNucbBudgFailPolicyArgs_t>(*this,
                                              &policy_args,
                                              out_legal_candidates_primed,
                                              m_intr_nucb_loci_tables,
                                              candidate_centers,
                                              candidate_radius,
                                              parent_node_id,
                                              sample,
                                              sim U_THREAD_ID_ARG);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  nuclear body interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed nuclear body interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_nucb_kin_on_fail(
        uSisIntrNucbBudgFailPolicyArgs_t* const p_policy_args,
        const uUInt candidate_id,
        const uSisIntrNucbFilterCore_t& c,
        const sample_t& sample,
        const sim_t& sim) {
        uAssert(p_policy_args);
        return intr_nucb_on_fail(p_policy_args->kin_delta_fails,
                                 p_policy_args->kin_fail_table,
                                 m_intr_nucb_kin_num_fail,
                                 sim.get_intr_nucb_kin_max_fail(),
                                 candidate_id,
                                 c,
                                 sample,
                                 sim);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  nuclear body interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed nuclear body interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_nucb_ko_on_fail(
        uSisIntrNucbBudgFailPolicyArgs_t* const p_policy_args,
        const uUInt candidate_id,
        const uSisIntrNucbFilterCore_t& c,
        const sample_t& sample,
        const sim_t& sim) {
        uAssert(p_policy_args);
        return intr_nucb_on_fail(p_policy_args->ko_delta_fails,
                                 p_policy_args->ko_fail_table,
                                 m_intr_nucb_ko_num_fail,
                                 sim.get_intr_nucb_ko_max_fail(),
                                 candidate_id,
                                 c,
                                 sample,
                                 sim);
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - Unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - Radius of the node to add
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation of parameter sample
     */
    void intr_nucb_update(const uUInt node_id,
                          const uReal* point,
                          const uReal radius,
                          const uUInt candidate_id,
                          sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // Acquire TLS scratch buffers
        const uSisIntrNucbBudgFailPolicyArgs_t policy_args =
            sim.get_intr_nucb_budg_fail_policy_args(U_THREAD_ID_0_ARG);
        // Knock-in: Remove failed interactions prior to satisfaction testing
        // and update fail counts
        uAssertBounds(candidate_id,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.kin_fail_table.size()));
        uAssert(policy_args.kin_delta_fails.n_elem ==
                U_TO_MAT_SZ_T(policy_args.kin_fail_table.size()));
        uAssert(policy_args.kin_delta_fails.at(candidate_id) ==
                U_TO_UINT(policy_args.kin_fail_table[candidate_id].size()));
        intr_nucb_update_fail(m_intr_nucb_loci_tables.loci_kin,
                              m_intr_nucb_kin_num_fail,
                              sim.get_intr_nucb_kin_max_fail(),
                              policy_args.kin_fail_table[candidate_id],
                              sample,
                              sim);
        uAssert(m_intr_nucb_kin_num_fail <= sim.get_intr_nucb_kin_max_fail());

        // Knock-out: Remove failed interactions prior to satisfaction testing
        // and update fail counts
        uAssertBounds(candidate_id,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.ko_fail_table.size()));
        uAssert(policy_args.ko_delta_fails.n_elem ==
                U_TO_MAT_SZ_T(policy_args.ko_fail_table.size()));
        uAssert(policy_args.ko_delta_fails.at(candidate_id) ==
                U_TO_UINT(policy_args.ko_fail_table[candidate_id].size()));
        intr_nucb_update_fail(m_intr_nucb_loci_tables.loci_ko,
                              m_intr_nucb_ko_num_fail,
                              sim.get_intr_nucb_ko_max_fail(),
                              policy_args.ko_fail_table[candidate_id],
                              sample,
                              sim);
        uAssert(m_intr_nucb_ko_num_fail <= sim.get_intr_nucb_ko_max_fail());

        // Remove satisfied interactions from active loci tables
        intr_nucb_batch_util_t::template remove_satisf<intr_nucb_filt_kin_t,
                                                       intr_nucb_filt_ko_t>(
            m_intr_nucb_loci_tables,
            node_id,
            point,
            radius,
            candidate_id,
            sample,
            sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Utility resets TLS buffers prior to constraint checks
     */
    static void intr_nucb_reset_tls(
        uSisIntrNucbBudgFailPolicyArgs_t& policy_args,
        const sim_t& sim) {
        // Reset TLS scratch buffers
        policy_args.kin_delta_fails.zeros();
        policy_args.ko_delta_fails.zeros();
        // Using resize(0) instead of clear() to hopefully avoid freeing
        // backing store on older STL libraries (i.e. reduce allocations)
        // https://stackoverflow.com/questions/2738967/vector-clear-vs-resize
        const size_t num_candidates = policy_args.kin_delta_fails.n_elem;
        policy_args.kin_fail_table.resize(0);
        policy_args.kin_fail_table.resize(num_candidates);
        policy_args.ko_fail_table.resize(0);
        policy_args.ko_fail_table.resize(num_candidates);
        // Verify TLS buffer assumptions
        uAssertPosEq(U_TO_UINT(policy_args.kin_delta_fails.n_elem),
                     sim.get_num_unit_sphere_sample_points());
        uAssertPosEq(policy_args.kin_delta_fails.n_elem,
                     policy_args.ko_delta_fails.n_elem);
        uAssertPosEq(policy_args.kin_fail_table.size(),
                     policy_args.ko_fail_table.size());
        uAssertPosEq(policy_args.kin_delta_fails.n_elem,
                     U_TO_MAT_SZ_T(policy_args.kin_fail_table.size()));
    }

    /**
     * Implementation for when a candidate will never satisfy a nuclear body
     *  interaction
     * @param delta_fails - Buffer of incremental (delta) failure counts at
     *  each candidate position relative to current sample state, will be
     *  incremented at corresponding candidate
     * @param fail_table - Table of failed interactions at each candidate, if
     *  candidate is not culled, then will be updated with information to
     *  allow future removal of the failed interaction
     * @param num_fail - Current number of failed interactions at sample
     * @param max_fail - Maximum allowed number of failed interactions
     * @param candidate_id - Candidate which failed nuclear body interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    static uBool intr_nucb_on_fail(uUIVecCol& delta_fails,
                                   uSisIntrNucbBudgFailTable_t& fail_table,
                                   const uUInt num_fail,
                                   const uUInt max_fail,
                                   const uUInt candidate_id,
                                   const uSisIntrNucbFilterCore_t& c,
                                   const sample_t& sample,
                                   const sim_t& sim) {

        uAssertPosEq(U_TO_UINT(delta_fails.n_elem),
                     sim.get_num_unit_sphere_sample_points());
        uAssertBounds(
            candidate_id, U_TO_UINT(0), U_TO_UINT(delta_fails.n_elem));
        uAssert((delta_fails.at(candidate_id) + num_fail) <= max_fail);
        // Increment failure count
        ++(delta_fails.at(candidate_id));
        const uUInt cand_num_fail = delta_fails.at(candidate_id) + num_fail;
        if (cand_num_fail <= max_fail) {
            // Update failed interactions table
            uSisIntrNucbBudgFail_t fail_nfo;
            fail_nfo.intr_id = c.intr_id;
            fail_nfo.frag_lid = sim.get_growth_lid(c.frag_nid_lo);
            uAssert(sim.get_growth_lid(c.frag_nid_lo) ==
                    sim.get_growth_lid(c.frag_nid_hi));
            uAssertBounds(
                candidate_id, U_TO_UINT(0), U_TO_UINT(fail_table.size()));
            fail_table[candidate_id].push_back(fail_nfo);
            uAssertPosEq(delta_fails.at(candidate_id),
                         U_TO_UINT(fail_table[candidate_id].size()));
            // Retain candidate
            return uTRUE;
        }
        // Cull candidate
        return uFALSE;
    }

    /**
     * Removes failed nuclear body interactions according to parameter
     * 'fail_list', will also update 'num_fail' count. Here 'interaction'
     * refers to all interactions of same type such as 'knock-in' or
     * 'knock-out' (but not both!)
     * @param intr_loci - Table of interactions partitioned by locus
     * @param num_fail - Updated total number of failed interactions
     * @param max_fail - Maximum allowed number of failed interactions
     * @param fail_list - List of failed interactions at chosen candidate
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    static void intr_nucb_update_fail(
        uSisIntrLociTable_t& intr_loci,
        uUInt& num_fail,
        const uUInt max_fail,
        const uSisIntrNucbBudgFailList_t& fail_list,
        const sample_t& sample,
        const sim_t& sim) {
        // Update failure count
        const size_t delta_fail = fail_list.size();
        num_fail += U_TO_UINT(delta_fail);
        uAssert(num_fail <= max_fail);
        // Process each failed interaction
        for (size_t i = 0; i < delta_fail; ++i) {
            // Handle to failure info
            const uSisIntrNucbBudgFail_t& fail_nfo = fail_list[i];
            uAssertBounds(
                fail_nfo.frag_lid, U_TO_UINT(0), U_TO_UINT(intr_loci.size()));
            if (sample_t::is_mloc_growth) {
                uIntrLibBudgUtil::remove_fail(intr_loci[fail_nfo.frag_lid],
                                              fail_nfo.intr_id);
            } else {
                uAssert(fail_nfo.frag_lid == U_TO_UINT(0));
                uAssert(intr_loci.size() == ((size_t)1));
                uIntrLibBudgUtil::remove_fail(intr_loci[0], fail_nfo.intr_id);
            }
        }
    }

    /**
     * Collection of active nuclear body interactions partitioned by loci
     */
    uSisIntrNucbLociTables_t m_intr_nucb_loci_tables;

    /**
     * Number of failed nuclear body knock-in interactions
     */
    uUInt m_intr_nucb_kin_num_fail;

    /**
     * Number of failed nuclear body knock-in interactions
     */
    uUInt m_intr_nucb_ko_num_fail;
};

#endif  // uSisSeoSphereBudgIntrNucbMixin_h
