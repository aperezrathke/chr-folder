//****************************************************************************
// uTime.h
//****************************************************************************

/**
 * Simple time utility methods
 */

#ifndef uTime_h
#define uTime_h

#include "uBuild.h"

#include <time.h>

/**
 * @return integral based time in seconds
 */
inline time_t u_time_secs() { return time(NULL); }

// Platform specific implementations based on:
//  https://stackoverflow.com/questions/17432502/how-can-i-measure-cpu-time-and-wall-clock-time-on-both-linux-windows
#ifdef U_BUILD_OS_WINDOWS

    #define WIN32_LEAN_AND_MEAN
    #define NOMINMAX
    #include <Windows.h>

    /**
     *  @return wall clock time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_wall() {
        LARGE_INTEGER time, freq;
        if (QueryPerformanceFrequency(&freq) && QueryPerformanceCounter(&time)) {
            return (double)time.QuadPart / freq.QuadPart;
        }
        return 0.0;
    }

    /**
     * @return CPU time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_cpu() {
        FILETIME a, b, c, d;
        if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0) {
            return (double)(d.dwLowDateTime |
                            ((unsigned long long)d.dwHighDateTime << 32)) *
                   0.0000001;
        }
        return 0.0;
    }

#elif defined(U_BUILD_OS_UNIX)

    #include <sys/time.h>

    /**
     *  @return wall clock time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_wall() {
        struct timeval time;
        if (gettimeofday(&time, NULL) == 0) {
            return (double)time.tv_sec + (double)time.tv_usec * .000001;
        }
        return 0.0;
    }

    /**
     * @return CPU time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_cpu() { return (double)clock() / CLOCKS_PER_SEC; }

#else

    /**
     *  @return wall clock time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_wall() { return time(NULL); }

    /**
     * @return CPU time in seconds, defaults to 0.0 if error
     */
    inline double u_time_secs_cpu() { return time(NULL); }

#endif  // Platform check

#endif  // uTime_h
