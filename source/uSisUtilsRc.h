//****************************************************************************
// uSisUtilsRc.h
//****************************************************************************

/**
 * Common utilities for rejection control of sequential importance sampling of
 * chromatin chains.
 */

#ifndef uSisUtilsRc_h
#define uSisUtilsRc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uGlobals.h"
#include "uSisFailStatus.h"
#include "uThread.h"
#include "uTypes.h"

#include <numeric>  // needed for std:iota

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro for declaring regrow callbacks for use by rejection control mixin.
 * This defines a default null interface (does nothing)
 */
#define U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE                    \
    void reset_for_regrowth(const sample_t& sample,                  \
                            const uUInt num_completed_grow_steps,    \
                            const sim_t& sim,                        \
                            uSpConstConfig_t config) {}              \
    void on_reseed_finish(const sample_t& sample,                    \
                          uReal& log_weight,                         \
                          bool& status,                              \
                          const sim_t& sim) {}                       \
    void on_regrow_step_finish(sample_t& sample,                     \
                               uReal& log_weight,                    \
                               bool& status,                         \
                               const uUInt num_completed_grow_steps, \
                               const sim_t& sim_t U_THREAD_ID_PARAM) {}

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility namespace
 */
namespace uSisUtilsRc {

/**
 * @param out_log_weight - If the sample passes the checkpoint, then:
 *  log_weight = old_log_weight - log(keep_probability)
 *  else, the sample did not pass the checkpoint and log_weight = 0
 * @param out_log_keep_weight - If the sample passes the checkpoint, then:
 *  log_keep_weight = old_log_keep_weight + log(keep_probability)
 *  else, the sample did not pass the checkpoint and log_keep_weight = 0.
 *  This stores the running product of acceptance probability for all
 *  checkpoints encountered by this sample. This value can then be used
 *  for estimating a partition constant.
 * @param normed_exp_weight - the transformed weight (normalized by
 *  the current or cached max weight)
 * @param scale - The checkpoint scale. A samples is kept with probability
 *  min(1.0, norm_exp_weight / scale), else it is regrown
 * @return TRUE if sample needs to be regrown, FALSE o/w
 */
inline bool checkpoint_filter(uReal& out_log_weight,
                              uReal& out_log_keep_weight,
                              const uReal normed_exp_weight,
                              const uReal scale U_THREAD_ID_PARAM) {
    // Samples are kept with probability :
    //  min(1, w_i / scale) where w_i is weight of i-th sample
    const uReal scaled_weight = normed_exp_weight / scale;
    const uReal keep_prob = std::min(U_TO_REAL(1.0), scaled_weight);

    const bool b_should_regrow = (uRng.unif_real() > keep_prob);

    // We kept sample -> correct its weight
    if (!b_should_regrow) {
        // New weight should be:
        //  w_i / keep_prob
        // Since we are in log space:
        //  log( w_i / keep_prob ) = log(w_i) - log(keep_prob)
        const uReal log_keep_prob = log(keep_prob);
        out_log_weight -= log_keep_prob;
        out_log_keep_weight += log_keep_prob;
    }
    // Else, sample should be regrown, reset its weight to 0
    else {
        out_log_weight = out_log_keep_weight = U_TO_REAL(0.0);
    }

    return b_should_regrow;
}

/**
 * WARNING - No actual seed() call may be made - check implementation
 * for regrow_attempt_from_template() if this is not desired behavior.
 * Will regrow a sample starting from a parameter template sample.
 * @param out_sample - The sample to regrow
 * @param out_log_weight - The log weight of the sample after regrowth
 * @param sim - the parent simulation
 * @param target_num_grow_steps - the total number of seed() + grow_*()
 *  calls that will be attempted for successful completion. Note, no
 *  actual seed() calls may be made - see regrow_attempt_from_template()
 * @param sample_id - The sample identifier (index into samples array)
 * @param template_sample - The template sample state to start the
 *  regrowth procedure from
 * @param template_num_grow_steps - The total number of seed() +
 *  grow_*() calls assumed to have been comppleted by the template
 *  sample.
 * @param init_log_weight - The out_log_weight is initialized to this
 *  value prior to regrowth
 * @return final status of last grow_*) call -> TRUE if sample needs
 *  to continue growing, FALSE if finished growing. We should not return
 *  any dead chains. Currently, method will infinite loop until chain is
 *  no longer dead.
 */
template <typename sample_t, typename sim_t>
bool regrow_force_from_template(sample_t& out_sample,
                                uReal& out_log_weight,
                                sim_t& sim,
                                const uUInt target_num_grow_steps,
                                const uUInt sample_id,
                                const sample_t& template_sample,
                                const uUInt template_num_grow_steps,
                                const uReal init_log_weight U_THREAD_ID_PARAM) {
    // Final growth status of sample: TRUE if sample still needs growing,
    // FALSE if sample is dead or has finished
    bool status = false;

    do {
        status =
            sim.regrow_attempt_from_template(out_sample,
                                             out_log_weight,
                                             sim,
                                             target_num_grow_steps,
                                             sample_id,
                                             template_sample,
                                             template_num_grow_steps,
                                             init_log_weight U_THREAD_ID_ARG);
        // @WARNING - THIS COULD INFINITE LOOP!
    } while (out_sample.is_dead());

    return status;
}

/**
 * Regrows a sample from start or from template depending on simulation
 * status. If simulation has a registered template, then will force regrow
 * from that template. Else, will force regrow from start.
 * @param out_sample - The sample to be regrown
 * @param out_log_weight - The final log weight of the sample
 * @param sim - The parent simulation
 * @param target_num_grow_steps - The total number of seed() + grow_*()
 *  calls that must be successfully completed
 * @param sample_id - The sample identifier (index into samples array)
 * @return final status of last grow_*() call -> TRUE if sample needs
 *  to continue growing, FALSE if finished growing. We should not return
 *  any dead chains. Currently, method will infinite loop until chain is
 *  no longer dead.
 */
template <typename sample_t, typename sim_t>
bool regrow_force(sample_t& out_sample,
                  uReal& out_log_weight,
                  sim_t& sim,
                  const uUInt target_num_grow_steps,
                  const uUInt sample_id U_THREAD_ID_PARAM) {
    if (sim.is_run_from_template()) {
        return regrow_force_from_template(
            out_sample,
            out_log_weight,
            sim,
            target_num_grow_steps,
            sample_id,
            sim.get_template_sample(),
            sim.get_template_num_grow_steps(),
            sim.get_template_log_weight() U_THREAD_ID_ARG);
    }

    return sim.regrow_force_from_start(out_sample,
                                       out_log_weight,
                                       sim,
                                       target_num_grow_steps,
                                       sample_id U_THREAD_ID_ARG);
}

#ifdef U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
/**
 * Debug utility for reporting quantile cutoffs from resulting scale
 * Statistics reported:
 *  - min weight
 *  - first quartile weight
 *  - median weight
 *  - mean weight
 *  - third quartile weight
 *  - max weight
 *  - quantile of scale value (proportion of weights less than scale)
 * @param scale - the cutoff scale - samples are accepted/rejected according to
 *   probability = min(1, weight / scale)
 * @param norm_exp_weights - the vector of exp transformed weights
 *   normalized by the max weight - should be in (0,1]
 * @param log_weights - the vector of unnormalized log weights
 */
inline void report_weight_summary(const uReal scale,
                                  const uVecCol& norm_exp_weights,
                                  const uVecCol& log_weights) {
    // Verify parameter assumptions - scale must be greater than zero as we are
    // dividing by it!
    uAssert(scale > 0.0);
    // Verify parameter assumptions - weights arrays must be same size
    const uUInt n_weights = U_TO_UINT(norm_exp_weights.size());
    uAssertPosEq(norm_exp_weights.size(), log_weights.size());
    // Sort by indices as we have two parallel arrays of weights
    std::vector<uUInt> indices(n_weights);
    // Fill indices array range with sequentially increasing values
    std::iota(std::begin(indices), std::end(indices), 0);
    // Sort indices based on comparing values in weights array
    std::sort(indices.begin(),
              indices.end(),
              [&log_weights](const uUInt ix1, const uUInt ix2) {
                  return log_weights[ix1] < log_weights[ix2];
              });

    uLogf("Weight summary statistics for scale: %f\n", scale);
    // Minimum weight
    const uUInt ix_min = indices[0];
    uAssertBounds(ix_min, 0, n_weights);
    uLogf("\tMin weight (exp, log): %f, %f\n",
          norm_exp_weights[ix_min],
          log_weights[ix_min]);
    // First quartile weight
    const uUInt ix_qt1 = indices[n_weights / 4];
    uAssertBounds(ix_qt1, 0, n_weights);
    uLogf("\t1st quartile: %f, %f\n",
          norm_exp_weights[ix_qt1],
          log_weights[ix_qt1]);
    // Median weight
    const uUInt ix_median = indices[n_weights / 2];
    uAssertBounds(ix_median, 0, n_weights);
    uLogf("\tMedian: %f, %f\n",
          norm_exp_weights[ix_median],
          log_weights[ix_median]);
    // Mean weight
    uLogf("\tMean: %f, %f\n",
          uMatrixUtils::mean(norm_exp_weights),
          uMatrixUtils::mean(log_weights));
    // Third quartile weight
    const uUInt ix_qt3 = indices[(n_weights * 3) / 4];
    uAssertBounds(ix_qt3, 0, n_weights);
    uLogf("\t3rd quartile: %f, %f\n",
          norm_exp_weights[ix_qt3],
          log_weights[ix_qt3]);
    // Max weight
    const uUInt ix_max = indices[n_weights - 1];
    uAssertBounds(ix_max, 0, n_weights);
    uLogf("\tMax: %f, %f\n", norm_exp_weights[ix_max], log_weights[ix_max]);
#ifdef U_BUILD_CXX_11
    // Scale quantile
    const auto it_low = std::lower_bound(
        indices.begin(),
        indices.end(),
        scale,
        [&norm_exp_weights](const uUInt ix, const uReal scale_) {
            return norm_exp_weights[ix] < scale_;
        });
    const size_t ix_qt_scale = it_low - indices.begin();
    const uReal qt_scale =
        U_TO_REAL(100.0) * U_TO_REAL(ix_qt_scale) / U_TO_REAL(n_weights);
    uLogf("\tScale quantile [0,100]: %f\n", qt_scale);
#endif  // U_BUILD_CXX_11
}
#endif  // U_BUILD_ENABLE_RC_WEIGHT_SUMMARY

}  // namespace uSisUtilsRc

#endif  // uSisUtilsRc_h
