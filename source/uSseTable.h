//****************************************************************************
// uSseTable.h
//****************************************************************************

/**
 * A bare-bones object for allocating tabular data for use with SSE intrinsics.
 */

#ifndef uSseTable_h
#define uSseTable_h

#include "uBuild.h"
#include "uAssert.h"
#include "uMemAlign.h"
#include "uSse.h"

#include <string.h>

/**
 * Aligns internal data for use with SSE load
 */
class uSseTable {
public:
    /**
     * Number of rows
     */
    const uUInt n_rows;

    /**
     * Number of columns
     */
    const uUInt n_cols;

    /**
     * Total number of elements
     */
    const uUInt n_elem;

    /**
     * Default constructor
     */
    uSseTable() : n_rows(0), n_cols(0), n_elem(0), mem(NULL) {}

    /**
     * Copy constructor
     */
    uSseTable(const uSseTable& other)
        : n_rows(other.n_rows), n_cols(other.n_cols), n_elem(other.n_elem) {
        u_aligned_malloc(
            mem, n_elem * sizeof(uReal), U_SSE_ALIGNMENT_256_PD, uReal);
        uAssert(mem);
        memcpy(mem, other.mem, n_elem * sizeof(uReal));
    }

    /**
     * Assignment operator
     */
    uSseTable& operator=(const uSseTable& other) {
        clear();
        set_size(other.n_rows, other.n_cols);
        u_aligned_malloc(
            mem, n_elem * sizeof(uReal), U_SSE_ALIGNMENT_256_PD, uReal);
        uAssert(mem);
        memcpy(mem, other.mem, n_elem * sizeof(uReal));
    }

    /**
     * Destructor
     */
    ~uSseTable() { clear(); }

    /**
     * Clears backing store!
     */
    void clear() {
        u_aligned_free(mem);
        mem = NULL;
        set_size(0, 0);
    }

    /**
     * ALLOCATES an aligned table of n_rows x n_cols filled with zeros
     */
    void zeros(const uUInt n_rows_, const uUInt n_cols_) {
        set_size(n_rows_, n_cols_);
        u_aligned_free(mem);
        u_aligned_malloc(
            mem, n_elem * sizeof(uReal), U_SSE_ALIGNMENT_256_PD, uReal);
        uAssert(mem);
        memset(mem, 0, n_elem * sizeof(uReal));
    }

    /**
     * @return map from 2-D index (in_row, in_col) to 1-D index into memptr
     */
    static inline uUInt to_1D_index(const uUInt in_row,
                                    const uUInt in_col,
                                    const uUInt n_rows) {
        uAssert(in_row < n_rows);
        uAssert(n_rows > 0);
        return in_row + (in_col * n_rows);
    }

    /**
     * @return raw column pointer
     */
    inline uReal* colptr(const uUInt in_col) {
        uAssertBounds(in_col, 0, n_cols);
        return &(mem[in_col * n_rows]);
    }

    /**
     * @return const raw column pointer
     */
    inline const uReal* colptr(const uUInt in_col) const {
        uAssertBounds(in_col, 0, n_cols);
        return &(mem[in_col * n_rows]);
    }

    /**
     * @return pointer to internal memory
     */
    inline uReal* memptr() { return mem; }

    /**
     * @return const pointer to internal memory
     */
    inline const uReal* memptr() const { return mem; }

private:
    /**
     * Update read-only dimensions
     */
    void set_size(const uUInt n_rows_, const uUInt n_cols_) {
        const_cast<uUInt&>(n_rows) = n_rows_;
        const_cast<uUInt&>(n_cols) = n_cols_;
        const_cast<uUInt&>(n_elem) = n_rows_ * n_cols_;
    }

    /**
     * Internal memory
     */
    uReal* mem;
};

#endif  // uSseTable_h
