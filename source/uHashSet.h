//****************************************************************************
// uHashMap.h
//****************************************************************************

#ifndef uHashSet_h
#define uHashSet_h

/**
 * @brief Hash-based set implementation
 */

#include "uBuild.h"

#ifdef U_BUILD_USE_SPARSEPP_HASH

    // https://github.com/greg7mdp/sparsepp
#   include <sparsepp/spp.h>
#   define U_HASH_SET spp::sparse_hash_set

#else

#   ifdef U_BUILD_CXX_11
#       include <unordered_set>
#       define U_HASH_SET std::unordered_set
#   else
#       include <boost/unordered_set.hpp>
#       define U_HASH_SET boost::unordered_set
#   endif  // U_BUILD_CXX_11

#endif  // U_BUILD_USE_SPARSEPP_HASH

#endif  // uHashSet_h
