//****************************************************************************
// uLmrkUtils.h
//****************************************************************************

/**
 * Utility library for landmark trial runners
 */

#ifndef uLmrkUtils_h
#define uLmrkUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uBroadPhase.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHetrNodeRadiusMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisNullIntrChrMixin.h"
#include "uSisNullIntrLamMixin.h"
#include "uSisNullIntrNucbMixin.h"
#include "uSisSample.h"
#include "uSisSeoBendEnergyMixin.h"
#include "uSisSeoMlocGrowthMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSeoUnifCubeSeedMixin.h"
#include "uSisSim.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"

//****************************************************************************
// MACROS
//****************************************************************************

/**
 * Macro to declare common attributes for single locus, homogeneous chromatin
 * nodes (all nodes have same diameter) to help ensure parity across parent
 * and child simulations. For landmark trial runners, we want to ensure the
 * parent and child samples are the same else copying will not work.
 */
#define U_SIS_DECLARE_LMRK_SLOC_HOMG_ATTRIBS(t_SisGlue)             \
    /* Simulation */                                                \
    typedef uSisSim<t_SisGlue> sim_t;                               \
    /* Sample */                                                    \
    typedef uSisSample<t_SisGlue> sample_t;                         \
    /* Simulation level node radius mixin */                        \
    typedef uSisHomgNodeRadiusSimLevelMixin<t_SisGlue>              \
        sim_level_node_radius_mixin_t;                              \
    /* Sample level node radius mixin */                            \
    typedef uSisHomgNodeRadiusMixin<t_SisGlue> node_radius_mixin_t; \
    /* Simulation level growth mixin */                             \
    typedef uSisSeoSloc::GrowthSimLevelMixin_serial<t_SisGlue>      \
        sim_level_growth_mixin_t;                                   \
    /* Sample level growth mixin */                                 \
    typedef uSisSeoSloc::GrowthMixin<t_SisGlue> growth_mixin_t;     \
    /* Simulation level nuclear mixin */                            \
    typedef uSisSphereNuclearSimLevelMixin_serial<t_SisGlue>        \
        sim_level_nuclear_mixin_t;                                  \
    /* Sample level nuclear mixin */                                \
    typedef uSisSphereNuclearMixin<t_SisGlue> nuclear_mixin_t;      \
    /* Simulation level collision mixin */                          \
    typedef uSisHardShellCollisionSimLevelMixin_serial<t_SisGlue>   \
        sim_level_collision_mixin_t;                                \
    /* Sample level collision mixin */                              \
    typedef uSisHardShellCollisionMixin<t_SisGlue, uBroadPhase_t>   \
        collision_mixin_t

/**
 * The default specification for common mixins shared by the uniform null
 * model for single locus, homogeneous chromatin nodes (all nodes have same
 * diameter). The null model is used for generating uniformly sampled chains
 * without interaction constraints.
 */
#define U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_NULL_MODEL_ATTRIBS(t_SisGlue)      \
    U_SIS_DECLARE_LMRK_SLOC_HOMG_ATTRIBS(t_SisGlue);                         \
    /* Simulation level chrome-to-chrome interaction mixin */                \
    typedef uSisNullIntrChrSimLevelMixin<t_SisGlue>                          \
        sim_level_intr_chr_mixin_t;                                          \
    /* Sample level chrome-to-chrome interaction mixin */                    \
    typedef uSisNullIntrChrMixin<t_SisGlue> intr_chr_mixin_t;                \
    /* Simulation level lamina interaction mixin */                          \
    typedef uSisNullIntrLamSimLevelMixin<t_SisGlue>                          \
        sim_level_intr_lam_mixin_t;                                          \
    /* Sample level lamina interaction mixin */                              \
    typedef uSisNullIntrLamMixin<t_SisGlue> intr_lam_mixin_t;                \
    /* Simulation level nuclear body interaction mixin */                    \
    typedef uSisNullIntrNucbSimLevelMixin<t_SisGlue>                         \
        sim_level_intr_nucb_mixin_t;                                         \
    /* Sample level nuclear body interaction mixin */                        \
    typedef uSisNullIntrNucbMixin<t_SisGlue> intr_nucb_mixin_t;              \
    /* Simulation level energy mixin */                                      \
    typedef uSisUnifEnergySimLevelMixin<t_SisGlue> sim_level_energy_mixin_t; \
    /* Sample level energy mixin */                                          \
    typedef uSisUnifEnergyMixin<t_SisGlue> energy_mixin_t;                   \
    /* Seed mixin */                                                         \
    typedef uSisSeoUnifCubeSeedMixin<t_SisGlue> seed_mixin_t

/**
 * Macro to declare common attributes for multiple loci, heterogeneous
 * chromatin nodes (nodes may have varying diameter) to help ensure parity
 * across parent and child simulations. For landmark trial runners, we want to
 * ensure the parent and child samples are the same else copying will not work.
 */
#define U_SIS_DECLARE_LMRK_MLOC_HETR_ATTRIBS(t_SisGlue)             \
    /* Simulation */                                                \
    typedef uSisSim<t_SisGlue> sim_t;                               \
    /* Sample */                                                    \
    typedef uSisSample<t_SisGlue> sample_t;                         \
    /* Simulation level node radius mixin */                        \
    typedef uSisHetrNodeRadiusSimLevelMixin<t_SisGlue>              \
        sim_level_node_radius_mixin_t;                              \
    /* Sample level node radius mixin */                            \
    typedef uSisHetrNodeRadiusMixin<t_SisGlue> node_radius_mixin_t; \
    /* Simulation level growth mixin */                             \
    typedef uSisSeoMloc::GrowthSimLevelMixin_serial<t_SisGlue>      \
        sim_level_growth_mixin_t;                                   \
    /* Sample level growth mixin */                                 \
    typedef uSisSeoMloc::GrowthMixin<t_SisGlue> growth_mixin_t;     \
    /* Simulation level nuclear mixin */                            \
    typedef uSisSphereNuclearSimLevelMixin_serial<t_SisGlue>        \
        sim_level_nuclear_mixin_t;                                  \
    /* Sample level nuclear mixin */                                \
    typedef uSisSphereNuclearMixin<t_SisGlue> nuclear_mixin_t;      \
    /* Simulation level collision mixin */                          \
    typedef uSisBroadCollisionSimLevelMixin_serial<t_SisGlue>       \
        sim_level_collision_mixin_t;                                \
    /* Sample level collision mixin */                              \
    typedef uSisBroadCollisionMixin<t_SisGlue, uBroadPhase_t> collision_mixin_t

/**
 * The default specification for common mixins shared by the energy-based null
 * model for multiple loci, heterogeneous chromatin nodes (nodes may have
 * varying diameter). The null model is used for generating uniformly sampled
 * chains without interaction constraints.
 */
#define U_SIS_DECLARE_LMRK_BOLTZ_MLOC_HETR_NULL_MODEL_ATTRIBS(t_SisGlue) \
    U_SIS_DECLARE_LMRK_MLOC_HETR_ATTRIBS(t_SisGlue);                     \
    /* Simulation level chrome-to-chrome interaction mixin */            \
    typedef uSisNullIntrChrSimLevelMixin<t_SisGlue>                      \
        sim_level_intr_chr_mixin_t;                                      \
    /* Sample level chrome-to-chrome interaction mixin */                \
    typedef uSisNullIntrChrMixin<t_SisGlue> intr_chr_mixin_t;            \
    /* Simulation lamina interaction mixin */                            \
    typedef uSisNullIntrLamSimLevelMixin<t_SisGlue>                      \
        sim_level_intr_lam_mixin_t;                                      \
    /* Sample level lamina interaction mixin */                          \
    typedef uSisNullIntrLamMixin<t_SisGlue> intr_lam_mixin_t;            \
    /* Simulation level nuclear body interaction mixin */                \
    typedef uSisNullIntrNucbSimLevelMixin<t_SisGlue>                     \
        sim_level_intr_nucb_mixin_t;                                     \
    /* Sample level nuclear body interaction mixin */                    \
    typedef uSisNullIntrNucbMixin<t_SisGlue> intr_nucb_mixin_t;          \
    /* Simulation level energy mixin */                                  \
    typedef uSisSeoBendEnergySimLevelMixin_serial<t_SisGlue>             \
        sim_level_energy_mixin_t;                                        \
    /* Sample level energy mixin */                                      \
    typedef uSisSeoBendEnergyMixin<t_SisGlue> energy_mixin_t;            \
    /* Seed mixin */                                                     \
    typedef uSisSeoUnifCubeSeedMixin<t_SisGlue> seed_mixin_t

#endif  // uLmrkUtils_h
