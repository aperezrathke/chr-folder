//****************************************************************************
// uSisIntrNucbBudgUtil.h
//****************************************************************************

/**
 * Utility header for budgeted nuclear body interactions
 */

#ifndef uSisIntrNucbBudgUtil_h
#define uSisIntrNucbBudgUtil_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// Typedefs and structs
//****************************************************************************

/**
 * Information needed for failed interaction removal at selected candidate
 */
typedef struct {
    /**
     * Interaction column index
     */
    uUInt intr_id;
    /**
     * Locus identifier corresponding to interaction fragment
     */
    uUInt frag_lid;
} uSisIntrNucbBudgFail_t;

/**
 * List of failed interactions at a single candidate
 */
typedef std::vector<uSisIntrNucbBudgFail_t> uSisIntrNucbBudgFailList_t;

/**
 * Table of failed interactions for all candidates, where each row is list of
 *  failed interactions at a single candidate
 */
typedef std::vector<uSisIntrNucbBudgFailList_t> uSisIntrNucbBudgFailTable_t;

/**
 * Budgeted nuclear body interaction failure policy arguments, stores scratch
 *  buffers for tracking failed interactions at all candidates
 * @WARNING - DO NOT CHANGE THE ORDER OF THESE DATA MEMBERS WITHOUT ALSO
 *  CHANGING ALL INSTANCES WHERE THIS STRUCTURE IS INITIALIZED!
 */
typedef struct {
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-in, nuclear body interaction failures at each candidate
     */
    uUIVecCol& kin_delta_fails;
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-out, nuclear body interaction failures at each candidate
     */
    uUIVecCol& ko_delta_fails;
    /**
     * Handle to scratch table for storing failed knock-in, nuclear body
     *  interactions at each candidate position
     */
    uSisIntrNucbBudgFailTable_t& kin_fail_table;
    /**
     * Handle to scratch table for storing failed knock-in, nuclear body
     *  interactions at each candidate position
     */
    uSisIntrNucbBudgFailTable_t& ko_fail_table;
} uSisIntrNucbBudgFailPolicyArgs_t;

/**
 * Nuclear body knock-in budget access policy
 */
template <typename t_SisGlue>
class uSisIntrNucbBudgAccKin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_nucb_kin_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

/**
 * Nuclear body knock-out budget access policy
 */
template <typename t_SisGlue>
class uSisIntrNucbBudgAccKo {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_nucb_ko_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

#endif  // uSisIntrNucbBudgUtil_h
