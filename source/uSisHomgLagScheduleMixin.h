//****************************************************************************
// uSisHomgLagScheduleMixin.h
//****************************************************************************

/**
 * ScheduleMixin = A mixin that defines the checkpoint schedule for
 * a quality control algorithm.
 */

#ifndef uSisHomgLagScheduleMixin_h
#define uSisHomgLagScheduleMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uTypes.h"

/**
 * A quality control schedule is responsible for determining when check
 * points occur.
 *
 * A homogeneous lag schedule always sets a checkpoint on every i-th
 *  grow phase iteration.
 *
 * This schedule is deterministic and supports landmark trial runner
 *  tr_find_first() and tr_find_next() calls.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing from configuration file
    enum uOptE e_Key = uOpt_qc_schedule_lag_slot0>
class uSisHomgLagScheduleMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin avoids recomputing exponential
     * weights
     */
    enum { calculates_norm_exp_weights = false };

    /**
     * The default lag value
     */
    enum { default_lag = 1 };

    /**
     * Constructor
     */
    uSisHomgLagScheduleMixin() { clear(); }

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     * @param is_tr - [unused] set to true if schedule is being used for
     *  landmark trial running, false o/w
     */
    void init(const sim_t& sim,
              uSpConstConfig_t config,
              const bool is_tr U_THREAD_ID_PARAM) {
        clear();
        // Check if option exists in config
        config->read_into<uUInt>(m_lag, e_Key);
        const uUInt MIN_LAG = is_tr ? U_TO_UINT(2) : U_TO_UINT(1);
        if (m_lag < MIN_LAG) {
            uLogf("Warning: homogeneous schedule lag < %d, setting to %d.\n",
                  (int)MIN_LAG,
                  (int)MIN_LAG);
            m_lag = MIN_LAG;
        }
        // We require that our lag value be positive
        uAssert((m_lag >= MIN_LAG) && (MIN_LAG >= U_TO_UINT(1)));
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config,
                    const bool is_tr U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() { m_lag = default_lag; }

    /**
     * Quality control interface
     * Determines if a quality control check point has been reached (meaning
     * it's okay to go ahead and perform quality control step).
     * @param out_norm_exp_weights - an optional *uninitialized* vector of
     *  exponential weights that must be initialized if:
     *      calculates_norm_exp_weights == TRUE
     * @param out_max_log_weight - an optional *uninitialized* reference to the
     *  maximum log weight within the log_weights vector. Must be initialized
     *  if: calculates_norm_exp_weights == TRUE
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin
     * @param sim - parent simulation
     */
    inline uBool qc_checkpoint_reached(
        uVecCol& out_norm_exp_weights,
        uReal& out_max_log_weight,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_level_qc_mixin_t& qc,
        const sim_t& sim) const {
        return ((num_completed_grow_steps_per_sample % m_lag) == 0);
    }

    /**
     * Trial runner interface
     * @return first landmark grow step count
     */
    inline uUInt tr_find_first(const uUInt max_grow_steps) const {
        // Trial runner expects at least 2 steps
        uAssert(m_lag > U_TO_UINT(1));
        return std::min(m_lag, max_grow_steps);;
    }

    /**
     * Trial runner interface
     * @param completed_grow_steps - number of grow steps already completed
     * @param max_grow_steps - maximum number of grow steps allowed
     * @return next landmark grow step count
     */
    inline uUInt tr_find_next(const uUInt completed_grow_steps,
                              const uUInt max_grow_steps) const {
        const uUInt next_grow_steps =
            U_NEXT_MULTIPLE(completed_grow_steps, m_lag);
        return std::min(next_grow_steps, max_grow_steps);
    }

private:
    /**
     * A check point is encountered based on multiples of this value. For
     * example, if lag value is 1, then a check point occurs after every
     * grow step.
     */
    uUInt m_lag;
};

#endif  // uSisHomgLagScheduleMixin_h
