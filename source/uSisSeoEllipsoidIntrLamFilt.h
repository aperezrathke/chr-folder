//****************************************************************************
// uSisSeoEllipsoidIntrLamFilt.h
//****************************************************************************

/**
 * @brief Lamina interaction filters for ellipsoidal nucleus
 */

#ifndef uSisSeoEllipsoidIntrLamFilt_h
#define uSisSeoEllipsoidIntrLamFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisIntrLamCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Filters
//****************************************************************************

/**
 * Effectively namespace for lamina-interaction knock-in utilities assuming an
 *  ellipsoidal nucleus
 */
template <typename t_SisGlue>
class uSisSeoEllipsoidIntrLamKinFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrLamCommonFilt<glue_t> intr_lam_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within an ellipsoidal nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED SCHUR (ELEMENT-WISE) PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrLamFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(schur_buffer.n_rows));
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Determine distance threshold
        const uReal dsurf_pad_fc2c = calc_dist_check(c, sample, sim);
        // Verify that candidate position schur product has been cached
        uAssert(intr_lam_common_filt_t::check_schur(
            U_SIS_SEED_CANDIDATE_ID, schur_buffer, node_center.memptr()));
        // Get schur product of candidate bead center position
        const uReal x2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_X);
        const uReal y2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Y);
        const uReal z2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Z);
        uAssert(x2 >= U_TO_REAL(0.0));
        uAssert(y2 >= U_TO_REAL(0.0));
        uAssert(z2 >= U_TO_REAL(0.0));
        // Test: constraint may be satisfiable - must be on our outside of
        //  shrunken volume
        uBool b_okay =
            intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
                x2, y2, z2, dsurf_pad_fc2c, sim);
        if (!b_okay) {
            // Let policy have final say
            b_okay = policy.intr_lam_kin_on_fail(
                p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within a spherical nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED SCHUR (ELEMENT-WISE) PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrLamFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine distance threshold
        const uReal dsurf_pad_fc2c = calc_dist_check(c, sample, sim);
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssert(schur_buffer.n_cols == candidate_centers.n_cols);
        uAssert(schur_buffer.n_rows == candidate_centers.n_rows);
        uAssert(schur_buffer.size() == candidate_centers.size());
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }
            // Verify that candidate position schur product has been cached
            uAssert(intr_lam_common_filt_t::check_schur(
                i, schur_buffer, candidate_centers));
            // Test if interaction can be satisfied
            const uReal x2 = schur_buffer.at(i, uDim_X);
            const uReal y2 = schur_buffer.at(i, uDim_Y);
            const uReal z2 = schur_buffer.at(i, uDim_Y);
            uAssert(x2 >= U_TO_REAL(0.0));
            uAssert(y2 >= U_TO_REAL(0.0));
            uAssert(z2 >= U_TO_REAL(0.0));
            uBool b_okay =
                intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
                    x2, y2, z2, dsurf_pad_fc2c, sim);
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_lam_kin_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-in lamina interaction constraint
     * under assumptions of single-end, ordered (SEO) growth and ellipsoidal
     * nucleus
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrLamFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        if (!intr_lam_common_filt_t::is_in_frag(
                c.node_nid, c.frag_nid_lo, c.frag_nid_hi)) {
            return uFALSE;
        }
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(
            candidate_id, U_TO_UINT(0), U_TO_UINT(schur_buffer.n_rows));
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Verify schur product of selected node was cached
        uAssert(intr_lam_common_filt_t::check_schur(
            U_TO_MAT_SZ_T(candidate_id), schur_buffer, B));
        // Get schur product of selected bead center position
        const uReal x2 = schur_buffer.at(candidate_id, uDim_X);
        uAssert(x2 >= U_TO_REAL(0.0));
        const uReal y2 = schur_buffer.at(candidate_id, uDim_Y);
        uAssert(y2 >= U_TO_REAL(0.0));
        const uReal z2 = schur_buffer.at(candidate_id, uDim_Z);
        uAssert(z2 >= U_TO_REAL(0.0));
        // Get constrained interaction distance relative to nuclear surface;
        // for heterogeneous case, this value must be padded by radius of
        // tested interacting fragment node
        const uReal dsurf_pad =
            (sample_t::is_homg_radius)
                ? sim.get_intr_lam_kin_dist(c.intr_id)
                : (sim.get_intr_lam_kin_dist(c.intr_id) +
                   sample.get_node_radius(c.frag_nid_hi, sim));
        uAssert(dsurf_pad > U_TO_REAL(0.0));
        // Test if satisfied, must be on or outside shrunken volume
        const uBool result =
            intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
                x2, y2, z2, dsurf_pad, sim);
        // @TODO - If we are last node of fragment, this should always be
        //  satisfied, else we would have been marked as violator!
        uAssert(result || (c.node_nid < c.frag_nid_hi));
        return result;
    }

private:
    /**
     * Computes interaction distance threshold
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return Distance threshold
     */
    static inline uReal calc_dist_check(const uSisIntrLamFilterCore_t& c,
                                        const sample_t& sample,
                                        const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of last bead of constrained fragment (where
        // tested bead index <= last bead of constrained fragment index)
        uAssert(c.node_nid <= c.frag_nid_hi);
        const uReal fc2c =
            sample.get_max_c2c_len(c.node_nid, c.frag_nid_hi, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        uAssert(sample.get_node_radius(c.frag_nid_hi, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance relative to nuclear surface;
        // for heterogeneous case, this value must be padded by radius of last
        // node of interacting fragment
        const uReal dsurf_pad_fc2c =
            (sample_t::is_homg_radius)
                ? (sim.get_intr_lam_kin_dist(c.intr_id) + fc2c)
                : (sim.get_intr_lam_kin_dist(c.intr_id) +
                   sample.get_node_radius(c.frag_nid_hi, sim) + fc2c);
        uAssert(dsurf_pad_fc2c > U_TO_REAL(0.0));
        return dsurf_pad_fc2c;
    }

    // Disallow any form of instantiation
    uSisSeoEllipsoidIntrLamKinFilt(const uSisSeoEllipsoidIntrLamKinFilt&);
    uSisSeoEllipsoidIntrLamKinFilt& operator=(
        const uSisSeoEllipsoidIntrLamKinFilt&);
};

/**
 * Effectively namespace for lamina-interaction knock-out utilities assuming a
 *  spherical nucleus
 */
template <typename t_SisGlue>
class uSisSeoEllipsoidIntrLamKoFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrLamCommonFilt<glue_t> intr_lam_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within an ellipsoidal nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED SCHUR (ELEMENT-WISE) PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrLamFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine distance threshold
        uAssert(c.node_nid <= c.frag_nid_lo);
        const uReal dsurf_pad_fc2c =
            calc_dist_check(c, c.frag_nid_lo, sample, sim);
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(schur_buffer.n_rows));
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Verify that candidate position schur product has been cached
        uAssert(intr_lam_common_filt_t::check_schur(
            U_SIS_SEED_CANDIDATE_ID, schur_buffer, node_center.memptr()));
        // Get schur product of candidate bead center position
        const uReal x2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_X);
        const uReal y2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Y);
        const uReal z2 = schur_buffer.at(U_SIS_SEED_CANDIDATE_ID, uDim_Z);
        uAssert(x2 >= U_TO_REAL(0.0));
        uAssert(y2 >= U_TO_REAL(0.0));
        uAssert(z2 >= U_TO_REAL(0.0));
        // Test: constraint may be satisfiable - must be w/in shrunken volume
        uBool b_okay =
            !intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
                x2, y2, z2, dsurf_pad_fc2c, sim);
        if (!b_okay) {
            // Let policy have final say
            b_okay = policy.intr_lam_ko_on_fail(
                p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within an ellipsoidal nucleus
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED SCHUR (ELEMENT-WISE) PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrLamFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine first "active" bead index of interacting fragment
        const uUInt active_nid = std::max(c.node_nid, c.frag_nid_lo);
        uAssert(active_nid <= c.frag_nid_hi);
        // Determine distance threshold
        const uReal dsurf_pad_fc2c =
            calc_dist_check(c, active_nid, sample, sim);
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssert(schur_buffer.n_cols == candidate_centers.n_cols);
        uAssert(schur_buffer.n_rows == candidate_centers.n_rows);
        uAssert(schur_buffer.size() == candidate_centers.size());
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }
            // Verify that candidate position schur product has been cached
            uAssert(intr_lam_common_filt_t::check_schur(
                i, schur_buffer, candidate_centers));
            const uReal x2 = schur_buffer.at(i, uDim_X);
            const uReal y2 = schur_buffer.at(i, uDim_Y);
            const uReal z2 = schur_buffer.at(i, uDim_Z);
            uAssert(x2 >= U_TO_REAL(0.0));
            uAssert(y2 >= U_TO_REAL(0.0));
            uAssert(z2 >= U_TO_REAL(0.0));
            // Test: constraint may be satisfiable - must be w/in shrunken
            // volume
            uBool b_okay =
                !intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
                    x2, y2, z2, dsurf_pad_fc2c, sim);
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_lam_ko_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-out lamina interaction constraint
     * under assumptions of single-end, ordered (SEO) growth and ellipsoidal
     * nucleus
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrLamFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        uAssert(c.node_nid <= c.frag_nid_hi);
        if (c.node_nid != c.frag_nid_hi) {
            // Knock-out interaction cannot be satisfied until entire fragment
            // has been grown
            return uFALSE;
        }
#ifdef U_BUILD_ENABLE_ASSERT
        // Assumes element wise product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uMatrix& schur_buffer =
            sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(
            candidate_id, U_TO_UINT(0), U_TO_UINT(schur_buffer.n_rows));
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        // Verify schur product of selected node was cached
        uAssert(intr_lam_common_filt_t::check_schur(
            U_TO_MAT_SZ_T(candidate_id), schur_buffer, B));
        // Get schur product of selected bead center position
        const uReal x2 = schur_buffer.at(candidate_id, uDim_X);
        uAssert(x2 >= U_TO_REAL(0.0));
        const uReal y2 = schur_buffer.at(candidate_id, uDim_Y);
        uAssert(y2 >= U_TO_REAL(0.0));
        const uReal z2 = schur_buffer.at(candidate_id, uDim_Z);
        uAssert(z2 >= U_TO_REAL(0.0));
        // Get constrained interaction distance relative to nuclear surface;
        // for heterogeneous case, this value must be padded by radius of
        // tested interacting fragment node
        const uReal dsurf_pad =
            (sample_t::is_homg_radius)
                ? sim.get_intr_lam_ko_dist(c.intr_id)
                : (sim.get_intr_lam_ko_dist(c.intr_id) +
                   sample.get_node_radius(c.frag_nid_hi, sim));
        uAssert(dsurf_pad > U_TO_REAL(0.0));
        // Test if satisfied, must be within shrunken volume
        uAssert(!intr_lam_common_filt_t::is_on_or_outside_shrunken_ellip_vol(
            x2, y2, z2, dsurf_pad, sim));
#endif  // U_BUILD_ENABLE_ASSERT
        // The method can_satisf(frag_nid_hi) should be equivalent to
        // is_satisf(frag_nid_hi), therefore no further validation should be
        // necessary!
        return uTRUE;
    }

private:
    /**
     * Computes interaction distance threshold
     * @param c - Core filter arguments
     * @param active_nid - max(current nid, frag_nid_lo)
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return Distance threshold
     */
    static inline uReal calc_dist_check(const uSisIntrLamFilterCore_t& c,
                                        const uUInt active_nid,
                                        const sample_t& sample,
                                        const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of first "active" bead of the constrained
        // fragment, where first "active" bead is:
        //  - if candidate node_nid < fragment nid_lo -> fragment nid_lo
        //  - else -> candidate node_nid, as candidate is assumed to be
        //      within the interacting fragment
        uAssert(c.node_nid <= active_nid);
        uAssertBoundsInc(active_nid, c.frag_nid_lo, c.frag_nid_hi);
        const uReal fc2c = sample.get_max_c2c_len(c.node_nid, active_nid, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        // If tested node is "active", then fc2c should be 0!
        uAssert((c.node_nid != active_nid) || (fc2c == U_TO_REAL(0.0)));
        uAssert(sample.get_node_radius(active_nid, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance relative to nuclear surface;
        // for heterogeneous case, this value must be padded by radius of
        // active node of interacting fragment and the padded result MAY BE
        // NEGATIVE
        const uReal dsurf_pad_fc2c =
            (sample_t::is_homg_radius)
                ? (sim.get_intr_lam_ko_dist(c.intr_id) - fc2c)
                : (sim.get_intr_lam_ko_dist(c.intr_id) +
                   sample.get_node_radius(active_nid, sim) - fc2c);
        return dsurf_pad_fc2c;
    }

    // Disallow any form of instantiation
    uSisSeoEllipsoidIntrLamKoFilt(const uSisSeoEllipsoidIntrLamKoFilt&);
    uSisSeoEllipsoidIntrLamKoFilt& operator=(
        const uSisSeoEllipsoidIntrLamKoFilt&);
};

#endif  // uSisSeoEllipsoidIntrLamFilt_h
