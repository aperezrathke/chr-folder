//****************************************************************************
// uSisParallelRegrowthRcMixin.h
//****************************************************************************

/**
 * ParalellRegrowthRcMixin = A mixin that implements a parallel regrowth
 * procedure during rejection control. For more info on rejection control:
 *
 * Liu, Jun S., Rong Chen, and Wing Hung Wong. "Rejection control and
 *  sequential importance sampling." Journal of the American Statistical
 *  Association 93.443 (1998): 1022-1031.
 */

#ifndef uSisParallelRegrowthRcMixin_h
#define uSisParallelRegrowthRcMixin_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_THREADS

#include "uSisNullQcModMixin.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Performs batch regrowth operations in parallel
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisParallelRegrowthRcMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

private:
    /**
     * Class defines what work should be done by the current thread
     */
    class regrowth_work_t : public uMappableWork {
    public:
        /**
         * Constructor - initializes targets
         */
        regrowth_work_t(uWrappedBitset& status,
                        std::vector<sample_t>& samples,
                        uVecCol& log_weights,
                        sim_level_qc_mixin_t& qc,
                        sim_t& sim,
                        const uWrappedBitset& should_regrow,
                        const uUInt target_num_grow_steps)
            : m_status(status),
              m_samples(samples),
              m_log_weights(log_weights),
              m_qc(qc),
              m_sim(sim),
              m_cached_num_samples_to_regrow(U_TO_UINT(should_regrow.count())),
              m_target_num_grow_steps(target_num_grow_steps) {
            uAssert(target_num_grow_steps > 0);

            // Note: bit sets writes are not thread safe, so we need to
            // convert to a std::vector. However, we only care about the true
            // bits so we also need to keep track of the bit index of each
            // true bit.

            uAssert(m_cached_num_samples_to_regrow > 0);
            m_atomic_status.resize(m_cached_num_samples_to_regrow, uFALSE);
            m_status_index.reserve(m_cached_num_samples_to_regrow);

            for (size_t i = should_regrow.find_first(); i != should_regrow.npos;
                 i = should_regrow.find_next(i)) {
                // Keep track of true bit indices
                m_status_index.push_back(U_TO_UINT(i));
            }
        }

        /**
         * Regrows samples over parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         */
        virtual uBool do_range(const uUInt ix_start,
                               const uUInt count,
                               const uUInt thread_id) {
            uAssertPosEq(m_cached_num_samples_to_regrow, m_status_index.size());
            uAssert(m_cached_num_samples_to_regrow == m_atomic_status.size());
            const uUInt end =
                std::min(ix_start + count, m_cached_num_samples_to_regrow);

            uUInt ix_target = 0;
            for (uUInt i = ix_start; i < end; ++i) {
                uAssertBounds(i, 0, m_status_index.size());
                uAssertBounds(i, 0, m_atomic_status.size());

                ix_target = m_status_index[i];

                uAssertBounds(ix_target, 0, m_samples.size());
                uAssertBounds(ix_target, 0, m_log_weights.size());

                // regrow sample
                m_atomic_status[i] =
                    m_qc.rc_regrow(m_samples[ix_target],
                                   m_log_weights.at(U_TO_MAT_SZ_T(ix_target)),
                                   m_sim,
                                   m_target_num_grow_steps,
                                   ix_target,
                                   thread_id);

            }  // end iteration over regrown samples

            return (end < m_cached_num_samples_to_regrow);
        }

        /**
         * Needed to convert internal status buffer representation back to a
         * (non-thread safe) bit set representation
         */
        void finalize_status() {
            uAssertPosEq(m_cached_num_samples_to_regrow, m_status_index.size());
            uAssert(m_cached_num_samples_to_regrow == m_atomic_status.size());

            uUInt ix_target = 0;
            for (size_t i = 0; i < m_cached_num_samples_to_regrow; ++i) {
                uAssertBounds(i, 0, m_status_index.size());
                uAssertBounds(i, 0, m_atomic_status.size());

                ix_target = m_status_index[i];

                uAssertBounds(ix_target, 0, m_status.size());

                m_status.set(ix_target, (uTRUE == m_atomic_status[i]));
            }
        }

    private:
        /**
         * Stores the bit index of i-th status field within m_status. Used
         * for converting from m_atomic_status (a std::vector) back to
         * m_status (a bit set) - these values are the same as the true bit
         * indices within should_regrow constructor parameter.
         */
        std::vector<uUInt> m_status_index;

        /**
         * Stores the final status of all samples marked for regrowth. This is
         * a thread safe write structure but it needs to be converted back to
         * a bit set after we've finished parallel processing
         */
        std::vector<uBool> m_atomic_status;

        /**
         * The final output status of all samples for this rejection control
         * checkpoint
         */
        uWrappedBitset& m_status;

        /**
         * The set of all samples, only those indexed by m_status_index will
         * be regrown
         */
        std::vector<sample_t>& m_samples;

        /**
         * The log weights for all samples. Only those samples that are
         * regrown will be modified.
         */
        uVecCol& m_log_weights;

        /**
         * Our rejection control quality control handle
         */
        sim_level_qc_mixin_t& m_qc;

        /**
         * The parent simulation
         */
        sim_t& m_sim;

        /**
         * The cached number of samples to be regrown
         */
        const uUInt m_cached_num_samples_to_regrow;

        /**
         * The number of seed + grow_step calls to complete for each sample
         */
        const uUInt m_target_num_grow_steps;
    };

    /**
     * Defines the chunk size - the amount of work each thread grabs and
     * must complete before grabbing another chunk of work
     */
    enum { inc_by = 8 };

public:
    /**
     * @WARNING - ONLY CALL FROM MAIN THREAD!
     * Regrows all samples for which should_regrow is true
     * @param status - the current growth status of all samples. Only samples
     *  that are regrown will have their status updated
     * @param samples - samples that need to be regrown
     * @param log_weights - the log weights of all samples. Only samples that
     *  are regrown will have their log weights updated
     * @param qc - the parent quality control mixin
     * @param sim - the parent simulation
     * @param should_regrow - i-th sample is regrown if i-th bit is 1
     * @param target_num_grow_steps - the number of grow steps to complete
     */
    void rc_batch_regrow(uWrappedBitset& status,
                         std::vector<sample_t>& samples,
                         uVecCol& log_weights,
                         sim_level_qc_mixin_t& qc,
                         sim_t& sim,
                         const uWrappedBitset& should_regrow,
                         const uUInt target_num_grow_steps U_THREAD_ID_PARAM) {
        // @TODO - if calling from non-main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);

        regrowth_work_t work(status,
                             samples,
                             log_weights,
                             qc,
                             sim,
                             should_regrow,
                             target_num_grow_steps);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        uParMap.wait_until_work_finished(work, inc_by);

        work.finalize_status();
    }
};

#else  // !defined (U_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef U_BUILD_COMPILER_MSVC
    #pragma message( \
        "Warning: parallel regrowth disabled. Defaulting to serial method.")
#elif defined(U_BUILD_COMPILER_GCC)
    #pragma message \
        "Warning: parallel regrowth disabled. Defaulting to serial method."
#endif  // compiler check

#include "uSisSerialRegrowthRcMixin.h"

#define uSisParallelRegrowthRcMixin uSisSerialRegrowthRcMixin

#endif  // U_BUILD_ENABLE_THREADS

#endif  // uSisParallelRegrowthRcMixin_h
