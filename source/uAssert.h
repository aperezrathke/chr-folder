//****************************************************************************
// uAssert.h
//****************************************************************************

/**
 * @brief Assertion hooks
 *
 * uAssert(expr_) - If assertions are enabled, expr_ is evaluated and
 *  execution is halted if expr_ == false. If assertions are disabled, then
 *  this code is a no-op and has no performance penalty.
 *
 * uVerify(expr_) - If assertions are enabled, then will behave exactly as
 *  uAssert. If assertions are disabled, then will still evaluate expr_ but
 *  program will no longer halt if expr_ evaluates to false.
 */

#ifndef uAssert_h
#define uAssert_h

#include "uBuild.h"
#include "uTypes.h"

/**
 * Make sure that NDEBUG is defined if asserts are not enabled. C++ standard
 * specifies to define NDEBUG if assertions are to be turned off.
 */
#ifndef U_BUILD_ENABLE_ASSERT
#   ifndef NDEBUG
#       define NDEBUG
#   endif  // NDEBUG
#endif  // U_BUILD_ENABLE_ASSERT

/**
 * Add compiler messages on MSVC and GCC compilers to let us know that
 * assertions are enabled.
 */
#if !defined(NDEBUG) || defined(U_BUILD_ENABLE_ASSERT)
#   ifdef U_BUILD_COMPILER_MSVC
#       pragma message("Assertions enabled.")
#   elif (defined(U_BUILD_COMPILER_GCC) || defined(U_BUILD_COMPILER_ICC))
#       pragma message "Assertions enabled."
#   endif  // compiler check
#endif  // !defined(NDEBUG) || defined(U_BUILD_ENABLE_ASSERT)

#include <assert.h>

//****************************************************************************
// Override
//****************************************************************************

#ifdef U_BUILD_COMPILER_MSVC
#   ifdef _WIN64
#       include <intrin.h>
#   endif  // _WIN64
#endif  // U_BUILD_COMPILER_MSVC

/**
 * This part is ugly, but, at the end of the day, to toggle assertions, just
 * define U_BUILD_ENABLE_ASSERT within uBuild.h or through compiler command
 * line and this part will override (if need be) to give the desired behavior.
 *
 * We want to route to the std assert implementation in the following
 * conditions:
 * - Asserts are disabled via undefined U_BUILD_ENABLE_ASSERT directive
 * - or standard NDEBUG preprocessor directive is not defined and asserts are
 *   enabled
 */
#if (!defined(U_BUILD_ENABLE_ASSERT) || \
     (defined(U_BUILD_ENABLE_ASSERT) && !defined(NDEBUG)))
// Defer to std assert implementation
#   define uAssert assert
#else
/**
 * Else, in CMake build system, it's hard to modify the NDEBUG flag which is
 * always defined in release builds. So if we really want asserts in release
 * mode, we need to 'override' by routing to our own version.
 */
inline void uAssert(const bool expr) {
    if (!expr) {
        /**
         * Call compiler/platform specific break routine. Note: asm int 3
         * is inline assembly to call interrupt 3 which usually
         * programmatically adds a breakpoint (or traps to the debugger)
         */
#   ifdef U_BUILD_COMPILER_MSVC
        /**
         * https://msdn.microsoft.com/en-us/library/45yd4tzz.aspx
         * see also __debugbreak() intrinsic:
         * https://msdn.microsoft.com/en-us/library/f408b4et.aspx
         */
#       ifdef _WIN64
        // inline assembly is not supported for 64-bit architectures
        __debugbreak();
#       else
        __asm int 3;
#       endif  // _WIN64
#   elif defined(U_BUILD_COMPILER_GCC)
        // http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
        __asm__("int $3");
#   elif defined(U_BUILD_COMPILER_ICC)
        /**
         * Intel compiler supports both microsoft and gcc style asm tags
         * but recommends gcc/gnu-style
         * https://software.intel.com/en-us/node/513428
         */
        __asm__("int $3");
#   else
#       error Unrecognized platform
#endif  // compiler check
    }
}
#endif  // override assert

//****************************************************************************
// Macros
//****************************************************************************

// Additional includes for assertion utilities
#ifdef U_BUILD_ENABLE_ASSERT
#   include <cmath>
#   include <limits>
#endif  // U_BUILD_ENABLE_ASSERT

/**
 * Asserts that a == b and a > 0 (is positive)
 */
#define uAssertPosEq(a, b) uAssert(((a) > 0) && ((a) == (b)))

/**
 * Bounds checking that element i is within [l, u)
 */
#define uAssertBounds(i_, l_, u_) uAssert(((i_) >= (l_)) && ((i_) < (u_)))

/**
 * Bounds checking inclusive that element i is within [l, u]
 */
#define uAssertBoundsInc(i_, l_, u_) uAssert(((i_) >= (l_)) && ((i_) <= (u_)))

//****************************************************************************
// Macros for floating-point numbers
//****************************************************************************

/**
 * Asserts on floating point equality within a threshold
 */
#define uAssertRealEq(a, b) uAssert(U_REAL_CMP_EQ((a), (b)))

/**
 * Asserts on floating point a <= b within a threshold
 */
#define uAssertRealLte(a, b) uAssert(U_REAL_CMP_LTE((a), (b)))

/**
 * Asserts on floating point a >= b within a threshold
 */
#define uAssertRealGte(a, b) uAssert(U_REAL_CMP_GTE((a), (b)))

/**
 * Asserts that a == b and a > 0 (is positive) for real values
 */
#define uAssertRealPosEq(a, b)         \
    do {                               \
        uAssert((a) > U_TO_REAL(0.0)); \
        uAssertRealEq((a), (b));       \
    } while (0)

/**
 * Bounds checking that real element i is within [l, u)
 */
#define uAssertRealBounds(i_, l_, u_) \
    do {                              \
        uAssertRealGte((i_), (l_));   \
        uAssert((i_) < (u_));         \
    } while (0)

/**
 * Bounds checking inclusive that real element i is within [l, u]
 */
#define uAssertRealBoundsInc(i_, l_, u_) \
    do {                                 \
        uAssertRealGte((i_), (l_));      \
        uAssertRealLte((i_), (u_));      \
    } while (0)

//****************************************************************************
// Verify
//****************************************************************************

/**
 * Code within a verify(<code>) block will always be executed even if asserts
 * are disabled. However, if asserts are enabled, it will additionally trigger
 * an assert if code evaluates to a false conditional.
 */
#ifdef U_BUILD_ENABLE_ASSERT
#   define uVerify uAssert
#else
#   define uVerify(expr_) expr_
#endif  // U_BUILD_ENABLE_ASSERT

#endif  // uAssert_h
