//****************************************************************************
// uSisSelectUnif.h
//****************************************************************************

#ifndef uSisSelectUnif_h
#define uSisSelectUnif_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uGlobals.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

#include <vector>

/**
 * Uniformly selects a single sample among a set of samples
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisSelectUnif {
public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Uniformly selects among a set of samples
     * @param log_weights - Log weight at each sample (all weights are assumed
     *  to be valid)
     * @param samples_unused - set of samples to select from
     * @param completed_grow_steps_unused - Number of completed grow steps for
     *  each sample in 'samples' set
     * @param log_p_select - The output log of the selection probability
     * @return Unsigned integer index of selected sample
     */
    template <typename t_sample>
    static inline uUInt select_index(
        const uVecCol& log_weights,
        const std::vector<t_sample>& samples_unused,
        const uUInt completed_grow_steps_unused,
        uReal& log_p_select U_THREAD_ID_PARAM) {
        uAssert(log_weights.size() >= 1);
        log_p_select = log(U_TO_REAL(1.0) / U_TO_REAL(log_weights.n_elem));
        // Select a random candidate
        return uRng.unif_int<uUInt>(0, (log_weights.n_elem - 1));
    }

    // Interface expected by landmark trial runner (null)
    void init(const sim_t&, uSpConstConfig_t U_THREAD_ID_PARAM) {}
    void clear() {}
    void reset_for_next_trial(const sim_t&,
                              uSpConstConfig_t U_THREAD_ID_PARAM) {}
};

#endif  // uSisSelectUnif_h
