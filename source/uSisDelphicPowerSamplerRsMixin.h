//****************************************************************************
// uSisDelphicPowerSamplerRsMixin.h
//****************************************************************************

/**
 * SamplerRsMixin = A mixin that defines how samples are chosen when a resample
 * checkpoint is encountered
 */

#ifndef uSisDelphicPowerSamplerRsMixin_h
#define uSisDelphicPowerSamplerRsMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uSisPowerSamplerRsMixin.h"
#include "uSisUtilsQc.h"
#include "uTypes.h"

/**
 * All sample look-ahead weights are raised to a power 'alpha' and then
 *  selected with probability proportional to transformed weight.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // Summarizes log weights at look-ahead ensembles to a scalar
    // e.g. log(mean weight), log(median weight)
    typename t_Summary,
    // Sub-simulation type
    typename t_DelphicSubSim,
    // Defines method to(T1 from_, T2 to_) which
    // converts sample of type T1 to sample of type T2
    // See uSisSampleCopier_unsafe
    typename t_DelphicSampleCopyier,
    // Configuration key for look-ahead steps
    // e.g. uOpt_delphic_steps_slot0
    enum uOptE e_Key_steps,
    // Configuration key for power exponent 'alpha'
    // e.g uOpt_delphic_power_alpha_slot0
    enum uOptE e_Key_alpha>
class uSisDelphicPowerSamplerRsMixin
    : private uSisPowerSamplerRsMixin<t_SisGlue, e_Key_alpha> {
private:
    /**
     * Typedefs
     */
    typedef t_Summary uDelphicSummary;
    typedef t_DelphicSubSim uDelphicSubSim;
    typedef typename uDelphicSubSim::sample_t uDelphicSubSample;
    typedef t_DelphicSampleCopyier uDelphicSampleCopyier;
    /**
     * PIMPL sampler
     */
    typedef uSisPowerSamplerRsMixin<t_SisGlue, e_Key_alpha> t_PowUtilBase;

public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin ensures the parameter
     * norm exponential weights are valid
     */
    enum { needs_norm_exp_weights = false };

    /**
     * Default constructor
     */
    uSisDelphicPowerSamplerRsMixin() { clear(); }

    /**
     * Initializes sampler
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        ////////////////////////////////////////////////////////////
        // Initialize look-ahead steps
        config->read_into(this->m_delphic_steps, e_Key_steps);
        // Warn user if Delphic step count is 0
        if (this->m_delphic_steps == U_TO_UINT(0)) {
            uLogf("Warning: Delphic power sampler steps == 0.\n");
        }
        // Warn user if large Delphic step count used
        else if (this->m_delphic_steps > U_SIS_DELPHIC_WARN_STEPS_THRESH) {
            uLogf("Warning: Delphic power sampler steps > %d.\n",
                  (int)U_SIS_DELPHIC_WARN_STEPS_THRESH);
        }
        ////////////////////////////////////////////////////////////
        // Initialize alpha exponent (power weight smoothing util)
        t_PowUtilBase::init(sim, config U_THREAD_ID_ARG);
        ////////////////////////////////////////////////////////////
        // Handle to sub-sim configuration, initialize according to:
        //      1) quality control child configuration
        //      2) else, default to (parent) parameter configuration
        uSpConstConfig_t sub_sim_cfg = config;
        // Check if quality control child exists
        if (config->get_child_qc()) {
            sub_sim_cfg = config->get_child_qc();
        } else {
            uLogf(
                "Warning: Delphic power sampler unable to detect suitable "
                "child configuration for sub sim, deferring to parent.\n");
        }
        ////////////////////////////////////////////////////////////
        // Initialize sub-simulation
        m_delphic_sub_sim.init(sub_sim_cfg U_THREAD_ID_ARG);
        // Assume parent and child sim grow step counts match
        const uUInt MAX_GROW_STEPS_PARENT = sim.get_max_num_grow_steps(sim);
        const uUInt MAX_GROW_STEPS_CHILD =
            m_delphic_sub_sim.get_max_num_grow_steps(m_delphic_sub_sim);
        if (MAX_GROW_STEPS_PARENT != MAX_GROW_STEPS_CHILD) {
            // THIS IS NOT FOOLPROOF AS IT'S THEORETICALLY POSSIBLE (BUT NOT
            // CURRENTLY POSSIBLE) THAT PARENT AND CHILD SIMULATIONS MAY HAVE
            // THE SAME TOTAL NUMBER OF GROW STEPS BUT DIFFERING INTERMEDIATE
            // MONOMER COUNTS. HOWEVER, THIS IS A SIMPLE CHECK THAT WILL
            // PROBABLY TRAP MOST CASES. IN GENERAL, NEED TO STANDARDIZE GROW
            // STEP WITH MONOMER COUNT TO REALLY FIX ANY POSSIBLE DESCREPANCY
            uLogf(
                "Error: Parent and child simulation grow step count mismatch. "
                "Exiting.\n");
            exit(uExitCode_error);
        }
        ////////////////////////////////////////////////////////////
        // Initialize Delphic ensemble summary weights buffer
        uAssert(sim.get_target_total_num_samples() > 0);
        m_delphic_norm_exp_weights.zeros(sim.get_target_total_num_samples());
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {
        t_PowUtilBase::init_light(sim, config U_THREAD_ID_ARG);
        m_delphic_sub_sim.init(U_THREAD_ID_0_ARG);
        m_delphic_norm_exp_weights.zeros(sim.get_target_total_num_samples());
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_delphic_steps = U_SIS_DELPHIC_DEFAULT_STEPS;
        t_PowUtilBase::clear();
        m_delphic_sub_sim.clear();
        m_delphic_norm_exp_weights.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        t_PowUtilBase::reset_for_next_trial(sim, config U_THREAD_ID_ARG);
        m_delphic_sub_sim.init(U_THREAD_ID_0_ARG);
        m_delphic_norm_exp_weights.zeros(sim.get_target_trial_num_samples());
    }

    /**
     * Performs resampling and modifies status and log weights
     * @param out_status - 1 if sample is still able to grow, 0 otherwise
     * @param out_log_weights - The current log weight of each sample, will
     *  be replaced by modified weight of chosen sample
     * @param out_samples - The current set of samples to resample from. The
     *  final set of selected samples will overwrite this vector.
     * @param norm_exp_weights - The vector of exp transformed weights
     *  normalized by the max weight - should be in (0,1]. Weights will no
     *  longer be representative of sample after this function finishes.
     * @param max_log_weight - The unnormalized maximal log weight. Weight
     *  will no longer be representative of sample after this function
     *  finishes.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - The parent quality control mixin
     * @param sim - The parent simulation
     */
    void qc_resample(uWrappedBitset& out_status,
                     uVecCol& out_log_weights,
                     std::vector<sample_t>& out_samples,
                     const uVecCol& norm_exp_weights,
                     const uReal max_log_weight,
                     const uUInt num_completed_grow_steps_per_sample,
                     const sim_level_qc_mixin_t qc,
                     const sim_t& sim U_THREAD_ID_PARAM) {
        // Assume positive number of samples and parallel log weights
        const size_t NUM_SAMPLES = out_samples.size();
        uAssertPosEq(out_log_weights.n_elem, U_TO_MAT_SZ_T(NUM_SAMPLES));
        // Compute target grow step count
        uAssert(num_completed_grow_steps_per_sample <=
                m_delphic_sub_sim.get_max_num_grow_steps(m_delphic_sub_sim));
        const uUInt target_grow_steps = std::min(
            num_completed_grow_steps_per_sample + m_delphic_steps,
            m_delphic_sub_sim.get_max_num_grow_steps(m_delphic_sub_sim));
        uAssert(target_grow_steps >= num_completed_grow_steps_per_sample);
        // Ensure buffer is correct size
        m_delphic_norm_exp_weights.set_size(out_log_weights.n_elem);
        uReal delphic_max_log_weight = U_TO_REAL(0.0);
        // Avoid Delphic sub-simulation if step size is 0
        if (num_completed_grow_steps_per_sample >= target_grow_steps) {
            if (sim_level_qc_mixin_t::calculates_norm_exp_weights) {
                // Avoid exponential transform if already computed
                m_delphic_norm_exp_weights = norm_exp_weights;
                delphic_max_log_weight = max_log_weight;
            } else {
                uSisUtilsQc::calc_norm_exp_weights(m_delphic_norm_exp_weights,
                                                   delphic_max_log_weight,
                                                   out_log_weights);
            }
        } else {
            // Perform Delphic look-ahead summary at each sample:
            //  m_delphic_norm_exp_weights will transiently store log(summary)
            for (size_t i = 0; i < NUM_SAMPLES; ++i) {
                uAssertBounds(i, 0, out_samples.size());
                // Get template sample and log weight
                const sample_t& sample = out_samples[i];
                uDelphicSubSample sub_sample_template;
                m_delphic_sample_copier.to(sample /*from*/,
                                           sub_sample_template /*to*/);
                uAssertBounds(i, 0, (size_t)out_log_weights.n_elem);
                const uReal log_weight = out_log_weights.at(U_TO_MAT_SZ_T(i));
                // Perform Delphic look-ahead simulation
                m_delphic_sub_sim.init(U_THREAD_ID_0_ARG);
                m_delphic_sub_sim.run_from_template_until(
                    target_grow_steps,
                    sub_sample_template,
                    num_completed_grow_steps_per_sample,
                    log_weight U_THREAD_ID_ARG);
                const uReal summary = uDelphicSummary::summarize(
                    m_delphic_sub_sim.get_completed_log_weights_view());
                uAssertBounds(i, 0, (size_t)m_delphic_norm_exp_weights.n_elem);
                m_delphic_norm_exp_weights.at(i) = summary;
            }
            // NOTE: m_delphic_norm_exp_weights is actually in log space, let's
            // convert from log(summary) to normalized exp(log(summary)))
            delphic_max_log_weight = m_delphic_norm_exp_weights.max();
            m_delphic_norm_exp_weights -= max_log_weight;
            m_delphic_norm_exp_weights =
                uMatrixUtils::exp(m_delphic_norm_exp_weights);
        }
        // Defer to PIMPL power sampler
        t_PowUtilBase::qc_resample(out_status,
                                   out_log_weights,
                                   out_samples,
                                   m_delphic_norm_exp_weights,
                                   delphic_max_log_weight,
                                   num_completed_grow_steps_per_sample,
                                   qc,
                                   sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Number of look-ahead grow steps
     */
    uUInt m_delphic_steps;

    /**
     * Sub-simulation for estimating look-ahead summaries
     */
    uDelphicSubSim m_delphic_sub_sim;

    /**
     * Used for converting between parent and child simulation sample types
     */
    uDelphicSampleCopyier m_delphic_sample_copier;

    /**
     * Internal buffer of exponential weights (derived from Delphic look-ahead
     * log_weights summary). These weights are normalized relative to a
     * maximum log weight.
     */
    uVecCol m_delphic_norm_exp_weights;
};

#endif  // uSisDelphicPowerSamplerRsMixin_h
