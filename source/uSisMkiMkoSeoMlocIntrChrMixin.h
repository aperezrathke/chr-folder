//****************************************************************************
// uSisMkiMkoSeoMlocIntrChrMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Mloc: Multiple loci
 *
 * Defines a general chrome-to-chrome interaction mixin supporting knock-in
 * and knock-out constraints over multiple (or single) loci.
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * and/or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisMkiMkoSeoMlocIntrChrMixin_h
#define uSisMkiMkoSeoMlocIntrChrMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibBudgUtil.h"
#include "uSisNullIntrChrSeedPolicy.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisMkiMkoSeoMlocIntrChrSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisMkiMkoSeoMlocIntrChrMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Chr-chr knock-in budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_chr_budg_acc_kin_t;

    /**
     * Chr-chr knock-out budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_chr_budg_acc_ko_t;

    /**
     * Batch interactions utility
     */
    typedef uSisIntrChrCommonBatch<t_SisGlue> intr_chr_batch_util_t;

    /**
     * Chr-chr interactions seed policy for first node
     */
    typedef uSisNullIntrChrSeedPolicy_t intr_chr_seed_first_policy_t;

    /**
     * Chr-chr interactions seed policy after first node placed
     */
    typedef struct {
        /**
         * Chr-chr interactions should be updated
         */
        enum { should_update = true };

        /**
         * @param sample - Sample being seeded
         * @param nfo - Common seed information
         * @param sim - Outer simulation
         * @return uTRUE if seed can satisfy interactions, uFALSE o/w
         */
        static uBool is_seed_okay(sample_t& sample,
                                  const uSisUtils::seed_info_t& nfo,
                                  const sim_t& sim U_THREAD_ID_PARAM) {
            // Test if interactions can be satisfied
            return intr_chr_batch_util_t::is_seed_okay(
                sample,
                (void*)NULL,
                sample.m_intr_chr_loci_tables,
                nfo,
                sample,
                sim U_THREAD_ID_ARG);
        }
    } intr_chr_seed_next_policy_t;

    /**
     * Default constructor
     */
    uSisMkiMkoSeoMlocIntrChrMixin() { this->clear(); }

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        m_intr_chr_loci_tables = sim.get_intr_chr_loci_tables();
    }

    /**
     * Resets to default state
     */
    void clear() { intr_chr_batch_util_t::clear(m_intr_chr_loci_tables); }

    /**
     * ASSUMES sequential growth from first to last node. Determines which
     * candidate positions violate (i.e. cannot possibly satisfy)
     * chrome-to-chrome interaction constraints.
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *  positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void intr_chr_filter(uBoolVecCol& out_legal_candidates_primed,
                                const uMatrix& candidate_centers,
                                const uReal candidate_radius,
                                const uUInt parent_node_id,
                                const sample_t& sample,
                                const sim_t& sim U_THREAD_ID_PARAM) {
        // Mark candidates that violate constraints
        intr_chr_batch_util_t::mark_viol(*this,
                                         (void*)NULL,
                                         out_legal_candidates_primed,
                                         m_intr_chr_loci_tables,
                                         candidate_centers,
                                         candidate_radius,
                                         parent_node_id,
                                         sample,
                                         sim);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_kin_on_fail(void* const p_policy_args,
                                      const uUInt candidate_id,
                                      const uSisIntrChrFilterCore_t& c,
                                      const sample_t& sample,
                                      const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_ko_on_fail(void* const p_policy_args,
                                     const uUInt candidate_id,
                                     const uSisIntrChrFilterCore_t& c,
                                     const sample_t& sample,
                                     const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - a unique chromatin node identifier
     * @param B - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param candidate_id - identifies candidate that was selected
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    inline void intr_chr_update(const uUInt node_id,
                                const uReal* B,
                                const uReal radius,
                                const uUInt candidate_id,
                                const sample_t& sample,
                                const sim_t& sim U_THREAD_ID_PARAM) {
        // Remove satisfied interactions from active loci tables
        intr_chr_batch_util_t::remove_satisf(m_intr_chr_loci_tables,
                                             node_id,
                                             B,
                                             radius,
                                             candidate_id,
                                             sample,
                                             sim);
    }

    // Accessors

    /**
     * @return Chr-chr interaction seed policy for first seeded node
     */
    intr_chr_seed_first_policy_t get_intr_chr_seed_first_policy() {
        return intr_chr_seed_first_policy_t();
    }

    /**
     * @return Chr-chr interaction seed policy for nodes seeded after first
     */
    intr_chr_seed_next_policy_t get_intr_chr_seed_next_policy() {
        return intr_chr_seed_next_policy_t();
    }

private:
    /**
     * Collection of active interactions partitioned by loci
     */
    uSisIntrChrLociTables_t m_intr_chr_loci_tables;
};

#endif  // uSisMkiMkoSeoMlocIntrChrMixin_h
