//****************************************************************************
// uSisUtilsGrowth.h
//****************************************************************************

/**
 * Common utilities for sequential importance sampling of chromatin chains
 */

#ifndef uSisUtilsGrowth_h
#define uSisUtilsGrowth_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitset.h"
#include "uDistUtils.h"
#include "uGlobals.h"
#include "uSisFailStatus.h"
#include "uSisUtilsCollision.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypeTraits.h"
#include "uTypes.h"

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro to inject convenient typedefs into simulation, sample, and mixins
 */
#define U_SIS_INJECT_TYPEDEFS(t_SisGlue)                                       \
    typedef t_SisGlue glue_t;                                                  \
    typedef typename t_SisGlue::sim_t sim_t;                                   \
    typedef typename t_SisGlue::sample_t sample_t;                             \
    typedef typename t_SisGlue::trial_runner_mixin_t trial_runner_mixin_t;     \
    typedef typename t_SisGlue::sim_level_qc_mixin_t sim_level_qc_mixin_t;     \
    typedef typename t_SisGlue::sim_level_node_radius_mixin_t                  \
        sim_level_node_radius_mixin_t;                                         \
    typedef typename t_SisGlue::node_radius_mixin_t node_radius_mixin_t;       \
    typedef                                                                    \
        typename t_SisGlue::sim_level_growth_mixin_t sim_level_growth_mixin_t; \
    typedef typename t_SisGlue::growth_mixin_t growth_mixin_t;                 \
    typedef typename t_SisGlue::sim_level_nuclear_mixin_t                      \
        sim_level_nuclear_mixin_t;                                             \
    typedef typename t_SisGlue::nuclear_mixin_t nuclear_mixin_t;               \
    typedef typename t_SisGlue::sim_level_collision_mixin_t                    \
        sim_level_collision_mixin_t;                                           \
    typedef typename t_SisGlue::collision_mixin_t collision_mixin_t;           \
    typedef typename t_SisGlue::sim_level_intr_chr_mixin_t                     \
        sim_level_intr_chr_mixin_t;                                            \
    typedef typename t_SisGlue::intr_chr_mixin_t intr_chr_mixin_t;             \
    typedef typename t_SisGlue::sim_level_intr_lam_mixin_t                     \
        sim_level_intr_lam_mixin_t;                                            \
    typedef typename t_SisGlue::intr_lam_mixin_t intr_lam_mixin_t;             \
    typedef typename t_SisGlue::sim_level_intr_nucb_mixin_t                    \
        sim_level_intr_nucb_mixin_t;                                           \
    typedef typename t_SisGlue::intr_nucb_mixin_t intr_nucb_mixin_t;           \
    typedef                                                                    \
        typename t_SisGlue::sim_level_energy_mixin_t sim_level_energy_mixin_t; \
    typedef typename t_SisGlue::energy_mixin_t energy_mixin_t;                 \
    typedef typename t_SisGlue::seed_mixin_t seed_mixin_t

/**
 * Seed node candidate identifier
 */
#define U_SIS_SEED_CANDIDATE_ID U_TO_UINT(0)

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility namespace
 */
namespace uSisUtils {

/**
 * Common seed information
 */
typedef struct {
    // Seed node center position
    const uVecCol* p_node_center;
    // Seed node identifier
    uUInt nid;
    // Seed node locus
    uUInt lid;
    // Seed node radius
    uReal node_radius;
} seed_info_t;

/**
 * Null seed clash policy, interface for testing if seed node is clash free
 */
class SeedFirstClashPolicy {
public:
    /**
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return Always returns TRUE!
     */
    template <typename sim_t>
    static inline uBool is_seed_okay(const seed_info_t& nfo,
                                     const sim_t& sim_unused) {
        return uTRUE;
    }
};

/**
 * Full seed clash policy, rejects seed node if it clashes with previous nodes
 */
class SeedNextClashPolicy {
public:
    /**
     * Constructor - initialize handle to set of existing nodes to check
     *  collisions against
     * @param clash_nodes - Column major matrix of clash nodes. Each column is
     *  expected to be (X, Y, Z, R) tuple where (X, Y, Z) is a node center and
     *  R is the node radius.
     */
    explicit SeedNextClashPolicy(const uMatrix& clash_nodes)
        : m_clash_nodes(clash_nodes) {}
    /**
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return TRUE if point does not collide with any other existing nodes,
     *  FALSE o/w
     */
    template <typename sim_t>
    inline uBool is_seed_okay(const seed_info_t& nfo, const sim_t& sim) const {
        uAssert(nfo.p_node_center);
        const uVecCol& node_center = (*nfo.p_node_center);
        // Note: deferring to inhomogeneous collision because expect clash
        // nodes to be small set which may make SSE overhead drown out any
        // possible performance increase from homogeneous collisions.
        return uSisUtils::narrow_phase_inhomogeneous_sphere_collision(
            &(node_center.at(0)), nfo.node_radius, m_clash_nodes, sim);
    }

private:
    /**
     * Set of existing nodes to check for clashes against
     */
    const uMatrix& m_clash_nodes;
};

/**
 * @param num_grown_nodes - number of successfully grown nodes
 * @param sim - parent simulation
 * @return TRUE if sample will still need additional grow steps after
 * the current seed/grow step finishes, FALSE otherwise
 */
template <typename sim_t>
inline bool will_still_need_growing(const uUInt num_grown_nodes,
                                    const sim_t& sim) {
    uAssertBoundsInc(num_grown_nodes, 0, sim.get_max_total_num_nodes());
    return num_grown_nodes < sim.get_max_total_num_nodes();
}

/**
 * Function selects a random candidate that has a 1 value from
 * parameter [0,1] vector
 * @WARNING - ASSUMES AT LEAST 1 LEGAL CANDIDATE!
 * @param is_legal - [0,1] vector : 1 if candidate is legal for
 *  selection, 0 otherwise
 * @param num_legal - the number of 1s in legal_candidates (must be > 0)
 * @return Index of selected candidate.
 */
inline uUInt runif_select_legal_growth_candidate(const uBoolVecCol& is_legal,
                                                 const uUInt num_legal
                                                     U_THREAD_ID_PARAM) {
    // Bounds checking
    uAssert(is_legal.size() > 0);
    uAssert(uMatrixUtils::sum(is_legal) == num_legal);
    // This method assumes at least a single legal candidate
    uAssertBounds(num_legal, 1, is_legal.size() + 1);

    // Select a random candidate
    const uUInt winner = uRng.unif_int<uUInt>(1, num_legal);
    // Find index of winning candidate
    uUInt num_legal_encountered = 0;
    const uUInt num_total_candidates = U_TO_UINT(is_legal.size());
    for (uUInt i = 0; i < num_total_candidates; ++i) {
        uAssert(is_legal(i) == 0 || is_legal(i) == 1);
        num_legal_encountered += is_legal.at(i);
        if (num_legal_encountered == winner) {
            return i;
        }
    }
    uAssert(false && "should never reach here");
    return 0xDEADBEEF;
}

/**
 * Updates sample mixin states with new node information
 * @param node_id - a unique chromatin node identifier
 * @param point - 3D coordinates of node centroid
 * @param radius - the radius of the node to add
 * @param candidate_id - identifies candidate that was selected
 * @param sample - the sample containing this mixin
 * @param sim - the parent simulation of parameter sample
 */
template <typename sample_t, typename sim_t, bool should_update_intr_chr>
inline void on_node_grown(const uUInt node_id,
                          const uReal* point,
                          const uReal radius,
                          const uUInt candidate_id,
                          sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
    // Update collision state
    uAssert(radius > U_TO_REAL(0.0));
    sample.collision_add(node_id, point, radius, sample, sim);

    // Conditionally update chr-chr interaction state - does not need to be
    //  updated if only a single node exists! This should be a compile time
    //  branch and should have no runtime overhead.
    if (should_update_intr_chr) {
        sample.intr_chr_update(
            node_id, point, radius, candidate_id, sample, sim U_THREAD_ID_ARG);
    }

    // Update lamina interaction state
    sample.intr_lam_update(
        node_id, point, radius, candidate_id, sample, sim U_THREAD_ID_ARG);

    // Update nuclear body interaction state
    sample.intr_nucb_update(
        node_id, point, radius, candidate_id, sample, sim U_THREAD_ID_ARG);
}

/**
 * Determines which positions should be considered for growing chain from
 * parameter node. These positions have not been checked for nuclear
 * constraint satisfaction or self-avoidance.
 * @param out_candidate_positions - the output unfiltered 3-D positions of
 *  each candidate node center
 * @param p_parent_node_center - the 3-D position of the parent node
 * @param candidate_radius - the radius of each candidate node. Assumes
 *  all candidates have same radius.
 * @param parent_radius - the radius of the parent node
 * @param sphere_sample_points - matrix of basis points on unit sphere
 *  of same dimensions as out_candidate_positions. Defines where candidate
 *  positions are placed relative to parent node.
 */
inline void calc_candidate_positions(uMatrix& out_candidate_positions,
                                     const uReal* const p_parent_node_center,
                                     const uReal candidate_radius,
                                     const uReal parent_radius,
                                     const uMatrix& sphere_sample_points) {
    // Check validity of parent node memory as best we can
    uAssert(NULL != p_parent_node_center);
    // Assuming row-wise point storage for sphere sample points
    uAssert(sphere_sample_points.n_cols == uDim_num);
    uAssert(sphere_sample_points.size() > 0);

    // Convert column-based node center to row-based node center so that
    // we can use the resulting row vector for translation of the sphere
    // points (as sphere points are row-wise)
    const uVecRow parent_node_center(
        // BEGIN HACK
        // Unfortunately, no easy way to convert from a column vector
        // view to a row vector view - so we have to cast away the const
        // temporarily
        const_cast<uReal*>(p_parent_node_center), /*aux_mem*/
        // END HACK
        uDim_num, /*aux_length*/
        false,    /*copy_aux_mem*/
        true);    /*strict*/

    // Obtain reference to buffer for storing scaled, translated from node
    // center candidate positions
    uMatrix& scale_trans_buffer = out_candidate_positions;
    uAssert(scale_trans_buffer.n_cols == sphere_sample_points.n_cols);
    uAssert(scale_trans_buffer.n_rows == sphere_sample_points.n_rows);
    uAssert(scale_trans_buffer.size() == sphere_sample_points.size());

    // Copy sphere points into buffer
    memcpy(scale_trans_buffer.memptr(),
           sphere_sample_points.memptr(),
           scale_trans_buffer.size() * sizeof(uReal));

    // Scale each point by radial sum
    const uReal scalef = candidate_radius + parent_radius;
    scale_trans_buffer *= scalef;

    // Finally, translate each point by the node center
    scale_trans_buffer.each_row() += parent_node_center;
}

/**
 * Performs common growth operations
 * @param parent_node_id - node identifier to perform growth ops at
 * @param out_candidate_positions - reference to candidate growth
 *  positions (candidate node centers)
 * @param out_legal_candidates - reference to [0,1] vector where i-th
 *  element is 0 if it satisfies all constraints, 0 otherwise
 * @param candidate_radius - radius of candidate node to grow
 * @param parent_radius - radius of parent node that we are growing
 *  candidate from
 * @param sample - sample that is being grown
 * @param sim - parent simulation of sample being grown
 * @return The number of legal growth candidates
 */
template <typename sample_t, typename sim_t>
inline uUInt get_legal_growth_candidates_at(const uUInt parent_node_id,
                                            uMatrix& out_candidate_positions,
                                            uBoolVecCol& out_legal_candidates,
                                            const uReal candidate_radius,
                                            const uReal parent_radius,
                                            sample_t& sample,
                                            const sim_t& sim
                                                U_THREAD_ID_PARAM) {
    {
        U_SCOPED_STAT_TIMER(uSTAT_GetCandidateNodePositionsTime);

        // Obtain candidate node centers relative to current node
        sample.calc_candidate_positions(out_candidate_positions,
                                        parent_node_id,
                                        candidate_radius,
                                        parent_radius,
                                        sample,
                                        sim U_THREAD_ID_ARG);
    }

    {
        U_SCOPED_STAT_TIMER(uSTAT_NuclearFilterTime);

        // See which candidates are confined within nucleus
        sample.nuclear_filter(out_legal_candidates,
                              out_candidate_positions,
                              candidate_radius,
                              sample,
                              sim U_THREAD_ID_ARG);
    }

    {
        U_SCOPED_STAT_TIMER(uSTAT_IntrLamFilterTime);

        // See which candidates satisfy lamina interactions
        sample.intr_lam_filter(out_legal_candidates,
                               out_candidate_positions,
                               candidate_radius,
                               parent_node_id,
                               sample,
                               sim U_THREAD_ID_ARG);
    }

    {
        U_SCOPED_STAT_TIMER(uSTAT_IntrNucbFilterTime);

        // See which candidates satisfy nuclear body interactions
        sample.intr_nucb_filter(out_legal_candidates,
                                out_candidate_positions,
                                candidate_radius,
                                parent_node_id,
                                sample,
                                sim U_THREAD_ID_ARG);
    }

    {
        U_SCOPED_STAT_TIMER(uSTAT_CollisionFilterTime);

        // See which candidates are self-avoiding
        sample.collision_filter(out_legal_candidates,
                                out_candidate_positions,
                                candidate_radius,
                                parent_node_id,
                                parent_radius,
                                sample,
                                sim U_THREAD_ID_ARG);
    }

    {
        U_SCOPED_STAT_TIMER(uSTAT_IntrChrFilterTime);

        // See which candidates satisfy pairwise chromatin interactions
        sample.intr_chr_filter(out_legal_candidates,
                               out_candidate_positions,
                               candidate_radius,
                               parent_node_id,
                               sample,
                               sim U_THREAD_ID_ARG);
    }

    // Determine which candidates satisfy all constraints
    return uMatrixUtils::sum(out_legal_candidates);
}

/**
 * Performs brute-force all-to-all narrow-phase collisions and possibly other
 * structural checks to make sure chain growth is proceeding as expected.
 * @param active_nodes - bit set with 1 if node should be checked against
 *  for constraint satisfaction, 0 otherwise
 * @param sample - the sample to check
 * @param sim - the parent simulation of the sample
 * @return TRUE (1) if all checks pass, FALSE (0) otherwise
 */
template <typename sample_t, typename sim_t>
uBool satisfies_robust_constraint_checks(const uBitset& active_nodes,
                                         const sample_t& sample,
                                         const sim_t& sim) {
    // This is a very expensive check, so only perform if enabled.
#ifndef U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS
    return uTRUE;
#endif  // U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS

    // This check only applies to non-dead chains
    // The previous iteration should have already validated this chain
    if (sample.is_dead()) {
        return uTRUE;
    }

    // Assume each column is a node position
    uAssert(sample.get_node_positions().n_rows == uDim_num);
    uAssert(sample.get_node_positions().n_cols == active_nodes.size());

    // Defragment
    const size_t n_nodes = active_nodes.count();
    // Allocate extra row dimension for node radius
    uMatrix node_positions(uDim_num_extended, U_TO_MAT_SZ_T(n_nodes));
    uMatrixUtils::uword itr = 0;
    for (uBitset::size_type i = active_nodes.find_first(); i != uBitset::npos;
         i = active_nodes.find_next(i)) {
        uReal* const dst = node_positions.colptr(itr++);
        const uReal* const src =
            sample.get_node_positions().colptr(U_TO_MAT_SZ_T(i));
        dst[uDim_X] = src[uDim_X];
        dst[uDim_Y] = src[uDim_Y];
        dst[uDim_Z] = src[uDim_Z];
        dst[uDim_radius] = sample.get_node_radius(U_TO_UINT(i), sim);
    }
    uAssert(itr == U_TO_MAT_SZ_T(n_nodes));

    uReal node_radius, allowed_nuclear_sq_radius = U_TO_REAL(0.0);
    uBool b_is_valid = uTRUE;
    uMatrixUtils::uword i;

    for (i = 0; i < U_TO_MAT_SZ_T(n_nodes - 1); ++i) {
        // Bounds checking
        uAssertBounds(i, 0, node_positions.n_cols);
        uAssert((i + 1) < node_positions.n_cols);

        // Determine radius of current node
        node_radius = node_positions.colptr(i)[uDim_radius];

        // Create cropped view of remaining nodes
        uAssert((node_positions.n_cols - i - 1) > 0);
        const uMatrix node_view(node_positions.colptr(i + 1),
                                node_positions.n_rows,
                                node_positions.n_cols - i - 1,
                                false, /*copy_aux_mem*/
                                true); /*strict*/

        // See if we satisfy self-avoidance
        const uReal* const node_center = node_positions.colptr(i);
        b_is_valid = narrow_phase_inhomogeneous_sphere_collision(
            node_center, /*candidate_center*/
            node_radius, /*candidate_radius*/
            node_view,   /*node_centers*/
            sim);
        uAssert(b_is_valid);

        // Break out of loop if any of those checks failed
        if (!b_is_valid) {
            break;
        }
    }

    return b_is_valid;
}

/**
 * Performs incremental locus chain seo growth. The term "seo" stands for
 * "single-end, ordered" meaning that nodes are grown from a single end and
 * that nodes within a locus chain are grown in order from first to last.
 * "Ordered" also means that node identifiers are sequential where next
 * monomer identifier is one beyond the previous node identifier within the
 * same locus chain. The template parameter "should_grow_2nd" must be true
 * if growing second node in locus chain, false o/w.
 * @param sample - Parent sample containing this mixin
 * @param log_weight - The current and output log weight of the sample
 * @param num_grown_nodes - Number of successfully grown nodes
 * @param parent_node_id - Identifier of parent node from which to place
 *  candidate
 * @param sim - Parent simulation containing global sample data
 * @return FALSE if sample died during growth, TRUE o/w
 */
template <typename sample_t, typename sim_t, bool should_grow_2nd>
inline bool grow_after_1st_seo(sample_t& sample,
                               uReal& log_weight,
                               const uUInt num_grown_nodes,
                               const uUInt parent_node_id,
                               const sim_t& sim U_THREAD_ID_PARAM) {
    uAssert(!sample.is_dead());
    uAssertBounds(num_grown_nodes, 1, sim.get_max_total_num_nodes());

    // Determine which node to grow chromatin chain from
    // @WARNING - THIS ONLY HOLDS IF GROWTH IS ORDERED: next node identifier
    //  in locus chain is assumed to be directly after parent node identifier
    uAssertBounds(parent_node_id, 0, (sim.get_max_total_num_nodes() - 1));
    const uUInt next_node_id = parent_node_id + 1;

    // Determine candidate node centers:

    // We assume buffer for storing candidate positions resides at
    // simulation level
    uMatrix& candidate_positions =
        sim.get_candidate_positions(U_THREAD_ID_0_ARG);

    // Similarly, buffer for storing which candidates are legal is
    // assumed to reside at simulation level
    uBoolVecCol& legal_candidates = sim.get_legal_candidates(U_THREAD_ID_0_ARG);

    const uReal candidate_node_radius =
        sample.get_candidate_node_radius(next_node_id, sim);

    const uUInt num_legal = uSisUtils::get_legal_growth_candidates_at(
        parent_node_id,
        candidate_positions,
        legal_candidates,
        candidate_node_radius,
        sample.get_node_radius(parent_node_id, sim),
        sample,
        sim U_THREAD_ID_ARG);

    // See if we can grow anywhere
    if (num_legal > 0) {
        // Hurray! We have room to grow - lets select a legal position
        // randomly
        uAssert(candidate_positions.n_rows == legal_candidates.size());
        uAssert(candidate_positions.n_cols == uDim_num);
        uAssert(sample.get_node_positions().n_rows == uDim_num);

        // Defer to energy policy for candidate selection
        const uUInt idx = should_grow_2nd
                              ? sample.energy_select_2nd(sample,
                                                         log_weight,
                                                         parent_node_id,
                                                         candidate_positions,
                                                         candidate_node_radius,
                                                         legal_candidates,
                                                         num_legal,
                                                         sim U_THREAD_ID_ARG)
                              : sample.energy_select_nth(sample,
                                                         log_weight,
                                                         parent_node_id,
                                                         candidate_positions,
                                                         candidate_node_radius,
                                                         legal_candidates,
                                                         num_legal,
                                                         sim U_THREAD_ID_ARG);

        // Set selected candidate as part of chain
        uReal* const dst = sample.get_node_positions().colptr(next_node_id);
        uAssert(dst != NULL);
        uAssertBounds(idx, 0, candidate_positions.n_rows);
        dst[uDim_X] = candidate_positions.at(idx, uDim_X);
        dst[uDim_Y] = candidate_positions.at(idx, uDim_Y);
        dst[uDim_Z] = candidate_positions.at(idx, uDim_Z);

        // Verify growth is proceeding as expected
        uAssert(sample.check_growth(num_grown_nodes + 1, sample, sim));

        // Inform sample mixins that node was added to chain
        uSisUtils::
            on_node_grown<sample_t, sim_t, true /*should_update_intr_chr*/>(
                next_node_id,
                dst,
                candidate_node_radius,
                idx,
                sample,
                sim U_THREAD_ID_ARG);
    } else {
        // We have nowhere to grow!
        sample.mark_dead(uSisFail_CONS, log_weight);
        return false;
    }

    // Sample is not dead!
    return true;
}

}  // namespace uSisUtils

#endif  // uSisUtilsGrowth_h
