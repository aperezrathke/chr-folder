//****************************************************************************
// uTestSisLmrkSelDelPow.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * using a landmark trial runner with Delphic power selection policy.
 *
 * Usage:
 * -test_sis_lmrk_sel_del_pow
 */

#ifdef uTestSisLmrkSelDelPow_h
#   error "Test Sis Lmrk Sel Del Pow included multiple times!"
#endif  // uTestSisLmrkSelDelPow_h
#define uTestSisLmrkSelDelPow_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Child & parent simulation null quality control mixin
#include "uSisNullQcSimLevelMixin.h"

// Landmark trial runner mixin
#include "uSisHomgLagScheduleMixin.h"
#include "uSisLandmarkTrialRunnerMixin.h"
#include "uSisQcSummaryMeanWeight.h"
#include "uSisSelectDelphicPower.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"
#include "uSisSerialBatchGrower.h"

#include "uMockUpUtils.h"

#include <iostream>
#include <string>

namespace {
/**
 * Wire together rejection control child simulation
 */
class uTestSisLmrkDelPowSelChildSimGlue {
public:
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_NULL_MODEL_ATTRIBS(
        uTestSisLmrkDelPowSelChildSimGlue);

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisLmrkDelPowSelChildSimGlue,
        uSisSerialBatchGrower<uTestSisLmrkDelPowSelChildSimGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisNullQcSimLevelMixin<uTestSisLmrkDelPowSelChildSimGlue>
        sim_level_qc_mixin_t;
};

/**
 * Wire together landmark trial runner parent simulation
 */
class uTestSisLmrkDelPowSelParentSimGlue {
public:
    // WARNING: We want parent and child sample types to match to
    // allow *unsafe* copying!
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_NULL_MODEL_ATTRIBS(
        uTestSisLmrkDelPowSelParentSimGlue);

    // Trial runner mixin
    typedef uSisLandmarkTrialRunnerMixin<
        uTestSisLmrkDelPowSelParentSimGlue,
        uSisSelectDelphicPower<
            uTestSisLmrkDelPowSelParentSimGlue,
            uSisQcSummaryMeanWeight,
            uSisSubSimAccess_threaded<uTestSisLmrkDelPowSelChildSimGlue::sim_t>,
            uSisSampleCopier_unsafe,
            uOpt_delphic_steps_slot0,
            uOpt_delphic_power_alpha_slot0>,
        uSisHomgLagScheduleMixin<uTestSisLmrkDelPowSelParentSimGlue,
                                 uOpt_qc_schedule_lag_slot0>,
        uSisSubSimAccess_threaded<uTestSisLmrkDelPowSelChildSimGlue::sim_t>,
        uSisSampleCopier_unsafe,
        uSisParallelBatchGrower<uTestSisLmrkDelPowSelParentSimGlue>,
        // enable heartbeat logging for trial runner
        true>
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisNullQcSimLevelMixin<uTestSisLmrkDelPowSelParentSimGlue>
        sim_level_qc_mixin_t;
};

}  // end of anonymous namespace

/**
 * Entry point for test
 */
int uTestSisLmrkSelDelPowMain(const uCmdOptsMap& cmd_opts) {
    // Macro for top-level simulation
    typedef uTestSisLmrkDelPowSelParentSimGlue::sim_t parent_sim_t;

    // Common settings
    const uReal MAX_NODE_DIAMETER = U_TO_REAL(300.0);
    const uReal NUCLEAR_DIAMETER = U_TO_REAL(9000.0);
    const std::vector<uUInt> NUM_NODES(1 /*n_chains*/, 150 /*n_nodes*/);
    const uUInt NUM_UNIT_SPHERE_SAMPLE_POINTS = 100;

    // Create a parent configuration
    uSpConfig_t parent_config = uSmartPtr::make_shared<uConfig>();
    parent_config->output_dir = std::string("null");
    parent_config->max_trials = U_TO_UINT(3);
    parent_config->ensemble_size = U_TO_UINT(20);
    parent_config->set_option(uOpt_max_node_diameter, MAX_NODE_DIAMETER);
    parent_config->set_option(uOpt_nuclear_diameter, NUCLEAR_DIAMETER);
    parent_config->num_nodes = NUM_NODES;
    parent_config->num_unit_sphere_sample_points =
        NUM_UNIT_SPHERE_SAMPLE_POINTS;

    // Configure parent landmark schedule
    const uUInt LANDMARK_LAG = U_TO_UINT(20);
    parent_config->set_option(uOpt_qc_schedule_type_slot0, "homg_lag");
    parent_config->set_option(uOpt_qc_schedule_lag_slot0, LANDMARK_LAG);

    // Configure parent landmark Delphic power selection
    const uUInt DELPHIC_SELECT_STEPS = 10;
    const uReal DELPHIC_SELECT_POW_ALPHA = U_TO_REAL(1.0);
    parent_config->set_option(uOpt_delphic_steps_slot0, DELPHIC_SELECT_STEPS);
    parent_config->set_option(uOpt_delphic_power_alpha_slot0,
                              DELPHIC_SELECT_POW_ALPHA);

    // Create child configuration for trial runner
    uSpConfig_t child_config_tr = uSmartPtr::make_shared<uConfig>();
    // Register child configuration for trial runner
    parent_config->set_child_tr(child_config_tr,
                                uTRUE /*should_copy_named_vars_to_child*/);
    child_config_tr->ensemble_size = U_TO_UINT(10);

    // Create child configuration for trial runner selection (Delphic power)
    uSpConfig_t child_config_tr_sel = uSmartPtr::make_shared<uConfig>();
    // Register child configuration for trial runner selection
    parent_config->set_child_tr_sel(child_config_tr_sel, uTRUE);
    child_config_tr_sel->ensemble_size = U_TO_UINT(20);

    // Output configuration
    std::cout << "Running Test Sis Lrmk Sel Del Pow." << std::endl;
    parent_config->print();

    // Configure number of worker threads
    // Uncomment below to manually configure thread count:
    // U_SET_NUM_WORKER_THREADS(parent_config, 7);

    // Initialize globals
    uG::init(parent_config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    parent_sim_t sim(parent_config U_MAIN_THREAD_ID_ARG);

    // Do rejection control simulation!
    const uBool result = sim.run();

    // Compute rejection control effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());

    std::cout << "Lmrk simulation completed with status: " << result
              << std::endl;
    std::cout << "Lmrk effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
