//****************************************************************************
// uSisSeoBendEnergySimLevelMixin.inl
//****************************************************************************

/**
 * Sis = sequential importance sampling
 * Seo = Single-end, ordered growth - assumes monomers are grown in order
 *  from beginning to end
 */

/**
 * Mixin encapsulating global (sample independent) data and utilities. In this
 * case, the simulation level mixin manages common configuration parameters
 * (e.g. energy scalars) as well as any scratch buffers (e.g. candidate weight
 *  buffers)
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisSeoBendEnergySimLevelMixin_threaded
#else
class uSisSeoBendEnergySimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
{
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();

        // Initialize bend rigidities
        this->init_bend_rigidities(sim, config);

        // This assumes sphere sample points have been initialized!
        const uUInt num_candidates = sim.get_num_unit_sphere_sample_points();
        uAssert(num_candidates > U_TO_UINT(0));

        // Initialize defragmented candidate negative energies buffer
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_energy_cand_defrag_nener, zeros, num_candidates);

        // Initialize defragmented candidate probability mass buffer
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_energy_cand_defrag_pmass, zeros, num_candidates);

        // Initialize defragmented candidate positions (row-major)
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(m_energy_cand_defrag_pos,
                                         zeros,
                                         num_candidates, /*n_rows*/
                                         uDim_num);      /*n_rows*/

        // Initialize buffer for storing mapping from defragmented candidate
        // index to original fragmented index
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_energy_cand_defrag_ix, zeros, num_candidates);
    }

    /**
     * Resets to default state
     */
    inline void clear() {
        m_energy_bend_rigidities.clear();
        m_energy_cand_defrag_nener.clear();
        m_energy_cand_defrag_pmass.clear();
        m_energy_cand_defrag_pos.clear();
        m_energy_cand_defrag_ix.clear();
    }

    // Accessors

    /**
     * Interface to retrieve bend rigidity scaling factor
     * @param parent id - identifier of parent node
     * @return bending rigidity scale factor in [0, +inf]
     */
    inline uReal get_energy_bend_rigidity(const uUInt parent_id) const {
        uAssertBounds(parent_id, 0, U_TO_UINT(m_energy_bend_rigidities.size()));
        return m_energy_bend_rigidities[parent_id];
    }

    /**
     * Mutable accessor needed for convenient tuning of bend rigidities
     */
    inline std::vector<uReal>& get_energy_bend_rigidities() {
        return m_energy_bend_rigidities;
    }

    /**
     * @return Fixed size candidate negative energy buffer
     */
    inline uVecCol& get_energy_cand_defrag_nener(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_energy_cand_defrag_nener);
    }

    /**
     * @return Fixed size candidate exp(-energy) = probability mass buffer
     */
    inline uVecCol& get_energy_cand_defrag_pmass(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_energy_cand_defrag_pmass);
    }

    /**
     * @return Fixed size candidate positions buffer for better cache usage
     */
    inline uSseTable& get_energy_cand_defrag_pos(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_energy_cand_defrag_pos);
    }

    /**
     * @return Fixed size candidate positions index buffer
     */
    inline uUIVecCol& get_energy_cand_defrag_ix(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_energy_cand_defrag_ix);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Initializes internal bend rigidities from configuration object
     */
    void init_bend_rigidities(const sim_t& sim, uSpConstConfig_t config) {
        const uUInt num_nodes = sim.get_max_total_num_nodes();
        uAssert(num_nodes > 1);
        uBool b_rigidities_initialized = uFALSE;
        // Check if RLE rigidity file exists
        std::string bend_rigidity_fpath;
        if (config->resolve_path(bend_rigidity_fpath,
                                 uOpt_energy_bend_rigidity_fpath)) {
            // Parse rigidities
            if (uParserRle::read<uReal, std::vector<uReal> >(
                    m_energy_bend_rigidities, bend_rigidity_fpath)) {
                b_rigidities_initialized =
                    this->check_bend_rigidities(num_nodes);
            } else {
                // Warning, failed to parse rigidity file
                uLogf("Warning: unable to parse %s.\n",
                      bend_rigidity_fpath.c_str());
            }
        }
        // Else, check if scalar rigidity specified
        if (!b_rigidities_initialized) {
            uReal bend_rigidity = U_TO_REAL(0.0);
            config->read_into<uReal>(bend_rigidity, uOpt_energy_bend_rigidity);
            // Initialize from scalar bend rigidity
            m_energy_bend_rigidities.resize(num_nodes, bend_rigidity);
            b_rigidities_initialized = uTRUE;
        }
        // Verify rigidities are properly configured
        uAssert(b_rigidities_initialized &&
                this->check_bend_rigidities(num_nodes));
    }

    /**
     * Checks internal bend rigidities are configured properly
     * @return true if bend rigidities are in expected format, false o/w
     */
    bool check_bend_rigidities(const uUInt num_nodes) const {
        // Check rigidities exist for each node
        if (num_nodes != U_TO_UINT(m_energy_bend_rigidities.size())) {
            // Warning, each node was not assigned a rigidity
            uLogf("Warning: expected %d != %d rigidities.\n",
                  ((int)num_nodes),
                  ((int)m_energy_bend_rigidities.size()));
            return false;
        }
        // Note, rigidities may be negative. For example, if monomer
        // triplet i-1, i, i+1 has length == Kuhn length, then a rigidity
        // of 0 may not allow the chain to be flexible enough chain to
        // reproduce the desired Kuhn length.

        // All rigidities okay!
        return true;
    }

    /**
     * Bending rigidity at each parent node, must be non-negative.
     * Determines energy penalty applied:
     *  E = energy_bend_rigidity * (1 - cos(theta)).
     * Therefore, larger values imply stiffer polymers.
     */
    std::vector<uReal> m_energy_bend_rigidities;

    /**
     * Declaring mutable because parent level simulation is likely to be
     * declared as const. These buffers are for working with "defragmented"
     * candidate positions: some candidates will not pass constraint filters
     * and they should be removed prior to energy computation.
     */

    /**
     * Scratch buffer for storing -energies at each candidate
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_energy_cand_defrag_nener);

    /**
     * Scratch buffer for storing exp(-energies) = probability mass at each
     * candidate
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_energy_cand_defrag_pmass);

    /**
     * Contiguous candidate positions
     */
    mutable U_DECLARE_TLS_DATA(uSseTable, m_energy_cand_defrag_pos);

    /**
     * Candidate indices, map from defragmented index to actual index
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_energy_cand_defrag_ix);
};
