//****************************************************************************
// uChromatinExportGzipper.h
//****************************************************************************

/**
 * Utility for writing compressed sample coordinates
 */

#ifndef uChromatinExportGzipper_h
#define uChromatinExportGzipper_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"
#include "uFilesystem.h"
#include "uStreamUtils.h"
#include "uTypes.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <zstr.hpp>

//****************************************************************************
// Compressed export
//****************************************************************************

/**
 * Effectively a namespace for compressed chromatin export utilities
 */
template <typename t_exporter_raw>
class uChromatinExportGzipper {
public:
    /**
     * Raw (uncompressed) exporter type
     */
    typedef t_exporter_raw exporter_raw_t;

    /**
     * Writes sample node positions
     * @param fpath - path to write the exported sample
     * @param args - exporter arguments
     */
    static void write_chromatin(const std::string& fpath,
                                const uChromatinExportArgs_t& args) {
        if (!chromatin_export_check(args)) {
            std::cout << "Error, unable to export sample.\n";
            return;
        }

        uFs_create_parent_dirs(fpath);
        zstr::ofstream fout(fpath.c_str());

        if (!fout) {
            std::cout << "Warning, unable to open: " << fpath << std::endl
                      << "\tfor writing compressed format.\n";
            return;
        }

        std::stringstream ss;
        exporter_raw_t::write_chromatin(ss, args);
        ss.seekg(0, std::ios::beg);
        uStreamUtils::cat(ss, fout);
        // zstr::ofstream has no close() method!
    }

    /**
     * @return exporter name
     */
    static std::string get_name() {
        return exporter_raw_t::get_name() + std::string(".gz");
    }

    /**
     * @return exporter format
     */
    static std::string get_format() { return get_name(); }

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes) {
        exporter_raw_t::warn_format(max_total_num_nodes);
    }
};

#endif  // uChromatinExportGzipper_h
