//****************************************************************************
// uSisRejectionControlQcSimLevelMixin.h
//****************************************************************************

/**
 * QcMixin = A mixin that performs Quality Control to minimize variance in
 *  sample weights. Examples of algorithms intended to achieve this include
 *  Rejection Control and Resampling. See:
 *
 * Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science
 *   & Business Media, 2008.
 *
 * This is a mixin that performs rejection control. See:
 *
 * Liu, Jun S., Rong Chen, and Wing Hung Wong. "Rejection control and
 *  sequential importance sampling." Journal of the American Statistical
 *  Association 93.443 (1998): 1022-1031.
 */

#ifndef uSisRejectionControlQcSimLevelMixin_h
#define uSisRejectionControlQcSimLevelMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uHashMap.h"
#include "uRand.h"
#include "uSisUtilsQc.h"
#include "uSisUtilsRc.h"
#include "uStats.h"
#include "uTypes.h"

/**
 * This class defines on_grow_phase_finish() to perform rejection control
 * filtering of the current set of samples. Samples that fail the rejection
 * control step must be regrown.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // Determines schedule of when to run rejection control
    typename t_RcSched,
    // Determines weight scaling during rejection control steps
    typename t_RcScale,
    // Determines the regrowth procedure (e.g. parallel or serial)
    typename t_RcRegrowth,
    // Set to true to enable console logging of checkpoints
    bool rc_heartbeat_,
    // Set to true to enable normalization of output weights
    bool rc_norm_>
class uSisRejectionControlQcSimLevelMixin : public t_RcSched,
                                            public t_RcScale,
                                            public t_RcRegrowth {
private:
    /**
     * Scheduling type
     */
    typedef t_RcSched uRcSched_t;

    /**
     * Scaling type
     */
    typedef t_RcScale uRcScale_t;

    /**
     * Regrowth type
     */
    typedef t_RcRegrowth uRcRegrowth_t;

    /**
     * Allow it to call regrowth()
     */
    friend uRcRegrowth_t;

    /**
     * Structure containing the data to reproduce a previously encountered
     * checkpoint.
     */
    struct checkpoint_t {
        /**
         * The maximum log weight at this checkpoint, needed for normalizing
         * log weights prior to exp transform (to avoid precision issues)
         */
        uReal max_log_weight;

        /**
         * All weights at checkpoint are scaled by this factor. A sample is
         * kept with probability = min(1.0, w_i / scale) where w_i is weight
         * of i-th sample.
         */
        uReal scale;
    };

    /**
     * Enum switches
     */
    enum {
        // Flip switch to enable logging whenever a checkpoint is reached
        rc_heartbeat = rc_heartbeat_,
        // Allow weight normalization
        rc_norm = rc_norm_
    };

public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisRejectionControlQcSimLevelMixin() {
        // Initialize empty key
        U_HASH_SET_EMPTY_KEY(m_checkpoints, U_UINT_MAX);

        // Initialize deleted key
        U_HASH_SET_DELETED_KEY(m_checkpoints, (U_UINT_MAX - 1));
    }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Clear any previous state
        this->clear();

        // Allocate exponential weights buffer
        uAssert(sim.get_target_total_num_samples() > 0);
        m_norm_exp_weights.zeros(sim.get_target_total_num_samples());

        // Allocate trial keep probabilities buffer
        m_trial_log_keep_weights.zeros(sim.get_target_total_num_samples());

        // Allocate completed keep probabilities buffer
        m_completed_log_keep_weights.zeros(sim.get_target_total_num_samples());

        // Allocate should regrow buffer
        m_should_regrow.resize(sim.get_target_total_num_samples());

        // Initialize schedule
        uRcSched_t::init(sim, config, false /*is_tr*/ U_THREAD_ID_ARG);

        // Initialize threshold
        uRcScale_t::init(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->init(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_max_encountered_grow_step = 0;
        m_checkpoints.clear();
        m_norm_exp_weights.clear();
        m_trial_log_keep_weights.clear();
        m_completed_log_keep_weights.clear();
        m_should_regrow.clear();
        this->uRcSched_t::clear();
        this->uRcScale_t::clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Re-size exponential weights buffer
        uAssert(sim.get_max_total_num_nodes() > 0);
        m_norm_exp_weights.zeros(sim.get_target_trial_num_samples());

        // Re-size trial keep probabilities buffer
        m_trial_log_keep_weights.zeros(sim.get_target_trial_num_samples());

        // Re-size should regrow buffer
        m_should_regrow.resize(sim.get_target_trial_num_samples());

        // Warning: these probably should not require a reset!
        this->uRcSched_t::reset_for_next_trial(sim, config U_THREAD_ID_ARG);
        this->uRcScale_t::reset_for_next_trial(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Callback for when all samples have finished a single grow phase
     * @param status - The current status of each trial sample - 1 means
     *  chain is still growing, 0 means chain is no longer growing. May be
     *  modified by this function call.
     * @param log_weights - The current log weights of each sample. May be
     *  modified by this function call.
     * @param samples - The current set of trial samples. May be modified by
     *  this function call.
     * @param num_completed_grow_steps_per_sample - The number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     */
    inline void on_grow_phase_finish(
        uWrappedBitset& status,
        uVecCol& log_weights,
        std::vector<sample_t>& samples,
        const uUInt num_completed_grow_steps_per_sample,
        sim_t& sim U_THREAD_ID_PARAM) {
        // Should not be called if all samples are dead
        uAssert(!uSisUtilsQc::are_all_samples_dead(samples));

        // Should only be called after sample_t::grow_*()
        uAssert(num_completed_grow_steps_per_sample > 1);

        // Verify parallel data structures
        uAssertPosEq(status.size(), log_weights.size());
        uAssert(status.size() == samples.size());
        uAssert(status.size() == m_norm_exp_weights.size());
        uAssert(status.size() == m_trial_log_keep_weights.size());
        uAssert(status.size() == m_should_regrow.size());

        // See if we are at a checkpoint
        checkpoint_t cp;
        if (!rc_prime_for_checkpoint(
                cp, log_weights, num_completed_grow_steps_per_sample, sim)) {
            // Early out if no checkpoint reached
            return;
        }

        if (rc_heartbeat) {
            uLogf("-Rejection control checkpoint reached at grow step %u.\n",
                  (unsigned int)num_completed_grow_steps_per_sample);

#ifdef U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
            uSisUtilsRc::report_weight_summary(
                cp.scale, m_norm_exp_weights, log_weights);
#endif  // U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
        }

        U_SCOPED_INTERVAL_LOGGER(uSTAT_QcCheckpointInterval);

        // Make sure checkpoint data is valid
        uAssert(cp.max_log_weight >= U_TO_REAL(0.0));
        uAssert(cp.scale > U_TO_REAL(0.0));

        // Determine if samples need to be regrown
        const uMatSz_t n_samples = log_weights.n_elem;
        for (uMatSz_t i = 0; i < n_samples; ++i) {
            // Mark all samples which don't pass checkpoint
            m_should_regrow.set(
                ((size_t)i),
                uSisUtilsRc::checkpoint_filter(log_weights.at(i),
                                               m_trial_log_keep_weights.at(i),
                                               m_norm_exp_weights.at(i),
                                               cp.scale U_THREAD_ID_ARG));
        }

        // Wrap bit set for faster iteration
        const uWrappedBitset wrapped_should_regrow(
            U_WRAP_BITSET(m_should_regrow));

        // Early out if nothing needs to be regrown!
        if (!wrapped_should_regrow.any()) {
            return;
        }

        // Regrow selected samples
        {
            U_SCOPED_STAT_TIMER(uSTAT_RcRegrowthTime);

            this->uRcRegrowth_t::rc_batch_regrow(
                status,
                samples,
                log_weights,
                *this,
                sim,
                wrapped_should_regrow,
                num_completed_grow_steps_per_sample U_THREAD_ID_ARG);
        }  // end timer scope

    }  // end on_grow_phase_finish()

    // BEGIN REGROW INTERFACE
    // Interface used by trial runner when regrowing samples

    /**
     * Called by trial runner when prepping a sample to be regrown to a
     * certain length. Default simply forwards to mixins.
     */
    void reset_for_regrowth(const sample_t& sample,
                            const uUInt num_completed_grow_steps,
                            const sim_t& sim,
                            uSpConstConfig_t config) {
        this->uRcSched_t::reset_for_regrowth(
            *this, sample, num_completed_grow_steps, sim, config);
        this->uRcScale_t::reset_for_regrowth(
            *this, sample, num_completed_grow_steps, sim, config);
    }

    /**
     * Called by trial runner when the sample has finished being seeded during
     * the regrowth procedure. Default simply forwards to mixins.
     * @WARNING
     * @ASSUMPTION
     * Assuming no checkpoint will occur during seeding
     */
    void on_reseed_finish(const sample_t& sample,
                          uReal& out_log_weight,
                          bool& status,
                          const sim_t& sim) {
        this->uRcSched_t::on_reseed_finish(*this, sample, sim);
        this->uRcScale_t::on_reseed_finish(*this, sample, sim);
    }

    /**
     * Called by trial runner when sample has finished a grow_*() call
     * during the regrowth procedure. Determines if a checkpoint has been
     * reached. Samples which do not pass the checkpoint are marked dead.
     */
    void on_regrow_step_finish(sample_t& sample,
                               uReal& out_log_weight,
                               bool& status,
                               const uUInt num_completed_grow_steps,
                               const uUInt sample_id,
                               const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(!sample.is_dead());
        uAssert(out_log_weight > U_TO_REAL(0.0));

        // Let mixins update their state first
        this->uRcSched_t::on_regrow_step_finish(
            *this, sample, status, num_completed_grow_steps, sim);
        this->uRcScale_t::on_regrow_step_finish(
            *this, sample, status, num_completed_grow_steps, sim);

        // See if we've reached a checkpoint
        checkpoint_t cp;
        if (rc_get_cached_checkpoint(cp, num_completed_grow_steps)) {
            // Normalize log weight relative to max log weight at time
            // of checkpoint
            const uReal norm_exp_weight =
                exp(out_log_weight - cp.max_log_weight);

            // We hit a checkpoint, see if we pass it
            uAssertBounds(sample_id, 0, m_trial_log_keep_weights.size());
            uReal& out_log_keep_weight = m_trial_log_keep_weights.at(sample_id);
            if (uSisUtilsRc::checkpoint_filter(out_log_weight,
                                               out_log_keep_weight,
                                               norm_exp_weight,
                                               cp.scale U_THREAD_ID_ARG)) {
                U_INC_STAT_COUNTER(uSTAT_RcReRegrowthCount);

                // We got filtered -> we did not pass go, we did not
                // collect $200.
                status = false;
                sample.mark_dead(uSisFail_QC, out_log_weight);
            }  // end checkpoint filter
        }      // end if checkpoint
    }

    // END REGROW INTEFACE

    /**
     * Callback for when sim has finished a main trial
     */
    void on_trial_finish(const uVecCol& trial_log_weight,
                         const std::vector<sample_t>& trial_samples,
                         const uUInt prev_num_completed_samples) {
        rc_conditional_copy_completed_keep_weights(
            trial_log_weight, trial_samples, prev_num_completed_samples);
    }

    /**
     * Callback for when sim has finished a trial from start until target
     */
    void on_trial_finish_from_start(const uVecCol& trial_log_weight,
                                    const std::vector<sample_t>& trial_samples,
                                    const uUInt prev_num_completed_samples) {
        rc_conditional_copy_completed_keep_weights(
            trial_log_weight, trial_samples, prev_num_completed_samples);
    }

    /**
     * Callback for when sim has finished a trial from template until target
     */
    void on_trial_finish_from_template(
        const uVecCol& trial_log_weight,
        const std::vector<sample_t>& trial_samples,
        const uUInt prev_num_completed_samples) {
        rc_conditional_copy_completed_keep_weights(
            trial_log_weight, trial_samples, prev_num_completed_samples);
    }

    /**
     * Callback for when sim has finished a main run
     */
    void on_run_finish(uVecCol& completed_log_weights) {
        rc_conditional_norm_weights(completed_log_weights);
    }

    /**
     * Callback for when sim has finished a run from start until target
     */
    void on_run_finish_from_start(uVecCol& completed_log_weights) {
        rc_conditional_norm_weights(completed_log_weights);
    }

    /**
     * Callback for when sim has finished a run from template until target
     */
    void on_run_finish_from_template(uVecCol& completed_log_weights) {
        rc_conditional_norm_weights(completed_log_weights);
    }

private:
    /**
     * Hook for when a simulation run*() trial has finished. If enabled, the
     * keep weights for the trial will be stored so that they may later be
     * used to estimate a normalization constant.
     * @param trial_log_weights - The final sample log weights for the current
     *  trial, may be dead so check sample_t::is_dead() before copying
     * @param trial_samples - The corresponding collection of trial samples,
     *  must be same size as trial_log_weights.
     * @param prev_num_completed_samples - The number of completed samples
     *  prior to latest batch being added
     */
    void rc_conditional_copy_completed_keep_weights(
        const uVecCol& trial_log_weights,
        const std::vector<sample_t>& trial_samples,
        const uUInt prev_num_completed_samples) {
        // Keep weights are only needed if normalization enabled
        if (!rc_norm) {
            return;
        }
        uAssertPosEq(m_trial_log_keep_weights.size(), trial_log_weights.size());
        uAssert(trial_log_weights.size() == trial_samples.size());
        const uUInt num_trial_samples = U_TO_UINT(trial_log_weights.size());
        uUInt num_completed_samples = prev_num_completed_samples;
        for (uUInt i = 0; i < num_trial_samples; ++i) {
            const uReal log_weight = trial_log_weights.at(i);
            if (!trial_samples[i].is_dead()) {
                // Copy keep weight to completed set
                uAssertBounds(num_completed_samples,
                              0,
                              m_completed_log_keep_weights.size());
                m_completed_log_keep_weights.at(num_completed_samples++) =
                    m_trial_log_keep_weights.at(i);
            }
        }
    }

    /**
     * @WARNING - Will clobber internal norm_exp_weights buffer
     * Hook for normalizing output weights once a simulation is about to
     * finish a run*() call. This allows weights to be comparable for samples
     * that are regrown from different checkpoint conditions. Will only
     * normalize if keep weights were saved during trial runs.
     * @param completed_log_weights - The unnormalized weights which will be
     *  normalized once method completes.
     */
    void rc_conditional_norm_weights(uVecCol& completed_log_weights) {
        // Only normalize if enabled
        if (!rc_norm) {
            return;
        }

        // Early out if no work to be done
        if (completed_log_weights.empty()) {
            return;
        }

        // It's possible for the completed log weights to be smaller than the
        // allocated log keep weights as some samples may not have been able
        // to complete within the trial limit. To account for this, simply
        // resize the log keep weights buffer to be same size as log weights.
        uAssert(completed_log_weights.size() <=
                m_completed_log_keep_weights.size());
        if (completed_log_weights.size() <
            m_completed_log_keep_weights.size()) {
            m_completed_log_keep_weights.resize(completed_log_weights.size());
        }

        // Estimate log norm for the rejection control distribution (i.e. the
        // partition constant). See:
        //  Monte Carlo strategies in scientific computing, pg. 44
        // which states that the normalizing constant may be unbiasedly
        // estimated from the average of the rejection probabilities.
        m_norm_exp_weights.set_size(completed_log_weights.size());
        const uReal log_norm = uSisUtilsQc::calc_mean_log_weight(
            m_norm_exp_weights, m_completed_log_keep_weights);

        // Now add the normalizing constant to the final set of weights
        // (equivalent to dividing by 1/Z where Z is partition constant)
        completed_log_weights += log_norm;
    }

    /**
     * Utility function for use in on_grow_phase_finish(). Determines if we've
     * encountered a checkpoint - either in the past or a new checkpoint based
     * on scheduling policy. If checkpoint is encountered, checkpoint will be
     * cached, norm exp weights will be initialized, and max growth phase
     * marker will be updated.
     * @param out_cp - the output checkpoint data if it exists
     * @param log_weights - The current log weights of each sample
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     */
    inline uBool rc_prime_for_checkpoint(
        checkpoint_t& out_cp,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_t& sim) {
        uAssert(num_completed_grow_steps_per_sample > 1);
        uAssertPosEq(m_norm_exp_weights.size(), log_weights.size());

        uBool b_checkpoint_encountered = uFALSE;

        // See if we've already encountered this growth phase. If so, then we
        // must use cached data. This is possible if we're undergoing multiple
        // trials because a few chains died beyond the last checkpoint.
        if (num_completed_grow_steps_per_sample <=
            m_max_encountered_grow_step) {
            b_checkpoint_encountered = rc_get_cached_checkpoint(
                out_cp, num_completed_grow_steps_per_sample);

            // After function completes and if a checkpoint is encountered,
            // rest of code assumes norm_exp_weights have been computed - so
            // let's compute them here:
            if (b_checkpoint_encountered) {
                uSisUtilsQc::calc_norm_exp_weights_using(
                    m_norm_exp_weights, out_cp.max_log_weight, log_weights);
            }
        }
        // Else, new territory, determine if a new checkpoint encountered
        else {
            // Determine if we should perform rejection control
            if (uRcSched_t::qc_checkpoint_reached(
                    m_norm_exp_weights,
                    out_cp.max_log_weight,
                    log_weights,
                    num_completed_grow_steps_per_sample,
                    *this,
                    sim)) {
                // Determine exponential weights
                if (!uRcSched_t::calculates_norm_exp_weights) {
                    uSisUtilsQc::calc_norm_exp_weights(
                        m_norm_exp_weights, out_cp.max_log_weight, log_weights);
                }

                // Determine scaling factor
                out_cp.scale = uRcScale_t::rc_get_scale(
                    m_norm_exp_weights,
                    out_cp.max_log_weight,
                    num_completed_grow_steps_per_sample,
                    *this,
                    sim);

                // Cache new checkpoint
                uAssert(
                    m_checkpoints.find(num_completed_grow_steps_per_sample) ==
                    m_checkpoints.end());
                m_checkpoints[num_completed_grow_steps_per_sample] = out_cp;
                uAssert(
                    m_checkpoints.find(num_completed_grow_steps_per_sample) !=
                    m_checkpoints.end());

                // Update growth phase marker
                m_max_encountered_grow_step =
                    num_completed_grow_steps_per_sample;

                // Flag that checkpoint has been encountered
                b_checkpoint_encountered = uTRUE;
            }
        }

        return b_checkpoint_encountered;
    }

    /**
     * @param out_cp - Cached checkpoint data is stored in this struct
     * @param num_completed_grow_steps - the number of seed +
     *  grow_step calls
     * @return TRUE if checkpoint data exists for growth phase, FALSE o/w
     */
    inline uBool rc_get_cached_checkpoint(
        checkpoint_t& out_cp,
        const uUInt num_completed_grow_steps) {
        // We should only be calling this if we know we've encountered this
        // phase before
        uAssert(num_completed_grow_steps <= m_max_encountered_grow_step);

        // Check if this a checkpoint
        typename grow_step_to_checkpoints_map_t::const_iterator cp_itr(
            m_checkpoints.find(num_completed_grow_steps));

        if (cp_itr != m_checkpoints.end()) {
            // We found a checkpoint
            out_cp = cp_itr->second;
            return uTRUE;
        }

        // No checkpoint found
        return uFALSE;
    }

    /**
     * The workhorse method of rejection control. Samples that do not pass a
     * checkpoint must be regrown entirely!
     *
     * @param out_sample - The sample to be regrown
     * @param out_log_weight - The final log weight of the sample after
     *  checkpoint correction
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the number of grow steps to complete
     * @param sample_id - The sample identifier (index into samples array)
     * @return final status of last grow_*() call -> TRUE if sample needs
     *  to continue growing, FALSE if finished growing. We should not return
     *  any dead chains. Currently, method will infinite loop until chain is
     *  no longer dead.
     */
    inline bool rc_regrow(sample_t& out_sample,
                          uReal& out_log_weight,
                          sim_t& sim,
                          const uUInt target_num_grow_steps,
                          const uUInt sample_id U_THREAD_ID_PARAM) {
        U_INC_STAT_COUNTER(uSTAT_RcRegrowthCount);

        return uSisUtilsRc::regrow_force(out_sample,
                                         out_log_weight,
                                         sim,
                                         target_num_grow_steps,
                                         sample_id U_THREAD_ID_ARG);
    }

    /**
     * Data type for mapping a grow step count to its checkpoint data
     */
    typedef U_HASH_MAP<uUInt, checkpoint_t> grow_step_to_checkpoints_map_t;

    /**
     * Used for storing parameters every time we encounter a checkpoint. This
     * is needed so that samples being regrown can pass each checkpoint under
     * the same conditions.
     */
    grow_step_to_checkpoints_map_t m_checkpoints;

    /**
     * Internal buffer of exponential weights (derived from log_weights).
     * These weights are normalized relative to a maximum log weight.
     */
    uVecCol m_norm_exp_weights;

    /**
     * Transient keep weights buffer that changes each trial
     */
    uVecCol m_trial_log_keep_weights;

    /**
     * Internal buffer of keep probabilities at each sample. This value is
     * needed if a partition constant is to be estimated. The value at each
     * sample is equal to product[(min{1, w_i/c_i})] over all checkpoints,
     * where w_i is the sample weight at the i-th checkpoint and c_i is the
     * rejection threshold value at the i-th checkpoint.
     *
     * The partition constant can then be estimated as:
     *    sum[exp(log_keep_weight_k)] / N where k is k-th sample and N is
     *  total number of samples
     */
    uVecCol m_completed_log_keep_weights;

    /**
     * Internal bit set buffer for keeping track of which samples to regrow.
     * i-th bit is 1 if sample should be regrown, 0 otherwise
     *
     * Collecting all samples that need to be grown so that they can be more
     * efficiently batch queued into worker threads if a threaded
     * implementation is available.
     */
    uBitset m_should_regrow;

    /**
     * Keeps track of the highest grow step we've encountered. If we
     * encounter a step less than this, then we use cached checkpoint data as
     * opposed to recomputing new checkpoint data.
     */
    uUInt m_max_encountered_grow_step;
};

#endif  // uSisRejectionControlQcSimLevelMixin_h
