//****************************************************************************
// uSisHardShellCollisionMixin.h
//****************************************************************************

/**
 * A collision mixin manages the details for checking candidate nodes to see if
 * they satisfy self-avoidance (do not collide with any existing nodes)
 *
 * An accessory structure is the simulation level collision mixin which
 * define global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 * HardShell - Uses broad phase filtering and then applies a narrow phase hard
 *  shell (i.e. no overlap) collision model to enforce 'self-avoidance'
 */

#ifndef uSisHardShellCollisionMixin_h
#define uSisHardShellCollisionMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uBitsetHandle.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uSseTable.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypeTraits.h"
#include "uTypes.h"
#include "uWrappedBroadPhase.h"

#include <vector>

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisHardShellCollisionSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers. Does not depend on node homogeneity.
 */
template <typename t_SisGlue, typename t_BroadUtil>
class uSisHardShellCollisionMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Our broad phase collision implementation
     */
    typedef t_BroadUtil broad_util_t;

    /**
     * Our simplified broad phase filtering utility type
     */
    typedef uWrappedBroadPhase<broad_util_t> wrapped_broad_util_t;

    /**
     * Initializes collision mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // If this assert trips, then uint16_t is insufficient number of bits
        // to represent the number of nodes in each sample.
        uAssert(sim.get_max_total_num_nodes() <=
                static_cast<uUInt>(U_UINT16_MAX));
        this->m_collision_broad_phase.init(sim.get_max_total_num_nodes(),
                                           sim.get_collision_grid_cell_length(),
                                           sim.get_max_nuclear_radius());
    }

    /**
     * Resets to default state
     */
    void clear() { this->m_collision_broad_phase.clear(); }

    /**
     * Updates collision state with new node information
     * @param node_id - a unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    inline void collision_add(const uUInt node_id,
                              const uReal* point,
                              const uReal radius,
                              sample_t& sample,
                              const sim_t& sim) {
        uAssertBounds(node_id, 0, sim.get_max_total_num_nodes());
        uAssert(radius > U_TO_REAL(0.0));
        uAssert(radius <= sim.get_max_node_diameter());
        m_collision_broad_phase.add(node_id,
                                    point,
                                    radius,
                                    sim.get_collision_grid_cell_length(),
                                    sim.get_collision_bounding_radius());
    }

    /**
     * Determines which positions are self-avoiding
     * @param out_legal_candidates_primed - [0,1] array for which elements
     *  satisfy nuclear constraint, merged with collision results.
     *  Final array is [0,1] if self-avoiding and also satisfies previous
     *  constraints.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for collision.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     * positions were generated from. Needed to avoid redundant collision
     * checks.
     * @param parent_radius - The radius of the parent node corresponding to
     *  parent_node_id (this node has already been added to the sample).
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void collision_filter(uBoolVecCol& out_legal_candidates_primed,
                                 const uMatrix& candidate_centers,
                                 const uReal candidate_radius,
                                 const uUInt parent_node_id,
                                 const uReal parent_radius,
                                 const sample_t& sample,
                                 const sim_t& sim U_THREAD_ID_PARAM) const {
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssertPosEq(out_legal_candidates_primed.size(),
                     candidate_centers.n_rows);
        uAssertBounds(parent_node_id, 0, sim.get_max_total_num_nodes());
        uAssert(sample.get_node_positions().n_cols ==
                sim.get_max_total_num_nodes());

        // Broad phase: Spatial subdivision acceleration to reduce growth
        // constant in O(n^2) collisions
        // @OPTIMIZATION: this has been factored out of loop as hash checks on
        // each candidate were showing up on the profiler hot path.
        uWrappedBitset broad_phase_mask(
            sim.get_collision_broad_phase_mask(U_THREAD_ID_0_ARG));
        uAssertPosEq(broad_phase_mask.size(), sim.get_max_total_num_nodes());
        m_collision_broad_phase.template filter<uWrappedBitset>(
            broad_phase_mask,
            sample.get_node_positions().colptr(U_TO_MAT_SZ_T(parent_node_id)),
            ((U_TO_REAL(2.0) * candidate_radius) + parent_radius),
            sim.get_collision_grid_cell_length(),
            sim.get_collision_bounding_radius());
        // Avoid checking against node used to generate candidate positions
        broad_phase_mask.set(parent_node_id, false);

        // @OPTIMIZATION: Defragment broad phase positions - better cache
        // performance
        uSseTable& broad_phase_positions_buffer =
            sim.get_collision_broad_phase_positions(U_THREAD_ID_0_ARG);
        uAssert(broad_phase_positions_buffer.n_rows >=
                sample.get_node_positions().n_rows);
        uAssert(broad_phase_positions_buffer.n_cols ==
                sample.get_node_positions().n_cols);
        const size_t num_broad_phase = broad_phase_mask.count();
        uUInt itr_rev = U_TO_UINT(num_broad_phase);
        for (uWrappedBitset::size_type i = broad_phase_mask.find_first();
             i != uWrappedBitset::npos;
             i = broad_phase_mask.find_next(i)) {
            uAssert(num_broad_phase < broad_phase_positions_buffer.n_cols);
            uAssert(i < sample.get_node_positions().n_cols);
            // @OPTIMIZATION - checking collisions against most recent nodes
            // first gives slight benchmark improvement
            uReal* const dst = broad_phase_positions_buffer.colptr(--itr_rev);
            const uReal* const src =
                sample.get_node_positions().colptr(U_TO_MAT_SZ_T(i));
            dst[uDim_X] = src[uDim_X];
            dst[uDim_Y] = src[uDim_Y];
            dst[uDim_Z] = src[uDim_Z];
            // For non-homogeneous collisions, encode distance threshold in
            // last element. This is a compile time constant, therefore this
            // branch should have no runtime cost for homogeneous nodes.
            if (!sample_t::is_homg_radius) {
                uAssert(broad_phase_positions_buffer.n_rows > uDim_radius);
                dst[uDim_radius] =
                    sim.sim_level_collision_mixin_t::
                        calc_collision_hetr_min_n2n_sq_dist(
                            sample.get_node_radius(U_TO_UINT(i), sim),
                            candidate_radius);
            }
        }

        // Create cropped view of broad phase positions
        const uMatrix broad_phase_positions_view(
            broad_phase_positions_buffer.memptr(), /*ptr_aux_mem*/
            broad_phase_positions_buffer.n_rows,   /*n_rows*/
            U_TO_MAT_SZ_T(num_broad_phase),        /*n_cols*/
            false,                                 /*copy_aux_mem*/
            true                                   /*strict*/
        );

        // Narrow phase check for each candidate
        uReal candidate_center[uDim_num];
        const uUInt num_candidates =
            U_TO_UINT(out_legal_candidates_primed.size());
        for (uUInt i = 0; i < num_candidates; ++i) {
            // Skip collision checks if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }

            candidate_center[uDim_X] = candidate_centers.at(i, uDim_X);
            candidate_center[uDim_Y] = candidate_centers.at(i, uDim_Y);
            candidate_center[uDim_Z] = candidate_centers.at(i, uDim_Z);

            // Narrow phase: Detailed collision check of broad phase results
            out_legal_candidates_primed.at(i) =
                sample_t::is_homg_radius
                    // Homogeneous case
                    ? uSisUtils::narrow_phase_homogeneous_sphere_collision(
                          &(candidate_center[0]),
                          broad_phase_positions_view,
                          sim.get_collision_homg_min_n2n_sq_dist())
                    // Non-homogeneous case
                    : uSisUtils::narrow_phase_inhomogeneous_sphere_collision(
                          &(candidate_center[0]), broad_phase_positions_view);
        }  // end of iteration over primed legal candidates

    }  // end of collision_filter() function call

    /**
     * @return Wrapper interface for performing single broad phase checks
     */
    inline wrapped_broad_util_t get_collision_broad_phase_wrapped(
        const sample_t& sample,
        const sim_t& sim) const {
        return wrapped_broad_util_t(this->m_collision_broad_phase,
                                    sim.get_collision_grid_cell_length(),
                                    sim.get_collision_bounding_radius());
    }

    /**
     * @return Const handle to broad phase
     */
    inline const broad_util_t& get_collision_broad_phase() const {
        return m_collision_broad_phase;
    }

private:
    /**
     * Our broad phase collision utility
     */
    broad_util_t m_collision_broad_phase;
};

#endif  // uSisHardShellCollisionMixin_h
