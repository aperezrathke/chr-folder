//****************************************************************************
// uSisSeoMlocGrowthMixin.h
//****************************************************************************

/**
 * Growth mixin for multiple loci which always grows each loci from first to
 *  last node. To simplify assumptions made by other mixins (e.g. energy
 *  mixin, interaction mixin, etc.), growth is assumed to proceed in the
 *  following phases:
 *      - 1st phase (seeding): All loci are seeded
 *      - 2nd phase: The second monomer node for all loci is grown
 *      - Nth phases: The nth monomer for a random locus is grown
 *  Note difference between 1st and 2nd phases with the Nth phases. In 1st
 *  and 2nd phases, multiple nodes are grown during a single growth phase;
 *  in contrast, for Nth phases, only a single locus node is grown.
 */

#ifndef uSisSeoMlocGrowthMixin_h
#define uSisSeoMlocGrowthMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uRand.h"
#include "uShufflerFisherYates.h"
#include "uSisSeGrowthSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uTypes.h"

//****************************************************************************
// Sim-level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSeoMlocGrowthSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisSeoMlocGrowthSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

//****************************************************************************
// Sample-level mixin
//****************************************************************************

/**
 * Sis = sequential importance sampling
 * SingleEnd = growth is from only one end of chain
 * MultiLocus = built for multiple loci chains
 * Ordered = nodes are grown from first to last node always
 */
namespace uSisSeoMloc {
/**
 * Mixin encapsulating sample specific data and routines
 */
template <typename t_SisGlue>
class GrowthMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Allow compile time branching based on single locus (sloc) vs
     * multi-loci (mloc) growth
     */
    enum { is_mloc_growth = true };

    /**
     * Initializes growth mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // If this assert trips, then uint16_t is insufficient number of bits
        // to represent the number of nodes in each sample.
        uAssert(sim.get_max_total_num_nodes() <=
                static_cast<uUInt>(U_UINT16_MAX));
        const uMatSz_t num_nodes = U_TO_MAT_SZ_T(sim.get_max_total_num_nodes());
        m_growth_perm_loci_order.zeros(num_nodes);
        const uMatSz_t num_loci = U_TO_MAT_SZ_T(sim.get_num_loci());
        m_growth_num_at_loci.zeros(num_loci);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_growth_perm_loci_order.clear();
        m_growth_num_at_loci.clear();
    }

    /**
     * Initializes first node positions for all loci
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current log weight of this sample
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool seed(sample_t& sample,
                     uReal& log_weight,
                     const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(log_weight == U_TO_REAL(0.0));
        // Determine growth order
        this->growth_init_perm(sample, sim U_THREAD_ID_ARG);

        const uUInt n_seed = sim.get_growth_num_seed();
        uAssertPosEq(n_seed, sim.get_max_num_nodes_at_locus().size());
        uMatrix& positions_buff =
            sim.get_growth_seed_positions(U_THREAD_ID_0_ARG);
        uAssert(positions_buff.n_rows == uDim_num_extended);
        uAssert(positions_buff.n_cols == n_seed);
        uAssert(m_growth_perm_loci_order.n_elem ==
                sim.get_max_total_num_nodes());

        // Seed first locus
        uUInt nid;
        uReal radius;
        this->seed_locus(0, /*lix*/
                         uSisUtils::SeedFirstClashPolicy(),
                         sample.get_intr_chr_seed_first_policy(),
                         sample,
                         nid,
                         radius,
                         sim U_THREAD_ID_ARG);

        // Seed remaining loci
        for (uUInt i = 1; i < n_seed; ++i) {
            // Update positions buffer for previously seeded nodes. Note, we
            // are using nid and radius from previous iteration
            uAssertBounds(nid, 0, sample.get_node_positions().n_cols);
            uAssert(radius > U_TO_REAL(0.0));
            positions_buff.at(uDim_X, (i - 1)) =
                sample.get_node_positions().at(uDim_X, nid);
            positions_buff.at(uDim_Y, (i - 1)) =
                sample.get_node_positions().at(uDim_Y, nid);
            positions_buff.at(uDim_Z, (i - 1)) =
                sample.get_node_positions().at(uDim_Z, nid);
            positions_buff.at(uDim_radius, (i - 1)) = radius;

            // Update positions view
            const uMatrix positions_view(positions_buff.memptr(), /*aux_mem*/
                                         positions_buff.n_rows,   /*n_rows*/
                                         i,                       /*n_cols*/
                                         false, /*copy_aux_mem*/
                                         true); /*strict*/

            this->seed_locus(i, /*lix*/
                             uSisUtils::SeedNextClashPolicy(positions_view),
                             sample.get_intr_chr_seed_next_policy(),
                             sample,
                             nid,
                             radius,
                             sim U_THREAD_ID_ARG);
        }

        return uSisUtils::will_still_need_growing(n_seed, sim);
    }

    /**
     * Performs incremental locus chain growth directly after 1st growth phase
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current and output log weight of the sample
     * @param num_completed_grow_steps - The number of growth calls (seed() +
     *  grow_*()) that have completed. This value is reset between trials.
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool grow_2nd(sample_t& sample,
                         uReal& log_weight,
                         const uUInt num_completed_grow_steps,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps == 1);
        const uUInt n_seed = sim.get_growth_num_seed();
        uAssertPosEq(n_seed, sim.get_max_num_nodes_at_locus().size());
        uAssert(get_num_grown_nodes(num_completed_grow_steps, sample, sim) ==
                n_seed);
        uAssert(uMatrixUtils::sum(m_growth_num_at_loci) == n_seed);
        const uUInt n_2nd = sim.get_growth_num_2nd();
        uAssertBoundsInc(n_2nd, 1, n_seed);
        const uUInt num_target_nodes = n_seed + n_2nd;
        uAssert(num_target_nodes <= sim.get_max_total_num_nodes());

        // grow 2nd node for each loci
        uUInt i = n_seed;
        bool is_alive = false;
        do {
            uAssert(i < num_target_nodes);
            uAssertBounds(i, 0, m_growth_perm_loci_order.n_elem);
            const uUInt lid = m_growth_perm_loci_order.at(i);
            uAssertBounds(lid, 0, n_seed);
            uAssert(sim.get_max_num_nodes_at_locus()[lid] >= 1);
            const uUInt parent_nid = sim.get_growth_nid(lid, 0 /*nof*/);
            uAssertBounds(parent_nid, 0, sim.get_max_total_num_nodes());

            is_alive = this->grow_after_1st<true /*should_grow_2nd*/>(
                lid,
                sample,
                log_weight,
                i,  // num_grown_nodes
                parent_nid,
                sim U_THREAD_ID_ARG);

        } while (is_alive && (++i < num_target_nodes));

        uAssert(!is_alive ||
                (uMatrixUtils::sum(m_growth_num_at_loci) == num_target_nodes));
        uAssert(!is_alive || (get_num_grown_nodes(num_completed_grow_steps + 1,
                                                  sample,
                                                  sim) == num_target_nodes));
        return is_alive &&
               uSisUtils::will_still_need_growing(num_target_nodes, sim);
    }

    /**
     * Performs incremental locus chain growth after 1st and 2nd growth phases
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current and output log weight of the sample
     * @param num_completed_grow_steps - The number of growth calls (seed() +
     *  grow_*()) that have completed. This value is reset between trials.
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool grow_nth(sample_t& sample,
                         uReal& log_weight,
                         const uUInt num_completed_grow_steps,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps >= 2);
        const uUInt num_grown =
            get_num_grown_nodes(num_completed_grow_steps, sample, sim);
        uAssert(uMatrixUtils::sum(m_growth_num_at_loci) == num_grown);
        uAssertBoundsInc(num_grown, 0, sim.get_max_total_num_nodes());
        uAssert(num_grown < m_growth_perm_loci_order.n_elem);
        const uUInt lid = m_growth_perm_loci_order.at(num_grown);
        uAssertBounds(lid, 0, m_growth_num_at_loci.n_elem);
        uAssertPosEq(m_growth_num_at_loci.n_elem,
                     sim.get_max_num_nodes_at_locus().size());
        const uUInt nof = m_growth_num_at_loci.at(lid);
        uAssert(nof >= 2);
        const uUInt parent_nid = sim.get_growth_nid(lid, nof - 1 /*nof*/);

        const bool is_alive = this->grow_after_1st<false /*should_grow_2nd*/>(
            lid,
            sample,
            log_weight,
            num_grown,
            parent_nid,
            sim U_THREAD_ID_ARG);

        uAssert(!is_alive || (get_num_grown_nodes(num_completed_grow_steps + 1,
                                                  sample,
                                                  sim) == (num_grown + 1)));

        return is_alive &&
               uSisUtils::will_still_need_growing(num_grown + 1, sim);
    }

    /**
     * @param num_completed_grow_steps - The number of growth calls (seed() +
     *  grow_*) that have completed. Value is reset between trials.
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @return Number of nodes currently grown by sample
     */
    inline uUInt get_num_grown_nodes(const uUInt num_completed_grow_steps,
                                     const sample_t& sample,
                                     const sim_t& sim) const {
        // Defer to simulation level mixin
        return sim.get_num_grown_nodes(num_completed_grow_steps);
    }

    /**
     * @HACK - For proximity (knock-in) interaction testing between different
     *  loci, we need to be able to query how many nodes have already been
     *  grown at each locus.
     * @param lid - locus identifier
     * @param num_completed_grow_steps - [UNUSED] The number of growth
     *  calls (seed() + growth_step()) that have completed. This value is
     *  reset between trials.
     * @param sample - [UNUSED] parent sample containing this mixin [used]
     * @param sim - [UNUSED] parent simulation containing global sample data
     */
    inline uUInt get_num_grown_nodes_at_locus(
        const uUInt lid,
        const uUInt num_completed_grow_steps,
        const sample_t& sample,
        const sim_t& sim) const {
        uAssertBounds(lid, U_TO_UINT(0), m_growth_num_at_loci.n_elem);
        return m_growth_num_at_loci.at(lid);
    }

    /**
     * Performs brute-force narrow-phase collisions and other structural
     * checks to make sure chain growth is proceeding as expected.
     * THIS CODE SHOULD NEVER BE CALLED IN RELEASE! IT IS DEBUGGING CODE
     * TO VALIDATE CORRECTNESS OF GROWTH ALGORITHMS.
     * @param num_grown_nodes - number of successfully grown nodes
     * @param sample - Sample to check
     * @param sim - Parent simulation
     * @return TRUE if all collision constraints satisfied, FALSE o/w
     */
    uBool check_growth(const uUInt num_grown_nodes,
                       const sample_t& sample,
                       const sim_t& sim) const {
        // This is a very expensive check, so only perform if enabled.
#ifndef U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS
        return uTRUE;
#endif
        uAssertBoundsInc(num_grown_nodes, 0, sim.get_max_total_num_nodes());
        uAssertPosEq(sample.get_node_positions().n_cols,
                     sim.get_max_total_num_nodes());
        uAssert(sample.get_node_positions().n_rows == uDim_num);
        uBitset active_nodes(sample.get_node_positions().n_cols);
        uUIVecCol num_grown_at_loci;
        num_grown_at_loci.zeros(
            U_TO_MAT_SZ_T(sim.get_max_num_nodes_at_locus().size()));

        uAssertBoundsInc(num_grown_nodes, 0, m_growth_perm_loci_order.n_elem);
        for (uUInt i = 0; i < num_grown_nodes; ++i) {
            const uUInt lid = m_growth_perm_loci_order.at(i);
            uAssertBounds(lid, 0, num_grown_at_loci.n_elem);
            const uUInt nof = num_grown_at_loci.at(lid)++;
            const uUInt nid = sim.get_growth_nid(lid, nof);
            uAssertBounds(nid, 0, active_nodes.size());
            active_nodes.set(nid);
        }

        return uSisUtils::satisfies_robust_constraint_checks(
            active_nodes, sample, sim);
    }

private:
    /**
     * Initialize permuted loci growth order. Since all permutations are
     * equally likely (at least in terms of the index permutation), we do not
     * need to keep track of this probability.
     */
    void growth_init_perm(const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // Reset order
        m_growth_perm_loci_order = sim.get_growth_sorted_loci_order();

        // Early out if we only have a single locus
        if (sim.get_max_num_nodes_at_locus().size() < 2) {
            return;
        }

        // Determine permutation intervals
        const uUInt n_seed = sim.get_growth_num_seed();
        const uUInt n_2nd = sim.get_growth_num_2nd();
        const uUInt n_max = sim.get_max_total_num_nodes();
        uAssertBoundsInc(n_seed, n_2nd, n_max);
        uAssertBoundsInc((n_seed + n_2nd), 1, n_max);

        // Shuffle seed phase
        uShufflerFisherYatesCore::shuffle(m_growth_perm_loci_order,
                                          0,           // ix_min
                                          n_seed - 1,  // ix_max
                                          n_seed - 1,  // ix_pool
                                          uRng);

        // Shuffle 2nd growth phase
        if (n_seed < n_max) {
            uShufflerFisherYatesCore::shuffle(m_growth_perm_loci_order,
                                              n_seed,              // ix_min
                                              n_seed + n_2nd - 1,  // ix_max
                                              n_seed + n_2nd - 1,  // ix_pool
                                              uRng);
        }

        // Shuffle nth growth phase
        if (n_seed + n_2nd < (n_max - 1)) {
            uShufflerFisherYatesCore::shuffle(m_growth_perm_loci_order,
                                              n_seed + n_2nd,  // ix_min
                                              n_max - 1,       // ix_max
                                              n_max - 1,       // ix_pool
                                              uRng);
        }
    }

    /**
     * Utility method to randomly place a chromatin node center within
     * nuclear sphere. Collision state is also updated.
     * @param lix - Index into loci order array
     * @param clash_func - Callback determines if placed node is clash free
     * @param intr_chr_func - Callback determines if placed node satisfies
     *  chr-chr interaction constraints
     * @param sample - Sample to place the node within
     * @param nid - Output node identifier of seeded node
     * @param radius - Output radius of seeded node
     * @param sim - Parent simulation of the sample
     */
    template <typename SeedClashPolicy_t, typename SeedIntrChrPolicy_t>
    inline void seed_locus(const uUInt lix,
                           const SeedClashPolicy_t& clash_func,
                           const SeedIntrChrPolicy_t& intr_chr_func,
                           sample_t& sample,
                           uUInt& nid,
                           uReal& radius,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        uAssertBounds(lix, 0, m_growth_perm_loci_order.n_elem);
        const uUInt lid = m_growth_perm_loci_order.at(lix);
        uAssertBounds(lid, 0, sim.get_growth_num_seed());
        nid = sim.get_growth_nid(lid, 0 /* nof */);
        uAssertBounds(nid, 0, sim.get_max_total_num_nodes());
        radius = sample.get_candidate_node_radius(nid, sim);
        sim.seed_core(sample,
                      nid,
                      lid,
                      radius,
                      clash_func,
                      intr_chr_func,
                      sim U_THREAD_ID_ARG);
        this->growth_update_locus_count(lid, sim);
    }

    /**
     * Performs incremental locus chain growth. If growing 2nd node in locus,
     * the template parameter "should_grow_2nd" must be true and false o/w.
     * @param locus_id - Locus identifier
     * @param sample - Parent sample containing this mixin
     * @param log_weight - Current and output log weight of the sample
     * @param num_grown_nodes - Number of successfully grown nodes
     * @param parent_node_id - Identifier of parent node from which to place
     *  candidate
     * @param sim - Parent simulation containing global sample data
     * @return FALSE if sample died, TRUE o/w
     */
    template <bool should_grow_2nd>
    inline bool grow_after_1st(const uUInt locus_id,
                               sample_t& sample,
                               uReal& log_weight,
                               const uUInt num_grown_nodes,
                               const uUInt parent_node_id,
                               const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(uSisUtils::will_still_need_growing(num_grown_nodes, sim));
        const bool is_alive =
            uSisUtils::grow_after_1st_seo<sample_t, sim_t, should_grow_2nd>(
                sample,
                log_weight,
                num_grown_nodes,
                parent_node_id,
                sim U_THREAD_ID_ARG);
        this->growth_update_locus_count(locus_id, sim);
        uAssert(!is_alive || (uMatrixUtils::sum(m_growth_num_at_loci) ==
                              (num_grown_nodes + 1)));
        return is_alive;
    }

    /**
     * Updates number of nodes grown at parameter locus
     * @param lid - Locus identifier
     * @param sim - Simulation object
     */
    inline void growth_update_locus_count(const uUInt lid, const sim_t& sim) {
        uAssertBounds(lid, 0, m_growth_num_at_loci.n_elem);
        uAssertPosEq(m_growth_num_at_loci.n_elem,
                     sim.get_max_num_nodes_at_locus().size());
        uAssert(lid < m_growth_num_at_loci.n_elem);
        ++(m_growth_num_at_loci.at(lid));
        uAssert(m_growth_num_at_loci.at(lid) <=
                sim.get_max_num_nodes_at_locus()[lid]);
    }

    /**
     * The random permuted order in which loci are grown
     */
    uUI16VecCol m_growth_perm_loci_order;

    /**
     * Keeps track of the number of nodes grown at each loci
     */
    uUI16VecCol m_growth_num_at_loci;
};

}  // namespace uSisSeoMloc

#endif  // uSisSeoMlocGrowthMixin_h
