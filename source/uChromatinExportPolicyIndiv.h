//****************************************************************************
// uChromatinExportPolicyIndiv.h
//****************************************************************************

/**
 * Utility for writing chromatin chains as individual files (one per chain)
 */

#ifndef uChromatinExportPolicyIndiv_h
#define uChromatinExportPolicyIndiv_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"
#include "uSisUtilsExportPath.h"
#include "uTypes.h"

//****************************************************************************
// Individual chromatin policy export
//****************************************************************************

/**
 * One file per chromatin chain export policy
 */
template <typename t_exporter>
class uChromatinExportPolicyIndiv {
public:
    /**
     * Raw exporter type
     */
    typedef t_exporter exporter_t;

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes) {
        exporter_t::warn_format(max_total_num_nodes);
    }

    /**
     * Called once before processing all sample geometries, meant to
     *  initialize export policy
     */
    static void begin() {}

    /**
     * Called once after processing all sample geometries, meant to flush and
     *  teardown export policy
     * @param config - user configuration
     */
    static void end(uSpConstConfig_t config) {}

    /**
     * Process sample node positions
     * @param args - exporter arguments
     * @param i - sample index
     * @param config - user configuration
     */
    static void process(const uChromatinExportArgs_t& args,
                        const uUInt i,
                        uSpConstConfig_t config) {
        // Determine file name
        const std::string fpath =
            uSisUtils::get_export_fpath(config,
                                        exporter_t::get_name(),    /*subdir*/
                                        u2Str(i),                  /*file_id*/
                                        exporter_t::get_format()); /*extension*/
        // Write chromatin sample
        exporter_t::write_chromatin(fpath, args);
    }
};

#endif  // uChromatinExportPolicyIndiv_h
