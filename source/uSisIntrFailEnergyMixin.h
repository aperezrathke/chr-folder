//****************************************************************************
// uSisIntrFailEnergyMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * IntrFail: Interaction Failure
 *
 * Sample energy is proportional to [budgeted] interaction constraint
 *  failure fraction
 */

#ifndef uSisIntrFailEnergyMixin_h
#define uSisIntrFailEnergyMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisIntrFailEnergySimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisIntrFailEnergyMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Budget accessor policies
     */

    // Chr-chr knock-in budget access
    typedef typename intr_chr_mixin_t::intr_chr_budg_acc_kin_t
        intr_chr_budg_acc_kin_base_t;
    typedef uEnerIntrFailBudgAcc<intr_chr_budg_acc_kin_base_t,
                                 eEnerIntrFail_chr_kin>
        intr_chr_budg_acc_kin_t;

    // Chr-chr knock-out budget access
    typedef typename intr_chr_mixin_t::intr_chr_budg_acc_ko_t
        intr_chr_budg_acc_ko_base_t;
    typedef uEnerIntrFailBudgAcc<intr_chr_budg_acc_ko_base_t,
                                 eEnerIntrFail_chr_ko>
        intr_chr_budg_acc_ko_t;

    // Lamina knock-in budget access
    typedef typename intr_lam_mixin_t::intr_lam_budg_acc_kin_t
        intr_lam_budg_acc_kin_base_t;
    typedef uEnerIntrFailBudgAcc<intr_lam_budg_acc_kin_base_t,
                                 eEnerIntrFail_lam_kin>
        intr_lam_budg_acc_kin_t;

    // Lamina knock-out budget access
    typedef typename intr_lam_mixin_t::intr_lam_budg_acc_ko_t
        intr_lam_budg_acc_ko_base_t;
    typedef uEnerIntrFailBudgAcc<intr_lam_budg_acc_ko_base_t,
                                 eEnerIntrFail_lam_ko>
        intr_lam_budg_acc_ko_t;

    // Nuclear body knock-in budget access
    typedef typename intr_nucb_mixin_t::intr_nucb_budg_acc_kin_t
        intr_nucb_budg_acc_kin_base_t;
    typedef uEnerIntrFailBudgAcc<intr_nucb_budg_acc_kin_base_t,
                                 eEnerIntrFail_nucb_kin>
        intr_nucb_budg_acc_kin_t;

    // Nuclear body knock-out budget access
    typedef typename intr_nucb_mixin_t::intr_nucb_budg_acc_ko_t
        intr_nucb_budg_acc_ko_base_t;
    typedef uEnerIntrFailBudgAcc<intr_nucb_budg_acc_ko_base_t,
                                 eEnerIntrFail_nucb_ko>
        intr_nucb_budg_acc_ko_t;

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Energy selection policy for 2nd growth phase
     */
    inline uUInt energy_select_2nd(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        return energy_select_nth(sample,
                                 log_weight,
                                 parent_id,
                                 candidate_positions,
                                 candidate_radius,
                                 is_legal,
                                 num_legal,
                                 sim U_THREAD_ID_ARG);
    }

    /**
     * THIS INTERFACE METHOD IS INTENDED TO BE CALLED AFTER FIRST AND SECOND
     * NODES OF A CHAIN HAVE BEEN SEQUENTIALLY GROWN.
     *
     * @WARNING - ASSUMES AT LEAST 1 LEGAL CANDIDATE!
     *
     * Selects among candidates with energy proportional to interaction
     *  failure fraction
     *
     * @param sample - Sample containing this mixin
     * @param log_weight - Current and output log weight of the sample
     * @param parent_id - Index of parent node within sample positions matrix
     *  (note, this is not same as candidate positions matrix)
     * @param candidate_positions - Matrix of row-major candidate positions
     * @param candidate_radius - Radius of candidate node
     * @param is_legal - [0, 1] vector : 1 if candidate is legal for
     *  selection, 0 otherwise
     * @param num_legal - Number of 1s in legal_candidates (must be > 0)
     * @param sim - Parent simulation containing global sample data
     * @return Index of selected candidate.
     */
    inline uUInt energy_select_nth(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        // This method assumes at least a single legal candidate
        uAssertBoundsInc(num_legal, U_TO_UINT(1), U_TO_UINT(is_legal.n_elem));
        // Assume is_legal is 0|1 vector
        uAssert(uMatrixUtils::sum(is_legal) == num_legal);

        // Total number of candidates (including non-legal)
        const uUInt num_candidates = U_TO_UINT(is_legal.n_elem);
        uAssert(num_candidates >= num_legal);

        /**********************************************************************
         * Defragment candidates, compute energies
         */

        uVecCol& cand_defrag_nener_buff =
            sim.get_ener_cand_defrag_nener(U_THREAD_ID_0_ARG);
        uVecCol& cand_defrag_pmass_buff =
            sim.get_ener_cand_defrag_pmass(U_THREAD_ID_0_ARG);
        uUIVecCol& cand_defrag_ix_buff =
            sim.get_ener_cand_defrag_ix(U_THREAD_ID_0_ARG);
        uAssert(cand_defrag_nener_buff.n_elem == is_legal.n_elem);
        uAssert(cand_defrag_pmass_buff.n_elem == is_legal.n_elem);
        uAssert(cand_defrag_ix_buff.n_elem == is_legal.n_elem);

        uUInt i = 0;
        for (uUInt j = 0; j < num_candidates; ++j) {
            if (is_legal.at(U_TO_MAT_SZ_T(j))) {
                uAssertBounds(i, U_TO_UINT(0), num_legal);
                uAssert(i < cand_defrag_nener_buff.n_elem);
                uAssert(i < cand_defrag_ix_buff.n_elem);
                // Tabulate energy components
                uReal nener = U_TO_REAL(0.0);

                // Chr-chr knock-in - compile time branch
                if (!intr_chr_budg_acc_kin_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_chr_budg_acc_kin_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Chr-chr knock-out - compile time branch
                if (!intr_chr_budg_acc_ko_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_chr_budg_acc_ko_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Lamina knock-in - compile time branch
                if (!intr_lam_budg_acc_kin_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_lam_budg_acc_kin_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Lamina knock-out - compile time branch
                if (!intr_lam_budg_acc_ko_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_lam_budg_acc_ko_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Nuclear body knock-in - compile time branch
                if (!intr_nucb_budg_acc_kin_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_nucb_budg_acc_kin_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Nuclear body knock-out - compile time branch
                if (!intr_nucb_budg_acc_ko_t::is_null_intr_budg) {
                    nener -= calc_ener_intr_fail<intr_nucb_budg_acc_ko_t>(
                        j, sample, sim U_THREAD_ID_ARG);
                }

                // Update negative energy for defrag candidate
                cand_defrag_nener_buff.at(i) = nener;
                // Store mapping from defrag index to original candidate index
                // @WARNING - NOTICE 'i++' UPDATE, BE CAREFUL WITH ORDER!
                cand_defrag_ix_buff.at(i++) = j;
            }
        }
        uAssert(num_legal == i);

        /**********************************************************************
         * Create (aliased) cropped views
         */

        // Create cropped view for storing candidate -energies
        uVecCol cand_defrag_nener_view(
            cand_defrag_nener_buff.memptr(), /*aux_mem*/
            num_legal,                       /*n_elem*/
            false,                           /*copy_aux_mem*/
            true);                           /*strict*/

        // Create cropped view for storing candidate probability masses
        uVecCol cand_defrag_pmass_view(
            cand_defrag_pmass_buff.memptr(), /*aux_mem*/
            num_legal,                       /*n_elem*/
            false,                           /*copy_aux_mem*/
            true);                           /*strict*/

        // Create cropped view of defragmented candidate indices
        const uUIVecCol cand_defrag_ix_view(
            cand_defrag_ix_buff.memptr(), /*aux_mem*/
            num_legal,                    /*n_elem*/
            false,                        /*copy_aux_mem*/
            true);                        /*strict*/

        /**********************************************************************
         * Determine sample probabilities
         */

        // Numerically stabilize vector by subtracting max element
        const uReal max_elem = cand_defrag_nener_view.max();
        cand_defrag_nener_view -= max_elem;
        cand_defrag_pmass_view = uMatrixUtils::exp(cand_defrag_nener_view);
        // Determine partition constant
        const uReal z = uMatrixUtils::sum(cand_defrag_pmass_view);
        uAssert(z > U_TO_REAL(0.0));

        /**********************************************************************
         * Select candidate and update log weight
         */

        // Select candidate using unnormalized probability
        i = uSisUtilsQc::rand_select_index(cand_defrag_pmass_view,
                                           z U_THREAD_ID_ARG);
        uAssertBounds(i, 0, cand_defrag_ix_view.n_elem);
        uAssertBounds(cand_defrag_ix_view.at(i), 0, is_legal.size());
        uAssert(is_legal.at(cand_defrag_ix_view.at(i)));

        // Update log weight
        /**
         * The probability of selecting a sample, Ps, is given by expression:
         *  Ps(i) = exp(-E_fail(i)) / sum_k{exp(-E_fail(k))}
         * For numerical stability, the max element has been subtracted to
         * obtain an equivalent expression:
         *  Ps(i) = [exp(-E_fail(i) - maxE) * exp(maxE)] /
         *              sum_k{exp(-E_fail(k) - maxE) * exp(maxE)}
         *        = exp(-E_fail(i) - maxE) / sum_k{exp(-E_fail(k) - maxE)}
         * The unnormalized target distribution Pt(i) is given by expression:
         *  Pt(i) = exp(-E_fail(i))
         *        = exp(-E_fail(i) - maxE) * exp(maxE)
         * This implies that the new log weight for a selected sample is:
         *   log_w' = log_w + log{ Pt(i) / Ps(i) }
         *   log_w' = log_w + log{ [exp(-E_fail(i) - maxE) * exp(maxE)] /
         *                         [exp(-E_fail(i) - maxE) /
         *                              sum_k{exp(-E_fail(k) - maxE)}] }
         *   log_w' = log_w + maxE + log{sum_k{exp(-E_fail(k) - maxE)}}
         * See http://www.hongliangjie.com/2011/01/07/logsum/
         */
        log_weight += max_elem + log(z);

        // Map defrag index to original index
        return cand_defrag_ix_view.at(i);
    }

private:
    /**
     * @param candidate_id - Candidate index (fragmented)
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return Component failure energy
     */
    template <typename t_EntrIntrFailBudgAcc>
    inline static uReal calc_ener_intr_fail(const uUInt candidate_id,
                                            const sample_t& sample,
                                            const sim_t& sim
                                                U_THREAD_ID_PARAM) {
        const enum eEnerIntrFail eix =
            t_EntrIntrFailBudgAcc::get_ener_intr_fail_type();
        uAssertBounds(
            U_TO_UINT(eix), U_TO_UINT(0), U_TO_UINT(eEnerIntrFail_num));
        const uReal scale = sim.get_ener_intr_fail_scale(eix);
        const uReal delta_fail =
            U_TO_REAL(t_EntrIntrFailBudgAcc::get_delta_fail(
                candidate_id, sample, sim U_THREAD_ID_ARG));
        return scale * delta_fail;
    }
};

#endif  // uSisIntrFailEnergyMixin_h
