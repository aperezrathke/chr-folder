//****************************************************************************
// uChromatinExportCsv.h
//****************************************************************************

/**
 * Utility for writing CSV chromatin files
 */

#ifndef uChromatinExportCsv_h
#define uChromatinExportCsv_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"

#include <iostream>
#include <string>

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Exported lines starting with this prefix string indicate comment lines
 */
#define U_CHROMATIN_EXPORT_CSV_COMMENT_PREFIX "#"

/**
 * Exported line starting with this prefix string holds the sample log weight
 */
#define U_CHROMATIN_EXPORT_CSV_LOG_WEIGHT_PREFIX \
    U_CHROMATIN_EXPORT_CSV_COMMENT_PREFIX " LOG WEIGHT:"

//****************************************************************************
// CSV export
//****************************************************************************

/**
 * Effectively a namespace for CSV chromatin export utilities
 */
class uChromatinExportCsv {
public:
    /**
     * Writes sample node positions to CSV file in format:
     *
     *  # LOG WEIGHT: <log_weight>
     *  <x1>,<y1>,<z1>
     *  ...
     *  <xN>,<yN>,<zN>
     *
     * If extended format:
     *
     *  # LOG WEIGHT: <log_weight>
     *  # NUCLEAR RADIUS: <nuclear_radius>
     *  <x1>,<y1>,<z1>,<radius1>,<chain1>
     *  ...
     *  <xN>,<yN>,<zN>,<radius1>,<chain1>
     *
     * @param fpath - path to write the exported sample
     * @param args - exporter arguments, note scale and interaction arguments
     *  are ignored
     */
    static void write_chromatin(const std::string& fpath,
                                const uChromatinExportArgs_t& args);

    /**
     * Write sample node positions to 'out' stream
     * @WARNING - DOES NOT CLOSE STREAM!
     * @param out - output stream
     * @param args - exporter args
     */
    static void write_chromatin(std::ostream& out,
                                const uChromatinExportArgs_t& args);

    /**
     *  @return log weight as CSV comment string
     */
    static std::string to_log_weight_comment(const uReal log_weight);

    /**
     * @return exporter name
     */
    static std::string get_name() { return std::string("csv"); }

    /**
     * @return exporter format
     */
    static std::string get_format() { return get_name(); }

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes) {}
};

#endif  // uChromatinExportCsv_h
