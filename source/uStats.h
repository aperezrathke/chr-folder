//****************************************************************************
// uStats.h
//****************************************************************************

/**
 * @brief Simple stats structures for profiling and tracking counts
 */

#ifndef uStats_h
#define uStats_h

#include "uBuild.h"

// For convenience - include interval logging
#include "uIntervalLogger.h"

#ifdef U_BUILD_ENABLE_STATS

#include "uAssert.h"
#include "uLogf.h"
#include "uThread.h"
#include "uTime.h"
#include "uTypes.h"

/**
 * Enumerated unique identifiers for all timer stats
 */
enum uAllTimerStats {
    uSTAT_TotalTime = 0,
    uSTAT_GetCandidateNodePositionsTime,
    uSTAT_NuclearFilterTime,
    uSTAT_CollisionFilterTime,
    uSTAT_CollisionBroadPhaseTime,
    uSTAT_CollisionNarrowPhaseTime,
    uSTAT_RcRegrowthTime,
    uSTAT_IntrChrFilterTime,
    uSTAT_IntrLamFilterTime,
    uSTAT_IntrNucbFilterTime,
    uSTAT_TimerMAX
};

/**
 * Enumerated unique identifiers for all counter stats
 */
enum uAllCounterStats {
    uSTAT_TrialsCount = 0,
    uSTAT_CompletedSamplesCount,
    uSTAT_RcRegrowthCount,
    uSTAT_RcReRegrowthCount,
    uSTAT_CounterMAX
};

#define U_SCOPED_STAT_TIMER(stat_id) \
    uStats::ScopedStatTimer ScopedTimer_##stat_id(stat_id)

#define U_INC_STAT_COUNTER(stat_id) uStats::GCounterStats[stat_id].inc_by(1)

#define U_INC_STAT_COUNTER_BY(stat_id, amount) \
    uStats::GCounterStats[stat_id].inc_by(amount)

namespace uStats {

/**
 * For timing function calls
 */
class TimerStat {
public:
    TimerStat() : m_total_time(0.0) {
#ifdef U_BUILD_ENABLE_THREADS
        uAssert(m_total_time.is_lock_free());
#endif  // U_BUILD_ENABLE_THREADS
    }

    inline void inc_total_time(const double t) {
        U_ATOMIC_ADD_DOUBLE(m_total_time, t);
    }
    inline double get_total_time() const { return m_total_time; }

private:
    /**
     * The total time of this stat
     */
    uAtomicDouble_t m_total_time;
};

/**
 * For counting how many times an event occurs
 */
class CounterStat {
public:
    CounterStat() : m_counter(0) {
#ifdef U_BUILD_ENABLE_THREADS
        uAssert(m_counter.is_lock_free());
#endif  // U_BUILD_ENABLE_THREADS
    }

    inline void inc_by(const uUInt amount) { m_counter += amount; }
    inline uUInt get_counter() const { return m_counter; }

private:
    uAtomicUInt m_counter;
};

/**
 * All the timer stat instances
 */
extern TimerStat GTimerStats[uSTAT_TimerMAX];

/**
 * All the counter stat instances
 */
extern CounterStat GCounterStats[uSTAT_CounterMAX];

/**
 * A synchronous timer which updates on destruction
 */
class ScopedStatTimer {
public:
    explicit ScopedStatTimer(const uAllTimerStats stat_id)
        : m_stat_id(stat_id), m_start_time(u_time_secs_wall()) {}

    ~ScopedStatTimer() {
        GTimerStats[m_stat_id].inc_total_time(u_time_secs_wall() -
                                              m_start_time);
    }

private:
    const uAllTimerStats m_stat_id;
    const double m_start_time;
};

/**
 * Report stats - print to stdout
 */
extern void report_stats();

}  // namespace uStats

#define U_STATS_REPORT uStats::report_stats()

#else  // !U__BUILD_ENABLE_STATS

#define U_SCOPED_STAT_TIMER(stat_id) ((void)0)
#define U_INC_STAT_COUNTER(stat_id) ((void)0)
#define U_INC_STAT_COUNTER_BY(stat_id, amount) ((void)0)
#define U_STATS_REPORT ((void)0)

#endif  // U_BUILD_ENABLE_STATS

#endif  // uStats_h
