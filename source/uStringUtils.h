//****************************************************************************
// uStringUtils.h
//****************************************************************************

/**
 * @brief Utilities for manipulating and converting to/from strings.
 */

#ifndef uStringUtils_h
#define uStringUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uTypes.h"

#include <sstream>
#include <string>

// Utilities for testing and converting strings to other data types
namespace uStringUtils {

    /**
     * Determine if parameter string 's' begins with string 'prefix'
     * @param s - string to determine if it starts with 'prefix'
     * @param prefix - checks to see if first prefix.size() characters match
     * @return TRUE if 's' starts with 'prefix', FALSE o/w
     */
    inline uBool starts_with(const std::string& s, const std::string& prefix) {
        return (0 == s.compare(0, prefix.size(), prefix));
    }

    /**
     * Determine if parameter string 's' ends with string 'suffix'
     * @param s - string to determine if it ends with 'prefix'
     * @param suffix - checks to see if last suffix.length() characters match
     * @return TRUE if 's' ends with 'suffix', FALSE o/w
     */
    inline uBool ends_with(const std::string& s, const std::string& suffix) {
        if (s.length() >= suffix.length()) {
            return (0 == s.compare(s.length() - suffix.length(),
                                   suffix.length(),
                                   suffix));
        }
        return uFALSE;
    }

    /**
     * Utility method to convert from a string type to any other data type
     *
     * Copied from ConfigFile.h (protected member)
     *
     * @param s - string to convert
     * @return the value of the converted string
     */
    template <class T>
    inline T string_as_T(const std::string& s) {
        // Convert from a string to a T
        // Type T must support >> operator
        T t;
        std::istringstream ist(s);
        ist >> t;
        return t;
    }

#ifdef U_BUILD_CXX_11

    /**
     * Specialization to convert string to default floating point type
     */
    template <>
    inline uReal string_as_T<uReal>(const std::string& s) {
        return static_cast<uReal>(std::stod(s));
    }

    /**
     * Specialization to convert string to integer type
     */
    template <>
    inline int string_as_T<int>(const std::string& s) {
        return std::stoi(s);
    }

    /**
     * Specialization to convert string to unsigned integer type
     */
    template <>
    inline unsigned int string_as_T<unsigned int>(const std::string& s) {
        return std::stoul(s);
    }

    /**
     * Specialization to convert string to string (do nothing)
     */
    template <>
    inline std::string string_as_T<std::string>(const std::string& s) {
        return s;
    }

#endif  // U_BUILD_CXX_11

    /**
     * NAIVE INTEGER CONVERSION ROUTINE(S)
     *
     * THESE ROUTINE(S) ARE NOT AS ROBUST AS STL OR BOOST ROUTINES, BUT THEY
     * CAN BE SIGNIFICANTLY (~7X) FASTER! SHOULD ONLY BE USED ON TRUSTED DATA!
     *
     * See:
     *  http://jsteemann.github.io/blog/2016/06/02/fastest-string-to-uint64-conversion-method/
     *  https://stackoverflow.com/questions/16826422/c-most-efficient-way-to-convert-string-to-int-faster-than-atoi
     *  https://tombarta.wordpress.com/2008/04/23/specializing-atoi/
     */

    /**
     * Unchecked utility method to convert from a string type to an unsigned
     * integral type.
     * @WARNING - ASSUMES T_UINT IS AN UNSIGNED INTEGRAL TYPE
     * @WARNING - ASSUMES 's' IS ONLY INTEGER DIGITS AND NO OTHER CHARACTERS.
     *  FOR INSTANCE, 's' MAY *NOT* CONTAIN '-' (sign), '.' (dot), OR *ANY*
     *  CHARACTER THAT IS NOT IN '0123456789'
     *
     * @param s - string to convert
     * @return the unsigned integral value of the converted string
     */
    template <class T_UINT>
    inline T_UINT string_as_T_UINT_naive(const std::string& s) {
        T_UINT result = 0;
        const char* p = s.c_str();
        while (*p) {
            result *= 10;
            result += *(p++) - '0';
        }
#ifdef U_BUILD_ENABLE_ROBUST_PARSER_CHECKS
        uAssert(result == string_as_T<T_UINT>(s));
#endif // U_BUILD_ENABLE_ROBUST_PARSER_CHECKS
        return result;
    }

}  // namespace uStringUtils

#ifdef U_BUILD_CXX_11

/**
 * Wrapper for converting "arbitrary" value to a string
 * @return string representation of value
 */
template <typename t_>
inline std::string u2Str(const t_& val) {
    return std::to_string(val);
}

#else  // C++11 not-enabled

/**
 * Wrapper for converting "arbitrary" value to a string
 * Copied from ConfigFile.h (protected member)
 * @return string representation of value
 */
template <typename t_>
inline std::string u2Str(const t_& val) {
    // Convert from a T to a string
    // Type T must support << operator
    std::ostringstream ost;
    ost << val;
    return ost.str();
}

#endif  // U_BUILD_CXX_11

/**
 * Specialization for string objects (do not perform conversion)
 */
template <>
inline std::string u2Str(const std::string& val) {
    return val;
}

#endif  // uStringUtils_h
