//****************************************************************************
// uSisIntrChrFilterKinIntra.h
//****************************************************************************

/**
 * Filter for within-locus, knock-in chr-chr interactions
 */

#ifndef uSisIntrChrFilterKinIntra_h
#define uSisIntrChrFilterKinIntra_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uDistUtils.h"
#include "uIntrLibDefs.h"
#include "uSisIntrChrCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// uSisIntrChrFilterKinIntra
//****************************************************************************

/**
 * Stateless class for within-locus, knock-in chr-chr interaction testing
 */
template <typename t_SisGlue>
class uSisIntrChrFilterKinIntra {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utility type
     */
    typedef uSisIntrChrCommonFilt<t_SisGlue> common_filt_t;

    /**
     * Determines if any candidate positions violate the parameter interaction
     * constraint - specifically a within (intra) locus, knock-in constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrChrFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim) {
        uAssert(common_filt_t::check_format_sloc(c, sim));
        // See if we can avoid interaction testing
        if (common_filt_t::should_early_out_sloc(
                c.node_nid, c.frag_a_nid_hi + U_TO_UINT(1), c.frag_b_nid_hi)) {
            return;
        }
        // Defer to common routine
        common_filt_t::mark_viol_kin_full_A(policy,
                                            p_policy_args,
                                            out_legal_candidates_primed,
                                            c,
                                            candidate_centers,
                                            sample,
                                            sim);
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a within (intra) locus, knock-in constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrChrFilterCore_t& c,
                           const uReal* const B,
                           const sample_t& sample,
                           const sim_t& sim) {
        uAssert(common_filt_t::check_format_sloc(c, sim));
        // See if we can avoid interaction testing
        if (common_filt_t::should_early_out_sloc(
                c.node_nid, c.frag_b_nid_lo, c.frag_b_nid_hi)) {
            return uFALSE;
        }
        // Defer to common routine
        return common_filt_t::is_satisf_kin_full_A(c, B, sample, sim);
    }

private:
    // Disallow any form of instantiation
    uSisIntrChrFilterKinIntra(const uSisIntrChrFilterKinIntra&);
    uSisIntrChrFilterKinIntra& operator=(const uSisIntrChrFilterKinIntra&);
};

#endif  // uSisIntrChrFilterKinIntra_h
