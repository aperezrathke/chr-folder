//****************************************************************************
// uSisNullIntrChrMixin.h
//****************************************************************************

/**
 * Defines a null (non-functional) chrome-to-chrome interaction mixin.
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisNullIntrChrMixin_h
#define uSisNullIntrChrMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibBudgUtil.h"
#include "uSisNullIntrChrSeedPolicy.h"
#include "uSisNullSimLevelMixin.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNullIntrChrSimLevelMixin : public uSisNullSimLevelMixin<t_SisGlue> {
public:
    // Accessors

    /**
     * @return Chr-chr knock-in interaction failure budget
     */
    uUInt get_intr_chr_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Chr-chr knock-out interaction failure budget
     */
    uUInt get_intr_chr_ko_max_fail() const { return U_TO_UINT(0); }

    // Export interface

    /**
     * Copies interaction fragments to 'out'
     */
    void get_intr_chr_frags(uUIMatrix& out) const { out.clear(); }

    /**
     * Copies knock-in interactions to 'out'
     */
    void get_intr_chr_kin(uUIMatrix& out) const { out.clear(); }

    /**
     * Copies knock-out interactions to 'out'
     */
    void get_intr_chr_ko(uUIMatrix& out) const { out.clear(); }
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNullIntrChrMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Chr-chr knock-in budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_chr_budg_acc_kin_t;

    /**
     * Chr-chr knock-out budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_chr_budg_acc_ko_t;

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Determines which positions satisfy interaction constraints
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_chr_filter(uBoolVecCol& out_legal_candidates_primed,
                         const uMatrix& candidate_centers,
                         const uReal candidate_radius,
                         const uUInt parent_node_id,
                         const sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) const {}

    /**
     * Updates sample mixin states with new node information
     * @param node_id - a unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param candidate_id - identifies candidate that was selected
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    void intr_chr_update(const uUInt node_id,
                         const uReal* point,
                         const uReal radius,
                         const uUInt candidate_id,
                         sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) const {}

    // Accessors

    /**
     * @return Chr-chr interaction seed policy for first seeded node
     */
    uSisNullIntrChrSeedPolicy_t get_intr_chr_seed_first_policy() {
        return uSisNullIntrChrSeedPolicy_t();
    }

    /**
     * @return Chr-chr interaction seed policy for nodes seeded after first
     */
    uSisNullIntrChrSeedPolicy_t get_intr_chr_seed_next_policy() {
        return uSisNullIntrChrSeedPolicy_t();
    }
};

#endif  // uSisNullIntrChrMixin_h
