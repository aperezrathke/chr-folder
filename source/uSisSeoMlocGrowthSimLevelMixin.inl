//****************************************************************************
// uSisSeoMlocGrowthSimLevelMixin.inl
//****************************************************************************

/**
 * Sis = sequential importance sampling
 * Seo = Single-end ordered growth from only one end of chain. Ordered = for
 *  each locus, nodes are grown from first to last node always
 * Mloc = built for multiple loci = multiple chains
 */
namespace uSisSeoMloc {
/**
 * Mixin encapsulating global (sample independent) data and utilities
 * In this case, the simulation level growth mixin manages the number of
 * grown nodes that all samples (that are not dead) have achieved.
 *
 * Class also provides various mappings to convert to/from linearized, 1-D
 * representations to 2-D representations. Specifically, the following
 * terminology is used:
 *  node identifier : nid : Represents the linearized 1-D index of a monomer
 *      node. This value is an integer in range [0, total # of nodes). It
 *      also is the same as the column index containing the node position in
 *      the node positions matrix.
 *  locus identifier : lid : The first component of the 2-D tuple (lid, nof).
 *      Identifier of the locus chain, where a locus is a separate polymer
 *      containing its own set of monomer nodes.
 *  node offset : nof : The second component of the 2-D tuple (lid, nof).
 *      Determines placement of monomer node within its parent locus chain.
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class GrowthSimLevelMixin_threaded
    : public uSisSeGrowthSimLevelMixin_threaded<t_SisGlue> {
private:
    // https://stackoverflow.com/questions/1643035/propagating-typedef-from-based-to-derived-class-for-template
    typedef uSisSeGrowthSimLevelMixin_threaded<t_SisGlue> segrbase_t;
#else
class GrowthSimLevelMixin_serial
    : public uSisSeGrowthSimLevelMixin_serial<t_SisGlue> {
private:
    typedef uSisSeGrowthSimLevelMixin_serial<t_SisGlue> segrbase_t;
#endif  // U_INJECT_THREADED_TLS
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Clear any old state
        this->clear();

        // Initialize single-end growth buffers
        segrbase_t::init(sim, config U_THREAD_ID_ARG);

        // Initialize various mappings from 1-D node identifiers to 2-D
        // (locus identifier, locus offset) tuples
        const uUInt max_total_num_nodes = sim.get_max_total_num_nodes();
        uAssert(max_total_num_nodes >= 1);
        const std::vector<uUInt>& max_nodes_at_locus =
            sim.get_max_num_nodes_at_locus();
        uAssertBoundsInc(max_nodes_at_locus.size(), 1, max_total_num_nodes);
        m_growth_nid_to_lid_map.zeros(max_total_num_nodes);
        m_growth_nid_to_nof_map.zeros(max_total_num_nodes);
        m_growth_lid_nof_to_nid_map.resize(max_nodes_at_locus.size());
        uUInt num_singleton_loci = 0;
        // nid = node identifier
        // lid = locus identifier
        // nof = node offset
        uUInt nid = 0;
        uUInt lid = 0;
        for (uUInt lid = 0; lid < max_nodes_at_locus.size(); ++lid) {
            const uUInt n = max_nodes_at_locus[lid];
            uAssert(n > 0);
            uAssertBounds(lid, 0, m_growth_lid_nof_to_nid_map.size());
            m_growth_lid_nof_to_nid_map[lid].resize(n);
            for (uUInt nof = 0; nof < n; ++nof) {
                uAssertBounds(nid, 0, m_growth_nid_to_lid_map.n_elem);
                m_growth_nid_to_lid_map.at(nid) = lid;
                uAssertBounds(nid, 0, m_growth_nid_to_nof_map.n_elem);
                m_growth_nid_to_nof_map.at(nid) = nof;
                uAssertBounds(nof, 0, m_growth_lid_nof_to_nid_map[lid].size());
                m_growth_lid_nof_to_nid_map[lid][nof] = nid;
                ++nid;
            }
            num_singleton_loci += (n == 1);
        }
        uAssert(nid == max_total_num_nodes);

        // Initialize growth counts
        m_growth_num_seed = U_TO_UINT(max_nodes_at_locus.size());
        uAssertBoundsInc(num_singleton_loci, 0, m_growth_num_seed);
        m_growth_num_2nd = m_growth_num_seed - num_singleton_loci;
        uAssertBoundsInc(m_growth_num_2nd, 0, m_growth_num_seed);
        uAssertBoundsInc(
            (m_growth_num_seed + m_growth_num_2nd), 1, max_total_num_nodes);
        m_growth_max_steps =
            (max_total_num_nodes - m_growth_num_seed - m_growth_num_2nd) +
            (m_growth_num_seed > 0) + (m_growth_num_2nd > 0);
        uAssertBoundsInc(m_growth_max_steps, 1, max_total_num_nodes);

        // Initialize base loci growth order
        m_growth_sorted_loci_order.zeros(max_total_num_nodes);
        std::vector<uUInt> nodes_at_locus = max_nodes_at_locus;
        // If this assert trips, then uint16_t is insufficient number of bits
        // to represent the number of chains in each sample.
        uAssert(nodes_at_locus.size() <= static_cast<size_t>(U_UINT16_MAX));
        nid = 0;
        while (nid < max_total_num_nodes) {
            for (lid = 0; lid < max_nodes_at_locus.size(); ++lid) {
                if (nodes_at_locus[lid] > 0) {
                    uAssertBounds(nid, 0, m_growth_sorted_loci_order.n_elem);
                    m_growth_sorted_loci_order.at(nid++) = lid;
                    --(nodes_at_locus[lid]);
                }
            }
        }

        // Initialize seed positions buffers. Allocating
        // col-major positions where each col is an (X, Y, Z, R) where X, Y, Z
        // are the node center and R node radius.
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(m_growth_seed_positions,
                                         zeros,
                                         uDim_num_extended,  /*n_rows*/
                                         m_growth_num_seed); /*n_cols*/
    }

    /**
     * Resets to default state
     */
    inline void clear() {
        // Clear single-end growth buffers
        segrbase_t::clear();
        // Clear seed positions buffer
        m_growth_seed_positions.clear();
        // Clear base permutation
        m_growth_sorted_loci_order.clear();
        m_growth_num_seed = 0;
        m_growth_num_2nd = 0;
        m_growth_max_steps = 0;
        // Clear node mappings
        m_growth_nid_to_lid_map.clear();
        m_growth_nid_to_nof_map.clear();
        m_growth_lid_nof_to_nid_map.clear();
    }

    // Accessors

    /**
     * @return col-major positions buffer used for small-scale collision
     *  detection during seed() calls. Each col of the matrix is an
     *  (X, Y, Z, R) tuple where (X, Y, Z) are the node positions and R is the
     *  node radius.
     */
    inline uMatrix& get_growth_seed_positions(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_growth_seed_positions);
    }

    /**
     * @return non-permuted loci growth order
     */
    inline const uUI16VecCol& get_growth_sorted_loci_order() const {
        return m_growth_sorted_loci_order;
    }

    /**
     * @return 1-D node identifier from 2-D (lid, nof) tuple
     */
    inline uUInt get_growth_nid(const uUInt lid, const uUInt nof) const {
        uAssertBounds(lid, 0, m_growth_lid_nof_to_nid_map.size());
        uAssertBounds(nof, 0, m_growth_lid_nof_to_nid_map[lid].size());
        return m_growth_lid_nof_to_nid_map[lid][nof];
    }

    /**
     * @return locus identifier associated with with node identifier
     */
    inline uUInt get_growth_lid(const uUInt nid) const {
        uAssertBounds(nid, 0, m_growth_nid_to_lid_map.n_elem);
        return m_growth_nid_to_lid_map.at(nid);
    }

    /**
     * @return locus node offset associated with node identifier
     */
    inline uUInt get_growth_nof(const uUInt nid) const {
        uAssertBounds(nid, 0, m_growth_nid_to_nof_map.n_elem);
        return m_growth_nid_to_nof_map.at(nid);
    }

    /**
     * Obtain 2-D (lid, nof) tuple from 1-D node identifier
     */
    inline void get_growth_lid_nof(uUInt& lid,
                                   uUInt& nof,
                                   const uUInt nid) const {
        lid = this->get_growth_lid(nid);
        nof = this->get_growth_nof(nid);
    }

    /**
     * @param num_completed_grow_steps - The number of growth
     *  calls (seed() + grow_*()) that have completed. This value is
     *  reset between trials.
     * @return number of grown nodes at each sample as of the last
     * finished grow step
     */
    inline uUInt get_num_grown_nodes(
        const uUInt num_completed_grow_steps) const {
        //  seed phase: # of nodes grown = # of loci
        //  2nd phase: # of nodes = # of active loci
        //  n > 2 phase: # of nodes grown = (n-2) + (n_seed) + (n_2nd)
        uAssert(m_growth_num_seed > 0);
        uAssertBoundsInc(m_growth_num_2nd, 0, m_growth_num_seed);
        return ((num_completed_grow_steps >= 1) * m_growth_num_seed) +
               ((num_completed_grow_steps >= 2) * m_growth_num_2nd) +
               ((num_completed_grow_steps > 2) *
                (num_completed_grow_steps - 2));
    }

    /**
     * @return number of nodes grown during seed() phase
     */
    inline uUInt get_growth_num_seed() const {
        uAssert(m_growth_num_seed > 0);
        return m_growth_num_seed;
    }

    /**
     * @return number of nodes grown during grow_2nd() phase
     */
    inline uUInt get_growth_num_2nd() const {
        uAssertBoundsInc(m_growth_num_2nd, 0, m_growth_num_seed);
        return m_growth_num_2nd;
    }

    /**
     * @param sim - the parent simulation
     * @return - the total number of seed() + grow_*() calls required to
     *  grow a single sample.
     */
    inline uUInt get_max_num_grow_steps(const sim_t& sim) const {
        uAssertBoundsInc(m_growth_max_steps, 1, sim.get_max_total_num_nodes());
        return m_growth_max_steps;
    }

private:
    /**
     * Buffer for storing node positions during seed phase
     */
    mutable U_DECLARE_TLS_DATA(uMatrix, m_growth_seed_positions);

    /**
     * Initial permutation, all samples permute this vector
     */
    uUI16VecCol m_growth_sorted_loci_order;

    /**
     * Maps 1-D node identifier to its locus identifier
     */
    uUIVecCol m_growth_nid_to_lid_map;

    /**
     * Maps 1-D node identifier to its offset within locus
     */
    uUIVecCol m_growth_nid_to_nof_map;

    /**
     * Maps 2-D (locus identifier, node offset) to 1-D node identifier
     */
    std::vector<std::vector<uUInt> > m_growth_lid_nof_to_nid_map;

    /**
     * Number of monomers grown during seed phase
     */
    uUInt m_growth_num_seed;

    /**
     * Number of monomers grown during second grow phase
     */
    uUInt m_growth_num_2nd;

    /**
     * Max number of seed() + grow_*() calls needed to complete a sample
     */
    uUInt m_growth_max_steps;
};

}  // namespace uSisSeoMloc
