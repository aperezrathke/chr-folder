//****************************************************************************
// uBuild.h
//****************************************************************************

/**
 * @brief Global build defines
 */

#ifndef uBuild_h
#define uBuild_h

/**
 * OS detection
 */
#ifdef _WIN32
#   define U_BUILD_OS_WINDOWS
#else
#   define U_BUILD_OS_UNIX
#endif // _WIN32

/**
 * Build types
 */
#ifndef U_BUILD_TYPE_DEBUG
// _DEBUG is defined by Visual Studio (non-ANSI compliant)
// NDEBUG is defined by CMake for GCC release builds
// NDEBUG is part of C++ ANSI standard for disabling assertions
#   if defined(_DEBUG) || (defined(U_BUILD_OS_UNIX) && !defined(NDEBUG))
#       define U_BUILD_TYPE_DEBUG
#   else
// Switch to force enable debug build
//#       define U_BUILD_TYPE_DEBUG
#endif
#endif // U_BUILD_TYPE_DEBUG

/**
 * Compiler detection
 */
#ifdef _MSC_VER
#   define U_BUILD_COMPILER_MSVC
#elif (defined(__ICC) || defined(__ICL) || defined(__INTEL_COMPILER))
#   define U_BUILD_COMPILER_ICC
#elif (defined(__GNUC__))
#   define U_BUILD_COMPILER_GCC
#else
#   error Unrecognized compiler
#endif

/**
 * C++11
 */
// http://stackoverflow.com/questions/5047971/how-do-i-check-for-c11-support
#ifndef U_BUILD_CXX_11
#   ifdef U_BUILD_COMPILER_MSVC
#       if (__cplusplus >= 199711L)
#           define U_BUILD_CXX_11
#       endif
#   elif (__cplusplus >= 201103L)
#       define U_BUILD_CXX_11
#   endif
#endif // U_BUILD_CXX_11

/**
 * Benchmarking: enable benchmark build which has following effects:
 *  - No random seeding (rand is deterministic)
 *  - Enable commandlets (needed for benchmark commandlet)
 *  - Enable interval logging (needed for total execution time)
 *
 * In addition, you will likely want a serial version instead of
 * multi-threaded depending on how well a profiler can account for
 * threading. Also, use of interval logging with threads may not
 * be entirely safe depending on quality control or trial runner
 * mixins being used.
 *
 * To use, run release build of benchmark executable. The standard
 * commandlet for benchmarking can be ran with command line:
 *  <path_to_exe> -benchmark
 */
#ifndef U_BUILD_ENABLE_BENCHMARK
// Uncomment to enable benchmark build
//#   define U_BUILD_ENABLE_BENCHMARK
#endif // U_BUILD_ENABLE_BENCHMARK

/**
 * This master switch toggles assertion checking
 * (will enable even if NDEBUG is defined)
 */
//#define U_BUILD_ENABLE_ASSERT

// Allow assertions to be on by default for debug builds and off o/w
#ifndef U_BUILD_ENABLE_ASSERT
#   ifdef U_BUILD_TYPE_DEBUG
#       define U_BUILD_ENABLE_ASSERT
#   else
#       ifndef NDEBUG
#           define NDEBUG
#       endif // NDEBUG
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ENABLE_ASSERT

/**
 * Enables all-to-all brute force checking of self-avoidance and nuclear
 * constraint properties to ensure that all samples are valid. Very slow so we
 * are making this a switch even if assertions are enabled.
 */
#ifndef U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS
#   ifdef U_BUILD_ENABLE_ASSERT
//#       define U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS
#   endif // U_BUILD_ENABLE_ASSERT
#endif // U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS

/**
 * Performs extra string parsing checks to verify data imported properly.
 * These extra checks can be significantly slower and should only be enabled
 * if data integrity is of concern.
 */
#ifndef U_BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   ifdef U_BUILD_ENABLE_ASSERT
//#       define U_BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   endif // U_BUILD_ENABLE_ASSERT
#endif // U_BUILD_ENABLE_ROBUST_PARSER_CHECKS

/**
 * Enables logging utilities
 */
#ifndef U_BUILD_ENABLE_LOGGING
// Benchmarking requires logging of intervals
#   ifdef U_BUILD_ENABLE_BENCHMARK
#       define U_BUILD_ENABLE_LOGGING
#   else
// Comment out line below to disable logging
#       define U_BUILD_ENABLE_LOGGING
#   endif // U_BUILD_ENABLE_BENCHMARK
#endif // U_BUILD_ENABLE_LOGGING

/**
 * Switch to toggle stats profiling
 */
#ifndef U_BUILD_ENABLE_STATS
#   ifdef U_BUILD_TYPE_DEBUG
#       define U_BUILD_ENABLE_STATS
#   else
// Uncomment line below to enable stats in release
//#       define U_BUILD_ENABLE_STATS
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ENABLE_STATS

/** 
 * Interval logging allows use of timers which auto report
 * durations to console, hence dependence on logging
 */
#if !defined(U_BUILD_ENABLE_INTERVAL_LOGGING) && defined(U_BUILD_ENABLE_LOGGING)
#   if defined(U_BUILD_ENABLE_STATS) || defined(U_BUILD_ENABLE_BENCHMARK)
#       define U_BUILD_ENABLE_INTERVAL_LOGGING
#   else
// Uncomment line below to override enable even when stats are disabled
//#       define U_BUILD_ENABLE_INTERVAL_LOGGING
#   endif // U_BUILD_ENABLE_STATS
#endif // U_BUILD_ENABLE_INTERVAL_LOGGING

/**
 * This switch toggles if commandlets should be enabled
 */
#ifndef U_BUILD_ENABLE_COMMANDLETS
#   if defined(U_BUILD_TYPE_DEBUG) || defined(U_BUILD_ENABLE_BENCHMARK) 
#       define U_BUILD_ENABLE_COMMANDLETS
#   else
// Uncomment line below to enable commandlets in release
//#      define U_BUILD_ENABLE_COMMANDLETS
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ENABLE_COMMANDLETS

/**
 * This switch toggles if tests should be enabled
 */
#ifndef U_BUILD_ENABLE_TESTS
#   if defined(U_BUILD_TYPE_DEBUG) 
#       define U_BUILD_ENABLE_TESTS
#   else
// Uncomment line below to enable tests in release
//#       define U_BUILD_ENABLE_TESTS
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ENABLE_TEST

/**
 * This switch toggles if armadillo library should be built with extra debug
 * information such as bounds checking.
 */
// Allow armadillo debug information for debug builds and off o/w
#ifndef U_BUILD_ARMADILLO_DEBUG
#   ifdef U_BUILD_TYPE_DEBUG
#       define U_BUILD_ARMADILLO_DEBUG
#   else
#       define ARMA_NO_DEBUG
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ARMADILLO_DEBUG

/**
 * Switch to toggle seeding the random number generator
 */
#ifndef U_BUILD_SEED_RAND
#   if !defined(U_BUILD_TYPE_DEBUG) \
        && !defined(U_BUILD_ENABLE_STATS) \
        && !defined(U_BUILD_ENABLE_BENCHMARK) \
        && !defined(U_BUILD_ENABLE_TESTS)
#       define U_BUILD_SEED_RAND
#   endif
#endif // U_BUILD_SEED_RAND

/**
 * Improvement upon Google sparsehash. Offers close to the same memory usage
 * as Google sparsehash with performance closer to unordered_map.
 */
#ifndef U_BUILD_USE_SPARSEPP_HASH
// (Un)comment below to toggle usage of sparsepp hash library.
//#   define U_BUILD_USE_SPARSEPP_HASH
#endif // U_BUILD_USE_SPARSEPP_HASH

/**
 * Enable Google sparsehash instead of default unordered_map
 * OPTIMIZATION: Benchmark demonstrated 2X speedup using
 * unordered_map over sparsehash; hence disabling sparsehash.
 */
#ifndef U_BUILD_USE_GOOGLE_SPARSEHASH
// (Un)comment below to toggle usage of google sparsehash library. If
// disabled, will default to std::unordered_map
//#   define U_BUILD_USE_GOOGLE_SPARSEHASH
#endif // U_BUILD_USE_GOOGLE_SPARSEHASH

#ifdef U_BUILD_USE_GOOGLE_SPARSEHASH
#   ifndef U_BUILD_USE_GOOGLE_SPARSEHASH_DENSE
// (Un)comment below to toggle usage of google dense (perf) vs sparse (mem)
// hash structures
#       define U_BUILD_USE_GOOGLE_SPARSEHASH_DENSE
#   endif // U_BUILD_USE_GOOGLE_SPARSEHASH_DENSE
#endif // U_BUILD_USE_GOOGLE_SPARSEHASH

/**
 * Allows threaded implementations for growth phases and rejection control
 */
#ifndef U_BUILD_ENABLE_THREADS
#   ifdef U_BUILD_COMPILER_MSVC
// (Un) comment below to toggle usage of threading in visual studio
//#       define U_BUILD_ENABLE_THREADS
#   else
// Note, for Linux builds, we typically prefer the CMake build system to
// configure this flag; therefore, it should be undefined by default.
// However, to override build system behavior, simply enable it here.
//#       define U_BUILD_ENABLE_THREADS
#   endif // U_BUILD_COMPILER_MSVC
#endif // U_BUILD_ENABLE_THREADS

#ifndef U_BUILD_ENABLE_BOOST_MEM_ALIGN
// (Un) comment below to toggle usage of Boost for aligned allocations
//#   define U_BUILD_ENABLE_BOOST_MEM_ALIGN
#endif // U_BUILD_ENABLE_BOOST_MEM_ALIGN

/**
 * Allows bitsets to use 64-bit block size
 */
#ifndef U_BUILD_ENABLE_64BIT_BLOCK
// (Un) comment below to toggle usage of 64 bit blocks
#   define U_BUILD_ENABLE_64BIT_BLOCK
#endif // U_BUILD_ENABLE_64BIT_BLOCK

/**
 * Allows use of popcnt intrinsic for counting number
 * of bits in a dynamic bitset
 */
#ifndef U_BUILD_ENABLE_POPCNT
// (Un) comment below to toggle usage of popcnt intrinsic
#   define U_BUILD_ENABLE_POPCNT
#endif // U_BUILD_ENABLE_POPCNT

#ifndef U_BUILD_ENABLE_SSE_COLLISION
#   ifndef U_BUILD_DISABLE_SSE_COLLISION
// (Un) comment below to toggle usage of SSE intrinsics within narrow phase
// collision filtering - requires AVX2 support!
#       define U_BUILD_ENABLE_SSE_COLLISION
#   endif // U_BUILD_DISABLE_SSE_COLLISION
#endif // U_BUILD_ENABLE_SSE_COLLISION

/**
 * Default is 32-bit collision grid key which seems to serve most biological
 * use cases regarding nuclear size. To enable 64-bit grid keys (e.g. to
 * simulate a negligible confinement effect), then enable this switch 
 */
#ifndef U_BUILD_ENABLE_BIG_GRID_COLLISION64
// (Un) comment below to toggle 64-bit grid key support
//#   define U_BUILD_ENABLE_BIG_GRID_COLLISION64
#endif // U_BUILD_ENABLE_BIG_GRID_COLLISION64

/**
 * Enable super-massive nuclear sizes with 128-bit grid keys
 */
#ifndef U_BUILD_ENABLE_BIG_GRID_COLLISION128
// (Un) comment below to toggle 128-bit grid key support
//#   define U_BUILD_ENABLE_BIG_GRID_COLLISION128
#endif // U_BUILD_ENABLE_BIG_GRID_COLLISION128

/**
 * Enable 'big' element identifiers for broad phase collision. The default
 * only allows a maximum monomer count of 2^16 - 1 = 65,536 - 1; this flag 
 * allows a maximum monomer count of 2^32 - 1 = 4,294,967,296 - 1
 */
#ifndef U_BUILD_ENABLE_BIG_ELEM_ID
// @HACK - Enable if big grid is enabled
#   if defined(U_BUILD_ENABLE_BIG_GRID_COLLISION64) \
        || defined(U_BUILD_ENABLE_BIG_GRID_COLLISION128)
#       define U_BUILD_ENABLE_BIG_ELEM_ID
#   else
//#       define U_BUILD_ENABLE_BIG_ELEM_ID
#   endif // U_BUILD_ENABLE_BIG_GRID_COLLISION64|128
#endif // U_BUILD_ENABLE_BIG_ELEM_ID

#ifndef U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
// Enable by default in debug
#   ifdef U_BUILD_TYPE_DEBUG
#       define U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
#   else
// (Un) comment below to toggle weight summary statistics reporting in rejection control in release mode
//#       define U_BUILD_ENABLE_RC_WEIGHT_SUMMARY
#   endif // U_BUILD_TYPE_DEBUG
#endif // U_BUILD_ENABLE_RC_WEIGHT_SUMMARY

/**
 * @HACK! Resampling operations based on sample weight are greatly simplified
 * if dead samples are simply assigned a very low probability of selection.
 * This may have to be revisited if actual log weights can be this negative!
 */
#define U_SIS_DEAD_SAMPLE_LOG_WEIGHT U_TO_REAL(-7331.0)

/**
 * Prefix to denote command line arguments meant for trial runner child
 * configurations
 */
#define U_CONFIG_CHILD_TR_CMDARG_PREFIX '.'

/**
 * Prefix to denote command line arguments meant for trial runner select child
 * configurations
 */
#define U_CONFIG_CHILD_TR_SEL_CMDARG_PREFIX '*'

/**
 * Prefix to denote command line arguments meant for quality control child
 * configurations
 */
#define U_CONFIG_CHILD_QC_CMDARG_PREFIX '+'

/**
 * Cap maximum grow chunk size for multi-threaded build to have better thread
 * granularity for trials beyond the first
 */
#ifdef U_BUILD_ENABLE_THREADS
#   define U_BUILD_MAX_GROW_CHUNK_SIZE U_TO_UINT(8)
#endif // U_BUILD_ENABLE_THREADS

 /**
  * Default weight smoothing (or boosting) exponent
  */
#define U_SIS_QC_SAMPLER_POWER_DEFAULT_ALPHA U_TO_REAL(1.0)

/**
 * Default number of look-ahead grow steps for Delphic simulations
 */
#define U_SIS_DELPHIC_DEFAULT_STEPS U_TO_UINT(0)

/**
 * Warning is triggered if Delphic look-ahead is greater than this value
 */
#define U_SIS_DELPHIC_WARN_STEPS_THRESH U_TO_UINT(50)

#endif  // uBuild_h
