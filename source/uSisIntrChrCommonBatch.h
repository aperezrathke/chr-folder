//****************************************************************************
// uSisIntrChrCommonBatch.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utilities for chr-chr interaction batch tests
 */

#ifndef uSisIntrChrCommonBatch_h
#define uSisIntrChrCommonBatch_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibDefs.h"
#include "uSisIntrChrFilterKinInter.h"
#include "uSisIntrChrFilterKinIntra.h"
#include "uSisIntrChrFilterKoInter.h"
#include "uSisIntrChrFilterKoIntra.h"
#include "uSisUtilsGrowth.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Typedefs and structs
//****************************************************************************

/**
 * Collection of loci tables
 */
typedef struct {
    /**
     * Table of within-locus knock-in interactions
     */
    uSisIntrLociTable_t loci_kin_intra;

    /**
     * Table of between-loci knock-in interactions
     */
    uSisIntrLociTable_t loci_kin_inter;

    /**
     * Table of within-locus knock-out interactions
     */
    uSisIntrLociTable_t loci_ko_intra;

    /**
     * Table of between-loci knock-out interactions
     */
    uSisIntrLociTable_t loci_ko_inter;
} uSisIntrChrLociTables_t;

/**
 * Core arguments needed for batch marking candidates which violate
 * interaction constraints
 */
typedef struct {
    /**
     * Matrix containing candidate positions to test for interaction
     * constraint violations.
     */
    const uMatrix* p_candidate_centers;
    /**
     * The radius of the candidate node (assumes all have same radius)
     */
    uReal candidate_radius;
    /**
     * The node identifier at which candidate will be placed
     */
    uUInt candidate_nid;
    /**
     * Locus containing candidate node
     */
    uUInt candidate_lid;
    /**
     * Table of interaction constraint column identifiers partitioned by loci
     */
    const uSisIntrLociTable_t* p_intr_loci;
    /**
     * 2 x N matrix where N is number of interaction pairs, each column is an
     * interaction pair, each row is column index into fragments (frags) matrix
     */
    const uUIMatrix* p_intrs;
    /**
     * 2 x M matrix where M is number of fragments, defines the node spans
     * constituting a chromatin fragment
     */
    const uUIMatrix* p_frags;
} uSisIntrChrBatchMarkViolCore_t;

/**
 * Core arguments needed for batch checking if any interaction constraints
 * have been satisfied
 */
typedef struct {
    /**
     * Unique chromatin node identifier for latest node added to sample
     */
    uUInt nid;
    /**
     * Locus identifier corresponding to 'nid'
     */
    uUInt lid;
    /**
     * The radius of the added node at 'nid'
     */
    uReal radius;
    /**
     * 3D coordinates of node centroid at 'nid'
     */
    const uReal* B;
    /**
     * 2 x N matrix where N is number of interaction pairs, each column is an
     * interaction pair, each row is column index into fragments (frags) matrix
     */
    const uUIMatrix* p_intrs;
    /**
     * 2 x M matrix where M is number of fragments, defines the node spans
     * constituting a chromatin fragment
     */
    const uUIMatrix* p_frags;
} uSisIntrChrBatchIsSatisfCore_t;

//****************************************************************************
// uSisIntrChrCommonBatch
//****************************************************************************

/**
 * Effectively namespace for shared chr-chr interaction batch utilities
 */
template <typename t_SisGlue>
class uSisIntrChrCommonBatch {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Filter utilities
     */
    typedef uSisIntrChrFilterKinIntra<t_SisGlue> filt_kin_intra_t;
    typedef uSisIntrChrFilterKinInter<t_SisGlue> filt_kin_inter_t;
    typedef uSisIntrChrFilterKoIntra<t_SisGlue> filt_ko_intra_t;
    typedef uSisIntrChrFilterKoInter<t_SisGlue> filt_ko_inter_t;

    /**
     * Utility to clear all chr-chr interaction tables
     */
    static void clear(uSisIntrChrLociTables_t& loci_tables) {
        loci_tables.loci_kin_intra.clear();
        loci_tables.loci_kin_inter.clear();
        loci_tables.loci_ko_intra.clear();
        loci_tables.loci_ko_inter.clear();
    }

    /**
     * Determines if seeded node can satisfy associated chromatin-chromatin
     *  interaction constraints (only *INTER*-chromosomal associations are
     *  tested, therefore this is only necessary for multi-locus growth).
     * @param policy - Callback policy for seed failure
     * @param p_policy_args - Pointer to additional policy arguments
     * @param loci_tables - Active loci interactions tables
     * @param nfo - Common seed information
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrChrLociTables_t& loci_tables,
                              const uSisUtils::seed_info_t& nfo,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        U_STATIC_ASSERT(U_SIS_SEED_CANDIDATE_ID == U_TO_UINT(0));
        // This routine should never be called for single locus growth
        uAssert(sample_t::is_mloc_growth);
        if (sample_t::is_mloc_growth) {
            // @HACK - Alias legal candidates buffer to allow usage of
            //  mark_viol_() routines
            uBoolVecCol& legal_candidates =
                sim.get_legal_candidates(U_THREAD_ID_0_ARG);
            uAssert(legal_candidates.n_elem >= U_TO_MAT_SZ_T(1));
            uBoolVecCol legal_candidates_ALIAS(
                legal_candidates.memptr(),
                U_TO_MAT_SZ_T(1), /*number_of_elements*/
                false,            /*copy_aux_mem*/
                true);            /*strict*/
            uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                          U_TO_UINT(0),
                          U_TO_UINT(legal_candidates_ALIAS.n_elem));
            legal_candidates_ALIAS.at(U_SIS_SEED_CANDIDATE_ID) = uTRUE;
            // @HACK - Alias seed position vector as row-major matrix to allow
            //  usage of mark_viol_() routines
            uAssert(nfo.p_node_center);
            uAssert(nfo.p_node_center->n_elem == uDim_num);
            const uMatrix candidate_centers_ALIAS(
                const_cast<uReal*>(nfo.p_node_center->memptr()),
                U_TO_MAT_SZ_T(1), /*n_rows*/
                uDim_num,         /*n_cols*/
                false,            /*copy_aux_mem*/
                true);            /*strict*/
            uAssert(candidate_centers_ALIAS.at(U_TO_MAT_SZ_T(0), uDim_X) ==
                    nfo.p_node_center->at(uDim_X));
            uAssert(candidate_centers_ALIAS.at(U_TO_MAT_SZ_T(0), uDim_Y) ==
                    nfo.p_node_center->at(uDim_Y));
            uAssert(candidate_centers_ALIAS.at(U_TO_MAT_SZ_T(0), uDim_Z) ==
                    nfo.p_node_center->at(uDim_Z));
            // Initialize core arguments
            uSisIntrChrBatchMarkViolCore_t mvc /* = {0}*/;
            mvc.p_candidate_centers = &candidate_centers_ALIAS;
            mvc.candidate_radius = nfo.node_radius;
            mvc.candidate_nid = nfo.nid;
            uAssertBounds(
                mvc.candidate_nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
            mvc.candidate_lid = nfo.lid;
            uAssert(mvc.candidate_lid == sim.get_growth_lid(mvc.candidate_nid));
            uAssertBounds(mvc.candidate_lid, U_TO_UINT(0), sim.get_num_loci());
            mvc.p_frags = &(sim.get_intr_chr_frags());
            // Master interaction tables
            const uUIMatrix& intr_kin = sim.get_intr_chr_kin();
            const uUIMatrix& intr_ko = sim.get_intr_chr_ko();
            // Between-loci, knock-in interactions
            mvc.p_intr_loci = &loci_tables.loci_kin_inter;
            mvc.p_intrs = &intr_kin;
            mark_viol_<filt_kin_inter_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                legal_candidates_ALIAS,
                mvc,
                sample,
                sim);
            // Check if constraints can be satisfied
            if (!legal_candidates_ALIAS.at(U_SIS_SEED_CANDIDATE_ID)) {
                // Violation encountered, early out
                return uFALSE;
            }
            // Between-loci, knock-out interactions
            mvc.p_intr_loci = &loci_tables.loci_ko_inter;
            mvc.p_intrs = &intr_ko;
            mark_viol_<filt_ko_inter_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                legal_candidates_ALIAS,
                mvc,
                sample,
                sim);
            // Return result of constraint checks
            return legal_candidates_ALIAS.at(U_SIS_SEED_CANDIDATE_ID);
        }
        // This routine should never reach here!
        uAssert(uFALSE);
        return uTRUE;
    }

    /**
     * ASSUMES sequential growth from first to last node. Determines which
     * candidate positions violate (i.e. cannot possibly satisfy)
     * chrome-to-chrome interaction constraints.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param loci_tables - Active loci interactions tables
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *  positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrChrLociTables_t& loci_tables,
                          const uMatrix& candidate_centers,
                          const uReal candidate_radius,
                          const uUInt parent_node_id,
                          const sample_t& sample,
                          const sim_t& sim) {
        // Initialize core arguments
        uSisIntrChrBatchMarkViolCore_t mvc /* = {0}*/;
        mvc.p_candidate_centers = &candidate_centers;
        mvc.candidate_radius = candidate_radius;
        // @WARNING - assumes candidate node id is +1 from parent node id
        mvc.candidate_nid = parent_node_id + 1;
        uAssertBounds(
            mvc.candidate_nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
        mvc.candidate_lid = sim.get_growth_lid(mvc.candidate_nid);
        uAssertBounds(mvc.candidate_lid, U_TO_UINT(0), sim.get_num_loci());
        mvc.p_frags = &(sim.get_intr_chr_frags());

        // Master interaction tables
        const uUIMatrix& intr_kin = sim.get_intr_chr_kin();
        const uUIMatrix& intr_ko = sim.get_intr_chr_ko();

        // Within-locus, knock-in interactions
        mvc.p_intr_loci = &loci_tables.loci_kin_intra;
        mvc.p_intrs = &intr_kin;
        mark_viol_<filt_kin_intra_t, policy_t, policy_args_t>(
            policy,
            p_policy_args,
            out_legal_candidates_primed,
            mvc,
            sample,
            sim);

        // Within-locus, knock-out interactions
        mvc.p_intr_loci = &loci_tables.loci_ko_intra;
        mvc.p_intrs = &intr_ko;
        mark_viol_<filt_ko_intra_t, policy_t, policy_args_t>(
            policy,
            p_policy_args,
            out_legal_candidates_primed,
            mvc,
            sample,
            sim);

        // Inter-loci interactions, only tested if multi-loci growth enabled
        if (sample_t::is_mloc_growth) {
            // Between-loci, knock-in interactions
            mvc.p_intr_loci = &loci_tables.loci_kin_inter;
            mvc.p_intrs = &intr_kin;
            mark_viol_<filt_kin_inter_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                out_legal_candidates_primed,
                mvc,
                sample,
                sim);

            // Between-loci, knock-out interactions
            mvc.p_intr_loci = &loci_tables.loci_ko_inter;
            mvc.p_intrs = &intr_ko;
            mark_viol_<filt_ko_inter_t, policy_t, policy_args_t>(
                policy,
                p_policy_args,
                out_legal_candidates_primed,
                mvc,
                sample,
                sim);
        }
    }

    /**
     * Updates loci interaction tables by removing any satisfied interactions
     * @param loci_tables - Mutable active loci interactions tables, any
     *  satisfied interactions will be removed
     * @param node_id - Unique chromatin node identifier
     * @param B - 3D coordinates of node centroid
     * @param radius - Radius of the node to add
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation of parameter sample
     */
    static void remove_satisf(uSisIntrChrLociTables_t& loci_tables,
                              const uUInt node_id,
                              const uReal* B,
                              const uReal radius,
                              const uUInt candidate_id,
                              const sample_t& sample,
                              const sim_t& sim) {
        // Initialize core arguments
        uSisIntrChrBatchIsSatisfCore_t isc /* = {0}*/;
        isc.nid = node_id;
        uAssertBounds(isc.nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
        isc.lid = sim.get_growth_lid(node_id);
        uAssertBounds(isc.lid, U_TO_UINT(0), sim.get_num_loci());
        isc.radius = radius;
        isc.B = B;
        isc.p_frags = &(sim.get_intr_chr_frags());

        // Master interaction tables
        const uUIMatrix& intr_kin = sim.get_intr_chr_kin();
        const uUIMatrix& intr_ko = sim.get_intr_chr_ko();

        // Within-locus, knock-in interactions
        isc.p_intrs = &intr_kin;
        remove_satisf_<filt_kin_intra_t, false /*is_mirrored*/>(
            loci_tables.loci_kin_intra, isc, sample, sim);

        // Within-locus, knock-out interactions
        isc.p_intrs = &intr_ko;
        remove_satisf_<filt_ko_intra_t, false /*is_mirrored*/>(
            loci_tables.loci_ko_intra, isc, sample, sim);

        // Inter-loci interactions, only tested if multi-loci growth enabled
        if (sample_t::is_mloc_growth) {
            // Between-loci, knock-in interactions
            isc.p_intrs = &intr_kin;
            remove_satisf_<filt_kin_inter_t, true /*is_mirrored*/>(
                loci_tables.loci_kin_inter, isc, sample, sim);

            // Between-loci, knock-out interactions
            isc.p_intrs = &intr_ko;
            remove_satisf_<filt_ko_inter_t, true /*is_mirrored*/>(
                loci_tables.loci_ko_inter, isc, sample, sim);
        }
    }

private:
    /**
     * Determines if any candidates violate chr-chr interaction constraints.
     *  The template argument 'filt_t' provides a stateless interface for
     *  checking if a single interaction has been violated.
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param mvc - Core arguments for batch marking constraint violations
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_t, typename policy_t, typename policy_args_t>
    static void mark_viol_(policy_t& policy,
                           policy_args_t* const p_policy_args,
                           uBoolVecCol& out_legal_candidates_primed,
                           const uSisIntrChrBatchMarkViolCore_t& mvc,
                           const sample_t& sample,
                           const sim_t& sim) {
        // Verify bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssert(mvc.p_candidate_centers);
        uAssertBounds(mvc.candidate_nid, U_TO_UINT(0), max_nodes);
        uAssertBounds(mvc.candidate_lid, U_TO_UINT(0), sim.get_num_loci());

        // Use references
        const uSisIntrLociTable_t& intr_loci = *mvc.p_intr_loci;
        const uUIMatrix& intrs = *mvc.p_intrs;
        const uUIMatrix& frags = *mvc.p_frags;

        // Early out if interaction table is empty
        if (intr_loci.empty()) {
            return;
        }

        // Verify matrix sizes if interactions present
        uAssertPosEq(intrs.n_rows, uIntrLibPairIxNum);
        uAssertPosEq(intrs.n_rows, frags.n_rows);

        // Early out if no interactions involving this locus
        uAssertPosEq(U_TO_UINT(intr_loci.size()), sim.get_num_loci());
        const uSisIntrIndexVec_t& intr_locus = intr_loci[mvc.candidate_lid];
        if (intr_locus.empty()) {
            return;
        }

        // Initialize core arguments to filter
        uSisIntrChrFilterCore_t fc /* = {0}*/;
        fc.node_nid = mvc.candidate_nid;
        fc.node_lid = mvc.candidate_lid;
        fc.node_radius = mvc.candidate_radius;

        // Apply interaction filter to candidates
        const size_t intr_locus_num = intr_locus.size();
        for (size_t i = 0; i < intr_locus_num; ++i) {
            const uUInt intr_col = intr_locus[i];
            fc.intr_id = intr_col;
            get_frag_ranges_(fc, intr_col, intrs, frags, max_nodes);
            uAssert(check_locus_involved_(fc, sim));
            filt_t::mark_viol(policy,
                              p_policy_args,
                              out_legal_candidates_primed,
                              fc,
                              *(mvc.p_candidate_centers),
                              sample,
                              sim);
        }
    }

    /**
     * Determines if any chr-chr interaction constraints have been satisfied
     *  and updates loci interaction lists so that they are no longer checked
     *  during the growth phase. The template argument 'filt_t' provides a
     *  stateless interface for checking if a single interaction has been
     *  satisfied. If the template argument 'is_mirrored' == true, then we
     *  must also update the mirrored interaction list of the alternate locus
     *  for any satisfied constraints.
     * @param intr_loci - Output table of interaction constraint column
     *  identifiers (partitioned by loci); satisfied constraints are removed
     * @param isc - Core arguments for batch checking constraint satisfaction
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    template <typename filt_t, bool is_mirrored>
    static void remove_satisf_(uSisIntrLociTable_t& intr_loci,
                               const uSisIntrChrBatchIsSatisfCore_t& isc,
                               const sample_t& sample,
                               const sim_t& sim) {
        // Verify bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBounds(isc.nid, U_TO_UINT(0), max_nodes);
        uAssertBounds(isc.lid, U_TO_UINT(0), sim.get_num_loci());

        // Early out if interaction table is empty
        if (intr_loci.empty()) {
            return;
        }

        // Use references
        const uUIMatrix& intrs = *isc.p_intrs;
        const uUIMatrix& frags = *isc.p_frags;

        // Verify matrix sizes if interactions present
        uAssertPosEq(intrs.n_rows, uIntrLibPairIxNum);
        uAssertPosEq(intrs.n_rows, frags.n_rows);

        // Early out if no interactions involving this locus
        uAssertPosEq(U_TO_UINT(intr_loci.size()), sim.get_num_loci());
        uSisIntrIndexVec_t& intr_locus = intr_loci[isc.lid];
        if (intr_locus.empty()) {
            return;
        }

        // Initialize core arguments to filter
        uSisIntrChrFilterCore_t fc /* = {0}*/;
        fc.node_nid = isc.nid;
        fc.node_lid = isc.lid;
        fc.node_radius = isc.radius;

        // Update satisfied interactions
        const int intr_locus_num = static_cast<int>(intr_locus.size());
        for (int i = intr_locus_num - 1; i >= 0; --i) {
            const uUInt intr_col = intr_locus[i];
            fc.intr_id = intr_col;
            get_frag_ranges_(fc, intr_col, intrs, frags, max_nodes);
            uAssert(check_locus_involved_(fc, sim));
            if (filt_t::is_satisf(fc, isc.B, sample, sim)) {
                // Remove satisfied constraint from further testing
                std::swap(intr_locus[i], intr_locus.back());
                uAssert(intr_locus.back() == intr_col);
                intr_locus.pop_back();
                // Check if interaction is mirrored at two different loci
                if (is_mirrored) {
                    // Remove mirrored interaction in alt locus's constraints
                    remove_mirror_(intr_loci, intr_col, fc, sim);
                }
            }
        }
    }

    /**
     * Obtain node spans for interacting fragment regions, fragments are
     *  assumed disjoint
     * @param fc - Core arguments to filter, fragment spans are written to
     *  the corresponding data members
     * @param intr_col - Interaction column index within 'intrs' matrix
     * @param intrs - 2 x N matrix where N is number of interaction pairs,
     *  each column is interaction pair, each row is column index into
     *  fragments (frags) matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param max_nodes - Maximum node identifier, used for bounds checking
     *  only if asserts are enabled
     */
    inline static void get_frag_ranges_(uSisIntrChrFilterCore_t& fc,
                                        const uUInt intr_col,
                                        const uUIMatrix& intrs,
                                        const uUIMatrix& frags,
                                        const uUInt max_nodes) {
        uAssertBounds(intr_col, U_TO_UINT(0), intrs.n_cols);
        const uUInt frag_a = intrs.at(uIntrLibPairIxMin, intr_col);
        uAssertBounds(frag_a, U_TO_UINT(0), frags.n_cols);
        const uUInt frag_b = intrs.at(uIntrLibPairIxMax, intr_col);
        uAssertBounds(frag_b, U_TO_UINT(0), frags.n_cols);
        fc.frag_a_nid_lo = frags.at(uIntrLibPairIxMin, frag_a);
        uAssertBounds(fc.frag_a_nid_lo, U_TO_UINT(0), max_nodes);
        fc.frag_a_nid_hi = frags.at(uIntrLibPairIxMax, frag_a);
        uAssertBounds(fc.frag_a_nid_hi, U_TO_UINT(0), max_nodes);
        fc.frag_b_nid_lo = frags.at(uIntrLibPairIxMin, frag_b);
        uAssertBounds(fc.frag_b_nid_lo, U_TO_UINT(0), max_nodes);
        fc.frag_b_nid_hi = frags.at(uIntrLibPairIxMax, frag_b);
        uAssertBounds(fc.frag_b_nid_hi, U_TO_UINT(0), max_nodes);
        uAssert(fc.frag_a_nid_lo <= fc.frag_a_nid_hi);
        uAssert(fc.frag_b_nid_lo <= fc.frag_b_nid_hi);
        uAssert(fc.frag_a_nid_hi < fc.frag_b_nid_lo);
    }

    /**
     * Utility to help verify interactions were properly partitioned by loci
     * @param fc - Core filter arguments
     * @param sim - Parent simulation
     * @return true if at least one node identifier involves parameter locus,
     *  false o/w
     */
    static bool check_locus_involved_(const uSisIntrChrFilterCore_t& fc,
                                      const sim_t& sim) {
        const uUInt nid_a = fc.frag_a_nid_lo;
        const uUInt nid_b = fc.frag_b_nid_lo;
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        const uUInt max_loci = sim.get_num_loci();
        uAssertBounds(fc.node_lid, U_TO_UINT(0), max_loci);
        uAssertBounds(nid_a, U_TO_UINT(0), max_nodes);
        uAssertBounds(nid_b, U_TO_UINT(0), max_nodes);
        const uUInt lid_a = sim.get_growth_lid(nid_a);
        uAssertBounds(lid_a, U_TO_UINT(0), max_loci);
        const uUInt lid_b = sim.get_growth_lid(nid_b);
        uAssertBounds(lid_b, U_TO_UINT(0), max_loci);
        return (fc.node_lid == lid_a) || (fc.node_lid == lid_b);
    }

    /**
     * For between-loci chr-chr interactions (i.e. inter-loci), the chr-chr
     *  interaction identifiers are mirrored at both loci. Therefore, if we
     *  detect that an interaction constraint has been satisfied through
     *  growth at one of the loci, then we must also remove the satisfied
     *  constraint from the constraint list of the alternate locus.
     * @param intr_loci - Table of interaction constraint column identifiers
     *  partitioned by loci
     * @param intr_col - Interaction identifier to be searched for in
     *  alternate locus's constraint set
     * @param fc - Core filter arguments, needed for details such as
     *  fragment spans and loci involved.
     * @param sim - Parent simulation
     */
    inline static void remove_mirror_(uSisIntrLociTable_t& intr_loci,
                                      const uUInt intr_col,
                                      const uSisIntrChrFilterCore_t& fc,
                                      const sim_t& sim) {
        // Determine alternate locus identifier
        const uUInt lids[2] = {sim.get_growth_lid(fc.frag_a_nid_lo),
                               sim.get_growth_lid(fc.frag_b_nid_lo)};
        uAssert(lids[0] != lids[1]);
        uAssertBounds(lids[0], U_TO_UINT(0), sim.get_num_loci());
        uAssertBounds(lids[1], U_TO_UINT(0), sim.get_num_loci());
        const uBool i_lid_alt = (lids[1] != fc.node_lid);
        uAssertBounds(
            i_lid_alt, U_TO_UINT(0), U_TO_UINT((sizeof(lids) / sizeof(uUInt))));
        const uUInt lid_alt = lids[i_lid_alt];
        uAssert(lid_alt != fc.node_lid);
        uAssertBounds(lid_alt, U_TO_UINT(0), U_TO_UINT(intr_loci.size()));
        // Remove interaction from alternate locus
        uSisIntrIndexVec_t& intr_locus_alt = intr_loci[lid_alt];
        const size_t intr_locus_alt_num = intr_locus_alt.size();
        for (size_t i = 0; i < intr_locus_alt_num; ++i) {
            const uUInt intr_col_alt = intr_locus_alt[i];
            if (intr_col_alt == intr_col) {
                // Mirrored interaction found, remove it and exit
                std::swap(intr_locus_alt[i], intr_locus_alt.back());
                uAssert(intr_locus_alt.back() == intr_col);
                intr_locus_alt.pop_back();
                return;
            }
        }
        // If we reach here, then mirrored inter-loci interaction was not
        // found and something is wrong!
        uAssert(false);
    }

    // Disallow any form of instantiation
    uSisIntrChrCommonBatch(const uSisIntrChrCommonBatch&);
    uSisIntrChrCommonBatch& operator=(const uSisIntrChrCommonBatch&);
};

#endif  // uSisIntrChrCommonBatch_h
