//****************************************************************************
// uBitset.h
//****************************************************************************

/**
 * Our 'hard' bit set type - a bit set class that will allocate and manage its
 * underlying bit buffer.
 *
 * This class complements the 'soft' bit set handle which does not manage its
 * underlying bit buffer.
 */

#ifndef uBitset_h
#define uBitset_h

#include "uBuild.h"
#include "uBitsetCompileTimeProperties.h"

#include <boost/dynamic_bitset.hpp>

// Note - changing the block type here will also change the block type for the
// pooled bit set array and its handle.
typedef boost::dynamic_bitset<
    uBitsetCompileTimeProperties::block_type /*Block*, Allocator*/>
    uBitset;

#endif  // uBitset_h
