//****************************************************************************
// uGlobals.cpp
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uRand.h"
#include "uThread.h"

/**
 * Namespace to house global data
 */
namespace uG {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * uG::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
U_DECLARE_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: uG:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
U_DECLARE_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
U_DECLARE_TLS_DATA(uRand, rng);

/**
 * Initialize global data from runtime configuration
 */
void init(uSpConstConfig_t cfg) {
#ifdef U_BUILD_ENABLE_THREADS
    // Initialize thread count
    U_NUM_WORKER_THREADS_VAR_NAME = U_DEFAULT_NUM_THREADS;
    cfg->read_into(U_NUM_WORKER_THREADS_VAR_NAME, uOpt_num_worker_threads);
    uAssert(U_NUM_WORKER_THREADS_VAR_NAME > 0);
    U_NUM_WORKER_THREADS_VAR_NAME =
        (U_NUM_WORKER_THREADS_VAR_NAME < 1) ? 1 : U_NUM_WORKER_THREADS_VAR_NAME;

    // Initialize parallel mapper
    U_PARALLEL_MAPPER_VAR_NAME.init(U_NUM_WORKER_THREADS_VAR_NAME);
#endif  // U_BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    U_INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    U_TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * Initializes global data to default values
 */
void default_init() {
#ifdef U_BUILD_ENABLE_THREADS
    // Initialize thread count
    U_NUM_WORKER_THREADS_VAR_NAME = U_DEFAULT_NUM_THREADS;

    // Initialize parallel mapper
    U_PARALLEL_MAPPER_VAR_NAME.init(U_NUM_WORKER_THREADS_VAR_NAME);
#endif  // U_BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    U_INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    U_TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
void teardown() {
#ifdef U_BUILD_ENABLE_THREADS
    U_PARALLEL_MAPPER_VAR_NAME.teardown();
#endif  // U_BUILD_ENABLE_THREADS
}

}  // namespace uG
