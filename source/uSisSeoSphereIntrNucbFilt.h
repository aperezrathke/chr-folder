//****************************************************************************
// uSisSeoSphereIntrNucbFilt.h
//****************************************************************************

/**
 * @brief Spherical nuclear body interaction filters
 */

#ifndef uSisSeoSphereIntrNucbFilt_h
#define uSisSeoSphereIntrNucbFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uDistUtils.h"
#include "uSisIntrNucbCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Filters
//****************************************************************************

/**
 * Utilities for spherical nuclear body knock-in interactions
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrNucbKinFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrNucbCommonFilt<glue_t> intr_nucb_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter nuclear
     * body interaction constraint under the assumptions of single-end,
     * ordered (SEO) growth and a spherical nuclear body.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrNucbFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // Determine squared distance threshold
        const uReal dist2_check = calc_dist2_check(c, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Buffer for computing squared distance
        uReal dist2[uDim_num];
        // Compute squared distance between node and body centers
        const uReal* const B = node_center.memptr();
        U_DIST2(dist2, c.p_body_center, B);
        uAssertRealGte(dist2[uDim_dist], U_TO_REAL(0.0));
        // Test if interaction can be satisfied
        uBool b_okay = dist2[uDim_dist] <= dist2_check;
        if (!b_okay) {
            // Let policy have final say
            b_okay = policy.intr_nucb_kin_on_fail(
                p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter nuclear
     * body interaction constraint under the assumptions of single-end,
     * ordered (SEO) growth and a spherical nuclear body.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrNucbFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // Determine squared distance threshold
        const uReal dist2_check = calc_dist2_check(c, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Buffer for storing contiguous x,y,z candidate position
        uReal B[uDim_num];
        // Buffer for computing squared distance
        uReal dist2[uDim_num];
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }
            // Load candidate center, compute squared distance to body
            B[uDim_X] = candidate_centers.at(i, uDim_X);
            B[uDim_Y] = candidate_centers.at(i, uDim_Y);
            B[uDim_Z] = candidate_centers.at(i, uDim_Z);
            U_DIST2(dist2, c.p_body_center, B);
            uAssertRealGte(dist2[uDim_dist], U_TO_REAL(0.0));
            // Test if interaction can be satisfied
            uBool b_okay = dist2[uDim_dist] <= dist2_check;
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_nucb_kin_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }  // End iteration over candidates
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-in nuclear body interaction
     * constraint under assumptions of single-end, ordered (SEO) growth and
     * spherical nuclear body
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrNucbFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        if (!intr_nucb_common_filt_t::is_in_frag(
                c.node_nid, c.frag_nid_lo, c.frag_nid_hi)) {
            return uFALSE;
        }
        // Buffer for computing squared distance
        uReal dist2[uDim_num];
        // Compute distance to nuclear body center
        U_DIST2(dist2, c.p_body_center, B);
        uAssertRealGte(dist2[uDim_dist], U_TO_REAL(0.0));
        // Compile time branch on radius type
        if (sample_t::is_homg_radius) {
            // CASE: HOMOGENEOUS RADIUS
            // Squared distance threshold already computed
            return dist2[uDim_dist] <= sim.get_intr_nucb_kin_dist2(c.intr_id);
        }
        // CASE: HETEROGENEOUS RADIUS
        // Pad squared distance threshold by node radius
        uReal dist2_check =
            sim.get_intr_nucb_kin_dist(c.intr_id) + c.node_radius;
        dist2_check *= dist2_check;
        return dist2[uDim_dist] <= dist2_check;
    }

private:
    /**
     * Computes squared distance threshold
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return Squared distance threshold
     */
    static inline uReal calc_dist2_check(const uSisIntrNucbFilterCore_t& c,
                                         const sample_t& sample,
                                         const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of last bead of constrained fragment (where
        // tested bead index <= last bead of constrained fragment index)
        uAssert(c.node_nid <= c.frag_nid_hi);
        const uReal fc2c =
            sample.get_max_c2c_len(c.node_nid, c.frag_nid_hi, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        uAssert(sample.get_node_radius(c.frag_nid_hi, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance; for heterogeneous case, this
        // value must be padded by radius of last node of interacting fragment
        const uReal ki_dist =
            (sample_t::is_homg_radius)
                ? sim.get_intr_nucb_kin_dist(c.intr_id)
                : (sim.get_intr_nucb_kin_dist(c.intr_id) +
                   sample.get_node_radius(c.frag_nid_hi, sim));
        // Compute squared distance threshold
        uReal dist2_check = ki_dist + fc2c;
        uAssert(dist2_check >= U_TO_REAL(0.0));
        dist2_check *= dist2_check;
        return dist2_check;
    }

    // Disallow any form of instantiation
    uSisSeoSphereIntrNucbKinFilt(const uSisSeoSphereIntrNucbKinFilt&);
    uSisSeoSphereIntrNucbKinFilt& operator=(
        const uSisSeoSphereIntrNucbKinFilt&);
};

/**
 * Utilities for spherical nuclear body knock-out interactions
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrNucbKoFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrNucbCommonFilt<glue_t> intr_nucb_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter nuclear
     * body interaction constraint under the assumptions of single-end,
     * ordered (SEO) growth and a spherical nuclear body.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrNucbFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // Determine distance check
        uReal dist2_check;
        uAssert(c.node_nid <= c.frag_nid_lo);
        uBool b_okay =
            calc_dist2_check(dist2_check, c, c.frag_nid_lo, sample, sim);
        if (!b_okay) {
            // CASE: DISTANCE CHECK NEEDED
            uAssert(dist2_check >= U_TO_REAL(0.0));
            // Buffer for computing squared distance
            uReal dist2[uDim_num];
            // Compute squared distance between node and body centers
            const uReal* const B = node_center.memptr();
            U_DIST2(dist2, c.p_body_center, B);
            uAssertRealGte(dist2[uDim_dist], U_TO_REAL(0.0));
            // Test if interaction can be satisfied
            b_okay = dist2[uDim_dist] > dist2_check;
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_nucb_ko_on_fail(
                    p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
            }
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter nuclear
     * body interaction constraint under the assumptions of single-end,
     * ordered (SEO) growth and a spherical nuclear body.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrNucbFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // Determine first "active" bead index of interacting fragment
        const uUInt active_nid = std::max(c.node_nid, c.frag_nid_lo);
        uAssert(active_nid <= c.frag_nid_hi);
        // Determine distance check
        uReal dist2_check;
        uBool b_okay =
            calc_dist2_check(dist2_check, c, active_nid, sample, sim);
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        if (b_okay) {
            // CASE: TRIVIAL TEST PASSED, NO DISTANCE CHECK NEEDED
            for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
                // Skip if a prior constraint isn't satisfied
                if (!out_legal_candidates_primed.at(i)) {
                    continue;
                }
                // Mark okay
                out_legal_candidates_primed.at(i) = uTRUE;
            }  // End iteration over candidates
        } else {
            // CASE: TRIVIAL TEST FAILED, DISTANCE CHECK NEEDED
            uAssert(dist2_check >= U_TO_REAL(0.0));
            // Buffer for storing contiguous x,y,z candidate position
            uReal B[uDim_num];
            // Buffer for computing squared distance
            uReal dist2[uDim_num];
            for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
                // Skip if a prior constraint isn't satisfied
                if (!out_legal_candidates_primed.at(i)) {
                    continue;
                }
                // Load candidate center, compute squared distance to body
                B[uDim_X] = candidate_centers.at(i, uDim_X);
                B[uDim_Y] = candidate_centers.at(i, uDim_Y);
                B[uDim_Z] = candidate_centers.at(i, uDim_Z);
                U_DIST2(dist2, c.p_body_center, B);
                uAssertRealGte(dist2[uDim_dist], U_TO_REAL(0.0));
                // Test if interaction can be satisfied
                uBool b_okay = dist2[uDim_dist] > dist2_check;
                if (!b_okay) {
                    // Let policy have final say
                    b_okay = policy.intr_nucb_ko_on_fail(
                        p_policy_args, i, c, sample, sim);
                }
                out_legal_candidates_primed.at(i) = b_okay;
            }  // End iteration over candidates
        }
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-out nuclear body interaction
     * constraint under assumptions of single-end, ordered (SEO) growth and
     * spherical nuclear body
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrNucbFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_nucb_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        uAssert(c.node_nid <= c.frag_nid_hi);
        if (c.node_nid != c.frag_nid_hi) {
            // Knock-out interaction cannot be satisfied until entire fragment
            // has been grown
            return uFALSE;
        }
        // @TODO - verify that if mark_viol(...) check was okay, then no more
        //  work is needed (assert on this!)
        return uTRUE;
    }

private:
    /**
     * Computes squared distance threshold
     * @param dist2_check - Output squared distance threshold
     * @param c - Core filter arguments
     * @param active_nid - max(current nid, frag_nid_lo)
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return uTRUE if knock-out test is satisfied without computing distance
     *  checks from node to body (occurs when fc2c > ko_dist_pad, where fc2c
     *  is max center-to-center length from test node to first active node of
     *  constrained fragment, and ko_dist_pad is knock out distance plus
     *  active node radius), uFALSE o/w
     * @WARNING - if return is uTRUE, dist2_check is not valid and must be
     *  ignored!
     */
    static inline uBool calc_dist2_check(uReal& dist2_check,
                                         const uSisIntrNucbFilterCore_t& c,
                                         const uUInt active_nid,
                                         const sample_t& sample,
                                         const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of first "active" bead of the constrained
        // fragment, where first "active" bead is:
        //  - if candidate node_nid < fragment nid_lo -> fragment nid_lo
        //  - else -> candidate node_nid, as candidate is assumed to be
        //      within the interacting fragment
        uAssert(c.node_nid <= active_nid);
        uAssertBoundsInc(active_nid, c.frag_nid_lo, c.frag_nid_hi);
        const uReal fc2c = sample.get_max_c2c_len(c.node_nid, active_nid, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        // If tested node is "active", then fc2c should be 0!
        uAssert((c.node_nid != active_nid) || (fc2c == U_TO_REAL(0.0)));
        uAssert(sample.get_node_radius(active_nid, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance; for heterogeneous case, this
        // value must be padded by radius of active node
        const uReal ko_dist = (sample_t::is_homg_radius)
                                  ? sim.get_intr_nucb_ko_dist(c.intr_id)
                                  : (sim.get_intr_nucb_ko_dist(c.intr_id) +
                                     sample.get_node_radius(active_nid, sim));
        uAssert(ko_dist > U_TO_REAL(0.0));
        // Early out if trivial test satisfaction
        if (ko_dist < fc2c) {
            // @WARNING, dist2_check is not valid!
            // Signal that distance checks can be avoided
            return uTRUE;
        }
        // Determine squared distance threshold
        dist2_check = ko_dist - fc2c;
        dist2_check *= dist2_check;
        // Signal that distance checks are needed
        return uFALSE;
    }

    // Disallow any form of instantiation
    uSisSeoSphereIntrNucbKoFilt(const uSisSeoSphereIntrNucbKoFilt&);
    uSisSeoSphereIntrNucbKoFilt& operator=(const uSisSeoSphereIntrNucbKoFilt&);
};

#endif  // uSisSeoSphereIntrNucbFilt_h
