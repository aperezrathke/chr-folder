//****************************************************************************
// uSisIntrFailEnergySimLevelMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * IntrFail: Interaction Failure
 *
 * Sample energy is proportional to [budgeted] interaction constraint
 *  failure fraction
 */

#ifndef uSisIntrFailEnergySimLevelMixin_h
#define uSisIntrFailEnergySimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Misc
//****************************************************************************

/**
 * Enumerated interaction failure energies
 * @WARNING - THIS MUST MATCH ORDER OF CORRESPONDING enum uOptE!
 */
enum eEnerIntrFail {
    eEnerIntrFail_chr_kin = 0,
    eEnerIntrFail_chr_ko,
    eEnerIntrFail_lam_kin,
    eEnerIntrFail_lam_ko,
    eEnerIntrFail_nucb_kin,
    eEnerIntrFail_nucb_ko,
    eEnerIntrFail_num
};

/**
 * Budget access decorator
 */
template <typename t_IntrBudgAcc, enum eEnerIntrFail eif>
class uEnerIntrFailBudgAcc : public t_IntrBudgAcc {
public :
    /**
     * Indicate budget access
     */
    enum {is_null_intr_budg = t_IntrBudgAcc::is_null_intr_budg};
    /**
     * @return Energy type enum
     */
    inline static enum eEnerIntrFail get_ener_intr_fail_type() {
        return eif;
    }
};

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisIntrFailEnergySimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisIntrFailEnergySimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisIntrFailEnergySimLevelMixin_h
