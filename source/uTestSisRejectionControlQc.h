//****************************************************************************
// uTestSisRejectionControlQc.h
//****************************************************************************

/**
 * Tests growth of a simple chromatin chain (single locus) but with rejection
 * control as the quality control (Qc) mixin.
 *
 * Usage:
 * -test_sis_rejection_control_qc
 */

#ifdef uTestSisRejectionControlQc_h
#   error "Test Sis Rejection Control Qc included multiple times!"
#endif  // uTestSisRejectionControlQc_h
#define uTestSisRejectionControlQc_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Rejection control mixin
#include "uSisDynamicEssScheduleMixin.h"
#include "uSisFixedQuantileScaleRcMixin.h"
#include "uSisHetrLagScheduleMixin.h"
#include "uSisInterpWeightsScaleRcMixin.h"
#include "uSisParallelRegrowthRcMixin.h"
#include "uSisRejectionControlQcSimLevelMixin.h"
#include "uSisSerialRegrowthRcMixin.h"

// Use null QC as a control
#include "uMockUpUtils.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"

#include <iostream>
#include <string>

// Uncomment to enable dynamic scheduling instead of fixed lag scheduling
//#define U_TEST_SIS_RC_SCHED_DYNAMIC_ESS

// Uncomment to enable quantile scaling instead of weight interpolation scaling
//#define U_TEST_SIS_RC_SCALE_FIXED_QUANTILE

namespace {
/**
 * Wire together a rejection control simulation
 */
class uTestSisRcQcGlue {
public:
    // Simulation
    typedef uSisSim<uTestSisRcQcGlue> sim_t;
    // Sample
    typedef uSisSample<uTestSisRcQcGlue> sample_t;

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisRcQcGlue,
        uSisParallelBatchGrower<uTestSisRcQcGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisRejectionControlQcSimLevelMixin<
        uTestSisRcQcGlue,
    // Set the schedule
#ifdef U_TEST_SIS_RC_SCHED_DYNAMIC_ESS
        uSisDynamicEssScheduleMixin<uTestSisRcQcGlue>,
#else
        uSisHetrLagScheduleMixin<uTestSisRcQcGlue>,
#endif  // U_TEST_SIS_RC_SCHED_DYNAMIC_ESS
        // Set the scale
#ifdef U_TEST_SIS_RC_SCALE_FIXED_QUANTILE
        uSisFixedQuantileScaleRcMixin<uTestSisRcQcGlue>,
#else
        uSisInterpWeightsScaleRcMixin<uTestSisRcQcGlue>,
#endif  // U_TEST_SIS_RC_SCALE_FIXED_QUANTILE
        uSisParallelRegrowthRcMixin<uTestSisRcQcGlue>,
        // Enable heartbeat logging
        true,
        // Disable weight normalization
        false>
        sim_level_qc_mixin_t;

    // Simulation level node radius mixin
    typedef uSisHomgNodeRadiusSimLevelMixin<uTestSisRcQcGlue>
        sim_level_node_radius_mixin_t;
    // Sample level node radius mixin
    typedef uSisHomgNodeRadiusMixin<uTestSisRcQcGlue> node_radius_mixin_t;
    // Simulation level growth mixin
    typedef uSisSeoSloc::GrowthSimLevelMixin_threaded<uTestSisRcQcGlue>
        sim_level_growth_mixin_t;
    // Sample level growth mixin
    typedef uSisSeoSloc::GrowthMixin<uTestSisRcQcGlue> growth_mixin_t;
    // Simulation level nuclear mixin
    typedef uSisSphereNuclearSimLevelMixin_threaded<uTestSisRcQcGlue>
        sim_level_nuclear_mixin_t;
    // Sample level nuclear mixin
    typedef uSisSphereNuclearMixin<uTestSisRcQcGlue> nuclear_mixin_t;
    // Simulation level collision mixin
    typedef uSisHardShellCollisionSimLevelMixin_threaded<uTestSisRcQcGlue>
        sim_level_collision_mixin_t;
    // Sample level collision mixin
    typedef uSisHardShellCollisionMixin<uTestSisRcQcGlue, uBroadPhase_t>
        collision_mixin_t;
    // Simulation level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrSimLevelMixin<uTestSisRcQcGlue>
        sim_level_intr_chr_mixin_t;
    // Sample level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrMixin<uTestSisRcQcGlue> intr_chr_mixin_t;
    // Simulation level lamina interaction mixin
    typedef uSisNullIntrLamSimLevelMixin<uTestSisRcQcGlue>
        sim_level_intr_lam_mixin_t;
    // Sample level lamina interaction mixin
    typedef uSisNullIntrLamMixin<uTestSisRcQcGlue> intr_lam_mixin_t;
    // Simulation level nuclear body interaction mixin
    typedef uSisNullIntrNucbSimLevelMixin<uTestSisRcQcGlue>
        sim_level_intr_nucb_mixin_t;
    // Sample level nuclear body interaction mixin
    typedef uSisNullIntrNucbMixin<uTestSisRcQcGlue> intr_nucb_mixin_t;
    // Simulation level energy mixin
    typedef uSisUnifEnergySimLevelMixin<uTestSisRcQcGlue>
        sim_level_energy_mixin_t;
    // Sample level energy mixin
    typedef uSisUnifEnergyMixin<uTestSisRcQcGlue> energy_mixin_t;
    // Seed mixin
    typedef uSisSeoUnifCubeSeedMixin<uTestSisRcQcGlue> seed_mixin_t;
};
}  // end of anonymous namespace

/**
 * Entry point for test
 * rc = rejection control
 * qc = quality control
 */
int uTestSisRejectionControlQcMain(const uCmdOptsMap& cmd_opts) {
    // Expose rc qc simulation
    typedef uTestSisRcQcGlue::sim_t rc_sim_t;

    // Expose null qc simulation as control
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t null_sim_t;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 3;
    config->ensemble_size = 100;
    config->set_option(uOpt_max_node_diameter, U_TO_REAL(300));
    config->set_option(uOpt_nuclear_diameter, U_TO_REAL(9000));
    config->num_nodes = std::vector<uUInt>(1 /*n_chains*/, 100 /*n_nodes*/);
    config->num_unit_sphere_sample_points = 100;

    // Configure rc schedule
#ifdef U_TEST_SIS_RC_SCHED_DYNAMIC_ESS
    // Dynamic schedule
    config->set_option(uOpt_qc_schedule_type_slot0, "dynamic_ess");
    config->set_option(uOpt_qc_schedule_dynamic_ess_thresh_slot0, 0.35);
#else
    // Deterministic schedule
    config->set_option(uOpt_qc_schedule_type_slot0, "homg_lag");
    config->set_option(uOpt_qc_schedule_lag_slot0, 11);
#endif  // U_SIS_LMRK_TR_TEST_SCHED_DYNAMIC_ESS

    // Configure rc scale
#ifdef U_TEST_SIS_RC_SCALE_FIXED_QUANTILE
    config->set_option(uOpt_rc_scale_type, "fixed_quantile");
    config->set_option(uOpt_rc_scale_fixed_quantile, 0.25);
#else
    config->set_option(uOpt_rc_scale_type, "interp_weights");
    config->set_option(uOpt_rc_scale_interp_weights_c_min, 0.0);
    config->set_option(uOpt_rc_scale_interp_weights_c_mean, 0.25);
    config->set_option(uOpt_rc_scale_interp_weights_c_max, 0.75);
#endif  // U_SIS_RC_TEST_SCALE_FIXED_QUANTILE

    // Output configuration
    std::cout << "Running Test Sis Rejection Control Qc." << std::endl;
    config->print();

    // Configure number of worker threads
    U_SET_NUM_WORKER_THREADS(config, 5);

    // Initialize globals
    uG::init(config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    rc_sim_t rc_sim(config U_MAIN_THREAD_ID_ARG);

    // Do rejection control simulation!
    const uBool rc_result = rc_sim.run();

    // Compute rejection control effective sample size
    const uReal rc_ess = uSisUtilsQc::calc_ess_cv_from_log(
        rc_sim.get_completed_log_weights_view());

    std::cout << "Rc simulation completed with status: " << rc_result
              << std::endl;
    std::cout << "Rc effective sample size: " << rc_ess << std::endl;

    // Run control simulation
    null_sim_t null_sim(config U_MAIN_THREAD_ID_ARG);

    // Do control simulation!
    const uBool null_result = null_sim.run();

    // Compute null effective sample size
    const uReal null_ess = uSisUtilsQc::calc_ess_cv_from_log(
        null_sim.get_completed_log_weights_view());

    std::cout << "Null simulation completed with status: " << null_result
              << std::endl;
    std::cout << "Null effective sample size: " << null_ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
