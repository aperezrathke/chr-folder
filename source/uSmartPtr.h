//****************************************************************************
// uSmartPtr.h
//****************************************************************************

/**
 * @brief Selects between C++11 and boost smart pointer implementations
 */

#ifndef uSmartPtr_h
#define uSmartPtr_h

#include "uBuild.h"

// Check if C++11 is enabled
#ifdef U_BUILD_CXX_11
#   include <memory>
#   define uSmartPtr std
#else
// Else, defer to boost implementation
#   include <boost/enable_shared_from_this.hpp>
#   include <boost/make_shared.hpp>
#   include <boost/shared_ptr.hpp>
#   include <boost/weak_ptr.hpp>
#   define uSmartPtr boost
#endif

#endif  // uSmartPtr_h
