//****************************************************************************
// uChromatinExportLigc.h
//****************************************************************************

/**
 * Utility for writing ligation contact tuples
 */

#ifndef uChromatinExportLigc_h
#define uChromatinExportLigc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uBroadPhase.h"
#include "uExportFlags.h"
#include "uLigcCore.h"
#include "uTypes.h"

#include <string>
#include <vector>

//****************************************************************************
// Ligc export
//****************************************************************************

/**
 * Effectively a namespace for ligc chromatin export utilities
 */
class uChromatinExportLigc {
public:
    /**
     * Information needed to batch export ligations
     */
    typedef struct {
        /**
         * Broad phase: The length of a uniform grid cell
         */
        uReal m_broad_cell_length;
        /**
         * Broad phase: The max positive grid coordinate
         */
        uReal m_broad_radius;
        /**
         * Euclidean distance ligation threshold
         */
        uReal m_ki_dist;
        /**
         * Log weight at each sample
         */
        const uVecCol* mp_log_weights;
        /**
         * Handle to radius at each monomer node
         */
        const uVecCol* mp_node_radii;
        /**
         * Handles to node positions for all samples
         */
        std::vector<const uMatrix*> m_node_positions;
        /**
         * Handles to broad phases for all samples
         */
        std::vector<const uBroadPhase_t*> m_broad_phases;
        /**
         * Export configuration bits
         */
        uUInt m_export;
        /**
         * Base output directory
         */
        std::string m_output_dir;
        /**
         * Job identifier
         */
        std::string m_job_id;
#ifdef U_BUILD_ENABLE_THREADS
        /**
         * The amount of work each thread grabs at a time
         */
        uUInt m_chunk_size;
#endif  // U_BUILD_ENABLE_THREADS
    } info_t;

    /**
     * @return uTRUE if ligc export indicated, uFALSE o/w
     */
    static uBool should_export(const uUInt export_flags);

    /**
     * Batch export ligc tuples for all samples from simulation if indicated
     * @WARNING - CAN ONLY BE CALLED FROM MAIN THREAD!
     * @param sim - The simulation to export
     * @param flags - Export flags
     */
    template <typename sim_t>
    static void export_ligc(const sim_t& sim, const uUInt export_flags) {
        // Check if export is indicated
        if (!should_export(export_flags)) {
            return;
        }

        // Collect export information directly from simulation and/or
        // configuration
        info_t info;
        info.m_broad_cell_length = sim.get_collision_grid_cell_length();
        info.m_broad_radius = sim.get_collision_bounding_radius();
        info.m_ki_dist = uLigcCore::get_dist(sim.get_config());
        info.mp_log_weights = &(sim.get_completed_log_weights_view());
        info.mp_node_radii = &(sim.get_node_radii());
        // Condition needed to initialize internal broad phase mask buffer
        uAssertPosEq(U_TO_UINT(sim.get_node_radii().n_elem),
                     sim.get_max_total_num_nodes());
        info.m_export = export_flags;
        info.m_output_dir = sim.get_config()->output_dir;
        info.m_job_id = sim.get_config()->job_id;
#ifdef U_BUILD_ENABLE_THREADS
        info.m_chunk_size = sim.get_grow_chunk_size();
#endif  // U_BUILD_ENABLE_THREADS

        // Collect geometry from each sample
        typedef typename sim_t::sample_t sample_t;
        const std::vector<sample_t>& samples = sim.get_completed_samples();
        const uUInt n_samples = U_TO_UINT(samples.size());
        info.m_node_positions.resize(n_samples, NULL);
        info.m_broad_phases.resize(n_samples, NULL);
        for (uUInt i = 0; i < n_samples; ++i) {
            const sample_t& sample = samples[i];
            info.m_node_positions[i] = &(sample.get_node_positions());
            info.m_broad_phases[i] = &(sample.get_collision_broad_phase());
        }

        // Batch export ligc tuples
        export_ligc(info);
    }

private:
    /**
     * Batch export ligc tuples for all samples
     * @WARNING - CAN ONLY BE CALLED FROM MAIN THREAD!
     * @param info - collated information needed to export ligc data
     */
    static void export_ligc(const info_t& info);
};

#endif  // uChromatinExportLigc_h
