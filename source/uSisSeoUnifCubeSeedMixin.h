//****************************************************************************
// uSisSeoUnifCubeSeedMixin.h
//****************************************************************************

/**
 * Defines seed policy that rejection samples uniform points from within a
 *  a cubic volume.
 *
 * ASSUMES SINGLE-END, ORDERED (SEO) GROWTH!
 */

#ifndef uSisSeoUnifCubeSeedMixin_h
#define uSisSeoUnifCubeSeedMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisNullSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Seed mixin manages placement of initial polymer nodes
 * @WARNING - ASSUMED TO BE LAST SIMULATION LEVEL MIXIN INITIALIZED!
 */
template <typename t_SisGlue>
class uSisSeoUnifCubeSeedMixin : public uSisNullSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Allocate seed radii matrix
        const uMatSz_t num_loci = U_TO_MAT_SZ_T(sim.get_num_loci());
        uAssert(num_loci >= U_TO_MAT_SZ_T(1));
        m_seed_radii.set_size(uDim_num, num_loci);
        m_seed_radii.zeros();
        // Initialize from nuclear confinement
        // @TODO - Should we defer to nuclear mixin for this part?
        U_STATIC_ASSERT((uDim_X == 0) && (uDim_Y == 1) && (uDim_Z == 2));
        for (uMatSz_t lid = 0; lid < num_loci; ++lid) {
            // Bounds check locus identifier
            uAssertBounds(
                ((size_t)lid), 0, sim.get_max_num_nodes_at_locus().size());
            // Assume non-empty locus
            uAssert(sim.get_max_num_nodes_at_locus()[lid] > U_TO_UINT(0));
            // Determine node identifier of first node of this locus
            const uUInt node0_nid =
                sim.get_growth_nid(U_TO_UINT(lid), U_TO_UINT(0) /*nof*/);
            // Bounds check node identifier
            uAssertBounds(
                node0_nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
            // Determine radius of seed node at this locus
            const uReal node0_radius = sim.get_node_radius(U_TO_UINT(0));
            // Get allowed nuclear radii along each axis
            for (uMatSz_t axis = U_TO_MAT_SZ_T(uDim_X);
                 axis <= U_TO_MAT_SZ_T(uDim_Z);
                 ++axis) {
                const uReal allowed_nuc_rad = sim.get_allowed_nuclear_radius(
                    (enum uDim)axis, node0_radius, sim);
                uAssert(allowed_nuc_rad > U_TO_REAL(0.0));
                uAssertBounds(axis, U_TO_MAT_SZ_T(0), m_seed_radii.n_rows);
                uAssertBounds(lid, U_TO_MAT_SZ_T(0), m_seed_radii.n_cols);
                m_seed_radii.at(axis, lid) = allowed_nuc_rad;
            }
        }
        // Update according to lamina interactions
        sim.sim_level_intr_lam_mixin_t::update_seed_radii(
            m_seed_radii, sim, config);
        // Update according to nuclear body interactions
        sim.sim_level_intr_nucb_mixin_t::update_seed_radii(
            m_seed_radii, sim, config);
    }

    /**
     * Resets to default state
     */
    void clear() { m_seed_radii.clear(); }

    /**
     * Utility function to randomly place a chromatin node center that
     * satisfies hard constraints such as nuclear confinement, volume
     * exclusion, and/or interactions. Collision state is also updated.
     * @param - Sample to be seeded
     * @param nid - Node identifier, column index within node positions matrix
     * @param lid - Locus identifier
     * @param node_radius - Radius of seed node
     * @param clash_func - Callback determines if placed node is clash free
     * @param sim - Outer simulation
     */
    template <typename SeedClashPolicy_t, typename SeedIntrChrPolicy_t>
    void seed_core(sample_t& sample,
                   const uUInt nid,
                   const uUInt lid,
                   const uReal node_radius,
                   const SeedClashPolicy_t& clash_func,
                   const SeedIntrChrPolicy_t& intr_chr_func,
                   const sim_t& sim U_THREAD_ID_PARAM) const {

        // Bounds check node id
        uAssertBounds(nid, U_TO_UINT(0), sample.get_node_positions().n_cols);
        // Bounds check locus identifier
        uAssertBounds(lid, U_TO_UINT(0), sim.get_num_loci());
        uAssertPosEq(sim.get_num_loci(), U_TO_UINT(m_seed_radii.n_cols));
        uAssertPosEq(m_seed_radii.n_rows, U_TO_MAT_SZ_T(uDim_num));

        // Wrap column containing node position into a vector object
        uVecCol node_center(sample.get_node_positions().colptr(nid), /*aux_mem*/
                            uDim_num, /*number_of_elements*/
                            false,    /*copy_aux_mem*/
                            true);    /*strict*/

        // Get seed cube positive extents
        const uReal radius_x = m_seed_radii.at(uDim_X, lid);
        uAssert(radius_x > U_TO_REAL(0.0));
        const uReal radius_y = m_seed_radii.at(uDim_Y, lid);
        uAssert(radius_y > U_TO_REAL(0.0));
        const uReal radius_z = m_seed_radii.at(uDim_Z, lid);
        uAssert(radius_z > U_TO_REAL(0.0));

        // Initialize invariant seed information
        const uSisUtils::seed_info_t nfo = {
            &node_center, nid, lid, node_radius};

        // Loop until we find a valid point
        do {
            // Generate random point inside cube
            node_center.at(uDim_X) = uRng.unif_real(-radius_x, radius_x);
            node_center.at(uDim_Y) = uRng.unif_real(-radius_y, radius_y);
            node_center.at(uDim_Z) = uRng.unif_real(-radius_z, radius_z);
        } while (
            (!sample.nuclear_mixin_t::is_seed_okay(
                sample, nfo, sim U_THREAD_ID_ARG)) ||
            (!clash_func.is_seed_okay(nfo, sim)) ||
            (!intr_chr_func.is_seed_okay(sample, nfo, sim U_THREAD_ID_ARG)) ||
            (!sample.intr_lam_mixin_t::is_seed_okay(
                sample, nfo, sim U_THREAD_ID_ARG)) ||
            (!sample.intr_nucb_mixin_t::is_seed_okay(
                sample, nfo, sim U_THREAD_ID_ARG)));

        // Update sample mixin states with new node information
        uSisUtils::
            on_node_grown<sample_t, sim_t, SeedIntrChrPolicy_t::should_update>(
                nid,
                node_center.memptr(),
                node_radius,
                U_SIS_SEED_CANDIDATE_ID,
                sample,
                sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Matrix of size 3 rows x (number of loci) columns, each column is the
     *  (X, Y, Z) radii respectively of the bounding seed cube for that locus
     */
    uMatrix m_seed_radii;
};

#endif  // uSisSeoUnifCubeSeedMixin_h
