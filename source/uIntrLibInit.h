//****************************************************************************
// uIntrLibInit.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utility functions for initializing interaction
 *  constraints
 */

#ifndef uIntrLibInit_h
#define uIntrLibInit_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uTypes.h"

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Effectively namespace for shared interaction utilities
 */
class uIntrLibInit {
public:
    /**
     * SIMULATION LEVEL UTILITY
     * If any element of 'indices' vector would result in boundary violation,
     *  program exits.
     * @param indices - Vector of indices to bounds test, each index must be
     *   in [0,M) where M is number of columns in 'frags'' matrix
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     */
    static void check_bounds_exit_on_fail(const uUIVecCol& indices,
                                          const uUIMatrix& frags) {
        for (uMatSz_t i = 0; i < indices.n_elem; ++i) {
            const uUInt frag_col = indices.at(i);
            if (frag_col >= frags.n_cols) {
                uLogf("Error: out-of-bounds fragment index. Exiting.\n");
                exit(uExitCode_error);
            }
        }
    }

private:
    // Disallow any form of instantiation
    uIntrLibInit(const uIntrLibInit&);
    uIntrLibInit& operator=(const uIntrLibInit&);
};

#endif  // uIntrLibInit_h
