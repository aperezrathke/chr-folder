//****************************************************************************
// uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Mloc: Multiple loci
 * Budg: Interaction constraint failures allowed up to budget
 *
 * Defines a budgeted chrome-to-chrome interaction mixin supporting knock-in
 * and knock-out constraints over multiple (or single) loci
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * and/or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_h
#define uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibConfig.h"
#include "uLogf.h"
#include "uSisIntrChrBudgUtil.h"
#include "uSisIntrChrCommonBatch.h"
#include "uSisMkiMkoSeoMlocIntrChrSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin_h
