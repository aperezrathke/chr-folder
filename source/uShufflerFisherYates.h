//****************************************************************************
// uShufflerFisherYates.h
//****************************************************************************

/**
 * @brief - Generates unbiased permutations
 */

/**
 * @WARNING - THIS UTILITY WILL NOT WORK WITH 0-LENGTH VECTORS! THE CLIENT
 *  IS RESPONSIBLE FOR PASSING IN NON-EMPTY VECTORS. THERE ARE ASSERTS TO
 *  TO CATCH THIS EDGE CASE BUT ASSERTS ARE NOT ALWAYS ENABLED - SUCH AS
 *  FOR RELEASE BUILDS.
 */

/**
 * Note, template argument 't_ivec_arma' assumes an integer vector type that
 *  has interface consistent with the Armadillo matrix library:
 *
 *  http://arma.sourceforge.net/
 */

#ifndef uShufflerFisherYates_h
#define uShufflerFisherYates_h

#include "uBuild.h"
#include "uAssert.h"
#include "uRand.h"
#include "uTypes.h"

#include <algorithm>

/**
 * Utility for generating a Fisher-Yates shuffle. See:
 *   https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 *   https://eli.thegreenplace.net/2010/05/28/the-intuition-behind-fisher-yates-shuffling/
 *   https://stackoverflow.com/questions/9345087/choose-m-elements-randomly-from-a-vector-containing-n-elements
 * A Fisher-Yates shuffle generates an unbiased permutation. In other words,
 * all permutations are equally likely. This algorithm is also more efficient
 * when you only require a partial permutation of an index set.
 */
class uShufflerFisherYatesCore {
public:
    /**
     * Generates a sorted index vector from [0, n-1]. In other words, the
     * output vector will have elements in order: 0 1 ... (n-1)
     * @param out - unsigned integer vector for storing sorted index set
     * @param n - the index range to sort [0, (n-1)]
     */
    template <typename t_ivec_arma = uUIVecCol>
    inline static void get_sorted_indices(t_ivec_arma& out_sorted,
                                          const uUInt n) {
        uAssert(n > U_TO_UINT(0));
        out_sorted = uMatrixUtils::linspace<t_ivec_arma>(
            U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(n - 1), U_TO_MAT_SZ_T(n));
        uAssert(out_sorted.is_sorted());
        uAssert(out_sorted.n_elem == U_TO_MAT_SZ_T(n));
    }

    /**
     * Performs complete permutation of all elements
     * @param out_perm - output permuted index vector
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = uUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm, uRand& rng) {
        uAssert(out_perm.n_elem > U_TO_MAT_SZ_T(0));
        shuffle(out_perm,
                U_TO_UINT(0),
                U_TO_UINT(out_perm.n_elem - 1),
                U_TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of first 'm' elements. Considers all
     * elements of candidates for the permutation.
     * @param out_perm - output permuted index vector up to first m elements
     * @param m - only the first 'm' elements will be permuted where
     *  0 < m <= n == out_perm.n_elem
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = uUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const uUInt m,
                               uRand& rng) {
        uAssertBoundsInc(m, U_TO_UINT(1), U_TO_UINT(out_perm.n_elem));
        shuffle(out_perm,
                U_TO_UINT(0),
                m - U_TO_UINT(1),
                U_TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of elements in range [ix_min, ix_max] with
     * all elements in [ix_min, ix_pool] as possible candidates and the
     * following inequalities hold: ix_min <= ix_max and ix_max <= ix_pool
     * @param out_perm - output permuted index vector, only elements in range
     *  [ix_min, ix_max] are considered permuted; however, elements in
     *  [ix_max+1, ix_pool] may be modified but are not considered as part of
     *  final permutation.
     * @param ix_min - lower bound of range to permute, must be <= ix_max
     * @param ix_max - upper bound of range to permute, must be >= ix_min and
     *  <= ix_pool
     * @param ix_pool - upper bound of candidate elements considered for
     *  permuting, considered elements are those in range [ix_min, ix_pool],
     *  must be >= ix_max
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = uUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const uUInt ix_min,
                               const uUInt ix_max,
                               const uUInt ix_pool,
                               uRand& rng) {
        uAssert(out_perm.n_elem > U_TO_MAT_SZ_T(0));
        uAssertBounds(ix_min, U_TO_UINT(0), U_TO_UINT(out_perm.n_elem));
        uAssertBounds(ix_max, U_TO_UINT(0), U_TO_UINT(out_perm.n_elem));
        uAssertBounds(ix_pool, U_TO_UINT(0), U_TO_UINT(out_perm.n_elem));
        uAssert(ix_min <= ix_max);
        uAssert(ix_max <= ix_pool);
        uAssert((ix_pool - ix_min) > U_TO_UINT(0));
        for (uUInt i = ix_min; i <= ix_max; ++i) {
            const uUInt ix_swap = rng.unif_int(i, ix_pool);
            uAssertBoundsInc(ix_swap, i, ix_pool);
            std::swap(out_perm.at(U_TO_MAT_SZ_T(i)),
                      out_perm.at(U_TO_MAT_SZ_T(ix_swap)));
        }
    }
};

/**
 * Encapsulated Fisher-Yates object, maintains internal index buffers
 */
template <typename t_ivec_arma = uUIVecCol>
class uShufflerFisherYates {
public:
    /**
     * Default constructor - uninitialized!
     */
    uShufflerFisherYates() {}

    /**
     * Constructor
     * @param n - number of indices that need shuffling
     */
    explicit uShufflerFisherYates(const uUInt n) : sorted(n), perm(n) {
        uAssert(n > U_TO_UINT(0));
        uShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Initialize shuffler with 'n' elements, where n > 0
     * @param n - number of indices that may be shuffled
     */
    void init(const uUInt n) {
        uAssert(n > U_TO_UINT(0));
        sorted.set_size(n);
        perm.set_size(n);
        uShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Obtain partially shuffled index set, compute shuffle for first 'm'
     * elements only (with all elements as possible candidates)
     * @param m - first 0 < m <= n elements of index set will be shuffled
     * @param rng - random number generator
     * @return index set with first m elements shuffled
     */
    const t_ivec_arma& shuffle(const uUInt m, uRand& rng) {
        uAssert(m > U_TO_UINT(0));
        uAssert(m <= U_TO_UINT(sorted.n_elem));
        perm = sorted;
        uShufflerFisherYatesCore::shuffle(perm, m, rng);
        return this->get_view(m);
    }

    /**
     * @WARNING - DO NOT CALL THIS METHOD UNLESS YOU KNOW WHAT YOUR ARE DOING!
     * @return View of internal permutation buffer as size 'm'
     */
    inline const t_ivec_arma& get_view(const uUInt m) {
        uAssert(m > U_TO_UINT(0));
        uAssert(m <= U_TO_UINT(perm.n_elem));
        // return aliased view for vector of length 'm'
        new (&perm_view) t_ivec_arma(perm.memptr(),
                                     U_TO_MAT_SZ_T(m),
                                     false, /* copy */
                                     true); /* strict*/
        return perm_view;
    }

private:
    // Sorted index set with elements [0 to n-1]
    t_ivec_arma sorted;
    // Scratch buffer for permuting sorted index set
    t_ivec_arma perm;
    // Aliased view of permuted buffer of size m
    t_ivec_arma perm_view;
};

#endif  // uShufflerFisherYates_h
