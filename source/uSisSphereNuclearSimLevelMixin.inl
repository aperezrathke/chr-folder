//****************************************************************************
// uSisSphereNuclearSimLevelMixin.inl
//****************************************************************************

/**
 * A nuclear mixin manages the details for checking candidate nodes to see if
 * they lie within the nuclear volume; this mixing enforces a spherical
 * nuclear confinement.
 *
 * An accessory structure is the simulation level nuclear mixin which defines
 * global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 */

/**
 * Mixin encapsulating global (sample independent) data and utilities. In
 * this case, the simulation level nuclear mixin manages the pre-allocated
 * scratch buffers used in nuclear constraint checking. Does not depend on
 * node homogeneity.
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisSphereNuclearSimLevelMixin_threaded
#else
class uSisSphereNuclearSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
{
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag spherical nucleus
     */
    enum { is_sphere_nucleus = true };

    /**
     * Initializes simulation level nuclear mixin by pre-allocating scratch
     * buffers used for nuclear constraint checking.
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        // Initialize spherical volume
        uSisUtils::NucConfig::init_axis(m_nuc_diameter,
                                        m_nuc_radius,
                                        m_nuc_homg_allowed_radius,
                                        m_nuc_homg_allowed_sq_radius,
                                        uOpt_nuclear_diameter,
                                        U_TO_REAL(0.0), /*def_diam*/
                                        sim,
                                        config);
        // This assumes sphere sample points have been initialized!
        const uMatrix& unit_sphere_sample_points =
            sim.get_unit_sphere_sample_points();
        uAssert(unit_sphere_sample_points.size() > 0);
        // Assuming each row is a sample position (row-major)
        uAssert(unit_sphere_sample_points.n_cols == uDim_num);
        // Initialize TLS
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(m_nuc_schur_buffer,
                                         zeros,
                                         unit_sphere_sample_points.n_rows,
                                         unit_sphere_sample_points.n_cols);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_nuc_dot_buffer, zeros, unit_sphere_sample_points.n_rows);
        // Post-condition(s)
        uAssert(m_nuc_diameter > U_TO_REAL(0.0));
        uAssertRealPosEq(m_nuc_radius, (U_TO_REAL(0.5) * m_nuc_diameter));
        uAssert(m_nuc_homg_allowed_radius < m_nuc_radius);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_nuc_diameter = U_TO_REAL(0.0);
        m_nuc_radius = U_TO_REAL(0.0);
        m_nuc_homg_allowed_radius = U_TO_REAL(0.0);
        m_nuc_homg_allowed_sq_radius = U_TO_REAL(0.0);
        m_nuc_schur_buffer.clear();
        m_nuc_dot_buffer.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * Support ellipsoidal interface
     * @return Nuclear radius
     */
    inline uReal get_nuclear_radius(const enum uDim axis_unused) const {
        return this->get_max_nuclear_radius();
    }

    /**
     * Nodes (monomers) are rejected if they do not fit within nucleus. Since
     * all chromatin samples have the same nuclear dimensions, we are making
     * this a property of the simulation. This method has 'max' to allow for
     * different shapes such as triaxial ellipsoids which are specified by 3
     * radii, this makes it clear we simply want the largest radius.
     * @return Largest nuclear radius
     */
    inline uReal get_max_nuclear_radius() const {
        uAssertRealPosEq(m_nuc_radius, m_nuc_diameter * U_TO_REAL(0.5));
        return m_nuc_radius;
    }

    /**
     * Support more general ellipsoid interface, allow axial dimension to be
     * specified.
     * @WARNING - MUST BE CALLED WITH ACTUAL NODE RADIUS!
     * @param axis_unused - Axial dimension to query (ignored)
     * @param node_radius - Radius of element to fit within nucleus
     * @param sim - Parent simulation
     * @return Allowed nuclear radius for the node to fit within along the
     *  parameter axis
     */
    inline uReal get_allowed_nuclear_radius(const enum uDim axis_unused,
                                            const uReal node_radius,
                                            const sim_t& sim) const {
        return this->get_allowed_nuclear_radius(node_radius, sim);
    }

    /**
     * @WARNING - MUST BE CALLED WITH ACTUAL NODE RADIUS!
     * @param node_radius - Radius of element to fit within nucleus
     * @param sim - Parent simulation
     * @return Allowed nuclear radius for the node to fit within
     */
    inline uReal get_allowed_nuclear_radius(const uReal node_radius,
                                            const sim_t& sim) const {
        if (sim_t::is_homg_radius) {
            // CASE: HOMOGENEOUS NODES
            uAssertRealEq((m_nuc_radius - node_radius),
                          m_nuc_homg_allowed_radius);
            uAssertRealPosEq(
                m_nuc_homg_allowed_radius * m_nuc_homg_allowed_radius,
                m_nuc_homg_allowed_sq_radius);
            return this->m_nuc_homg_allowed_radius;
        }
        // ELSE: HETEROGENEOUS NODES
        uAssert(node_radius < m_nuc_radius);
        return m_nuc_radius - node_radius;
    }

    /**
     * Nodes (monomers) are rejected if they do not fit within nucleus. Since
     * all chromatin samples have the same nuclear dimensions, we are making
     * this a property of the simulation.
     * @WARNING - MUST BE CALLED WITH ACTUAL NODE RADIUS
     * @param node_radius - Radius of element to fit within nucleus
     * @param sim - Parent simulation
     * @return Square of the allowed nuclear radius
     */
    inline uReal get_allowed_nuclear_sq_radius(const uReal node_radius,
                                               const sim_t& sim) const {
        if (sim_t::is_homg_radius) {
            // CASE: HOMOGENEOUS NODES
            uAssertRealEq((m_nuc_radius - node_radius),
                          m_nuc_homg_allowed_radius);
            uAssertRealPosEq(
                m_nuc_homg_allowed_radius * m_nuc_homg_allowed_radius,
                m_nuc_homg_allowed_sq_radius);
            return this->m_nuc_homg_allowed_sq_radius;
        }
        // ELSE: HETEROGENEOUS NODES
        const uReal allowed_nuclear_radius =
            this->get_allowed_nuclear_radius(node_radius, sim);
        return allowed_nuclear_radius * allowed_nuclear_radius;
    }

    // Note: parent simulation is likely to be const, therefore making buffers
    // mutable objects
    inline uMatrix& get_nuc_schur_buffer(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_nuc_schur_buffer);
    }
    inline uVecCol& get_nuc_dot_buffer(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_nuc_dot_buffer);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Monomers are rejected if they do not fit within nuclear sphere.
     *
     * Units: Angstroms
     */
    uReal m_nuc_diameter;

    /**
     * The radius of the nucleus in Angstroms
     */
    uReal m_nuc_radius;

    /**
     * Used for determining portion of nuclear volume that a chromatin node
     * center may reside: allowed_r = (nuclear_radius - node_radius)
     *
     * Assumed to be same for all nodes in simulation
     * @WARNING - ONLY VALID FOR HOMOGENEOUS NODE SIMULATIONS!
     */
    uReal m_nuc_homg_allowed_radius;

    /**
     * The allowed squared radius of the nuclear sphere.
     * The radius is corrected by the node radius.
     * allowed_r^2 = (nuclear_radius - node_radius)^2
     * Nodes (monomers) are rejected if they do not fit within nucleus sphere:
     *   dot(node, node) > allowed_r^2
     *
     * Assumed to be same for all nodes in simulation.
     * @WARNING - ONLY VALID FOR HOMOGENEOUS NODE SIMULATIONS!
     */
    uReal m_nuc_homg_allowed_sq_radius;

    /**
     * Buffer to store element-wise multiplication
     */
    mutable U_DECLARE_TLS_DATA(uMatrix, m_nuc_schur_buffer);

    /**
     * Stores final dot product of candidate positions
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_nuc_dot_buffer);
};
