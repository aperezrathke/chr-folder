//****************************************************************************
// uSisLandmarkTrialRunnerMixin.h
//****************************************************************************

/**
 * Trial Runners must implement run_trial(). This method defines the main
 * update loop during a single trial of the the simulation. A "trial" is
 * defined as a single attempt at growing all target samples from end to end.
 * At the end of a trial, the simulation object will move all completed
 * samples to a completed pool and then run an entirely new trial for the
 * failed samples.
 *
 * In addition, the trial runner must keep track of the number of growth
 * calls (seed + step) for each sample.
 *
 * The landmark trial runner grows each sample from one landmark node count
 * to the next landmark node count. Each sample is grown by running a
 * sub-simulation and then selecting from the resulting sub-ensemble. The
 * sub-simulation may implement its own form of quality control. In addition,
 * the parent landmark simulation may apply its own quality control at
 * each landmark state.
 */

// @TODO - Handle dead chains from sub-simulation runs

#ifndef uSisLandmarkTrialRunnerMixin_h
#define uSisLandmarkTrialRunnerMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitsetHandle.h"
#include "uConfig.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// uSisLandmarkTrialRunnerMixin
//****************************************************************************

template <
    // The core simulation structures
    typename t_SisGlue,
    // Determines selection policy at landmark points
    typename t_LmrkSelect,
    // Determines landmark schedule
    typename t_LmrkSched,
    // Determines sub-simulation for generating sub-ensembles between
    // landmark states. Distinguish between _threaded and _serial
    // sub-simulation access.
    // See uSisSubSimAccess class
    typename t_LmrkSubSimAccess,
    // Defines method to(T1 from_, T2 to_) which
    // converts sample of type T1 to sample of type T2
    // See uSisSampleCopier_unsafe
    typename t_LmrkSampleCopyier,
    // Defines methods batch_grow_first() and batch_grow_next() which may
    // have parallel or serial implementations
    typename t_LmrkGrower,
    // Set to true to enable console logging of grow phases
    bool lmrk_heartbeat_>
class uSisLandmarkTrialRunnerMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

private:
    /**
     * Typedefs
     */
    typedef t_LmrkSelect uLmrkSelect_t;
    typedef t_LmrkSched uLmrkSched_t;
    typedef t_LmrkSubSimAccess uLmrkSubSimAccess;
    typedef typename uLmrkSubSimAccess::sim_t uLmrkSubSim;
    typedef typename uLmrkSubSim::sample_t uLmrkSubSample;
    typedef t_LmrkSampleCopyier uLmrkSampleCopyier;
    typedef t_LmrkGrower uLmrkGrower;

    // Allow access to internal growth methods
    friend uLmrkGrower;

    /**
     * Enum switch(es)
     */
    enum {
        // Flip switch to enable logging whenever a growth phase is completed
        lmrk_heartbeat = lmrk_heartbeat_
    };

public:
    /**
     * WARNING: MAY ONLY BE CALLED FROM MAIN THREAD
     * Runs a single trial of the simulation. Assumes simulation has been
     * (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation
     */
    void run_trial(std::vector<sample_t>& trial_samples,
                   uVecCol& trial_log_weights,
                   uWrappedBitset& wrapped_trial_status,
                   sim_t& sim) {
        uAssert(m_num_completed_grow_steps_per_sample == 0);

        // Grow to first landmark
        uUInt target_num_grow_steps =
            this->find_first_landmark(U_MAIN_THREAD_ID_0_ARG);
        m_grower.batch_grow_1st(wrapped_trial_status,
                                trial_samples,
                                trial_log_weights,
                                *this,
                                sim,
                                target_num_grow_steps U_MAIN_THREAD_ID_ARG);

        // Update growth count and inform simulation
        // @TODO - Should we call sim.on_seed_finish()?
        m_num_completed_grow_steps_per_sample = target_num_grow_steps;
        sim.on_grow_phase_finish(wrapped_trial_status U_MAIN_THREAD_ID_ARG);
        log_heartbeat(trial_log_weights);

        // Grow remaining nodes
        while (wrapped_trial_status.any()) {
            // Check schedule for next landmark
            target_num_grow_steps = this->find_next_landmark(
                m_num_completed_grow_steps_per_sample U_MAIN_THREAD_ID_ARG);

            // Grow to next landmark
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                target_num_grow_steps,
                m_num_completed_grow_steps_per_sample U_MAIN_THREAD_ID_ARG);

            // Update growth count
            m_num_completed_grow_steps_per_sample = target_num_grow_steps;

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_MAIN_THREAD_ID_ARG);
            log_heartbeat(trial_log_weights);
        }
    }

    /**
     * Runs a single trial of the simulation with the growth count capped at
     * parameter target grow steps. Will stop trial once target grow count has
     * been reached. Assumes simulation has been (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that each sample must successfully complete
     */
    void run_trial_from_start_until(std::vector<sample_t>& trial_samples,
                                    uVecCol& trial_log_weights,
                                    uWrappedBitset& wrapped_trial_status,
                                    sim_t& sim,
                                    const uUInt target_num_grow_steps
                                        U_THREAD_ID_PARAM) {
        uAssert(m_num_completed_grow_steps_per_sample == 0);
        // Assume not running from template
        uAssert(!sim.is_run_from_template());
        // Assume target step count is reasonable
        uAssert(this->check_target_num_grow_steps(
            target_num_grow_steps U_THREAD_ID_ARG));

        // Determine first landmark
        uUInt internal_target_num_grow_steps =
            std::min(this->find_first_landmark(U_THREAD_ID_0_ARG),
                     target_num_grow_steps);

        // Grow to first landmark
        m_grower.batch_grow_1st(wrapped_trial_status,
                                trial_samples,
                                trial_log_weights,
                                *this,
                                sim,
                                internal_target_num_grow_steps U_THREAD_ID_ARG);

        // Update growth count and inform simulation
        // @TODO - Should we call sim.on_seed_finish()?
        m_num_completed_grow_steps_per_sample = internal_target_num_grow_steps;
        sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);

        // Exit if we've reached our target growth count
        if (m_num_completed_grow_steps_per_sample == target_num_grow_steps) {
            goto exit_run_trial_from_start_until;
        }

        // Grow remaining nodes
        while (wrapped_trial_status.any()) {
            // Check schedule for next landmark
            internal_target_num_grow_steps =
                this->find_next_landmark(m_num_completed_grow_steps_per_sample,
                                         target_num_grow_steps U_THREAD_ID_ARG);

            // Grow to next landmark
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                internal_target_num_grow_steps,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Update our completed grow step count
            m_num_completed_grow_steps_per_sample =
                internal_target_num_grow_steps;

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);

            // Exit if we've reached our target growth count
            if (m_num_completed_grow_steps_per_sample ==
                target_num_grow_steps) {
                goto exit_run_trial_from_start_until;
            }
        }

        // Start of common clean-up code
    exit_run_trial_from_start_until:
        // Verify we match expected grow count
        uAssert((this->get_num_completed_grow_steps_per_sample() ==
                 target_num_grow_steps) ||
                !wrapped_trial_status.any());
        // @HACK - Signal that we are done growing non-dead chains by setting
        // all status bits to false
        wrapped_trial_status.reset();
    }

    /**
     * ASSUMES TEMPLATE SAMPLE HAS BEEN SEEDED!
     * Runs a single trial of the simulation using template sample as the
     * initial starting state for each sample to be grown. Will stop trial
     * once target grow count has been reached. Assumes simulation has been
     * (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation, also contains template sample info
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that each sample must successfully complete (includes template
     *  grow count as well)
     */
    void run_trial_from_template_until(std::vector<sample_t>& trial_samples,
                                       uVecCol& trial_log_weights,
                                       uWrappedBitset& wrapped_trial_status,
                                       sim_t& sim,
                                       const uUInt target_num_grow_steps
                                           U_THREAD_ID_PARAM) {
        uAssert(m_num_completed_grow_steps_per_sample == 0);
        // Verify we are running from template
        uAssert(sim.is_run_from_template());
        // Verify template has been seeded
        uAssert(sim.get_template_num_grow_steps() > 1);
        // Verify target grow count is greater than template count
        uAssert(sim.get_template_num_grow_steps() < target_num_grow_steps);
        // Assume target step count is reasonable
        uAssert(this->check_target_num_grow_steps(
            target_num_grow_steps U_THREAD_ID_ARG));
        // Verify parallel sets
        uAssertPosEq(trial_samples.size(), trial_log_weights.size());
        uAssert(trial_samples.size() == wrapped_trial_status.size());

        // Counter for iterating over each sample
        size_t i = 0;

        // Store return status of growth operations
        bool status = false;

        // Initialize samples using template
        const size_t n_trial_samples = trial_samples.size();
        for (i = 0; i < n_trial_samples; ++i) {
            sample_t& sample(trial_samples[i]);
            sample = sim.get_template_sample();
            uReal& log_weight(trial_log_weights.at(U_TO_MAT_SZ_T(i)));
            log_weight = sim.get_template_log_weight();
            uAssert(wrapped_trial_status.test(i));
        }

        // @TODO - is on_init_from_template() method needed?
        // normally, we would call on_seed_finish() or
        // reset_mixins_for_regrowth() or something like that here.
        uUInt internal_target_num_grow_steps =
            sim.get_template_num_grow_steps();
        m_num_completed_grow_steps_per_sample = internal_target_num_grow_steps;

        // Grow nodes
        while (wrapped_trial_status.any()) {
            // Check schedule for next landmark
            internal_target_num_grow_steps =
                this->find_next_landmark(m_num_completed_grow_steps_per_sample,
                                         target_num_grow_steps U_THREAD_ID_ARG);

            // Grow to next landmark
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                internal_target_num_grow_steps,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Update our completed grow step count
            m_num_completed_grow_steps_per_sample =
                internal_target_num_grow_steps;

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);

            // Exit if we've reached our target growth count
            if (this->get_num_completed_grow_steps_per_sample() ==
                target_num_grow_steps) {
                break;
            }
        }

        // Verify we match expected grow count
        uAssert((this->get_num_completed_grow_steps_per_sample() ==
                 target_num_grow_steps) ||
                !wrapped_trial_status.any());
        // @HACK - Signal that we are done growing non-dead chains by setting
        // all status bits to false
        wrapped_trial_status.reset();
    }

    /**
     * Initializes trial runner
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Clear any previous state
        clear();

        // Initialize landmark selection
        m_select_pol.init(sim, config U_THREAD_ID_ARG);
        // Initialize landmark schedule
        m_sched_pol.init(sim, config, true /*is_tr*/ U_THREAD_ID_ARG);

        // Handle to sub-sim configuration (default to parent)
        uSpConstConfig_t sub_sim_cfg = config;
        if (config->get_child_tr()) {
            sub_sim_cfg = config->get_child_tr();
        }

        // Initialize sub-simulation
        m_sub_sim_access.init(sub_sim_cfg U_THREAD_ID_ARG);
    }

    /**
     * Clear trial runner to default state
     */
    void clear() {
        m_select_pol.clear();
        m_sched_pol.clear();
        m_num_completed_grow_steps_per_sample = 0;
        m_sub_sim_access.clear();
    }

    /**
     * Preps for next trial
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        m_select_pol.reset_for_next_trial(sim, config U_THREAD_ID_ARG);
        m_sched_pol.reset_for_next_trial(sim, config U_THREAD_ID_ARG);
        m_num_completed_grow_steps_per_sample = 0;
        m_sub_sim_access.init(U_THREAD_ID_0_ARG);
    }

    /**
     * @return The number of seed() + grow_*() calls for the longest
     *  active sample within the trial
     */
    inline uUInt get_num_completed_grow_steps_per_sample() const {
        return m_num_completed_grow_steps_per_sample;
    }

    /**
     * Called after initial chromatin node(s) have been placed at all samples.
     * Updates count of grow steps.
     * @param sim - parent simulation containing global sample data
     */
    void on_seed_finish(const sim_t& sim) {}

    /**
     * Called after a set of incremental chromatin nodes have been appended to
     * each active sample. Updates count of grow steps.
     * @param sim - parent simulation containing global sample data
     */
    void on_grow_phase_finish(const sim_t& sim U_THREAD_ID_PARAM) {}

    /**
     * Will regrow a single sample to target grow step count
     * @param out_sample - The sample to be regrown
     * @param out_log_weight - The final log weight of the sample
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that must be successfully completed
     * @param sample_id - The sample identifier (index into samples array)
     * @return final status of last grow_*() call -> TRUE if sample needs
     *  to continue growing, FALSE if finished growing. We should not return
     *  any dead chains. Currently, method will infinite loop until chain is
     *  no longer dead.
     */
    bool regrow_force_from_start(sample_t& out_sample,
                                 uReal& out_log_weight,
                                 sim_t& sim,
                                 const uUInt target_num_grow_steps,
                                 const uUInt sample_id U_THREAD_ID_PARAM) {
        // If this assert trips, client should be calling
        // regrow_attempt_from_template() method instead
        uAssert(!sim.is_run_from_template());
        // Assume target step count is reasonable
        uAssert(this->check_target_num_grow_steps(
            target_num_grow_steps U_THREAD_ID_ARG));

        // Used for storing status after each grow step
        bool status = false;

        // Keeps track of number of completed grow steps
        // (seed() + grow_*() calls)
        uUInt num_grow_steps = 0;

        // Transient intermediate grow steps to next landmark
        uUInt internal_target_num_grow_steps = 0;

        while (num_grow_steps < target_num_grow_steps) {
            //////////////////////////////////////////////////////
            // Reset sample
            //////////////////////////////////////////////////////

            uAssert(num_grow_steps == 0);

            // Reset any simulation level mixins that need it
            // @Warning - If they do need to be reset, they are probably not
            // thread safe!
            sim.reset_mixins_for_regrowth(out_sample, 0);

            // Reset sample weight
            out_log_weight = U_TO_REAL(0.0);

            // Reset the sample
            sim.reset_sample(out_sample);

            //////////////////////////////////////////////////////
            // First landmark
            //////////////////////////////////////////////////////

            // Determine first landmark
            internal_target_num_grow_steps =
                std::min(this->find_first_landmark(U_THREAD_ID_0_ARG),
                         target_num_grow_steps);

            // Perform growth to landmark
            status = this->grow_1st(internal_target_num_grow_steps,
                                    out_sample,
                                    out_log_weight U_THREAD_ID_ARG);

            // Check if growth failed
            if (out_sample.is_dead()) {
                uAssert(!status);
                num_grow_steps = 0;
                continue;
            }

            // Update grow step count
            num_grow_steps = internal_target_num_grow_steps;

            // Check if mixins handle this event
            sim.on_regrow_step_finish(out_sample,
                                      out_log_weight,
                                      status,
                                      num_grow_steps,
                                      sample_id U_THREAD_ID_ARG);

            // Check if sample was culled by mixins
            if (out_sample.is_dead()) {
                uAssert(!status);
                num_grow_steps = 0;
                continue;
            }

            //////////////////////////////////////////////////////
            // Remaining landmarks
            //////////////////////////////////////////////////////

            while (num_grow_steps < target_num_grow_steps) {
                // @WARNING - To simplify code and avoid extraneous
                // conditionals, am assuming client is passing a reasonable
                // value for target_num_grow_steps such that no more
                // grow_*() calls than necessary are required. In other
                // words, no overgrowth will occur.
                uAssert(status);
                uAssert(!out_sample.is_dead());

                // Determine next landmark
                internal_target_num_grow_steps = this->find_next_landmark(
                    num_grow_steps, target_num_grow_steps U_THREAD_ID_ARG);

                // Perform growth to landmark
                status = this->grow_nth(internal_target_num_grow_steps,
                                        out_sample,
                                        num_grow_steps,
                                        out_log_weight);

                // Check if growth failed
                if (out_sample.is_dead()) {
                    uAssert(!status);
                    num_grow_steps = 0;
                    continue;
                }

                // Update grow step count
                num_grow_steps = internal_target_num_grow_steps;

                // Check if mixins handle this event
                sim.on_regrow_step_finish(out_sample,
                                          out_log_weight,
                                          status,
                                          num_grow_steps,
                                          sample_id U_THREAD_ID_ARG);

                // Check if sample was culled by mixins
                if (out_sample.is_dead()) {
                    // Restart dead sample
                    uAssert(!status);
                    num_grow_steps = 0;
                    break;
                }
            }  // end of next landmark growth loop
        }      // end of full growth loop

        uAssert(!out_sample.is_dead());
        return status;
    }

    /**
     * Will perform a single attempt to regrow a sample using a given starting
     *  template. Meant to work with partial rejection control
     * @WARNING - Assumes no seeding is required by the sample
     * @param out_sample - The sample to attempt regrowth
     * @param out_log_weight - The log weight of the sample after regrowth
     *  attempt, will be set to highly negative value if growth fails.
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that will be attempted for successful completion. Note, no
     *  actual seed() calls will be made as template sample is assumed to have
     *  already been seeded.
     * @param sample_id - The sample identifier (index into samples array)
     * @param template_sample - The template sample state to start the
     *  regrowth procedure from
     * @param template_num_grow_steps - The total number of seed() +
     *  grow_*() calls assumed to have been comppleted by the template
     *  sample.
     * @param init_log_weight - The out_log_weight is initialized to this
     *  value prior to regrowth
     * @return TRUE if regrowth attempt successful and sample still needs
     *  growing. FALSE if regrowth attempt successful and sample no longer
     *  needs growing or if regrowth attempt unsuccessful (to distinguish
     *  between these two cases, check sample_t::is_dead())
     */
    bool regrow_attempt_from_template(sample_t& out_sample,
                                      uReal& out_log_weight,
                                      sim_t& sim,
                                      const uUInt target_num_grow_steps,
                                      const uUInt sample_id,
                                      const sample_t& template_sample,
                                      const uUInt template_num_grow_steps,
                                      const uReal init_log_weight
                                          U_THREAD_ID_PARAM) {
        // Same constraint - we require at least a single
        // grow_*() beyond seeding
        uAssert(sim.get_max_total_num_nodes() > 1);
        // We require that target exceeds template
        uAssert(template_num_grow_steps < target_num_grow_steps);
        // We require that template is valid
        uAssert(!template_sample.is_dead());
        // Assume target step count is reasonable
        uAssert(this->check_target_num_grow_steps(
            target_num_grow_steps U_THREAD_ID_ARG));

        // Used for storing status after each grow step
        bool status = true;

        // Reset any mixins that need it
        // @WARNING - Probably not thread safe if any sim level mixins need
        // resetting
        sim.reset_mixins_for_regrowth(out_sample, template_num_grow_steps);

        // Overwrite sample
        out_sample = template_sample;

        // Update sample weight
        out_log_weight = init_log_weight;

        // Update grow step count
        uUInt num_grow_steps = template_num_grow_steps;

        // Transient intermediate grow steps to next landmark
        uUInt internal_target_num_grow_steps = 0;

        while (num_grow_steps < target_num_grow_steps) {
            uAssert(status);

            // Check schedule for next landmark
            internal_target_num_grow_steps = this->find_next_landmark(
                num_grow_steps, target_num_grow_steps U_THREAD_ID_ARG);

            // Perform growth to landmark
            status = this->grow_nth(internal_target_num_grow_steps,
                                    out_sample,
                                    num_grow_steps,
                                    out_log_weight);

            // Check if growth failed
            if (out_sample.is_dead()) {
                uAssert(!status);
                break;
            }

            // Update grow step count
            num_grow_steps = internal_target_num_grow_steps;

            // Let simulation/mixins know that sample has completed a
            // grow_*() call
            sim.on_regrow_step_finish(out_sample,
                                      out_log_weight,
                                      status,
                                      num_grow_steps,
                                      sample_id U_THREAD_ID_ARG);

            // Check if sample was culled by mixins
            if (out_sample.is_dead()) {
                uAssert(!status);
                break;
            }
        }

        return status;
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * @return thread-specific sub-simulation object
     */
    inline uLmrkSubSim& get_sub_sim(U_THREAD_ID_0_PARAM) {
        return m_sub_sim_access.get_sub_sim(U_THREAD_ID_0_ARG);
    }

    /**
     * @return first landmark grow step count
     */
    inline uUInt find_first_landmark(U_THREAD_ID_0_PARAM) {
        const uLmrkSubSim& sub_sim = this->get_sub_sim(U_THREAD_ID_0_ARG);
        const uUInt max_grow_step_count =
            sub_sim.get_max_num_grow_steps(sub_sim);
        const uUInt grow_step_count =
            m_sched_pol.tr_find_first(max_grow_step_count);
        // Assume target step count is reasonable
        uAssert(
            this->check_target_num_grow_steps(grow_step_count U_THREAD_ID_ARG));
        return grow_step_count;
    }

    /**
     * @param prev_grow_step_count - the number of completed grow steps
     * @return the next landmark grow step count after the current grow step
     * count
     */
    inline uUInt find_next_landmark(
        const uUInt prev_grow_step_count U_THREAD_ID_PARAM) {
        const uLmrkSubSim& sub_sim = this->get_sub_sim(U_THREAD_ID_0_ARG);
        const uUInt max_grow_step_count =
            sub_sim.get_max_num_grow_steps(sub_sim);
        const uUInt grow_step_count =
            m_sched_pol.tr_find_next(prev_grow_step_count, max_grow_step_count);
        uAssert(grow_step_count > prev_grow_step_count);
        // Assume target step count is reasonable
        uAssert(
            this->check_target_num_grow_steps(grow_step_count U_THREAD_ID_ARG));
        return grow_step_count;
    }

    /**
     * @param grow_step_count - the current grow step count
     * @param max_grow_step_count - the grow step is capped to this value
     * @return the next landmark grow step count after the current grow step
     * count
     */
    inline uUInt find_next_landmark(const uUInt grow_step_count,
                                    const uUInt max_grow_step_count
                                        U_THREAD_ID_PARAM) {
        uAssert(max_grow_step_count > 0);
        uAssert(max_grow_step_count > grow_step_count);
        // Assume target step count is reasonable
        uAssert(this->check_target_num_grow_steps(
            max_grow_step_count U_THREAD_ID_ARG));
        return std::min(
            this->find_next_landmark(grow_step_count U_THREAD_ID_ARG),
            max_grow_step_count);
    }

    /**
     * Grows blank sample to first landmark
     * @param target_num_grow_steps - The number of grow steps to complete
     * @param sample - The sample to grow
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check sample_t::is_dead() to detect failure)
     */
    bool grow_1st(const uUInt target_num_grow_steps,
                  sample_t& sample,
                  uReal& log_weight,
                  const sim_t& sim_unused U_THREAD_ID_PARAM) {
        uAssert(target_num_grow_steps > 0);
        uAssert(log_weight == 0);

        // Obtain handle to sub-simulation
        uLmrkSubSim& sub_sim = this->get_sub_sim(U_THREAD_ID_0_ARG);

        // Initialize sub-simulation (light weight)
        sub_sim.init(U_THREAD_ID_0_ARG);

        // Generate sub-ensemble
        sub_sim.run_from_start_until(target_num_grow_steps U_THREAD_ID_ARG);

        return this->epilog_grow_to_landmark(
            target_num_grow_steps, sample, log_weight, sub_sim U_THREAD_ID_ARG);
    }

    /**
     * SHOULD NEVER BE CALLED! Only provided to give consistent interface as
     * canonical trial runner
     * Grows sample from current state to next landmark state
     * @param target_num_grow_steps - The number of grow steps to complete
     * @param sample - The sample to grow to next landmark
     * @param sample_num_grow_steps - The number of grow steps completed by
     *  parameter sample
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check log_weight to see if dead)
     */
    bool grow_2nd(const uUInt target_num_grow_steps,
                  sample_t& sample,
                  const uUInt sample_num_grow_steps,
                  uReal& log_weight,
                  const sim_t& sim_unused U_THREAD_ID_PARAM) {
        uAssert(false);
        return false;
    }

    /**
     * NO GUARANTEE THAT SAMPLE WILL BE SEEDED
     * Grows sample from current state to next landmark state
     * @param target_num_grow_steps - The number of grow steps to complete
     * @param sample - The sample to grow to next landmark
     * @param sample_num_grow_steps - The number of grow steps completed by
     *  parameter sample
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check log_weight to see if dead)
     */
    bool grow_nth(const uUInt target_num_grow_steps,
                  sample_t& sample,
                  const uUInt sample_num_grow_steps,
                  uReal& log_weight,
                  const sim_t& sim_unused U_THREAD_ID_PARAM) {
        uAssert(target_num_grow_steps > sample_num_grow_steps);
        uAssert(sample_num_grow_steps > 1);

        // Obtain handle to sub-simulation
        uLmrkSubSim& sub_sim = this->get_sub_sim(U_THREAD_ID_0_ARG);

        // Generate template for sub-simulation
        uLmrkSubSample sub_sample_template;
        m_sample_copier.to(sample /*from*/, sub_sample_template /*to*/);

        // Initialize sub-simulation (light weight)
        sub_sim.init(U_THREAD_ID_0_ARG);

        sub_sim.run_from_template_until(target_num_grow_steps,
                                        sub_sample_template,
                                        sample_num_grow_steps,
                                        log_weight U_THREAD_ID_ARG);

        return this->epilog_grow_to_landmark(
            target_num_grow_steps, sample, log_weight, sub_sim U_THREAD_ID_ARG);
    }

    /**
     * @HACK, @WARNING - Assumes sample is complete if:
     *      target grow steps == sub sim max grow steps
     * Common work that is done after a sample has been grown to a landmark.
     * Selects a sample from the sub-ensemble and adjusts weight according to
     * selection probability.
     * @param target_num_grow_steps - The number of completed grow steps
     *  for the sub-ensemble
     * @param sample - The output sample
     * @param log_weight - The output sample log weight
     * @param sub_sim - The sub-simulation with a completed sub-ensemble
     * @return True if sample needs growing, False otherwise
     */
    bool epilog_grow_to_landmark(const uUInt target_num_grow_steps,
                                 sample_t& sample,
                                 uReal& log_weight,
                                 const uLmrkSubSim& sub_sim U_THREAD_ID_PARAM) {
        // Early out if no sub-simulation samples completed
        if (sub_sim.get_completed_samples().empty()) {
            // @HACK, @WARNING: assumes constraint failure
            sample.mark_dead(uSisFail_CONS, log_weight);
            return false;
        }

        // Obtain handle to sub-sim completed log weights
        const uVecCol& sub_completed_log_weights =
            sub_sim.get_completed_log_weights_view();

        uAssertPosEq(sub_sim.get_completed_samples().size(),
                     sub_completed_log_weights.size());

        // Get selected index
        uReal log_p_select = U_TO_REAL(0.0);
        const uUInt idx =
            m_select_pol.select_index(sub_completed_log_weights,
                                      sub_sim.get_completed_samples(),
                                      target_num_grow_steps,
                                      log_p_select U_THREAD_ID_ARG);
        uAssert(log_p_select <= U_TO_REAL(0.0));
        uAssertBounds(idx, 0, sub_sim.get_completed_samples().size());

        // Verify selected sample isn't dead
        uAssert(!sample.is_dead());

        // Obtain handle to selected subsample
        const uLmrkSubSample& sub_sample = sub_sim.get_completed_samples()[idx];

        // Copy subsample to output sample
        m_sample_copier.to(sub_sample /*from*/, sample /*to*/);

        // Set and adjust weight by selection probability
        log_weight = sub_completed_log_weights.at(idx) - log_p_select;

        // Check if max grow step count has been reached
        return target_num_grow_steps < sub_sim.get_max_num_grow_steps(sub_sim);
    }

    /**
     * Conditionally logs state of growth simulation if enabled
     * @param trial_log_weights - Vector of log weights of samples being grown
     */
    inline void log_heartbeat(const uVecCol& trial_log_weights) const {
        if (lmrk_heartbeat) {
            uLogf(
                "-Landmark trial runner reached growth phase: %u\n\t-ESS: %f\n",
                (unsigned int)m_num_completed_grow_steps_per_sample,
                uSisUtilsQc::calc_ess_cv_from_log(trial_log_weights));
        }
    }

    /**
     * @return True if target grow step count is valid, False o/w
     */
    inline bool check_target_num_grow_steps(
        const uUInt target_num_grow_steps U_THREAD_ID_PARAM) {
        const uLmrkSubSim& sub_sim = this->get_sub_sim(U_THREAD_ID_0_ARG);
        return (target_num_grow_steps > 1) &&
               (target_num_grow_steps <=
                sub_sim.get_max_num_grow_steps(sub_sim));
    }

    /**
     * Landmark selection policy. Once a set of candidates have
     * reached a landmark state, one is selected according to
     * this policy
     */
    uLmrkSelect_t m_select_pol;

    /**
     * Landmark schedule policy. Determines which states are
     * landmark states.
     */
    uLmrkSched_t m_sched_pol;

    /**
     * Used for converting between parent and child simulation sample types
     */
    uLmrkSampleCopyier m_sample_copier;

    /**
     * Used for growing a batch of samples
     */
    uLmrkGrower m_grower;

    /**
     * Counter to keep track of number of grow steps completed. This
     * is the sum of seed() + grow_*() calls within a single trial and is
     * reset between trials.
     */
    uUInt m_num_completed_grow_steps_per_sample;

    /**
     * Defines how our sub-simulation objects are accessed in threaded
     * and non-threaded environments
     */
    uLmrkSubSimAccess m_sub_sim_access;
};

//****************************************************************************
// uSisSubSimAccess
//****************************************************************************

/**
 * For a serial landmark trial runner:
 *  Set template parameter t_LmrkSubSimAccess = uSisSubSimAccess_serial<t>
 *
 * For a full TLS landmark trial runner:
 *  Set template parameter t_LmrkSubSimAccess = uSisSubSimAccess_threaded<t>
 *
 * where t = sub-simulation type
 */

#include "uSisSubSimAccess.h"

//****************************************************************************
// uSisSampleCopier_unsafe
//****************************************************************************

/**
 * Utility for copying samples across parent|child simulations
 */

#include "uSisSampleCopier_unsafe.h"

#endif  // uSisLandmarkTrialRunnerMixin_h
