//****************************************************************************
// uSisSim.h
//****************************************************************************

/**
 * Simulates sequential importance sampling for a self-avoiding random walk
 * within a nucleus.
 */

#ifndef uSisSim_h
#define uSisSim_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitset.h"
#include "uBitsetHandle.h"
#include "uConfig.h"
#include "uSisTemplateSampleInfo.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// Interface
//****************************************************************************

/**
 * Sis= sequential importance sampling
 * Sim = simulation
 *
 * Simulation generates an ensemble of self-avoiding chromatin chain samples
 * within a nuclear sphere.
 *
 * This class is meant to define the bare essentials for running a simulation.
 * The template sample type is responsible for housing per sample data and
 * locus chain growth details/implementation.
 */
template <typename t_SisGlue>
class uSisSim : public t_SisGlue::trial_runner_mixin_t,
                public t_SisGlue::sim_level_qc_mixin_t,
                public t_SisGlue::sim_level_node_radius_mixin_t,
                public t_SisGlue::sim_level_growth_mixin_t,
                public t_SisGlue::sim_level_nuclear_mixin_t,
                public t_SisGlue::sim_level_collision_mixin_t,
                public t_SisGlue::sim_level_intr_chr_mixin_t,
                public t_SisGlue::sim_level_intr_lam_mixin_t,
                public t_SisGlue::sim_level_intr_nucb_mixin_t,
                public t_SisGlue::sim_level_energy_mixin_t,
                public t_SisGlue::seed_mixin_t {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    // Constructors

    /**
     * Default constructor
     */
    uSisSim() { clear(); }

    /**
     * Constructs from config object
     * @param configuration object specifying simulation parameters
     */
    explicit uSisSim(uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->init(config U_THREAD_ID_ARG);
    }

    /**
     * Initializes from config object
     * @param configuration object specifying simulation parameters
     */
    void init(uSpConstConfig_t config U_THREAD_ID_PARAM);

    /**
     * Lightweight re-initialization using saved config object used during
     * full initialization. This is a cheaper version of init() assumming that
     *  1.) init(config) has already been called
     *  2.) The simulation configuration has not changed
     * Refreshes simulation back to init state, ready to run a new simulation
     */
    void init(U_THREAD_ID_0_PARAM);

    /**
     * Resets to default empty state
     */
    void clear() {
        m_config.reset();
        m_template_nfo.clear();
        m_max_total_num_nodes = 0;
#ifdef U_BUILD_ENABLE_THREADS
        m_grow_chunk_size = 0;
#endif  // U_BUILD_ENABLE_THREADS
        m_unit_sphere_sample_points.clear();
        m_trial_status.clear();
        m_trial_log_weights.clear();
        m_completed_log_weights_view.clear();
        m_completed_log_weights_buffer.clear();
        this->trial_runner_mixin_t::clear();
        this->sim_level_qc_mixin_t::clear();
        this->sim_level_node_radius_mixin_t::clear();
        this->sim_level_growth_mixin_t::clear();
        this->sim_level_nuclear_mixin_t::clear();
        this->sim_level_collision_mixin_t::clear();
        this->sim_level_intr_chr_mixin_t::clear();
        this->sim_level_intr_lam_mixin_t::clear();
        this->sim_level_intr_nucb_mixin_t::clear();
        this->sim_level_energy_mixin_t::clear();
        this->seed_mixin_t::clear();
        m_trial_samples.clear();
        m_completed_samples.clear();
    }

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Runs entire simulation to completion.
     * Assumes simulation has been initialized.
     * @return TRUE if simulation successfully generated target ensemble size,
     *  FALSE otherwise.
     */
    uBool run();

    /**
     * Runs simulation like normal except that samples are considered
     * completed after their absolute grow step count (total # of seed +
     # grow_step calls) reaches the target count.
     * @param target_num_grow_steps - the total # of seed + grow_step calls
     * that each sample must successfully complete. Simulation will stop once
     * all samples have reached this count.
     * @return TRUE if simulation successfully generated target ensemble size,
     *  FALSE otherwise.
     */
    uBool run_from_start_until(
        const uUInt target_num_grow_steps U_THREAD_ID_PARAM);

    /**
     * Runs simulation like normal except samples are initialized to a
     * common starting template state. The simulation terminates after
     * each simulation successfully completes target_num_grow_steps
     * (where the template_num_grow_steps are included in this total)
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that each sample must successfully complete (includes template
     *  grow count as well)
     * @param template_sample - The template sample state to initialize all
     *  sample to
     * @param template_num_grow_steps - The total number of seed() +
     *  grow_*() calls assumed to have been comppleted by the template
     *  sample.
     * @param template_log_weight - The log weight of the template sample
     * @return TRUE if simulation successfully generated target ensemble size,
     *  FALSE otherwise.
     */
    uBool run_from_template_until(const uUInt target_num_grow_steps,
                                  const sample_t& template_sample,
                                  const uUInt template_num_grow_steps,
                                  const uReal template_log_weight
                                      U_THREAD_ID_PARAM);

    // Accessors

    /**
     * @return View of configuration object
     */
    inline uSpConstConfig_t get_config() const { return m_config; }

    /**
     * @return target number of samples to generate across all trials
     */
    inline uUInt get_target_total_num_samples() const {
        uAssert(m_config->ensemble_size > 0);
        return m_config->ensemble_size;
    }

    /**
     * @return target number of samples to generate for current trial
     */
    inline uUInt get_target_trial_num_samples() const {
        return U_TO_UINT(m_trial_samples.size());
    }

    /**
     * @return The maximum number of nodes for a sample.
     */
    inline uUInt get_max_total_num_nodes() const {
        uAssert(m_max_total_num_nodes > 0);
        return m_max_total_num_nodes;
    }

#ifdef U_BUILD_ENABLE_THREADS
    inline uUInt get_grow_chunk_size() const {
        uAssert(m_grow_chunk_size > 0);
        return m_grow_chunk_size;
    }
#endif  // U_BUILD_ENABLE_THREADS

    /**
     * @return vector with maximum number of nodes at each locus.
     */
    inline const std::vector<uUInt>& get_max_num_nodes_at_locus() const {
        return m_config->num_nodes;
    }

    /**
     * @return number of loci being simulated
     */
    inline uUInt get_num_loci() const {
        return U_TO_UINT(m_config->num_nodes.size());
    }

    /**
     * Polymer chains are grown sequentially by sampling points relative to
     * the tail node surface. Of the points that fit within the nuclear sphere
     * and which do not collide with any other nodes (chain is self-avoiding),
     * one is randomly selected to grow the chain. Since all nodes have the
     * same basis sample points, we are making this a property of the parent
     * simulation.
     * @return const reference to matrix containing points to sample on unit
     *  sphere
     */
    inline const uMatrix& get_unit_sphere_sample_points() const {
        // Note: to be useful, these points have to be scaled by the tail node
        // diameter and then translated by the tail node center.
        uAssert(m_unit_sphere_sample_points.size() >= 3);
        uAssert(m_unit_sphere_sample_points.n_rows > 0);
        uAssert(m_unit_sphere_sample_points.n_cols == 3);
        return m_unit_sphere_sample_points;
    }

    /**
     * @return Number of candidate positions sampled on unit sphere when
     *  growing next node in polymer chain
     */
    inline uUInt get_num_unit_sphere_sample_points() const {
        // Assuming each row is a sample position (row-major)
        uAssert(m_unit_sphere_sample_points.n_cols == uDim_num);
        return U_TO_UINT(m_unit_sphere_sample_points.n_rows);
    }

    /**
     * Note: calling this method has side effects as the view is updated!
     * @return vector of log weights at each finished sample
     */
    inline const uVecCol& get_completed_log_weights_view() const {
        uAssertPosEq(m_completed_log_weights_buffer.size(),
                     this->get_target_total_num_samples());
        uAssert(m_completed_samples.size() <=
                this->get_target_total_num_samples());
        // @HACK - Using placement new instead of sub-matrix views because too
        // many methods in this code base expect a uVecCol parameter. If using
        // a view type, would have to either template these methods or change
        // parameter type to match return type of uVecCol::head(n_elem). The
        // simpler solution is just use placement new to help ensure we are
        // not allocating new memory. I don't *think* this is leaking as I ran
        // a simple test and the destructor for member variable
        // m_completed_log_weights_view is getting called.
        new (&m_completed_log_weights_view)
            uVecCol(m_completed_log_weights_buffer.memptr(),
                    U_TO_MAT_SZ_T(m_completed_samples.size()),
                    false, /* copy */
                    true   /* strict */
            );
        uAssert(m_completed_log_weights_view.memptr() ==
                m_completed_log_weights_buffer.memptr());
        return m_completed_log_weights_view;
    }

    /**
     * @return Const vector of samples that have successfully generated
     */
    inline const std::vector<sample_t>& get_completed_samples() const {
        uAssert(m_completed_samples.size() <=
                this->get_target_total_num_samples());
        return m_completed_samples;
    }

    /**
     * @return TRUE if simulation has generated the target number of non-dead
     *  samples, FALSE otherwise
     */
    inline uBool has_completed_successfully() const {
        uAssert(m_completed_samples.size() <=
                this->get_target_total_num_samples());
        return get_completed_samples().size() ==
               this->get_target_total_num_samples();
    }

    /**
     * @return TRUE if template sample has been registered, FALSE otherwise.
     * When set, the template sample is meant to serve as the initial starting
     * state for any newly grown samples.
     */
    inline uBool is_run_from_template() const {
        uAssert(m_template_nfo.check_state());
        return m_template_nfo.m_enabled;
    }

    /**
     * @return Reference to registered template sample. When set, the template
     * sample is meant to serve as the initial starting state for any newly
     * grown samples.
     */
    inline const sample_t& get_template_sample() const {
        uAssert(m_template_nfo.check_state(uTRUE));
        return m_template_nfo.m_sample;
    }

    /**
     * @return Number of grow step calls (seed + grow_step) for the registered
     * template sample. When set, the template sample is meant to serve as the
     * initial starting state for any newly grown samples.
     */
    inline uUInt get_template_num_grow_steps() const {
        uAssert(m_template_nfo.check_state(uTRUE));
        return m_template_nfo.m_num_grow_steps;
    }

    /**
     * @return The log weight of the template sample. When set, the template
     * sample is meant to serve as the initial starting state for any newly
     * grown samples.
     */
    inline uReal get_template_log_weight() const {
        uAssert(m_template_nfo.check_state(uTRUE));
        return m_template_nfo.m_log_weight;
    }

    /**
     * Exposing to allow quality control operations to reset sample states
     * between trials
     */
    inline void reset_sample(sample_t& sample U_THREAD_ID_PARAM) {
        sample.init(*this, m_config U_THREAD_ID_ARG);
    }

    /**
     * Called after initial chromatin node(s) have been placed at all samples.
     * Routes to relevant simulation level mixins.
     */
    void on_seed_finish();

    /**
     * Called after a set of incremental chromatin nodes have been appended to
     * each active sample. Routes to relevant simulation level mixins.
     * @param wrapped_trial_status - The trial status wrapped as a handle.
     *  This is to avoid having to wrap the underlying status buffer again.
     *  Also, wrapped bit sets have faster bit iteration methods as they use
     *  compiler intrinsics.
     */
    void on_grow_phase_finish(
        uWrappedBitset& wrapped_trial_status U_THREAD_ID_PARAM);

    // BEGIN REGROW INTERFACE

    /**
     * The following methods are used by rejection control mixin for regrowing
     * a single sample.
     */

    /**
     * Informs sim-level mixins that parameter sample is about to be regrown
     * from scratch. Allows mixins to handle this event.
     * @param sample - The sample being regrown
     * @param num_completed_grow_steps - The number of seed() +
     *  grow_*() calls for the sample being regrown
     */
    void reset_mixins_for_regrowth(const sample_t& sample,
                                   const uUInt num_completed_grow_steps);

    /**
     * Informs mixins that sample has finished being reseeded. Allows mixins
     * to handle this event.
     */
    void on_reseed_finish(const sample_t& sample,
                          uReal& log_weight,
                          bool& status);

    /**
     * Informs mixins that sample has finished being regrown. Allows mixins to
     * handle this event.
     */
    void on_regrow_step_finish(sample_t& sample,
                               uReal& log_weight,
                               bool& status,
                               const uUInt num_completed_grow_steps,
                               const uUInt sample_id U_THREAD_ID_PARAM);

    // END REGROW INTERFACE

    // @ HACK - exposing method to allow landmark trial runner
    //  sub sim access
    /**
     * Resets simulation state to run a new trial
     */
    void reset_for_next_trial(U_THREAD_ID_0_PARAM);

private:
    /**
     * Performs post-trial work like transferring completed samples.
     */
    void on_trial_finish();

    /**
     * Preps mixins for next trial
     */
    void reset_mixins_for_next_trial(U_THREAD_ID_0_PARAM);

    /**
     * Does not resize vector!
     * @return vector of log weights at each finished sample
     */
    uVecCol& get_completed_log_weights_view_unsafe() {
        uAssert(m_completed_log_weights_view.size() ==
                m_completed_samples.size());
        return m_completed_log_weights_view;
    }

    /**
     * @return maximum number of trials allowed
     */
    inline uUInt get_max_num_trials() const {
        uAssert(m_config->max_trials > 0);
        return m_config->max_trials;
    }

    /**
     * @return Vector of samples that are currently being generated
     */
    inline std::vector<sample_t>& get_trial_samples() {
        uAssertPosEq(m_trial_log_weights.size(), m_trial_samples.size());
        return m_trial_samples;
    }

    /**
     * Our configuration object - used for resetting between trial samples
     */
    uSpConstConfig_t m_config;

    /**
     * Utility structure for managing template sample data
     */
    uSisTemplateSampleInfo<sample_t> m_template_nfo;

    /**
     * The maximum number of nodes per sample.
     * The sum of the maximum number of nodes at each locus.
     */
    uUInt m_max_total_num_nodes;

#ifdef U_BUILD_ENABLE_THREADS
    /**
     * Atomic number of samples processed by worker threads before looking for
     * more work
     */
    uUInt m_grow_chunk_size;
#endif  // U_BUILD_ENABLE_THREADS

    /**
     * A matrix of regularly-spaced points on the surface of the unit sphere
     */
    uMatrix m_unit_sphere_sample_points;

    /**
     * Contains status bit for each sample. If 0, then sample is stuck (has
     * failed) or has finished. To determine if the sample is stuck,
     * inspection of the log weights is necessary.
     */
    uBitset m_trial_status;

    /**
     * A vector containing the current log weights at each sample being
     * generated. Resides at simulation level for easier access when pruning
     * or resampling, etc.
     */
    uVecCol m_trial_log_weights;

    /**
     * A vector containing the final log weights at each successfully finished
     * sample. Resides at simulation level for easier access when pruning
     * or resampling, etc. This should only be accessed via the
     * accessor method: get_completed_log_weights_view()
     */
    mutable uVecCol m_completed_log_weights_view;

    /**
     * The underlying reserved memory for the completed log weights
     */
    mutable uVecCol m_completed_log_weights_buffer;

    /**
     * The trial ensemble - a collection of self-avoiding chromatin samples
     * that are currently being generated.
     */
    std::vector<sample_t> m_trial_samples;

    /**
     * The final ensemble - a collection of self-avoiding chromatin samples
     * that have successfully finished.
     */
    std::vector<sample_t> m_completed_samples;
};

// Include implementation as we are a template class
#include "uSisSim.inl"

#endif  // uSisSim_h
