//****************************************************************************
// uSisInterWeightsScaleRcMixin.h
//****************************************************************************

/**
 * ScaleRcMixin = A mixin that defines the checkpoint scaling thresholds for
 *   rejection control algorithm. See:
 *
 * Liu, Jun S., Rong Chen, and Wing Hung Wong. "Rejection control and
 *  sequential importance sampling." Journal of the American Statistical
 *  Association 93.443 (1998): 1022-1031.
 *
 * A sample passes rejection control checkpoints with probability:
 *
 *  P = min(1, w_i / scale) where w_i is weight of i-th sample.
 *
 * This mixin defines the scale denominator for checkpoint culling.
 */

#ifndef uSisInterWeightsScaleRcMixin_h
#define uSisInterWeightsScaleRcMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uIsFinite.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uTypes.h"

/**
 * Based on empirical formula given in Liu (1998), computes:
 *  scale = c_min * min_weight + c_mean * mean_weight + c_max * max_weight
 *
 * where c_min + c_mean + c_max = 1 and all are >= 0
 *
 * This introduces a "negligible" amount of bias which is reduced with larger
 * sample sizes.
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisInterpWeightsScaleRcMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisInterpWeightsScaleRcMixin() { clear(); }

    /**
     * Initializes scale mixin
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
        // Check if options exists in config
        config->read_into<uReal>(this->c_min,
                                 uOpt_rc_scale_interp_weights_c_min);
        config->read_into<uReal>(this->c_mean,
                                 uOpt_rc_scale_interp_weights_c_mean);
        config->read_into<uReal>(this->c_max,
                                 uOpt_rc_scale_interp_weights_c_max);
        uAssert(check_coeffs());
    }

    /**
     * Resets to default state
     */
    inline void clear() {
        this->c_min = this->c_mean = this->c_max = U_TO_REAL(0.0);
    }

    /**
     * Determines the scale used to determine if a sample should be regrown
     * @param norm_exp_weights - the vector of exp transformed weights
     *   normalized by the max weight - should be in (0,1]
     * @param max_log_weight - the unnormalized maximal log weight
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin (must be a rejection
     *   control mixin)
     * @param sim - parent simulation
     * @return scale factor for accept/reject probability = min(1, weight/scale)
     */
    inline uReal rc_get_scale(const uVecCol& norm_exp_weights,
                              const uReal max_log_weight,
                              const uUInt num_completed_grow_steps_per_sample,
                              const sim_level_qc_mixin_t& qc,
                              const sim_t& sim) {
        uAssert(check_coeffs());
        uAssert(norm_exp_weights.n_elem > 0);

        // Compute min, mean, and max normed exp weight
        uReal weight = norm_exp_weights.at(0);
        uReal min_ = weight;
        uReal mean_ = weight;
        uReal max_ = weight;  // Note: this should probably always be 1.0 as
                              // all weights are scaled by the max weight so
                              // largest weight will be 1.0

        const uMatSz_t n_weights = norm_exp_weights.n_elem;
        for (uMatSz_t i = 1; i < n_weights; ++i) {
            weight = norm_exp_weights.at(i);
            uAssert(uIsFinite(weight));
            min_ = (weight < min_) ? weight : min_;
            max_ = (weight > max_) ? weight : max_;
            mean_ += weight;
        }

        mean_ /= U_TO_REAL(n_weights);

        // Compute scale factor as weighted average
        const uReal scale = (this->c_min * min_) + (this->c_mean * mean_) +
                            (this->c_max * max_);

        return scale;
    }

private:
    /**
     * Asserts that weights are non-negative and sum to 1
     */
    inline uBool check_coeffs() {
        // We require that the coefficients sum to 1.0
        uAssertRealEq(c_min + c_mean + c_max, U_TO_REAL(1.0));
        // We require that coefficients are non-negative
        uAssert(c_min >= U_TO_REAL(0.0));
        uAssert(c_mean >= U_TO_REAL(0.0));
        uAssert(c_max >= U_TO_REAL(0.0));
        return uTRUE;
    }

    /**
     * The [0,1] coefficient for the minimum weight
     */
    uReal c_min;

    /**
     * The [0,1] coefficient for the mean weight
     */
    uReal c_mean;

    /**
     * The [0,1] coefficient for the maximum weight
     */
    uReal c_max;
};

#endif  // uSisInterWeightsScaleRcMixin_h
