//****************************************************************************
// uTestEbrTuner.h
//****************************************************************************

/**
 * Commandlet for testing energy bend rigidity (ebr) tuner
 *
 * Usage:
 * -test_ebr_tuner
 *      [--conf <path>] [--ebr_tuner_kuhn_bp <+float>]
 *      [--ebr_tuner_node_bp_fpath <path>] [--ebr_tuner_config_fpath <path>]
 *      [--ebr_tuner_out_fpath <path>]
 *      [--energy_hetr_bend_rigidity_fpath <path>]
 *
 * or, if default settings are okay to use, then simply:
 * -test_ebr_tuner
 */

#ifdef uTestEbrTuner_h
#   error "Test Ebr Tuner included multiple times!"
#endif  // uTestEbrTuner_h
#define uTestEbrTuner_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uMockUpUtils.h"
#include "uSisSeoBendEnergyRigidityTuner.h"

/**
 * Entry point for test
 */
int uTestEbrTunerMain(const uCmdOptsMap& cmd_opts) {
    // Typedefs
    typedef uMockUpUtils::uSisBendSeoMlocHetrNullGlue::sim_t sim_t;
    typedef uSisSeoBendEnergyRigidityTuner<typename sim_t::glue_t> tuner_t;

    // Copy command line options so that we can modify if necessary
    uCmdOptsMap ebr_cmd_opts(cmd_opts);

    // Default configuration path
    const std::string DEFAULT_CONF_PATH = "../tests/test_ebr_tuner/sim.ini";
    // Check if configuration path exists
    const std::string conf_cmd_key(uOpt_get_cmd_switch(uOpt_conf));
    ebr_cmd_opts.set_option_if_absent(conf_cmd_key, DEFAULT_CONF_PATH);

    // Export final simulation samples
    const std::string export_pml_key(
        uOpt_get_cmd_switch(uOpt_export_pml_unary));
    ebr_cmd_opts.set_option_if_absent(export_pml_key, "");

    // Tune energy bend rigidities
    tuner_t t;
    return t.tune(ebr_cmd_opts);
}

#endif  // U_BUILD_ENABLE_TESTS
