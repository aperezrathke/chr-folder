//****************************************************************************
// uTestSisHomgUnifLagQcSchedule.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * with resampling quality control and homogeneous, uniform lag schedule
 *
 * Usage:
 * -test_sis_homg_unif_lag_qc_sched
 */

#ifdef uTestSisHomgUnifLagQcSchedule_h
#   error "Test Sis Homg Unif Lag Qc Sched included multiple times!"
#endif  // uTestSisHomgUnifLagQcSchedule_h
#define uTestSisHomgUnifLagQcSchedule_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Resampling mixin
#include "uSisHomgUnifLagScheduleMixin.h"
#include "uSisPowerSamplerRsMixin.h"
#include "uSisResamplingQcSimLevelMixin.h"

// Use null QC as a control
#include "uMockUpUtils.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"

#include <iostream>
#include <string>

namespace {
/**
 * Now wire together a resampling simulation
 */
class uTestSisHomgUnifLagRsQcGlue {
public:
    // Simulation
    typedef uSisSim<uTestSisHomgUnifLagRsQcGlue> sim_t;
    // Sample
    typedef uSisSample<uTestSisHomgUnifLagRsQcGlue> sample_t;

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisHomgUnifLagRsQcGlue,
        uSisParallelBatchGrower<uTestSisHomgUnifLagRsQcGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisResamplingQcSimLevelMixin<
        uTestSisHomgUnifLagRsQcGlue,
        uSisHomgUnifLagScheduleMixin<uTestSisHomgUnifLagRsQcGlue,
                                     uOpt_qc_schedule_lag_slot0>,
        uSisPowerSamplerRsMixin<uTestSisHomgUnifLagRsQcGlue,
                                uOpt_qc_sampler_power_alpha_slot0>,
        true>  // enable heartbeat
        sim_level_qc_mixin_t;

    // Simulation level node radius mixin
    typedef uSisHomgNodeRadiusSimLevelMixin<uTestSisHomgUnifLagRsQcGlue>
        sim_level_node_radius_mixin_t;
    // Sample level node radius mixin
    typedef uSisHomgNodeRadiusMixin<uTestSisHomgUnifLagRsQcGlue>
        node_radius_mixin_t;
    // Simulation level growth mixin
    typedef uSisSeoSloc::GrowthSimLevelMixin_threaded<
        uTestSisHomgUnifLagRsQcGlue>
        sim_level_growth_mixin_t;
    // Sample level growth mixin
    typedef uSisSeoSloc::GrowthMixin<uTestSisHomgUnifLagRsQcGlue>
        growth_mixin_t;
    // Simulation level nuclear mixin
    typedef uSisSphereNuclearSimLevelMixin_threaded<uTestSisHomgUnifLagRsQcGlue>
        sim_level_nuclear_mixin_t;
    // Sample level nuclear mixin
    typedef uSisSphereNuclearMixin<uTestSisHomgUnifLagRsQcGlue> nuclear_mixin_t;
    // Simulation level collision mixin
    typedef uSisHardShellCollisionSimLevelMixin_threaded<
        uTestSisHomgUnifLagRsQcGlue>
        sim_level_collision_mixin_t;
    // Sample level collision mixin
    typedef uSisHardShellCollisionMixin<uTestSisHomgUnifLagRsQcGlue,
                                        uBroadPhase_t>
        collision_mixin_t;
    // Simulation level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrSimLevelMixin<uTestSisHomgUnifLagRsQcGlue>
        sim_level_intr_chr_mixin_t;
    // Sample level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrMixin<uTestSisHomgUnifLagRsQcGlue> intr_chr_mixin_t;
    // Simulation level lamina interaction mixin
    typedef uSisNullIntrLamSimLevelMixin<uTestSisHomgUnifLagRsQcGlue>
        sim_level_intr_lam_mixin_t;
    // Sample level lamina interaction mixin
    typedef uSisNullIntrLamMixin<uTestSisHomgUnifLagRsQcGlue> intr_lam_mixin_t;
    // Simulation level nuclear body interaction mixin
    typedef uSisNullIntrNucbSimLevelMixin<uTestSisHomgUnifLagRsQcGlue>
        sim_level_intr_nucb_mixin_t;
    // Sample level nuclear body interaction mixin
    typedef uSisNullIntrNucbMixin<uTestSisHomgUnifLagRsQcGlue>
        intr_nucb_mixin_t;
    // Simulation level energy mixin
    typedef uSisUnifEnergySimLevelMixin<uTestSisHomgUnifLagRsQcGlue>
        sim_level_energy_mixin_t;
    // Sample level energy mixin
    typedef uSisUnifEnergyMixin<uTestSisHomgUnifLagRsQcGlue> energy_mixin_t;
    // Seed mixin
    typedef uSisSeoUnifCubeSeedMixin<uTestSisHomgUnifLagRsQcGlue> seed_mixin_t;
};

}  // end of anonymous namespace

/**
 * Entry point for test
 * sis - sequential importance sampling
 * homg = homogeneous
 * unif lag = uniform sampling within lag intervals
 * qc = quality control
 * sched = schedule
 */
int uTestSisHomgUnifLagQcSchedMain(const uCmdOptsMap& cmd_opts) {
    // Expose rs qc simulation
    typedef uTestSisHomgUnifLagRsQcGlue::sim_t rs_sim_t;

    // Expose null qc simulation as control
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t null_sim_t;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 3;
    config->ensemble_size = 1000;
    config->set_option(uOpt_max_node_diameter, 300);
    config->set_option(uOpt_nuclear_diameter, 9000);
    config->num_nodes = std::vector<uUInt>(1 /* n_chains*/, 1000 /* n_nodes*/);
    config->num_unit_sphere_sample_points = 50;

    // Configure rs schedule (dynamic schedule)
    // See uSisHomgLagScheduleMixin for a deterministic schedule
    config->set_option(uOpt_qc_schedule_type_slot0, "homg_unif_lag");
    config->set_option(uOpt_qc_schedule_lag_slot0, 50);

    // Configure sampler
    config->set_option(uOpt_rs_sampler_type_slot0, "power");
    config->set_option(uOpt_qc_sampler_power_alpha_slot0, 1.0);

    // Output configuration
    std::cout << "Running Test Sis Homg Unif Lag Qc Schedule." << std::endl;
    config->print();

    // Initialize globals
    uG::init(config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    rs_sim_t rs_sim(config U_MAIN_THREAD_ID_ARG);

    // Do resampling simulation!
    const uBool rs_result = rs_sim.run();

    // Compute resampling effective sample size
    const uReal rs_ess = uSisUtilsQc::calc_ess_cv_from_log(
        rs_sim.get_completed_log_weights_view());

    std::cout << "Rs simulation completed with status: " << rs_result
              << std::endl;
    std::cout << "Rs effective sample size: " << rs_ess << std::endl;

    uSisUtilsQc::report_percent_unique_monomers(rs_sim.get_completed_samples());

    // Run control simulation
    null_sim_t null_sim(config U_MAIN_THREAD_ID_ARG);

    // Do control simulation!
    const uBool null_result = null_sim.run();

    // Compute null effective sample size
    const uReal null_ess = uSisUtilsQc::calc_ess_cv_from_log(
        null_sim.get_completed_log_weights_view());

    std::cout << "Null simulation completed with status: " << null_result
              << std::endl;
    std::cout << "Null effective sample size: " << null_ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
