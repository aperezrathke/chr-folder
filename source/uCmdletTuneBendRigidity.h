//****************************************************************************
// uCmdletTuneBendRigidity.h
//****************************************************************************

/**
 * Commandlet for tuning the bending rigidity of the bending energy function
 * such that Kuhn length matches empirical results.
 *
 * Usage:
 * -cmdlet_tune_bend_rigidity <args>
 *
 * See "uSisSeoBendEnergyRigidityTuner.h" for <args> expected by tuner.
 */

#ifdef uCmdletTuneBendRigidity_h
#   error "Commandlet Tune Bend Rigidity included multiple times!"
#endif  // uCmdletTuneBendRigidity_h
#define uCmdletTuneBendRigidity_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uCmdOptsMap.h"
#include "uLogf.h"
#include "uMockUpUtils.h"
#include "uSisSeoBendEnergyRigidityTuner.h"

/**
 * Entry point for commandlet
 */
int uCmdletTuneBendRigidityMain(const uCmdOptsMap& cmd_opts) {
    uLogf("Running Commandlet Tune Bend Rigidity.\n");
    // Expose final simulation
    // @TODO - make this user configurable!
    typedef uMockUpUtils::uSisBendSeoMlocHetrNullGlue::sim_t sim_t;
    uSisSeoBendEnergyRigidityTuner<typename sim_t::glue_t> t;
    return t.tune(cmd_opts);
}

#endif  // U_BUILD_ENABLE_COMMANDLETS
