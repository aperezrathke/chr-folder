//****************************************************************************
// uSisSeGrowthSimLevelMixin.inl
//****************************************************************************

/**
 * WARNING - DO NOT INCLUDE THIS FILE DIRECTLY! Use:
 *  #include uSisSeGrowthSimLevelMixin.h
 * instead. YOU HAVE BEEN WARNED!
 */

/**
 * Mixin stores pre-allocated scratch buffers used for single end growth
 * Sis = sequential importance sampling
 * Se = Single-end growth is from only one end of chain
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisSeGrowthSimLevelMixin_threaded
#else
class uSisSeGrowthSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
    : public uSisNullGrowthSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // This assumes sphere sample points have been initialized!
        const uMatrix& unit_sphere_sample_points =
            sim.get_unit_sphere_sample_points();

        // Assuming each row is a sample position
        uAssert(unit_sphere_sample_points.n_cols == uDim_num);

        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(this->candidate_positions,
                                         zeros,
                                         unit_sphere_sample_points.n_rows,
                                         unit_sphere_sample_points.n_cols);

        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            this->legal_candidates, zeros, unit_sphere_sample_points.n_rows);
    }

    /**
     * Resets to default state
     */
    inline void clear() {
        this->candidate_positions.clear();
        this->legal_candidates.clear();
    }

    // Accessors

    inline uMatrix& get_candidate_positions(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(this->candidate_positions);
    }

    inline uBoolVecCol& get_legal_candidates(U_THREAD_ID_0_PARAM) const {
        U_ACCESS_TLS_DATA(return this->legal_candidates);
    }

private:
    // Declaring mutable because parent level simulation is likely
    // to be declared as const

    /**
     * Stores 1 (TRUE) if element satisfies all constraints (e.g. nuclear
     * and collision) and 0 (FALSE) otherwise.
     */
    mutable U_DECLARE_TLS_DATA(uBoolVecCol, legal_candidates);

    /**
     * Buffer to store scaled, translated from node center candidate positions
     */
    mutable U_DECLARE_TLS_DATA(uMatrix, candidate_positions);
};
