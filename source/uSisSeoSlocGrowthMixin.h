//****************************************************************************
// uSisSeoSlocGrowthMixin.h
//****************************************************************************

/**
 * Growth mixin for a single locus which always grows from first to last node
 */

#ifndef uSisSeoSlocGrowthMixin_h
#define uSisSeoSlocGrowthMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uRand.h"
#include "uSisSeGrowthSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uTypes.h"

//****************************************************************************
// Sim-level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSeoSlocGrowthSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisSeoSlocGrowthSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

//****************************************************************************
// Sample-level mixin
//****************************************************************************

/**
 * Sis = Sequential importance sampling
 * Seo = Single-end, ordered growth is from only one end of chain. Ordered =
 *  nodes aregrown from first to last node always
 * Sloc = Single-locus, built for only single locus chain
 */
namespace uSisSeoSloc {
/**
 * Mixin encapsulating sample specific data and routines
 */
template <typename t_SisGlue>
class GrowthMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Allow compile time branching based on single locus (sloc) vs
     * multi-loci (mloc) growth
     */
    enum { is_mloc_growth = false };

    /**
     * Initializes growth mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Initializes first node positions
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current log weight of this sample
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool seed(sample_t& sample,
                     uReal& log_weight,
                     const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(log_weight == U_TO_REAL(0.0));
        // Ordered mixin always starts growing from 0th node
        sim.seed_core(sample,
                      U_TO_UINT(0), /*nid*/
                      U_TO_UINT(0), /*lid*/
                      sample.get_candidate_node_radius(0 /*node_id*/, sim),
                      uSisUtils::SeedFirstClashPolicy(),
                      sample.get_intr_chr_seed_first_policy(),
                      sim U_THREAD_ID_ARG);
        return uSisUtils::will_still_need_growing(1,  // num_grown_nodes
                                                  sim);
    }

    /**
     * Performs incremental locus chain growth directly after 1st growth phase
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current and output log weight of the sample
     * @param num_completed_grow_steps - The number of growth calls (seed() +
     *  grow_*()) that have completed. This value is reset between trials.
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool grow_2nd(sample_t& sample,
                         uReal& log_weight,
                         const uUInt num_completed_grow_steps,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps == 1);
        uAssert(this->get_num_grown_nodes(
                    num_completed_grow_steps, sample, sim) == 1);
        return grow_after_1st<true /*should_grow_2nd*/>(sample,
                                                        log_weight,
                                                        1,  // num_grown_nodes
                                                        0,  // parent_node_id
                                                        sim U_THREAD_ID_ARG);
    }

    /**
     * Performs incremental locus chain growth after 1st and 2nd growth phases
     * @param sample - parent sample containing this mixin
     * @param log_weight - the current and output log weight of the sample
     * @param num_completed_grow_steps - The number of growth calls (seed() +
     *  grow_*()) that have completed. This value is reset between trials.
     * @param sim - parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    inline bool grow_nth(sample_t& sample,
                         uReal& log_weight,
                         const uUInt num_completed_grow_steps,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps >= 2);
        const uUInt num_grown_nodes =
            this->get_num_grown_nodes(num_completed_grow_steps, sample, sim);
        return grow_after_1st<false /*should_grow_2nd*/>(
            sample,
            log_weight,
            num_grown_nodes,      // num_grown_nodes
            num_grown_nodes - 1,  // parent_node_id
            sim U_THREAD_ID_ARG);
    }

    /**
     * @param num_completed_grow_steps - The number of growth
     *  calls (seed() + growth_step()) that have completed. This value is
     *  reset between trials.
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @return Number of nodes currently grown by sample
     */
    inline uUInt get_num_grown_nodes(const uUInt num_completed_grow_steps,
                                     const sample_t& sample,
                                     const sim_t& sim) const {
        // Defer to simulation level mixin
        return sim.get_num_grown_nodes(num_completed_grow_steps);
    }

    /**
     * @HACK - For proximity (knock-in) interaction testing between different
     *  loci, we need to be able to query how many nodes have already been
     *  grown at each locus. To compile with locus agnostic code, we need to
     *  provide this interface even if it should probably never be called at
     *  runtime!
     * @param lid - [UNUSED] locus identifier
     * @param num_completed_grow_steps - The number of growth
     *  calls (seed() + growth_step()) that have completed. This value is
     *  reset between trials.
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     */
    inline uUInt get_num_grown_nodes_at_locus(
        const uUInt lid,
        const uUInt num_completed_grow_steps,
        const sample_t& sample,
        const sim_t& sim) const {
        uAssert(lid == U_TO_UINT(0));
        return get_num_grown_nodes(num_completed_grow_steps, sample, sim);
    }

    /**
     * Performs brute-force narrow-phase collisions and other structural
     * checks to make sure chain growth is proceeding as expected.
     * THIS CODE SHOULD NEVER BE CALLED IN RELEASE! IT IS DEBUGGING CODE
     * TO VALIDATE CORRECTNESS OF GROWTH ALGORITHMS.
     * @param num_grown_nodes - number of successfully grown nodes
     * @param sample - the sample to check
     * @param sim - the parent simulation
     * @return TRUE if all collision constraints satisfied, FALSE o/w
     */
    uBool check_growth(const uUInt num_grown_nodes,
                       const sample_t& sample,
                       const sim_t& sim) const {
        // This is a very expensive check, so only perform if enabled.
#ifndef U_BUILD_ENABLE_ROBUST_CONSTRAINT_CHECKS
        return uTRUE;
#endif
        uAssertBoundsInc(num_grown_nodes, 0, sim.get_max_total_num_nodes());
        uAssertPosEq(sample.get_node_positions().n_cols,
                     sim.get_max_total_num_nodes());
        uAssert(sample.get_node_positions().n_rows == uDim_num);
        uBitset active_nodes(sample.get_node_positions().n_cols);
        for (uUInt i_node = 0; i_node < num_grown_nodes; ++i_node) {
            active_nodes.set(i_node);
        }
        return uSisUtils::satisfies_robust_constraint_checks(
            active_nodes, sample, sim);
    }

private:
    /**
     * Performs incremental locus chain growth. If growing 2nd node in locus,
     * the template parameter "should_grow_2nd" must be true and false o/w.
     * @param sample - Parent sample containing this mixin
     * @param log_weight - The current and output log weight of the sample
     * @param num_grown_nodes - Number of successfully grown nodes
     * @param parent_node_id - Identifier of parent node from which to place
     *  candidate
     * @param sim - Parent simulation containing global sample data
     * @return TRUE if sample needs to continue growing, FALSE if node is
     * done growing (either because of completion or because it's dead)
     */
    template <bool should_grow_2nd>
    static inline bool grow_after_1st(sample_t& sample,
                                      uReal& log_weight,
                                      const uUInt num_grown_nodes,
                                      const uUInt parent_node_id,
                                      const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(uSisUtils::will_still_need_growing(num_grown_nodes, sim));
        const bool is_alive =
            uSisUtils::grow_after_1st_seo<sample_t, sim_t, should_grow_2nd>(
                sample,
                log_weight,
                num_grown_nodes,
                parent_node_id,
                sim U_THREAD_ID_ARG);
        return is_alive &&
               uSisUtils::will_still_need_growing(num_grown_nodes + 1, sim);
    }
};

}  // namespace uSisSeoSloc

#endif  // uSisSeoSlocGrowthMixin_h
