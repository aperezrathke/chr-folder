//****************************************************************************
// uTestHash.h
//****************************************************************************

/**
 * Commandlet for testing the hash interface
 *
 * Usage:
 * -test_hash
 */

#ifdef uTestHash_h
#   error "Test Hash included multiple times!"
#endif  // uTestHash_h
#define uTestHash_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uAssert.h"
#include "uCmdOptsMap.h"
#include "uExitCodes.h"
#include "uHashMap.h"
#include "uTypes.h"

#include <iostream>
#include <limits>

/**
 * Entry point for test
 */
int uTestHashMain(const uCmdOptsMap& cmd_opts) {
    uUIntToUIntMap hash_map;
    typedef uUIntToUIntMap::key_type key_t;
    typedef uUIntToUIntMap::value_type::second_type val_t;

    std::cout << "Running Test Hash." << std::endl;

    // Don't ever use these keys!
    // Note: for broad phase - could probably set these to be
    // std::numeric_limits and std::numeric_limits - 1 for the key type
    U_HASH_SET_EMPTY_KEY(hash_map, 0);
    U_HASH_SET_DELETED_KEY(hash_map, 1);

    const uUInt MIN_SIZE = 12000;
    U_HASH_RESERVE(hash_map, MIN_SIZE);
    std::cout << "Starting bucket count: " << hash_map.bucket_count()
              << std::endl;
    uAssert(hash_map.bucket_count() >= MIN_SIZE);

    const key_t KEYS[] = {
        2,
        4,
        101,
        0xDEADBEEF,
        std::numeric_limits<uUIntToUIntMap::key_type>::max(),
        std::numeric_limits<uUIntToUIntMap::key_type>::max() - 1};

    const val_t VALS[] = {100, 200, 5000, 7331, 0xDEADBEEF, 0x8BADF00D};

    const uUInt N = sizeof(KEYS) / sizeof(key_t);
    uAssert((sizeof(VALS) / sizeof(val_t)) == N);

    uUInt i = 0;
    for (i = 0; i < N; ++i) {
        hash_map[KEYS[i]] = VALS[i];
        std::cout << "Inserted (" << KEYS[i] << ", " << hash_map[KEYS[i]] << ")"
                  << std::endl;
    }

    for (i = 0; i < N; ++i) {
        uAssert(hash_map.find(KEYS[i]) != hash_map.end());
    }

    uAssert(hash_map.find(0x1CEB00DA) == hash_map.end());

    std::cout << "Ending bucket count: " << hash_map.bucket_count()
              << std::endl;
    uAssert(hash_map.bucket_count() >= MIN_SIZE);

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
