//****************************************************************************
// uSisQcSummaryMedianWeight.h
//****************************************************************************

#ifndef uSisQcSummaryMedianWeight_h
#define uSisQcSummaryMedianWeight_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uTypes.h"

/**
 * Summarizes the log weights by the median weight (and then log of median)
 */
class uSisQcSummaryMedianWeight {
public:
    /**
     * Summarizes log weight vector: computes median of weights, then log
     *  transforms the median weight
     * @param log_weights - Log weight at each sample (all weights are assumed
     *  to be valid)
     * @return Scalar summary of completed log weights, in this case the
     *  median of the weights is computed and then returned in log space
     */
    static uReal summarize(const uVecCol& log_weights) {
        // If empty, return dead sample log weight
        if (log_weights.is_empty()) {
            return U_SIS_DEAD_SAMPLE_LOG_WEIGHT;
        }
        // Else, compute log(mean weight)
        uAssert(log_weights.size() >= 1);
        // Compute log(mean weight)
        uVecCol norm_exp_weights(log_weights.n_elem);
        return uMatrixUtils::median(log_weights);
    }
};

#endif  // uSisQcSummaryMedianWeight_h
