//****************************************************************************
// uSisSubSimAccess
//****************************************************************************

#ifndef uSisSubSimAccess_h
#define uSisSubSimAccess_h

#include "uBuild.h"

/**
 * For a serial landmark trial runner:
 *  Set template parameter t_LmrkSubSimAccess = uSisSubSimAccess_serial<t>
 *
 * For a full TLS landmark trial runner:
 *  Set template parameter t_LmrkSubSimAccess = uSisSubSimAccess_threaded<t>
 *
 * where t = sub-simulation type
 */

 /**
  * Apologies for the use of ugly macros in order to define multiple class
  * definitions. The multiple class definitions are necessary to maintain DRY
  * principle while still having both serial and threaded implementations of
  * which both can function in a threaded or non-threaded environment.
  *
  * This is mostly useful for having a serial implementation in a multi-
  * threaded environment that can still forward to the proper thread-specific
  * random number generator. In turn, the serial implementation is assumed
  * to be run only on a single thread and therefore does not incur the same
  * level of overhead as the full threaded TLS class implementation.
  */

  // Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSubSimAccess.inl"
#undef U_INJECT_THREADED_TLS

#ifdef U_BUILD_ENABLE_THREADS
    // Disable TLS
#   include "uThreadNullTLS.inl"
    // Define implementation with no TLS support but can still function in a
    // threaded environment if needed.
#   include "uSisSubSimAccess.inl"
    // Restore thread support if enabled
#   include "uThreadTLS.inl"
#else
    // No need to redefine class, just 'typedef' the template class
#   define uSisSubSimAccess_serial uSisSubSimAccess_threaded
#endif  // U_BUILD_ENABLE_THREADS

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif // uSisSubSimAccess_h
