//****************************************************************************
// uSelectorTower.h
//****************************************************************************

/**
 * @brief - Samples a vector element according to the discrete probability
 *  distribution defined by the vector
 */

#ifndef uSelectorTower_h
#define uSelectorTower_h

#include "uBuild.h"
#include "uAssert.h"
#include "uRand.h"
#include "uTypes.h"

/**
 * Basic idea:
 *  1) Compute CDF of empirical distribution
 *  2) Generate a uniform random variable U in [0,1)
 *  3) Select first element with CDF <= U
 */
class uSelectorTower {
public:
    /**
     * @param p_select - a vector of probabilities - must sum to 1.0 and must
     *  have at least 1 element
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] of being chosen
     */
    static inline uUInt rand_select_index(const uVecCol& p_select, uRand& rng) {
        const uUInt n_choices = U_TO_UINT(p_select.n_elem);
        uAssertRealEq(uMatrixUtils::sum(p_select), U_TO_REAL(1.0));
        uAssert(n_choices > 0);

        // Uniformly select a real number in [0, 1)
        const uReal r_choice = rng.unif_real();
        uAssert(r_choice <= uMatrixUtils::sum(p_select));

        // Used to identify which bucket r_choice falls within
        uReal p_sum = U_TO_REAL(0.0);

        // Determine which index to select
        for (uUInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        uAssert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }

    /**
     * @param p_select - a vector of possibly unnormalized probabilities - must
     *  sum to 'z' and must have at least 1 element
     * @param z - the normalizing constant = sum(p_select)
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] / z of being chosen
     */
    static inline uUInt rand_select_index(const uVecCol& p_select,
                                          const uReal z,
                                          uRand& rng) {
        const uUInt n_choices = U_TO_UINT(p_select.n_elem);
        uAssertRealEq(uMatrixUtils::sum(p_select), z);
        uAssert(n_choices > 0);

        // Uniformly select a real number in [0, z)
        const uReal r_choice = rng.unif_real(0, z);
        uAssert(r_choice <= z);

        // Used to identify which bucket r_choice falls within
        uReal p_sum = U_TO_REAL(0.0);

        // Determine which index to select
        for (uUInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        uAssert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }
};

#endif  // uSelectorTower_h
