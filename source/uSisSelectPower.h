//****************************************************************************
// uSisSelectPower.h
//****************************************************************************

#ifndef uSisSelectPower_h
#define uSisSelectPower_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

/**
 * Selects a single sample among a set of samples with probability
 * proportional to its relative weight raised to a user-defined power (alpha)
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing from configuration file
    // e.g uOpt_qc_sampler_power_alpha_slot0
    enum uOptE e_Key>
class uSisSelectPower {
public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    // Interface expected by landmark trial runner

    /**
     * Initializes sampler from config object
     * @param sim - parent simulation
     * @param cfg - configuration object specifying simulation parameters
     */
    void init(const sim_t& sim, uSpConstConfig_t cfg U_THREAD_ID_PARAM) {
        clear();
        // Check if option exists in config
        cfg->read_into(this->alpha, e_Key);
        if (this->alpha < U_TO_REAL(0.0)) {
            uLogf("Error: power selection exponent alpha %f < 0. Exiting.\n",
                  (double)this->alpha);
            exit(uExitCode_error);
        }
        // We require that our threshold value be non-negative
        uAssert(this->alpha >= U_TO_REAL(0.0));
    }

    /**
     * Resets to default state
     */
    void clear() { this->alpha = U_SIS_QC_SAMPLER_POWER_DEFAULT_ALPHA; }

    /**
     * Does nothing
     */
    void reset_for_next_trial(const sim_t&,
                              uSpConstConfig_t U_THREAD_ID_PARAM) {}

    /**
     * Selects among a set of samples with probability proportional to
     * the relative weight raised to a power alpha
     * @param log_weights - log weight at each sample (all weights are assumed
     *  to be valid)
     * @param samples_unused - set of samples to select from
     * @param completed_grow_steps_unused - Number of completed grow steps for
     *  each sample in 'samples' set
     * @param log_p_select - the output log of the selection probability
     * @return Unsigned integer index of selected sample
     */
    template <typename t_sample>
    inline uUInt select_index(const uVecCol& log_weights,
                              const std::vector<t_sample>& samples_unused,
                              const uUInt completed_grow_steps_unused,
                              uReal& log_p_select U_THREAD_ID_PARAM) {
        uAssert(log_weights.size() >= 1);

        // Compute normalized weights
        uReal max_log_weight_unused;
        uVecCol norm_exp_weights(log_weights.n_elem);
        uSisUtilsQc::calc_norm_exp_weights(
            norm_exp_weights, max_log_weight_unused, log_weights);
        uAssertPosEq(norm_exp_weights.n_elem, log_weights.n_elem);

        // Compute smoothed probabilities by raising weights to the alpha
        // power. Samples are chosen based on this smoothed weight.
        uVecCol smoothed_probabilities;
        uSisUtilsQc::calc_power_smoothed_probabilities(
            smoothed_probabilities, norm_exp_weights, this->alpha);
        uAssertPosEq(smoothed_probabilities.n_elem, log_weights.n_elem);

        // Select a random sample
        const uUInt i_keep = uSisUtilsQc::rand_select_index(
            smoothed_probabilities U_THREAD_ID_ARG);
        uAssertBounds(i_keep, 0, log_weights.n_elem);

        // Compute log selection probability
        log_p_select = log(smoothed_probabilities.at(i_keep));

        // Return selected index
        return i_keep;
    }

private:
    /**
     * All sample weights are raised to this power. Each sample then has
     * probability proportional to raised weight of being selected. For values
     * of alpha < 1.0, this has the effect of smoothing out the weight
     * distribution and (ideally) leading to more diversity. For value of
     * alpha > 1.0, this has the effect of boosting the highest weights
     * (and less diversity).
     */
    uReal alpha;
};

#endif  // uSisSelectPower_h
