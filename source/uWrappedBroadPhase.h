//****************************************************************************
// uWrappedBroadPhase.h
//***************************************************************************

#ifndef uWrappedBroadPhase_h
#define uWrappedBroadPhase_h

#include "uBuild.h"
#include "uTypes.h"

/**
 * Provides simplified interface for broad phase filtering
 */
template <typename broad_phase_t>
class uWrappedBroadPhase {
public:
    /**
     * @param in_broad_phase - underlying broad phase structure
     *  to redirect calls to
     * @param in_cell_length - the length of a grid cell
     * @param in_grid_radius - the radius of the broad phase grid
     */
    inline uWrappedBroadPhase(const broad_phase_t& in_broad_phase,
                              const uReal in_cell_length,
                              const uReal in_grid_radius)
        : broad_phase(in_broad_phase),
          cell_length(in_cell_length),
          grid_radius(in_grid_radius) {
        uAssert(cell_length > U_TO_REAL(0.0));
        uAssert(grid_radius > U_TO_REAL(0.0));
    }

    /**
     * Performs broad phase filtering of an element with
     * 3-d center at p_center and with parameter radius
     * @param out_results - bitset where element is true if that
     *  element needs narrow phase collision checking, false o/w
     * @param p_center - 3-d center element position to check
     * @param radius - node radius
     */
    template <typename t_bitset>
    void filter(t_bitset& out_results,
                const uReal* const p_center,
                const uReal radius) const {
        uAssert(NULL != p_center);
        // Broad phase: Spatial subdivision acceleration to avoid O(n^2)
        // collisions
        return broad_phase.template filter<t_bitset>(
            out_results, p_center, radius, cell_length, grid_radius);
    }

private:
    /**
     * Underlying broad phase implementation
     */
    const broad_phase_t& broad_phase;

    /**
     * The length of a uniform grid cell
     */
    const uReal cell_length;

    /**
     * The max positive grid coordinate
     */
    const uReal grid_radius;
};

#endif  // uWrappedBroadPhase_h
