//****************************************************************************
// uThreadNullTLS.inl
//****************************************************************************

/**
 * WARNING: OVERWRITES THREAD LOCAL STORAGE (TLS) MACROS TO NULL
 * IMPLEMENTATION.
 *
 * This can be useful if you want to a serial implementation of a class that
 * will only ever run on a single thread but in a multi-threaded environment.
 *
 * MAKE SURE YOU KNOW WHAT YOU ARE DOING WHEN INCLUDING THIS FILE AS THESE
 * MACROS WILL OVERWRITE ANY COMMON DEFINES WITHIN uThread.h!!!
 */

/**
 * Undefine any previously existing macros
 */

#ifdef U_DECLARE_TLS_DATA
#   undef U_DECLARE_TLS_DATA
#endif  // U_DECLARE_TLS_DATA

#ifdef U_ACCESS_TLS_DATA
#   undef U_ACCESS_TLS_DATA
#endif  // U_ACCESS_TLS_DATA

#ifdef U_INIT_TLS_DATA_0_PARAM
#   undef U_INIT_TLS_DATA_0_PARAM
#endif  // U_INIT_TLS_DATA_0_PARAM

#ifdef U_INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef U_INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // U_INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef U_INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef U_INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // U_INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef U_TLS_DATA_FOR_EACH_0_PARAM
#   undef U_TLS_DATA_FOR_EACH_0_PARAM
#endif  // U_TLS_DATA_FOR_EACH_0_PARAM

#ifdef U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB
#   undef U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB
#endif  // U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB

#ifdef U_TLS_DATA_FOR_EACH_1_PARAM
#   undef U_TLS_DATA_FOR_EACH_1_PARAM
#endif  // U_TLS_DATA_FOR_EACH_1_PARAM

/**
 * Redefine macros
 */

#define U_DECLARE_TLS_DATA(type, name) type name

#define U_ACCESS_TLS_DATA(name) name

#define U_INIT_TLS_DATA_0_PARAM(name) ((void)0)

#define U_INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    name.memb_func(param_1)

#define U_INIT_TLS_DATA_FOR_EACH_2_PARAM(name, memb_func, param_1, param_2) \
    name.memb_func(param_1, param_2)

#define U_TLS_DATA_FOR_EACH_0_PARAM(name, memb_func) name.memb_func()

#define U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(name, nonmemb_func) \
    nonmemb_func(name)

#define U_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    name.memb_func(param_1)
