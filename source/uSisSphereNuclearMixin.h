//****************************************************************************
// uSisSphereNuclearMixin.h
//****************************************************************************

/**
 * A nuclear mixin manages the details for checking candidate nodes to see if
 * they lie within the nuclear volume; this mixing enforces a spherical
 * nuclear confinement.
 *
 * An accessory structure is the simulation level nuclear mixin which defines
 * global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 */

#ifndef uSisSphereNuclearMixin_h
#define uSisSphereNuclearMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisSphereNuclearSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers.
 */
template <typename t_SisGlue>
class uSisSphereNuclearMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag spherical nucleus
     */
    enum { is_sphere_nucleus = true };

    /**
     * Initializes nuclear constraint mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Require simulation mixin is also spherical
        U_STATIC_ASSERT(sim_t::is_sphere_nucleus);
    }

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Checks if seed can satisfy nuclear confinement constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE if seed can satisfy constraints, uFALSE o/w
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) const {
        uAssert(nfo.p_node_center);
        // @TODO - store dot product in local variable instead of TLS if not
        //  modeling lamina interactions (compile time enum branch)
        // Obtain reference to buffer for storing dot product
        uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(dot_buffer.n_elem));
        dot_buffer.at(U_SIS_SEED_CANDIDATE_ID) =
            uMatrixUtils::dot(*nfo.p_node_center, *nfo.p_node_center);
        const uReal allowed_nuclear_sq_radius =
            sim.get_allowed_nuclear_sq_radius(nfo.node_radius, sim);
        return dot_buffer.at(U_SIS_SEED_CANDIDATE_ID) <=
               allowed_nuclear_sq_radius;
    }

    /**
     * Determines which positions should be considered for growing chain from
     * parameter node. These positions have not been checked for nuclear
     * constraint satisfaction or self-avoidance.
     * @param out_candidate_positions - Output unfiltered 3-D positions of
     *  each candidate node center
     * @param parent_node_id - Node to extend chromatin chain from
     * @param candidate_radius - Radius of each candidate node, assumes all
     *  candidates have same radius.
     * @param parent_radius - Radius of the parent node
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void calc_candidate_positions(uMatrix& out_candidate_positions,
                                         const uUInt parent_node_id,
                                         const uReal candidate_radius,
                                         const uReal parent_radius,
                                         const sample_t& sample,
                                         const sim_t& sim
                                             U_THREAD_ID_PARAM) const {
        const uMatrix& node_positions = sample.get_node_positions();
        // Assuming column-wise point storage for node positions
        uAssert(node_positions.n_rows == uDim_num);
        uAssert(node_positions.size() > 0);
        uAssertBounds(parent_node_id, 0, node_positions.n_cols);
        uSisUtils::calc_candidate_positions(
            out_candidate_positions,
            node_positions.colptr(parent_node_id),
            candidate_radius,
            parent_radius,
            sim.get_unit_sphere_sample_points());
    }

    /**
     * Determines which positions fit within nuclear volume
     * @param out_legal_candidates - Output integer [0,1] array where i-th
     *  element is 1 if i-th candidate node position would satisfy nuclear
     *  volume constraint and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test if they are within nucleus.
     * @param candidate_radius - Radius of each candidate, assumes all
     *  candidates have same radius.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void nuclear_filter(uBoolVecCol& out_legal_candidates,
                               const uMatrix& candidate_centers,
                               const uReal& candidate_radius,
                               const sample_t& sample,
                               const sim_t& sim U_THREAD_ID_PARAM) const {
        // Assuming non-empty matrix with positions in rows
        uAssert(candidate_centers.size() > 0);
        uAssert(candidate_centers.n_cols == uDim_num);

        // Defer to simulation level mixin for buffers

        // Obtain reference to buffer for storing element-wise multiplication
        uMatrix& schur_buffer = sim.get_nuc_schur_buffer(U_THREAD_ID_0_ARG);
        uAssert(schur_buffer.n_cols == candidate_centers.n_cols);
        uAssert(schur_buffer.n_rows == candidate_centers.n_rows);
        uAssert(schur_buffer.size() == candidate_centers.size());
        uAssert(schur_buffer.n_cols == uDim_num);

        // Obtain reference to buffer for storing dot product
        uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssert(dot_buffer.n_rows == schur_buffer.n_rows);
        uAssert(dot_buffer.n_rows == candidate_centers.n_rows);

        // Points are within nuclear volume if the distance from the origin
        // is less than the allowed nuclear radius - where the allowed radius
        // is equal to (nuclear_radius - node_radius). It's more efficient to
        // test against the squared distance via the following equation:
        // x^2 + y^2 + z^2 <= (nuclear_radius - node_radius)^2
        // Any node center satisfying the above equation is within the nuclear
        // volume. A vectorized way of computing this constraint is to compute
        // the dot product of each candidate node center and see if it's less
        // than the allowed nuclear squared radius.

        // So, compute dot product of each candidate position:
        // Compute the element-wise (Schur) product
        schur_buffer = candidate_centers % candidate_centers;
        // Perform addition of column vectors to obtain dot product
        dot_buffer =
            schur_buffer.unsafe_col(uDim_X) + schur_buffer.unsafe_col(uDim_Y);
        dot_buffer += schur_buffer.unsafe_col(uDim_Z);

        uAssert(out_legal_candidates.n_rows == dot_buffer.n_rows);
        uAssert(out_legal_candidates.size() == dot_buffer.size());
        uAssert(out_legal_candidates.n_elem == dot_buffer.n_elem);

        // Test to see if dot product (the squared distance from the origin)
        // is less than allowed nuclear squared radius. If so, then candidate
        // node center is within nucleus.
        // @TODO - make sure this gets vectorized, see:
        // https://msdn.microsoft.com/en-us/library/jj614596(v=vs.120).aspx
        const uReal allowed_nuclear_sq_radius =
            sim.get_allowed_nuclear_sq_radius(candidate_radius, sim);
        const uUInt n_elem = U_TO_UINT(dot_buffer.n_elem);
        uBool* const out_legal_candidates_mem = out_legal_candidates.memptr();
        const uReal* const dot_buffer_mem = dot_buffer.memptr();
        for (uUInt i = 0; i < n_elem; ++i) {
            out_legal_candidates_mem[i] =
                (dot_buffer_mem[i] <= allowed_nuclear_sq_radius);
        }
    }
};

#endif  // uSisSphereNuclearMixin_h
