//****************************************************************************
// uSisSeoEllipsoidBudgIntrLamSimLevelMixin.inl
//****************************************************************************

/**
 * A lamina interaction mixin manages nuclear lamina association constraints.
 * Note, this is different from a "nuclear mixin" which is used for modeling
 * confinement by constraining all monomer beads to reside within the nuclear
 * volume. In contrast, the lamina interaction mixin enforces polymer
 * fragments to be proximal or distal to the nuclear periphery as configured
 * by the user.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin assumes an ellipsoidal nuclear volume and provides support for a
 * failure budget.
 */

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Budgeted lamina interaction simulation mixin
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisSeoEllipsoidBudgIntrLamSimLevelMixin_threaded
#else
class uSisSeoEllipsoidBudgIntrLamSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
    // @HACK - Inheritance for composition purposes (no virtual methods)
    : public uSisSeoEllipsoidIntrLamSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Parent type
     */
    typedef uSisSeoEllipsoidIntrLamSimLevelMixin<glue_t>
        parent_sim_level_intr_lam_mixin_t;

    /**
     * Default constructor
     */
#ifdef U_INJECT_THREADED_TLS
    uSisSeoEllipsoidBudgIntrLamSimLevelMixin_threaded()
#else
    uSisSeoEllipsoidBudgIntrLamSimLevelMixin_serial()
#endif  // U_INJECT_THREADED_TLS
    {
        clear();
    }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Reset state
        this->clear();
        // Initialize parent
        parent_sim_level_intr_lam_mixin_t::init(sim, config U_THREAD_ID_ARG);
        // Initialize failure budgets
        uIntrLibConfig::load_fail_budg(
            m_intr_lam_kin_max_fail,
            config,
            uOpt_lam_knock_in_fail_budget,
            uIntrLamDefaultKnockInFailBudgFrac,
            U_TO_UINT(this->get_intr_lam_kin().n_elem),
            uIntrLamKnockInDataName);
        uIntrLibConfig::load_fail_budg(
            m_intr_lam_ko_max_fail,
            config,
            uOpt_lam_knock_out_fail_budget,
            uIntrLamDefaultKnockOutFailBudgFrac,
            U_TO_UINT(this->get_intr_lam_ko().n_elem),
            uIntrLamKnockOutDataName);

        // Initialize TLS
        const uUInt num_candidates = sim.get_num_unit_sphere_sample_points();
        uAssert(num_candidates > U_TO_UINT(0));
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_lam_kin_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_lam_ko_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_lam_kin_fail_table, resize, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_lam_ko_fail_table, resize, num_candidates);
    }

    /**
     * Resets to default state
     */
    void clear() {
        parent_sim_level_intr_lam_mixin_t::clear();
        m_intr_lam_kin_max_fail = U_TO_UINT(0);
        m_intr_lam_ko_max_fail = U_TO_UINT(0);
        m_intr_lam_kin_delta_fails.clear();
        m_intr_lam_ko_delta_fails.clear();
        m_intr_lam_kin_fail_table.clear();
        m_intr_lam_ko_fail_table.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Lamina knock-in interaction failure budget
     */
    inline uUInt get_intr_lam_kin_max_fail() const {
        uAssertBoundsInc(m_intr_lam_kin_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_lam_kin().n_elem));
        return m_intr_lam_kin_max_fail;
    }

    /**
     * @return Lamina knock-out interaction failure budget
     */
    inline uUInt get_intr_lam_ko_max_fail() const {
        uAssertBoundsInc(m_intr_lam_ko_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_lam_ko().n_elem));
        return m_intr_lam_ko_max_fail;
    }

    // Note: parent simulation is likely to be const, therefore making buffers
    // mutable objects
    inline uUIVecCol& get_intr_lam_kin_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_lam_kin_delta_fails);
    }
    inline uUIVecCol& get_intr_lam_ko_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_lam_ko_delta_fails);
    }
    inline uSisIntrLamBudgFailTable_t& get_intr_lam_kin_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_lam_kin_fail_table);
    }
    inline uSisIntrLamBudgFailTable_t& get_intr_lam_ko_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_lam_ko_fail_table);
    }

    // @return TLS handles for budget lamina interaction failure policy
    inline uSisIntrLamBudgFailPolicyArgs_t get_intr_lam_budg_fail_policy_args(
        U_THREAD_ID_0_PARAM) const {
        uSisIntrLamBudgFailPolicyArgs_t policy_args = {
            this->get_intr_lam_kin_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_lam_ko_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_lam_kin_fail_table(U_THREAD_ID_0_ARG),
            this->get_intr_lam_ko_fail_table(U_THREAD_ID_0_ARG)};
        return policy_args;
    }

private:
    /**
     * Lamina knock-in interaction failure budget, max number of interaction
     *  constraints that are allowed to fail
     */
    uUInt m_intr_lam_kin_max_fail;

    /**
     * Lamina knock-out interaction failure budget, max number of interaction
     *  constraints are allowed to fail
     */
    uUInt m_intr_lam_ko_max_fail;

    /**
     * Scratch buffer for storing incremental (delta) knock-in failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_lam_kin_delta_fails);

    /**
     * Scratch buffer for storing incremental (delta) knock-out failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_lam_ko_delta_fails);

    // Note, the size of each candidate's fail list should be same as the
    //  corresponding count in the delta fail vector. Though this information
    //  is redundant, am still retaining separate 'delta' vector for failed
    //  counts at each candidate, as this makes it easier to implement
    //  energy selection models based on failure counts.

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-in interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrLamBudgFailTable_t,
                               m_intr_lam_kin_fail_table);

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-out interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrLamBudgFailTable_t,
                               m_intr_lam_ko_fail_table);
};
