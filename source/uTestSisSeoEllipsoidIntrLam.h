//****************************************************************************
// uTestSisSeoEllipsoidIntrLam.h
//****************************************************************************

/**
 * Tests lamina interactions under assumption of ellipsoid nuclear volume
 *
 * sis: Sequential importance sampling
 * seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * sloc: Single locus
 * mloc: Multiple loci
 *
 * Also:
 *
 *  hetr: Heterogeneous radius nodes
 *  homg: Homogeneous radius nodes
 *
 * Usage for heterogeneous radius:
 *
 *  (single locus)
 * -test_sis_seo_ellip_intr_lam_sloc_hetr --conf <conf_path>
 *
 *  (multiple loci)
 * -test_sis_seo_ellip_intr_lam_mloc_hetr --conf <conf_path>
 *
 * Usage for homogeneous radius:
 *
 *  (single locus)
 * -test_sis_seo_ellip_intr_lam_sloc_homg --conf <conf_path>
 *
 *  (multiple loci)
 * -test_sis_seo_ellip_intr_lam_mloc_homg --conf <conf_path>
 *
 * or, if defaults are okay to use, then respectively:
 *
 * -test_sis_seo_ellip_intr_lam_sloc_hetr
 * -test_sis_seo_ellip_intr_lam_mloc_hetr
 * -test_sis_seo_ellip_intr_lam_sloc_homg
 * -test_sis_seo_ellip_intr_lam_mloc_homg
 *
 *  Tests also available for budgeted interactions:
 *
 * -test_sis_seo_ellip_intr_lam_sloc_hetr_budg [--conf <path>]
 * -test_sis_seo_ellip_intr_lam_mloc_hetr_budg [--conf <path>]
 * -test_sis_seo_ellip_intr_lam_sloc_homg_budg [--conf <path>]
 * -test_sis_seo_ellip_intr_lam_mloc_homg_budg [--conf <path>]
 *
 * Currently, budgeted interaction tests are over hard-coded budget profile
 *  and, therefore, setting them in INI will have no effect
 */

#ifdef uTestSisSeoEllipsoidIntrLam_h
#   error "Test Sis Seo Ellipsoid Intr Lam included multiple times!"
#endif  // uTestSisSeoEllipsoidIntrLam_h
#define uTestSisSeoEllipsoidIntrLam_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uFilesystem.h"
#include "uLogf.h"
#include "uMockUpUtils.h"
#include "uSisNullIntrChrMixin.h"
#include "uSisSeoEllipsoidBudgIntrLamMixin.h"
#include "uSisSeoEllipsoidIntrLamMixin.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypes.h"

#include <string>

namespace uTestSisSeoEllipsoidIntrLam {

/**
 * A library glue for multi-locus, SEO growth with KI and KO constraints and
 * heterogeneous radius nodes
 */
U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(HetrGlue,
                                     uSisSeoMloc::GrowthMixin,
                                     uSisSeoMloc::GrowthSimLevelMixin_threaded,
                                     uSisHetrNodeRadiusMixin,
                                     uSisHetrNodeRadiusSimLevelMixin,
                                     uSisNullIntrChrMixin,
                                     uSisNullIntrChrSimLevelMixin,
                                     uSisSeoEllipsoidIntrLamMixin,
                                     uSisSeoEllipsoidIntrLamSimLevelMixin,
                                     uSisNullIntrNucbMixin,
                                     uSisNullIntrNucbSimLevelMixin,
                                     uSisUnifEnergyMixin,
                                     uSisUnifEnergySimLevelMixin);

/**
 * Budgeted, heterogeneous glue
 */
U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(
    HetrBudgGlue,
    uSisSeoMloc::GrowthMixin,
    uSisSeoMloc::GrowthSimLevelMixin_threaded,
    uSisHetrNodeRadiusMixin,
    uSisHetrNodeRadiusSimLevelMixin,
    uSisNullIntrChrMixin,
    uSisNullIntrChrSimLevelMixin,
    uSisSeoEllipsoidBudgIntrLamMixin,
    uSisSeoEllipsoidBudgIntrLamSimLevelMixin_threaded,
    uSisNullIntrNucbMixin,
    uSisNullIntrNucbSimLevelMixin,
    uSisUnifEnergyMixin,
    uSisUnifEnergySimLevelMixin);

/**
 * A library glue for multi-locus, SEO growth with KI and KO constraints and
 * homogeneous radius nodes
 */
U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(HomgGlue,
                                     uSisSeoMloc::GrowthMixin,
                                     uSisSeoMloc::GrowthSimLevelMixin_threaded,
                                     uSisHomgNodeRadiusMixin,
                                     uSisHomgNodeRadiusSimLevelMixin,
                                     uSisNullIntrChrMixin,
                                     uSisNullIntrChrSimLevelMixin,
                                     uSisSeoEllipsoidIntrLamMixin,
                                     uSisSeoEllipsoidIntrLamSimLevelMixin,
                                     uSisNullIntrNucbMixin,
                                     uSisNullIntrNucbSimLevelMixin,
                                     uSisUnifEnergyMixin,
                                     uSisUnifEnergySimLevelMixin);

/**
 * Budgeted, homogeneous glue
 */
U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(
    HomgBudgGlue,
    uSisSeoMloc::GrowthMixin,
    uSisSeoMloc::GrowthSimLevelMixin_threaded,
    uSisHomgNodeRadiusMixin,
    uSisHomgNodeRadiusSimLevelMixin,
    uSisNullIntrChrMixin,
    uSisNullIntrChrSimLevelMixin,
    uSisSeoEllipsoidBudgIntrLamMixin,
    uSisSeoEllipsoidBudgIntrLamSimLevelMixin_threaded,
    uSisNullIntrNucbMixin,
    uSisNullIntrNucbSimLevelMixin,
    uSisUnifEnergyMixin,
    uSisUnifEnergySimLevelMixin);

/**
 * Utility verifies that all samples within simulation have satisfied knock-in
 * constraints
 */
template <typename sim_t>
uBool verify_ki(const sim_t& sim) {
    // @TODO!
    return uTRUE;
}

/**
 * Utility verifies that all samples within simulation have satisfied
 * knock-out constraints
 */
template <typename sim_t>
uBool verify_ko(const sim_t& sim) {
    // @TODO!
    return uTRUE;
}

/**
 * Common utility to run a single test simulation
 */
template <typename sim_t>
int run_sim(const uSpConstConfig_t config,
            const std::string& announce,
            const uUInt test_id) {
    // Output configuration
    uLogf("%d: %s\n", (int)test_id, announce.c_str());
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Export!
    uSisUtils::export_sim(sim);

    // Destroy globals
    uG::teardown();

    // Verify interactions
    uLogf("%d: TEST knock-in lamina interactions...\n", (int)test_id);
    const uBool ki_pass = verify_ki(sim);
    uLogf("%d: TEST knock-in lamina interactions %s\n",
          (int)test_id,
          (ki_pass ? "PASS" : "FAIL"));
    uLogf("%d: TEST knock-out lamina interactions...\n", (int)test_id);
    const uBool ko_pass = verify_ko(sim);
    uLogf("%d: TEST knock-out lamina interactions %s\n",
          (int)test_id,
          (ko_pass ? "PASS" : "FAIL"));

    // Return normal if all tests passed, else error
    return (ki_pass && ko_pass) ? uExitCode_normal : uExitCode_error;
}

/**
 * Run a suite of tests
 */
template <typename sim_t>
int run_test_suite(const uCmdOptsMap& cmd_opts,
                   const std::string& announce,
                   const std::string& subdir_name) {
    // Create a base configuration
    uSpConfig_t template_config = uSmartPtr::make_shared<uConfig>();

    // Override from command line
    template_config->init(cmd_opts);

    // Output exit status
    int ec = uExitCode_normal;

    // Process child test configurations
    const uUInt MAX_TESTS = 1000;
    for (uUInt off = 0; off <= MAX_TESTS; ++off) {
        std::string test_child_conf_path;
        if (!template_config->resolve_path_vec(
                test_child_conf_path, uOpt_test_child_conf, off)) {
            // No more child tests!
            break;
        }
        // Configure test
        uCmdOptsMap job_cmd = cmd_opts;
        job_cmd.set_option(uOpt_get_cmd_switch(uOpt_conf_child_tr),
                           test_child_conf_path);
        uSpConfig_t job_config = uSmartPtr::make_shared<uConfig>();
        job_config->init(job_cmd);
        if (!job_config->get_child_tr()) {
            uLogf(
                "Error: test suite child configuration %d not found. "
                "Exiting.\n",
                (int)off);
            exit(uExitCode_error);
        }

        // Configure export flags
        job_config->export_flags =
            uExportCsv | uExportPml | uExportExtended | uExportIntrLam;
        job_config->get_child_tr()->export_flags = job_config->export_flags;

        // Determine output folder
        if (!subdir_name.empty()) {
            job_config->output_dir =
                (uFs::path(job_config->output_dir) / uFs::path(subdir_name))
                    .string();
            job_config->get_child_tr()->output_dir = job_config->output_dir;
        }

        uLogf(
            "\n**************************************\nTEST "
            "%d\n**************************************\n\n",
            (int)off);
        if (!run_sim<sim_t>(job_config->get_child_tr(), announce, off) ==
            uExitCode_normal) {
            // Test failed
            ec = uExitCode_error;
            break;
        }
    }

    // Return exit status
    return ec;
}

/**
 * Runs test suite for parameter budget
 */
template <typename sim_t>
int run_test_suite_budg_at(const uCmdOptsMap& cmd_opts_,
                           const std::string& announce_no_dot_or_newline,
                           const std::string& subdir_name_,
                           const uReal kin_fail_perc,
                           const uReal ko_fail_perc) {
    // Assume budgets are in [0,1]
    uAssertRealBoundsInc(kin_fail_perc, U_TO_REAL(0.0), U_TO_REAL(1.0));
    uAssertRealBoundsInc(ko_fail_perc, U_TO_REAL(0.0), U_TO_REAL(1.0));
    // Override command-line budgets
    uCmdOptsMap cmd_opts = cmd_opts_;
    const std::string kin_fail_key(
        uOpt_get_cmd_switch(uOpt_lam_knock_in_fail_budget));
    const std::string ko_fail_key(
        uOpt_get_cmd_switch(uOpt_lam_knock_out_fail_budget));
    const std::string str_kin_fail_perc = u2Str(kin_fail_perc);
    const std::string str_ko_fail_perc = u2Str(ko_fail_perc);
    cmd_opts.set_option(kin_fail_key, str_kin_fail_perc.c_str());
    cmd_opts.set_option(ko_fail_key, str_ko_fail_perc.c_str());
    // Resolve announce string
    const std::string announce =
        announce_no_dot_or_newline + std::string("\n\t-kin_budg = ") +
        str_kin_fail_perc + std::string("\n\t-ko_budg = ") + str_ko_fail_perc +
        std::string("\n");
    // Resolve output subdirectory
    const std::string subdir_name = subdir_name_ + std::string(".kinb.") +
                                    str_kin_fail_perc + std::string(".kob.") +
                                    str_ko_fail_perc;
    // Launch test suite
    return run_test_suite<sim_t>(cmd_opts, announce, subdir_name);
}

/**
 * Run test suites for hard-coded budget profile
 */
template <typename sim_t>
int run_test_suite_budg_prof(const uCmdOptsMap& cmd_opts,
                             const std::string& announce_no_dot_or_newline,
                             const std::string& subdir_name) {

    // Homogeneous fail budget profile (KIN fail budget == KO fail budget)
    const uReal homg_fail_budg_prof[] = {
        U_TO_REAL(0.0), U_TO_REAL(0.5), U_TO_REAL(1.0)};
    // Determine number of elements in profile
    size_t n_prof =
        sizeof(homg_fail_budg_prof) / sizeof(homg_fail_budg_prof[0]);
    // Process profile
    for (size_t i = 0; i < n_prof; ++i) {
        const uReal fail_budg = homg_fail_budg_prof[i];
        const int ec = run_test_suite_budg_at<sim_t>(cmd_opts,
                                                     announce_no_dot_or_newline,
                                                     subdir_name,
                                                     fail_budg,
                                                     fail_budg);
        if (ec != uExitCode_normal) {
            // Early out if suite failed
            return ec;
        }
    }

    // If we reached here, all tests passed!
    return uExitCode_normal;
}

/**
 * Initialize command line options, defer to default path if not specified
 */
void init_cmd(uCmdOptsMap& out_cmd,
              const uCmdOptsMap& in_cmd,
              const std::string& ini_name) {
    out_cmd = in_cmd;
    // Default INI path
    const std::string ini_path =
        "../tests/test_sis_seo_ellip_intr_lam/" + ini_name;
    // Check if key present, set to default if not specified
    const std::string conf_key(uOpt_get_cmd_switch(uOpt_conf));
    out_cmd.set_option_if_absent(conf_key, ini_path);
}

}  // namespace uTestSisSeoEllipsoidIntrLam

/**
 * Entry point for SLOC commandlet test with heterogeneous radius nodes
 */
int uTestSisSeoEllipsoidIntrLamSlocHetrMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HetrGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.hetr");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Sloc Hetr.\n",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with heterogeneous radius nodes and
 *  failure budget
 */
int uTestSisSeoEllipsoidIntrLamSlocHetrBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HetrBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.hetr.budg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Sloc Hetr Budg",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with heterogeneous radius nodes
 */
int uTestSisSeoEllipsoidIntrLamMlocHetrMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HetrGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.hetr");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Mloc Hetr.\n",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with heterogeneous radius nodes and
 *  failure budget
 */
int uTestSisSeoEllipsoidIntrLamMlocHetrBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HetrBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.hetr.budg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Mloc Hetr Budg",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with homogeneous radius nodes
 */
int uTestSisSeoEllipsoidIntrLamSlocHomgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HomgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.homg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Sloc Homg.\n",
        subdir_name);
}

/**
 * Entry point for SLOC commandlet test with homogeneous radius nodes and
 *  failure budget
 */
int uTestSisSeoEllipsoidIntrLamSlocHomgBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HomgBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.sloc.ini");

    // Launch test suite
    const std::string subdir_name("sloc.homg.budg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Sloc Homg Budg",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with homogeneous radius nodes
 */
int uTestSisSeoEllipsoidIntrLamMlocHomgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HomgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.homg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Mloc Homg.\n",
        subdir_name);
}

/**
 * Entry point for MLOC commandlet test with homogeneous radius nodes and
 *  failure budget
 */
int uTestSisSeoEllipsoidIntrLamMlocHomgBudgMain(const uCmdOptsMap& cmd_opts_) {
    // Expose final simulation
    typedef uTestSisSeoEllipsoidIntrLam::HomgBudgGlue::sim_t sim_t;

    // Check if configuration specified, else set to default
    uCmdOptsMap cmd_opts;
    uTestSisSeoEllipsoidIntrLam::init_cmd(cmd_opts, cmd_opts_, "base.mloc.ini");

    // Launch test suite
    const std::string subdir_name("mloc.homg.budg");
    return uTestSisSeoEllipsoidIntrLam::run_test_suite_budg_prof<sim_t>(
        cmd_opts,
        "Running Test Sis Seo Ellipsoid Intr Lam Mloc Homg Budg",
        subdir_name);
}

#endif  // U_BUILD_ENABLE_TESTS
