//****************************************************************************
// uTypes.h
//****************************************************************************

/**
 * @brief Defines core primitive types
 */

#ifndef uTypes_h
#define uTypes_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uStaticAssert.h"

#include <limits>
#include <stdint.h>

//****************************************************************************
// Types
//****************************************************************************

/**
 * Our default floating point type
 */
typedef double uReal;

/**
 * Macro for casting to uReal
 */
#define U_TO_REAL(x) (static_cast<uReal>((x)))

/**
 * Maximum positive real value
 * Note: Maximum negative real value is -U_REAL_MAX!
 */
#define U_REAL_MAX (std::numeric_limits<uReal>::max())

/**
 * The difference between 1.0 and the next representable value of the given
 * floating-point type. Can be used for checking floating-point "equality".
 * Explicitly Using float type because thats the threshold level of precision
 * we need (and want) for most comparison operations.
 */
#define U_REAL_EPS U_TO_REAL(std::numeric_limits<float>::epsilon())

/**
 * Macro for comparing real valued equality
 */
#define U_REAL_CMP_EQ(a, b) \
    (std::abs((a) - (b)) < (U_TO_REAL(2.0) * U_REAL_EPS))

/**
 * Macro for comparing real valued a <= b
 */
#define U_REAL_CMP_LTE(a, b) (U_REAL_CMP_EQ((a), (b)) || ((a) < (b)))

/**
 * Macro for comparing real valued a >= b
 */
#define U_REAL_CMP_GTE(a, b) U_REAL_CMP_LTE((b), (a))

/**
 * Boolean type
 */
typedef unsigned int uBool;

#ifdef uTRUE
#   error uTRUE already defined!
#endif  // uTRUE

#define uTRUE 1

#ifdef uFALSE
#   error uFALSE already defined!
#endif  // uFALSE

#define uFALSE 0

/**
 * Unsigned integer type
 */
typedef uint64_t uUInt;

/**
 * Macro for casting to uUInt
 */
#define U_TO_UINT(x) (static_cast<uUInt>((x)))

/**
 * The maximum uUInt value
 */
// @HACK - Older compilers may complain when using the "function call":
//      std::numeric_limits<uUInt>::max()
//  within U_STATIC_ASSERT(); as work-around, use stdint.h hard-coded value
#define U_UINT_MAX UINT64_MAX

/**
 * Macro which is 1 if unsigned integer argument is power of 2, 0 o/w
 */
#define U_UINT_IS_POW_2(ui) ((ui) && !((ui) & ((ui)-1)))

/**
 * 16-bit unsigned integer type
 */
typedef uint16_t uUInt16_t;

/**
 * Maximum 16-bit unsigned integer type
 */
#define U_UINT16_MAX UINT16_MAX

/**
 * 'Small' element identifier type, uUInt will always be at least this range.
 *  Intended type for sample-level, broad phase storage
 */
#ifdef U_BUILD_ENABLE_BIG_ELEM_ID
    typedef uint32_t uUElemId_t;
#else
    typedef uUInt16_t uUElemId_t;
#endif // U_BUILD_ENABLE_BIG_ELEM_ID
U_STATIC_ASSERT(sizeof(uUElemId_t) <= sizeof(uUInt));

/**
 * Max element identifier (maximum total number of monomers per sample)
 */
#ifdef U_BUILD_ENABLE_BIG_ELEM_ID
#   define U_UELEMID_MAX UINT32_MAX
#else
#   define U_UELEMID_MAX U_UINT16_MAX
#endif // U_BUILD_ENABLE_BIG_ELEM_ID
U_STATIC_ASSERT(U_UELEMID_MAX <= U_UINT_MAX);

/**
 * Macro to clip a scalar 'x' to be in range [lo, hi]
 */
#define U_CLIP(x_, lo_, hi_) std::max(std::min((x_), (hi_)), (lo_))

/**
 * Offsets for X,Y,Z dimensions
 */
enum uDim {
    uDim_dist = 0,
    uDim_radius = 3,
    uDim_dist2_thresh = uDim_radius,
    uDim_X = 0,
    uDim_Y,
    uDim_Z,
    // number of dimensions for (X,Y,Z) tuple
    uDim_num,
    // number of dimensions for (X,Y,Z,R|dist2_thresh) tuple
    uDim_num_extended
};

/**
 * Matrix type
 */
#include "uArmadillo.h"

#endif  // uTypes_h
