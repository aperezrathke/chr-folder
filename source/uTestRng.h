//****************************************************************************
// uTestRng.h
//****************************************************************************

/**
 * Commandlet for testing random number generation
 *
 * Usage:
 * -test_rng
 */

#ifdef uTestRng_h
#   error "Test Rng included multiple times!"
#endif  // uTestRng_h
#define uTestRng_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uExitCodes.h"
#include "uGlobals.h"
#include "uThread.h"
#include "uTypes.h"

#include <iostream>

namespace {
namespace uTestRng {
    /**
     * Utility to report the generated number and the interval it should fall within
     */
    template <typename t_>
    void report_interval(t_ rn, t_ lower_, t_ upper_) {
        std::cout << "Generated " << rn << " in [" << lower_ << ", " << upper_
                  << ")\n";
    }

    /**
     * For interval in [0,1)
     */
    void generate_and_report_interval(U_THREAD_ID_0_PARAM) {
        report_interval(uRng.unif_real(), U_TO_REAL(0.0), U_TO_REAL(1.0));
    }

    /**
     * For interval in [lower_, upper_] in real numbers
     */
    void generate_and_report_interval(uReal lower_,
                                      uReal upper_ U_THREAD_ID_PARAM) {
        report_interval(uRng.unif_real(lower_, upper_), lower_, upper_);
    }

    /**
     * For interval in [lower_, upper_] in integer numbers
     */
    void generate_and_report_interval(int lower_, int upper_ U_THREAD_ID_PARAM) {
        report_interval(uRng.unif_int(lower_, upper_), lower_, upper_);
    }
}  // namespace uTestRng
}  // Anonymous namespace

/**
 * Entry point for test
 */
int uTestRngMain(const uCmdOptsMap& cmd_opts) {
    uG::default_init();
    using namespace uTestRng;
    generate_and_report_interval(U_MAIN_THREAD_ID_0_ARG);
    generate_and_report_interval(U_TO_REAL(-1.0),
                                 U_TO_REAL(1.0) U_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(U_TO_REAL(-5.0),
                                 U_TO_REAL(-1.0) U_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 100 U_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 0 U_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(0, 100 U_MAIN_THREAD_ID_ARG);
    uG::teardown();
    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
