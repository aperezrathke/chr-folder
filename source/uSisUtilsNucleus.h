//****************************************************************************
// uSisUtilsNucleus.h
//****************************************************************************

/**
 * Miscellaneous utilities related to the nucleus
 */

#ifndef uSisUtilsNucleus_h
#define uSisUtilsNucleus_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility namespace
 */
namespace uSisUtils {
/**
 * Nuclear configuration query utilities
 */
class NucConfig {
public:
    /**
     * Utility for determining maximum specified nuclear diameter, useful
     * for commandlets which may not create formal simulations
     * @param out - Output diameter
     * @WARNING 'out' is initialized to 0 even if user did not specify a
     *  relevant value
     * @param config - User configuration
     * @return uTRUE if value found in config, uFALSE o/w
     */
    static uBool get_max_diameter(uReal& out, uSpConstConfig_t config) {
        // Initialize to spherical default
        out = U_TO_REAL(0.0);
        uBool result = config->read_into(out, uOpt_nuclear_diameter);
        // Check for ellipsoid principal axis
        uReal nuc_diam = U_TO_REAL(0.0);
        result |= config->read_into(nuc_diam, uOpt_nuclear_diameter_x);
        out = std::max(out, nuc_diam);
        result |= config->read_into(nuc_diam, uOpt_nuclear_diameter_y);
        out = std::max(out, nuc_diam);
        result |= config->read_into(nuc_diam, uOpt_nuclear_diameter_z);
        out = std::max(out, nuc_diam);
        return result;
    }

    /**
     * Initialize nuclear diameter and radius along parameter principal axis.
     * WILL EXIT WITH ERROR CODE IF NON-POSITIVE VALUE ENCOUNTERED!
     * @param out_diam - Output nuclear diameter along principal axis
     * @param out_rad - Output nuclear radius along principal axis
     * @param out_homg_rad - Output allowed nuclear radius for homogeneous
     *  node simulations
     * @param out_homg_sq_rad - Output allowed nuclear radius squared for
     *  homogeneous node simulations
     * @param opt_axis - Option key for principal axis
     * @param def_diam - Default nuclear diameter
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration
     */
    template <typename sim_t>
    static void init_axis(uReal& out_diam,
                          uReal& out_rad,
                          uReal& out_homg_allowed_rad,
                          uReal& out_homg_allowed_sq_rad,
                          const enum uOptE opt_axis,
                          const uReal def_diam,
                          const sim_t& sim,
                          uSpConstConfig_t config) {
        // Read diameter along principal axis
        out_diam = def_diam;
        config->read_into(out_diam, opt_axis);
        if (out_diam <= U_TO_REAL(0.0)) {
            uLogf(
                "Error: Non-positive nuclear diameter (%s = %f) encountered. "
                "Exiting.\n",
                uOpt_get_cmd_switch(opt_axis),
                (double)out_diam);
            exit(uExitCode_error);
        }
        // Verify nuclear diameter is greater than node diameter
        const uReal max_node_diameter = sim.get_max_node_diameter();
        if (max_node_diameter >= out_diam) {
            uLogf(
                "Error: nuclear diameter (%s = %f) <= max node diameter %f. "
                "Exiting.\n",
                uOpt_get_cmd_switch(opt_axis),
                (double)out_diam,
                (double)max_node_diameter);
            exit(uExitCode_error);
        }
        // Cache nuclear radius
        out_rad = U_TO_REAL(0.5) * out_diam;
        // Determine if we can cache allowed nuclear radius for homogeneous
        // node simulations (all nodes have same radii)
        if (sim_t::is_homg_radius) {
            // Initialize the global allowed nuclear radius, true for all
            // nodes since all nodes are homogeneous
            out_homg_allowed_rad =
                out_rad - (U_TO_REAL(0.5) * max_node_diameter);
            uAssert(out_homg_allowed_rad > U_TO_REAL(0.0));
            // Initialize global allowed squared nuclear radius for all nodes
            out_homg_allowed_sq_rad =
                out_homg_allowed_rad * out_homg_allowed_rad;
        }
    }
};

}  // namespace uSisUtils

#endif  // uSisUtilsNucleus_h
