//****************************************************************************
// uOpts.h
//****************************************************************************

/**
 * Defines the program options
 */

#ifndef uOpts_h
#define uOpts_h

#include "uBuild.h"
#include "uAssert.h"

/**
 * Enumerated user options
 */
enum uOptE {
    /**
     * Null dispatcher for user-defined models
     */
      uOpt_null_dispatch = 0

    /**
     * Main dispatcher for user-defined models
     */
    , uOpt_main_dispatch

    /**
     * Fea(sibility) dispatcher for user-defined models
     */
    , uOpt_fea_dispatch

    /**
     * Prints command line options and how to use program
     * Note: option can only be specified via command line
     */
    , uOpt_help

    /**
     * Same as uOpt_help
     */
    , uOpt_app_help

    /**
     * @DEPRECATED - legacy support, use bake system instead
     * Runs SIS for single knock-in interaction constrained samples using
     * nested landmark trial runners with resampling and rejection control.
     */
    , uOpt_app_sis_unif_ski_lmrk_rs_rc

    /**
     * Specifies path to configuration INI file
     * Note: option can only be specified via command line
     */
    , uOpt_conf

    /**
     * @DEPRECATED - legacy support, same as uOpt_conf_child_tr
     * Specifies optional path trial runner child (sub-sim) configuration.
     */
    , uOpt_conf_child

    /**
     * Specifies optional path trial runner child (sub-sim) configuration.
     */
    , uOpt_conf_child_tr

    /**
     * Specifies optional path trial runner select child (sub-sim)
     * configuration.
     */
    , uOpt_conf_child_tr_sel

    /**
     * Specifies optional path to quality control child (sub-sim)
     * configuration.
     */
    , uOpt_conf_child_qc

    /**
     * Archetype configuration used by INI file, option values not specified
     * by main INI will be searched for in the archetype - meant to specify
     * defaults and/or common option settings
     */
    , uOpt_arch

    /**
     * Specifies directory to store generated chains
     */
    , uOpt_output_dir

    /**
     * Prefix appended to all output chains
     */
    , uOpt_job_id

#ifdef U_BUILD_ENABLE_THREADS
    /**
     * Specifies the number of worker threads
     */
    , uOpt_num_worker_threads

    /**
     * Specifies the atomic work performed by each thread during parallel
     * growth
     */
    , uOpt_grow_chunk_size
#endif  // U_BUILD_ENABLE_THREADS

    /**
     * Max number of trials that simulation will run. A 0 or negative input
     * results in the maximum number of iterations (max value of unsigned int)
     *
     * A trial is a complete run of the the simulation. However, after the
     * run, there may be loci chains that died (were unable to proceed because
     * they grew into a corner). Each trial will attempt to generate the
     * remaining chains until the target ensemble size is reached or the
     * maximum trial count is reached - whichever comes first.
     */
    , uOpt_max_trials

    /**
     * Nodes are rejected if they do not fit within nuclear volume. For
     * spherical nucleus, defines sphere diameter. For other volumes such as
     * ellipsoidal, defines the default diameter along each principal axis.
     *
     * Units: Angstroms
     */
    , uOpt_nuclear_diameter

    /**
     * Nuclear diameter along 'x' principal axis, ignored if spherical nucleus
     */
    , uOpt_nuclear_diameter_x

    /**
     * Nuclear diameter along 'y' principal axis, ignored if spherical nucleus
     */
    , uOpt_nuclear_diameter_y

    /**
     * Nuclear diameter along 'z' principal axis, ignored if spherical nucleus
     */
    , uOpt_nuclear_diameter_z

    /**
     * Number of nodes (monomers) for a chromatin polymer chain.
     *
     * Format:
     * num_nodes_<locus_id> = <positive integer>
     *
     * where <locus_id> is a unique, sequential 0-based index identifying the
     * chromatin chain and <positive integer> is the number of nodes belonging
     * to that chain.
     *
     * Each <locus_id> represents a unique genomic loci.
     *
     * E.g.:
     * num_nodes_0 = 100
     *
     * will assign 100 chromatin nodes to the 0-th chromatin chain
     *
     * If <locus_id> is omitted (e.g. - num_nodes = 100), then the <locus_id>
     * will default to 0.
     */
    , uOpt_num_nodes

    /**
     * Node diameter for homogeneous polymers (all nodes same size). Value is
     * ignored for heterogeneous polymers.
     *
     * Note: node == smallest monomer unit modeled when generating a chromatin
     * polymer chain.
     *
     * Units: Angstroms
     */
    , uOpt_max_node_diameter

    /**
     * Path to whitespace or comma delimited (CSV) file containing diameters
     * at each node. ONLY USED FOR HETEROGENEOUS NODES. Ignored if nodes are
     * homogeneous. Current format is run-length encoded:
     *  <run : +integer> delim <diameter : +float>
     *  ...
     * Where "run" specifies the number of consecutive nodes with the given
     * diameter and 'delim' is any comma or any whitespace character. The sum
     * of the run values must match the total node count for all chains.
     * Diameters are assumed to be in the same order as specified by
     * "num_nodes*" param.
     *
     * Example:
     *   6, 55.0
     *  12, 100.0
     *   7, 27.5
     *
     * This will assign diameters as follows: first six nodes will each have
     * diameters of 55.0, the next 12 nodes will each have diameter 100.0, and
     * the last seven nodes will each have diameter of 27.5 Angstroms.
     */
    , uOpt_node_diameter_fpath

    /**
     * Number of points to sample on surface of node sphere when growing a chain
     * and deciding where to add the next node.
     */
    , uOpt_num_unit_sphere_sample_points

    /**
     * Number of samples to be generated for each run of the program.
     * A sample may consist of multiple chromatin chains if modeling multiple
     * loci simultaneously.
     */
    , uOpt_ensemble_size

    /**
     * Boolean - controls if percent unique monomers should be reported
     *  at each node position, ideal for resampling-based simulations
     */
    , uOpt_report_perc_uniq

    /**
     * Flag all samples to be exported to individual CSV files. This is a
     *  unary, command-line only switch with usage: -export_csv
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_csv_unary

    /**
     * Flag all samples to be exported to individual CSV files. This is a
     *  binary option which can be specified using INI or command-line, usage:
     *      command-line: --export_csv <0|1>
     *      INI: export_csv = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_csv_binary

    /**
     * Flag all samples to be exported to individual compressed CSV files.
     * This is a unary, command-line only switch with usage: -export_csv_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_csv_gz_unary

    /**
     * Flag all samples to be exported to individual compressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_csv_gz <0|1>
     *      INI: export_csv_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_csv_gz_binary

    /**
     * Flag all samples to be exported to bulk uncompressed CSV files.
     * This is a unary, command-line only switch with usage: -export_csv_bulk
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_csv_bulk_unary

    /**
     * Flag all samples to be exported to bulk uncompressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_csv_bulk <0|1>
     *      INI: export_csv_bulk = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_csv_bulk_binary

    /**
     * Flag all samples to be exported to bulk compressed CSV files.
     * This is a unary, command-line only switch with usage: -export_csv_bulk_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_csv_bulk_gz_unary

    /**
     * Flag all samples to be exported to bulk compressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_csv_bulk_gz <0|1>
     *      INI: export_csv_bulk_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_csv_bulk_gz_binary

    /**
     * Overload default export precision for CSV files. Usage:
     *  --export_csv_precision <+integer>
     */
    , uOpt_export_csv_precision

    /**
     * Flag all samples to be exported to individual PDB files. This is a
     *  unary command-line only switch with usage: -export_pdb
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pdb_unary

    /**
     * Flag all samples to be exported to individual PDB files. This is a
     *  binary option which can be specified using INI or command-line, usage:
     *      command-line: --export_pdb <0|1>
     *      INI: export_pdb = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pdb_binary

    /**
     * Flag all samples to be exported to individual compressed PDB files.
     *  This is a unary command-line only switch with usage: -export_pdb_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pdb_gz_unary

    /**
     * Flag all samples to be exported to individual compressed PDB files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_pdb_gz <0|1>
     *      INI: export_pdb_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pdb_gz_binary

    /**
     * Flag all samples to be exported to bulk uncompressed PDB files.
     * This is a unary, command-line only switch with usage: -export_pdb_bulk
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pdb_bulk_unary

    /**
     * Flag all samples to be exported to bulk uncompressed PDB files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_pdb_bulk <0|1>
     *      INI: export_pdb_bulk = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pdb_bulk_binary

    /**
     * Flag all samples to be exported to bulk compressed PDB files.
     * This is a unary, command-line only switch with usage: -export_pdb_bulk_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pdb_bulk_gz_unary

    /**
     * Flag all samples to be exported to bulk compressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_csv_bulk_gz <0|1>
     *      INI: export_csv_bulk_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pdb_bulk_gz_binary

    /**
     * Flag all samples to be exported to individual PML files. This is a
     *  unary command-line only switch with usage: -export_pml
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pml_unary

    /**
     * Flag all samples to be exported to individual PML files. This is a
     *  binary option which can be specified using INI or command-line, usage:
     *      command-line: --export_pml <0|1>
     *      INI: export_pml = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pml_binary

    /**
     * Flag all samples to be exported to individual compressed PML files.
     *  This is a unary command-line only switch with usage: -export_pml_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pml_gz_unary

    /**
     * Flag all samples to be exported to individual compressed PML files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_pml_gz <0|1>
     *      INI: export_pml_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pml_gz_binary

    /**
     * Flag all samples to be exported to bulk uncompressed PML files.
     * This is a unary, command-line only switch with usage: -export_pml_bulk
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pml_bulk_unary

    /**
     * Flag all samples to be exported to bulk uncompressed PML files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_pml_bulk <0|1>
     *      INI: export_pml_bulk = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pml_bulk_binary

    /**
     * Flag all samples to be exported to bulk compressed PML files.
     * This is a unary, command-line only switch with usage: -export_pml_bulk_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_pml_bulk_gz_unary

    /**
     * Flag all samples to be exported to bulk compressed PML files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_pml_bulk_gz <0|1>
     *      INI: export_pml_bulk_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_pml_bulk_gz_binary

    /**
     * Flag log weights to be exported to a single file. This is a unary,
     *  command-line only switch with usage: -export_log_weights
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_log_weights_unary

    /**
     * Flag log weights to be exported to a single file. This is a binary
     *  option which can be specified using INI or command-line, usage:
     *      command-line: --export_log_weights <0|1>
     *      INI: export_log_weights = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_log_weights_binary

    /**
     * Flag export of extended information about each chromatin node such as
     *  radius and locus identifier. Also emits nuclear radius. This is a
     *  unary, command-line only switch with usage: -export_extended
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_extended_unary

    /**
     * Flag export of extended information about each chromatin node such as
     *  radius and locus identifier. Also emits nuclear radius. This is a
     *  binary option which can be specified using INI or command-line, usage:
     *      command-line: --export_extended <0|1>
     *      INI: export_extended = <0|1>
     */
    , uOpt_export_extended_binary

    /**
     * Flag export of nuclear shell, MAY NOT BE supported by all export
     *  formats or nuclear shape types. This is a unary, command-line only
     *  switch with usage: -export_nucleus
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_nucleus_unary

    /**
     * Flag export of nuclear shell, MAY NOT BE supported by all export
     *  formats or nuclear shape types. This is a binary option which can be
     *  specified using INI or command-line, usage:
     *      command-line: --export_nucleus <0|1>
     *      INI: export_nucleus = <0|1>
     */
    , uOpt_export_nucleus_binary

    /**
     * Flag export of constrained chr-chr interactions at each sample. This
     *  is a unary, command-line only switch with usage: -export_intr_chr
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_intr_chr_unary

    /**
     * Flag export of constrained chr-chr interactions at each sample. This
     *  is a binary option which can be specified using INI or command-line.
     *  Usage:
     *      command-line: --export_intr_chr <0|1>
     *      INI: export_intr_chr = <0|1>
     */
    , uOpt_export_intr_chr_binary

    /**
     * Flag export of constrained lam-chr interactions at each sample. This
     *  is a unary, command-line only switch with usage: -export_intr_lam
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_intr_lam_unary

    /**
     * Flag export of constrained lam-chr interactions at each sample. This
     *  is a binary option which can be specified using INI or command-line.
     *  Usage:
     *      command-line: --export_intr_lam <0|1>
     *      INI: export_intr_lam = <0|1>
     */
    , uOpt_export_intr_lam_binary

    /**
     * Flag export of constrained nuclear body interactions at each sample.
     *  This is a unary, command-line only switch with usage:
     #      -export_intr_nucb
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_intr_nucb_unary

    /**
     * Flag export of constrained nuclear body interactions at each sample.
     *  This is a binary option with usage:
     *      command-line: --export_intr_nucb <0|1>
     *      INI: export_intr_nucb = <0|1>
     */
    , uOpt_export_intr_nucb_binary

    /**
     * Enables ligation contacts to be exported in plain-text CSV format. This
     *  is a unary, command-line only switch with usage: -export_ligc_csv
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_ligc_csv_unary

    /**
     * Enables ligation contacts to be exported in plain-text CSV format. This
     *  is a binary option which can be specified using INI or command-line.
     *  Usage:
     *      command-line: --export_ligc_csv <0|1>
     *      INI: export_ligc_csv = <0|1>
     */
    , uOpt_export_ligc_csv_binary

    /**
     * Enables ligation contacts to be exported in gzip compressed CSV format.
     * This is a unary, command-line only switch with usage:
     #      -export_ligc_csv_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
     , uOpt_export_ligc_csv_gz_unary

    /**
     * Enables ligation contacts to be exported in gzip compressed CSV format.
     *  This is a binary option which can be specified using INI or
     *  command-line.
     *  Usage:
     *      command-line: --export_ligc_csv_gz <0|1>
     *      INI: export_ligc_csv_gz = <0|1>
     */
    , uOpt_export_ligc_csv_gz_binary

    /**
     * Flag all ligation contacts to be exported to bulk uncompressed CSV files.
     * This is a unary, command-line only switch with usage: -export_ligc_csv_bulk
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_ligc_csv_bulk_unary

    /**
     * Flag all ligation contacts to be exported to bulk uncompressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_ligc_csv_bulk <0|1>
     *      INI: export_ligc_csv_bulk = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_ligc_csv_bulk_binary

    /**
     * Flag all ligation contacts to be exported to bulk compressed CSV files.
     * This is a unary, command-line only switch with usage: -export_ligc_csv_bulk_gz
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_ligc_csv_bulk_gz_unary

    /**
     * Flag all ligation contacts to be exported to bulk compressed CSV files.
     *  This is a binary option which can be specified using INI or
     *  command-line, usage:
     *      command-line: --export_ligc_csv_bulk_gz <0|1>
     *      INI: export_ligc_csv_bulk_gz = <0|1>
     *  where 0 -> no export, 1 -> enable export
     */
    , uOpt_export_ligc_csv_bulk_gz_binary

    /**
     * Enables ligation contacts to be exported in custom binary format. This
     *  is a unary, command-line only switch with usage: -export_ligc_bin
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_ligc_bin_unary

    /**
     * Enables ligation contacts to be exported in custom binary format. This
     *  is a binary option which can be specified using INI or command-line.
     *  Usage:
     *      command-line: --export_ligc_bin <0|1>
     *      INI: export_ligc_bin = <0|1>
     */
    , uOpt_export_ligc_bin_binary

    /**
     * Enable export of self (i,i) and bonded (i,i+1) ligations. This is a
     *  unary, command-line only switch with usage: -export_ligc_bonded
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_export_ligc_bonded_unary

    /**
     * Enable export of self (i,i) and bonded (i,i+1) ligations. This is a
     *  binary option which can be specified using INI or command-line. Usage:
     *      command-line: --export_ligc_bonded <0|1>
     *      INI: export_ligc_bonded = <0|1>
     */
    , uOpt_export_ligc_bonded_binary

    /**
     * Path to whitespace or comma delimited (CSV) file containing the bend
     * rigidity at each parent node in run-length-encoded (RLE) format. RLE
     * format follows the pattern:
     *
     *  <run : +integer> delim <rigidity : float>
     *  ...
     *
     * Where "run" specifies the number of consecutive nodes with the given
     * rigidity and 'delim' is any comma or any whitespace character. The sum
     * of the run values must match the total node count for all chains.
     * Rigidities are assumed to be in the same order as specified by
     * "num_nodes*" param.
     *
     * Example:
     *   6, 1.0
     *  12, 2.0
     *   7, -1.5
     *
     * This will assign the first 6 nodes to have rigidities 1.0, the next 12
     * nodes to have rigidities 2.0, and the last 7 nodes to have rigidities
     * -1.5.
     *
     * NOTE: FILE FORMAT EXPECTS A VALUE TO BE DEFINED AT ALL MONOMER NODES;
     * HOWEVER, THE VALUE AT THE FIRST AND LAST NODES OF EACH LOCUS CHAIN ARE
     * IGNORED BY SIMULATION.
     */
    , uOpt_energy_bend_rigidity_fpath

    /**
     * Scalar real-valued polymer stiffness assigned to all nodes. Higher
     * values simulate longer persistence|Kuhn lengths (i.e. less flexible
     * polymers). Ignored if 'energy_bend_rigidity_fpath' is specified.
     */
    , uOpt_energy_bend_rigidity

    /**
     * Real-valued scale constant for knock-in, chr-chr interaction failure
     * energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_chr_kin

    /**
     * Real-valued scale constant for knock-out, chr-chr interaction failure
     * energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_chr_ko

    /**
     * Real-valued scale constant for knock-in, lamina interaction failure
     * energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_lam_kin

    /**
     * Real-valued scale constant for knock-out, lamina interaction failure
     * energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_lam_ko

    /**
     * Real-valued scale constant for knock-in, nuclear body interaction
     * failure energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_nucb_kin

    /**
     * Real-valued scale constant for knock-out, nuclear body interaction
     * failure energy
     * @WARNING - THE ORDER OF THE FAIL SCALE OPTIONS MUST MATCH ORDER OF
     *  enum eEnerIntrFail in uSisIntrFailEnergySimLevelMixin.h!
     */
    , uOpt_energy_intr_fail_scale_nucb_ko

    /**
     * Broad phase collision grid has cells of dimension L x L x L. The
     * default value of L is twice the median node radius. Users can specify
     * a specific value of L through this parameter.
     */
    , uOpt_collision_grid_cell_length

    /**
     * Narrow phase collision overlap factor (ofa), must be >= 0. The value
     * '1' corresponds to canonical 'hard shell' collisions, '0' effectively
     * disables collisions, and value in (0,1) is 'soft shell' collision
     * according to percentage of monomer overlap. Only used if 'ofa'
     * collisions enabled.
     */
    , uOpt_collision_ofa

    /**
     * Selects schedule for use in quality control mixin
     * Options: homg_lag|hetr_lag|dynamic_ess
     * Several slots available if multiple schedules needed
     */
    , uOpt_qc_schedule_type_slot0
    , uOpt_qc_schedule_type_slot1
    , uOpt_qc_schedule_type_slot2
    , uOpt_qc_schedule_type_slot3
    , uOpt_qc_schedule_type_slot4
    , uOpt_qc_schedule_type_slot5
    , uOpt_qc_schedule_type_slot6
    , uOpt_qc_schedule_type_slot7

    /**
     * Maps to the actual lag value for use in quality control schedule
     * Only used if uOpt_qc_schedule_type == homg_lag|hetr_lag
     * Several slots available if multiple schedules needed.
     */
    , uOpt_qc_schedule_lag_slot0
    , uOpt_qc_schedule_lag_slot1
    , uOpt_qc_schedule_lag_slot2
    , uOpt_qc_schedule_lag_slot3
    , uOpt_qc_schedule_lag_slot4
    , uOpt_qc_schedule_lag_slot5
    , uOpt_qc_schedule_lag_slot6
    , uOpt_qc_schedule_lag_slot7

    /**
     * Path to run-length encoded (RLE) heterogeneous lag files
     * Only used if uOpt_qc_schedule_type == hetr_lag
     * Several slots available if multiple schedules needed.
     */
    , uOpt_qc_schedule_lag_file_slot0
    , uOpt_qc_schedule_lag_file_slot1
    , uOpt_qc_schedule_lag_file_slot2
    , uOpt_qc_schedule_lag_file_slot3
    , uOpt_qc_schedule_lag_file_slot4
    , uOpt_qc_schedule_lag_file_slot5
    , uOpt_qc_schedule_lag_file_slot6
    , uOpt_qc_schedule_lag_file_slot7

    /**
     * Maps to the threshold value for use in quality control schedule.
     * Will trigger a checkpoint if ess < thresh.
     * Only used if uOpt_qc_schedule_type == dynamic_ess
     * Several slots available if multiple schedules needed
     */
    , uOpt_qc_schedule_dynamic_ess_thresh_slot0
    , uOpt_qc_schedule_dynamic_ess_thresh_slot1
    , uOpt_qc_schedule_dynamic_ess_thresh_slot2
    , uOpt_qc_schedule_dynamic_ess_thresh_slot3
    , uOpt_qc_schedule_dynamic_ess_thresh_slot4
    , uOpt_qc_schedule_dynamic_ess_thresh_slot5
    , uOpt_qc_schedule_dynamic_ess_thresh_slot6
    , uOpt_qc_schedule_dynamic_ess_thresh_slot7

    /**
     * Selects sampler type for use in resampling
     * Options: power|<@TODO: add more types>
     * Several slots available
     */
    , uOpt_rs_sampler_type_slot0
    , uOpt_rs_sampler_type_slot1
    , uOpt_rs_sampler_type_slot2
    , uOpt_rs_sampler_type_slot3
    , uOpt_rs_sampler_type_slot4
    , uOpt_rs_sampler_type_slot5
    , uOpt_rs_sampler_type_slot6
    , uOpt_rs_sampler_type_slot7

    /**
     * Maps to the power which all weights are raised when using a power
     * sampler for sample selection (e.g. resampling, landmark collection)
     * Several slots available if multiple samplers needed
     */
    , uOpt_qc_sampler_power_alpha_slot0
    , uOpt_qc_sampler_power_alpha_slot1
    , uOpt_qc_sampler_power_alpha_slot2
    , uOpt_qc_sampler_power_alpha_slot3
    , uOpt_qc_sampler_power_alpha_slot4
    , uOpt_qc_sampler_power_alpha_slot5
    , uOpt_qc_sampler_power_alpha_slot6
    , uOpt_qc_sampler_power_alpha_slot7

    /**
     * Delphic look-ahead steps slots
     */
    , uOpt_delphic_steps_slot0
    , uOpt_delphic_steps_slot1
    , uOpt_delphic_steps_slot2
    , uOpt_delphic_steps_slot3
    , uOpt_delphic_steps_slot4
    , uOpt_delphic_steps_slot5
    , uOpt_delphic_steps_slot6
    , uOpt_delphic_steps_slot7

    /**
     * Delphic power selection/sampler slots
     */
    , uOpt_delphic_power_alpha_slot0
    , uOpt_delphic_power_alpha_slot1
    , uOpt_delphic_power_alpha_slot2
    , uOpt_delphic_power_alpha_slot3
    , uOpt_delphic_power_alpha_slot4
    , uOpt_delphic_power_alpha_slot5
    , uOpt_delphic_power_alpha_slot6
    , uOpt_delphic_power_alpha_slot7

    /**
     * Selects scale for use in rejection control QC mixin
     * Options: fixed_quantile | interp_weights
     */
    , uOpt_rc_scale_type

    /**
     * Sets the minimum quantile used for automatic checkpoint passage
     * Only for use with fixed quantile scale type
     */
    , uOpt_rc_scale_fixed_quantile

    /**
     * Sets the coefficient for the minimum weight within rejection control
     * scale mixin. Only used if uOpt_rc_scale_type == interp_weights. Scale
     * is computed as:
     * scale = c_min * min_weight + c_mean * mean_weight + c_max * max_weight
     */
    , uOpt_rc_scale_interp_weights_c_min

    /**
     * Sets the coefficient for the mean weight within rejection control
     * scale mixin. Only used if uOpt_rc_scale_type == interp_weights. Scale
     * is computed as:
     * scale = c_min * min_weight + c_mean * mean_weight + c_max * max_weight
     */
    , uOpt_rc_scale_interp_weights_c_mean

    /**
     * Sets the coefficient for the maximum weight within rejection control
     * scale mixin. Only used if uOpt_rc_scale_type == interp_weights. Scale
     * is computed as:
     * scale = c_min * min_weight + c_mean * mean_weight + c_max * max_weight
     */
    , uOpt_rc_scale_interp_weights_c_max

    /**
     * Overloads default ligation distance for ligation contact export
     */
    , uOpt_ligc_dist

    // BEGIN INTERACTION CONSTRAINT OPTIONS
    /**
     * Path to chr-chr interaction fragments file. Each line in file is
     * [start, end] 0-based index pair defining the node span of a single
     * chromatin fragment. Fragments may then serve as contact constraints
     * (see uOpt_chr_knock_in or uOpt_chr_knock_out for how to specify
     * constraints).
     */
    , uOpt_chr_frag

    /**
     * Path to knock-in chr-chr interaction constraints file. Each line in
     * file is a pair of 0-based indices, where each index is a line number in
     * the fragments file (see uOpt_chr_frag). Each index pair defines the
     * chromatin fragments that may be constrained to be no farther than
     * uOpt_chr_knock_in_dist Angstroms from each other.
     */
    , uOpt_chr_knock_in

    /**
     * Path to knock-out chr-chr interaction constraints file. Each line in
     * file is a pair of 0-based indices, where each index is a line number in
     * the fragments file (see uOpt_chr_frag). Each index pair defines the
     * chromatin fragments that may be constrained to be farther than
     * uOpt_chr_knock_out_dist Angstroms from each other.
     */
    , uOpt_chr_knock_out

    /**
     * Inclusive threshold distance in Angstroms for knock-in chr-chr
     * interaction constraints. Fragment regions of enforced knock-in
     * constraints cannot be separated by more than this distance. See also
     * defaults defined in uIntrLibDefs.h
     */
    , uOpt_chr_knock_in_dist

    /**
     * RLE file for heterogeneous knock-in distances at each chr-chr 
     * interaction KI constraint. If specified by user, then any
     * uOpt_chr_knock_in_dist argument is ignored.
     */
    , uOpt_chr_knock_in_dist_fpath

    /**
     * Exclusive threshold distance in Angstroms for knock-out chr-chr
     * interaction constraints. Fragment regions of enforced knock-out
     * constraints must be farther than this distance. See also defaults
     * defined uIntrLibDefs.h
     */
    , uOpt_chr_knock_out_dist

    /**
     * RLE file for heterogeneous knock-out distances at each chr-chr
     * interaction KO constraint. If specified by user, then any
     * uOpt_chr_knock_out_dist argument is ignored.
     */
    , uOpt_chr_knock_out_dist_fpath

    /**
     * INI/command line bit mask string for knock-in chr-chr interactions. Any
     * non-bit characters are stripped prior to bit conversion; however, if
     * whitespace, commas, or semi-colons present on command line, then entire
     * argument must be enclosed in quotes
     */
    , uOpt_chr_knock_in_bit_mask

    /**
     * Path to file containing bit mask string for knock-in chr-chr
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_chr_knock_in_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-in chr-chr
     * interactions. Delimiter is any non-integer character; however if
     * whitespace is present, then entire argument must be enclosed in quotes
     * ("). Index mask is an alternative to bit mask for specifying a subset
     * of interactions to enforce; however if any bit mask is present (via
     * command line or file), then index mask is ignored.
     */
    , uOpt_chr_knock_in_index_mask

    /**
     * Path to file containing index mask for knock-in chr-chr interactions,
     * ignored if command line index mask or any bit mask (command or file)
     * are present
     */
    , uOpt_chr_knock_in_index_mask_fpath

    /**
     * Command line bit mask string for knock-out chr-chr interactions. Any
     * non-bit characters are stripped prior to bit conversion; however, if
     * whitespace is present, then entire argument must be enclosed in quotes
     */
    , uOpt_chr_knock_out_bit_mask

    /**
     * Path to file containing bit mask string for knock-out chr-chr
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_chr_knock_out_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-out
     * chr-chr interactions. Delimiter is any non-integer character; however
     * if whitespace is present, then entire argument must be enclosed in
     * quotes ("). Index mask is an alternative to bit mask for specifying a
     * subset of interactions to enforce; however if any bit mask is present
     * (via command line or file), then index mask is ignored.
     */
    , uOpt_chr_knock_out_index_mask

    /**
     * Path to file containing index mask for knock-out chr-chr interactions,
     * ignored if command line index mask or any bit mask (command or file)
     * are present
     */
    , uOpt_chr_knock_out_index_mask_fpath

    /**
     * Proportion [0,1] of chr-chr knock-in interactions that can fail
     */
    , uOpt_chr_knock_in_fail_budget

    /**
     * Proportion [0,1] of chr-chr knock-out interactions that can fail
     */
    , uOpt_chr_knock_out_fail_budget

    /**
     * Path to lam-chr interaction fragments file. Each line in file is
     * [start, end] 0-based index pair defining the node span of a single
     * chromatin fragment. Fragments may then participate in lamina
     * constraints (see uOpt_lam_knock_in or uOpt_lam_knock_out for how to
     * specify constraints).
     */
    , uOpt_lam_frag

    /**
     * Path to knock-in lam-chr interaction constraints file. Each entry is a
     * 0-based index, where each index is a line number in the fragments file
     * (see uOpt_lam_frag). Each index defines the chromatin fragment that may
     * be constrained to be no farther than uOpt_lam_knock_in_dist Angstroms
     * from the nuclear lamina.
     */
    , uOpt_lam_knock_in

    /**
     * Path to knock-out lam-chr interaction constraints file. Each entry is a
     * 0-based index, where each index is a line number in the fragments file
     * (see uOpt_lam_frag). Each index defines the chromatin fragment that may
     * be constrained to be farther than uOpt_lam_knock_out_dist Angstroms
     * from the nuclear lamina.
     */
    , uOpt_lam_knock_out

    /**
     * Inclusive threshold distance in Angstroms for knock-in lam-chr
     * interaction constraints. Fragment regions of enforced knock-in
     * constraints cannot be separated by more than this distance from the
     * nuclear lamina. See also defaults defined in uIntrLibDefs.h
     */
    , uOpt_lam_knock_in_dist

    /**
     * RLE file for heterogeneous knock-in distances at each lam-chr 
     * interaction KI constraint. If specified by user, then any
     * uOpt_lam_knock_in_dist argument is ignored.
     */
    , uOpt_lam_knock_in_dist_fpath

    /**
     * Exclusive threshold distance in Angstroms for knock-out lam-chr
     * interaction constraints. Fragment regions of enforced knock-out
     * constraints must be farther than this distance from the nuclear lamina.
     * See also defaults defined uIntrLibDefs.h
     */
    , uOpt_lam_knock_out_dist

    /**
     * RLE file for heterogeneous knock-out distances at each lam-chr
     * interaction KO constraint. If specified by user, then any
     * uOpt_lam_knock_out_dist argument is ignored.
     */
    , uOpt_lam_knock_out_dist_fpath

    /**
     * INI/command line bit mask string for knock-in lam-chr interactions. Any
     * non-bit characters are stripped prior to bit conversion; however, if
     * whitespace, commas, or semi-colons present on command line, then entire
     * argument must be enclosed in quotes
     */
    , uOpt_lam_knock_in_bit_mask

    /**
     * Path to file containing bit mask string for knock-in lam-chr
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_lam_knock_in_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-in lam-chr
     * interactions. Delimiter is any non-integer character; however if
     * whitespace is present, then entire argument must be enclosed in quotes
     * ("). Index mask is an alternative to bit mask for specifying a subset
     * of interactions to enforce; however if any bit mask is present (via
     * command line or file), then index mask is ignored.
     */
    , uOpt_lam_knock_in_index_mask

    /**
     * Path to file containing index mask for knock-in lam-chr interactions,
     * ignored if command line index mask or any bit mask (command or file)
     * are present
     */
    , uOpt_lam_knock_in_index_mask_fpath

    /**
     * Command line bit mask string for knock-out lam-chr interactions. Any
     * non-bit characters are stripped prior to bit conversion; however, if
     * whitespace is present, then entire argument must be enclosed in quotes
     */
    , uOpt_lam_knock_out_bit_mask

    /**
     * Path to file containing bit mask string for knock-out lam-chr
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_lam_knock_out_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-out
     * lam-chr interactions. Delimiter is any non-integer character; however
     * if whitespace is present, then entire argument must be enclosed in
     * quotes ("). Index mask is an alternative to bit mask for specifying a
     * subset of interactions to enforce; however if any bit mask is present
     * (via command line or file), then index mask is ignored.
     */
    , uOpt_lam_knock_out_index_mask

    /**
     * Path to file containing index mask for knock-out lam-chr interactions,
     * ignored if command line index mask or any bit mask (command or file)
     * are present
     */
    , uOpt_lam_knock_out_index_mask_fpath

    /**
     * Proportion [0,1] of lamina knock-in interactions that can fail
     */
    , uOpt_lam_knock_in_fail_budget

    /**
     * Proportion [0,1] of lamina knock-out interactions that can fail
     */
    , uOpt_lam_knock_out_fail_budget

    /**
     * Path to nuclear body interaction fragments file. Each line in file is
     * [start, end] 0-based index pair defining the node span of a single
     * chromatin fragment. Fragments may then participate in nuclear body
     * constraints (see uOpt_nucb_knock_in or uOpt_nucb_knock_out for how to
     * specify constraints).
     */
     , uOpt_nucb_frag

    /**
     * Path to knock-in nuclear body interaction constraints file. Each line
     * in file is a pair of 0-based indices, where first index is a line
     * number into the the fragments file (see uOpt_nucb_frag) and second
     * index is a line number of into the body centers file (see
     * uOpt_nucb_body_centers). Each index pair defines a chromatin fragment
     * that may be constrained to be no farther than uOpt_nucb_knock_in_dist
     * Angstroms (surface-to-surface) from the the matched nuclear body.
     */
    , uOpt_nucb_knock_in

    /**
     * Path to knock-out nuclear body interaction constraints file. Each line
     * in file is a pair of 0-based indices, where first index is a line
     * number into the the fragments file (see uOpt_nucb_frag) and second
     * index is a line number of into the body centers file (see
     * uOpt_nucb_body_centers). Each index pair defines a chromatin fragment
     * that may be constrained to be farther than uOpt_nucb_knock_out_dist
     * Angstroms (surface-to-surface) from the the matched nuclear body.
     */
    , uOpt_nucb_knock_out

    /**
     * Inclusive surface-to-surface threshold distance in Angstroms for
     * knock-in nuclear body interaction constraints. At least one bead in
     * each constrained fragment region must be within this distance from
     * matched nuclear bodies. See also defaults defined in uIntrLibDefs.h
     */
    , uOpt_nucb_knock_in_dist

    /**
     * RLE file for heterogeneous knock-in distances at each nuclear body
     * interaction KI constraint. If specified by user, then any
     * uOpt_nucb_knock_in_dist argument is ignored.
     */
    , uOpt_nucb_knock_in_dist_fpath

    /**
     * Exclusive surface-to-surface threshold distance in Angstroms for
     * knock-out nuclear body interaction constraints. Fragment regions of
     * enforced knock-out constraints must be farther than this distance from
     * matched nuclear bodies. See also defaults defined uIntrLibDefs.h
     */
    , uOpt_nucb_knock_out_dist

    /**
     * RLE file for heterogeneous knock-out distances at each nuclear body
     * interaction KO constraint. If specified by user, then any
     * uOpt_nucb_knock_out_dist argument is ignored.
     */
    , uOpt_nucb_knock_out_dist_fpath

    /**
     * INI/command line bit mask string for knock-in nuclear body
     * interactions. Any non-bit characters are stripped prior to bit
     * conversion; however, if whitespace, commas, or semi-colons present on
     * command line, then entire argument must be enclosed in quotes
     */
    , uOpt_nucb_knock_in_bit_mask

    /**
     * Path to file containing bit mask string for knock-in nuclear body
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_nucb_knock_in_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-in nuclear
     * body interactions. Delimiter is any non-integer character; however if
     * whitespace is present, then entire argument must be enclosed in quotes
     * ("). Index mask is an alternative to bit mask for specifying a subset
     * of interactions to enforce; however if any bit mask is present (via
     * command line or file), then index mask is ignored.
     */
    , uOpt_nucb_knock_in_index_mask

    /**
     * Path to file containing index mask for knock-out nuclear body
     * interactions, ignored if command line index mask or any bit mask
     * (command or file) are present
     */
    , uOpt_nucb_knock_in_index_mask_fpath

    /**
     * Command line bit mask string for knock-out nuclear body interactions.
     * Any non-bit characters are stripped prior to bit conversion; however,
     * if whitespace is present, then entire argument must be enclosed in
     * quotes
     */
    , uOpt_nucb_knock_out_bit_mask

    /**
     * Path to file containing bit mask string for knock-out nuclear body
     * interactions, ignored if command line bit mask is present
     */
    , uOpt_nucb_knock_out_bit_mask_fpath

    /**
     * INI/Command-line string for set of indices to mask for knock-out
     * nuclear body interactions. Delimiter is any non-integer character;
     * however if whitespace is present, then entire argument must be enclosed
     * in quotes ("). Index mask is an alternative to bit mask for specifying
     * a subset of interactions to enforce; however if any bit mask is present
     * (via command line or file), then index mask is ignored.
     */
    , uOpt_nucb_knock_out_index_mask

    /**
     * Path to file containing index mask for knock-out nuclear body
     * interactions, ignored if command line index mask or any bit mask
     * (command or file) are present
     */
    , uOpt_nucb_knock_out_index_mask_fpath

    /**
     * Proportion [0,1] of nuclear body knock-in interactions that can fail
     */
    , uOpt_nucb_knock_in_fail_budget

    /**
     * Proportion [0,1] of nuclear body knock-out interactions that can fail
     */
    , uOpt_nucb_knock_out_fail_budget

    /**
     * Path to nuclear body (x, y, z) centers file. Each line in file is a
     * whitespace or comma-delimited (x, y, z) or (x, y, z, d) tuple with body
     * center and optional diameter, lines may be heterogeneous (i.e. file may
     * be mixture of 3-tuples and 4-tuples). These bodies may then be used as
     * nuclear body constraints (see uOpt_nucb_knock_in or uOpt_nucb_knock_out
     * for how to specify constraints). Any specified diameters via this
     * option will be overloaded by uOpt_nucb_diameter* options if present.
     */
    , uOpt_nucb_centers

    /**
     * [Optional] Scalar homogeneous diameter (default = 0) applied to all
     * nuclear bodies, ignored if diameter file present
     */
    , uOpt_nucb_diameter

    /**
     * [Optional] Comma or whitespace delimited nuclear body diameter file
     * (non-rle), must have same number of entries as corresponding
     * 'centers' file
     */
    , uOpt_nucb_diameter_fpath

    /**
     * [Optional] Run length encoded (rle) nuclear body diameter file, ignored
     * if non-rle file is specified
     */
    , uOpt_nucb_diameter_fpath_rle

    /**
     * @DEPRECATED - USE BIT OR INDEX MASK WITH MKI MKO MLOC INTERACTION MIXIN
     * 0-based index representing line in knock-in constraints file (see
     * uOpt_chr_knock_in). For single knock-in (SKI) experiments, this specifies
     * which of the knock-in constraints to enforce (all other constraints are
     * are ignored).
     */
    , uOpt_ski_ix
// END INTERACTION CONSTRAINT OPTIONS

#ifdef U_BUILD_ENABLE_COMMANDLETS
    /**
     * Prints basic information about program.
     */
    , uOpt_cmdlet_info

    /**
     * Calculate ligation contacts
     */
    , uOpt_cmdlet_ligc_calc

    /**
     * Ligation contact input directory containing polymer geometry. User may
     * specify this option to override default: "<output_dir>/csv"
     */
    , uOpt_ligc_calc_import_dir

    /**
     * Ligation contact output directory for storing ligation tuples. User may
     * specify this option to override default:
     *
     *  "<output_dir>/ligc<.fmt>", where "<.fmt>" is the exported data format
     *
     * Note, the export format(s) will be appended to output folder. For
     * example, if "export_ligc_csv = 1", then the derived output folder
     * will be: "<ligc_calc_export_dir>.csv"
     */
    , uOpt_ligc_calc_export_dir

    /**
     * Ligc population distribution commandlet
     */
    , uOpt_cmdlet_ligc_popd

    /**
     * Number of bins in contact matrix, defaults to total polymer node count
     */
    , uOpt_ligc_popd_num_bins

    /**
     * Ligc population distribution bootstrap (BLB) outer replicates
     */
    , uOpt_ligc_popd_blb_repo

    /**
     * Ligc population distribution bootstrap (BLB) inner replicates
     */
    , uOpt_ligc_popd_blb_repi

    /**
     * Ligc population distribution bootstrap (BLB) exponent in [0,1] such
     * that n^nexp (where n is total population size after outliers removed)
     * defines the number of unique data samples per outer BLB replicate
     */
    , uOpt_ligc_popd_blb_nexp

    /**
     * [Optional] For threaded-builds only, defines the number of inner
     * bootstrap (BLB) replicates processed by each thread when grabbing work
     */
    , uOpt_ligc_popd_blb_chunk_size

    /**
     * Unary command-line only switch, if present, ligc samples with weights
     *  beyond [Q1 - 1.5 * IQR, Q3 + 1.5 * IQR] will be excluded
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_exclude_outliers_unary

   /**
     * Binary command-line only switch, if present, ligc samples with weights
     *  beyond [Q1 - 1.5 * IQR, Q3 + 1.5 * IQR] will be excluded. Usage:
     *      command-line: --ligc_popd_exclude_outliers <0|1>
     *      INI: ligc_popd_exclude_outliers = <0|1>
     */
    , uOpt_ligc_popd_exclude_outliers_binary

    /**
     * Ligc population distribution input directory containing ligc files.
     * User may use this option to override defaults according to first found:
     *  "<output_dir>/ligc.bin|ligc.csv.gz|ligc.csv"
     */
    , uOpt_ligc_popd_import_dir

    /**
     * Ligc population distribution output directory containing bootstrap
     * (BLB) replicates. User may specify this option to override default:
     *
     *       "<output_dir>/ligc.popd<.fmt>", where "<.fmt>" is the exported
     *           data format
     *
     * Note, the export format(s) will be appended to the output folder. For
     * example, if "ligc_popd_export_ltri_csv = 1", then the derived output
     * will be: "<ligc_popd_export_dir>.ltri.csv"
     */
    , uOpt_ligc_popd_export_dir

    /**
     * Enables ligc population distribution export of each bootstrap (BLB)
     * replicate's aggregated contact matrix as a COOrdinate matrix, each row
     * is an (i,j,v) == (row,column,value) triplet in CSV format
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_coo_csv_unary

    /**
     * Binary switch for ligc population distribution export of each bootstrap
     * BLB) replicate's aggregated contact matrix as COOrdinate matrix, each
     * row is an (i,j,v) == (row,column,value) CSV triplet
     *  Usage:
     *      command-line: --ligc_popd_export_coo_csv <0|1>
     *      INI: ligc_popd_export_coo_csv = <0|1>
     */
    , uOpt_ligc_popd_export_coo_csv_binary

    /**
     * Enables ligc population distribution export of each bootstrap (BLB)
     * replicate's aggregated contact matrix as a COOrdinate matrix, each row
     * is an (i,j,v) == (row,column,value) triplet in gzip-compressed CSV
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_coo_csv_gz_unary

    /**
     * Binary switch for ligc population distribution export of each bootstrap
     * BLB) replicate's aggregated contact matrix as COOrdinate matrix, each
     * row is an (i,j,v) == (row,column,value) gzip-compressed CSV triplet
     *  Usage:
     *      command-line: --ligc_popd_export_coo_csv_gz <0|1>
     *      INI: ligc_popd_export_coo_csv_gz = <0|1>
     */
    , uOpt_ligc_popd_export_coo_csv_gz_binary

    /**
     * Enables ligc population distribution export of each bootstrap (BLB)
     * replicate's aggregated contact matrix as a banded matrix (each row is a
     * diagonal of the matrix) in CSV format
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_lband_csv_unary

    /**
     * Binary switch for ligc population distribution export of each bootstrap
     * BLB) replicate's aggregated contact matrix as banded matrix (each row
     * is a diagonal of the matrix) as CSV
     *  Usage:
     *      command-line: --ligc_popd_export_lband_csv <0|1>
     *      INI: ligc_popd_export_lband_csv = <0|1>
     */
    , uOpt_ligc_popd_export_lband_csv_binary

    /**
     * Enables ligc population distribution export of each bootstrap (BLB)
     * replicate's aggregated contact matrix as a banded matrix (each row is a
     * diagonal of the matrix) in gzip-compressed CSV format
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_lband_csv_gz_unary

    /**
     * Binary switch for ligc population distribution export of each bootstrap
     * BLB) replicate's aggregated contact matrix as banded matrix (each row
     * is a diagonal of the matrix) as gzip-compressed CSV
     *  Usage:
     *      command-line: --ligc_popd_export_lband_csv_gz <0|1>
     *      INI: ligc_popd_export_lband_csv_gz = <0|1>
     */
    , uOpt_ligc_popd_export_lband_csv_gz_binary

    /**
     * Enables ligc population distribution export of lower triangle of each
     * bootstrap (BLB) replicate's aggregated contact matrix in CSV format
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_ltri_csv_unary

    /**
     * Binary switch for ligc population distribution export of lower triangle
     * of each bootstrap (BLB) replicate's aggregated contact matrix as CSV
     *  Usage:
     *      command-line: --ligc_popd_export_ltri_csv <0|1>
     *      INI: ligc_popd_export_ltri_csv = <0|1>
     */
    , uOpt_ligc_popd_export_ltri_csv_binary

    /**
     * Enables ligc population distribution export of lower triangle of each
     * bootstrap (BLB) replicate's aggregated contact matrix in gzip-
     * compressed CSV format
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_ltri_csv_gz_unary

    /**
     * Binary switch for ligc population distribution export of lower triangle
     * of each bootstrap (BLB) replicate's aggregated contact matrix as gzip-
     * compressed CSV
     *  Usage:
     *      command-line: --ligc_popd_export_ltri_csv_gz <0|1>
     *      INI: ligc_popd_export_ltri_csv_gz = <0|1>
     */
    , uOpt_ligc_popd_export_ltri_csv_gz_binary

    /**
     * Enable export of self (i,i) and bonded (i,i+1) ligations within ligc
     *  population distribution aggregated contact matrices. This is a unary,
     *  command-line only switch with usage: --ligc_popd_export_bonded
     * NOTE: UNARY OPTIONS HAVE PRECEDENCE OVER BINARY OPTIONS!
     */
    , uOpt_ligc_popd_export_bonded_unary

    /**
     * Enable export of self (i,i) and bonded (i,i+1) ligations within ligc
     *  population distribution aggregated contact matrices. This is a binary
     *  option which can be specified using INI or command-line. Usage:
     *      command-line: --ligc_popd_export_bonded <0|1>
     *      INI: ligc_popd_export_bonded = <0|1>
     */
    , uOpt_ligc_popd_export_bonded_binary

    /**
     * Commandlet for tuning energy bend rigidity
     */
    , uOpt_cmdlet_tune_bend_rigidity

    /**
     * Writes to console Rgl script code for plotting node sphere points.
     */
    , uOpt_cmdlet_sphere_surface_rgl

    /**
     * Benchmarks (profiles) growth of a simple, single locus chromatin chain
     * grown from a single end using Boltzmann bend energy distribution
     */
    , uOpt_cmdlet_benchmark_sis_bend_seo_sloc_growth

    /**
     * Benchmarks (profiles) growth of a simple, single locus chromatin chain
     * grown from a single end using uniform sampling
     */
    , uOpt_cmdlet_benchmark_sis_unif_seo_sloc_growth

    /**
     * Runs benchmark commandlet. Same as running unif single end growth
     * benchmark. Usage: <path_to_exe> -benchmark Note: for brevity this does
     * not follow usual commandlet pattern where the command line contains
     * "commandlet_" prefix
     */
    , uOpt_benchmark

#endif  // U_BUILD_ENABLE_COMMANDLETS

#ifdef U_BUILD_ENABLE_TESTS
    /**
     * Possible vectorized argument which may be used by tests, meant to specify
     * path to child test configurations
     */
    , uOpt_test_child_conf

#ifdef U_BUILD_ENABLE_COMMANDLETS
    /**
     * Test ligation contact calculations
     */
    , uOpt_test_ligc_calc

    /**
     * Test ligation contact bootstrap (BLB) distribution
     */
    , uOpt_test_ligc_popd
#endif  // U_BUILD_ENABLE_COMMANDLETS

    /**
     * Tests smoothed cubic splines
     */
    , uOpt_test_csppline

    /**
     * Tests random number generator
     */
    , uOpt_test_rng

    /**
     * Tests hash interface
     */
    , uOpt_test_hash

    /**
     * Tests ellipsoidal nucleus
     */
    , uOpt_test_sis_ellipnuc

    /**
     * Tests overlap factor collisions
     */
    , uOpt_test_sis_ofa_collision

    /**
     * Tests homogeneous uniform lag schedule for quality control mixin
     */
    , uOpt_test_sis_homg_unif_lag_qc_sched

    /**
     * Tests homogeneous uniform lag schedule for trial runner mixin
     */
    , uOpt_test_sis_homg_unif_lag_tr_sched

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure budget
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure budget
     */
    , uOpt_test_sis_mki_mko_seo_intr_chr_mloc_homg_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_sloc_homg_budg
 
    /**
     * Tests MLOC multi-constraint knock-in/knock-out chrome-to-chrome
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_chr_mloc_homg_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_lam_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_lam_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_lam_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_lam_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_lam_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_lam_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_lam_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_lam_mloc_homg_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_lam_sph_mloc_homg_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes
     */
    , uOpt_test_sis_seo_ellip_intr_lam_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes
     */
    , uOpt_test_sis_seo_ellip_intr_lam_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes
     */
    , uOpt_test_sis_seo_ellip_intr_lam_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes
     */
    , uOpt_test_sis_seo_ellip_intr_lam_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_ellip_intr_lam_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_ellip_intr_lam_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_ellip_intr_lam_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out lamina interactions with
     * homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_ellip_intr_lam_mloc_homg_budg

    /**
    * Tests SLOC multi-constraint knock-in/knock-out nuclear body interactions
    * with heterogeneous nodes
    */
    , uOpt_test_sis_seo_sphere_intr_nucb_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure budget
     */
    , uOpt_test_sis_seo_sphere_intr_nucb_mloc_homg_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_hetr

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_hetr

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_homg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_homg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_hetr_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with heterogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_hetr_budg

    /**
     * Tests SLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_sloc_homg_budg

    /**
     * Tests MLOC multi-constraint knock-in/knock-out nuclear body
     * interactions with homogeneous nodes and failure budget + energy
     */
    , uOpt_test_sis_intr_fail_ener_nucb_sph_mloc_homg_budg

    /**
     * Tests growth of a simple, multiple loci chromatin chain
     */
    , uOpt_test_sis_mloc_growth

    /**
     * Tests landmark trial runner using rejection control
     */
    , uOpt_test_sis_lmrk_rc

    /**
     * Tests nested landmark trial runners using resampling and rejection
     * control
     */
    , uOpt_test_sis_lmrk_rs_rc

    /**
     * Tests nested landmark trial runners using nested resampling
     */
    , uOpt_test_sis_lmrk_rs_rs

    /**
     * Tests landmark trial runner using resampling
     */
    , uOpt_test_sis_lmrk_rs

    /**
     * Tests landmark trial with Delphic power selection
     */
    , uOpt_test_sis_lmrk_sel_del_pow

    /**
     * Tests growth of a single locus chromatin chain but with rejection
     * control algorithm used for population level quality control.
     */
    , uOpt_test_sis_rejection_control_qc

    /**
     * Tests growth of a single locus chromatin chain but with partial
     * rejection control algorithm used for population level quality control.
     */
    , uOpt_test_sis_partial_rejection_control_qc

    /**
     * Tests growth of a single locus chromatin chain but with power resampling
     * used for population level quality control.
     */
    , uOpt_test_sis_power_resampling_qc

    /**
     * Tests growth of a single locus chromatin chain but with residual
     * resampling used for population level quality control.
     */
    , uOpt_test_sis_residual_resampling_qc

    /**
     * Tests growth of a single locus chromatin chain but with Delphic power
     * resampling used for population level quality control.
     */
    , uOpt_test_sis_delphic_power_resampling_qc

    /**
     * Tests growth of a single chrome-to-chrome knock-in constraint.
     */
    , uOpt_test_sis_ski_seo_sloc_intr_chr

    /**
     * Tests CSV export of a small simulation.
     */
    , uOpt_test_csv

    /**
     * Tests compressed CSV export of a small simulation
     */
    , uOpt_test_csv_gz

    /**
     * Tests bulk CSV export of a small simulation.
     */
    , uOpt_test_csv_bulk

    /**
     * Tests bulk compressed CSV export of a small simulation
     */
    , uOpt_test_csv_bulk_gz

    /**
     * Tests PDB export of a small simulation.
     */
    , uOpt_test_pdb

    /**
     * Tests compressed PDB export of a small simulation
     */
    , uOpt_test_pdb_gz

    /**
     * Tests bulk PDB export of a small simulation.
     */
    , uOpt_test_pdb_bulk

    /**
     * Tests bulk compressed PDB export of a small simulation
     */
    , uOpt_test_pdb_bulk_gz

    /**
     * Tests PML export of a small simulation
     */
    , uOpt_test_pml

    /**
     * Tests compressed PML export of a small simulation
     */
    , uOpt_test_pml_gz

    /**
     * Tests bulk PML export of a small simulation.
     */
    , uOpt_test_pml_bulk

    /**
     * Tests bulk compressed PML export of a small simulation
     */
    , uOpt_test_pml_bulk_gz

    /**
     * Tests ligation contact CSV export of a small simulation
     */
    , uOpt_test_ligc_csv

    /**
     * Tests ligation contact compressed CSV export of a small simulation
     */
    , uOpt_test_ligc_csv_gz

    /**
     * Tests bulk ligation contact CSV export of a small simulation
     */
    , uOpt_test_ligc_csv_bulk

    /**
     * Tests bulk ligation contact compressed CSV export of a small simulation
     */
    , uOpt_test_ligc_csv_bulk_gz

    /**
     * Tests ligation contact binary export of a small simulation
     */
    , uOpt_test_ligc_bin

    /**
     * Tests energy bend rigidity (ebr) tuner
     */
    , uOpt_test_ebr_tuner

    /**
     * Tests zstr [de]compress
     */
    , uOpt_test_zstr

#ifdef U_BUILD_ENABLE_THREADS
    /**
     * Tests simple use case for parallel mapper
     */
    , uOpt_test_parallel_mapper
#endif  // U_BUILD_ENABLE_THREADS

#endif  // U_BUILD_ENABLE_TESTS

#if defined(U_BUILD_ENABLE_COMMANDLETS) || defined(U_BUILD_ENABLE_TESTS)
    /**
     * "ebr" == energy bend rigidity
     * Real-valued scalar number of base pairs defining the chromatin
     * "decorrelation" length (e.g. Kuhn length or persistence length), as
     * used by ebr tuner. This value defines the number of base pairs such
     * that the expected mean cosine is the target value as defined by
     * uOpt_ebr_tuner_decorr_cos (typically decorr_cos is 0.0).
     */
    , uOpt_ebr_tuner_decorr_bp

    /**
     * "ebr" == energy bend rigidity
     * Path to run-length encoded file with number of base pairs represented
     * by each monomer node, as used by ebr tuner. Specification is exactly
     * the same as uOpt_node_diameter_fpath except value column now defines
     * number of base pairs at each node rather than Euclidean length.
     */
    , uOpt_ebr_tuner_node_bp_fpath

    /**
     * "ebr" == energy bend rigidity
     * Path to write tuned bend rigidities. Values are written in run-length
     * encoded (RLE) format. The output file may then serve as a simulation
     * input by setting uOpt_energy_bend_rigidity_fpath to this file path.
     */
    , uOpt_ebr_tuner_out_fpath

    /**
     * "ebr" == energy bend rigidity
     * Maximum number of iterations for stochastic approximation
     */
    , uOpt_ebr_tuner_max_iters

    /**
     * "ebr" == energy bend rigidity
     * Step-size decay exponent used for stochastic approximation. Exponent
     * must be in (0, +inf). However, if using Polyak-Ruppert averaging
     * (m_should_avg = 1), then decay exponent must be in (0,1). Note, this is
     * called a rate simply because higher values lead to faster step decay.
     */
    , uOpt_ebr_tuner_step_decay_rate

    /**
     * "ebr" == energy bend rigidity
     * Constant scale factor applied to each stochastic approximation step,
     * must be positive and defaults to 1.0
     */
    , uOpt_ebr_tuner_step_scale

    /**
     * "ebr" == energy bend rigidity
     * Smoothing factor in [0, 1], used for dampening oscillations in step size.
     * Final step size is the weighted average of (1-lamba) * regular_step +
     * lamda * smoothed_step. Smoothing is performed by averaging target value
     * over last 'smooth count' time frames. Default configuration is to
     * disable this feature by setting lambda = 0.
     */
    , uOpt_ebr_tuner_step_smooth_lambda

    /**
     * "ebr" == energy bend rigidity
     * Smoothing size, must be at least 1, determines number of time frames to
     * use for computing the smoothed target value.
     */
    , uOpt_ebr_tuner_step_smooth_count

    /**
     * "ebr" == energy bend rigidity
     * Start iteration (0-based)  for when smoothing should be applied, it
     * will be applied for all subsequent trials (inclusive). This may help
     * converge slightly faster by smoothing the last 'count' trials.
     */
    , uOpt_ebr_tuner_step_smooth_start_iter

    /**
     * "ebr" == energy bend rigidity
     * If set to "1", then Polyak-Ruppert averaging will be applied after
     * stochastic approximation pass has finished, "0" otherwise
     */
    , uOpt_ebr_tuner_should_avg

    /**
     * "ebr" == energy bend rigidity
     * If set to "1", then the mean cosine at the the target genomic
     * decorrelation length is interpolated using bond length multiplicities,
     * else if "0", unweighted interpolation is used.
     */
    , uOpt_ebr_tuner_use_weighted_interp

    /**
     * Target mean cosine such that nodes are considered de-correlated with
     * respect to their bond angles (e.g. the base pair length at which
     * decorr_cos == 0.0 would define the value of the persistence length).
     * MUST BE IN RANGE [-1 to 1]. DEFAULTS TO 0.0
     */
    , uOpt_ebr_tuner_decorr_cos

    /**
     * ebr" == energy bend rigidity
     * Minimum bending rigidity constant allowed by tuner. If not specified,
     * will default to -20. MUST BE < ebr_tuner_rigid_max
     */
    , uOpt_ebr_tuner_rigid_min

    /**
     * ebr" == energy bend rigidity
     * Maximum bending rigidity constant allowed by tuner. If not specified,
     * will default to +20. MUST BE > ebr_tuner_rigid_max
     */
    , uOpt_ebr_tuner_rigid_max

    /**
     * "ebr" == energy bend rigidity
     * [Optional] convergence threshold for ebr tuner. If successive values of
     * the stochastic approximation root finding algorithm are within this
     * threshold tolerance, then convergence is assumed.
     */
    , uOpt_ebr_tuner_conv_thresh

    /**
     * "ebr" == energy bend rigidity
     * [Optional] successive values of the stochastic approximation root must
     * be within conv_thresh for these many iterations to be considered as
     * converged.
     */
    , uOpt_ebr_tuner_conv_iters

    /**
     * "ebr" == energy bend rigidity
     * [Optional] number of iterations tuner performs before switching bond
     * types. Tuner round-robin samples between bond types until convergence
     */
    , uOpt_ebr_tuner_bond_iters

    /**
     * "ebr" == energy bend rigidity
     * [Optional] logging interval used by ebr tuner to report current value
     * of bending rigidity constant and other tuner status details.
     */
    , uOpt_ebr_tuner_log_interval
#endif  // defined(U_BUILD_ENABLE_COMMANDLETS) || defined(U_BUILD_ENABLE_TESTS)

    /**
     * Total number of options, not an actual option
     */
    , uOpt_NUM_OPTS
};

/**
 * Info containing the option key, switch, and help description
 */
struct uOptInfo {
    /**
     * The key string for use in INI
     */
    const char* ini_key;

    /**
     * The key string for use on the command line
     */
    const char* cmd_switch;

    /**
     * The description output when user types '--help'
     */
    const char* desc;
};

/**
 * Array of option info structures. Array is indexed according to uOpt enum.
 */
extern const struct uOptInfo U_OPT_INFOS[uOpt_NUM_OPTS];

/**
 * Callback to print usage for a single switch
 */
extern void uOpt_print_usage(const uOptE ix);

/**
 * Callback for --help switch
 */
extern void uOpt_print_usage_all();

/**
 * Accessor for returning the ini key associated with option
 *
 * @param ix - enumerated index of option
 * @return the ini key string associated with option
 */
inline const char* uOpt_get_ini_key(const enum uOptE ix) {
    uAssertBounds(ix, 0, uOpt_NUM_OPTS);
    return U_OPT_INFOS[ix].ini_key;
}

/**
 * Accessor for returning the command line switch associated with option
 *
 * @param ix - enumerated index of option
 * @return the command line switch string associated with option
 */
inline const char* uOpt_get_cmd_switch(const enum uOptE ix) {
    uAssertBounds(ix, 0, uOpt_NUM_OPTS);
    return U_OPT_INFOS[ix].cmd_switch;
}

/**
 * Function pointer to accessor function
 * Makes it easier for client functions to remain agnostic of INI vs command
 * line keys
 */
typedef const char* (*uOpt_get_key_func)(const enum uOptE);

#endif  // uOpts_h
