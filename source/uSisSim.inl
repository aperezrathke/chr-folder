//****************************************************************************
// uSisSim.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef uSisSim_inl
#error "Sis Sim implementation included multiple times!"
#endif  // uSisSim_inl
#define uSisSim_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uSisUtilsQc.h"
#include "uSphereSurfacePointSampler.h"
#include "uStats.h"
#include "uThread.h"

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Initializes from config object
 * @param configuration object specifying simulation parameters
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::init(uSpConstConfig_t config U_THREAD_ID_PARAM) {
    uAssert(config->is_valid());

    // Wipe slate if necessary
    clear();

    // Store configuration, needed for resetting trials
    m_config = config;

    // Initialize maximum total node count (sum of all locus chains)
    uAssert(0 == m_max_total_num_nodes);
    for (uUInt i = 0; i < config->num_nodes.size(); ++i) {
        m_max_total_num_nodes += config->num_nodes[i];
    }

    // Many mixins assume > 1 total node count
    if (m_max_total_num_nodes <= U_TO_UINT(1)) {
        uLogf(
            "Error: Simulation requires total polymer node count > 1. "
            "Exiting.\n");
        exit(uExitCode_error);
    }

    // Initialize grow chunk size (threaded builds only)
#ifdef U_BUILD_ENABLE_THREADS
    {
        uUInt num_threads = U_DEFAULT_NUM_THREADS;
        config->read_into(num_threads, uOpt_num_worker_threads);
        uAssert(num_threads > U_TO_UINT(0));
        // Set default chunk size = max{1, ensemble_size / num_threads}
        m_grow_chunk_size = config->ensemble_size / num_threads;
        config->read_into(m_grow_chunk_size, uOpt_grow_chunk_size);
        // Handle case where user gives 0 chunk size or if the number of
        // threads is greater than ensemble size
        m_grow_chunk_size = std::max(U_TO_UINT(1), m_grow_chunk_size);
        // Cap maximum chunk size to offer better thread utilization if more
        // than a single trial is needed (as there are typically less samples
        // grown in subsequent trials)
        m_grow_chunk_size =
            std::min(U_BUILD_MAX_GROW_CHUNK_SIZE, m_grow_chunk_size);
        uAssert(m_grow_chunk_size > U_TO_UINT(0));
    }
#endif  // U_BUILD_ENABLE_THREADS

    // Initialize points to sample on surface of unit sphere
    uSphereSurfacePointSampler::get_points(
        m_unit_sphere_sample_points, config->num_unit_sphere_sample_points);

    // Set all samples to active status
    m_trial_status.resize(config->ensemble_size, true);
    // uAssert(this->trial_status.all());
    uAssert(m_trial_status.count() == m_trial_status.size());

    // Initialize log weights
    uAssert(0 < config->ensemble_size);
    m_trial_log_weights.zeros(config->ensemble_size);

    // Make sure completed log weights size is as expected from clear()
    uAssert(0 == m_completed_log_weights_view.size());
    // Reserve size for underlying completed log weights buffer.
    // This is just so we don't have to keep resizing the buffer between
    // trial runs.
    m_completed_log_weights_buffer.zeros(config->ensemble_size);

    // Allocate samples
    m_trial_samples.resize(config->ensemble_size);

    // Initialize mixins
    this->sim_level_node_radius_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_nuclear_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_growth_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_collision_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_intr_chr_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_intr_lam_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_intr_nucb_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->trial_runner_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_qc_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->sim_level_energy_mixin_t::init(*this, config U_THREAD_ID_ARG);
    this->seed_mixin_t::init(*this, config U_THREAD_ID_ARG);

    // Initialize chromatin samples
    for (uUInt i = 0; i < config->ensemble_size; ++i) {
        sample_t& sample(m_trial_samples[i]);
        sample.init(*this, config U_THREAD_ID_ARG);
    }

    // Initialize buffer for storing successfully completed samples
    m_completed_samples.reserve(config->ensemble_size);
}

/**
 * Lightweight re-initialization using saved config object used during
 * full initialization. This is a cheaper version of init() assumming that
 *  1.) init(config) has already been called
 *  2.) The simulation configuration has not changed
 * Refreshes simulation back to init state, ready to run a new simulation
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::init(U_THREAD_ID_0_PARAM) {
    uAssert(static_cast<bool>(m_config));

    // Clear template info
    m_template_nfo.clear();

    // Reset completed samples buffer
    m_completed_samples.clear();
    // According to standard, clear() leaves std::vector::capacity()
    // unchanged; therefore, shouldn't have to call reserve() again
    // m_completed_samples.reserve(m_config->ensemble_size);

    // Zero completed buffer weights
    // @TODO - may not need this
    m_completed_log_weights_buffer.zeros(m_config->ensemble_size);

    // Empty completed view
    // @HACK - use in place new (.set_size(0), .clear() cause asserts to trip)
    new (&m_completed_log_weights_view)
        uVecCol(m_completed_log_weights_buffer.memptr(),
                0,
                false, /* copy */
                true   /* strict */
        );

    // Initialize mixins and samples
    this->reset_for_next_trial(U_THREAD_ID_0_ARG);

    // @HACK - Quality control plugins such as rejection control and
    // partial rejection control need a full re-initialization
    this->sim_level_qc_mixin_t::init_light(*this, m_config U_THREAD_ID_ARG);
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Runs entire simulation to completion. Assumes simulation has been
 * (re-)initialized.
 * @return TRUE if simulation successfully generated target ensemble size,
 *  FALSE otherwise.
 */
template <typename t_SisGlue>
uBool uSisSim<t_SisGlue>::run() {
    U_SCOPED_STAT_TIMER(uSTAT_TotalTime);

    uAssert(this->get_max_num_trials() > 0);

    // Ensure that we wipe any template info
    m_template_nfo.clear();

    uBool has_completed = uFALSE;
    for (uUInt i_trial = 0; i_trial < this->get_max_num_trials(); ++i_trial) {
        // Verify that target sample count = completed + remaining
        // and also that target sample count > 0
        uAssertPosEq((this->get_completed_samples().size() +
                      this->get_trial_samples().size()),
                     this->get_target_total_num_samples());

        // Heartbeat - give notification about work remaining at start of trial
        uLogf("\nTrial %d of %d: Completed=%d (%f%%), Remaining=%d (%f%%)\n",
              (int)(i_trial + 1),
              (int)(this->get_max_num_trials()),
              (int)(this->get_completed_samples().size()),
              U_TO_REAL(100.0) *
                  U_TO_REAL(this->get_completed_samples().size()) /
                  U_TO_REAL(this->get_target_total_num_samples()),
              (int)(this->get_trial_samples().size()),
              U_TO_REAL(100.0) * U_TO_REAL(this->get_trial_samples().size()) /
                  U_TO_REAL(this->get_target_total_num_samples()));

        // Scoping for trial interval logger
        {
            U_SCOPED_INTERVAL_LOGGER(uSTAT_TrialInterval);

            // Used for keeping track of dead v. alive chains.
            // This bit set is wrapped as a handle as it features faster bit
            // iteration methods via usage of compiler intrinsics.
            uWrappedBitset wrapped_trial_status(U_WRAP_BITSET(m_trial_status));

            // Verify trial samples, log weights, and status are parallel sets
            uAssertPosEq(m_trial_samples.size(), m_trial_log_weights.size());
            uAssert(m_trial_samples.size() == wrapped_trial_status.size());

            // Perform trial
            this->trial_runner_mixin_t::run_trial(m_trial_samples,
                                                  m_trial_log_weights,
                                                  wrapped_trial_status,
                                                  *this);

            // Inform quality control that trial has completed
            this->sim_level_qc_mixin_t::on_trial_finish(
                m_trial_log_weights,
                m_trial_samples,
                U_TO_UINT(this->get_completed_samples().size()));

            // Perform any post-trial work
            this->on_trial_finish();
        }

        // If complete, then exit out of loop
        has_completed = this->has_completed_successfully();
        if (has_completed) {
            break;
        } else {
            // Else, prepare for a new trial
            this->reset_for_next_trial(U_MAIN_THREAD_ID_0_ARG);
        }
    }

    // Inform quality control that run has completed
    this->sim_level_qc_mixin_t::on_run_finish(
        this->get_completed_log_weights_view_unsafe());

    // Give notification about final status of simulation
    const uUInt n_completed = U_TO_UINT(this->get_completed_samples().size());
    const uUInt n_remaining =
        this->get_target_total_num_samples() - n_completed;
    const uReal n_targetf = U_TO_REAL(this->get_target_total_num_samples());
    uLogf(
        "\nSimulation finished with status %d:\n\t-Completed=%d "
        "(%f%%)\n\t-Remaining=%d (%f%%)\n",
        (int)has_completed,
        (int)n_completed,
        100.0 * ((double)(n_completed)) / ((double)n_targetf),
        (int)n_remaining,
        100.0 * ((double)(n_remaining)) / ((double)n_targetf));

    return has_completed;
}

/**
 * Runs simulation like normal except that samples are considered completed
 * after their absolute grow step count (total # of seed + # grow step calls)
 * reaches the target count.
 * @param target_num_grow_steps - the total # of seed + grow_step calls that
 * each sample must successfully complete. Simulation will stop once all
 * samples have reached this count.
 * @return TRUE if simulation successfully generated target ensemble size,
 *  FALSE otherwise.
 */
template <typename t_SisGlue>
uBool uSisSim<t_SisGlue>::run_from_start_until(
    const uUInt target_num_grow_steps U_THREAD_ID_PARAM) {
    // @TODO - this assert may not be correct if using a landmark trial runner.
    // May want to ask trial runner to obtain max num grow steps instead as
    // currently this is directing to growth mixin which is not actually used
    // when trials grow via sub-simulation. Although, for size reasons, may
    // want to specify the same growth mixins to allow easier interconversion.
    // Hence, am leaving this in for now.
    uAssert(target_num_grow_steps <= this->get_max_num_grow_steps(*this));
    uAssert(this->get_max_num_trials() > 0);

    // Ensure that we wipe any template info
    m_template_nfo.clear();

    uBool has_completed = uFALSE;
    for (uUInt i_trial = 0; i_trial < this->get_max_num_trials(); ++i_trial) {
        // Verify that target sample count = completed + remaining
        // and also that target sample count > 0
        uAssertPosEq((this->get_completed_samples().size() +
                      this->get_trial_samples().size()),
                     this->get_target_total_num_samples());

        // Used for keeping track of dead v. alive chains.
        // This bit set is wrapped as a handle as it features faster bit
        // iteration methods via usage of compiler intrinsics.
        uWrappedBitset wrapped_trial_status(U_WRAP_BITSET(m_trial_status));

        // Verify trial samples, log weights, and status are parallel sets
        uAssertPosEq(m_trial_samples.size(), m_trial_log_weights.size());
        uAssert(m_trial_samples.size() == wrapped_trial_status.size());

        // Perform trial
        this->trial_runner_mixin_t::run_trial_from_start_until(
            m_trial_samples,
            m_trial_log_weights,
            wrapped_trial_status,
            *this,
            target_num_grow_steps U_THREAD_ID_ARG);

        // Inform quality control that trial has completed
        this->sim_level_qc_mixin_t::on_trial_finish_from_start(
            m_trial_log_weights,
            m_trial_samples,
            U_TO_UINT(this->get_completed_samples().size()));

        // Perform any post-trial work
        this->on_trial_finish();

        // If complete, then exit out of loop
        has_completed = this->has_completed_successfully();
        if (has_completed) {
            break;
        } else {
            // Else, prepare for a new trial
            this->reset_for_next_trial(U_THREAD_ID_0_ARG);
        }
    }

    // Inform quality control that run has completed
    this->sim_level_qc_mixin_t::on_run_finish_from_start(
        this->get_completed_log_weights_view_unsafe());

    return has_completed;
}

/**
 * Runs simulation like normal except samples are initialized to a
 * common starting template state. The simulation terminates after
 * each sample successfully completes target_num_grow_steps
 * (where the template_num_grow_steps are included in this total)
 * @param target_num_grow_steps - the total number of seed() + grow_*()
 *  calls that each sample must successfully complete (includes template
 *  grow count as well)
 * @param template_sample - The template sample state to initialize all
 *  samples to
 * @param template_num_grow_steps - The total number of seed() + grow_*()
 *  calls assumed to have been completed by the template sample.
 * @param template_log_weight - The log weight of the template sample
 * @return TRUE if simulation successfully generated target ensemble size,
 *  FALSE otherwise.
 */
template <typename t_SisGlue>
uBool uSisSim<t_SisGlue>::run_from_template_until(
    const uUInt target_num_grow_steps,
    const sample_t& template_sample,
    const uUInt template_num_grow_steps,
    const uReal template_log_weight U_THREAD_ID_PARAM) {
    // @TODO - this assert may not be correct if using a landmark trial runner.
    // May want to ask trial runner to obtain max num grow steps instead as
    // currently this is directing to growth mixin which is not actually used
    // when trials grow via sub-simulation. Although, for size reasons, may
    // want to specify the same growth mixins to allow easier interconversion.
    // Hence, am leaving this in for now.
    uAssert(target_num_grow_steps <= this->get_max_num_grow_steps(*this));
    uAssert(this->get_max_num_trials() > 0);
    // We are assuming that template sample has been seeded
    uAssert(template_num_grow_steps >= 1);
    // We are assuming that target grow count exceeds template grow count
    uAssert(target_num_grow_steps > template_num_grow_steps);
    // Template cannot be a finished sample
    uAssert(template_num_grow_steps < this->get_max_num_grow_steps(*this));
    // Assume template sample is valid
    uAssert(!template_sample.is_dead());

    // Initialize template info
    m_template_nfo.init(
        template_sample, template_num_grow_steps, template_log_weight);

    uBool has_completed = uFALSE;
    for (uUInt i_trial = 0; i_trial < this->get_max_num_trials(); ++i_trial) {
        // Verify that target sample count = completed + remaining
        // and also that target sample count > 0
        uAssertPosEq((this->get_completed_samples().size() +
                      this->get_trial_samples().size()),
                     this->get_target_total_num_samples());

        // Used for keeping track of dead v. alive chains.
        // This bit set is wrapped as a handle as it features faster bit
        // iteration methods via usage of compiler intrinsics.
        uWrappedBitset wrapped_trial_status(U_WRAP_BITSET(m_trial_status));

        // Verify trial samples, log weights, and status are parallel sets
        uAssertPosEq(m_trial_samples.size(), m_trial_log_weights.size());
        uAssert(m_trial_samples.size() == wrapped_trial_status.size());

        // Perform trial
        this->trial_runner_mixin_t::run_trial_from_template_until(
            m_trial_samples,
            m_trial_log_weights,
            wrapped_trial_status,
            *this,
            target_num_grow_steps U_THREAD_ID_ARG);

        // Inform quality control that trial has completed
        this->sim_level_qc_mixin_t::on_trial_finish_from_template(
            m_trial_log_weights,
            m_trial_samples,
            U_TO_UINT(this->get_completed_samples().size()));

        // Perform any post-trial work
        this->on_trial_finish();

        // If complete, then exit out of loop
        has_completed = this->has_completed_successfully();
        if (has_completed) {
            break;
        } else {
            // Else, prepare for a new trial
            this->reset_for_next_trial(U_THREAD_ID_0_ARG);
        }
    }

    // Inform quality control that run has completed
    this->sim_level_qc_mixin_t::on_run_finish_from_template(
        this->get_completed_log_weights_view_unsafe());

    return has_completed;
}

/**
 * Called after initial chromatin node(s) have been placed at all samples.
 * Routes to relevant simulation level mixins.
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::on_seed_finish() {
    this->trial_runner_mixin_t::on_seed_finish(*this);
    this->sim_level_growth_mixin_t::on_seed_finish(*this);
}

/**
 * Called after a set of incremental chromatin nodes have been appended to
 * each active sample. Routes to relevant simulation level mixins.
 * @param wrapped_trial_status - The trial status wrapped as a handle.
 *  This is to avoid having to wrap the underlying status buffer again.
 *  Also, wrapped bit sets have faster bit iteration methods as they use
 *  compiler intrinsics.
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::on_grow_phase_finish(
    uWrappedBitset& wrapped_trial_status U_THREAD_ID_PARAM) {
    // Avoid post-growth operations if all samples are dead
    if (!uSisUtilsQc::are_all_samples_dead(m_trial_samples)) {
        // Inform trial runner mixin
        this->trial_runner_mixin_t::on_grow_phase_finish(*this U_THREAD_ID_ARG);
        // Inform growth mixin
        this->sim_level_growth_mixin_t::on_grow_phase_finish(
            this->trial_runner_mixin_t::
                get_num_completed_grow_steps_per_sample(),
            *this U_THREAD_ID_ARG);
        // Inform quality control mixin
        this->sim_level_qc_mixin_t::on_grow_phase_finish(
            wrapped_trial_status,
            m_trial_log_weights,
            m_trial_samples,
            this->trial_runner_mixin_t::
                get_num_completed_grow_steps_per_sample(),
            *this U_THREAD_ID_ARG);
    }
}

/**
 * Performs post-trial work like transferring completed samples.
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::on_trial_finish() {
    U_INC_STAT_COUNTER(uSTAT_TrialsCount);

    uAssertPosEq(m_trial_samples.size(), m_trial_log_weights.size());
    // Note: get_completed_log_weights_view has side effects! (view is updated)
    uAssert(this->get_completed_samples().size() ==
            this->get_completed_log_weights_view().size());

    const uUInt num_trial_samples = U_TO_UINT(m_trial_samples.size());
    for (uUInt i = 0; i < num_trial_samples; ++i) {
        // Verify that trial sample is no longer being grown
        uAssert(m_trial_status.test(i) == false);

        const uReal log_weight = m_trial_log_weights.at(i);
        if (!m_trial_samples[i].is_dead()) {
            // Copy successfully finished sample to the completed set
            uAssertBounds(m_completed_samples.size(),
                          0,
                          m_completed_log_weights_buffer.size());
            const uMatSz_t num_completed_samples =
                U_TO_MAT_SZ_T(m_completed_samples.size());
            m_completed_log_weights_buffer.at(num_completed_samples) =
                log_weight;
            m_completed_samples.push_back(m_trial_samples[i]);
            U_INC_STAT_COUNTER(uSTAT_CompletedSamplesCount);
        }
    }

    // Make sure our log weights view is consistent
    this->get_completed_log_weights_view();
}

/**
 * Resets simulation state to run a new trial
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::reset_for_next_trial(U_THREAD_ID_0_PARAM) {
    // We should not be doing this work if we've finished
    uAssert(!this->has_completed_successfully());

    // Determine number of samples that still need to be generated
    uAssert(this->get_target_total_num_samples() >
            this->get_completed_samples().size());
    const size_t num_remaining = this->get_target_total_num_samples() -
                                 this->get_completed_samples().size();

    // Re-size trial log weights
    m_trial_log_weights.zeros(U_TO_MAT_SZ_T(num_remaining));

    // Reset trial status
    m_trial_status.resize(num_remaining);
    m_trial_status.set();
    uAssert(m_trial_status.count() == m_trial_status.size());

    // Re-size trial samples
    uAssert(m_config->is_valid());
    m_trial_samples.resize(num_remaining);
    uAssert(m_trial_samples.size() == num_remaining);

    // Reset mixins
    this->reset_mixins_for_next_trial(U_THREAD_ID_0_ARG);

    // (Re-) initialize samples
    for (size_t i = 0; i < num_remaining; ++i) {
        // For now, just doing a complete re-initialization. Am not keeping
        // track of which samples are from previous trial and which have been
        // just created (guess I don't trust resize operation). Am assuming
        // init can be called and this will properly clear any old state.
        this->reset_sample(m_trial_samples[i] U_THREAD_ID_ARG);
    }
}

/**
 * Prepares mixins for next trial
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::reset_mixins_for_next_trial(U_THREAD_ID_0_PARAM) {
    // Trial runner
    this->trial_runner_mixin_t::reset_for_next_trial(*this,
                                                     m_config U_THREAD_ID_ARG);
    // Quality control
    this->sim_level_qc_mixin_t::reset_for_next_trial(*this,
                                                     m_config U_THREAD_ID_ARG);
    // Node radius
    this->sim_level_node_radius_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Growth
    this->sim_level_growth_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Nuclear
    this->sim_level_nuclear_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Collision
    this->sim_level_collision_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Chrome-to-chrome interaction
    this->sim_level_intr_chr_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Lamina interaction
    this->sim_level_intr_lam_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Nuclear body interaction
    this->sim_level_intr_nucb_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Energy
    this->sim_level_energy_mixin_t::reset_for_next_trial(
        *this, m_config U_THREAD_ID_ARG);
    // Seed
    this->seed_mixin_t::reset_for_next_trial(*this, m_config U_THREAD_ID_ARG);
}

// BEGIN REGROW INTERFACE

/**
 * The following methods are used by rejection control mixin for regrowing
 * a single sample.
 */

/**
 * Informs sim-level mixins that parameter sample is about to be regrown from
 * scratch. Allows mixins to handle this event.
 * @param sample - The sample being regrown
 * @param num_completed_grow_steps - The number of seed() + grow_*() calls for
 *  the longest active sample within the trial
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::reset_mixins_for_regrowth(
    const sample_t& sample,
    const uUInt num_completed_grow_steps) {
    // Trial runner
    this->trial_runner_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Quality control
    this->sim_level_qc_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Node radius
    this->sim_level_node_radius_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Growth
    this->sim_level_growth_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Nuclear
    this->sim_level_nuclear_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Collision
    this->sim_level_collision_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Chrome-to-chrome interaction
    this->sim_level_intr_chr_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Lamina interaction
    this->sim_level_intr_lam_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Nuclear body interaction
    this->sim_level_intr_nucb_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Energy
    this->sim_level_energy_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
    // Seed
    this->seed_mixin_t::reset_for_regrowth(
        sample, num_completed_grow_steps, *this, m_config);
}

/**
 * Informs mixins that sample has finished being reseeded. Allows mixins to
 * handle this event.
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::on_reseed_finish(const sample_t& sample,
                                          uReal& log_weight,
                                          bool& status) {
    // Trial runner
    this->trial_runner_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Quality control
    this->sim_level_qc_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Node radius
    this->sim_level_node_radius_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Growth
    this->sim_level_growth_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Nuclear
    this->sim_level_nuclear_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Collision
    this->sim_level_collision_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Chrome-to-chrome interaction
    this->sim_level_intr_chr_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Lamina interaction
    this->sim_level_intr_lam_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Nuclear body interaction
    this->sim_level_intr_nucb_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Energy
    this->sim_level_energy_mixin_t::on_reseed_finish(
        sample, log_weight, status, *this);
    // Seed
    this->seed_mixin_t::on_reseed_finish(sample, log_weight, status, *this);
}

/**
 * Informs mixins that sample has finished being regrown. Allows mixins to
 * handle this event.
 */
template <typename t_SisGlue>
void uSisSim<t_SisGlue>::on_regrow_step_finish(
    sample_t& sample,
    uReal& log_weight,
    bool& status,
    const uUInt num_completed_grow_steps,
    const uUInt sample_id U_THREAD_ID_PARAM) {
    // Trial runner
    this->trial_runner_mixin_t::on_regrow_step_finish(sample,
                                                      log_weight,
                                                      status,
                                                      num_completed_grow_steps,
                                                      *this U_THREAD_ID_ARG);
    // Quality control
    this->sim_level_qc_mixin_t::on_regrow_step_finish(sample,
                                                      log_weight,
                                                      status,
                                                      num_completed_grow_steps,
                                                      sample_id,
                                                      *this U_THREAD_ID_ARG);
    // Node radius
    this->sim_level_node_radius_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Growth
    this->sim_level_growth_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Nuclear
    this->sim_level_nuclear_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Collision
    this->sim_level_collision_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Chrome-to-chrome interaction
    this->sim_level_intr_chr_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Lamina interaction
    this->sim_level_intr_lam_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Nuclear body interaction
    this->sim_level_intr_nucb_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Energy
    this->sim_level_energy_mixin_t::on_regrow_step_finish(
        sample,
        log_weight,
        status,
        num_completed_grow_steps,
        *this U_THREAD_ID_ARG);
    // Seed
    this->seed_mixin_t::on_regrow_step_finish(sample,
                                              log_weight,
                                              status,
                                              num_completed_grow_steps,
                                              *this U_THREAD_ID_ARG);
}

// END REGROW INTERFACE
