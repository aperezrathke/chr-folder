//****************************************************************************
// uTestSisHomgUnifLagTrSchedule.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * using a landmark trial runner with homogeneous, uniform lag schedule
 *
 * Usage:
 * -test_sis_homg_unif_lag_tr_sched
 */

#ifdef uTestSisHomgUnifLagTrSchedule_h
#   error "Test Sis Homg Unif Lag Tr Sched included multiple times!"
#endif  // uTestSisHomgUnifLagTrSchedule_h
#define uTestSisHomgUnifLagTrSchedule_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Parent simulation null quality control mixin
#include "uSisNullQcSimLevelMixin.h"

// Landmark trial runner mixin
#include "uSisHomgUnifLagScheduleMixin.h"
#include "uSisLandmarkTrialRunnerMixin.h"
#include "uSisSelectPower.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"
#include "uSisSerialBatchGrower.h"

#include "uMockUpUtils.h"

#include <iostream>
#include <string>

namespace {
/**
 * Wire together rejection control child simulation
 */
class uTestSisHomgUnifLagTrChildGlue {
public:
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_NULL_MODEL_ATTRIBS(
        uTestSisHomgUnifLagTrChildGlue);

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisHomgUnifLagTrChildGlue,
        uSisSerialBatchGrower<uTestSisHomgUnifLagTrChildGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisNullQcSimLevelMixin<uTestSisHomgUnifLagTrChildGlue>
        sim_level_qc_mixin_t;
};

/**
 * Wire together landmark trial runner parent simulation
 */
class uTestSisHomgUnifLagTrParentGlue {
public:
    // WARNING: We want parent and child sample types to match to
    // allow *unsafe* copying!
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_NULL_MODEL_ATTRIBS(
        uTestSisHomgUnifLagTrParentGlue);

    // Trial runner mixin
    typedef uSisLandmarkTrialRunnerMixin<
        uTestSisHomgUnifLagTrParentGlue,
        uSisSelectPower<uTestSisHomgUnifLagTrParentGlue,
                        uOpt_qc_sampler_power_alpha_slot0>,
        uSisHomgUnifLagScheduleMixin<uTestSisHomgUnifLagTrParentGlue,
                                     uOpt_qc_schedule_lag_slot0>,
        uSisSubSimAccess_threaded<uTestSisHomgUnifLagTrChildGlue::sim_t>,
        uSisSampleCopier_unsafe,
        uSisParallelBatchGrower<uTestSisHomgUnifLagTrParentGlue>,
        // enable heartbeat logging for trial runner
        true>
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisNullQcSimLevelMixin<uTestSisHomgUnifLagTrParentGlue>
        sim_level_qc_mixin_t;
};

}  // end of anonymous namespace

/**
 * Entry point for test
 * sis - sequential importance sampling
 * homg = homogeneous
 * unif lag = uniform sampling within lag intervals
 * tr = trial runner
 * sched = schedule
 */
int uTestSisHomgUnifLagTrSchedMain(const uCmdOptsMap& cmd_opts) {
    // Macro for top-level simulation
    typedef uTestSisHomgUnifLagTrParentGlue::sim_t parent_sim_t;

    // Common settings
    const uReal MAX_NODE_DIAMETER = U_TO_REAL(300.0);
    const uReal NUCLEAR_DIAMETER = U_TO_REAL(9000.0);
    const std::vector<uUInt> NUM_NODES(1 /*n_chains*/, 1000 /*n_nodes*/);
    const uUInt NUM_UNIT_SPHERE_SAMPLE_POINTS = 100;
    const uReal LANDMARK_SELECT_ALPHA = U_TO_REAL(1.0);

    // Create a parent configuration
    uSpConfig_t parent_config = uSmartPtr::make_shared<uConfig>();
    parent_config->output_dir = std::string("null");
    parent_config->max_trials = 3;
    parent_config->ensemble_size = 100;
    parent_config->set_option(uOpt_max_node_diameter, MAX_NODE_DIAMETER);
    parent_config->set_option(uOpt_nuclear_diameter, NUCLEAR_DIAMETER);
    parent_config->num_nodes = NUM_NODES;
    parent_config->num_unit_sphere_sample_points =
        NUM_UNIT_SPHERE_SAMPLE_POINTS;

    // Configure parent landmark schedule
    const uUInt PARENT_LANDMARK_LAG = 100;
    parent_config->set_option(uOpt_qc_schedule_type_slot0, "homg_unif_lag");
    parent_config->set_option(uOpt_qc_schedule_lag_slot0, PARENT_LANDMARK_LAG);
    parent_config->set_option(uOpt_qc_sampler_power_alpha_slot0,
                              LANDMARK_SELECT_ALPHA);

    // Create a child configuration
    uSpConfig_t child_config = uSmartPtr::make_shared<uConfig>();

    // Register child configuration
    parent_config->set_child_tr(child_config,
                                uTRUE /*should_copy_named_vars_to_child*/);
    child_config->ensemble_size = 50;

    // Output configuration
    std::cout << "Running Test Sis Homg Unif Lag Tr Schedule." << std::endl;
    parent_config->print();

    // Configure number of worker threads
    // Uncomment below to manually configure thread count:
    // U_SET_NUM_WORKER_THREADS(parent_config, 7);

    // Initialize globals
    uG::init(parent_config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    parent_sim_t sim(parent_config U_MAIN_THREAD_ID_ARG);

    // Do rejection control simulation!
    const uBool result = sim.run();

    // Compute rejection control effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());

    std::cout << "Lmrk simulation completed with status: " << result
              << std::endl;
    std::cout << "Lmrk effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
