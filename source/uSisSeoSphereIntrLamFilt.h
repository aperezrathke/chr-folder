//****************************************************************************
// uSisSeoSphereIntrLamFilt.h
//****************************************************************************

/**
 * @brief Lamina interaction filters for spherical nucleus
 */

#ifndef uSisSeoSphereIntrLamFilt_h
#define uSisSeoSphereIntrLamFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisIntrLamCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Filters
//****************************************************************************

/**
 * Effectively namespace for lamina-interaction knock-in utilities assuming a
 *  spherical nucleus
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrLamKinFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrLamCommonFilt<glue_t> intr_lam_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within a spherical nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED DOT PRODUCT OF SEED CANDIDATE POSITION!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrLamFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine squared distance threshold
        const uReal dist2_check = calc_dist2_check(c, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(dot_buffer.n_elem));
        // Verify that candidate position dot product has been cached
        uAssert(intr_lam_common_filt_t::check_dot(
            U_SIS_SEED_CANDIDATE_ID, dot_buffer, node_center.memptr()));
        // Get self dot-product of candidate bead center position
        const uReal dot_bc = dot_buffer.at(U_SIS_SEED_CANDIDATE_ID);
        uAssert(dot_bc >= U_TO_REAL(0.0));
        // Test if interaction can be satisfied
        uBool b_okay = dist2_check <= dot_bc;
        if (!b_okay) {
            // Let policy have final say
            b_okay = policy.intr_lam_kin_on_fail(
                p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within a spherical nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED DOT PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrLamFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // For detailed notes on interaction test, see init_dist_scalars() of
        // uSisSeoSphereIntrLamSevelMixin::init_intr_lam_kin_policy_t
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssert(dot_buffer.n_elem == candidate_centers.n_rows);
        uAssert(dot_buffer.n_elem == out_legal_candidates_primed.n_elem);
        // Determine squared distance threshold
        const uReal dist2_check = calc_dist2_check(c, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }
            // Verify that candidate position dot product has been cached
            uAssert(intr_lam_common_filt_t::check_dot(
                i, dot_buffer, candidate_centers));
            // Get self dot-product of candidate bead center position
            const uReal dot_bc = dot_buffer.at(i);
            uAssert(dot_bc >= U_TO_REAL(0.0));
            // Test if interaction can be satisfied
            uBool b_okay = dist2_check <= dot_bc;
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_lam_kin_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }  // End iteration over candidates
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-in lamina interaction constraint
     * under assumptions of single-end, ordered (SEO) growth and spherical
     * nucleus
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrLamFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        // For detailed notes on interaction test, see init_dist_scalars() of
        // uSisSeoSphereIntrLamSevelMixin::init_intr_lam_kin_policy_t
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        if (!intr_lam_common_filt_t::is_in_frag(
                c.node_nid, c.frag_nid_lo, c.frag_nid_hi)) {
            return uFALSE;
        }
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(candidate_id, U_TO_UINT(0), U_TO_UINT(dot_buffer.n_elem));
        // Verify dot product of selected node was cached
        uAssert(intr_lam_common_filt_t::check_dot(
            U_TO_MAT_SZ_T(candidate_id), dot_buffer, B));
        // Get self dot-product of selected node (i.e. bead) center position
        const uReal dot_bc = dot_buffer.at(U_TO_MAT_SZ_T(candidate_id));
        uAssert(dot_bc >= U_TO_REAL(0.0));
        // Test if satisfied
        if (sample_t::is_homg_radius) {
            // CASE: HOMOGENEOUS RADIUS
            const uReal dorig_pad2 = sim.get_intr_lam_kin_dist2(c.intr_id);
            // Trivial interactions should have been removed during init
            uAssert(dorig_pad2 > U_TO_REAL(0.0));
            const uBool result = dorig_pad2 <= dot_bc;
            // @TODO - If we are the last node in fragment, this should always
            // be satisfied, else we would have been marked as violator!
            uAssert(result || (c.node_nid < c.frag_nid_hi));
            return result;
        }
        // DEFAULT: HETEROGENEOUS RADIUS
        const uReal dorig_pad = sim.get_intr_lam_kin_dist(c.intr_id) -
                                sample.get_node_radius(c.node_nid, sim);
        uReal dist2_check = std::max(dorig_pad, U_TO_REAL(0.0));
        dist2_check *= dist2_check;
        const uBool result = dist2_check <= dot_bc;
        // @TODO - If we are the last node in fragment, this should always be
        //  satisfied, else we would have been marked as violator!
        uAssert(result || (c.node_nid < c.frag_nid_hi));
        return result;
    }

private:
    /**
     * Computes squared distance threshold
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return Squared distance threshold
     */
    static inline uReal calc_dist2_check(const uSisIntrLamFilterCore_t& c,
                                         const sample_t& sample,
                                         const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of last bead of constrained fragment (where
        // tested bead index <= last bead of constrained fragment index)
        uAssert(c.node_nid <= c.frag_nid_hi);
        const uReal fc2c =
            sample.get_max_c2c_len(c.node_nid, c.frag_nid_hi, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        uAssert(sample.get_node_radius(c.frag_nid_hi, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance relative to origin; for
        // heterogeneous case, this value must be padded by radius of last
        // node of interacting fragment and the padded result MAY BE NEGATIVE
        // as it will not have undergone trivial interaction filtering in this
        // particular case
        const uReal dorig_pad =
            (sample_t::is_homg_radius)
                ? sim.get_intr_lam_kin_dist(c.intr_id)
                : (sim.get_intr_lam_kin_dist(c.intr_id) -
                   sample.get_node_radius(c.frag_nid_hi, sim));
        uAssert((!sample_t::is_homg_radius) || (dorig_pad > U_TO_REAL(0.0)));
        // Compute squared distance threshold
        uReal dist2_check = std::max(dorig_pad - fc2c, U_TO_REAL(0.0));
        uAssert(dist2_check >= U_TO_REAL(0.0));
        dist2_check *= dist2_check;
        return dist2_check;
    }

    // Disallow any form of instantiation
    uSisSeoSphereIntrLamKinFilt(const uSisSeoSphereIntrLamKinFilt&);
    uSisSeoSphereIntrLamKinFilt& operator=(const uSisSeoSphereIntrLamKinFilt&);
};

/**
 * Effectively namespace for lamina-interaction knock-out utilities assuming a
 *  spherical nucleus
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrLamKoFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utilities
     */
    typedef uSisIntrLamCommonFilt<glue_t> intr_lam_common_filt_t;

    /**
     * Determines if seed candidate position violates the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within a spherical nucleus.
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED DOT PRODUCT OF SEED CANDIDATE POSITION!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param c - Core filter arguments
     * @param node_center - Candidate seed position
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static uBool is_seed_okay(policy_t& policy,
                              policy_args_t* const p_policy_args,
                              const uSisIntrLamFilterCore_t& c,
                              const uVecCol& node_center,
                              const sample_t& sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine squared distance threshold
        const uReal dist2_check =
            calc_dist2_check(c, c.frag_nid_lo, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                      U_TO_UINT(0),
                      U_TO_UINT(dot_buffer.n_elem));
        // Verify that candidate position dot product has been cached
        uAssert(intr_lam_common_filt_t::check_dot(
            U_SIS_SEED_CANDIDATE_ID, dot_buffer, node_center.memptr()));
        // Get self dot-product of candidate bead center position
        const uReal dot_bc = dot_buffer.at(U_SIS_SEED_CANDIDATE_ID);
        uAssert(dot_bc >= U_TO_REAL(0.0));
        // Test if interaction can be satisfied
        uAssert(c.node_nid <= c.frag_nid_lo);
        uBool b_okay = (dist2_check > dot_bc);
        if (!b_okay) {
            // Let policy have final say
            b_okay = policy.intr_lam_ko_on_fail(
                p_policy_args, U_SIS_SEED_CANDIDATE_ID, c, sample, sim);
        }
        return b_okay;
    }

    /**
     * Determines if any candidate positions violate the parameter lamina
     * interaction constraint under the assumptions of single-end, ordered
     * (SEO) growth within a spherical nucleus
     * @WARNING - ASSUMES NUCLEAR FILTERING HAS ALREADY OCCURED AND HAS
     *  POPULATED DOT PRODUCT OF CANDIDATE POSITIONS!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrLamFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        // For detailed notes on interaction test, see init_dist_scalars() of
        // uSisSeoSphereIntrLamSevelMixin::init_intr_lam_ko_policy_t
        uAssert(candidate_centers.n_rows == out_legal_candidates_primed.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // Determine first "active" bead index of interacting fragment
        const uUInt active_nid = std::max(c.node_nid, c.frag_nid_lo);
        uAssert(active_nid <= c.frag_nid_hi);
        // Determine squared distance threshold
        const uReal dist2_check = calc_dist2_check(c, active_nid, sample, sim);
        uAssert(dist2_check >= U_TO_REAL(0.0));
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssert(dot_buffer.n_elem == candidate_centers.n_rows);
        uAssert(dot_buffer.n_elem == out_legal_candidates_primed.n_elem);
        // Process candidates
        const uMatSz_t num_cand = out_legal_candidates_primed.n_elem;
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < num_cand; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }
            // Verify that candidate position dot product has been cached
            uAssert(intr_lam_common_filt_t::check_dot(
                i, dot_buffer, candidate_centers));
            // Get self dot-product of candidate bead center position
            const uReal dot_bc = dot_buffer.at(i);
            uAssert(dot_bc >= U_TO_REAL(0.0));
            // Test if interaction can be satisfied
            uBool b_okay = (dist2_check > dot_bc);
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_lam_ko_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }  // End iteration over candidates
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a knock-out lamina interaction constraint
     * under assumptions of single-end, ordered (SEO) growth and spherical
     * nucleus
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrLamFilterCore_t& c,
                           const uReal* const B,
                           const uUInt candidate_id,
                           const sample_t& sample,
                           const sim_t& sim U_THREAD_ID_PARAM) {
        // For detailed notes on interaction test, see init_dist_scalars() of
        // uSisSeoSphereIntrLamSevelMixin::init_intr_lam_ko_policy_t
        uAssert(intr_lam_common_filt_t::check_format(c, sim));
        // See if we can avoid interaction testing
        uAssert(c.node_nid <= c.frag_nid_hi);
        if (c.node_nid != c.frag_nid_hi) {
            // Knock-out interaction cannot be satisfied until entire fragment
            // has been grown
            return uFALSE;
        }
#ifdef U_BUILD_ENABLE_ASSERT
        // Assumes self dot product of candidate centers has already been
        // computed by nuclear (confinement) mixin
        const uVecCol& dot_buffer = sim.get_nuc_dot_buffer(U_THREAD_ID_0_ARG);
        uAssertBounds(candidate_id, U_TO_UINT(0), U_TO_UINT(dot_buffer.n_elem));
        // Verify dot product of selected node was cached
        uAssert(intr_lam_common_filt_t::check_dot(
            U_TO_MAT_SZ_T(candidate_id), dot_buffer, B));
        // Get self dot-product of selected node (i.e. bead) center position
        const uReal dot_bc = dot_buffer.at(U_TO_MAT_SZ_T(candidate_id));
        uAssert(dot_bc >= U_TO_REAL(0.0));
        // Test if satisfied
        if (sample_t::is_homg_radius) {
            // CASE: HOMOGENEOUS RADIUS
            const uReal dorig_pad2 = sim.get_intr_lam_ko_dist2(c.intr_id);
            // Trivial interactions should have been removed during init
            uAssert(dorig_pad2 > U_TO_REAL(0.0));
            return dorig_pad2 > dot_bc;
        }
        // DEFAULT: HETEROGENEOUS RADIUS
        const uReal dorig_pad = sim.get_intr_lam_ko_dist(c.intr_id) -
                                sample.get_node_radius(c.node_nid, sim);
        uReal dist2_check = std::max(dorig_pad, U_TO_REAL(0.0));
        dist2_check *= dist2_check;
        uAssert(dist2_check > dot_bc);
#endif  // U_BUILD_ENABLE_ASSERT
        // If violation was not encountered, then constraint is satisfied, no
        // further validation should be necessary!
        return uTRUE;
    }

private:
    /**
     * Computes squared distance threshold
     * @param c - Core filter arguments
     * @param active_nid - max(current nid, frag_nid_lo)
     * @param sample - Parent sample
     * @param sim - Parent simulation
     * @return Squared distance threshold
     */
    static inline uReal calc_dist2_check(const uSisIntrLamFilterCore_t& c,
                                         const uUInt active_nid,
                                         const sample_t& sample,
                                         const sim_t& sim) {
        // Get maximum center-to-center length of segment defined by center of
        // tested bead to center of first "active" bead of the constrained
        // fragment, where first "active" bead is:
        //  - if candidate node_nid < fragment nid_lo -> fragment nid_lo
        //  - else -> candidate node_nid, as candidate is assumed to be
        //      within the interacting fragment
        uAssert(c.node_nid <= active_nid);
        uAssertBoundsInc(active_nid, c.frag_nid_lo, c.frag_nid_hi);
        const uReal fc2c = sample.get_max_c2c_len(c.node_nid, active_nid, sim);
        uAssert(fc2c >= U_TO_REAL(0.0));
        // If tested node is "active", then fc2c should be 0!
        uAssert((c.node_nid != active_nid) || (fc2c == U_TO_REAL(0.0)));
        uAssert(sample.get_node_radius(active_nid, sim) > U_TO_REAL(0.0));
        // Get constrained interaction distance relative to origin; for
        // heterogeneous case, this value must be padded by radius of active
        // node of interacting fragment and the padded result MAY BE NEGATIVE
        const uReal dorig_pad = (sample_t::is_homg_radius)
                                    ? sim.get_intr_lam_ko_dist(c.intr_id)
                                    : (sim.get_intr_lam_ko_dist(c.intr_id) -
                                       sample.get_node_radius(active_nid, sim));
        uAssert((!sample_t::is_homg_radius) || (dorig_pad > U_TO_REAL(0.0)));
        // Determine squared distance threshold
        uReal dist2_check = (sample_t::is_homg_radius)
                                ? (dorig_pad + fc2c)
                                : std::max(dorig_pad + fc2c, U_TO_REAL(0.0));
        uAssert(dist2_check >= U_TO_REAL(0.0));
        dist2_check *= dist2_check;
        return dist2_check;
    }

    // Disallow any form of instantiation
    uSisSeoSphereIntrLamKoFilt(const uSisSeoSphereIntrLamKoFilt&);
    uSisSeoSphereIntrLamKoFilt& operator=(const uSisSeoSphereIntrLamKoFilt&);
};

#endif  // uSisSeoSphereIntrLamFilt_h
