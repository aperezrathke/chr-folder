//****************************************************************************
// uSisUtilsQc.h
//****************************************************************************

/**
 * Common utilities for sequential importance sampling of chromatin chains
 */

#ifndef uSisUtilsQc_h
#define uSisUtilsQc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uGlobals.h"
#include "uSelectorTower.h"
#include "uStats.h"
#include "uTypeTraits.h"
#include "uTypes.h"

#include <algorithm>
#include <vector>

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro for declaring null (do nothing) versions for on_trial_finish*()
 * callbacks
 */
#define U_SIS_DECLARE_MIXIN_NULL_TRIAL_FINISH_INTERFACE              \
    void on_trial_finish(const uVecCol& trial_log_weights,           \
                         const std::vector<sample_t>& trial_samples, \
                         const uUInt prev_num_completed_samples) {}  \
    void on_trial_finish_from_start(                                 \
        const uVecCol& trial_log_weights,                            \
        const std::vector<sample_t>& trial_samples,                  \
        const uUInt prev_num_completed_samples) {}                   \
    void on_trial_finish_from_template(                              \
        const uVecCol& trial_log_weights,                            \
        const std::vector<sample_t>& trial_samples,                  \
        const uUInt prev_num_completed_samples) {}

/**
 * Macro for declaring null (do nothing) versions for on_run_finish*()
 * callbacks
 */
#define U_SIS_DECLARE_MIXIN_NULL_RUN_FINISH_INTERFACE                \
    void on_run_finish(uVecCol& completed_log_weights) {}            \
    void on_run_finish_from_start(uVecCol& completed_log_weights) {} \
    void on_run_finish_from_template(uVecCol& completed_log_weights) {}

/**
 * Utility macro to find the least upper bound of "i" that is strictly greater
 * than "i" and is also a multiple of "lag". Useful for lag-based schedules.
 */
#define U_NEXT_MULTIPLE(i__, lag__) \
    ((i__) + (lag__) - (((i__) + (lag__)) % (lag__)))

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility namespace
 */
namespace uSisUtilsQc {
/**
 * @param samples - vector of samples to test
 * @return true if all samples are dead, false o/w
 */
template <typename sample_t>
inline bool are_all_samples_dead(const std::vector<sample_t>& samples) {
    const size_t N_SAMPLES = samples.size();
    for (size_t i = 0; i < N_SAMPLES; ++i) {
        if (!samples[i].is_dead()) {
            return false;
        }
    }
    return true;
}

/**
 * Transforms log weights into exponential weights normalized relative to
 * a parameter log weight. Hence, all exponential weights are equal to:
 *
 * exp(log_weight - log_norm_weight)
 *                                = exp(log_weight) / exp(log_norm_weight)
 *                                = weight / norm_weight = w'
 *
 * To transform back to log weights, do: log(w') + log_norm_weight
 *                                = log(weight / norm_weight) + log_norm_weight
 *                                = log(weight * norm_weight / norm_weight)
 *                                = log(weight)
 *
 * @param out_exp_weights - A buffer for storing the exponential weights.
 *  All weights are normalized by the maximum weight and are in range
 *  (0,1]
 * @param norm_log_weight - The log weight to normalize by, used for
 *  transforming back to log weights from normalized exponential weights.
 * @param log_weights - A vector of logarithmic weights to transform
 */
inline void calc_norm_exp_weights_using(uVecCol& out_norm_exp_weights,
                                        const uReal norm_log_weight,
                                        const uVecCol& log_weights) {
    uAssertPosEq(out_norm_exp_weights.size(), log_weights.size());

    // Copy log weights
    memcpy(out_norm_exp_weights.memptr(),
           log_weights.memptr(),
           out_norm_exp_weights.size() * sizeof(uReal));

    // Normalize the log weights
    // Note: weights have not been transformed to exponential space yet...
    // Must be done in this order to avoid numerical precision issues!
    out_norm_exp_weights -= norm_log_weight;

    // Finally, transform to exponential space
    // @TODO - verify no dynamic allocations are taking place
    // @TODO - profile vs using explicit loop
    out_norm_exp_weights = uMatrixUtils::exp(out_norm_exp_weights);
}

/**
 * Transforms log weights into exponential weights normalized relative to
 * the maximum log weight. Hence, all exponential weights are equal to:
 *
 * exp(log_weight - max_log_weight)
 *                                 = exp(log_weight) / exp(max_log_weight)
 *
 * This means the maximum log weight will map to an exponential weight
 * of 1. Furthermore, all exponential weights are in range (0, 1].
 *
 * To transform back to log weights, do: log(exp_weight) + max_log_weight
 *
 * @param out_exp_weights - A buffer for storing the exp transformed weights.
 *  All weights are normalized by the maximum weight and are in range
 *  (0,1]
 *  @WARNING - MUST BE PRE-SIZED TO NUM_ELEM(log_weights)
 * @param out_max_log_weight - The maximum log weight, used for transforming
 *  back to log weights from normalized exponential weights.
 * @param log_weights - A vector of logarithmic weights to transform
 */
inline void calc_norm_exp_weights(uVecCol& out_norm_exp_weights,
                                  uReal& out_max_log_weight,
                                  const uVecCol& log_weights) {
    uAssertPosEq(out_norm_exp_weights.size(), log_weights.size());

    // Determine maximum log weight to normalize by
    out_max_log_weight = log_weights.max();

    return calc_norm_exp_weights_using(
        out_norm_exp_weights, out_max_log_weight, log_weights);
}

/**
 * Calculate effective sample size based on coefficient of
 * variation.
 *
 * ESS = 1 / (1 + cv^2)
 *
 * Note: the numerator should be num_samples, but we just want to know
 * how much each sample is approximately worth relative to target.
 *
 * where cv^2 = sample_variance / sample_mean^2
 *
 * See pgs. 35, 75 of Liu, Monte Carlo Strategies in Scientific Computing
 *
 * @param out_norm_exp_weights - The output normalized exponential weights
 *  computed as exp(log_weights - max(log_weights))
 * @param out_max_log_weight - The output maximum log weight
 * @param log_weights - The input log weight of each sample, must not alias
 *  memory for out_norm_exp_weights  
 * @return The effective sample size according to cv approximation
 */
inline uReal calc_ess_cv_from_log(uVecCol& out_norm_exp_weights,
                                  uReal& out_max_log_weight,
                                  const uVecCol& log_weights) {
    uAssert(log_weights.size() >= 1);
    uAssert(out_norm_exp_weights.mem != log_weights.mem);

    const uUInt num_samples = U_TO_UINT(log_weights.size());
    const uReal num_samples_f = U_TO_REAL(num_samples);

    out_norm_exp_weights.zeros(num_samples);

    // Compute relative weights
    calc_norm_exp_weights(
        out_norm_exp_weights, out_max_log_weight, log_weights);

    // Compute mean
    const uReal mean_ = uMatrixUtils::mean(out_norm_exp_weights);

    // Compute variance
    uReal var_ = U_TO_REAL(0.0);
    uReal dev = U_TO_REAL(0.0);
    for (uUInt i = 0; i < num_samples; ++i) {
        dev = out_norm_exp_weights.at(i) - mean_;
        var_ += dev * dev;
    }

    // @HACK - Numerical stability, avoid divide by zero!
    var_ /= std::max((num_samples_f - U_TO_REAL(1.0)), U_TO_REAL(1.0));

    // Compute coefficient of variation (squared)
    const uReal cv_sq = var_ / (mean_ * mean_);

    // Determine how much each sample contributes
    const uReal ess = U_TO_REAL(1.0) / (U_TO_REAL(1.0) + cv_sq);
    return ess;
}

/**
 * Calculate effective sample size based on coefficient of
 * variation.
 *
 * ESS = 1 / (1 + cv^2)
 *
 * Note: the numerator should be num_samples, but we just want to know
 * how much each sample is approximately worth relative to target.
 *
 * where cv^2 = sample_variance / sample_mean^2
 *
 * See pgs. 35, 75 of Liu, Monte Carlo Strategies in Scientific Computing
 *
 * @param log_weights - The log weight of each sample
 * @return effective sample size according to cv approximation
 */
inline uReal calc_ess_cv_from_log(const uVecCol& log_weights) {
    uVecCol norm_exp_weights;
    uReal max_log_weight;
    return calc_ess_cv_from_log(norm_exp_weights, max_log_weight, log_weights);
}

/**
 * Computes log(mean weight)
 * @param out_norm_exp_weights - a pre-sized buffer for storing
 *  exp(log_weights)
 *  @WARNING - MUST BE PRE-SIZED TO NUM_ELEM(log_weights)
 * @param log_weights - the log weights to compute the mean weight
 * @return the mean log weight computed as:
 *      log( 1/M * sum(w_i' * w_max) ) where w_i' = w_i / w_max
 *          = log( w_max * (1/M) * sum( w_i') )
 *          = log( w_max ) + log( mean(w_i') )
 */
inline uReal calc_mean_log_weight(uVecCol& out_norm_exp_weights,
                                  const uVecCol& log_weights) {
    uReal max_log_weight = U_TO_REAL(0.0);
    calc_norm_exp_weights(out_norm_exp_weights, max_log_weight, log_weights);
    uReal mean_log_weight = uMatrixUtils::mean(out_norm_exp_weights);
    mean_log_weight = log(mean_log_weight);
    mean_log_weight += max_log_weight;
    return mean_log_weight;
}

/**
 * @param p_select - a vector of probabilities - must sum to 1.0 and must
 *  have at least 1 element
 * @return an index [0, size-1] where each index i has probability
 *  p_select[i] of being chosen
 */
inline uUInt rand_select_index(const uVecCol& p_select U_THREAD_ID_PARAM) {
    return uSelectorTower::rand_select_index(p_select, uRng);
}

/**
 * @param p_select - a vector of possibly unnormalized probabilities - must
 *  sum to 'z' and must have at least 1 element
 * @param z - the normalizing constant = sum(p_select)
 * @return an index [0, size-1] where each index i has probability
 *  p_select[i] / z of being chosen
 */
inline uUInt rand_select_index(const uVecCol& p_select,
                               const uReal z U_THREAD_ID_PARAM) {
    return uSelectorTower::rand_select_index(p_select, z, uRng);
}

/**
 * Computes a vector of probabilities p such that:
 *  p_i = (w_i)^alpha / sum( (w_j)^alpha )
 * where w_i is the i-th sample weight and alpha is a real value
 * @param out_smoothed_probabilities - The output probabilities where
 *  each probability is proportional to the relative weight raised to
 *  the alpha power
 * @param norm_exp_weights - The normalized sample weights such that the
 *  max weight is 1 and all other weights are divided by max weight
 * @param alpha - The power to raise all weights by
 */
inline void calc_power_smoothed_probabilities(
    uVecCol& out_smoothed_probabilities,
    const uVecCol& norm_exp_weights,
    const uReal alpha) {
    // Compute smoothed probabilities by raising weights to the alpha power.
    if (U_REAL_CMP_EQ(alpha, U_TO_REAL(0.5))) {
        out_smoothed_probabilities = uMatrixUtils::sqrt(norm_exp_weights);
    } else if (U_REAL_CMP_EQ(alpha, U_TO_REAL(1.0))) {
        out_smoothed_probabilities = norm_exp_weights;
    } else {
        out_smoothed_probabilities = uMatrixUtils::pow(norm_exp_weights, alpha);
    }

    // Normalize smoothed weights to convert to probability
    out_smoothed_probabilities /= uMatrixUtils::sum(out_smoothed_probabilities);
    uAssertRealEq(uMatrixUtils::sum(out_smoothed_probabilities),
                  U_TO_REAL(1.0));
}

/**
 * Used for keeping tracking of how many times a monomer occurs
 */
typedef std::pair<uVecCol, uUInt> uMonomerTally;

/**
 * Keeps track of the unique nodes at a monomer positions for a batch
 * of samples
 */
typedef std::vector<uMonomerTally> uUniqueMonomerCounts;

/**
 * The unique node counts at each position of a (set of) chromatin
 *  chain(s)
 */
typedef std::vector<uUniqueMonomerCounts> uUniqueMonomerCountsMap;

/**
 * Obtain the counts for each unique node type at all monomer positions.
 * All samples are assumed to be of same length and ordered in the same
 * fashion.
 * @param out_unique_nodes - The output map which tabulates counts for
 *  unique nodes at each monomer position
 * @param samples - The batch of samples to tabulate counts for
 */
template <typename sample_t>
void calc_unique_monomer_counts(uUniqueMonomerCountsMap& out_unique_nodes,
                                const std::vector<sample_t>& samples) {
    // Early out if we have no samples
    out_unique_nodes.clear();
    if (samples.empty()) {
        return;
    }

    // Assume that node positions are in the columns
    uAssert(samples[0].get_node_positions().n_rows == uDim_num);
    uAssert(samples[0].get_node_positions().n_cols >= 1);

    const uUInt n_nodes = U_TO_UINT(samples[0].get_node_positions().n_cols);
    const uUInt n_samples = U_TO_UINT(samples.size());

    out_unique_nodes.resize(n_nodes);

    uVecCol point(uDim_num);

    // Iterate over all node positions
    for (uUInt i_node = 0; i_node < n_nodes; ++i_node) {
        uUniqueMonomerCounts& monomer_counts = out_unique_nodes[i_node];
        uAssert(monomer_counts.empty());

        // Check across all samples for unique nodes at this position
        for (uUInt i_sample = 0; i_sample < n_samples; ++i_sample) {
            const sample_t& sample = samples[i_sample];
            uBool b_node_is_unique = uTRUE;

            point = sample.get_node_positions().unsafe_col(i_node);

            // See if current node is unique
            for (uUInt i_node_type = 0; i_node_type < monomer_counts.size();
                 ++i_node_type) {
                uMonomerTally& tally = monomer_counts[i_node_type];
                if (tally.first[uDim_X] == point[uDim_X] &&
                    tally.first[uDim_Y] == point[uDim_Y] &&
                    tally.first[uDim_Z] == point[uDim_Z]) {
                    // Node is not unique - update tally
                    uAssert(tally.second > 0);
                    b_node_is_unique = uFALSE;
                    ++tally.second;
                    break;
                }
            }

            // Node is unique, add it to counts array
            if (b_node_is_unique) {
                uMonomerTally tally(point, 1);
                monomer_counts.push_back(tally);
            }
        }  // end iteration over samples
    }      // end iteration over node positions
}

/**
 * @param out_percent_unique - The percent unique at each monomer position
 * @param samples - The samples to test for uniqueness
 */
template <typename sample_t>
void calc_percent_unique_monomers(uVecCol& out_percent_unique,
                                  const std::vector<sample_t>& samples) {
    // Early out if we have no samples
    out_percent_unique.clear();
    if (samples.empty()) {
        return;
    }

    // Determine counts of unique nodes at each monomer position
    uUniqueMonomerCountsMap unique_nodes;
    calc_unique_monomer_counts(unique_nodes, samples);

    // Resize
    const uUInt n_nodes = U_TO_UINT(unique_nodes.size());
    uAssert(n_nodes > 0);
    out_percent_unique.zeros(n_nodes);

    // Multiplication is faster than division
    const uReal inv_n_samples = U_TO_REAL(1.0) / U_TO_REAL(samples.size());

    // Convert absolute counts to proportions
    for (uUInt i_node = 0; i_node < n_nodes; ++i_node) {
        uAssertBoundsInc(unique_nodes[i_node].size(), 1, samples.size());
        out_percent_unique[i_node] =
            U_TO_REAL(unique_nodes[i_node].size()) * inv_n_samples;
        uAssertBoundsInc(
            out_percent_unique[i_node], U_TO_REAL(0.0), U_TO_REAL(1.0));
    }
}

/**
 * Utility function to log percentage of unique monomers
 * @param samples - The batch of samples to report unique monomers for
 */
template <typename sample_t>
void report_percent_unique_monomers(const std::vector<sample_t>& samples) {
    uVecCol percent_unique;
    calc_percent_unique_monomers(percent_unique, samples);

    const uUInt n_nodes = U_TO_UINT(percent_unique.size());
    const uUInt max_row_count = 10;

    uLogf("\nPercent unique at each node:\n");

    for (uUInt i_node = 0; i_node < n_nodes; ++i_node) {
        if ((i_node % max_row_count) == 0) {
            uLogf("\n[%d]: ", (int)i_node);
        }

        uLogf("%3.1f,\t", U_TO_REAL(100.0) * percent_unique[i_node]);
    }

    uLogf("\n\nAverage percent unique: %f\n",
          U_TO_REAL(100.0) * uMatrixUtils::mean(percent_unique));
}

}  // namespace uSisUtilsQc

#endif  // uSisUtilsQc_h
