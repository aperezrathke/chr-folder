//****************************************************************************
// uHashMap.h
//****************************************************************************

#ifndef uHashMap_h
#define uHashMap_h

/**
 * @brief An associative array implemented as a hash table
 */

#include "uBuild.h"

#ifdef U_BUILD_USE_SPARSEPP_HASH

    // https://github.com/greg7mdp/sparsepp
#   include <sparsepp/spp.h>
#   define U_HASH_MAP spp::sparse_hash_map
#   define U_HASH_SET_EMPTY_KEY(hash_, key_)
#   define U_HASH_SET_DELETED_KEY(hash_, key_)
#   define U_HASH_RESERVE(hash_, n_) (hash_).reserve(n_)

#elif defined(U_BUILD_USE_GOOGLE_SPARSEHASH)

    /**
     * http://goog-sparsehash.sourceforge.net/doc/dense_hash_map.html
     * dense_hash_map requires you call set_empty_key() immediately after
     * constructing the hash map, and before calling any other
     * dense_hash_map method. (This is the largest difference between the
     * dense_hash_map API and other hash - map APIs. The argument to
     * set_empty_key() should be a key - value that is never used for
     * legitimate hash map entries. If you have no such key value, you will be
     * unable to use dense_hash_map. It is an error to call insert() with an
     * item whose key is the "empty key."
     *
     * dense_hash_map also requires you call set_deleted_key() before calling
     * erase().The argument to set_deleted_key() should be a key-value that is
     * never used for legitimate hash map entries. It must be different from
     * the key-value used for set_empty_key(). It is an error to call erase()
     * without first calling set_deleted_key(), and it is also an error to
     * call insert() with an item whose key is the "deleted key."
     *
     * There is no need to call set_deleted_key if you do not wish to call
     * erase() on the hash map.
     *
     * It is acceptable to change the deleted-key at any time by calling
     * set_deleted_key() with a new argument. You can also call
     * clear_deleted_key(), at which point all keys become valid for insertion
     * but no hashtable entries can be deleted until set_deleted_key() is
     * called again.
     */

#   ifdef U_BUILD_USE_GOOGLE_SPARSEHASH_DENSE
#       include <sparsehash/dense_hash_map>
#       define U_HASH_MAP google::dense_hash_map
#       define U_HASH_SET_EMPTY_KEY(hash_, key_) (hash_).set_empty_key((key_))
#   else
#       include <sparsehash/sparse_hash_map>
#       define U_HASH_MAP google::sparse_hash_map
#       define U_HASH_SET_EMPTY_KEY(hash_, key_)
#   endif // U_BUILD_USE_GOOGLE_SPARSEHASH_DENSE

    /**
     * Interface appears to be based on SGI hash_map<>
     * https://www.sgi.com/tech/stl/hash_map.html
     */
    
#   define U_HASH_SET_DELETED_KEY(hash_, key_) (hash_).set_deleted_key((key_))

    /**
     * Pre-allocate memory for at least 'n' elements
     * Note: min_load_factor(0.0) prevents hash table from ever shrinking
     */
#   define U_HASH_RESERVE(hash_, n_)  \
    do {                              \
        (hash_).min_load_factor(0.0); \
        (hash_).resize(n_);           \
    } while (0)

#else

    // Not using google sparse hash, default to std::unordered_map
    // or boost::unordered_map
#   ifdef U_BUILD_CXX_11
#       include <unordered_map>
#       define U_HASH_MAP std::unordered_map
#   else
#       include <boost/unordered_map.hpp>
#       define U_HASH_MAP boost::unordered_map
#   endif // U_BUILD_CXX_11

#   define U_HASH_SET_EMPTY_KEY(hash_, key_)
#   define U_HASH_SET_DELETED_KEY(hash_, key_)
#   define U_HASH_RESERVE(hash_, n_) (hash_).reserve(n_)

#endif // U_BUILD_USE_GOOGLE_SPARSEHASH

/**
 * Hash map from key = unsigned integer to value = unsigned integer
 */
typedef U_HASH_MAP<unsigned int, unsigned int>  uUIntToUIntMap;

#endif  // uHashMap_h
