//****************************************************************************
// uSisSeoBendEnergyMixin.h
//****************************************************************************

/**
 * Sis = Sequential importance sampled
 * Seo = Single-end, ordered growth - assumes monomers are grown in order
 *  from beginning to end
 *
 * Defines an energy-based mixin for scoring samples. The sampling
 *  distribution is:
 *
 *      Ps(sample_i) = exp(-Energy(sample_i)) / sum_k{exp(-Energy(sample_k)}
 *
 * The target distribution is the unnormalized:
 *
 *      Pt(sample_i) = exp(-Energy(sample_i))
 *
 * Therefore, the unnormalized weight of a selected sample is:
 *
 *      w = Pt / Ps = sum_k{exp(-Energy(sample_k)}
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

/**
 * NOTE: Currently only persistence length bending energy is used
 * as reported in many texts, in particular:
 *
 *  Hsu, Hsiao-Ping, Wolfgang Paul, and Kurt Binder. "Polymer chain stiffness
 *      vs. excluded volume: A Monte Carlo study of the crossover towards the
 *      worm-like chain model." EPL (Europhysics Letters) 92, no. 2 (2010):
 *      28003.
 *
 *  Muralidhar, Abhiram, and Kevin D. Dorfman. "Kirkwood diffusivity of long
 *      semiflexible chains in nanochannel confinement." Macromolecules 48,
 *      no. 8 (2015): 2829-2839.
 *
 * The bending energy has form:
 *      (k_B/T)U_bend = kappa * (1 - cos(theta)) where (k_B/T) is the typical
 *  Boltzmann constant (k_b) and Temperature (T), kappa is the bending
 *  rigidity, and cos(theta) is the bond angle between adjacent monomer
 *  triplets.
 */

#ifndef uSisSeoBendEnergyMixin_h
#define uSisSeoBendEnergyMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uOpts.h"
#include "uParserRle.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uSisUtilsRc.h"
#include "uSseTable.h"
#include "uTypes.h"

//****************************************************************************
// Sim-level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSeoBendEnergySimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisSeoBendEnergySimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

//****************************************************************************
// Importance weight updating policies
//****************************************************************************

/**
 * Ps = sampling distribution
 * Pt = target distribution
 */

/**
 * Policy which updates log weight assuming Ps is locally normalized Boltzmann
 * distribution and Pt is improper Boltzmann distribution
 */
class uSisWeightPolicyBoltz2Boltz {
public:
    /**
     * Update weight according to Ps: Local Boltzmann, Pt: Improper Boltzmann
     * @param log_weight - current sample log weight
     * @param cand_defrag_nener_view - (negative energy - max_elem) at each
     *  candidate
     * @param max_elem - Maximum negative energy value among all candidates
     * @param z - Normalizing constant = sum(exp(cand_defrag_nener_view))
     * @param i - index of selected defrag candidate
     */
    inline static void update_lnweight(uReal& log_weight,
                                       const uVecCol& cand_defrag_nener_view,
                                       const uReal max_elem,
                                       const uReal z,
                                       const uUInt i) {
        /**
         * The probability of selecting a sample, Ps, is given by expression:
         *  Ps(i) = exp(-E_bend(i)') / sum_k{exp(-E_bend(k)')}
         * For numerical stability, the max element has been subtracted to
         * obtain an equivalent expression:
         *  Ps(i) = [exp(-E_bend(i)' - maxE) * exp(maxE)] /
         *              sum_k{exp(-E_bend(k)' - maxE) * exp(maxE)}
         *        = exp(-E_bend(i)' - maxE) / sum_k{exp(-E_bend(k)' - maxE)}
         * The unnormalized target distribution Pt(i) is given by expression:
         *  Pt(i) = exp(-E_bend(i)')
         *        = exp(-E_bend(i)' - maxE) * exp(maxE)
         * This implies that the new log weight for a selected sample is:
         *   log_w' = log_w + log{ Pt(i) / Ps(i) }
         *   log_w' = log_w + log{ [exp(-E_bend(i)' - maxE) * exp(maxE)] /
         *                         [exp(-E_bend(i)' - maxE) /
         *                              sum_k{exp(-E_bend(k)' - maxE)}] }
         *   log_w' = log_w + maxE + log{sum_k{exp(-E_bend(k)' - maxE)}}
         * See http://www.hongliangjie.com/2011/01/07/logsum/
         */
        log_weight += max_elem + log(z);
    }
};

/**
 * Policy which updates log weight assuming Ps is locally normalized Boltzmann
 * distribution and Pt is improper uniform distribution
 */
class uSisWeightPolicyBoltz2Unif {
public:
    /**
     * Update weight according to Ps: Local Boltzmann, Pt: Improper Uniform
     * @param log_weight - current sample log weight
     * @param cand_defrag_nener_view - (negative energy - max_elem) at each
     *  candidate
     * @param max_elem - Maximum negative energy value among all candidates
     * @param z - Normalizing constant = sum(exp(cand_defrag_nener_view))
     * @param i - index of selected defrag candidate
     */
    inline static void update_lnweight(uReal& log_weight,
                                       const uVecCol& cand_defrag_nener_view,
                                       const uReal max_elem,
                                       const uReal z,
                                       const uUInt i) {
        /**
         * Pt = 1 = Improper uniform
         * Ps = exp(-E_i) / sum_j{exp(-E_j)} = Locally normalized Boltzmann
         *
         * Weight update = Pt / Ps = 1 / ( exp(-E_i) / sum_j{exp(-E_j)} )
         *               = sum_j{exp(-E_j)} / exp(-E_i)
         *               = z / cand_defrag_pmass_view.at(i)
         *
         * Log weight update = log[Pt / Ps]
         *                   = log[sum_j{exp(-E_j)} / exp(-E_i)]
         *                   = log[sum_j{exp(-E_j)}] - log[exp(-E_i)]
         *                   = log[sum_j{exp(-E_j)}] - (-E_i)
         *                   = log[z] - cand_defrag_nener_view.at(i)
         *
         * Note, max_elem is unused as it cancels out in ratio
         */
        uAssert(z > U_TO_REAL(0.0));
        uAssertBounds(i, 0, cand_defrag_nener_view.n_elem);
        log_weight += log(z) - cand_defrag_nener_view.at(i);
    }
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue,
          typename t_WeightPolicy = uSisWeightPolicyBoltz2Boltz>
class uSisSeoBendEnergyMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Weight update policy
     */
    typedef t_WeightPolicy energy_weight_policy_t;

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * THIS INTERFACE METHOD IS INTENDED TO BE CALLED AFTER FIRST NODE OF
     * CHAIN HAS BEEN SEEDED.
     *
     * @WARNING - ASSUMES AT LEAST 1 LEGAL CANDIDATE!
     *
     * Special case for second node of chain: uniformly selects among legal
     * candidates (does not have energy bias). Will also update log_weight to
     * account for sampling bias.
     *
     * @param sample - Sample containing this mixin
     * @param log_weight - Current and output log weight of the sample
     * @param parent_id - Index of parent node within sample positions matrix
     *  (note, this is not same as candidate positions matrix)
     * @param candidate_positions - Matrix of row-major candidate positions
     * @param candidate_radius - Radius of candidate node
     * @param is_legal - [0, 1] vector : 1 if candidate is legal for
     *  selection, 0 otherwise
     * @param num_legal - Number of 1s in legal_candidates (must be > 0)
     * @param sim - Parent simulation containing global sample data
     * @return Index of selected candidate.
     */
    inline uUInt energy_select_2nd(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_legal >= 1);
        // Update weight
        log_weight += log(U_TO_REAL(num_legal));
        // Uniformly select a candidate
        return uSisUtils::runif_select_legal_growth_candidate(
            is_legal, num_legal U_THREAD_ID_ARG);
    }

    /**
     * THIS INTERFACE METHOD IS INTENDED TO BE CALLED AFTER FIRST AND SECOND
     * NODES OF A CHAIN HAVE BEEN SEQUENTIALLY GROWN.
     *
     * @WARNING - ASSUMES AT LEAST 1 LEGAL CANDIDATE!
     *
     * Selects energy samples according to Boltzmann distribution, corrects
     *  for sampling bias and sets target distribution as unnormalized
     *  Boltzmann distribution
     *
     * @param sample - Sample containing this mixin
     * @param log_weight - Current and output log weight of the sample
     * @param parent_id - Index of parent node within sample positions matrix
     *  (note, this is not same as candidate positions matrix)
     * @param candidate_positions - Matrix of row-major candidate positions
     * @param candidate_radius - Radius of candidate node
     * @param is_legal - [0, 1] vector : 1 if candidate is legal for
     *  selection, 0 otherwise
     * @param num_legal - Number of 1s in legal_candidates (must be > 0)
     * @param sim - Parent simulation containing global sample data
     * @return index of selected candidate.
     */
    inline uUInt energy_select_nth(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        // This method assumes at least a single legal candidate
        uAssertBoundsInc(num_legal, U_TO_UINT(1), U_TO_UINT(is_legal.n_elem));
        // Assume candidate positions are row-major - each row is <x,y,z> tuple
        uAssert(is_legal.size() == candidate_positions.n_rows);
        uAssert(candidate_positions.n_cols == uDim_num);
        // Assume is_legal is 0|1 vector
        uAssert(uMatrixUtils::sum(is_legal) == num_legal);
        // Assume candidates have non-trivial radius
        uAssert(candidate_radius > U_TO_REAL(0.0));

        /**
         * Compute bending energy - models stiffness of polymer. See:
         *
         *  Hiemenz, Paul C., and Timothy P. Lodge. Polymer chemistry. CRC
         *      press, 2007.
         *
         * for pretty good introduction to persistence length and Kuhn length.
         * Essentially, persistence length is defined (in non-excluded volume
         * worm-like chain [WLC]) as average distance between monomers such
         * that bond vectors are orthogonal (90 deg). The Kuhn length is
         * defined as the average distance such that bond lengths are facing
         * opposite directions (180 deg). The bending energy model is given
         * by the expression:
         *
         * The bending energy has form:
         *  (k_B/T)U_bend = kappa * (1 - cos(theta)) where (k_B/T) is the
         *      typical Boltzmann constant (k_b) and Temperature (T), kappa is
         *      the bending rigidity, and cos(theta) is the bond angle between
         *      adjacent monomer triplets.
         *
         * To match empirically observed persistence or Kuhn lengths, the
         * bending rigidity 'kappa' should be tuned such that the observed
         * moments are matched under excluded volume and confinement.
         */

        // Total number of candidates (including non-legal)
        const uUInt num_candidates = U_TO_UINT(is_legal.n_elem);
        uAssert(num_candidates >= num_legal);

        /**********************************************************************
         * Defragment candidate positions, ideally allows better cache
         * utilization and SIMD vectorization.
         */

        uVecCol& cand_defrag_nener_buff =
            sim.get_energy_cand_defrag_nener(U_THREAD_ID_0_ARG);
        uVecCol& cand_defrag_pmass_buff =
            sim.get_energy_cand_defrag_pmass(U_THREAD_ID_0_ARG);
        uSseTable& cand_defrag_pos_buff =
            sim.get_energy_cand_defrag_pos(U_THREAD_ID_0_ARG);
        uUIVecCol& cand_defrag_ix_buff =
            sim.get_energy_cand_defrag_ix(U_THREAD_ID_0_ARG);
        uAssert(cand_defrag_nener_buff.n_elem == candidate_positions.n_rows);
        uAssert(cand_defrag_pmass_buff.n_elem == candidate_positions.n_rows);
        uAssert(cand_defrag_pos_buff.n_rows == candidate_positions.n_rows);
        uAssert(cand_defrag_pos_buff.n_cols == candidate_positions.n_cols);
        uAssert(cand_defrag_ix_buff.n_elem == candidate_positions.n_rows);

        uReal* const cand_defrag_pos_mem = cand_defrag_pos_buff.memptr();
        uUInt i = 0;
        for (uUInt j = 0; j < num_candidates; ++j) {
            // @TODO - if we iterate column-major - does this have better cache?
            if (is_legal.at(U_TO_MAT_SZ_T(j))) {
                uAssertBounds(i, U_TO_UINT(0), num_legal);
                uAssert(i < cand_defrag_pos_buff.n_rows);
                uAssert(i < cand_defrag_ix_buff.n_elem);
                // Candidates are row-major!
                // Defragment positions
                const uUInt x_index =
                    uSseTable::to_1D_index(i, uDim_X, num_legal);
                const uUInt y_index =
                    uSseTable::to_1D_index(i, uDim_Y, num_legal);
                const uUInt z_index =
                    uSseTable::to_1D_index(i, uDim_Z, num_legal);
                uAssertBounds(x_index, 0, cand_defrag_pos_buff.n_elem);
                uAssertBounds(y_index, 0, cand_defrag_pos_buff.n_elem);
                uAssertBounds(z_index, 0, cand_defrag_pos_buff.n_elem);
                cand_defrag_pos_mem[x_index] =
                    candidate_positions.at(j, uDim_X);
                cand_defrag_pos_mem[y_index] =
                    candidate_positions.at(j, uDim_Y);
                cand_defrag_pos_mem[z_index] =
                    candidate_positions.at(j, uDim_Z);
                // Store mapping from defrag index to original candidate index
                cand_defrag_ix_buff.at(i++) = j;
            }
        }
        uAssert(num_legal == i);

        // Create cropped view for storing candidate -energies
        uVecCol cand_defrag_nener_view(
            cand_defrag_nener_buff.memptr(), /*aux_mem*/
            num_legal,                       /*n_elem*/
            false,                           /*copy_aux_mem*/
            true);                           /*strict*/

        // Create cropped view for storing candidate probability masses
        uVecCol cand_defrag_pmass_view(
            cand_defrag_pmass_buff.memptr(), /*aux_mem*/
            num_legal,                       /*n_elem*/
            false,                           /*copy_aux_mem*/
            true);                           /*strict*/

        // Create cropped view of defragmented candidate positions
        uMatrix cand_defrag_pos_view(cand_defrag_pos_buff.memptr(), /*aux_mem*/
                                     num_legal,                     /*n_rows*/
                                     uDim_num,                      /*n_cols*/
                                     false, /*copy_aux_mem*/
                                     true); /*strict*/

        // Create cropped view of defragmented candidate indices
        const uUIVecCol cand_defrag_ix_view(
            cand_defrag_ix_buff.memptr(), /*aux_mem*/
            num_legal,                    /*n_elem*/
            false,                        /*copy_aux_mem*/
            true);                        /*strict*/

        /**********************************************************************
         * Compute scaled parent bond vector
         */

        /**
         * Candidate (parent_id + 1) must be on third node of chain or greater!
         * Note, node positions are stored column-major (whereas candidate
         * positions are stored row-major).
         */
        uAssertBounds(parent_id, 1, sample.get_node_positions().n_cols - 1);
        const uReal* const prev_center =
            sample.get_node_positions().colptr(parent_id - 1);
        const uReal* const curr_center =
            sample.get_node_positions().colptr(parent_id);
        /**
         * @TODO - consider optimizing this if nodes are homogeneous
         * Determine parent scaling factor, idea is to scale the single parent
         * bond vector accordingly rather than waste cycles scaling all
         * candidate bond vectors
         */
        const uReal prev_radius = sample.get_node_radius(parent_id - 1, sim);
        uAssert(prev_radius > U_TO_REAL(0.0));
        const uReal curr_radius = sample.get_node_radius(parent_id, sim);
        uAssert(curr_radius > U_TO_REAL(0.0));
        const uReal bend_rigidity = sim.get_energy_bend_rigidity(parent_id);
        const uReal curr_bond_scale =
            bend_rigidity /
            ((prev_radius + curr_radius) * (curr_radius + candidate_radius));
        /**
         * Scaled parent bond vector (points from prev to parent).
         * Essentially, parent bond vector will have length:
         *  |parent| = (bend_rigidity / |candidate|) where |candidate| is the
         *      candidate bond length
         * Then computing projection of candidate bond onto parent:
         *  dot(parent, candidate)
         *      = |parent||candidate|cos(theta)
         *      = (bend_rigidity/|candidate|)|candidate|cos(theta)
         *      = bend_rigidity * cos(theta)
         * This saves a few instructions when computing the bending energy as
         * then all we have to do is add -bend_rigidity to each dot product:
         *  -E_bend = -bend_rigidity(1 - cos(theta))
         *          = -bend_rigidity + bend_rigidity * cos(theta)
         * However, we can actually skip the addition of -bend_rigidity as
         * it's just a scaling constant in the Boltzmann distribution that
         * will cancel out of all ratios. Hence, our effective energy is:
         *  -E_bend' = bend_rigidity * cos(theta)
         */
        const uReal curr_bond_vec[uDim_num] = {
            (curr_center[uDim_X] - prev_center[uDim_X]) * curr_bond_scale,
            (curr_center[uDim_Y] - prev_center[uDim_Y]) * curr_bond_scale,
            (curr_center[uDim_Z] - prev_center[uDim_Z]) * curr_bond_scale};
        // Create wrapper view for parent bond vector
        const uVecRow curr_bond_view(
            // BEGIN HACK - cast to const row vector view
            const_cast<uReal*>(&curr_bond_vec[0]), /*aux_mem*/
            // END HACK
            uDim_num, /*n_elem*/
            false,    /*copy_aux_mem*/
            true);    /*strict*/
        uAssertRealEq(uMatrixUtils::norm(curr_bond_view, 2),
                      fabs(bend_rigidity) / (curr_radius + candidate_radius));

        /**********************************************************************
         * Compute bend energy
         */

        /**
         * Convert column-based node center to row-based node center so that
         * we can use the resulting row vector for translation of the candidate
         * points (as candidate points are row-wise)
         */
        const uVecRow curr_center_view(
            // BEGIN HACK
            // Unfortunately, no easy way to convert from a column vector
            // view to a row vector view - so we have to cast away the const
            // temporarily
            const_cast<uReal*>(curr_center), /*aux_mem*/
            // END HACK
            uDim_num, /*aux_length*/
            false,    /*copy_aux_mem*/
            true);    /*strict*/

        // Compute unnormalized candidate bond vectors
        cand_defrag_pos_view.each_row() -= curr_center_view;
        // Verify candidate bonds have assumed bond length
        uAssert(check_candidate_bonds(cand_defrag_pos_view,
                                      curr_radius + candidate_radius));

        // Compute projection (dot product) of candidate bond onto scaled
        // parent bond
        cand_defrag_pos_view.each_row() %= curr_bond_view;
        cand_defrag_nener_view = cand_defrag_pos_view.unsafe_col(uDim_X) +
                                 cand_defrag_pos_view.unsafe_col(uDim_Y);
        cand_defrag_nener_view += cand_defrag_pos_view.unsafe_col(uDim_Z);
        // Each element of candidate negative energy view now has value:
        //  -E_bend(i)' = bend_rigidity * cos(theta_i)

        /**********************************************************************
         * Compute unnormalized candidate selection probability
         */

        /**
         * The probability of selecting a sample, Ps, is given by expression:
         *  Ps(i) = exp(-E_bend(i)') / sum_k{exp(-E_bend(k)')}
         * For numerical stability, lets subtract max element to obtain an
         * equivalent expression:
         *  Ps(i) = [exp(-E_bend(i)' - maxE) * exp(maxE)] /
         *              sum_k{exp(-E_bend(k)' - maxE) * exp(maxE)}
         *        = exp(-E_bend(i)' - maxE) / sum_k{exp(-E_bend(k)' - maxE)}
         */

        // Find max( bend_rigidity * cos(theta_i)) which will be in range
        // [-bend_rigidity, +bend_rigidity] since cos(theta) is in [-1,1].
        const uReal max_elem = cand_defrag_nener_view.max();
        uAssertRealBoundsInc(
            max_elem, -fabs(bend_rigidity), fabs(bend_rigidity));
        // Numerically stabilize vector by subtracting max element
        cand_defrag_nener_view -= max_elem;
        // Transform to exponential space
        // @TODO - verify this is not allocating new memory!
        cand_defrag_pmass_view = uMatrixUtils::exp(cand_defrag_nener_view);
        // Determine partition constant: sum_k{exp(-E_bend(k)' - maxE)}
        const uReal z = uMatrixUtils::sum(cand_defrag_pmass_view);
        uAssert(z > U_TO_REAL(0.0));

        /**********************************************************************
         * Select candidate and update log weight
         */

        // Select candidate using unnormalized probability
        i = uSisUtilsQc::rand_select_index(cand_defrag_pmass_view,
                                           z U_THREAD_ID_ARG);
        uAssertBounds(i, 0, cand_defrag_ix_view.n_elem);
        uAssertBounds(cand_defrag_ix_view.at(i), 0, is_legal.size());
        uAssert(is_legal.at(cand_defrag_ix_view.at(i)));

        // Update log weight
        energy_weight_policy_t::update_lnweight(
            log_weight, cand_defrag_nener_view, max_elem, z, i);

        // Map defrag index to original index
        return cand_defrag_ix_view.at(i);
    }

private:
#ifdef U_BUILD_ENABLE_ASSERT
    /**
     * @param cand_bond_vecs - matrix where each row is an unnormalized
     *  candidate bond vector pointing from previous node to parent
     * @param expected_bond_length - the expected bond length of each
     *  candidate bond
     * @return true if all candidate bond lengths are expected bond length,
     *  false otherwise
     */
    static bool check_candidate_bonds(const uMatrix& cand_bond_vecs,
                                      const uReal expected_bond_length) {
        for (uUInt i = 0; i < cand_bond_vecs.n_rows; ++i) {
            const uReal cand_bond_length =
                uMatrixUtils::norm(cand_bond_vecs.row(i), 2);
            if (!U_REAL_CMP_EQ(cand_bond_length, expected_bond_length)) {
                uAssert(false);
                return false;
            }
        }
        return true;
    }
#endif  // U_BUILD_ENABLE_ASSERT
};

#endif  // uSisSeoBendEnergyMixin_h
