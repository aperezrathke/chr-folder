//****************************************************************************
// uBitsetHandle.h
//****************************************************************************

/**
 * Wrapper interface handles to either a uBitset or a bit set
 * from a pooled array. This unifies both interfaces but also allows pooled
 * bit set to call on the parent (outer) array for shared properties.
 *
 * The metaphor 'soft' is meant to imply that the wrapper does not own the
 * underlying bit buffer. It does not manage (allocate/destroy) the underlying
 * bit buffer, it simply provides bit manipulation functionality.
 */

#ifndef uBitsetHandle_h
#define uBitsetHandle_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitset.h"
#include "uBitsetCompileTimeProperties.h"
#include "uFindLowestBitPositionUtil.h"

#include <algorithm>

//****************************************************************************
// uBitsetHandle
//****************************************************************************

/**
 * A class that wraps around a raw bit buffer and allows bit manipulation
 */
template <typename t_BitsetRunTimeProperties>
class uBitsetHandle : public uBitsetCompileTimeProperties {
public:
    /**
     * Constructor to initialize bit buffer and runtime properties
     */
    uBitsetHandle(block_type* bits,
                  const t_BitsetRunTimeProperties& runtime_properties)
        : m_bits(bits), m_runtime_properties(runtime_properties) {}

    // BEGIN HACK
    /**
     * Constructor which wraps a const buffer, this should only be used if
     * creating a const handle.
     * http://stackoverflow.com/questions/6936124/why-does-c-not-have-a-const-constructor
     */
    uBitsetHandle(const block_type* bits,
                  const t_BitsetRunTimeProperties& runtime_properties)
        : m_bits(const_cast<block_type*>(bits)),
          m_runtime_properties(runtime_properties) {}
    // END HACK

    /**
     * @return The number of bits in the bit set
     */
    inline size_type size() const { return m_runtime_properties.size(); }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline size_type num_blocks() const {
        return m_runtime_properties.num_blocks();
    }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline size_type num_bytes() const {
        return m_runtime_properties.num_bytes();
    }

    /**
     * We need to expose underlying buffer!
     * Non-const version
     */
    inline block_type* get_buffer() {
        uAssert(m_bits);
        return m_bits;
    };

    /**
     * We need to expose underlying buffer!
     * Const version
     */
    inline const block_type* get_buffer() const {
        uAssert(m_bits);
        return m_bits;
    };

        // bitset operations - based on boost::dynamic_bitset

#ifdef U_BUILD_CXX_11
    template <typename t_bitset>
    uBitsetHandle& operator=(const t_bitset&& rhs);
#else
    uBitsetHandle& operator=(const uBitsetHandle&);
    template <typename t_bitset>
    uBitsetHandle& operator=(const t_bitset& rhs);
#endif  // U_BUILD_CXX_11

    template <typename t_bitset>
    uBitsetHandle& operator&=(const t_bitset& rhs);

    template <typename t_bitset>
    uBitsetHandle& operator|=(const t_bitset& rhs);

    template <typename t_bitset>
    uBitsetHandle& operator^=(const t_bitset& rhs);

    template <typename t_bitset>
    uBitsetHandle& operator-=(const t_bitset& rhs);

    uBitsetHandle& operator<<=(const size_type n);
    uBitsetHandle& operator>>=(size_type n);
    uBitset operator<<(size_type n) const;
    uBitset operator>>(size_type n) const;

    // basic bit operations - based on boost::dynamic_bitset

    uBitsetHandle& set(size_type n, bool val = true);
    uBitsetHandle& set();
    uBitsetHandle& reset(size_type n);
    uBitsetHandle& reset();
    uBitsetHandle& flip(size_type n);
    uBitsetHandle& flip();
    bool test(size_type n) const;
    bool any() const;
    bool none() const;
    uBitset operator~() const;
    size_type count() const;

    template <typename t_bitset>
    bool is_subset_of(const t_bitset& a) const;

    template <typename t_bitset>
    bool is_proper_subset_of(const t_bitset& a) const;

    template <typename t_bitset>
    bool intersects(const t_bitset& b) const;

    // lookup / bit iteration routines - based on boost::dynamic_bitset

    size_type find_first() const;
    size_type find_next(size_type pos) const;

    /**
     * Converts this handle into a stand-alone 'hard' bit set which manages
     * its own internal bit buffer
     * @return the converted parameter bit set
     */
    uBitset& to_hard_bitset(uBitset& out_bit_set) const;

    // comparison

    template <typename t_BitsetRunTimeProperties_, typename t_bitset>
    friend bool operator==(const uBitsetHandle<t_BitsetRunTimeProperties_>& a,
                           const t_bitset& b);

private:
    bool m_unchecked_test(size_type pos) const;
    size_type m_do_find_from(size_type first_block) const;
    index_type m_lowest_bit_position(block_type b) const;

    // Utilities - based on boost::dynamic_bitset
    block_type& m_highest_block();
    const block_type& m_highest_block() const;
    void m_zero_unused_bits();
    inline block_width_type count_extra_bits() const {
        return bit_index(size());
    }
    inline static size_type block_index(size_type pos) {
        return pos / bits_per_block;
    }
    inline static block_width_type bit_index(size_type pos) {
        return static_cast<block_width_type>(pos % bits_per_block);
    }
    inline static block_type bit_mask(size_type pos) {
        return block_type(1) << bit_index(pos);
    }

    /**
     * The raw bit buffer for bit manipulation
     */
    block_type* m_bits;

    /**
     * Properties to determine bit, block, and byte counts
     */
    const t_BitsetRunTimeProperties m_runtime_properties;
};

//****************************************************************************
// uBitsetUtils
//****************************************************************************

// BEGIN EVIL HACK - expose boost::dynamic_bitset::m_bits

// http://www.gotw.ca/gotw/076.htm
// In order to allow efficient interoperability between soft bit sets and hard
// bit sets, it would make our lives easier if we had access to the underlying
// buffer used by boost::dynamic_bitset (uBitset). This buffer is private so
// we need to do some evil hacks to expose it. The hack in question involves
// specializing a template member function to get at what we need. This is
// evil.

// First, create a unique type to specialize on:
struct uBitsetExposePrivateMemberHack_t {
    uBitsetCompileTimeProperties::block_type** mpp_bits;
};

// Next, specialize template member function to expose private member:
namespace boost {
    template <>
    template <>
    inline void uBitset::append(uBitsetExposePrivateMemberHack_t first,
        uBitsetExposePrivateMemberHack_t last) {
        *first.mpp_bits =
            (uBitsetCompileTimeProperties::block_type*)(&(this->m_bits[0]));
    }
}  // namespace boost
// END EVIL HACK

/**
 * An adapter/bridge namespace for operations common to various bitset
 * implementations such as boost::dynamic_bitset and uSoftBitsetHandle
 */
namespace uBitsetUtils {
    /**
     * Generic function to get underlying bit buffer
     */
    template <typename t_bitset>
    inline uBitsetCompileTimeProperties::block_type* get_bitset_buffer(
        const t_bitset& bitset) {
        uAssert(false && "No template match for bit set type!");
        return NULL;
    }

    // BEGIN EVIL HACK  - expose uBitset::m_bits

    // Finally, create our interface function to utilize the loophole:
    /**
     * Specialized template to get underlying bit buffer in a uBitset
     */
    inline uBitsetCompileTimeProperties::block_type* get_bitset_buffer(
        uBitset& bitset) {
        uBitsetCompileTimeProperties::block_type* out_buffer = NULL;
        uBitsetExposePrivateMemberHack_t evil_hack = {&out_buffer};
        bitset.append(evil_hack, evil_hack);
        uAssert(out_buffer != NULL);
        return out_buffer;
    }

    /**
     * Specialize template to get underlying const bit buffer in a uBitset
     */
    inline const uBitsetCompileTimeProperties::block_type* get_const_bitset_buffer(
        const uBitset& bitset) {
        return get_bitset_buffer(const_cast<uBitset&>(bitset));
    }

    // END EVIL HACK

    /**
     * Specialized template to get underlying bit buffer in a pooled bitset handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline uBitsetCompileTimeProperties::block_type* get_bitset_buffer(
        uBitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return &(bitset.get_buffer()[0]);
    }

    /**
     * Specialized template to get underlying const bit buffer in a pooled bitset
     * handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline const uBitsetCompileTimeProperties::block_type* get_const_bitset_buffer(
        const uBitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return &(bitset.get_buffer()[0]);
    }

    /**
     * Generic function to get number of bytes in a bitset
     */
    template <typename t_bitset>
    inline uBitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const t_bitset& bitset) {
        uAssert(false && "No template match for bit set type!");
        return 0;
    }

    /**
     * Specialized template to determine number of bytes in a uBitset
     */
    inline uBitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const uBitset& bitset) {
        return bitset.num_blocks() * sizeof(uBitset::block_type);
    }

    /**
     * Specialized template to determine number of bytes in a pooled bitset handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline uBitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const uBitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return bitset.num_bytes();
    }

    /**
     * @return minimum number of bytes between the two bit set
     */
    template <typename t_BitsetA, typename t_BitsetB>
    inline uBitsetCompileTimeProperties::size_type get_bitset_min_num_bytes(
        const t_BitsetA& bitset_a,
        const t_BitsetB& bitset_b) {
        return std::min<>(get_bitset_num_bytes(bitset_a),
                          get_bitset_num_bytes(bitset_b));
    }

}  // end of namespace uBitsetUtils

/////////////////////////////////////

//****************************************************************************
// uBitsetWrapperRunTimeProperties
//****************************************************************************

/**
 * Run time properties policy for use in uSoftBitsetHandle
 * Wraps a uBitset
 */
class uBitsetWrapperRunTimeProperties {
public:
    /**
     * @return The number of bits in the bit set
     */
    inline uBitset::size_type size() const { return m_parent_bit_set.size(); }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline uBitset::size_type num_blocks() const {
        return m_parent_bit_set.num_blocks();
    }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline uBitset::size_type num_bytes() const {
        return uBitsetUtils::get_bitset_num_bytes(m_parent_bit_set);
    }

    /**
     * Constructor
     */
    explicit uBitsetWrapperRunTimeProperties(const uBitset& parent_bit_set)
        : m_parent_bit_set(parent_bit_set) {}

private:
    /**
     * Reference to bit set used for defining the run time properties
     */
    const uBitset& m_parent_bit_set;
};

//****************************************************************************
// uBitsetHandle
//****************************************************************************

//-----------------------------------------------------------------------------
// bitset operations

// @WARNING - assignment operator assumes lhs and rhs are same buffer size.
//  If this is not the case, then no overflow will occur but it is possible
//  that an unallocated portion of memory will be read (leading to a segfault).

#ifdef U_BUILD_CXX_11

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator=(const t_bitset&& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

#else

// Overload default assignment operator
template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator=(
        const uBitsetHandle<t_BitsetRunTimeProperties>& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator=(const t_bitset& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

#endif  // U_BUILD_CXX_11

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator&=(const t_bitset& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] &= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator|=(const t_bitset& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] |= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator^=(const t_bitset& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] ^= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator-=(const t_bitset& rhs) {
    uAssert(size() == rhs.size());
    uAssert(num_blocks() == rhs.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(uBitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i)
        m_bits[i] &= ~rhs_buff[i];
    return *this;
}

//-----------------------------------------------------------------------------

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool uBitsetHandle<t_BitsetRunTimeProperties>::is_subset_of(
    const t_bitset& a) const {
    uAssert(size() == a.size());
    uAssert(num_blocks() == a.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* a_buff(uBitsetUtils::get_const_bitset_buffer(a));
    for (size_type i = 0; i < num_blocks(); ++i) {
        if (m_bits[i] & ~a_buff[i]) {
            return false;
        }
    }
    return true;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool uBitsetHandle<t_BitsetRunTimeProperties>::is_proper_subset_of(
    const t_bitset& a) const {
    uAssert(size() == a.size());
    uAssert(num_blocks() == a.num_blocks());
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));

    bool proper = false;

    const block_type* a_buff(uBitsetUtils::get_const_bitset_buffer(a));
    for (size_type i = 0; i < num_blocks(); ++i) {
        const block_type& bt = m_bits[i];
        const block_type& ba = a_buff[i];

        if (bt & ~ba)
            return false;  // not a subset at all
        if (ba & ~bt)
            proper = true;
    }

    return proper;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool uBitsetHandle<t_BitsetRunTimeProperties>::intersects(
    const t_bitset& b) const {
    uAssert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    size_type common_blocks =
        (num_blocks() < b.num_blocks()) ? num_blocks() : b.num_blocks();
    const block_type* b_buff(uBitsetUtils::get_const_bitset_buffer(b));
    for (size_type i = 0; i < common_blocks; ++i) {
        if (m_bits[i] & b_buff[i]) {
            return true;
        }
    }
    return false;
}

//-----------------------------------------------------------------------------

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator<<=(const size_type n) {
    // If shift amount is greater than bit set size, then just reset everything
    // to zero
    if (n >= size()) {
        return reset();
    }

    // else
    if (n > 0) {
        const size_type last = num_blocks() - 1;   // num_blocks() is >= 1
        const size_type div = n / bits_per_block;  // div is <= last
        const block_width_type r = bit_index(n);
        block_type* const b = &m_bits[0];

        if (r != 0) {
            block_width_type const rs = bits_per_block - r;

            for (size_type i = last - div; i > 0; --i) {
                b[i + div] = (b[i] << r) | (b[i - 1] >> rs);
            }
            b[div] = b[0] << r;
        } else {
            for (size_type i = last - div; i > 0; --i) {
                b[i + div] = b[i];
            }
            b[div] = b[0];
        }

        // zero out div blocks at the less significant end
        std::fill_n(b, div, static_cast<block_type>(0));

        // zero out any 1 bit that flowed into the unused part
        m_zero_unused_bits();  // thanks to Lester Gong
    }

    return *this;
}

// Based on boost::dynamic_bitset
template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::operator>>=(size_type n) {
    if (n >= size()) {
        return reset();
    }

    // else
    if (n > 0) {
        const size_type last = num_blocks() - 1;   // num_blocks() is >= 1
        const size_type div = n / bits_per_block;  // div is <= last
        const block_width_type r = bit_index(n);
        block_type* const b = &m_bits[0];

        if (r != 0) {
            block_width_type const ls = bits_per_block - r;

            for (size_type i = div; i < last; ++i) {
                b[i - div] = (b[i] >> r) | (b[i + 1] << ls);
            }
            // r bits go to zero
            b[last - div] = b[last] >> r;
        } else {
            for (size_type i = div; i <= last; ++i) {
                b[i - div] = b[i];
            }
            // note the '<=': the last iteration 'absorbs'
            // b[last-div] = b[last] >> 0;
        }

        // div blocks are zero filled at the most significant end
        std::fill_n(b + (num_blocks() - div), div, static_cast<block_type>(0));
    }

    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitset uBitsetHandle<t_BitsetRunTimeProperties>::operator<<(
    size_type n) const {
    uBitset r;
    return to_hard_bitset(r) <<= n;
}

template <typename t_BitsetRunTimeProperties>
uBitset uBitsetHandle<t_BitsetRunTimeProperties>::operator>>(
    size_type n) const {
    uBitset r;
    return to_hard_bitset(r) >>= n;
}

//-----------------------------------------------------------------------------
// global bitset operators

template <typename t_BitsetRunTimeProperties, typename t_bitset>
uBitset operator&(const uBitsetHandle<t_BitsetRunTimeProperties>& x,
                  const t_bitset& y) {
    uAssert(sizeof(uBitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
            sizeof(uBitset::block_type));
    uBitset b;
    x.to_hard_bitset(b);
    uBitsetCompileTimeProperties::block_type* b_buff(
        uBitsetUtils::get_bitset_buffer(b));
    uBitsetHandle<uBitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, uBitsetWrapperRunTimeProperties(b));
    b_wrapped &= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
uBitset operator|(const uBitsetHandle<t_BitsetRunTimeProperties>& x,
                  const t_bitset& y) {
    uAssert(sizeof(uBitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
            sizeof(uBitset::block_type));
    uBitset b;
    x.to_hard_bitset(b);
    uBitsetCompileTimeProperties::block_type* b_buff(
        uBitsetUtils::get_bitset_buffer(b));
    uBitsetHandle<uBitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, uBitsetWrapperRunTimeProperties(b));
    b_wrapped |= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
uBitset operator^(const uBitsetHandle<t_BitsetRunTimeProperties>& x,
                  const t_bitset& y) {
    uAssert(sizeof(uBitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
            sizeof(uBitset::block_type));
    uBitset b;
    x.to_hard_bitset(b);
    uBitsetCompileTimeProperties::block_type* b_buff(
        uBitsetUtils::get_bitset_buffer(b));
    uBitsetHandle<uBitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, uBitsetWrapperRunTimeProperties(b));
    b_wrapped ^= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
uBitset operator-(const uBitsetHandle<t_BitsetRunTimeProperties>& x,
                  const t_bitset& y) {
    uAssert(sizeof(uBitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
            sizeof(uBitset::block_type));
    uBitset b;
    x.to_hard_bitset(b);
    uBitsetCompileTimeProperties::block_type* b_buff(
        uBitsetUtils::get_bitset_buffer(b));
    uBitsetHandle<uBitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, uBitsetWrapperRunTimeProperties(b));
    b_wrapped -= y;
    return b;
}

//-----------------------------------------------------------------------------
// basic bit operations

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::set(size_type pos, bool val) {
    uAssert(pos < size());

    if (val) {
        m_bits[block_index(pos)] |= bit_mask(pos);
    } else {
        reset(pos);
    }

    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::set() {
    memset(&m_bits[0], ~block_type(0), num_bytes());
    m_zero_unused_bits();
    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::reset(size_type pos) {
    uAssert(pos < size());
    m_bits[block_index(pos)] &= ~bit_mask(pos);
    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::reset() {
    memset(&m_bits[0], block_type(0), num_bytes());
    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::flip(size_type pos) {
    uAssert(pos < size());
    m_bits[block_index(pos)] ^= bit_mask(pos);
    return *this;
}

template <typename t_BitsetRunTimeProperties>
uBitsetHandle<t_BitsetRunTimeProperties>&
    uBitsetHandle<t_BitsetRunTimeProperties>::flip() {
    for (size_type i = 0; i < num_blocks(); ++i) {
        m_bits[i] = ~m_bits[i];
    }
    m_zero_unused_bits();
    return *this;
}

template <typename t_BitsetRunTimeProperties>
bool uBitsetHandle<t_BitsetRunTimeProperties>::m_unchecked_test(
    size_type pos) const {
    // @TODO - see intrinsic for bit testing: (MS specific)
    //  _bittest, _bittest65
    // https://msdn.microsoft.com/en-us/librarY/h65k4tze(v=vs.120).aspx
    return (m_bits[block_index(pos)] & bit_mask(pos)) != 0;
}

template <typename t_BitsetRunTimeProperties>
bool uBitsetHandle<t_BitsetRunTimeProperties>::test(size_type pos) const {
    uAssert(pos < size());
    return m_unchecked_test(pos);
}

template <typename t_BitsetRunTimeProperties>
bool uBitsetHandle<t_BitsetRunTimeProperties>::any() const {
    for (size_type i = 0; i < num_blocks(); ++i) {
        if (m_bits[i]) {
            return true;
        }
    }
    return false;
}

template <typename t_BitsetRunTimeProperties>
inline bool uBitsetHandle<t_BitsetRunTimeProperties>::none() const {
    return !any();
}

template <typename t_BitsetRunTimeProperties>
uBitset uBitsetHandle<t_BitsetRunTimeProperties>::operator~() const {
    uBitset b;
    return to_hard_bitset(b).flip();
}

#include "uBitsetHandleCountBits.h"

// --------------------------------
// lookup

// look for the first bit "on", starting
// from the block with index first_block
//
template <typename t_BitsetRunTimeProperties>
typename uBitsetHandle<t_BitsetRunTimeProperties>::size_type
    uBitsetHandle<t_BitsetRunTimeProperties>::m_do_find_from(
        size_type first_block) const {
    size_type i = first_block;

    // skip null blocks
    while ((i < num_blocks()) && (m_bits[i] == 0)) {
        ++i;
    }

    if (i >= num_blocks()) {
        return npos;  // not found
    }

    return i * bits_per_block + m_lowest_bit_position(m_bits[i]);
}

template <typename t_BitsetRunTimeProperties>
typename uBitsetHandle<t_BitsetRunTimeProperties>::index_type
    uBitsetHandle<t_BitsetRunTimeProperties>::m_lowest_bit_position(
        block_type b) const {
    uAssert(sizeof(block_type) == 8);
    index_type lowest_bit_position = 0;
#ifdef U_BUILD_COMPILER_MSVC
    // Disable C4552 warning:
    // "!= operator has no effect; expected operator with side-effect".
    // uVerify is meant to be an assert when enabled, and to still execute
    // code when asserts are disabled.
#pragma warning(push)
#pragma warning(disable : 4552)
#endif  // U_BUILD_COMPILER_MSVC
    uVerify(0 !=
            uBitsetUtils::find_lowest_bit_position(&lowest_bit_position, b));
#ifdef U_BUILD_COMPILER_MSVC
#pragma warning(pop)
#endif  // U_BUILD_COMPILER_MSVC
    return lowest_bit_position;
}

template <typename t_BitsetRunTimeProperties>
typename uBitsetHandle<t_BitsetRunTimeProperties>::size_type
    uBitsetHandle<t_BitsetRunTimeProperties>::find_first() const {
    return m_do_find_from(0);
}

template <typename t_BitsetRunTimeProperties>
typename uBitsetHandle<t_BitsetRunTimeProperties>::size_type
    uBitsetHandle<t_BitsetRunTimeProperties>::find_next(size_type pos) const {
    const size_type sz = size();
    if (pos >= (sz - 1) || sz == 0) {
        return npos;
    }

    ++pos;

    const size_type blk = block_index(pos);
    const block_width_type ind = bit_index(pos);

    // shift bits up to one immediately after current
    const block_type fore = m_bits[blk] >> ind;

    return fore ? pos + static_cast<size_type>(m_lowest_bit_position(fore))
                : m_do_find_from(blk + 1);
}

// Converts this handle into a stand-alone PkBitSet
template <typename t_BitsetRunTimeProperties>
uBitset& uBitsetHandle<t_BitsetRunTimeProperties>::to_hard_bitset(
    uBitset& out_bitset) const {
    uAssert(sizeof(block_type) == sizeof(uBitset::block_type));
    out_bitset.resize(size(), false);
    block_type* out_buff(uBitsetUtils::get_bitset_buffer(out_bitset));
    memcpy(out_buff,
           &(m_bits[0]),
           uBitsetUtils::get_bitset_min_num_bytes(out_bitset, *this));
    return out_bitset;
}

//-----------------------------------------------------------------------------
// comparison

template <typename t_BitsetRunTimeProperties, typename t_bitset>
inline bool operator==(const uBitsetHandle<t_BitsetRunTimeProperties>& a,
                       const t_bitset& b) {
    const typename uBitsetHandle<t_BitsetRunTimeProperties>::block_type* b_buff(
        uBitsetUtils::get_const_bitset_buffer(b));
    return (a.size() == b.size()) &&
           (0 == memcmp(&(a.get_buffer()[0]),
                        b_buff,
                        uBitsetUtils::get_bitset_min_num_bytes(a, b)));
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
inline bool operator!=(const uBitsetHandle<t_BitsetRunTimeProperties>& a,
                       const t_bitset& b) {
    return !(a == b);
}

// Based on boost::dynamic_bitset
// gives a reference to the highest block
//
template <typename t_BitsetRunTimeProperties>
inline typename uBitsetHandle<t_BitsetRunTimeProperties>::block_type&
    uBitsetHandle<t_BitsetRunTimeProperties>::m_highest_block() {
    return const_cast<block_type&>(
        static_cast<const uBitsetHandle*>(this)->m_highest_block());
}

// Based on boost::dynamic_bitset
// gives a const-reference to the highest block
//
template <typename t_BitsetRunTimeProperties>
inline const typename uBitsetHandle<t_BitsetRunTimeProperties>::block_type&
    uBitsetHandle<t_BitsetRunTimeProperties>::m_highest_block() const {
    uAssert(size() > 0 && num_blocks() > 0);
    return m_bits[num_blocks() - 1];
}

// Based on boost::dynamic_bitset
// If size() is not a multiple of bits_per_block
// then not all the bits in the last block are used.
// This function resets the unused bits (convenient
// for the implementation of many member functions)
//
template <typename t_BitsetRunTimeProperties>
inline void uBitsetHandle<t_BitsetRunTimeProperties>::m_zero_unused_bits() {
    uAssert(num_blocks() == calc_num_blocks(size()));

    // if != 0 this is the number of bits used in the last block
    const block_width_type extra_bits = count_extra_bits();

    if (extra_bits != 0) {
        m_highest_block() &= ~(~static_cast<block_type>(0) << extra_bits);
    }
}

// Utility macros

/**
 * Define a stand-alone bit set type that leverages compiler intrinsics for
 * faster bit iteration
 */
typedef uBitsetHandle<uBitsetWrapperRunTimeProperties> uWrappedBitset;

/**
 * Macro to obtain a wrapped bit set handle from a uBitset
 */
#define U_WRAP_BITSET(b)                                 \
    uWrappedBitset(uBitsetUtils::get_bitset_buffer((b)), \
                   uBitsetWrapperRunTimeProperties((b)))

#endif  // uBitsetHandle_h
