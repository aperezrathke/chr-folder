//****************************************************************************
// uSisDynamicEssScheduleMixin.h
//****************************************************************************

/**
 * ScheduleMixin = A mixin that defines the checkpoint schedule for
 * a quality control algorithm.
 */

#ifndef uSisDynamicEssScheduleMixin_h
#define uSisDynamicEssScheduleMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * The default threshold at which a checkpoint will be triggered
 */
#define uSisDynamicEssScheduleMixin_DEFAULT_THRESH 0.2

/**
 * A quality control schedule is responsible for determining when check
 * points occur.
 *
 * This dynamic schedule will trigger a checkpoint if the effective sample
 * size is below a parameter threshold
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing from configuration file
    enum uOptE e_Key = uOpt_qc_schedule_dynamic_ess_thresh_slot0>
class uSisDynamicEssScheduleMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin avoids recomputing exponential
     * weights
     */
    enum { calculates_norm_exp_weights = true };

    /**
     * Default constructor
     */
    uSisDynamicEssScheduleMixin() { clear(); }

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     * @param is_tr - [unused] set to true if schedule is being used for
     *  landmark trial running, false o/w
     */
    void init(const sim_t& sim,
              uSpConstConfig_t config,
              const bool is_tr U_THREAD_ID_PARAM) {
        clear();
        // Check if option exists in config
        config->read_into<uReal>(this->thresh, e_Key);
        // We require that our threshold value be non-negative
        uAssert(this->thresh >= U_TO_REAL(0.0));
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config,
                    const bool is_tr) {}

    /**
     * Resets to default state
     */
    void clear() { this->thresh = uSisDynamicEssScheduleMixin_DEFAULT_THRESH; }

    /**
     * Determines if a quality control check point has been reached (meaning
     * it's okay to go ahead and perform quality control step).
     * @param out_norm_exp_weights - an optional *uninitialized* vector of
     *  exponential weights that must be initialized if:
     *      calculates_norm_exp_weights == TRUE
     * @param out_max_log_weight - an optional *uninitialized* reference to
     *  the maximum log weight within the log_weights vector. Must be
     *  initialized if: calculates_norm_exp_weights == TRUE
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin
     * @param sim - parent simulation
     */
    inline uBool qc_checkpoint_reached(
        uVecCol& out_norm_exp_weights,
        uReal& out_max_log_weight,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_level_qc_mixin_t& qc,
        const sim_t& sim) {
        const uReal ess = uSisUtilsQc::calc_ess_cv_from_log(
            out_norm_exp_weights, out_max_log_weight, log_weights);
        return ess < this->thresh;
    }

private:
    /**
     * A check point is encountered if effective sample size falls
     * below this value
     */
    uReal thresh;
};

#endif  // uSisDynamicEssScheduleMixin_h
