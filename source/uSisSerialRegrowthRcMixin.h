//****************************************************************************
// uSisSerialRegrowthRcMixin.h
//****************************************************************************

/**
 * SerialRegrowthRcMixin = A mixin that implements a serial regrowth procedure
 * during rejection control. For more info on rejection control:
 *
 * Liu, Jun S., Rong Chen, and Wing Hung Wong. "Rejection control and
 *  sequential importance sampling." Journal of the American Statistical
 *  Association 93.443 (1998): 1022-1031.
 */

#ifndef uSisSerialRegrowthRcMixin_h
#define uSisSerialRegrowthRcMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uSisNullQcModMixin.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Performs batch regrowth operations
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisSerialRegrowthRcMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Regrows all samples for which should_regrow is true
     * @param status - the current growth status of all samples. Only samples
     *  that are regrown will have their status updated
     * @param samples - samples that need to be regrown
     * @param log_weights - the log weights of all samples. Only samples that
     *  are regrown will have their log weights updated
     * @param qc - the parent quality control mixin
     * @param sim - the parent simulation
     * @param should_regrow - i-th sample is regrown if i-th bit is 1
     * @param target_num_grow_steps - the number of grow steps to complete
     */
    inline void rc_batch_regrow(uWrappedBitset& status,
                                std::vector<sample_t>& samples,
                                uVecCol& log_weights,
                                sim_level_qc_mixin_t& qc,
                                sim_t& sim,
                                const uWrappedBitset& should_regrow,
                                const uUInt target_num_grow_steps
                                    U_THREAD_ID_PARAM) {
        bool result = false;
        for (size_t i = should_regrow.find_first(); i != should_regrow.npos;
             i = should_regrow.find_next(i)) {
            uAssertBounds(i, 0, status.size());
            uAssertBounds(i, 0, samples.size());
            uAssertBounds(i, 0, log_weights.size());
            result = qc.rc_regrow(samples[i],
                                  log_weights.at(U_TO_MAT_SZ_T(i)),
                                  sim,
                                  target_num_grow_steps,
                                  U_TO_UINT(i) U_THREAD_ID_ARG);
            status.set(i, result);
        }  // end iteration over regrown samples
    }
};

#endif  // uSisSerialRegrowthRcMixin_h
