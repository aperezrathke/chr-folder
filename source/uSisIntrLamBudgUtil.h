//****************************************************************************
// uSisIntrLamBudgUtil.h
//****************************************************************************

/**
 * Utility header for budgeted lamina interactions
 */

#ifndef uSisIntrLamBudgUtil_h
#define uSisIntrLamBudgUtil_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// Typedefs and structs
//****************************************************************************

/**
 * Information needed for failed interaction removal at selected candidate
 */
typedef struct {
    /**
     * Index into interaction vector
     */
    uUInt intr_id;
    /**
     * Locus identifier corresponding to interaction fragment
     */
    uUInt frag_lid;
} uSisIntrLamBudgFail_t;

/**
 * List of failed interactions at a single candidate
 */
typedef std::vector<uSisIntrLamBudgFail_t> uSisIntrLamBudgFailList_t;

/**
 * Table of failed interactions for all candidates, where each row is list of
 *  failed interactions at a single candidate
 */
typedef std::vector<uSisIntrLamBudgFailList_t> uSisIntrLamBudgFailTable_t;

/**
 * Budgeted lamina interaction failure policy arguments, stores scratch
 *  buffers for tracking failed interactions at all candidates
 * @WARNING - DO NOT CHANGE THE ORDER OF THESE DATA MEMBERS WITHOUT ALSO
 *  CHANGING ALL INSTANCES WHERE THIS STRUCTURE IS INITIALIZED!
 */
typedef struct {
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-in, lamina interaction failures at each candidate position
     */
    uUIVecCol& kin_delta_fails;
    /**
     * Handle to scratch buffer for tallying incremental counts (deltas) of
     *  knock-out, lamina interaction failures at each candidate position
     */
    uUIVecCol& ko_delta_fails;
    /**
     * Handle to scratch table for storing failed knock-in, lamina
     *  interactions at each candidate position
     */
    uSisIntrLamBudgFailTable_t& kin_fail_table;
    /**
     * Handle to scratch table for storing failed knock-in, lamina
     *  interactions at each candidate position
     */
    uSisIntrLamBudgFailTable_t& ko_fail_table;
} uSisIntrLamBudgFailPolicyArgs_t;

/**
 * Lamina knock-in budget access policy
 */
template <typename t_SisGlue>
class uSisIntrLamBudgAccKin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_lam_kin_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

/**
 * Lamina knock-out budget access policy
 */
template <typename t_SisGlue>
class uSisIntrLamBudgAccKo {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate concrete budget access
     */
    enum { is_null_intr_budg = false };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        const uUIVecCol& delta_fails =
            sim.get_intr_lam_ko_delta_fails(U_THREAD_ID_0_ARG);
        uAssertBounds(
            U_TO_MAT_SZ_T(candidate_id), U_TO_MAT_SZ_T(0), delta_fails.n_elem);
        const uUInt delta_fail = delta_fails.at(candidate_id);
        return delta_fail;
    }
};

#endif  // uSisIntrLamBudgUtil_h
