//****************************************************************************
// uTestSisDelphicPowerResamplingQc.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * but with Delphic power resampling as the quality control (Qc) mixin.
 *
 * Usage:
 * -test_sis_delphic_power_resampling_qc
 */

#ifdef uTestSisDelphicPowerResamplingQc_h
#   error "Test Sis Delphic Power Resampling Qc included multiple times!"
#endif  // uTestSisPowerResamplingQc_h
#define uTestSisDelphicPowerResamplingQc_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uThread.h"
#include "uTypes.h"

// Resampling mixin
#include "uSisDelphicPowerSamplerRsMixin.h"
#include "uSisDynamicEssScheduleMixin.h"
#include "uSisQcSummaryMeanWeight.h"
#include "uSisResamplingQcSimLevelMixin.h"
#include "uSisSampleCopier_unsafe.h"

// Use null QC as a control
#include "uMockUpUtils.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"

#include <iostream>
#include <string>

/**
 * Macro for common typedefs between parent and child glue, does not declare
 *  quality control component
 */
#define U_TEST_SIS_DEL_POW_RS_QC_DECLARE_COMMON_TYPEDEFS(t_Glue__)             \
    typedef uSisSim<t_Glue__> sim_t;                                           \
    typedef uSisSample<t_Glue__> sample_t;                                     \
    typedef uSisCanonicalTrialRunnerMixin<t_Glue__,                            \
                                          uSisParallelBatchGrower<t_Glue__> >  \
        trial_runner_mixin_t;                                                  \
    typedef uSisSeoSloc::GrowthSimLevelMixin_threaded<t_Glue__>                \
        sim_level_growth_mixin_t;                                              \
    typedef uSisSeoSloc::GrowthMixin<t_Glue__> growth_mixin_t;                 \
    typedef uSisHomgNodeRadiusSimLevelMixin<t_Glue__>                          \
        sim_level_node_radius_mixin_t;                                         \
    typedef uSisHomgNodeRadiusMixin<t_Glue__> node_radius_mixin_t;             \
    typedef uSisSphereNuclearSimLevelMixin_threaded<t_Glue__>                  \
        sim_level_nuclear_mixin_t;                                             \
    typedef uSisSphereNuclearMixin<t_Glue__> nuclear_mixin_t;                  \
    typedef uSisHardShellCollisionSimLevelMixin_threaded<t_Glue__>             \
        sim_level_collision_mixin_t;                                           \
    typedef uSisHardShellCollisionMixin<t_Glue__, uBroadPhase_t>               \
        collision_mixin_t;                                                     \
    typedef uSisNullIntrChrSimLevelMixin<t_Glue__> sim_level_intr_chr_mixin_t; \
    typedef uSisNullIntrChrMixin<t_Glue__> intr_chr_mixin_t;                   \
    typedef uSisNullIntrLamSimLevelMixin<t_Glue__> sim_level_intr_lam_mixin_t; \
    typedef uSisNullIntrLamMixin<t_Glue__> intr_lam_mixin_t;                   \
    typedef uSisNullIntrNucbSimLevelMixin<t_Glue__>                            \
        sim_level_intr_nucb_mixin_t;                                           \
    typedef uSisNullIntrNucbMixin<t_Glue__> intr_nucb_mixin_t;                 \
    typedef uSisUnifEnergySimLevelMixin<t_Glue__> sim_level_energy_mixin_t;    \
    typedef uSisUnifEnergyMixin<t_Glue__> energy_mixin_t;                      \
    typedef uSisSeoUnifCubeSeedMixin<t_Glue__> seed_mixin_t

namespace {

/**
 * Inner child simulation for Delphic resampling look-ahead
 */
class uTestSisDelPowRsQcChildQcGlue {
public:
    U_TEST_SIS_DEL_POW_RS_QC_DECLARE_COMMON_TYPEDEFS(
        uTestSisDelPowRsQcChildQcGlue);

    // Simulation level quality control mixin (NULL)
    typedef uSisNullQcSimLevelMixin<uTestSisDelPowRsQcChildQcGlue>
        sim_level_qc_mixin_t;
};

/**
 * Now wire together a outer parent resampling simulation
 */
class uTestSisDelPowRsQcParentGlue {
public:
    U_TEST_SIS_DEL_POW_RS_QC_DECLARE_COMMON_TYPEDEFS(
        uTestSisDelPowRsQcParentGlue);

    // Simulation level quality control mixin (Delphic power resampler)
    typedef uSisResamplingQcSimLevelMixin<
        uTestSisDelPowRsQcParentGlue,
        uSisDynamicEssScheduleMixin<uTestSisDelPowRsQcParentGlue,
                                    uOpt_qc_schedule_dynamic_ess_thresh_slot0>,
        uSisDelphicPowerSamplerRsMixin<uTestSisDelPowRsQcParentGlue,
                                       uSisQcSummaryMeanWeight,
                                       uTestSisDelPowRsQcChildQcGlue::sim_t,
                                       uSisSampleCopier_unsafe,
                                       uOpt_delphic_steps_slot0,
                                       uOpt_delphic_power_alpha_slot0>,
        true>  // enable heartbeat
        sim_level_qc_mixin_t;
};

}  // end of anonymous namespace

/**
 * Entry point for test
 * rs = resampling
 * qc = quality control
 */
int uTestSisDelphicPowerResamplingQcMain(const uCmdOptsMap& cmd_opts) {
    // Expose rs qc simulation
    typedef uTestSisDelPowRsQcParentGlue::sim_t rs_sim_t;

    // Expose null qc simulation as control
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t null_sim_t;

    // Common settings
    const uReal MAX_NODE_DIAMETER = U_TO_REAL(300.0);
    const uReal NUCLEAR_DIAMETER = U_TO_REAL(9000.0);
    const std::vector<uUInt> NUM_NODES(1 /*n_chains*/, 150 /*n_nodes*/);
    const uUInt NUM_UNIT_SPHERE_SAMPLE_POINTS = 100;

    // Create a parent configuration
    uSpConfig_t parent_config = uSmartPtr::make_shared<uConfig>();
    parent_config->output_dir = std::string("null");
    parent_config->max_trials = U_TO_UINT(3);
    parent_config->ensemble_size = U_TO_UINT(20);
    parent_config->set_option(uOpt_max_node_diameter, MAX_NODE_DIAMETER);
    parent_config->set_option(uOpt_nuclear_diameter, NUCLEAR_DIAMETER);
    parent_config->num_nodes = NUM_NODES;
    parent_config->num_unit_sphere_sample_points =
        NUM_UNIT_SPHERE_SAMPLE_POINTS;

    // Configure rs schedule (dynamic schedule)
    // See uSisHomgLagScheduleMixin for a deterministic schedule
    parent_config->set_option(uOpt_qc_schedule_type_slot0, "dynamic_ess");
    parent_config->set_option(uOpt_qc_schedule_dynamic_ess_thresh_slot0, 0.30);

    // Configure Delphic sampler
    const uUInt DELPHIC_SELECT_STEPS = 10;
    const uReal DELPHIC_SELECT_POW_ALPHA = U_TO_REAL(1.0);
    parent_config->set_option(uOpt_rs_sampler_type_slot0, "delphic_power");
    parent_config->set_option(uOpt_delphic_power_alpha_slot0,
                              DELPHIC_SELECT_POW_ALPHA);
    parent_config->set_option(uOpt_delphic_steps_slot0, DELPHIC_SELECT_STEPS);

    // Create child configuration for Delphic sampler
    uSpConfig_t child_config_qc = uSmartPtr::make_shared<uConfig>();
    // Register child configuration for quality control mixin
    parent_config->set_child_qc(child_config_qc,
                                uTRUE /*should_copy_named_vars_to_child*/);
    child_config_qc->ensemble_size = U_TO_UINT(10);

    // Output configuration
    std::cout << "Running Test Sis Delphic Power Resampling Qc." << std::endl;
    parent_config->print();

    // Configure number of worker threads
    // Uncomment below to manually configure thread count:
    // U_SET_NUM_WORKER_THREADS(parent_config, 7);

    // Initialize globals
    uG::init(parent_config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    rs_sim_t rs_sim(parent_config U_MAIN_THREAD_ID_ARG);

    // Do resampling simulation!
    const uBool rs_result = rs_sim.run();

    // Compute resampling effective sample size
    const uReal rs_ess = uSisUtilsQc::calc_ess_cv_from_log(
        rs_sim.get_completed_log_weights_view());

    std::cout << "Rs simulation completed with status: " << rs_result
              << std::endl;
    std::cout << "Rs effective sample size: " << rs_ess << std::endl;

    uSisUtilsQc::report_percent_unique_monomers(rs_sim.get_completed_samples());

    // Run control simulation
    null_sim_t null_sim(parent_config U_MAIN_THREAD_ID_ARG);

    // Do control simulation!
    const uBool null_result = null_sim.run();

    // Compute null effective sample size
    const uReal null_ess = uSisUtilsQc::calc_ess_cv_from_log(
        null_sim.get_completed_log_weights_view());

    std::cout << "Null simulation completed with status: " << null_result
              << std::endl;
    std::cout << "Null effective sample size: " << null_ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
