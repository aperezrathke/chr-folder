//****************************************************************************
// uSisNullIntrChrSeedPolicy.h
//****************************************************************************

#ifndef uSisNullIntrChrSeedPolicy_h
#define uSisNullIntrChrSeedPolicy_h

#include "uBuild.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Chr-chr interactions null seed policy
 */
typedef struct {
    /**
     * Chr-chr interactions should not be updated
     */
    enum { should_update = false };

    /**
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE always!
     */
    template <typename sample_t, typename sim_t>
    inline static uBool is_seed_okay(sample_t& sample,
                                     const uSisUtils::seed_info_t& nfo,
                                     const sim_t& sim U_THREAD_ID_PARAM) {
        return uTRUE;
    }
} uSisNullIntrChrSeedPolicy_t;

#endif  // uSisNullIntrChrSeedPolicy_h
