//****************************************************************************
// uTestSisResidualResamplingQc.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * but with residual resampling as the quality control (Qc) mixin.
 *
 * Usage:
 * -test_sis_residual_resampling_qc
 */

#ifdef uTestSisResidualResamplingQc_h
#   error "Test Sis Residual Resampling Qc included multiple times!"
#endif  // uTestSisResidualResamplingQc_h
#define uTestSisResidualResamplingQc_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Resampling mixin
#include "uSisDynamicEssScheduleMixin.h"
#include "uSisResamplingQcSimLevelMixin.h"
#include "uSisResidualSamplerRsMixin.h"

// Use null QC as a control
#include "uMockUpUtils.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisSerialBatchGrower.h"

#include <iostream>
#include <string>

namespace {
/**
 * Now wire together a resampling simulation
 */
class uTestSisResidualRsQcGlue {
public:
    // Simulation
    typedef uSisSim<uTestSisResidualRsQcGlue> sim_t;
    // Sample
    typedef uSisSample<uTestSisResidualRsQcGlue> sample_t;

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisResidualRsQcGlue,
        uSisSerialBatchGrower<uTestSisResidualRsQcGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisResamplingQcSimLevelMixin<
        uTestSisResidualRsQcGlue,
        uSisDynamicEssScheduleMixin<uTestSisResidualRsQcGlue>,
        uSisResidualSamplerRsMixin<uTestSisResidualRsQcGlue>,
        true  // enable heartbeat
        >
        sim_level_qc_mixin_t;

    // Simulation level node radius mixin
    typedef uSisHomgNodeRadiusSimLevelMixin<uTestSisResidualRsQcGlue>
        sim_level_node_radius_mixin_t;
    // Sample level node radius mixin
    typedef uSisHomgNodeRadiusMixin<uTestSisResidualRsQcGlue>
        node_radius_mixin_t;
    // Simulation level growth mixin
    typedef uSisSeoSloc::GrowthSimLevelMixin_threaded<uTestSisResidualRsQcGlue>
        sim_level_growth_mixin_t;
    // Sample level growth mixin
    typedef uSisSeoSloc::GrowthMixin<uTestSisResidualRsQcGlue> growth_mixin_t;
    // Simulation level nuclear mixin
    typedef uSisSphereNuclearSimLevelMixin_threaded<uTestSisResidualRsQcGlue>
        sim_level_nuclear_mixin_t;
    // Sample level nuclear mixin
    typedef uSisSphereNuclearMixin<uTestSisResidualRsQcGlue> nuclear_mixin_t;
    // Simulation level collision mixin
    typedef uSisHardShellCollisionSimLevelMixin_threaded<
        uTestSisResidualRsQcGlue>
        sim_level_collision_mixin_t;
    // Sample level collision mixin
    typedef uSisHardShellCollisionMixin<uTestSisResidualRsQcGlue, uBroadPhase_t>
        collision_mixin_t;
    // Simulation level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrSimLevelMixin<uTestSisResidualRsQcGlue>
        sim_level_intr_chr_mixin_t;
    // Sample level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrMixin<uTestSisResidualRsQcGlue> intr_chr_mixin_t;
    // Simulation level lamina interaction mixin
    typedef uSisNullIntrLamSimLevelMixin<uTestSisResidualRsQcGlue>
        sim_level_intr_lam_mixin_t;
    // Sample level lamina interaction mixin
    typedef uSisNullIntrLamMixin<uTestSisResidualRsQcGlue> intr_lam_mixin_t;
    // Simulation level nuclear body interaction mixin
    typedef uSisNullIntrNucbSimLevelMixin<uTestSisResidualRsQcGlue>
        sim_level_intr_nucb_mixin_t;
    // Sample level nuclear body interaction mixin
    typedef uSisNullIntrNucbMixin<uTestSisResidualRsQcGlue> intr_nucb_mixin_t;
    // Simulation level energy mixin
    typedef uSisUnifEnergySimLevelMixin<uTestSisResidualRsQcGlue>
        sim_level_energy_mixin_t;
    // Sample level energy mixin
    typedef uSisUnifEnergyMixin<uTestSisResidualRsQcGlue> energy_mixin_t;
    // Seed mixin
    typedef uSisSeoUnifCubeSeedMixin<uTestSisResidualRsQcGlue> seed_mixin_t;
};
}  // end of anonymous namespace

/**
 * Entry point for test
 * rs = resampling
 * qc = quality control
 */
int uTestSisResidualResamplingQcMain(const uCmdOptsMap& cmd_opts) {
    // Expose rs qc simulation
    typedef uTestSisResidualRsQcGlue::sim_t rs_sim_t;

    // Expose null qc simulation as control
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t null_sim_t;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 3;
    config->ensemble_size = 1000;
    config->set_option(uOpt_max_node_diameter, U_TO_REAL(300));
    config->set_option(uOpt_nuclear_diameter, U_TO_REAL(9000));
    config->num_nodes = std::vector<uUInt>(1 /* n_chains*/, 500 /* n_nodes*/);
    config->num_unit_sphere_sample_points = 50;

    // Configure rc schedule (dynamic schedule)
    // See uSisHomgLagScheduleMixin for a deterministic schedule
    config->set_option(uOpt_qc_schedule_type_slot0, "dynamic_ess");
    config->set_option(uOpt_qc_schedule_dynamic_ess_thresh_slot0, "0.30");

    // Configure sampler
    config->set_option(uOpt_rs_sampler_type_slot0, "residual");

    // Output configuration
    std::cout << "Running Test Sis Residual Resampling Qc." << std::endl;
    config->print();

    // Configure number of worker threads
    U_SET_NUM_WORKER_THREADS(config, 1);

    // Initialize globals
    uG::init(config);

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    rs_sim_t rs_sim(config U_MAIN_THREAD_ID_ARG);

    // Do resampling simulation!
    const uBool rs_result = rs_sim.run();

    // Compute resampling effective sample size
    const uReal rs_ess = uSisUtilsQc::calc_ess_cv_from_log(
        rs_sim.get_completed_log_weights_view());

    std::cout << "Rs simulation completed with status: " << rs_result
              << std::endl;
    std::cout << "Rs effective sample size: " << rs_ess << std::endl;

    uSisUtilsQc::report_percent_unique_monomers(rs_sim.get_completed_samples());

    // Run control simulation
    std::cout << "Running null simulation." << std::endl;

    null_sim_t null_sim(config U_MAIN_THREAD_ID_ARG);

    // Do control simulation!
    const uBool null_result = null_sim.run();

    // Compute null effective sample size
    const uReal null_ess = uSisUtilsQc::calc_ess_cv_from_log(
        null_sim.get_completed_log_weights_view());

    std::cout << "Null simulation completed with status: " << null_result
              << std::endl;
    std::cout << "Null effective sample size: " << null_ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
