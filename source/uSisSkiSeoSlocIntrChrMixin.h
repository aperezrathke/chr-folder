//****************************************************************************
// uSisSkiSeoSlocIntrChrMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Ski: Single knock-in
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Sloc: Single locus (only a single locus is modeled)
 *
 * Defines a chrome-to-chrome interaction mixin where only a single knock-in
 * interaction is enforced.
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisSkiSeoSlocIntrChrMixin_h
#define uSisSkiSeoSlocIntrChrMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uExitCodes.h"
#include "uIntrLibConfig.h"
#include "uIntrLibDefs.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uSisIntrChrCommonInit.h"
#include "uSisIntrChrConfig.h"
#include "uSisIntrChrFilterKinIntra.h"
#include "uSisNullIntrChrSeedPolicy.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisSkiSeoSlocIntrChrSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisSkiSeoSlocIntrChrSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();

        // Load fragment spans
        uUIMatrix frags;
        uSisIntrChrConfig::load_frag(
            frags, config, uTRUE /*no_fail*/, sim.get_max_total_num_nodes());
        uAssert(frags.n_cols >= 1);
        uAssert(frags.n_rows == uIntrLibPairIxNum);

        // Load knock-in interactions
        uUIMatrix kin;
        uSisIntrChrConfig::load_knock_in(kin, config, uTRUE /*no_fail*/);
        uAssert(kin.n_cols >= 1);
        uAssert(kin.n_rows == uIntrLibPairIxNum);

        // Select single perturbation (column index of kin matrix)
        uUInt ski_ix = 0;
        if (!config->read_into<uUInt>(ski_ix, uOpt_ski_ix) ||
            (ski_ix >= kin.n_cols)) {
            uLogf("Error: unable to load or malformed SKI index. Exiting.\n");
            exit(uExitCode_error);
        }

        // Extract single interaction node spans
        uUInt frag_a = kin.at(uIntrLibPairIxMin, ski_ix);
        uUInt frag_b = kin.at(uIntrLibPairIxMax, ski_ix);
        uSisIntrChrCommonInit::format_intr_exit_on_fail(
            frag_a, frag_b, frags, sim);

        m_intr_chr_frag_a_nid_lo = frags.at(uIntrLibPairIxMin, frag_a);
        m_intr_chr_frag_a_nid_hi = frags.at(uIntrLibPairIxMax, frag_a);
        m_intr_chr_frag_b_nid_lo = frags.at(uIntrLibPairIxMin, frag_b);
        m_intr_chr_frag_b_nid_hi = frags.at(uIntrLibPairIxMax, frag_b);

        // Initialize proximity threshold - must be positive real
        m_intr_chr_kin_dist = uSisIntrChrConfig::get_knock_in_dist(config);
        uSisIntrChrCommonInit::init_dists(
            m_intr_chr_kin_dist, m_intr_chr_kin_dist2, sim);

        // Report initialized interaction state
        uLogf("Initialized chrome-to-chrome interaction constraint:\n");
        uLogf("\t-intr_chr_frag_a_nid_lo: %d\n", (int)m_intr_chr_frag_a_nid_lo);
        uLogf("\t-intr_chr_frag_a_nid_hi: %d\n", (int)m_intr_chr_frag_a_nid_hi);
        uLogf("\t-intr_chr_frag_b_nid_lo: %d\n", (int)m_intr_chr_frag_b_nid_lo);
        uLogf("\t-intr_chr_frag_b_nid_hi: %d\n", (int)m_intr_chr_frag_b_nid_hi);
        uLogf("\t-intr_chr_kin_dist (may be padded): %f\n",
              (double)m_intr_chr_kin_dist);
    }

    /**
     * Resets to default state
     */
    void clear() {
        this->m_intr_chr_frag_a_nid_lo = 0;
        this->m_intr_chr_frag_a_nid_hi = 0;
        this->m_intr_chr_frag_b_nid_lo = 0;
        this->m_intr_chr_frag_b_nid_hi = 0;
        this->m_intr_chr_kin_dist = U_TO_REAL(0.0);
        this->m_intr_chr_kin_dist2 = U_TO_REAL(0.0);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * @return lower|upper (min|max) index of 1st interacting fragment
     */
    inline uUInt get_intr_chr_frag_a_nid_lo() const {
        return m_intr_chr_frag_a_nid_lo;
    }
    inline uUInt get_intr_chr_frag_a_nid_hi() const {
        return m_intr_chr_frag_a_nid_hi;
    }

    /**
     * @return lower|upper (min|max) index of 2nd interacting fragment
     */
    inline uUInt get_intr_chr_frag_b_nid_lo() const {
        return m_intr_chr_frag_b_nid_lo;
    }
    inline uUInt get_intr_chr_frag_b_nid_hi() const {
        return m_intr_chr_frag_b_nid_hi;
    }

    /**
     * @return [Squared] knock-in Euclidean distance threshold
     */
    inline uReal get_intr_chr_kin_dist(const uUInt i_unused) const {
        return m_intr_chr_kin_dist;
    }
    inline uReal get_intr_chr_kin_dist2(const uUInt i_unused) const {
        return m_intr_chr_kin_dist2;
    }

    // Export interface

    /**
     * Copies chr-chr interaction fragments to 'out'
     */
    void get_intr_chr_frags(uUIMatrix& out) const {
        out.set_size(uIntrLibPairIxNum /*rows*/, 2 /*cols*/);
        out.at(uIntrLibPairIxMin, 0) = m_intr_chr_frag_a_nid_lo;
        out.at(uIntrLibPairIxMax, 0) = m_intr_chr_frag_a_nid_hi;
        out.at(uIntrLibPairIxMin, 1) = m_intr_chr_frag_b_nid_lo;
        out.at(uIntrLibPairIxMax, 1) = m_intr_chr_frag_b_nid_hi;
    }

    /**
     * Copies knock-in chr-chr interactions to 'out'
     */
    void get_intr_chr_kin(uUIMatrix& out) const {
        out.set_size(uIntrLibPairIxNum /*rows*/, 1 /*cols*/);
        out.at(uIntrLibPairIxMin, 0) = 0;
        out.at(uIntrLibPairIxMax, 0) = 1;
    }

    /**
     * Copies knock-out chr-chr interactions to 'out'
     */
    void get_intr_chr_ko(uUIMatrix& out) const { out.clear(); }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Node span for first fragment
     */
    uUInt m_intr_chr_frag_a_nid_lo;
    uUInt m_intr_chr_frag_a_nid_hi;

    /**
     * Node span for second fragment
     */
    uUInt m_intr_chr_frag_b_nid_lo;
    uUInt m_intr_chr_frag_b_nid_hi;

    /**
     * At least one node in first fragment can be no farther than this
     * distance from a node in the second fragment.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uReal m_intr_chr_kin_dist;
    /**
     * Squared distance proximity threshold.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE DIAMETER AT INITIALIZATION
     * ONLY IF NODES ARE HOMOGENEOUS.
     */
    uReal m_intr_chr_kin_dist2;
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisSkiSeoSlocIntrChrMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Filter utility
     */
    typedef uSisIntrChrFilterKinIntra<t_SisGlue> intr_chr_filt_kin_intra_t;

    /**
     * Default constructor
     */
    uSisSkiSeoSlocIntrChrMixin() : mb_intr_chr_satisf(uFALSE) {}

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
    }

    /**
     * Resets to default state
     */
    void clear() { mb_intr_chr_satisf = uFALSE; }

    /**
     * ASSUMES sequential growth from first to last node. Determines which
     * positions may satisfy interaction constraints.
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *  positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    inline void intr_chr_filter(uBoolVecCol& out_legal_candidates_primed,
                                const uMatrix& candidate_centers,
                                const uReal candidate_radius,
                                const uUInt parent_node_id,
                                const sample_t& sample,
                                const sim_t& sim U_THREAD_ID_PARAM) {
        // Early out if interaction already satisfied
        if (mb_intr_chr_satisf) {
            return;
        }

        // Initialize core filter arguments
        uSisIntrChrFilterCore_t c /*= { 0 }*/;
        c.frag_a_nid_lo = sim.get_intr_chr_frag_a_nid_lo();
        c.frag_a_nid_hi = sim.get_intr_chr_frag_a_nid_hi();
        c.frag_b_nid_lo = sim.get_intr_chr_frag_b_nid_lo();
        c.frag_b_nid_hi = sim.get_intr_chr_frag_b_nid_hi();
        // @WARNING - assumes candidate node id is +1 from parent node id
        c.node_nid = parent_node_id + 1;
        c.node_lid = sim.get_growth_lid(c.node_nid);
        c.node_radius = candidate_radius;

        // Defer to filter utility to mark constraint violations
        intr_chr_filt_kin_intra_t::mark_viol(*this,
                                             (void*)NULL,
                                             out_legal_candidates_primed,
                                             c,
                                             candidate_centers,
                                             sample,
                                             sim);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_kin_on_fail(void* const p_policy_args,
                                      const uUInt candidate_id,
                                      const uSisIntrChrFilterCore_t& c,
                                      const sample_t& sample,
                                      const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_ko_on_fail(void* const p_policy_args,
                                     const uUInt candidate_id,
                                     const uSisIntrChrFilterCore_t& c,
                                     const sample_t& sample,
                                     const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - a unique chromatin node identifier
     * @param B - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param candidate_id - identifies candidate that was selected
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    inline void intr_chr_update(const uUInt node_id,
                                const uReal* const B,
                                const uReal radius,
                                const uUInt candidate_id,
                                const sample_t& sample,
                                const sim_t& sim U_THREAD_ID_PARAM) {
        // Early out if interaction already satisfied
        if (mb_intr_chr_satisf) {
            return;
        }

        // Initialize core filter arguments
        uSisIntrChrFilterCore_t c /*= { 0 }*/;
        c.intr_id = U_TO_UINT(0);
        c.frag_a_nid_lo = sim.get_intr_chr_frag_a_nid_lo();
        c.frag_a_nid_hi = sim.get_intr_chr_frag_a_nid_hi();
        c.frag_b_nid_lo = sim.get_intr_chr_frag_b_nid_lo();
        c.frag_b_nid_hi = sim.get_intr_chr_frag_b_nid_hi();
        c.node_nid = node_id;
        c.node_lid = sim.get_growth_lid(c.node_nid);
        c.node_radius = radius;

        mb_intr_chr_satisf =
            intr_chr_filt_kin_intra_t::is_satisf(c, B, sample, sim);
    }

    // Accessors

    /**
     * @return Chr-chr interaction seed policy for first seeded node
     */
    uSisNullIntrChrSeedPolicy_t get_intr_chr_seed_first_policy() {
        return uSisNullIntrChrSeedPolicy_t();
    }

    /**
     * @return Chr-chr interaction seed policy for nodes seeded after first
     */
    uSisNullIntrChrSeedPolicy_t get_intr_chr_seed_next_policy() {
        return uSisNullIntrChrSeedPolicy_t();
    }

private:
    /**
     * Testing is avoided if constraint is already satisfied.
     */
    uBool mb_intr_chr_satisf;
};

#endif  // uSisSkiSeoSlocIntrChrMixin_h
