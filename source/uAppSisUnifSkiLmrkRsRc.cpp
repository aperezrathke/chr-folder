//****************************************************************************
// uAppSisSkiLmrkRsRc.h
//****************************************************************************

/**
 * App generates single knock-in interaction constrained samples using nested
 * landmark trial runners with resampling and rejection control.
 *
 * Usage:
 * -app_sis_ski_lmrk_rs_rc
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uTypes.h"

// Child simulation rejection control mixin
#include "uSisDynamicEssScheduleMixin.h"
#include "uSisFixedQuantileScaleRcMixin.h"
#include "uSisHetrLagScheduleMixin.h"
#include "uSisInterpWeightsScaleRcMixin.h"
#include "uSisParallelRegrowthRcMixin.h"
#include "uSisRejectionControlQcSimLevelMixin.h"
#include "uSisSerialRegrowthRcMixin.h"

// Parent simulation resampling quality control mixin
#include "uSisPowerSamplerRsMixin.h"
#include "uSisResamplingQcSimLevelMixin.h"
#include "uSisSkiMixedLagScheduleMixin.h"

// Grandparent simulation null quality control mixin
#include "uSisNullQcSimLevelMixin.h"

// Landmark trial runner mixin
#include "uSisLandmarkTrialRunnerMixin.h"
#include "uSisLmrkUtils.h"
#include "uSisSelectPower.h"

// Interaction mixin
#include "uSisSkiSeoSlocIntrChrMixin.h"

// Energy mixin
#include "uSisUnifEnergyMixin.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"

#include "uSisParallelBatchGrower.h"
#include "uSisSerialBatchGrower.h"

#include "uAppUtils.h"
#include "uSisUtilsExport.h"

#include <iostream>
#include <string>

//****************************************************************************
// Defines
//****************************************************************************

/**
 * The specification for common mixins shared by the SKI (single knock-in)
 * model for single locus, homogeneous chains (all nodes have same diameter).
 * The SKI model is used for generating uniformly sampled chains with a single
 * interaction constraint.
 */
#define U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_SKI_MODEL_ATTRIBS(t_SisGlue)       \
    U_SIS_DECLARE_LMRK_SLOC_HOMG_ATTRIBS(t_SisGlue);                         \
    /* Simulation level chrome-to-chrome interaction mixin */                \
    typedef uSisSkiSeoSlocIntrChrSimLevelMixin<t_SisGlue>                    \
        sim_level_intr_chr_mixin_t;                                          \
    /* Sample level chrome-to-chrome interaction mixin */                    \
    typedef uSisSkiSeoSlocIntrChrMixin<t_SisGlue> intr_chr_mixin_t;          \
    /* Simulation level lamina interaction mixin */                          \
    typedef uSisNullIntrLamSimLevelMixin<t_SisGlue>                          \
        sim_level_intr_lam_mixin_t;                                          \
    /* Sample level lamina interaction mixin */                              \
    typedef uSisNullIntrLamMixin<t_SisGlue> intr_lam_mixin_t;                \
    /* Simulation level nuclear body interaction mixin */                    \
    typedef uSisNullIntrNucbSimLevelMixin<t_SisGlue>                         \
        sim_level_intr_nucb_mixin_t;                                         \
    /* Sample level nuclear body interaction mixin */                        \
    typedef uSisNullIntrNucbMixin<t_SisGlue> intr_nucb_mixin_t;              \
    /* Simulation level energy mixin */                                      \
    typedef uSisUnifEnergySimLevelMixin<t_SisGlue> sim_level_energy_mixin_t; \
    /* Sample level energy mixin */                                          \
    typedef uSisUnifEnergyMixin<t_SisGlue> energy_mixin_t;                   \
    /* Seed mixin */                                                         \
    typedef uSisSeoUnifCubeSeedMixin<t_SisGlue> seed_mixin_t

//****************************************************************************
// uAppSisSkiLmrkRsRc
//****************************************************************************

namespace uAppSisUnifSkiLmrkRsRc {
/**
 * Wire together rejection control child simulation
 */
class ChildSimGlue {
public:
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_SKI_MODEL_ATTRIBS(ChildSimGlue);

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<ChildSimGlue,
                                          uSisSerialBatchGrower<ChildSimGlue> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisRejectionControlQcSimLevelMixin<
        ChildSimGlue,
        // Set the schedule
        uSisHetrLagScheduleMixin<ChildSimGlue,
                                 uOpt_qc_schedule_lag_file_slot3,
                                 uOpt_qc_schedule_lag_slot3>,
        // Set the scale
        uSisInterpWeightsScaleRcMixin<ChildSimGlue>,
        uSisSerialRegrowthRcMixin<ChildSimGlue>,
        // Disable heartbeat logging for rc
        false,
        // Enable weight normalization
        true>
        sim_level_qc_mixin_t;
};

/**
 * Wire together landmark trial runner with resampling parent simulation
 */
class ParentSimGlue {
public:
    // WARNING: We want parent and child sample types to match to
    // allow *unsafe* copying!
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_SKI_MODEL_ATTRIBS(ParentSimGlue);

    // Trial runner mixin
    typedef uSisLandmarkTrialRunnerMixin<
        ParentSimGlue,
        uSisSelectPower<ParentSimGlue, uOpt_qc_sampler_power_alpha_slot2>,
        uSisSkiMixedLagScheduleMixin<ParentSimGlue,
                                     uOpt_qc_schedule_lag_slot2,
                                     uOpt_qc_schedule_lag_slot4>,
        uSisSubSimAccess_serial<ChildSimGlue::sim_t>,
        uSisSampleCopier_unsafe,
        uSisSerialBatchGrower<ParentSimGlue>,
        // Disable heartbeat logging for trial runner
        false>
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisResamplingQcSimLevelMixin<
        ParentSimGlue,
        uSisHetrLagScheduleMixin<ParentSimGlue,
                                 uOpt_qc_schedule_lag_file_slot1,
                                 uOpt_qc_schedule_lag_slot1>,
        uSisPowerSamplerRsMixin<ParentSimGlue,
                                uOpt_qc_sampler_power_alpha_slot1>,
        false  // Disable heartbeat
        >
        sim_level_qc_mixin_t;
};

/**
 * Wire together landmark trial runner grandparent simulation
 */
class GrandparentSimGlue {
public:
    // WARNING: We want parent and child sample types to match to
    // allow *unsafe* copying!
    U_SIS_DECLARE_LMRK_UNIF_SLOC_HOMG_SKI_MODEL_ATTRIBS(GrandparentSimGlue);

    // Trial runner mixin
    typedef uSisLandmarkTrialRunnerMixin<
        GrandparentSimGlue,
        uSisSelectPower<GrandparentSimGlue, uOpt_qc_sampler_power_alpha_slot0>,
        uSisHetrLagScheduleMixin<GrandparentSimGlue,
                                 uOpt_qc_schedule_lag_file_slot0,
                                 uOpt_qc_schedule_lag_slot0>,
        uSisSubSimAccess_threaded<ParentSimGlue::sim_t>,
        uSisSampleCopier_unsafe,
        uSisParallelBatchGrower<GrandparentSimGlue>,
        // Enable heartbeat logging for trial runner
        true>
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisNullQcSimLevelMixin<GrandparentSimGlue> sim_level_qc_mixin_t;
};

}  // namespace uAppSisUnifSkiLmrkRsRc

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point for app
 */
int uAppSisUnifSkiLmrkRsRcMain(const uCmdOptsMap& cmd_opts) {
    // Macro for parent simulation type
    typedef uAppSisUnifSkiLmrkRsRc::GrandparentSimGlue::sim_t sim_t;

    const std::string announce(
        "Running SIS UNIF SLOC HOMG SKI with nested landmark: rs | rc.");

    // Defer to default
    return uAppUtils::default_main<sim_t>(cmd_opts, announce);
}
