//****************************************************************************
// uSisHardShellCollisionSimLevelMixin.h
//****************************************************************************

/**
 * A collision mixin manages the details for checking candidate nodes to see if
 * they satisfy self-avoidance (do not collide with any existing nodes)
 *
 * An accessory structure is the simulation level collision mixin which
 * define global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 * HardShell - Uses broad phase filtering and then applies a narrow phase hard
 *  shell (i.e. no overlap) collision model to enforce 'self-avoidance'
 */

#ifndef uSisHardShellCollisionSimLevelMixin_h
#define uSisHardShellCollisionSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisUtilsCollision.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uSseTable.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypeTraits.h"
#include "uTypes.h"
#include "uWrappedBroadPhase.h"

#include <vector>

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisHardShellCollisionSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisHardShellCollisionSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisHardShellCollisionSimLevelMixin_h
