//****************************************************************************
// uSisIntrLamCommonFilt.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utilities for lamina interaction filtering
 */

#ifndef uSisIntrLamCommonFilt_h
#define uSisIntrLamCommonFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uSisUtilsGrowth.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Structs
//****************************************************************************

/**
 * Core arguments used by lamina interaction filter utilities
 */
typedef struct {
    /**
     * Interaction index
     */
    uUInt intr_id;
    /**
     * Lower (min) node identifier for interacting fragment; note lo <= hi
     */
    uUInt frag_nid_lo;
    /**
     * Upper (max) node identifier for interacting fragment
     */
    uUInt frag_nid_hi;
    /**
     * Node identifier for either a candidate node (if marking violators) or a
     *  recently placed node (if checking constraint satisfaction)
     */
    uUInt node_nid;
    /**
     * The locus identifier corresponding to 'node_nid'
     */
    uUInt node_lid;
    /**
     * The node radius corresponding to 'node_nid'; note all candidate nodes
     *  with the same node identifier are assumed to have same radius
     */
    uReal node_radius;
} uSisIntrLamFilterCore_t;

//****************************************************************************
// uSisIntrLamCommonFilt
//****************************************************************************

/**
 * Effectively a namespace for shared interaction filter utilities
 */
template <typename t_SisGlue>
class uSisIntrLamCommonFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * @param nid - Node identifier
     * @param frag_nid_lo - Minimum fragment node identifier
     * @param frag_nid_hi - Maximum fragment node identifier
     * @return uTRUE if node at nid is within fragment, uFALSE o/w
     */
    inline static uBool is_in_frag(const uUInt nid,
                                   const uUInt frag_nid_lo,
                                   const uUInt frag_nid_hi) {
        // Under SEO growth, we should not call this method with fragment that
        // has already been grown!
        uAssert(nid <= frag_nid_hi);
        uAssert(frag_nid_lo <= frag_nid_hi);
        // Early out if we're not in a relevant region
        return ((nid >= frag_nid_lo) && (nid <= frag_nid_hi));
    }

    /**
     * Asserts if interaction is not formatted properly
     * @WARNING - ASSUMES SEO GROWTH!
     * @param c - Core filter arguments
     * @param sim - Outer simulation containing global sample data
     */
    static uBool check_format(const uSisIntrLamFilterCore_t& c,
                              const sim_t& sim) {
        // Assume SEO growth, constraint testing should be avoided after we've
        // grown beyond interacting fragment
        uAssert(c.node_nid <= c.frag_nid_hi);
        // Assume valid bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBoundsInc(c.frag_nid_lo, U_TO_UINT(0), c.frag_nid_hi);
        uAssertBounds(c.frag_nid_hi, c.frag_nid_lo, max_nodes);
        uAssertBounds(c.node_nid, U_TO_UINT(0), max_nodes);
        // Assume single locus
        const uUInt lid = sim.get_growth_lid(c.frag_nid_lo);
        uAssertBounds(lid, U_TO_UINT(0), sim.get_num_loci());
        uAssert(lid == sim.get_growth_lid(c.frag_nid_lo));
        uAssert(lid == sim.get_growth_lid(c.frag_nid_hi));
        uAssert(lid == sim.get_growth_lid(c.node_nid));
        uAssert(lid == c.node_lid);
        // Assume positive radius
        uAssert(c.node_radius > U_TO_REAL(0.0));
        return uTRUE;
    }

    /**
     * Verifies node center 'B' self dot product has been cached
     * @return TRUE if:
     *      dot_buffer[i] == dot{B, B}
     */
    static uBool check_dot(const uMatSz_t i,
                           const uVecCol& dot_buffer,
                           const uReal* const B) {
        const uReal x = B[uDim_X];
        const uReal y = B[uDim_Y];
        const uReal z = B[uDim_Z];
        const uReal dot = (x * x) + (y * y) + (z * z);
        uAssertBounds(i, U_TO_MAT_SZ_T(0), dot_buffer.n_elem);
        return U_REAL_CMP_EQ(dot, dot_buffer.at(i));
    }

    /**
     * Verifies candidate center self dot product has been cached
     * @return TRUE if:
     *      dot_buffer[i] == dot{candidate_center(i), candidate_center(i)}
     */
    static uBool check_dot(const uMatSz_t i,
                           const uVecCol& dot_buffer,
                           const uMatrix& candidate_centers) {
        uAssert(candidate_centers.n_rows == dot_buffer.n_elem);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssertBounds(i, U_TO_MAT_SZ_T(0), candidate_centers.n_rows);
        const uReal B[uDim_num] = {candidate_centers.at(i, uDim_X),
                                   candidate_centers.at(i, uDim_Y),
                                   candidate_centers.at(i, uDim_Z)};
        return check_dot(i, dot_buffer, &(B[0]));
    }

    /**
     * Verifies node center 'B' self element-wise (schur) product has been
     *  cached
     * @return TRUE if:
     *       schur_buffer[i, x|y|z] == B[x|y|z] * B[x|y|z]
     */
    static uBool check_schur(const uMatSz_t i,
                             const uMatrix& schur_buffer,
                             const uReal* const B) {
        const uReal x = B[uDim_X];
        const uReal y = B[uDim_Y];
        const uReal z = B[uDim_Z];
        uAssertBounds(i, U_TO_MAT_SZ_T(0), schur_buffer.n_rows);
        uAssertPosEq(schur_buffer.n_cols, uDim_num);
        return (U_REAL_CMP_EQ((x * x), schur_buffer.at(i, uDim_X))) &&
               (U_REAL_CMP_EQ((y * y), schur_buffer.at(i, uDim_Y))) &&
               (U_REAL_CMP_EQ((z * z), schur_buffer.at(i, uDim_Z)));
    }

    /**
     * Verifies candidate center self element-wise (schur) product has been
     *  cached
     * @return TRUE if:
     *      schur_buffer[i, x|y|z] == candidate_center[i, x|y|z] *
     *                                  candidate_center[i, x|y|z]
     */
    static uBool check_schur(const uMatSz_t i,
                             const uMatrix& schur_buffer,
                             const uMatrix& candidate_centers) {
        uAssert(candidate_centers.n_rows == schur_buffer.n_rows);
        uAssert(candidate_centers.n_cols == schur_buffer.n_cols);
        uAssert(candidate_centers.n_cols == uDim_num);
        uAssertBounds(i, U_TO_MAT_SZ_T(0), candidate_centers.n_rows);
        const uReal B[uDim_num] = {candidate_centers.at(i, uDim_X),
                                   candidate_centers.at(i, uDim_Y),
                                   candidate_centers.at(i, uDim_Z)};
        return check_schur(i, schur_buffer, &(B[0]));
    }

    /**
     * @param x2, y2, z2 - Squared bead center position to test
     * @param d_shrink - Amount to shrink principal radii of ellipsoid volume,
     *  MAY BE NEGATIVE in which case radii are inflated!
     * @param sim - Interface to query ellipsoid principal radii
     * @return uTRUE point is on or outside shrunken ellipsoid surface
     */
    inline static uBool is_on_or_outside_shrunken_ellip_vol(
        const uReal x2,
        const uReal y2,
        const uReal z2,
        const uReal d_shrink,
        const sim_t& sim) {
        uAssert(x2 >= U_TO_REAL(0.0));
        uAssert(y2 >= U_TO_REAL(0.0));
        uAssert(z2 >= U_TO_REAL(0.0));
        // "Shrink" principal radii by interaction distance, early out if
        // degenerate radius encountered
        const uReal rx = sim.get_nuclear_radius(uDim_X) - d_shrink;
        if (rx <= U_TO_REAL(0.0)) {
            return uTRUE;
        }
        const uReal ry = sim.get_nuclear_radius(uDim_Y) - d_shrink;
        if (ry <= U_TO_REAL(0.0)) {
            return uTRUE;
        }
        const uReal rz = sim.get_nuclear_radius(uDim_Z) - d_shrink;
        if (rz <= U_TO_REAL(0.0)) {
            return uTRUE;
        }
        // Test if constraint can possibly be satisfied
        const uReal test =
            (x2 / (rx * rx)) + (y2 / (ry * ry)) + (z2 / (rz * rz));
        // Check if point is on or outside of "shrunken" volume
        return test >= U_TO_REAL(1.0);
    }

private:
    // Disallow any form of instantiation
    uSisIntrLamCommonFilt(const uSisIntrLamCommonFilt&);
    uSisIntrLamCommonFilt& operator=(const uSisIntrLamCommonFilt&);
};

#endif  // uSisIntrLamCommonFilt_h
