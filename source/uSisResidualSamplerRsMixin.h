//****************************************************************************
// uSisResidualSamplerRsMixin.h
//****************************************************************************

/**
 * SamplerRsMixin = A mixin that defines how samples are chosen when a resample
 * checkpoint is encountered
 */

#ifndef uSisResidualSamplerRsMixin_h
#define uSisResidualSamplerRsMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uTypes.h"

/**
 * Implementation of residual resampling which claims to have better variance
 * reduction potential compared to standard multinomial (power) sampling.
 *
 * See pg. 72 of Liu, Monte Carlo Strategies in Scientific Computing
 *
 * For derivation of variance bounds using Jensen's inequality, see:
 *
 * Douc, Randal, and Olivier Capp�. "Comparison of resampling schemes for
 * particle filtering." Image and Signal Processing and Analysis, 2005. ISPA
 * 2005. Proceedings of the 4th International Symposium on. IEEE, 2005.
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisResidualSamplerRsMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin ensures the parameter
     * norm exponential weights are valid
     */
    enum { needs_norm_exp_weights = true };

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim, uSpConstConfig_t config) {}

    /**
     * Performs residual resampling and modifies status and log weights
     * @param out_status - 1 if sample is still able to grow, 0 otherwise
     * @param out_log_weights - The current log weight of each sample, will
     *  be replaced by modified weight of chosen sample
     * @param out_samples - The current set of samples to resample from. The
     *  final set of selected samples will overwrite this vector.
     * @param norm_exp_weights - The vector of exp transformed weights
     *  normalized by the max weight - should be in (0,1]. Weights will no
     *  longer be representative of sample after this function finishes.
     * @param max_log_weight - The unnormalized maximal log weight. Weight
     *  will no longer be representative of sample after this function
     *  finishes.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - The parent quality control mixin
     * @param sim - The parent simulation
     */
    void qc_resample(uWrappedBitset& out_status,
                     uVecCol& out_log_weights,
                     std::vector<sample_t>& out_samples,
                     const uVecCol& norm_exp_weights,
                     const uReal max_log_weight,
                     const uUInt num_completed_grow_steps_per_sample,
                     const sim_level_qc_mixin_t qc,
                     const sim_t& sim U_THREAD_ID_PARAM) {
        uAssertPosEq(out_status.size(), out_log_weights.size());
        uAssert(out_status.size() == out_samples.size());
        uAssert(out_status.size() == norm_exp_weights.size());
        uAssert(!uSisUtilsQc::are_all_samples_dead(out_samples));

        // Compute resample probability
        uVecCol probs;
        uSisUtilsQc::calc_power_smoothed_probabilities(
            probs, norm_exp_weights, U_TO_REAL(1.0));

        // Create the pool of samples to select from
        uBitset status_pool;
        out_status.to_hard_bitset(status_pool);
        uAssert(status_pool.size() == out_status.size());
        const std::vector<sample_t> SAMPLES_POOL(out_samples);

        // Set output weight = sum(w). This is proportional to mean(w) which
        // is what the weight update is supposed to be according Liu et al.
        // See basic power sampler with alpha=1, then new weight:
        //      w_i' = w_i / p_select = w_i / (w_i / sum(w)) = sum(w)
        // When using estimator: f_hat = sum(w_i * f / sum(w)), then this is
        // equivalent to setting w_i = mean(w) as 1/n terms cancel in the
        // ratio. According to Liu, the weight assignment for residual
        // resampling is the same as the power (multinomial) sampling method
        // with alpha = 1 (see also Randal et al).
        const uReal LOG_SUM_W =
            log(uMatrixUtils::sum(norm_exp_weights)) + max_log_weight;

        /////////////////////////////////////////
        // Residual resampling with replacement

        // First perform deterministic sampling:
        //  N_i = floor(N * p_i) where N_i is number of samples retained for
        //      sample i, N is total number of samples, and p_i is selection
        //      probability of sample i

        // Total number of samples in integral and floating-point formats
        const uUInt N_SAMPLES = U_TO_UINT(out_samples.size());
        const uReal N_SAMPLES_F = U_TO_REAL(N_SAMPLES);

        // Stochastic sampling probabilities, will be used to store temporary
        // floating point values such as N * p_i and cannot not be normalized
        // until after deterministic sampling is complete
        uVecCol probs_res(N_SAMPLES, uMatrixUtils::fill::zeros);
        uAssertPosEq(probs_res.size(), probs.size());
        // Stores current deterministic sample count: floor(N * p_i)
        uUInt n_i = U_TO_UINT(0);
        // Current output index
        uUInt ix = U_TO_UINT(0);
        // Iterators
        uUInt i, j;
        for (i = U_TO_UINT(0); i < N_SAMPLES; ++i) {
            // Skip sample if it's dead
            if (SAMPLES_POOL[i].is_dead()) {
                uAssert(!out_status.test(i));
                probs_res.at(i) = U_TO_REAL(0.0);
                continue;
            }

            // Store N * p_i temporarily in probs_res[i]
            probs_res.at(i) = N_SAMPLES_F * probs.at(i);
            // Ensure numerical stability by clamping result to be >= 0
            probs_res.at(i) = std::max(U_TO_REAL(0.0), probs_res.at(i));
            uAssert(probs_res.at(i) >= U_TO_REAL(0.0));

            // Compute deterministic sample count N_i = floor(N * p_i)
            n_i = U_TO_UINT(probs_res.at(i));

            // Extract deterministic samples
            const bool status_i = status_pool.test(i);
            uAssert(status_i);
            for (j = U_TO_UINT(0); j < n_i; ++j) {
                // Bounds check
                uAssertBounds(ix, U_TO_UINT(0), U_TO_UINT(out_samples.size()));
                // Replace sample
                out_samples[ix] = SAMPLES_POOL[i];
                // Replace sample status
                out_status.set(ix, status_i);
                // Update output index
                ++ix;
            }

            // Compute unnormalized residual probability:
            //  p_res = N * p_i - floor(N * p_i)
            probs_res.at(i) -= U_TO_REAL(n_i);
            // Set output log weight
            out_log_weights.at(i) = LOG_SUM_W;
        }

        // Normalize residual probabilities
        uAssertBounds(ix, 0, N_SAMPLES);
        probs_res /= (N_SAMPLES_F - U_TO_REAL(ix));
        uAssertRealEq(uMatrixUtils::sum(probs_res), U_TO_REAL(1.0));

        // Next perform stochastic sampling:
        //  p_i' = (N * p_i - N_i) / (N - sum(N_i)) where p_i' is new
        //      selection probability of sample i based on the residual
        //      (N * p_i - N_i) where terms are defined in previous comment

        for (; ix < N_SAMPLES; ++ix) {
            // Select a sample from pool
            i = uSisUtilsQc::rand_select_index(probs_res U_THREAD_ID_ARG);
            uAssertBounds(i, 0, N_SAMPLES);
            // Replace sample status
            uAssert(status_pool[i]);
            out_status.set(ix, status_pool.test(i));
            // Replace sample
            out_samples[ix] = SAMPLES_POOL[i];
        }
    }
};

#endif  // uSisResidualSamplerRsMixin_h
