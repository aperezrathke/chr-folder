//****************************************************************************
// uSisParallelBatchGrower.h
//****************************************************************************

#ifndef uSisParallelBatchGrower_h
#define uSisParallelBatchGrower_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_THREADS

#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// uSisParallelBatchGrower
//****************************************************************************

/**
 * Performs batch growth operations in parallel
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisParallelBatchGrower {
public:
    // Typedefs
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

private:
    /**
     * Common state shared by child work classes
     */
    struct grow_work_common_state_t {
        /**
         * The final output status of all samples for this growth operation
         */
        uWrappedBitset& m_status;

        /**
         * The set of all samples
         */
        std::vector<sample_t>& m_samples;

        /**
         * The log weights for all samples.
         */
        uVecCol& m_log_weights;

        /**
         * Our trial runner handle
         */
        trial_runner_mixin_t& m_tr;

        /**
         * The parent simulation
         */
        sim_t& m_sim;

        /**
         * The number of grow calls to complete for each sample
         */
        const uUInt m_target_num_grow_steps;
    };

    /**
     * Work object for performing initial grow calls
     */
    class grow_1st_work_t : public uMappableWork {
    public:
        /**
         * Constructor
         */
        explicit grow_1st_work_t(grow_work_common_state_t& state)
            : m_s(state),
              m_cached_num_samples_to_grow(U_TO_UINT(state.m_status.size())),
              m_atomic_status(state.m_status.size(), uTRUE) {
            uAssert(m_cached_num_samples_to_grow > 0);
        }

        /**
         * Grows samples over parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual uBool do_range(const uUInt ix_start,
                               const uUInt count,
                               const uUInt thread_id) {
            uAssert(this->check_arrays());

            // Determine end index
            const uUInt end =
                std::min(ix_start + count, m_cached_num_samples_to_grow);

            // Grow samples through first phase
            for (uUInt i = ix_start; i < end; ++i) {
                uAssertBounds(i, 0, m_atomic_status.size());
                sample_t& sample(m_s.m_samples[i]);
                uReal& log_weight(m_s.m_log_weights.at(U_TO_MAT_SZ_T(i)));
                m_atomic_status[i] =
                    m_s.m_tr.grow_1st(m_s.m_target_num_grow_steps,
                                      sample,
                                      log_weight,
                                      m_s.m_sim,
                                      thread_id);

            }  // end iteration over samples

            return (end < m_cached_num_samples_to_grow);
        }

        /**
         * Needed to convert internal status buffer representation back to a
         * (non-thread safe) bit set representation
         */
        void finalize_status() {
            uAssert(this->check_arrays());
            for (size_t i = 0; i < m_cached_num_samples_to_grow; ++i) {
                uAssertBounds(i, 0, m_atomic_status.size());
                uAssertBounds(i, 0, m_s.m_status.size());
                m_s.m_status.set(i, (uTRUE == m_atomic_status[i]));
            }
        }

    private:
        /**
         * Verify parallel arrays
         */
        inline bool check_arrays() const {
            uAssertPosEq(m_cached_num_samples_to_grow, m_atomic_status.size());
            uAssert(m_cached_num_samples_to_grow == m_s.m_log_weights.size());
            uAssert(m_cached_num_samples_to_grow == m_s.m_samples.size());
            uAssert(m_cached_num_samples_to_grow == m_s.m_status.size());
            return true;
        }

        /**
         * Common state
         */
        grow_work_common_state_t& m_s;

        /**
         * The cached number of samples to be grown
         */
        const uUInt m_cached_num_samples_to_grow;

        /**
         * Stores the final status of all samples after grow call. This is a
         * thread safe write structure but it needs to be converted back to a
         * bit set after we've finished parallel processing.
         */
        std::vector<uBool> m_atomic_status;
    };

    /**
     * Performs second growth phase
     */
    class grow_2nd_op_t {
    public:
        inline static uBool call(
            grow_work_common_state_t& state,
            sample_t& sample,
            const uUInt num_completed_grow_steps_per_sample,
            uReal& log_weight,
            const uUInt thread_id) {
            return state.m_tr.grow_2nd(state.m_target_num_grow_steps,
                                       sample,
                                       num_completed_grow_steps_per_sample,
                                       log_weight,
                                       state.m_sim,
                                       thread_id);
        }
    };

    /**
     * Performs nth grow phase
     */
    class grow_nth_op_t {
    public:
        inline static uBool call(
            grow_work_common_state_t& state,
            sample_t& sample,
            const uUInt num_completed_grow_steps_per_sample,
            uReal& log_weight,
            const uUInt thread_id) {
            return state.m_tr.grow_nth(state.m_target_num_grow_steps,
                                       sample,
                                       num_completed_grow_steps_per_sample,
                                       log_weight,
                                       state.m_sim,
                                       thread_id);
        }
    };

    /**
     * Class for defining thread-level work after samples have
     * gone through first growth phase
     */
    template <typename t_grow_op>
    class grow_after_1st_work_t : public uMappableWork {
    public:
        /**
         * Constructor
         */
        grow_after_1st_work_t(grow_work_common_state_t& state,
                              const uUInt num_completed_grow_steps_per_sample)
            : m_s(state),
              m_cached_num_samples_to_grow(U_TO_UINT(state.m_status.count())),
              m_num_completed_grow_steps_per_sample(
                  num_completed_grow_steps_per_sample) {
            // Note: bit sets writes are not thread safe, so we need to
            // convert to a std::vector. However, we only care about the true
            // bits so we also need to keep track of the bit index of each
            // true bit.

            uAssert(m_cached_num_samples_to_grow > 0);
            m_atomic_status.resize(m_cached_num_samples_to_grow, uFALSE);
            m_status_index.reserve(m_cached_num_samples_to_grow);

            for (size_t i = state.m_status.find_first();
                 i != state.m_status.npos;
                 i = state.m_status.find_next(i)) {
                // Keep track of true bit indices
                m_status_index.push_back(U_TO_UINT(i));
            }
        }

        /**
         * Grows samples over parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual uBool do_range(const uUInt ix_start,
                               const uUInt count,
                               const uUInt thread_id) {
            uAssertPosEq(m_cached_num_samples_to_grow, m_status_index.size());
            uAssert(m_cached_num_samples_to_grow == m_atomic_status.size());
            const uUInt end =
                std::min(ix_start + count, m_cached_num_samples_to_grow);

            // Grow samples through next phase
            uUInt ix_target = 0;
            for (uUInt i = ix_start; i < end; ++i) {
                uAssertBounds(i, 0, m_status_index.size());
                uAssertBounds(i, 0, m_atomic_status.size());

                ix_target = m_status_index[i];

                uAssertBounds(ix_target, 0, m_s.m_samples.size());
                uAssertBounds(ix_target, 0, m_s.m_log_weights.size());

                sample_t& sample(m_s.m_samples[ix_target]);
                uReal& log_weight(
                    m_s.m_log_weights.at(U_TO_MAT_SZ_T(ix_target)));
                m_atomic_status[i] =
                    t_grow_op::call(m_s,
                                    sample,
                                    m_num_completed_grow_steps_per_sample,
                                    log_weight,
                                    thread_id);
            }  // end iteration over samples

            return (end < m_cached_num_samples_to_grow);
        }

        /**
         * Needed to convert internal status buffer representation back to a
         * (non-thread safe) bit set representation
         */
        void finalize_status() {
            uAssertPosEq(m_cached_num_samples_to_grow, m_status_index.size());
            uAssert(m_cached_num_samples_to_grow == m_atomic_status.size());

            uUInt ix_target = 0;
            for (size_t i = 0; i < m_cached_num_samples_to_grow; ++i) {
                uAssertBounds(i, 0, m_status_index.size());
                uAssertBounds(i, 0, m_atomic_status.size());

                ix_target = m_status_index[i];

                uAssertBounds(ix_target, 0, m_s.m_status.size());

                m_s.m_status.set(ix_target, (uTRUE == m_atomic_status[i]));
            }
        }

    protected:
        /**
         * Common state
         */
        grow_work_common_state_t& m_s;

        /**
         * The cached number of samples to be grown
         */
        const uUInt m_cached_num_samples_to_grow;

        /**
         * The number of already completed grow steps for all samples
         */
        const uUInt m_num_completed_grow_steps_per_sample;

        /**
         * Stores the bit index of i-th status field within m_s.m_status. Used
         * for converting from m_atomic_status (a std::vector) back to
         * m_s.m_status (a bit set).
         */
        std::vector<uUInt> m_status_index;

        /**
         * Stores the final status of all samples after grow call. This is a
         * thread safe write structure but it needs to be converted back to a
         * bit set after we've finished parallel processing.
         */
        std::vector<uBool> m_atomic_status;
    };

    /**
     * Thread-level work for second growth phase
     */
    typedef grow_after_1st_work_t<grow_2nd_op_t> grow_2nd_work_t;

    /**
     * Thread level work for nth growth phase
     */
    typedef grow_after_1st_work_t<grow_nth_op_t> grow_nth_work_t;

    /**
     * Utility to run a work object until completion. Will finalize status.
     * @param work - The work object to complete
     * @param chunk_size - The amount of work each thread grabs and must
     *  complete before grabbing another chunk of work
     */
    template <typename t_work>
    static void do_batch_grow(t_work& work,
                              const uUInt chunk_size U_THREAD_ID_PARAM) {
        // @TODO - if calling from non-main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);

        // Chunk size must be positive!
        uAssert(chunk_size > 0);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        uParMap.wait_until_work_finished(work, chunk_size);

        work.finalize_status();
    }

public:
    /**
     * Performs the first growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  complete for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     */
    static void batch_grow_1st(uWrappedBitset& status,
                               std::vector<sample_t>& samples,
                               uVecCol& log_weights,
                               trial_runner_mixin_t& tr,
                               sim_t& sim,
                               const uUInt target_num_grow_steps
                                   U_THREAD_ID_PARAM) {
        // Verify growth target is positive
        uAssert(target_num_grow_steps > 0);

        struct grow_work_common_state_t s = {
            status, samples, log_weights, tr, sim, target_num_grow_steps};

        grow_1st_work_t work(s);

        do_batch_grow(work, sim.get_grow_chunk_size() U_THREAD_ID_ARG);
    }

    /**
     * Performs the second growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  grow to for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     * @param num_completed_grow_steps_per_sample - The number of grow steps
     *  each sample has already completed
     */
    static void batch_grow_2nd(uWrappedBitset& status,
                               std::vector<sample_t>& samples,
                               uVecCol& log_weights,
                               trial_runner_mixin_t& tr,
                               sim_t& sim,
                               const uUInt target_num_grow_steps,
                               const uUInt num_completed_grow_steps_per_sample
                                   U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps_per_sample < target_num_grow_steps);

        struct grow_work_common_state_t s = {
            status, samples, log_weights, tr, sim, target_num_grow_steps};

        grow_2nd_work_t work(s, num_completed_grow_steps_per_sample);

        do_batch_grow(work, sim.get_grow_chunk_size() U_THREAD_ID_ARG);
    }

    /**
     * Performs the next growth phase on a collection of samples
     * @param status - The growth status of the samples, false means sample is
     *  finished growing or is dead (check weight to differentiate), true
     *  means samples need further growth
     * @param samples - The collection of samples that need to be grown
     * @param log_weights - The log weights of all samples
     * @param tr - The parent trial runner
     * @param sim - The parent simulation
     * @param target_num_grow_steps - The suggested number of grow steps to
     *  grow to for the parameter trial runner. The trial runner is not
     *  required to use this parameter.
     * @param num_completed_grow_steps_per_sample - The number of grow steps
     *  each sample has already completed
     */
    static void batch_grow_nth(uWrappedBitset& status,
                               std::vector<sample_t>& samples,
                               uVecCol& log_weights,
                               trial_runner_mixin_t& tr,
                               sim_t& sim,
                               const uUInt target_num_grow_steps,
                               const uUInt num_completed_grow_steps_per_sample
                                   U_THREAD_ID_PARAM) {
        uAssert(num_completed_grow_steps_per_sample < target_num_grow_steps);

        struct grow_work_common_state_t s = {
            status, samples, log_weights, tr, sim, target_num_grow_steps};

        grow_nth_work_t work(s, num_completed_grow_steps_per_sample);

        do_batch_grow(work, sim.get_grow_chunk_size() U_THREAD_ID_ARG);
    }
};

#else  // !defined (U_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef U_BUILD_COMPILER_MSVC
#pragma message( \
    "Warning: parallel growth disabled. Defaulting to serial method.")
#elif defined(U_BUILD_COMPILER_GCC)
#pragma message \
    "Warning: parallel growth disabled. Defaulting to serial method."
#endif  // compiler check

#include "uSisSerialBatchGrower.h"

#define uSisParallelBatchGrower uSisSerialBatchGrower

#endif  // U_BUILD_ENABLE_THREADS

#endif  // uSisParallelBatchGrower_h
