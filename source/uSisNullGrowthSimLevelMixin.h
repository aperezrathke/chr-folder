//****************************************************************************
// uSisNullGrowthSimLevelMixin_h
//****************************************************************************

/**
 * Null interface for simulation level growth mixin
 */

#ifndef uSisNullGrowthSimLevelMixin_h
#define uSisNullGrowthSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisNullSimLevelMixin.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Interface
//****************************************************************************

/**
 * Mixin encapsulating global (sample independent) data and utilities
 */
template <typename t_SisGlue>
class uSisNullGrowthSimLevelMixin : public uSisNullSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Callback for when all samples have been seeded (have placed their
     *  initial nodes)
     * @param sim - parent simulation
     */
    inline void on_seed_finish(const sim_t& sim) {}

    /**
     * Callback for when all samples have finished a single growth phase
     * @param num_completed_grow_steps_per_sample - The number of growth
     *  calls (seed() + grow_*()) that have completed. This value is
     *  reset between trials.
     * @param log_weights - the current log weights of each sample
     * @param sim - parent simulation
     */
    inline void on_grow_phase_finish(
        const uUInt num_completed_grow_steps_per_sample,
        const sim_t& sim U_THREAD_ID_PARAM) {}
};

#endif  // uSisNullGrowthSimLevelMixin
