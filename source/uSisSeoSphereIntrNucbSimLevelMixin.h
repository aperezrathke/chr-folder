//****************************************************************************
// uSisSeoSphereIntrNucbSimLevelMixin.h
//****************************************************************************

/**
 * A nuclear body interaction mixin manages nuclear body association
 *  constraints. Note, this is different from a "nuclear mixin" which is used
 *  for modeling confinement by constraining all monomer beads to reside
 *  within the nuclear volume. In contrast, the nuclear body interaction mixin
 *  enforces polymer fragments to be proximal or distal to a specific
 *  spherical mass (x, y, z) coordinate as configured by the user.
 *  Technically, this mass does not have to reside within nuclear volume.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin does not explicitly enforce excluded volume of the mass; however,
 * this can be achieved using a radial knock-out distance constraint applied
 * to a set of fragments incorporating all beads.
 *
 * Note, if excluded volume is desired for a *large* number of nuclear bodies,
 * then a static broad phase can be created containing just the nuclear body
 * components. This *simulation level* broad phase may be checked against
 * during nuclear body filtering. However, if the number of nuclear bodies is
 * *sparse*, then it is likely more efficient to perform narrow phase checks
 * on every filter call as this implementation is intended for - @TODO -
 * consider adding implicit narrow phase excluded volume support which creates
 * a fragment containing all polymer beads and sets the knock-out distance
 * based on the nuclear body radius.
 */

#ifndef uSisSeoSphereIntrNucbSimLevelMixin_h
#define uSisSeoSphereIntrNucbSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibConfig.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uParserRle.h"
#include "uParserTable.h"
#include "uSisIntrNucbCommonBatch.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Mixin encapsulating global(sample independent) data and utilities.  All
 *  interaction distances are pre-padded by the corresponding nuclear body
 *  radii. The user configured interaction distances may additionally be
 *  padded if polymer nodes all have same radius (i.e. are homogeneous).
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrNucbSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Batch interactions utility
     */
    typedef uSisIntrNucbCommonBatch<t_SisGlue> intr_nucb_batch_util_t;

private:
    /**
     * Knock-in nuclear body interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIMatrix& get_intrs(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_kin;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_loci_tables.loci_kin;
        }
        static uVecCol& get_dist(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_kin_dist;
        }
        static uVecCol& get_dist2(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_kin_dist2;
        }
        static uReal get_dist_scalar_default() {
            return uIntrNucbDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_nucb_knock_in; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_nucb_knock_in_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_nucb_knock_in_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_nucb_knock_in_bit_mask;
        }
        static const char* get_data_name() { return uIntrNucbKnockInDataName; }
        /**
         * @param dist - Output Euclidean distance threshold, always padded by
         *  body radius, will additionally be padded by node radius if all
         *  nodes are homogeneous
         * @param dist2 - Output squared Euclidean distance threshold, only
         *  useful if nodes are homogeneous
         * @param p_body_center - Nuclear body (x, y, z) center
         * @param body_radius - Nuclear body radius
         * @param sim - Parent simulation
         * @return uTRUE if distance threshold is non-trivial, uFALSE o/w
         *  (uFALSE -> interaction is always satisfiable under nuclear
         *      confinement)
         */
        static uBool init_dist_scalars(uReal& dist,
                                       uReal& dist2,
                                       const uReal* const p_body_center,
                                       const uReal body_radius,
                                       const sim_t& sim) {
            // Distance threshold must be non-negative real
            uAssertRealGte(dist, U_TO_REAL(0.0));
            // Assume non-negative body radius
            uAssert(body_radius >= U_TO_REAL(0.0));
            // Check if we should pre-pad by bead radius
            if (sample_t::is_homg_radius) {
                dist += (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Pad by nuclear body radius
            dist += body_radius;
            // Just in case, enforce non-negative distance!
            dist = std::max(dist, U_TO_REAL(0.0));
            // Compute squared distance threshold
            dist2 = dist * dist;
            // @TODO - add check for trivial interactions
            return uTRUE;
        }
    } init_intr_nucb_kin_policy_t;

    /**
     * Knock-out nuclear body interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIMatrix& get_intrs(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_ko;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_loci_tables.loci_ko;
        }
        static uVecCol& get_dist(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_ko_dist;
        }
        static uVecCol& get_dist2(uSisSeoSphereIntrNucbSimLevelMixin& this_) {
            return this_.m_intr_nucb_ko_dist2;
        }
        static uReal get_dist_scalar_default() {
            return uIntrNucbDefaultKnockOutDistScale *
                   uIntrNucbDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_nucb_knock_out; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_nucb_knock_out_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_nucb_knock_out_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_nucb_knock_out_bit_mask;
        }
        static const char* get_data_name() { return uIntrNucbKnockOutDataName; }
        /**
         * @TODO - Exit with error code if constraint trivially infeasible
         * @param dist - Output Euclidean distance threshold, always padded by
         *  body radius, will additionally be padded by node radius if all
         *  nodes are homogeneous
         * @param dist2 - Output squared Euclidean distance threshold - only
         *  useful is nodes are homogeneous
         * @param p_body_center - Nuclear body (x, y, z) center
         * @param body_radius - Nuclear body radius
         * @param sim - Parent simulation
         * @return uTRUE (will exit on error)
         */
        static uBool init_dist_scalars(uReal& dist,
                                       uReal& dist2,
                                       const uReal* const p_body_center,
                                       const uReal body_radius,
                                       const sim_t& sim) {
            // Distance threshold must be non-negative real
            uAssertRealGte(dist, U_TO_REAL(0.0));
            // Assume non-negative body radius
            uAssert(body_radius >= U_TO_REAL(0.0));
            // Check if we should pre-pad by bead radius
            if (sample_t::is_homg_radius) {
                dist += (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Pad by nuclear body radius
            dist += body_radius;
            // Just in case, enforce non-negative distance!
            dist = std::max(dist, U_TO_REAL(0.0));
            // Compute squared distance
            dist2 = dist * dist;
            // @TODO - Check for trivially infeasible (need body center)
            return uTRUE;
        }
    } init_intr_nucb_ko_policy_t;

public:
    /**
     * Default constructor
     */
    uSisSeoSphereIntrNucbSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level (global) data
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        // Load fragment spans
        uIntrLibConfig::load_frag(m_intr_nucb_frags,
                                  config,
                                  uOpt_nucb_frag,
                                  uFALSE /*no_fail*/,
                                  uIntrNucbFragDataName,
                                  sim.get_max_total_num_nodes());
        uAssert(m_intr_nucb_frags.n_cols >= 1);
        uAssert(m_intr_nucb_frags.n_rows == uIntrLibPairIxNum);
        // Initialize nuclear body centers
        if (load_intr_nucb_bodies(
                m_intr_nucb_centers, m_intr_nucb_radii, sim, config)) {
            uAssertPosEq(m_intr_nucb_centers.n_cols, m_intr_nucb_radii.n_elem);
            uAssertPosEq(m_intr_nucb_centers.n_rows, uDim_num);
            // Initialize knock-in interactions
            init_intrs_nucb<init_intr_nucb_kin_policy_t>(*this,
                                                         config,
                                                         m_intr_nucb_frags,
                                                         m_intr_nucb_centers,
                                                         m_intr_nucb_radii,
                                                         sim);
            // Initialize knock-out interactions
            init_intrs_nucb<init_intr_nucb_ko_policy_t>(*this,
                                                        config,
                                                        m_intr_nucb_frags,
                                                        m_intr_nucb_centers,
                                                        m_intr_nucb_radii,
                                                        sim);
        } else {
            // No nuclear bodies found, reset state
            uLogf(
                "Warning: unable to initialize nuclear body interaction "
                "constraints.\n");
            clear();
        }
    }

    /**
     * Modifies seed radii according to nuclear body interaction constraints
     * @param seed_radii - Matrix of size 3 rows x (number of loci) columns,
     *  each column is the (X, Y, Z) radii respectively of the bounding seed
     *  volume for that locus
     * @param sim - Outer simulation
     * @param config - User configuration
     */
    void update_seed_radii(uMatrix& seed_radii,
                           const sim_t& sim,
                           uSpConstConfig_t config) const {
        // @TODO!
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_intr_nucb_frags.clear();
        m_intr_nucb_kin.clear();
        m_intr_nucb_ko.clear();
        m_intr_nucb_kin_dist.clear();
        m_intr_nucb_kin_dist2.clear();
        m_intr_nucb_ko_dist.clear();
        m_intr_nucb_ko_dist2.clear();
        intr_nucb_batch_util_t::clear(m_intr_nucb_loci_tables);
        m_intr_nucb_centers.clear();
        m_intr_nucb_radii.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Const handle to interaction fragments
     */
    inline const uUIMatrix& get_intr_nucb_frags() const {
        return m_intr_nucb_frags;
    }

    /**
     * @return Const handle to knock-in interactions
     */
    inline const uUIMatrix& get_intr_nucb_kin() const {
        return m_intr_nucb_kin;
    }

    /**
     * @return Const handle to knock-out interactions
     */
    inline const uUIMatrix& get_intr_nucb_ko() const { return m_intr_nucb_ko; }

    /**
     * @param i - Knock-in interaction index
     * @return [Squared] knock-in Euclidean distance threshold relative to
     *  origin
     */
    inline uReal get_intr_nucb_kin_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_nucb_kin_dist.n_elem);
        return m_intr_nucb_kin_dist.at(i);
    }
    inline uReal get_intr_nucb_kin_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_nucb_kin_dist2.n_elem);
        return m_intr_nucb_kin_dist2.at(i);
    }

    /**
     * @param i - Knock-out interaction index
     * @return [Squared] knock-out Euclidean distance threshold relative to
     *  origin
     */
    inline uReal get_intr_nucb_ko_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_nucb_ko_dist.n_elem);
        return m_intr_nucb_ko_dist.at(i);
    }
    inline uReal get_intr_nucb_ko_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_nucb_ko_dist2.n_elem);
        return m_intr_nucb_ko_dist2.at(i);
    }

    /**
     * @return Lamina knock-in interaction failure budget
     */
    inline uUInt get_intr_nucb_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Lamina knock-out interaction failure budget
     */
    inline uUInt get_intr_nucb_ko_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Handle to loci interaction tables
     */
    inline const uSisIntrNucbLociTables_t& get_intr_nucb_loci_tables() const {
        return m_intr_nucb_loci_tables;
    }

    /**
     * @return Table of lamina knock-in interactions
     */
    inline const uSisIntrLociTable_t& get_intr_nucb_loci_kin() const {
        return m_intr_nucb_loci_tables.loci_kin;
    }

    /**
     * @return Table of lamina knock-out interactions
     */
    inline const uSisIntrLociTable_t& get_intr_nucb_loci_ko() const {
        return m_intr_nucb_loci_tables.loci_ko;
    }

    /**
     * @return Const handle to nuclear body (x, y, z) centers
     */
    inline const uMatrix& get_intr_nucb_centers() const {
        return m_intr_nucb_centers;
    }

    /**
     * @return Const handle to nuclear body radii
     */
    inline const uVecCol& get_intr_nucb_radii() const {
        return m_intr_nucb_radii;
    }

    // Export interface

    /**
     * Copies lamina interaction fragments to 'out'
     */
    void get_intr_nucb_frags(uUIMatrix& out) const { out = m_intr_nucb_frags; }

    /**
     * Copies knock-in lamina interactions to 'out'
     */
    void get_intr_nucb_kin(uUIMatrix& out) const { out = m_intr_nucb_kin; }

    /**
     * Copies knock-out lamina interactions to 'out'
     */
    void get_intr_nucb_ko(uUIMatrix& out) const { out = m_intr_nucb_ko; }

    /**
     * Copies nuclear body (x,y,z) centers to 'out'
     */
    void get_intr_nucb_centers(uMatrix& out) const {
        out = m_intr_nucb_centers;
    }

    /**
     * Copies nuclear body radii to 'out'
     */
    void get_intr_nucb_radii(uVecCol& out) const { out = m_intr_nucb_radii; }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    // Initialize nuclear body centers and radii
    static uBool load_intr_nucb_bodies(uMatrix& body_centers,
                                       uVecCol& body_radii,
                                       const sim_t& sim,
                                       uSpConstConfig_t config) {
        ////////////////////////////////////////////////////////////
        // Body centers
        uAssert(body_centers.is_empty());
        uAssert(body_radii.is_empty());
        std::string fpath;
        if (!config->resolve_path(fpath, uOpt_nucb_centers)) {
            return uFALSE;
        }
        uParserTable table;
        if (!table.read(fpath, uFALSE /*has_header*/, uTRUE /*is_jagged*/)) {
            uLogf("Warning: Unable to read nuclear body centers from:\n\t%s\n",
                  fpath.c_str());
            return uFALSE;
        }
        if (table.num_rows() < U_TO_UINT(1)) {
            uLogf("Warning: No rows for nuclear body centers in:\n\t%s\n",
                  fpath.c_str());
            return uFALSE;
        }
        // For better cache utilization, store as column-major - we must
        // transpose the parsed table!
        body_centers.set_size(uDim_num /*n_rows*/, table.num_rows() /*n_cols*/);
        // Initialize radii to point masses
        body_radii.zeros(table.num_rows());
        for (uUInt i = 0; i < table.num_rows(); ++i) {
            const uUInt num_cols = table.num_cols(i);
            // Verify row is (x, y, z) or (x, y, z, d) tuple
            if ((num_cols != uDim_num) && (num_cols != uDim_num_extended)) {
                uLogf(
                    "Warning: Unexpected number of elements (%d) for nuclear "
                    "body row 0-index %d (must be %d or %d) in file:\n\t%s\n",
                    (int)num_cols,
                    (int)(i),
                    (int)uDim_num,
                    (int)uDim_num_extended,
                    fpath.c_str());
                return uFALSE;
            }
            // Read (x, y, z) tuple
            for (uUInt j = 0; j < U_TO_UINT(uDim_num); ++j) {
                const uReal xyz = table.get<uReal>(i, j);
                uAssertBounds(j, U_TO_UINT(0), U_TO_UINT(body_centers.n_rows));
                uAssertBounds(i, U_TO_UINT(0), U_TO_UINT(body_centers.n_cols));
                body_centers.at(j, i) = xyz;
            }
            // Read optional diameter
            if (num_cols == uDim_num_extended) {
                // Note: value at uDim_radius is a diameter in this case!
                uReal d = table.get<uReal>(i, uDim_radius);
                if (d < U_TO_REAL(0.0)) {
                    uLogf(
                        "Warning: Unexpected nuclear body diameter %f < 0 "
                        "(setting to 0), in file:\n\t%s\n",
                        (double)d,
                        fpath.c_str());
                    d = U_TO_REAL(0.0);
                }
                // Store as radius
                body_radii.at(i) = U_TO_REAL(0.5) * d;
            }
        }
        uAssertPosEq(body_centers.n_cols, body_radii.n_elem);
        uAssertPosEq(body_centers.n_rows, uDim_num);
        ////////////////////////////////////////////////////////////
        // Body radii (non-RLE)
        const uVecCol z(body_radii.n_elem, uMatrixUtils::fill::zeros);
        bool radii_overloaded = false;
        fpath.clear();
        if (config->read_into(fpath, uOpt_nucb_diameter_fpath)) {
            uVecCol diams;
            if (diams.load(fpath)) {
                if (diams.n_elem == body_centers.n_cols) {
                    body_radii = uMatrixUtils::max(diams, z);
                    // Convert from diameters to radii
                    body_radii *= U_TO_REAL(0.5);
                    radii_overloaded = true;
                } else {
                    uLogf(
                        "Warning: ignoring nuclear body diameters (%s) "
                        "from:\n\t\t%s\n\twhich encodes %d elements but "
                        "expected %d.\n",
                        uOpt_get_ini_key(uOpt_nucb_diameter_fpath),
                        fpath.c_str(),
                        (int)diams.n_elem,
                        (int)body_centers.n_cols);
                }
            }
        }
        ////////////////////////////////////////////////////////////
        // Body radii (RLE)
        fpath.clear();
        if (!radii_overloaded &&
            config->read_into(fpath, uOpt_nucb_diameter_fpath_rle)) {
            uVecCol diams;
            if (uParserRle::read<uReal, uVecCol>(diams, fpath)) {
                if (diams.n_elem == body_centers.n_cols) {
                    body_radii = uMatrixUtils::max(diams, z);
                    // Convert from diameters to radii
                    body_radii *= U_TO_REAL(0.5);
                    radii_overloaded = true;
                } else {
                    uLogf(
                        "Warning: ignoring nuclear body diameters (%s) "
                        "from:\n\t\t%s\n\twhich encodes %d elements but "
                        "expected %d.\n",
                        uOpt_get_ini_key(uOpt_nucb_diameter_fpath_rle),
                        fpath.c_str(),
                        (int)diams.n_elem,
                        (int)body_centers.n_cols);
                }
            }
        }
        ////////////////////////////////////////////////////////////
        // Body radii (scalar)
        uReal diam = U_TO_REAL(0.0);
        if (!radii_overloaded && config->read_into(diam, uOpt_nucb_diameter)) {
            if (diam < U_TO_REAL(0.0)) {
                uLogf(
                    "Warning: scalar nuclear body diameter < 0, setting to "
                    "0.\n");
                diam = U_TO_REAL(0.0);
            }
            // Store as radius
            body_radii.fill((U_TO_REAL(0.5) * diam));
        }
        ////////////////////////////////////////////////////////////
        // Post
        uAssertPosEq(body_centers.n_cols, body_radii.n_elem);
        uAssertPosEq(body_centers.n_rows, uDim_num);
        uReal mean_d = U_TO_REAL(0.0);
        if (body_radii.n_elem > U_TO_MAT_SZ_T(0)) {
            mean_d = U_TO_REAL(2.0) * uMatrixUtils::mean(body_radii);
        }
        uLogf("Loaded %d nuclear bodies with mean diameter %f Angstroms\n",
              (int)body_centers.n_cols,
              (double)mean_d);
        // Signal that load successful
        return uTRUE;
    }

    /**
     * Extracts masked nuclear body interaction data
     */
    static void apply_intrs_nucb_mask(const uBoolVecCol& mask,
                                      uUIMatrix& intrs,
                                      uVecCol& dist,
                                      uVecCol& dist2) {
        uAssert(mask.n_elem == intrs.n_cols);
        const uMatSz_t n_intrs = uMatrixUtils::sum(mask);
        uAssertBoundsInc(n_intrs, U_TO_MAT_SZ_T(0), intrs.n_cols);
        uUIMatrix temp_intrs;
        uVecCol temp_dist;
        uVecCol temp_dist2;
        // Extract masked data
        if (n_intrs > U_TO_UINT(0)) {
            temp_intrs.set_size(uIntrNucbPairIxNum, n_intrs);
            temp_dist.set_size(n_intrs);
            temp_dist2.set_size(n_intrs);
            uUInt dst = U_TO_UINT(0);
            for (uMatSz_t src = 0; src < mask.n_elem; ++src) {
                const uBool bit = mask.at(src);
                if (bit) {
                    uAssertBounds(dst, U_TO_UINT(0), temp_intrs.n_cols);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist.n_elem);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist2.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), intrs.n_cols);
                    uAssertBounds(src, U_TO_UINT(0), dist.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), dist2.n_elem);
                    temp_intrs.at(uIntrNucbPairIxFrag, dst) =
                        intrs.at(uIntrNucbPairIxFrag, src);
                    temp_intrs.at(uIntrNucbPairIxBody, dst) =
                        intrs.at(uIntrNucbPairIxBody, src);
                    temp_dist.at(dst) = dist.at(src);
                    temp_dist2.at(dst) = dist2.at(src);
                    ++dst;
                }
            }
            uAssert(dst == n_intrs);
        }
        // Overwrite buffers
        uAssert(temp_intrs.n_cols <= intrs.n_cols);
        uAssert(temp_intrs.n_rows == intrs.n_rows);
        uAssert(temp_dist.n_elem <= dist.n_elem);
        uAssert(temp_dist2.n_elem <= dist2.n_elem);
        uAssert(temp_dist2.n_elem == temp_dist.n_elem);
        uAssert(temp_intrs.n_cols == temp_dist.n_elem);
        intrs = temp_intrs;
        dist = temp_dist;
        dist2 = temp_dist2;
    }

    /**
     * Initialize nuclear body interactions
     */
    template <typename t_init>
    static void init_intrs_nucb(uSisSeoSphereIntrNucbSimLevelMixin& this_,
                                uSpConstConfig_t config,
                                const uUIMatrix& frags,
                                const uMatrix& body_centers,
                                const uVecCol& body_radii,
                                const sim_t& sim) {
        const char* const data_name = t_init::get_data_name();
        uAssert(data_name);
        // This method should not be called if no fragments exist
        uAssert(frags.n_cols >= 1);
        uAssert(frags.n_rows == uIntrLibPairIxNum);
        // This method should not be called if no nuclear bodies exist
        uAssertPosEq(body_centers.n_cols, body_radii.n_elem);
        uAssertPosEq(body_centers.n_rows, uDim_num);

        // Load interactions
        uUIMatrix& intrs = t_init::get_intrs(this_);
        if (!uIntrLibConfig::load_index_pairs(intrs,
                                              uFALSE /*b_order*/,
                                              config,
                                              t_init::get_opt_intr(),
                                              uFALSE /*no_fail*/,
                                              data_name)) {
            uLogf("Warning: no %s nuclear body interactions loaded.\n",
                  data_name);
            intrs.clear();
            return;
        }
        uAssert(intrs.n_cols >= 1);
        check_intr_nucb_bounds_exit_on_fail(
            intrs, frags, body_centers, data_name);

        // Initialize interaction distances (with optional padding)
        uVecCol& dist = t_init::get_dist(this_);
        uVecCol& dist2 = t_init::get_dist2(this_);
        uIntrLibConfig::load_dist(dist,
                                  intrs.n_cols,
                                  config,
                                  t_init::get_opt_dist_fpath(),
                                  t_init::get_opt_dist_scalar(),
                                  t_init::get_dist_scalar_default(),
                                  data_name);
        uAssertPosEq(dist.n_elem, intrs.n_cols);
        dist2.set_size(dist.n_elem);
        uBoolVecCol non_trivial_mask;
        non_trivial_mask.set_size(dist.n_elem);
        non_trivial_mask.fill(uTRUE);
        for (uMatSz_t i = 0; i < dist.n_elem; ++i) {
            uAssertBounds(i, U_TO_MAT_SZ_T(0), intrs.n_cols);
            uAssertBounds(uIntrNucbPairIxBody, U_TO_MAT_SZ_T(0), intrs.n_rows);
            const uUInt body_id = intrs.at(uIntrNucbPairIxBody, i);
            uAssertBounds(
                body_id, U_TO_UINT(0), U_TO_UINT(body_centers.n_cols));
            uAssertBounds(body_id, U_TO_UINT(0), U_TO_UINT(body_radii.n_elem));
            const uReal* const p_body_center = body_centers.colptr(body_id);
            const uReal body_radius = body_radii.at(body_id);
            non_trivial_mask.at(i) = t_init::init_dist_scalars(
                dist.at(i), dist2.at(i), p_body_center, body_radius, sim);
        }

        // Warn if trivial (always satisfied) interactions found
        const uMatSz_t num_non_trivial = uMatrixUtils::sum(non_trivial_mask);
        uAssert(num_non_trivial <= non_trivial_mask.n_elem);
        const uBool has_trivial = num_non_trivial != non_trivial_mask.n_elem;
        if (has_trivial) {
            const uMatSz_t num_trivial =
                non_trivial_mask.n_elem - num_non_trivial;
            uLogf(
                "Warning: %d trivial %s nuclear body interactions found (will "
                "be ignored).\n",
                (int)num_trivial,
                data_name);
        }

        // Read interactions mask
        uBoolVecCol mask;
        uIntrLibConfig::load_mask(
            mask, intrs.n_cols, config, t_init::get_opt_mask_min(), data_name);
        uAssert(mask.n_elem == intrs.n_cols);

        // Apply masking
        if (has_trivial) {
            uAssertPosEq(mask.n_elem, non_trivial_mask.n_elem);
            mask %= non_trivial_mask;
        }
        apply_intrs_nucb_mask(mask, intrs, dist, dist2);

        // Partition interactions by loci
        intr_nucb_batch_util_t::partition_by_loci(
            t_init::get_intr_loci(this_), intrs, frags, sim);

        // Compute mean interaction constraint distance (avoid divide-by-zero)
        double mean_dist = 0.0;
        if (intrs.n_cols > U_TO_MAT_SZ_T(0)) {
            mean_dist = (double)uMatrixUtils::mean(dist);
        }
        // Report initialized interaction state
        uAssert(mean_dist >= 0.0);
        uAssert(intrs.n_cols == dist.n_elem);
        uAssert(intrs.n_cols == dist2.n_elem);
        uLogf(
            "Initialized %d %s nuclear body interaction constraints.\n\t-<%s "
            "dist> (may be padded): %f.\n",
            (int)intrs.n_cols,
            data_name,
            data_name,
            mean_dist);
    }

    /**
     * Will exit if bounds violation encountered
     */
    static void check_intr_nucb_bounds_exit_on_fail(
        const uUIMatrix& intrs,
        const uUIMatrix& frags,
        const uMatrix& body_centers,
        const char* const data_name) {
        uAssert(intrs.is_empty() || (intrs.n_rows == uIntrNucbPairIxNum));
        for (uMatSz_t i = U_TO_MAT_SZ_T(0); i < intrs.n_cols; ++i) {
            const uUInt frag_col = intrs.at(uIntrNucbPairIxFrag, i);
            if (frag_col >= frags.n_cols) {
                uLogf(
                    "Error: Nuclear body, out-of-bounds fragment index (%s "
                    "interaction at 0-based index %d). Exiting.\n",
                    data_name,
                    (int)i);
                exit(uExitCode_error);
            }
            const uUInt body_col = intrs.at(uIntrNucbPairIxBody, i);
            if (body_col >= body_centers.n_cols) {
                uLogf(
                    "Error: Nuclear body, out-of-bounds body center index (%s "
                    "interaction at 0-based index %d). Exiting.\n",
                    data_name,
                    (int)i);
                exit(uExitCode_error);
            }
        }
    }

    /**
     * Fragment spans
     */
    uUIMatrix m_intr_nucb_frags;

    /**
     * Knock-in interactions
     */
    uUIMatrix m_intr_nucb_kin;

    /**
     * Knock-out interactions
     */
    uUIMatrix m_intr_nucb_ko;

    /**
     * At least one node in nuclear body interacting fragment must be at least
     * this distance from nuclear body (x, y, z) center. This value is padded
     * by nuclear body spherical radii at initialization.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_nucb_kin_dist;

    /**
     * Squared distance nuclear body proximity threshold
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_nucb_kin_dist2;

    /**
     * All nodes in nuclear body interacting fragment must be more than this
     * distance from the nuclear body (x, y, z) center. This value is padded
     * by nuclear body spherical radii at initialization.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_nucb_ko_dist;

    /**
     * Squared knock-out distance threshold
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_nucb_ko_dist2;

    /**
     * Collection of nuclear body interactions partitioned by loci
     */
    uSisIntrNucbLociTables_t m_intr_nucb_loci_tables;

    /**
     * Nuclear body (x, y, z) centers (column-major)
     */
    uMatrix m_intr_nucb_centers;

    /**
     * Nuclear body non-negative radii, elements may be zero (point mass)
     */
    uVecCol m_intr_nucb_radii;
};

#endif  // uSisSeoSphereIntrNucbSimLevelMixin_h
