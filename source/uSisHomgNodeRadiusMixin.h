//****************************************************************************
// uSisHomgNodeRadiusMixin.h
//****************************************************************************

/**
 * A homogeneous mixin manages the assumptions inherent to homogeneity (all
 * nodes being same size) or not.
 */

#ifndef uSisHomgNodeRadiusMixin_h
#define uSisHomgNodeRadiusMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uSisNodeRadiusMixin.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisHomgNodeRadiusSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag that simulation is homogeneous wrt to node radius
     */
    enum { is_homg_radius = true };

    /**
     * Default constructor
     */
    uSisHomgNodeRadiusSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level node radius mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Store the global node diameter and radius
        if (!config->read_into(this->homogeneous_node_diameter,
                               uOpt_max_node_diameter)) {
            uLogf("Error: max node diameter not specified.\n");
            exit(uExitCode_error);
        }
        this->homogeneous_node_radius =
            U_TO_REAL(0.5) * this->homogeneous_node_diameter;
        // Store vector of radii
        uAssert(sim.get_max_total_num_nodes() > 0);
        this->node_radii.set_size(U_TO_MAT_SZ_T(sim.get_max_total_num_nodes()));
        this->node_radii.fill(this->homogeneous_node_radius);
    }

    /**
     * Resets to default uninitialized state
     */
    void clear() {
        this->homogeneous_node_diameter = this->homogeneous_node_radius =
            U_TO_REAL(0.0);
        this->node_radii.clear();
    }

    /**
     * The maximum diameter of a monomer node within a chromatin polymer chain.
     * Since all nodes have same maximum diameter, we are making this a property
     * of the parent simulation.
     */
    inline uReal get_max_node_diameter() const {
        uAssert(this->homogeneous_node_diameter > U_TO_REAL(0.0));
        uAssert((U_TO_REAL(0.5) * this->homogeneous_node_diameter) ==
                this->homogeneous_node_radius);
        return homogeneous_node_diameter;
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * @return radius of i-th node
     */
    inline uReal get_node_radius(const uUInt node_id) const {
        return this->get_homogeneous_node_radius();
    }

    /**
     * @return vector of node radii
     */
    inline const uVecCol& get_node_radii() const { return this->node_radii; }

    /**
     * @return radius of candidate for i-th node
     */
    inline uReal get_candidate_node_radius(
        const uUInt candidate_node_id) const {
        return this->get_homogeneous_node_radius();
    }

    /**
     * @WARNING - DOES NOT CHECK IF CROSSING LOCUS BOUNDARIES!
     * Computes max node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible distance between the two node centers
     */
    inline uReal get_max_c2c_len(const uUInt start_id,
                                 const uUInt end_id) const {
        uAssert(end_id >= start_id);
        const uReal n = U_TO_REAL(end_id - start_id);
        return (U_TO_REAL(2.0) * this->homogeneous_node_radius * n);
    }

    /**
     * @WARNING - DOES NOT CHECK IF CROSSING LOCUS BOUNDARIES!
     * Computes max squared node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible squared distance between the two node centers
     */
    inline uReal get_max_sq_c2c_len(const uUInt start_id,
                                    const uUInt end_id) const {
        const uReal c2c_length = this->get_max_c2c_len(start_id, end_id);
        return (c2c_length * c2c_length);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * The radius of a monomer node within a chromatin polymer chain.
     */
    inline uReal get_homogeneous_node_radius() const {
        // Check to see that diameter = 2 * radius and is positive
        uAssertRealPosEq(
            this->homogeneous_node_radius,
            static_cast<const sim_t*>(this)->get_max_node_diameter() *
                U_TO_REAL(0.5));
        return this->homogeneous_node_radius;
    }

private:
    /**
     * The diameter of a node (chromatin monomer)
     * Assumed to be same for all nodes in simulation.
     */
    uReal homogeneous_node_diameter;

    /**
     * The radius of a node (chromatin monomer)
     * Assumed to be same for all nodes in simulation.
     */
    uReal homogeneous_node_radius;

    /**
     * Vector of all node radii
     */
    uVecCol node_radii;
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * "Typedef" for sample-level mixin
 */
#define uSisHomgNodeRadiusMixin uSisNodeRadiusMixin

#endif  // uSisHomgNodeRadiusMixin_h
