//****************************************************************************
// uSisSkiMixedLagScheduleMixin.h
//****************************************************************************

/**
 * ScheduleMixin = A mixin that defines the checkpoint schedule for
 * a quality control algorithm.
 */

#ifndef uSisSkiMixedLagScheduleMixin_h
#define uSisSkiMixedLagScheduleMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uTypes.h"

/**
 * A quality control schedule is responsible for determining when check
 * points occur.
 *
 * Schedule is meant for chrome-to-chrome, single knock-in interaction (SKI)
 * modeling. When the polymer chain is being grown in the region of the distal
 * fragment of an interaction pair, the weight variance is rather large and
 * the quality control acceptance criterion can become too stringent when
 * using methods like rejection control. To help make growth more feasible
 * (possibly at the cost of a slightly larger weight variance), this schedule
 * allows more frequent checkpoints when growing near the distal fragment of
 * the interaction pair. Specifically, the schedule is parameterized by a
 * major lag and a minor lag. The minor lag is used when growing near the
 * distal interaction fragment, otherwise the major lag is used.
 *
 * ASSUMES EXISTENCE OF "interface" defined by uSisSkiSeoSlocIntrChrMixin
 *
 * ASSUMES simulation level uSisSkiSeoSlocIntrChrSimLevelMixin has already
 * been initialized!
 *
 * This schedule is deterministic and supports landmark trial runner
 *  tr_find_first() and tr_find_next() calls.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The keys used for initializing from configuration file
    enum uOptE e_KeyMajor = uOpt_qc_schedule_lag_slot0,
    enum uOptE e_KeyMinor = uOpt_qc_schedule_lag_slot1>
class uSisSkiMixedLagScheduleMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin avoids recomputing exponential
     * weights
     */
    enum { calculates_norm_exp_weights = false };

    /**
     * Constructor
     */
    uSisSkiMixedLagScheduleMixin() { clear(); }

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     * @param is_tr - [unused] set to true if schedule is being used for
     *  landmark trial running, false o/w
     */
    void init(const sim_t& sim,
              uSpConstConfig_t config,
              const bool is_tr U_THREAD_ID_PARAM) {
        clear();

        // Read lag values
        config->read_into<uUInt>(sched_lag_major, e_KeyMajor);
        config->read_into<uUInt>(sched_lag_minor, e_KeyMinor);

        // We require that lag values be positive
        if ((sched_lag_major < 1) || (sched_lag_minor < 1)) {
            std::cout << "Error: lag values must be positive. Exiting.\n";
            exit(uExitCode_error);
        }

        // We require that minor lag be less than or equal to major
        if (sched_lag_minor > sched_lag_major) {
            std::cout << "Error: minor lag must be <= major lag. Exiting.\n";
            exit(uExitCode_error);
        }

        // Determine the interval for which the minor lag is active
        // Requires that simulation level interaction mixin has been
        // initialized! One way to help detect this is with the following:
        uAssert(sim.get_intr_chr_frag_a_nid_lo() <=
                sim.get_intr_chr_frag_a_nid_hi());
        uAssert(sim.get_intr_chr_frag_a_nid_hi() <
                sim.get_intr_chr_frag_b_nid_lo());
        uAssert(sim.get_intr_chr_frag_b_nid_lo() <=
                sim.get_intr_chr_frag_b_nid_hi());
        // Begin: use integer truncation to determine greatest lower bound
        // that is multiple of minor lag
        sched_beg_minor =
            (sim.get_intr_chr_frag_b_nid_lo() + 1) / sched_lag_minor;
        sched_beg_minor *= sched_lag_minor;
        // End: use integer truncation to determine least upper bound that is
        // multiple of minor lag
        sched_end_minor = (sim.get_intr_chr_frag_b_nid_hi() + sched_lag_minor) /
                          sched_lag_minor;
        sched_end_minor *= sched_lag_minor;
        // Assert minor lag interval covers distal fragment:
        uAssert(sched_beg_minor <= sched_end_minor);
        uAssert(sched_beg_minor <= (sim.get_intr_chr_frag_b_nid_lo() + 1));
        uAssert(sched_end_minor >= (sim.get_intr_chr_frag_b_nid_hi() + 1));

        uLogf("Initialized SKI schedule:\n");
        uLogf("\t-sched_beg_minor: %u\n", (unsigned int)this->sched_beg_minor);
        uLogf("\t-sched_end_minor: %u\n", (unsigned int)this->sched_end_minor);
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config,
                    const bool is_tr) {}

    /**
     * Resets to default state
     */
    void clear() {
        sched_lag_major = sched_lag_minor = sched_beg_minor = sched_end_minor =
            U_TO_UINT(0);
    }

    /**
     * Quality control interface
     * Determines if a quality control check point has been reached (meaning
     * it's okay to go ahead and perform quality control step).
     * @param out_norm_exp_weights - an optional *uninitialized* vector of
     *  exponential weights that must be initialized if:
     *      calculates_norm_exp_weights == TRUE
     * @param out_max_log_weight - an optional *uninitialized* reference to the
     *  maximum log weight within the log_weights vector. Must be initialized
     *  if: calculates_norm_exp_weights == TRUE
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin
     * @param sim - parent simulation
     */
    inline uBool qc_checkpoint_reached(
        uVecCol& out_norm_exp_weights,
        uReal& out_max_log_weight,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_level_qc_mixin_t& qc,
        const sim_t& sim) {
        if ((num_completed_grow_steps_per_sample % sched_lag_major) == 0) {
            return uTRUE;
        } else {
            if ((sched_beg_minor <= num_completed_grow_steps_per_sample) &&
                (sched_end_minor >= num_completed_grow_steps_per_sample)) {
                return ((num_completed_grow_steps_per_sample %
                         sched_lag_minor) == 0);
            }
        }

        return uFALSE;
    }

    /**
     * Trial runner interface
     * @param max_grow_steps - max grow step count
     * @return first landmark grow step count
     */
    inline uUInt tr_find_first(const uUInt max_grow_steps) const {
        // Trial runner expects at least 2 steps
        return std::max(U_TO_UINT(2),
                        std::min(sched_lag_major, sched_beg_minor));
    }

    /**
     * Trial runner interface
     * @param completed_grow_steps - number of grow steps already completed
     * @param max_grow_steps - maximum number of grow steps allowed
     * @return next landmark grow step count
     */
    inline uUInt tr_find_next(const uUInt completed_grow_steps,
                              const uUInt max_grow_steps) const {
        const uUInt next_grow_steps_major =
            U_NEXT_MULTIPLE(completed_grow_steps, sched_lag_major);
        uUInt next_grow_steps_minor = max_grow_steps;
        if ((completed_grow_steps <= sched_end_minor) &&
            (next_grow_steps_major >= sched_beg_minor)) {
            next_grow_steps_minor = std::max(
                sched_beg_minor,
                U_NEXT_MULTIPLE(completed_grow_steps, sched_lag_minor));
        }
        return std::min(next_grow_steps_major, next_grow_steps_minor);
    }

private:
    /**
     * Lag values: a check point is encountered based on multiples of these
     * values. For example, if the active lag value is 1, then a check point
     * occurs after every grow step.
     */

    /**
     * Major lag is used when not growing near the distal interaction fragment
     */
    uUInt sched_lag_major;

    /**
     * Minor lag is used when growing near the distal interaction fragment
     */
    uUInt sched_lag_minor;

    /**
     * [begin, end] intervals for minor lag usage
     */
    uUInt sched_beg_minor;
    uUInt sched_end_minor;
};

#endif  // uSisSkiMixedLagScheduleMixin_h
