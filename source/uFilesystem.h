//****************************************************************************
// uFilesystem.h
//****************************************************************************

/**
 * @brief Wraps boost or std filesystem library
 */

#ifndef uFilesystem_h
#define uFilesystem_h

#include "uBuild.h"

#include <string>

/**
 * Defer to boost filesystem, this may later be swapped to std namespace
 */
#include <boost/filesystem.hpp>
#define uFs boost::filesystem

/**
 * Creates any directories needed for parameter path
 * @param dir_path - path to directory
 * @return result of uFs::create_directories(...) for dir_path
 */
extern bool uFs_create_dirs(const uFs::path& dir_path);

/**
 * Creates any parent directories needed for parameter file path
 * @param file_path - path to file (not path to directory)
 * @return result of uFs::create_directories(...) for parent directories
 */
extern bool uFs_create_parent_dirs(const uFs::path& file_path);

/**
 * Creates lexically normal (removes extra path separators and dots) directory
 * path without trailing path separator slash(es)
 * @param dir_path - directory path to normalize
 */
extern void uFs_normalize_dir_path(uFs::path& dir_path);
extern void uFs_normalize_dir_path(std::string& dir_path);

#endif  // uFilesystem_h
