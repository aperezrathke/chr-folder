//****************************************************************************
// uSisResamplingQcSimLevelMixin.h
//****************************************************************************

/**
 * QcMixin = A mixin that performs Quality Control to minimize distance
 *  between target and trial sampling distributions. Examples of algorithms
 *  intended to achieve this include Rejection Control and Resampling. See:
 *
 * Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science
 *   & Business Media, 2008.
 *
 * This is a mixin that performs resampling.
 */

#ifndef uSisResamplingQcSimLevelMixin_h
#define uSisResamplingQcSimLevelMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uRand.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * This class defines on_grow_phase_finish() to perform resampling of the
 * current set of samples. Samples that are chosen have their weight
 * adjusted by the probability of selection.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // Determines schedule of when to run resampling
    typename t_Sched,
    // Determines probabilities for selecting each sample
    typename t_Sampler,
    // Set to true to enable console logging of checkpoints
    bool rc_heartbeat_>
class uSisResamplingQcSimLevelMixin : public t_Sched, public t_Sampler {
private:
    /**
     * Scheduling type
     */
    typedef t_Sched uRsSched_t;

    /**
     * Sample selector
     */
    typedef t_Sampler uRsSampler_t;

    // Flip switch to enable logging whenever a checkpoint is reached
    enum { rc_heartbeat = rc_heartbeat_ };

public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Clear any previous state
        this->clear();

        // Allocate exponential weights buffer
        if (uRsSched_t::calculates_norm_exp_weights ||
            uRsSampler_t::needs_norm_exp_weights) {
            uAssert(sim.get_target_total_num_samples() > 0);
            this->norm_exp_weights.zeros(sim.get_target_total_num_samples());
        }

        // Initialize schedule
        this->uRsSched_t::init(sim, config, false /*is_tr*/ U_THREAD_ID_ARG);

        // Initialize sampler
        this->uRsSampler_t::init(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // (Re)-allocate exponential weights buffer
        if (uRsSched_t::calculates_norm_exp_weights ||
            uRsSampler_t::needs_norm_exp_weights) {
            uAssert(sim.get_target_total_num_samples() > 0);
            this->norm_exp_weights.zeros(sim.get_target_total_num_samples());
        }

        // Initialize schedule
        this->uRsSched_t::init_light(
            sim, config, false /*is_tr*/ U_THREAD_ID_ARG);

        // Initialize sampler
        this->uRsSampler_t::init_light(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Resets to default state
     */
    void clear() {
        this->norm_exp_weights.clear();
        this->uRsSched_t::clear();
        this->uRsSampler_t::clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Re-size exponential weights buffer
        uAssert(sim.get_max_total_num_nodes() > 0);
        this->norm_exp_weights.zeros(sim.get_target_trial_num_samples());

        // Warning: these probably should not require a reset!
        this->uRsSched_t::reset_for_next_trial(sim, config U_THREAD_ID_ARG);
        this->uRsSampler_t::reset_for_next_trial(sim, config U_THREAD_ID_ARG);
    }

    /**
     * Callback for when all samples have finished a single grow phase
     * @param status - The current status of each trial sample - 1 means
     *  chain is still growing, 0 means chain is no longer growing. May be
     *  modified by this function call.
     * @param log_weights - The current log weights of each sample. May be
     *  modified by this function call.
     * @param samples - the current set of trial samples. May be modified by
     *  this function call.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     */
    inline void on_grow_phase_finish(
        uWrappedBitset& status,
        uVecCol& log_weights,
        std::vector<sample_t>& samples,
        const uUInt num_completed_grow_steps_per_sample,
        sim_t& sim U_THREAD_ID_PARAM) {
        // Early out if not enough samples are present
        if (samples.size() <= 1) {
            return;
        }

        // Should not be called if all samples are dead
        uAssert(!uSisUtilsQc::are_all_samples_dead(samples));

        // Should only be called after at least one sample_t::grow_*()
        uAssert(num_completed_grow_steps_per_sample > 1);

        // Verify parallel data structures
        uAssertPosEq(status.size(), log_weights.size());
        uAssert(status.size() == samples.size());
        if (uRsSched_t::calculates_norm_exp_weights ||
            uRsSampler_t::needs_norm_exp_weights) {
            uAssert(status.size() == this->norm_exp_weights.size());
        }

        // See if we've reached a checkpoint
        uReal max_log_weight = U_TO_REAL(0.0);
        if (this->uRsSched_t::qc_checkpoint_reached(
                this->norm_exp_weights,
                max_log_weight,
                log_weights,
                num_completed_grow_steps_per_sample,
                *this,
                sim)) {
            if (rc_heartbeat) {
                uLogf("-Resampling checkpoint reached at grow step %u.\n",
                      (unsigned int)num_completed_grow_steps_per_sample);
            }

            U_SCOPED_INTERVAL_LOGGER(uSTAT_QcCheckpointInterval);

            // Determine exponential weights if needed
            if (!this->uRsSched_t::calculates_norm_exp_weights &&
                this->uRsSampler_t::needs_norm_exp_weights) {
                uSisUtilsQc::calc_norm_exp_weights(
                    this->norm_exp_weights, max_log_weight, log_weights);
            }

            // Resample!
            this->uRsSampler_t::qc_resample(status,
                                            log_weights,
                                            samples,
                                            norm_exp_weights,
                                            max_log_weight,
                                            num_completed_grow_steps_per_sample,
                                            *this,
                                            sim U_THREAD_ID_ARG);
        }
    }

    U_SIS_DECLARE_MIXIN_NULL_TRIAL_FINISH_INTERFACE

    U_SIS_DECLARE_MIXIN_NULL_RUN_FINISH_INTERFACE

private:
    /**
     * Internal buffer of exponential weights (derived from log_weights).
     * These weights are normalized relative to a maximum log weight.
     */
    uVecCol norm_exp_weights;
};

#endif  // uSisResamplingQcSimLevelMixin_h
