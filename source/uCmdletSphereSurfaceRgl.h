//****************************************************************************
// uCmdletSphereSurfaceRgl.h
//****************************************************************************

/**
 * Commandlet for generating an Rgl script to render sphere surface points
 *
 * Usage:
 * -cmdlet_sphere_surface_rgl [--num_node_sample_points <uint>]
 * [--node_collision_diameter <uint>] [--conf <fpath>] > rgl_sphere_test.R
 *
 * This pipes the commandlet stdout to a runnable R script.
 */

#ifdef uCmdletSphereSurfaceRgl_h
#   error "Commandlet Sphere Surface Rgl included multiple times!"
#endif  // uCmdletSphereSurfaceRgl_h
#define uCmdletSphereSurfaceRgl_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uSphereSurfacePointSampler.h"

#include "uAssert.h"
#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uStringUtils.h"
#include "uTypes.h"

// Should be C++11 lambda but not all build systems support C++11
// Convert coordinates to csv lists
inline std::string uCmdletSphereSurfaceRgl_to_csv(
    const uUInt num_unit_sphere_sample_points,
    const uReal* vec) {
    uAssert(vec);
    std::string out_csv(u2Str(vec[0]));
    for (uUInt i = 1; i < num_unit_sphere_sample_points; ++i) {
        out_csv += ", " + u2Str(vec[i]);
    }
    return out_csv;
};

/**
 * Entry point for commandlet
 */
int uCmdletSphereSurfaceRglMain(const uCmdOptsMap& cmd_opts) {
    // Defaults
    const uReal DEFAULT_SPHERE_SCALE = U_TO_REAL(110.0);
    const uUInt DEFAULT_NUM_UNIT_SPHERE_SAMPLE_POINTS = 64;

    // Read simulation configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);

    // Determine amount to scale sphere
    uReal max_node_diameter = DEFAULT_SPHERE_SCALE;
    config->read_into(max_node_diameter, uOpt_max_node_diameter);
    const uReal sphere_scale = (max_node_diameter > U_TO_REAL(0.0))
                                   ? max_node_diameter
                                   : DEFAULT_SPHERE_SCALE;

    // Determine number of points to sample on sphere surface
    const uUInt num_unit_sphere_sample_points =
        (config->num_unit_sphere_sample_points > 0)
            ? config->num_unit_sphere_sample_points
            : DEFAULT_NUM_UNIT_SPHERE_SAMPLE_POINTS;

    // Compute surface points
    uMatrix points;
    uSphereSurfacePointSampler::get_points(points,
                                           num_unit_sphere_sample_points);

    // Scale points
    points *= DEFAULT_SPHERE_SCALE;

    const std::string x(uCmdletSphereSurfaceRgl_to_csv(
        num_unit_sphere_sample_points, points.colptr(uDim_X)));
    const std::string y(uCmdletSphereSurfaceRgl_to_csv(
        num_unit_sphere_sample_points, points.colptr(uDim_Y)));
    const std::string z(uCmdletSphereSurfaceRgl_to_csv(
        num_unit_sphere_sample_points, points.colptr(uDim_Z)));

    // Output to console
    printf("\n");
    printf("library(rgl)\n");
    printf("x = c(%s)\n", x.c_str());
    printf("y = c(%s)\n", y.c_str());
    printf("z = c(%s)\n", z.c_str());
    printf("plot3d(x=x, y=y, z=z, col=rainbow(%u))\n",
           (unsigned int)num_unit_sphere_sample_points);
    printf("\n");

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_COMMANDLETS
