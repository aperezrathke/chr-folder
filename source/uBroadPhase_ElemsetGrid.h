//****************************************************************************
// uBroadPhase_ElemsetGrid.h
//****************************************************************************

/**
 * Collision detection is divided into broad and narrow phases. The purpose of
 * the broad phase is to perform efficient ruling out of large portions of the
 * potential collidable objects. Those objects which are not filtered via the
 * broad phase must then undergo detailed collision checking in the narrow
 * phase.
 *
 * The data structure here is a 3-D regular grid. All grid cells are of the
 * same size and are cubic.
 *
 * This specific implementation uses a hash table from a hashed 3D position to
 * a set of generic ids. The set of ids defines which element volumes overlap
 * the grid cell volume. Broad phase must filter by finding which grid cell
 * volumes are intersected and return the corresponding element ids within
 * those cells.
 */

#ifndef uBroadPhase_ElemsetGrid_h
#define uBroadPhase_ElemsetGrid_h

#include "uBuild.h"
#include "uAssert.h"
#include "uExitCodes.h"
#include "uGridUtils.h"
#include "uHashMap.h"
#include "uLogf.h"
#include "uStats.h"
#include "uTypeTraits.h"
#include "uTypes.h"

#include <limits>
#include <vector>

#ifdef U_BUILD_ENABLE_ASSERT
// Include std::count() for verifying element sets only contain unique entries
#include <algorithm>
#endif  // U_BUILD_ENABLE_ASSERT

namespace uBroadPhase {
/**
 * A uniform grid implementation that needs to know the cell length and the
 * grid radius. Makes *no* assumptions that elements are homogeneous.
 */
template <typename elem_id_t_>
class ElemsetGrid {
public:
    /**
     * The type used for identifying an element within a grid cell
     * eg. int, std::pair<int, int>, etc.
     */
    typedef elem_id_t_ elem_id_t;

    /**
     * Our set type - a set is the collection of element identifiers
     * at a single grid cell (voxel)
     */
    typedef std::vector<elem_id_t> elem_set_t;

    /**
     * Default constructor
     */
    ElemsetGrid() {
        // Initialize hash table with empty and deleted placeholder keys
        U_HASH_SET_EMPTY_KEY(m_pos_3d_to_elem_ids, U_GRID_EMPTY_KEY);
        U_HASH_SET_DELETED_KEY(m_pos_3d_to_elem_ids, U_GRID_DELETED_KEY);
    }

    /**
     * Initializes grid structure centered at origin for a cubic
     * volume defined by [-max_grid_dim, max_grid_dim] along x, y, z.
     * @param max_num_elem - used for asserting if elem_id_t can store
     *  this many elements.
     * @param cell_length - this is typically set to the max element diameter,
     *  defines the length of a grid cell cube. Not stored and must be
     *  passed in every time due to memory efficiency concerns (i.e. -
     *  storing a single copy of this value at simulation level rather
     *  than redundant copies at each sample)
     * @param grid_radius - the maximum positive coordinate along any
     *  axis
     */
    void init(const uUInt max_num_elem,
              const uReal cell_length,
              const uReal grid_radius) {
        // If this assert trips, elem_id_t is too small for the number of
        // elements. Increase sizeof(elem_id_t).
        const bool elem_id_has_enough_bits = (max_num_elem <= U_UELEMID_MAX);
        uAssert(elem_id_has_enough_bits);
        if (!elem_id_has_enough_bits) {
            uLogf(
                "Error: Spatial acceleration data structure requires more bits "
                "for element identifier type. Exiting.\n");
            exit(uExitCode_error);
        }
        // If this assert trips, we need to increase sizeof(grid_t::key_type)
        const bool key_has_enough_bits =
            ((2 * grid_radius) / cell_length) <
            static_cast<double>(
                (uGridKey_t(1) << U_TO_GRID_SHIFT_T(uGridUtils::yBitShift)));
        uAssert(key_has_enough_bits);
        if (!key_has_enough_bits) {
            uLogf(
                "Error: Spatial acceleration data structure requires more bits "
                "for key type. Exiting.\n");
            exit(uExitCode_error);
        }
        // The above asserts only make sense if yBitShift is median:
        uAssert((uGridUtils::xBitShift < uGridUtils::yBitShift) &&
                (uGridUtils::yBitShift < uGridUtils::zBitShift));
        this->clear();
        // @TODO - consider U_HASH_RESERVE heuristic to avoid rehash()
        // Note: max_grid_dim, max_elem_dim are unused since we are a
        // sparse implementation and that information is passed to us in
        // filter call.
    }

    /**
     * Resets to default uninitialized state
     */
    void clear() { m_pos_3d_to_elem_ids.clear(); }

    /**
     * Broad phase: Spatial subdivision acceleration to reduce growth
     *  constant in O(n^2) collisions.
     * @param out_results - bit set of node ids that need further narrow
     *  phase consideration
     * @param elem_center - centroid of element
     * @param elem_radius - 1/2 side length of element's bounding cube
     * @param cell_length - this is typically set to the max element diameter,
     *  defines the length of a grid cell cube. Not stored and must be
     *  passed in every time due to memory efficiency concerns (i.e. -
     *  storing a single copy of this value at simulation level rather
     *  than redundant copies at each sample)
     * @param grid_radius - the max positive grid dimension
     */
    template <typename t_bitset>
    inline void filter(t_bitset& out_results,
                       const uReal* elem_center,
                       const uReal elem_radius,
                       const uReal cell_length,
                       const uReal grid_radius) const {
        U_SCOPED_STAT_TIMER(uSTAT_CollisionBroadPhaseTime);

        // Reset results
        out_results.reset();

        // Determine which grid cells elemental volume intersects
        uGridKey_t extents[uGridUtils::NumExtents];
        uGridUtils::quantize(
            elem_center, elem_radius, cell_length, grid_radius, &(extents[0]));

        // See if there are any elements in the overlapping grid cells
        uGridKey_t x, y, z;
        uGridKey_t key, key_base;
        for (x = extents[uGridUtils::ixMinX]; x <= extents[uGridUtils::ixMaxX];
             ++x) {
            for (y = extents[uGridUtils::ixMinY];
                 y <= extents[uGridUtils::ixMaxY];
                 ++y) {
                key_base = U_GRID_ENCODE_X_Y(x, y);
                for (z = extents[uGridUtils::ixMinZ];
                     z <= extents[uGridUtils::ixMaxZ];
                     ++z) {
                    key = U_GRID_ENCODE_Z(key_base, z);
                    uAssert(uGridUtils::encode(x, y, z) == key);
                    typename grid_t::const_iterator cell_itr(
                        m_pos_3d_to_elem_ids.find(key));
                    if (cell_itr != m_pos_3d_to_elem_ids.end()) {
                        // perform set union
                        const size_t n = cell_itr->second.size();
                        for (size_t i = 0; i < n; ++i) {
                            uAssertBounds(
                                cell_itr->second[i], 0, out_results.size());
                            out_results.set(cell_itr->second[i], uTRUE);
                        }  // end set union of element ids for narrow phase
                           // checking
                    }      // end check if voxel is empty
                }          // end iteration over z
            }              // end iteration over y
        }                  // end iteration over x
    }

    /**
     * Adds a new element to the broad phase collision state.
     * @param elem_id -  a unique element identifier
     * @param elem_center - 3-D position of element centroid
     * @param elem_radius - 1/2 side length of element's bounding cube
     * @param cell_length - this is typically set to the max element diameter,
     *  defines the length of a grid cell cube. Not stored and must be
     *  passed in every time due to memory efficiency concerns (i.e. -
     *  storing a single copy of this value at simulation level rather
     *  than redundant copies at each sample)
     * @param grid_radius - the max positive grid dimension
     */
    inline void add(const elem_id_t elem_id,
                    const uReal* elem_center,
                    const uReal elem_radius,
                    const uReal cell_length,
                    const uReal grid_radius) {
        U_SCOPED_STAT_TIMER(uSTAT_CollisionBroadPhaseTime);

        // Determine which grid cells elemental volume intersects
        uGridKey_t extents[uGridUtils::NumExtents];
        uGridUtils::quantize(
            elem_center, elem_radius, cell_length, grid_radius, &(extents[0]));

        // Add element to each overlapping grid cell
        uGridKey_t x, y, z;
        uGridKey_t key, key_base;
        for (x = extents[uGridUtils::ixMinX]; x <= extents[uGridUtils::ixMaxX];
             ++x) {
            for (y = extents[uGridUtils::ixMinY];
                 y <= extents[uGridUtils::ixMaxY];
                 ++y) {
                key_base = U_GRID_ENCODE_X_Y(x, y);
                for (z = extents[uGridUtils::ixMinZ];
                     z <= extents[uGridUtils::ixMaxZ];
                     ++z) {
                    key = U_GRID_ENCODE_Z(key_base, z);
                    uAssert(uGridUtils::encode(x, y, z) == key);
                    m_pos_3d_to_elem_ids[key].push_back(elem_id);
                    // Verify element is unique
                    uAssert(std::count(m_pos_3d_to_elem_ids[key].begin(),
                                       m_pos_3d_to_elem_ids[key].end(),
                                       elem_id) == 1);
                }  // end iteration over z
            }      // end iteration over y
        }          // end iteration over x
    }              // end function add()

private:
    /**
     * Mapping from 3-D position hash to set of element ids within that voxel
     */
    typedef U_HASH_MAP<uGridKey_t, elem_set_t> grid_t;
    grid_t m_pos_3d_to_elem_ids;
};

}  // namespace uBroadPhase

#endif  // uBroadPhase_ElemsetGrid_h
