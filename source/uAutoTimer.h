//****************************************************************************
// uAutoTimer.h
//****************************************************************************

/**
 * @brief Portable utility meant to mimic boost::timer::auto_cpu_timer
 */

#ifndef uAutoTimer_h
#define uAutoTimer_h

#include "uBuild.h"
#include "uLogf.h"
#include "uTime.h"

/**
 * Simple timer which reports elapsed duration between creation and destruction
 */
class uAutoTimer {
public:
    /**
     * Default constructor, initialize creation time
     */
    uAutoTimer()
        : m_start_secs_wall(u_time_secs_wall()),
          m_start_secs_cpu(u_time_secs_cpu()) {}

    /**
     * Destructor reports lifetime of timer in seconds
     */
    ~uAutoTimer() {
        uLogf("%fs wall, %fs CPU\n",
              u_time_secs_wall() - m_start_secs_wall,
              u_time_secs_cpu() - m_start_secs_cpu);
    }

private:
    /**
     * Creation times
     */
    double m_start_secs_wall;
    double m_start_secs_cpu;
};

#ifdef U_BUILD_ENABLE_INTERVAL_LOGGING

/**
 * Macro for creating an auto-timer
 */
#define U_DECLARE_AUTO_TIMER(name_) uAutoTimer auto_timer_##name_

#else

/**
 * Disable macro for creating an auto-timer
 */
#define U_DECLARE_AUTO_TIMER(name_)

#endif  // U_BUILD_ENABLE_INTERVAL_LOGGING

#endif  // uAutoTimer_h
