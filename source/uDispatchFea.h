//****************************************************************************
// uDispatchFea.h
//****************************************************************************

/**
 * Dispatches to user-generated fea models
 */

#ifndef uDispatchFea_h
#define uDispatchFea_h

#include "uBuild.h"
#include "uCanvasFea.h"
#include "uCmdOptsMap.h"
#include "uLogf.h"
#include "uOpts.h"

//****************************************************************************
// Main
//****************************************************************************

/**
 * The fea dispatcher! Usage:
 *
 *  --fea_dispatch <cmdkey>
 */
class uDispatchFea {
public:
    /**
     * Entry point for app
     */
    static int main(const uCmdOptsMap& cmd_opts) {
        std::string cmd_key;
        cmd_opts.read_into(cmd_key, uOpt_get_cmd_switch(uOpt_fea_dispatch));
        uLogf("Running fea dispatcher for: %s\n", cmd_key.c_str());
        return uSisFeaApps::dispatch(cmd_opts, cmd_key);
    }

private:
    // Disallow construction and assignment
    uDispatchFea();
    uDispatchFea(const uDispatchFea&);
    uDispatchFea& operator=(const uDispatchFea&);
};

#endif  // uDispatchFea_h
