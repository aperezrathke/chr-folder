//****************************************************************************
// uSse.h
//****************************************************************************

/**
 * Platform independent include for SSE intrinsics
 */

#ifndef uSse_h
#define uSse_h

//****************************************************************************
// Headers
//****************************************************************************

#include "uBuild.h"
#include "uIntrin.h"

//****************************************************************************
// Macros
//****************************************************************************

// In order to use _mm256_load_pd intrinsic, double * must be 32 byte aligned
#define U_SSE_ALIGNMENT_256_PD 32

// Number of dimensions in an SSE quadword
#define U_SSE_VEC_DIM 4

// Number of bytes defining an element of an SSE quadword
#define U_SSE_ELEM_BYTES 8

// @TODO - add macros for accessing elements of SSE vectors as Microsoft and
// GCC do it differently. In GCC, a vector is simply an array whereas in
// Microsoft, you have to access a separate member variable.

#endif  // uSse_h
