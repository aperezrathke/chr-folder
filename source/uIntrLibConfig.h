//****************************************************************************
// uIntrLibConfig.h
//****************************************************************************

/**
 * Core utilities for parsing and loading proximity constraint configurations
 */

#ifndef uIntrLibConfig_h
#define uIntrLibConfig_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uTypes.h"

//****************************************************************************
// uIntrLibConfig
//****************************************************************************

/**
 * Utilities for querying and loading user-specified proximity configuration
 */
namespace uIntrLibConfig {

/**
 * Loads plain-text index vector file from disk. The expected format is
 *  whitespace or CSV delimited
 * @param out - Output index vector, always cleared even if no load occurs
 * @param config - User configuration
 * @param key - Option key for file path to index file
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index data
 * @return uTRUE if successfully loaded, uFALSE o/w
 */
extern uBool load_index(uUIVecCol& out,
                        uSpConstConfig_t config,
                        const uOptE key,
                        const uBool no_fail,
                        const char* data_name);

/**
 * Loads plain-text index pairs file from disk. The expected format is:
 *
 *      <first_index><sep><second_index>
 *      ...
 *      <first_index><sep><second_index>
 *
 *  where <*index> is a 0-based integer index. The <sep> field separates
 *  integer indices and can be any length set of consecutive non-numeric
 *  characters. Therefore, <sep> formats such as comma-separated or white
 *  space delimited are both acceptable. Here is an example file in white
 *  space delimited format:
 *
 *          <line 0>: 0 4
 *          <line 1>: 5 8
 *          <line 2>: 9 12
 *          <line 3>: 13 13
 *
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class, always cleared even if no load occurs
 * @param b_order - If 'b_order' is TRUE, then the smaller index in pair is
 *  stored in row defined by enum uIntrLibPairIxMin and the larger index is
 *  stored in row defined by enum uIntrLibPairIxMax. If 'b_order' is FALSE,
 *  the first index read in pair is in row enum uIntrLibPairIxMin and second
 *  index read is in row enum uIntrLibPairIxMax.
 * @param config - User configuration
 * @param key - Option key for file path to index pairs
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index pair data
 * @return uTRUE if matrix successfully loaded, uFALSE o/w
 */
extern uBool load_index_pairs(uUIMatrix& out_pairs,
                              const uBool b_order,
                              uSpConstConfig_t config,
                              const uOptE key,
                              const uBool no_fail,
                              const char* data_name);

/**
 * Loads ordered, chromatin fragments from disk but defaults to individual
 *  monomers if no file loaded and no_fail is FALSE. File format is same as
 *  load_index_pairs().
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class. If no fragments file found, then output matrix will be
 *  2 x num_mon and each column will store the 0-based column index.
 * @param config - User configuration
 * @param key - Option key for file path to index pairs
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index pair data
 * @param num_mon - Number of chromatin monomers (nodes) present in simulation
 */
extern void load_frag(uUIMatrix& out_pairs,
                      uSpConstConfig_t config,
                      const uOptE key,
                      const uBool no_fail,
                      const char* data_name,
                      const uUInt num_mon);

/**
 * Initializes vector of distance constraint
 * @param out_dist - Output vector of distances
 * @WARNING - out_dist is always cleared, even if no data loaded!
 * @param num - Target size of 'out_dist'
 * @param config - User configuration
 * @param opt_fpath - Option key to RLE distances files
 * @param opt_scalar - Option key for scalar distance
 * @param default_scalar - Default scalar distance
 * @param data_name - Used for logging events involving distance data
 */
extern void load_dist(uVecCol& out_dist,
                      const uMatSz_t num,
                      uSpConstConfig_t config,
                      const enum uOptE opt_fpath,
                      const enum uOptE opt_scalar,
                      const uReal default_scalar,
                      const char* data_name);

/**
 * Initializes scalar distance constraint
 * @param config - User configuration
 * @param opt_scalar - Option key for scalar distance
 * @param default_scalar - Default scalar distance
 * @param data_name - Used for logging events involving distance data
 * @return User scalar distance value or default value if not specified
 */
extern uReal load_dist(uSpConstConfig_t config,
                       const enum uOptE opt_scalar,
                       const uReal default_scalar,
                       const char* data_name);

/**
 * Load mask from command line or file. A bit mask is a 0|1 string (all
 * characters not 0|1 are ignored). An index mask is a delimited set of
 * integer indices; the delimiter is any series of non-integer characters. The
 * priority for which mask is used when multiple masks are specified is as
 * follows:
 *
 *  bit_mask (cmd) > bit_mask_fpath (cmd) > index_mask (cmd)
 *      > index_mask_fpath (cmd) > bit_mask (INI) > bit_mask_fpath (INI)
 *          > index_mask (INI) > index_mask_fpath (INI)
 *
 * @param mask - Output boolean mask
 * @WARNING, mask always reset even if not loaded
 * @param num - Size of mask (mask will be sized to this value)
 * @param config - User configuration
 * @param min_mask_opt -Start mask user option
 * @WARNING, ASSUMES THE FOLLOWING:
 *  Will attempt to load a user mask according to the following priority:
 *  min_mask_opt + 0 = bit mask string
 *  min_mask_opt + 1 = path to external bit mask file
 *  min_mask_opt + 2 = index mask string
 *  min_mask_opt + 3 = path to external index mask file
 * @param data_name - Used for logging events involving mask data
 */
extern void load_mask(uBoolVecCol& mask,
                      const uMatSz_t num,
                      uSpConstConfig_t config,
                      const uOptE min_mask_opt,
                      const char* data_name);

/**
 * Initializes maximum number of interaction constraints that can fail. Note,
 *  user inputs a real-valued scalar option in [0,1] that represents the
 *  maximum proportion of constraints that may fail; in constrast, this method
 *  outputs an integral value in [0, 'num'].
 * @WARNING, THIS METHOD SHOULD BE CALLED AFTER THE RELEVANT CONSTRAINT SET
 *  HAS BEEN INITIALIZED AND FILTERED (i.e. after masking has been applied)!
 * @param max_fail - Output target, maximum number of constraints that are
 *  allowed to fail before sample is culled; output will always be integer in
 *  range [0, 'num'].
 * @param config - User configuration
 * @param fail_budg_opt - Option key for failure budget
 * @param default_fail_budg_frac - Default failure budget fraction in [0,1]
 * @param const uUInt num - Max number of constraints
 * @param data_name - Used for logging events
 */
extern void load_fail_budg(uUInt& max_fail,
                           uSpConstConfig_t config,
                           const uOptE fail_budg_opt,
                           const uReal default_fail_budg_frac,
                           const uUInt num,
                           const char* data_name);

}  // namespace uIntrLibConfig

#endif  // uIntrLibConfig_h
