//****************************************************************************
// uSisMkiMkoSeoMlocBudgIntrChrMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Mki: Multiple knock-in
 * Mko: Multiple knock-out
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Mloc: Multiple loci
 * Budg: Interaction constraint failures allowed up to budget
 *
 * Defines a budgeted chrome-to-chrome interaction mixin supporting knock-in
 * and knock-out constraints over multiple (or single) loci
 *
 * An interaction mixin manages the details for checking candidate nodes to
 * see if they satisfy the user-specified node contact constraints (knock-in
 * and/or knock-out).
 *
 * An accessory structure is the simulation level interaction mixin which
 * defines any global data and shared utilities among all samples.
 */

#ifndef uSisMkiMkoSeoMlocBudgIntrChrMixin_h
#define uSisMkiMkoSeoMlocBudgIntrChrMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibBudgUtil.h"
#include "uSisIntrChrBudgUtil.h"
#include "uSisNullIntrChrSeedPolicy.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisMkiMkoSeoMlocBudgIntrChrSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Budgeted chr-chr interaction sample mixin
 */
template <typename t_SisGlue>
class uSisMkiMkoSeoMlocBudgIntrChrMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Chr-chr knock-in budget access policy
     */
    typedef uSisIntrChrBudgAccKin<glue_t> intr_chr_budg_acc_kin_t;

    /**
     * Chr-chr knock-out budget access policy
     */
    typedef uSisIntrChrBudgAccKo<glue_t> intr_chr_budg_acc_ko_t;

    /**
     * Batch interactions utility
     */
    typedef uSisIntrChrCommonBatch<t_SisGlue> intr_chr_batch_util_t;

    /**
     * Chr-chr interactions seed policy for first node
     */
    typedef uSisNullIntrChrSeedPolicy_t intr_chr_seed_first_policy_t;

    /**
     * Chr-chr interactions seed policy after first node placed
     */
    typedef struct {
        /**
         * Chr-chr interactions should be updated
         */
        enum { should_update = true };

        /**
         * @param sample - Sample being seeded
         * @param nfo - Common seed information
         * @param sim - Outer simulation
         * @return uTRUE if seed can satisfy interactions, uFALSE o/w
         */
        static uBool is_seed_okay(sample_t& sample,
                                  const uSisUtils::seed_info_t& nfo,
                                  const sim_t& sim U_THREAD_ID_PARAM) {
            // Acquire TLS scratch buffers
            uSisIntrChrBudgFailPolicyArgs_t policy_args =
                sim.get_intr_chr_budg_fail_policy_args(U_THREAD_ID_0_ARG);
            // @TODO - consider only resetting seed candidate slot
            sample.intr_chr_reset_tls(policy_args, sim);
            uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                          U_TO_UINT(0),
                          U_TO_UINT(policy_args.kin_delta_fails.n_elem));
            uAssertBounds(U_SIS_SEED_CANDIDATE_ID,
                          U_TO_UINT(0),
                          U_TO_UINT(policy_args.ko_delta_fails.n_elem));
            // Test if interactions can be satisfied
            return intr_chr_batch_util_t::is_seed_okay(
                sample,
                &policy_args,
                sample.m_intr_chr_loci_tables,
                nfo,
                sample,
                sim U_THREAD_ID_ARG);
        }
    } intr_chr_seed_next_policy_t;

    /**
     * Default constructor
     */
    uSisMkiMkoSeoMlocBudgIntrChrMixin() { this->clear(); }

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        m_intr_chr_loci_tables = sim.get_intr_chr_loci_tables();
        // Assume clear() has initialized failure counts
        uAssert(m_intr_chr_kin_num_fail == U_TO_UINT(0));
        uAssert(m_intr_chr_ko_num_fail == U_TO_UINT(0));
    }

    /**
     * Resets to default state
     */
    void clear() {
        intr_chr_batch_util_t::clear(m_intr_chr_loci_tables);
        m_intr_chr_kin_num_fail = U_TO_UINT(0);
        m_intr_chr_ko_num_fail = U_TO_UINT(0);
    }

    /**
     * Determines which positions satisfy interaction constraints
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_chr_filter(uBoolVecCol& out_legal_candidates_primed,
                         const uMatrix& candidate_centers,
                         const uReal candidate_radius,
                         const uUInt parent_node_id,
                         const sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) const {
        // Acquire TLS scratch buffers
        uSisIntrChrBudgFailPolicyArgs_t policy_args =
            sim.get_intr_chr_budg_fail_policy_args(U_THREAD_ID_0_ARG);
        intr_chr_reset_tls(policy_args, sim);
        // Mark candidates that violate constraints
        intr_chr_batch_util_t::mark_viol(*this,
                                         &policy_args,
                                         out_legal_candidates_primed,
                                         m_intr_chr_loci_tables,
                                         candidate_centers,
                                         candidate_radius,
                                         parent_node_id,
                                         sample,
                                         sim);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_kin_on_fail(
        uSisIntrChrBudgFailPolicyArgs_t* const p_policy_args,
        const uUInt candidate_id,
        const uSisIntrChrFilterCore_t& c,
        const sample_t& sample,
        const sim_t& sim) const {
        uAssert(p_policy_args);
        return intr_chr_on_fail(p_policy_args->kin_delta_fails,
                                p_policy_args->kin_fail_table,
                                m_intr_chr_kin_num_fail,
                                sim.get_intr_chr_kin_max_fail(),
                                candidate_id,
                                c,
                                sample,
                                sim);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  chr-chr interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_chr_ko_on_fail(
        uSisIntrChrBudgFailPolicyArgs_t* const p_policy_args,
        const uUInt candidate_id,
        const uSisIntrChrFilterCore_t& c,
        const sample_t& sample,
        const sim_t& sim) const {
        uAssert(p_policy_args);
        return intr_chr_on_fail(p_policy_args->ko_delta_fails,
                                p_policy_args->ko_fail_table,
                                m_intr_chr_ko_num_fail,
                                sim.get_intr_chr_ko_max_fail(),
                                candidate_id,
                                c,
                                sample,
                                sim);
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - a unique chromatin node identifier
     * @param B - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param candidate_id - identifies candidate that was selected
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    void intr_chr_update(const uUInt node_id,
                         const uReal* B,
                         const uReal radius,
                         const uUInt candidate_id,
                         const sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        // Acquire TLS scratch buffers
        const uSisIntrChrBudgFailPolicyArgs_t policy_args =
            sim.get_intr_chr_budg_fail_policy_args(U_THREAD_ID_0_ARG);

        // Knock-in: Remove failed interactions prior to satisfaction testing
        // and update fail counts
        uAssertBounds(candidate_id,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.kin_fail_table.size()));
        uAssert(policy_args.kin_delta_fails.n_elem ==
                U_TO_MAT_SZ_T(policy_args.kin_fail_table.size()));
        uAssert(policy_args.kin_delta_fails.at(candidate_id) ==
                U_TO_UINT(policy_args.kin_fail_table[candidate_id].size()));
        intr_chr_update_fail(m_intr_chr_loci_tables.loci_kin_intra,
                             m_intr_chr_loci_tables.loci_kin_inter,
                             m_intr_chr_kin_num_fail,
                             sim.get_intr_chr_kin_max_fail(),
                             policy_args.kin_fail_table[candidate_id],
                             sample,
                             sim);
        uAssert(m_intr_chr_kin_num_fail <= sim.get_intr_chr_kin_max_fail());

        // Knock-out: Remove failed interactions prior to satisfaction testing
        // and update fail counts
        uAssertBounds(candidate_id,
                      U_TO_UINT(0),
                      U_TO_UINT(policy_args.ko_fail_table.size()));
        uAssert(policy_args.ko_delta_fails.n_elem ==
                U_TO_MAT_SZ_T(policy_args.ko_fail_table.size()));
        uAssert(policy_args.ko_delta_fails.at(candidate_id) ==
                U_TO_UINT(policy_args.ko_fail_table[candidate_id].size()));
        intr_chr_update_fail(m_intr_chr_loci_tables.loci_ko_intra,
                             m_intr_chr_loci_tables.loci_ko_inter,
                             m_intr_chr_ko_num_fail,
                             sim.get_intr_chr_ko_max_fail(),
                             policy_args.ko_fail_table[candidate_id],
                             sample,
                             sim);
        uAssert(m_intr_chr_ko_num_fail <= sim.get_intr_chr_ko_max_fail());

        // Remove satisfied interactions from active loci tables
        intr_chr_batch_util_t::remove_satisf(m_intr_chr_loci_tables,
                                             node_id,
                                             B,
                                             radius,
                                             candidate_id,
                                             sample,
                                             sim);
    }

    // Accessors

    /**
     * @return Chr-chr interaction seed policy for first seeded node
     */
    intr_chr_seed_first_policy_t get_intr_chr_seed_first_policy() {
        return intr_chr_seed_first_policy_t();
    }

    /**
     * @return Chr-chr interaction seed policy for nodes seeded after first
     */
    intr_chr_seed_next_policy_t get_intr_chr_seed_next_policy() {
        return intr_chr_seed_next_policy_t();
    }

private:
    /**
     * Utility resets TLS buffers prior to constraint checks
     */
    static void intr_chr_reset_tls(uSisIntrChrBudgFailPolicyArgs_t& policy_args,
                                   const sim_t& sim) {
        // Reset TLS scratch buffers
        policy_args.kin_delta_fails.zeros();
        policy_args.ko_delta_fails.zeros();
        // Using resize(0) instead of clear() to hopefully avoid freeing
        // backing store on older STL libraries (i.e. reduce allocations)
        // https://stackoverflow.com/questions/2738967/vector-clear-vs-resize
        const size_t num_candidates = policy_args.kin_delta_fails.n_elem;
        policy_args.kin_fail_table.resize(0);
        policy_args.kin_fail_table.resize(num_candidates);
        policy_args.ko_fail_table.resize(0);
        policy_args.ko_fail_table.resize(num_candidates);
        // Verify TLS buffer assumptions
        uAssertPosEq(U_TO_UINT(policy_args.kin_delta_fails.n_elem),
                     sim.get_num_unit_sphere_sample_points());
        uAssertPosEq(policy_args.kin_delta_fails.n_elem,
                     policy_args.ko_delta_fails.n_elem);
        uAssertPosEq(policy_args.kin_fail_table.size(),
                     policy_args.ko_fail_table.size());
        uAssertPosEq(policy_args.kin_delta_fails.n_elem,
                     U_TO_MAT_SZ_T(policy_args.kin_fail_table.size()));
    }

    /**
     * Implementation for when a candidate will never satisfy a chr-chr
     *  interaction
     * @param delta_fails - Buffer of incremental (delta) failure counts at
     *  each candidate position relative to current sample state, will
     *  be incremented at corresponding candidate
     * @param fail_table - Table of failed interactions at each candidate,
     *  if candidate is not culled, then will be updated with information to
     *  allow future removal of the failed interaction
     * @param num_fail - Current number of failed interactions at sample
     * @param max_fail - Maximum allowed number of failed interactions
     * @param candidate_id - Candidate which failed chr-chr interaction
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    static uBool intr_chr_on_fail(uUIVecCol& delta_fails,
                                  uSisIntrChrBudgFailTable_t& fail_table,
                                  const uUInt num_fail,
                                  const uUInt max_fail,
                                  const uUInt candidate_id,
                                  const uSisIntrChrFilterCore_t& c,
                                  const sample_t& sample,
                                  const sim_t& sim) {

        uAssertPosEq(U_TO_UINT(delta_fails.n_elem),
                     sim.get_num_unit_sphere_sample_points());
        uAssertBounds(
            candidate_id, U_TO_UINT(0), U_TO_UINT(delta_fails.n_elem));
        uAssert((delta_fails.at(candidate_id) + num_fail) <= max_fail);
        // Increment failure count
        ++(delta_fails.at(candidate_id));
        const uUInt cand_num_fail = delta_fails.at(candidate_id) + num_fail;
        if (cand_num_fail <= max_fail) {
            // Update failed interactions table
            uSisIntrChrBudgFail_t fail_nfo;
            fail_nfo.intr_id = c.intr_id;
            fail_nfo.frag_a_lid = sim.get_growth_lid(c.frag_a_nid_lo);
            uAssert(sim.get_growth_lid(c.frag_a_nid_lo) ==
                    sim.get_growth_lid(c.frag_a_nid_hi));
            fail_nfo.frag_b_lid = sim.get_growth_lid(c.frag_b_nid_lo);
            uAssert(sim.get_growth_lid(c.frag_b_nid_lo) ==
                    sim.get_growth_lid(c.frag_b_nid_hi));
            uAssertBounds(
                candidate_id, U_TO_UINT(0), U_TO_UINT(fail_table.size()));
            fail_table[candidate_id].push_back(fail_nfo);
            uAssertPosEq(delta_fails.at(candidate_id),
                         U_TO_UINT(fail_table[candidate_id].size()));
            // Retain candidate
            return uTRUE;
        }
        // Cull candidate
        return uFALSE;
    }

    /**
     * Removes failed chr-chr interactions according to parameter 'fail_list',
     *  will also update 'num_fail' count. Here 'interaction' refers to all
     *  interactions of same type such as 'knock-in' or 'knock-out' (but not
     *  both!)
     * @param loci_intra - Table of within-locus interactions
     * @param loci_inter - Table of between-locus interactions
     * @param num_fail - Updated total number of failed interactions
     * @param max_fail - Maximum allowed number of failed interactions
     * @param fail_list - List of failed interactions at chosen candidate
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    static void intr_chr_update_fail(uSisIntrLociTable_t& loci_intra,
                                     uSisIntrLociTable_t& loci_inter,
                                     uUInt& num_fail,
                                     const uUInt max_fail,
                                     const uSisIntrChrBudgFailList_t& fail_list,
                                     const sample_t& sample,
                                     const sim_t& sim) {

        // Update failure count
        const size_t delta_fail = fail_list.size();
        num_fail += U_TO_UINT(delta_fail);
        uAssert(num_fail <= max_fail);

        // Process each failed interaction
        for (size_t i = 0; i < delta_fail; ++i) {
            // Handle to failure info
            const uSisIntrChrBudgFail_t& fail_nfo = fail_list[i];
            if (sample_t::is_mloc_growth) {
                // CASE: MULTIPLE LOCUS GROWTH
                if (fail_nfo.frag_a_lid == fail_nfo.frag_b_lid) {
                    // CASE: WITHIN-LOCUS CHR-CHR INTERACTION
                    uAssertBounds(fail_nfo.frag_a_lid,
                                  U_TO_UINT(0),
                                  U_TO_UINT(loci_intra.size()));
                    uIntrLibBudgUtil::remove_fail(
                        loci_intra[fail_nfo.frag_a_lid], fail_nfo.intr_id);
                } else {
                    // CASE: BETWEEN-LOCUS CHR-CHR INTERACTION
                    uAssertBounds(fail_nfo.frag_a_lid,
                                  U_TO_UINT(0),
                                  U_TO_UINT(loci_inter.size()));
                    uIntrLibBudgUtil::remove_fail(
                        loci_inter[fail_nfo.frag_a_lid], fail_nfo.intr_id);
                    // Remove mirrored interaction
                    uAssertBounds(fail_nfo.frag_b_lid,
                                  U_TO_UINT(0),
                                  U_TO_UINT(loci_inter.size()));
                    uIntrLibBudgUtil::remove_fail(
                        loci_inter[fail_nfo.frag_b_lid], fail_nfo.intr_id);
                }

            } else {
                // CASE: SINGLE LOCUS GROWTH
                uAssert(fail_nfo.frag_a_lid == fail_nfo.frag_b_lid);
                uAssert(fail_nfo.frag_a_lid == U_TO_UINT(0));
                uAssert(loci_inter.empty());
                uAssert(loci_intra.size() == ((size_t)1));
                uIntrLibBudgUtil::remove_fail(loci_intra[0], fail_nfo.intr_id);
            }
        }
    }

    /**
     * Collection of active interactions partitioned by loci
     */
    uSisIntrChrLociTables_t m_intr_chr_loci_tables;

    /**
     * Number of failed chr-chr, knock-in interactions
     */
    uUInt m_intr_chr_kin_num_fail;

    /**
     * Number of failed chr-chr, knock-in interactions
     */
    uUInt m_intr_chr_ko_num_fail;
};

#endif  // uSisMkiMkoSeoMlocBudgIntrChrMixin_h
