//****************************************************************************
// uGridUtils.h
//****************************************************************************

/**
 * @brief Utilities common to various grid implementations
 */

#ifndef uGridUtils_h
#define uGridUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uHashMap.h"
#include "uTypeTraits.h"
#include "uTypes.h"

//****************************************************************************
// Key type
//****************************************************************************

/**
 * Key type defining bits allowed to encode a quantized (x,y,z) position
 */
#ifdef U_BUILD_ENABLE_BIG_GRID_COLLISION128
    #if BOOST_VERSION >= 105600
        #include <boost/multiprecision/cpp_int.hpp>
        typedef boost::multiprecision::uint128_t uGridKey_t;
        #define U_TO_GRID_SHIFT_T(x) static_cast<size_t>(x)
    #elif ((defined(U_BUILD_COMPILER_GCC) || defined(U_BUILD_COMPILER_ICC)) && \
       defined(U_BUILD_CXX_11))
        typedef __uint128_t uGridKey_t;
        #define U_TO_GRID_SHIFT_T(x) x
        // Custom specialization of std::hash
        #include <boost/functional/hash.hpp>
        namespace std {
        template <>
        struct hash<uGridKey_t> {
            typedef uGridKey_t argument_type;
            typedef std::size_t result_type;
            result_type operator()(const argument_type x) const noexcept {
                const char* first = (const char*)(&x);
                const char* last = first + sizeof(uGridKey_t);
                return boost::hash_range(first, last);
            }
        };
        }  // namespace std
    #else
        #error 128-bit integer not supported, consider upgrading BOOST >= 1.56
    #endif  // GCC or ICC
#elif defined(U_BUILD_ENABLE_BIG_GRID_COLLISION64)
    #include <stdint.h>
    typedef uint64_t uGridKey_t;
    #define U_TO_GRID_SHIFT_T(x) x
#else
    #include <stdint.h>
    typedef uint32_t uGridKey_t;
    #define U_TO_GRID_SHIFT_T(x) x
#endif  // U_BUILD_ENABLE_BIG_GRID_COLLISION64|128

//****************************************************************************
// Macros
//****************************************************************************

/**
 * elp - element point coordinate (can be off center)
 * elc - element center coordinate
 * elr - element radius
 * gr - grid radius
 * cl - length of single side of grid cell cube
 * Computes quantized cell min index
 */

/**
 * Computes quantized cell index
 */
#define U_DO_GRID_QUANTIZE(elp, gr, cl) \
    static_cast<uGridKey_t>((((elp) + (gr)) / (cl)))

#ifdef U_BUILD_ENABLE_ASSERT
/**
 * Provide a checked version of the quantization routine
 */
inline uGridKey_t U_GRID_QUANTIZE(const uReal elem_pos,
                                  const uReal grid_radius,
                                  const uReal cell_length) {
    uAssert(((elem_pos + grid_radius) / cell_length) >= U_TO_REAL(0.0));
    return U_DO_GRID_QUANTIZE(elem_pos, grid_radius, cell_length);
}
#else
#define U_GRID_QUANTIZE U_DO_GRID_QUANTIZE
#endif  // U_BUILD_ENABLE_ASSERT

/**
 * Computes quantized cell min index
 */
#define U_GRID_QUANTIZE_MIN(elc, elr, gr, cl) \
    U_GRID_QUANTIZE(((elc) - (elr)), (gr), (cl))

/**
 * Computes quantized cell max index
 */
#define U_GRID_QUANTIZE_MAX(elc, elr, gr, cl) \
    U_GRID_QUANTIZE(((elc) + (elr)), (gr), (cl))

/**
 * The default empty key if using google sparsehash
 */
#define U_GRID_EMPTY_KEY (std::numeric_limits<uGridKey_t>::max())

/**
 * The default deleted key if using google sparsehash
 */
#define U_GRID_DELETED_KEY (U_GRID_EMPTY_KEY - 1)

//****************************************************************************
// Utilities
//****************************************************************************

namespace uGridUtils {
/**
 * Assumed offsets into an extents array
 */
enum { ixMinX = 0, ixMaxX, ixMinY, ixMaxY, ixMinZ, ixMaxZ, NumExtents };

/**
 * Bit shifts for encoding 3-D cell coordinate as a single integer
 * Based on http://http.developer.nvidia.com/GPUGems3/gpugems3_ch32.html
 */
enum {
    xBitShift = 0,
    yBitShift = ((8 * sizeof(uGridKey_t)) / 3),
    zBitShift = ((16 * sizeof(uGridKey_t)) / 3)
};

/**
 * Note: this is a legacy method quantization which can be used for
 * asserting the newer U_GRID_ENCODE_* macros are correct. The macros are
 * slightly more performant than this method.
 * For hashing-based grid implementations
 * @param x - a quantized x coordinate
 * @param y - a quantized y coordinate
 * @param z - a quantized z coordinate
 * @return a single key representing the 3-D cell coordinate
 */
inline uGridKey_t encode(const uGridKey_t x,
                         const uGridKey_t y,
                         const uGridKey_t z) {
    // Based on https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch32.html
    // If any of these asserts trip, we need to increase the sizeof(key)
    uAssert(x < (uGridKey_t(1) << U_TO_GRID_SHIFT_T(yBitShift)));
    uAssert(y < (uGridKey_t(1) << U_TO_GRID_SHIFT_T(yBitShift)));
    uAssert(z < (uGridKey_t(1) << U_TO_GRID_SHIFT_T(yBitShift)));
    // The above asserts only make sense if yBitShift is median:
    uAssert((xBitShift < yBitShift) && (yBitShift < zBitShift));
    return ((x << U_TO_GRID_SHIFT_T(xBitShift)) |
            (y << U_TO_GRID_SHIFT_T(yBitShift)) |
            (z << U_TO_GRID_SHIFT_T(zBitShift)));
}

/**
 * @TODO - investigate restrict keyword on all parameters
 * Utility to map an element to a set of discrete grid cells
 * @param elem_center - the centroid of the element
 * @param elem_radius - 0.5 * elem_diameter
 * @param cell_length - length of single side of a grid cell cube
 * @param grid_radius - the max positive grid spatial coordinate (not cell)
 * @param min_x - the output minimum cell coordinate along x-axis
 * @param max_x - the output maximum cell coordinate along x-axis
 * @param min_y - the output minimum cell coordinate along y-axis
 * @param max_y - the output maximum cell coordinate along y-axis
 * @param min_z - the output minimum cell coordinate along z-axis
 * @param max_z - the output maximum cell coordinate along z-axis
 */
inline void quantize(const uReal* elem_center,
                     const uReal elem_radius,
                     const uReal cell_length,
                     const uReal grid_radius,
                     uGridKey_t& min_x,
                     uGridKey_t& max_x,
                     uGridKey_t& min_y,
                     uGridKey_t& max_y,
                     uGridKey_t& min_z,
                     uGridKey_t& max_z) {
    uAssert(elem_center);
    uAssert(elem_radius > U_TO_REAL(0.0));
    // Technically not a constraint, but if this is not true then we
    // shouldn't be using this structure
    uAssert(grid_radius > cell_length);
    // The (neg) -grid_radius is a constraint, but technically it can be
    // greater than +grid_radius for our mapping to still work
    uAssertBounds(elem_center[uDim_X], -grid_radius, grid_radius);
    uAssertBounds(elem_center[uDim_Y], -grid_radius, grid_radius);
    uAssertBounds(elem_center[uDim_Z], -grid_radius, grid_radius);

    // Based on quantization routine:
    // https://github.com/kirbysayshi/broad-phase-bng/blob/master/lib/ro.coltech.spatial-grid.js
    // http://buildnewgames.com/broad-phase-collision-detection/
    min_x = U_GRID_QUANTIZE_MIN(
        elem_center[uDim_X], elem_radius, grid_radius, cell_length);
    max_x = U_GRID_QUANTIZE_MAX(
        elem_center[uDim_X], elem_radius, grid_radius, cell_length);
    min_y = U_GRID_QUANTIZE_MIN(
        elem_center[uDim_Y], elem_radius, grid_radius, cell_length);
    max_y = U_GRID_QUANTIZE_MAX(
        elem_center[uDim_Y], elem_radius, grid_radius, cell_length);
    min_z = U_GRID_QUANTIZE_MIN(
        elem_center[uDim_Z], elem_radius, grid_radius, cell_length);
    max_z = U_GRID_QUANTIZE_MAX(
        elem_center[uDim_Z], elem_radius, grid_radius, cell_length);
}

/**
 * Utility to map an element to a set of discrete grid cells
 * @param elem_center - the centroid of the element
 * @param elem_radius - 0.5 * elem_diameter
 * @param cell_length - length of single side of a grid cell cube
 * @param grid_radius - the max positive grid spatial coordinate (not cell)
 * @param out_extents - a buffer for storing the quantized results (must
 *  be able to store NumExtents elements)
 */
inline void quantize(const uReal* elem_center,
                     const uReal elem_radius,
                     const uReal cell_length,
                     const uReal grid_radius,
                     uGridKey_t* out_extents) {
    uAssert(elem_center);
    uAssert(out_extents);
    quantize(elem_center,
             elem_radius,
             cell_length,
             grid_radius,
             out_extents[ixMinX],
             out_extents[ixMaxX],
             out_extents[ixMinY],
             out_extents[ixMaxY],
             out_extents[ixMinZ],
             out_extents[ixMaxZ]);
}
}  // namespace uGridUtils

/**
 * Macros for progressively determining grid key
 */
// This assumes uGridUtils::xBitShift == 0
#define U_GRID_ENCODE_X_Y(x, y) \
    ((x) | ((y) << U_TO_GRID_SHIFT_T(uGridUtils::yBitShift)))
#define U_GRID_ENCODE_Z(key_base, z) \
    ((key_base) | ((z) << U_TO_GRID_SHIFT_T(uGridUtils::zBitShift)))

#endif  // uGridUtils_h
