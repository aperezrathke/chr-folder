//****************************************************************************
// uSisHomgUnifLagScheduleMixin.h
//****************************************************************************

/**
 * ScheduleMixin = A mixin that defines the checkpoint schedule for
 * a quality control algorithm.
 */

#ifndef uSisHomgUnifLagScheduleMixin_h
#define uSisHomgUnifLagScheduleMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uGlobals.h"
#include "uOpts.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * A quality control schedule is responsible for determining when check
 * points occur.
 *
 * A homogeneous, uniform (HomgUnif) schedule. Can be thought of as a type of
 * *heterogeneous* lag schedule that is derived from a single homogeneous lag
 * argument. The homogeneous lag argument defines a set of intervals over the
 * completed growth phase counts:
 *
 *  {[1,lag], [lag+1,2*lag], [(2*lag)+1, 3*lag] ... }
 *
 * The schedule will pick a uniform random checkpoint within each interval.
 *
 * Initialization is random, but after initialization all checkpoints are
 * deterministic and therefore this schedule supports landmark trial runner
 *  tr_find_first() and tr_find_next() calls.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing from configuration file
    enum uOptE e_Key = uOpt_qc_schedule_lag_slot0>
class uSisHomgUnifLagScheduleMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin avoids recomputing exponential
     * weights
     */
    enum { calculates_norm_exp_weights = false };

    /**
     * The default lag value
     */
    enum { default_lag = 1 };

    /**
     * Constructor
     */
    uSisHomgUnifLagScheduleMixin() { clear(); }

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     * @param is_tr - [unused] set to true if schedule is being used for
     *  landmark trial running, false o/w
     */
    void init(const sim_t& sim,
              uSpConstConfig_t config,
              const bool is_tr U_THREAD_ID_PARAM) {
        clear();
        // Check if option exists in config
        config->read_into<uUInt>(m_lag, e_Key);
        const uUInt MIN_LAG = is_tr ? U_TO_UINT(2) : U_TO_UINT(1);
        if (m_lag < MIN_LAG) {
            uLogf(
                "Warning: homogeneous uniform schedule lag < %d, setting to "
                "%d.\n",
                (int)MIN_LAG,
                (int)MIN_LAG);
            m_lag = MIN_LAG;
        }
        // We require that our lag value be positive
        uAssert((m_lag >= MIN_LAG) && (MIN_LAG >= U_TO_UINT(1)));
        // Allocate number of intervals (integer ceiling)
        // https://stackoverflow.com/questions/2745074/fast-ceiling-of-an-integer-division-in-c-c
        uAssert(sim.get_max_num_grow_steps(sim) > U_TO_UINT(0));
        const uUInt num_intervals =
            U_TO_UINT(1) +
            ((sim.get_max_num_grow_steps(sim) - U_TO_UINT(1)) / m_lag);
        uAssert(num_intervals >= U_TO_UINT(1));
        m_checks.set_size(U_TO_MAT_SZ_T(num_intervals));
        this->sample_checks(is_tr U_THREAD_ID_ARG);
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config,
                    const bool is_tr U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {
        m_lag = default_lag;
        m_checks.clear();
    }

    /**
     * Quality control interface
     * Determines if a quality control check point has been reached (meaning
     * it's okay to go ahead and perform quality control step).
     * @param out_norm_exp_weights - an optional *uninitialized* vector of
     *  exponential weights that must be initialized if:
     *      calculates_norm_exp_weights == TRUE
     * @param out_max_log_weight - an optional *uninitialized* reference to the
     *  maximum log weight within the log_weights vector. Must be initialized
     *  if: calculates_norm_exp_weights == TRUE
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin
     * @param sim - parent simulation
     */
    inline uBool qc_checkpoint_reached(
        uVecCol& out_norm_exp_weights,
        uReal& out_max_log_weight,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_level_qc_mixin_t& qc,
        const sim_t& sim) const {
        // Determine lag interval index
        const uMatSz_t index =
            this->get_lag_index(num_completed_grow_steps_per_sample);
        uAssertBounds(index, U_TO_MAT_SZ_T(0), m_checks.n_elem);
        const uUInt grow_check = m_checks.at(index);
        return (num_completed_grow_steps_per_sample == grow_check);
    }

    /**
     * Trial runner interface
     * @param max_grow_steps - maximum number of grow steps allowed
     * @return first landmark grow step count
     */
    inline uUInt tr_find_first(const uUInt max_grow_steps) const {
        uAssert(m_checks.n_elem > U_TO_MAT_SZ_T(0));
        const uUInt grow_check =
            std::min(m_checks.at(U_TO_MAT_SZ_T(0)), max_grow_steps);
        uAssert(grow_check > U_TO_UINT(1));
        return grow_check;
    }

    /**
     * Trial runner interface
     * @param completed_grow_steps - number of grow steps already completed
     * @param max_grow_steps - maximum number of grow steps allowed
     * @return next landmark grow step count
     */
    inline uUInt tr_find_next(const uUInt completed_grow_steps,
                              const uUInt max_grow_steps) const {
        const uMatSz_t curr_index = this->get_lag_index(completed_grow_steps);
        uAssertBounds(curr_index, U_TO_MAT_SZ_T(0), m_checks.n_elem);
        const uUInt curr_check = m_checks.at(curr_index);
        if (curr_check > completed_grow_steps) {
            return std::min(curr_check, max_grow_steps);
        }
        const uMatSz_t next_index = curr_index + U_TO_MAT_SZ_T(1);
        if (next_index < m_checks.n_elem) {
            return std::min(m_checks.at(next_index), max_grow_steps);
        }
        return max_grow_steps;
    }

private:
    /**
     * @return index of lag interval for parameter step count
     */
    inline uUInt get_lag_index(const uUInt completed_grow_steps) const {
        uAssert(completed_grow_steps > U_TO_UINT(0));
        const uMatSz_t index =
            U_TO_MAT_SZ_T((completed_grow_steps - U_TO_UINT(1)) / m_lag);
        uAssertBounds(index, U_TO_MAT_SZ_T(0), m_checks.n_elem);
        return index;
    }

    /**
     * Determines which growth counts have check points using uniform sampling
     * within each lag interval.
     * @WARNING - ASSUMES m_checks HAS BEEN ALLOCATED!
     */
    void sample_checks(const bool is_tr U_THREAD_ID_PARAM) {
        // Assume positive lag
        uAssert(m_lag >= U_TO_UINT(1));
        // Assume lag > 1 for trial runner schedule
        uAssert((!is_tr) || (m_lag > U_TO_UINT(1)));
        // Assume at least single checkpoint interval
        uAssert(m_checks.n_elem >= U_TO_MAT_SZ_T(1));
        // Random number generator
        uRand& rng = uRng;
        // Random sample first lag interval
        const uMatSz_t num_intervals = m_checks.n_elem;
        uUInt grow_lo = is_tr ? U_TO_UINT(2) : U_TO_UINT(1);
        uUInt grow_hi = m_lag;
        uAssert(grow_lo <= grow_hi);
        uUInt grow_check = rng.unif_int(grow_lo, grow_hi);
        uAssertBoundsInc(grow_check, grow_lo, grow_hi);
        m_checks.at(U_TO_MAT_SZ_T(0)) = grow_check;
        // Random sample remaining lag intervals
        for (uMatSz_t i = U_TO_MAT_SZ_T(1); i < num_intervals; ++i) {
            grow_lo = grow_hi + U_TO_UINT(1);
            grow_hi += m_lag;
            uAssert(grow_lo <= grow_hi);
            grow_check = rng.unif_int(grow_lo, grow_hi);
            uAssertBoundsInc(grow_check, grow_lo, grow_hi);
            m_checks.at(i) = grow_check;
        }
    }

    /**
     * User lag argument
     */
    uUInt m_lag;

    /**
     * Maps lag intervals to the grow step count at which a checkpoint occurs
     */
    uUIVecCol m_checks;
};

#endif  // uSisHomgUnifLagScheduleMixin_h
