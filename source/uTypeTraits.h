//****************************************************************************
// uTypeTraits.h
//****************************************************************************

/**
 * @brief Type traits used for optimizing parameter types
 */

#ifndef uTypeTraits_h
#define uTypeTraits_h

#include "uBuild.h"

#ifdef U_BUILD_CXX_11
#   include <type_traits>
#   define uTypeTraits std
#else
#   include <boost/type_traits.hpp>
#   define uTypeTraits boost
#endif  // U_BUILD_CXX_11

namespace u {
/**
 * Selector utility classes
 */

// Default to pass by reference
template <typename T, bool should_pass_by_value>
struct param_type_selector {
    typedef const T& param_type;
    typedef const T& const_param_type;
};

// Pass by value selector
template <typename T>
struct param_type_selector<T, true /* should_pass_by_value */> {
    typedef const T param_type;
    typedef const T const_param_type;
};

/**
 * Call traits - contains optimal parameter passing type (POD not supported)
 */
template <typename T>
struct param_traits {
private:
    enum {
        should_pass_by_value = uTypeTraits::is_arithmetic<T>::value ||
                               uTypeTraits::is_pointer<T>::value
    };

public:
    typedef typename param_type_selector<T, should_pass_by_value>::param_type
        param_type;
    typedef
        typename param_type_selector<T, should_pass_by_value>::const_param_type
            const_param_type;
};

}  // namespace u

#endif  // uTypeTraits_h
