//****************************************************************************
// uSisHetrNodeRadiusMixin.h
//****************************************************************************

/**
 * A heterogeneous radius mixin which does not assume all nodes are same size
 *  but does assume that all radii are deterministic.
 */

#ifndef uSisHetrNodeRadiusMixin_h
#define uSisHetrNodeRadiusMixin_h

#include "uBuild.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uParserRle.h"
#include "uSisNodeRadiusMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uTypes.h"

//****************************************************************************
// Radius loading utility
//****************************************************************************

/**
 * Utility class for loading heterogeneous radii from user configuration
 */
class uParseHetrRadiiUtil {
public:
    /**
     * Parse radii according to user configuration, terminate program on error
     */
    static void get_no_fail(uVecCol& radii,
                            uSpConstConfig_t config,
                            const uUInt expected_size) {
        // We expect at least a single monomer
        uAssert(expected_size > U_TO_UINT(0));

        // Reset radii
        radii.clear();

        // Stores complete file path to diameter specification file
        std::string diam_fpath;

        // Determine parameter diameter specification file
        uBool b_diam_read_ok = uFALSE;
        if (config->resolve_path(diam_fpath, uOpt_node_diameter_fpath)) {
            // Parse diameters
            if (uParserRle::read<uReal, uVecCol>(radii, diam_fpath)) {
                b_diam_read_ok = uTRUE;
                // Convert from diameter to radii
                radii *= U_TO_REAL(0.5);
            } else {
                uLogf("Warning: unable to parse %s.\n", diam_fpath.c_str());
            }
        }

        // Check if homogeneous specification
        if (!b_diam_read_ok) {
            uReal homg_diam = U_TO_REAL(0.0);
            if (!config->read_into(homg_diam, uOpt_max_node_diameter)) {
                uLogf("Error: max node diameter not specified.\n");
                exit(uExitCode_error);
            }
            radii.set_size(U_TO_MAT_SZ_T(expected_size));
            radii.fill(homg_diam * U_TO_REAL(0.5));
        }

        // Check radii vector has expected count
        if (U_TO_UINT(radii.n_elem) != expected_size) {
            uLogf(
                "Error: expected %d node radii but initialized %d. Exiting.\n",
                ((int)expected_size),
                ((int)radii.n_elem));
            exit(uExitCode_error);
        }

        // Check all radii are positive
        for (uMatSz_t i = 0; i < radii.n_elem; ++i) {
            const uReal radius = radii.at(i);
            if (radius <= U_TO_REAL(0.0)) {
                uLogf(
                    "Error: node %d (zero-based) has radius %f < 0. Exiting.\n",
                    ((int)i),
                    ((double)radius));
                exit(uExitCode_error);
            }
        }
    }
};

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisHetrNodeRadiusSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag that simulation is heterogeneous wrt to node radius
     */
    enum { is_homg_radius = false };

    /**
     * Default constructor
     */
    uSisHetrNodeRadiusSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level node radius mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Reset
        clear();
        // Parse radii according to user configuration
        uParseHetrRadiiUtil::get_no_fail(
            m_hetr_node_radii, config, sim.get_max_total_num_nodes());
        // Initialize max node diameter
        m_hetr_max_node_diameter = U_TO_REAL(2.0) * m_hetr_node_radii.max();
        uAssert(m_hetr_max_node_diameter > U_TO_REAL(0.0));
        uAssert(m_hetr_node_radii.min() > U_TO_REAL(0.0));
        // Initialize cumulative c2c lengths
        m_hetr_cumsum_c2c_len.zeros(m_hetr_node_radii.n_elem);
        uReal c2c_len = U_TO_REAL(0.0);
        for (uMatSz_t i = U_TO_MAT_SZ_T(1); i < m_hetr_node_radii.n_elem; ++i) {
            c2c_len += m_hetr_node_radii.at(i - U_TO_MAT_SZ_T(1));
            c2c_len += m_hetr_node_radii.at(i);
            m_hetr_cumsum_c2c_len.at(i) = c2c_len;
        }
    }

    /**
     * Resets to default uninitialized state
     */
    void clear() {
        m_hetr_max_node_diameter = U_TO_REAL(0.0);
        m_hetr_node_radii.clear();
        m_hetr_cumsum_c2c_len.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * The maximum diameter of a monomer node within a chromatin polymer chain.
     * Since all nodes have same maximum diameter, we are making this a property
     * of the parent simulation.
     */
    inline uReal get_max_node_diameter() const {
        uAssert(m_hetr_max_node_diameter > U_TO_REAL(0.0));
        return m_hetr_max_node_diameter;
    }

    /**
     * @return radius of i-th node
     */
    inline uReal get_node_radius(const uUInt node_id) const {
        uAssertBounds(node_id, 0, m_hetr_node_radii.n_elem);
        return m_hetr_node_radii.at(node_id);
    }

    /**
     * @return vector of node radii
     */
    inline const uVecCol& get_node_radii() const { return m_hetr_node_radii; }

    /**
     * @return radius of candidate for i-th node
     */
    inline uReal get_candidate_node_radius(
        const uUInt candidate_node_id) const {
        return get_node_radius(candidate_node_id);
    }

    /**
     * @WARNING - DOES NOT CHECK IF CROSSING LOCUS BOUNDARIES!
     * Computes max node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible distance between the two node centers
     */
    inline uReal get_max_c2c_len(const uUInt start_id,
                                 const uUInt end_id) const {
        uAssertBounds(start_id, 0, m_hetr_node_radii.n_elem);
        uAssertBounds(end_id, start_id, m_hetr_node_radii.n_elem);
        uAssertRealGte(m_hetr_cumsum_c2c_len.at(end_id),
                       m_hetr_cumsum_c2c_len.at(start_id));
        const uReal c2cl_fast = m_hetr_cumsum_c2c_len.at(end_id) -
                                m_hetr_cumsum_c2c_len.at(start_id);
        uAssertRealGte(c2cl_fast, U_TO_REAL(0.0));
#ifdef U_BUILD_ENABLE_ASSERT
        // Sanity check - test against naive looping method
        uReal c2cl_slow = U_TO_REAL(0.0);
        const uUInt ONE = U_TO_UINT(1);
        for (uUInt i = start_id + ONE; i <= end_id; ++i) {
            c2cl_slow += m_hetr_node_radii.at(U_TO_MAT_SZ_T(i - ONE));
            c2cl_slow += m_hetr_node_radii.at(U_TO_MAT_SZ_T(i));
        }
        uAssertRealEq(c2cl_slow, c2cl_fast);
#endif  // U_BUILD_ENABLE_ASSERT
        return c2cl_fast;
    }

    /**
     * @WARNING - DOES NOT CHECK IF CROSSING LOCUS BOUNDARIES!
     * Computes max squared node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible squared distance between the two node centers
     */
    inline uReal get_max_sq_c2c_len(const uUInt start_id,
                                    const uUInt end_id) const {
        const uReal c2c_length = this->get_max_c2c_len(start_id, end_id);
        return (c2c_length * c2c_length);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Max node diameter among all nodes
     */
    uReal m_hetr_max_node_diameter;

    /**
     * The radius at each node (1-D linearized). Assumes nodes are in same
     * order as node positions matrix columns
     */
    uVecCol m_hetr_node_radii;

    /**
     * Cumulative sum of center-to-center lengths starting from first bead
     */
    uVecCol m_hetr_cumsum_c2c_len;
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * "Typedef" for sample-level mixin
 */
#define uSisHetrNodeRadiusMixin uSisNodeRadiusMixin

#endif  // uSisHetrNodeRadiusMixin_h
