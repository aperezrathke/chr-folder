//****************************************************************************
// uConfig.h
//
// Class for reading and storing chromatin ensemble generation options
//****************************************************************************

#ifndef uConfig_h
#define uConfig_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uCmdOptsMap.h"
#include "uConfigFile.h"
#include "uExportFlags.h"
#include "uOpts.h"
#include "uSmartPtr.h"
#include "uTypes.h"

#include <string>
#include <vector>

/**
 * Reads and stores users configuration parameters.
 *
 * Provides interface so that rest of application does not have to worry about
 * whether its options are coming from the command line or from a INI file
 */
class uConfig : public uSmartPtr::enable_shared_from_this<uConfig> {
public:
    /**
     * Typedefs
     */
    typedef uSmartPtr::shared_ptr<uConfig> uSpConfig_t;
    typedef uSmartPtr::shared_ptr<const uConfig> uSpConstConfig_t;
    typedef uSmartPtr::weak_ptr<uConfig> uWpConfig_t;
    typedef uSmartPtr::weak_ptr<const uConfig> uWpConstConfig_t;

    //************************************************************************
    // Public named variables
    //************************************************************************

    /**
     * Directory to store generated chains.
     */
    std::string output_dir;

    /**
     * Prefix appended to all output chains.
     */
    std::string job_id;

    /**
     * Max number of trials that simulation will run. A 0 or negative input
     * results in the maximum number of iterations (max value of unsigned int)
     *
     * A trial is a complete run of the the simulation. However, after the
     * run, there may be loci chains that died (were unable to proceed because
     * they grew into a corner). Each trial will attempt to generate the
     * remaining chains until the target ensemble size is reached or the
     * maximum trial count is reached - whichever comes first.
     */
    uUInt max_trials;

    /**
     * Number of nodes (monomers) for each chromatin loci to be
     * simultaneously modeled. Each loci will be modeled as a chromatin
     * polymer chain.
     */
    std::vector<uUInt> num_nodes;

    /**
     * Number of points to sample on surface of node sphere when growing a
     * chain and deciding where to add the next node.
     */
    uUInt num_unit_sphere_sample_points;

    /**
     * Number of samples to be generated for each run of the program.
     * A sample may consist of multiple chromatin chains if modeling multiple
     * loci simultaneously.
     */
    uUInt ensemble_size;

    /**
     * Flags indicating what should be exported and in what format(s)
     */
    uUInt export_flags;

    //************************************************************************
    // Public methods
    //************************************************************************

    /**
     * Default constructor creates an invalid but deterministic configuration
     */
    uConfig();

    /**
     * Sets configuration to default invalid but deterministic configuration
     */
    void clear();

    /**
     * Initializes from command line
     *
     * @param cmd_opts - command line options
     * @param should_clear - If TRUE, erases existing state in this object
     */
    void init(const uCmdOptsMap& cmd_opts, const uBool should_clear = uTRUE);

    /**
     * Prints to stdout any invalid configuration options
     *
     * @return TRUE (1) if configuration is well-formed, FALSE (0) otherwise
     */
    uBool is_valid() const;

    /**
     * Outputs configuration options to stdout
     */
    void print() const;

    /**
     * Search for value mapped to option key
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    uBool read_into(T& out_val,
                    const uOptE ix,
                    const uBool check_parent = uTRUE) const {
        // Check command line first
        if (m_cmd_opts.read_into(out_val, uOpt_get_cmd_switch(ix))) {
            return uTRUE;
        }
        // Else check INI file
        /*else*/ if (m_cfg_file.read_into(out_val, uOpt_get_ini_key(ix))) {
            return uTRUE;
        }
        // Else check INI archetype
        /*else*/ if (m_cfg_arch.read_into(out_val, uOpt_get_ini_key(ix))) {
            return uTRUE;
        }
        // Else check the parent configuration
        /*else*/ if (check_parent) {
            uSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into(
                    out_val, ix, uTRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return uFALSE;
    }

    /**
     * @WARNING - DOES NOT PERFORM PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<uOptE>& ixs,
                     const uBool check_parent = uTRUE) const;

    /**
     * PERFORMS PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param should_resolve - Parallel vector of same size as 'ixs' with i-th
     *  element TRUE if option is a path that must be resolved, FALSE o/w
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<uOptE>& ixs,
                     const std::vector<uBool>& should_resolve,
                     const uBool check_parent = uTRUE) const;

    /**
     * Search for value mapped to option vector key. Vector options are of INI
     * format: <opt_name>_<off> = <value> where 'off' is a non-negative integer
     * index. This method returns a SINGLE value mapped to the option offset.
     * Vector options are assumed to use 0-based indexing and be declared
     * sequentially (no skips).
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option prefix identifier
     * @param off - The option offset
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    uBool read_into_vec(T& out_val,
                        const uOptE ix,
                        const uUInt off,
                        const uBool check_parent = uTRUE) const {
        // Check command line first
        const std::string suffix(std::string("_") + u2Str(off));
        if (m_cmd_opts.read_into(out_val, uOpt_get_cmd_switch(ix) + suffix)) {
            return uTRUE;
        }
        // Else check INI file
        /*else*/ if (m_cfg_file.read_into(out_val,
                                          uOpt_get_ini_key(ix) + suffix)) {
            return uTRUE;
        }
        // Else check INI archetype
        /*else*/ if (m_cfg_arch.read_into(out_val,
                                          uOpt_get_ini_key(ix) + suffix)) {
            return uTRUE;
        }
        // Else check the parent configuration
        /*else*/ if (check_parent) {
            uSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into_vec(
                    out_val, ix, off, uTRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return uFALSE;
    }

    /**
     * Obtain path value resolved according to the rules (in order of
     * precedence)
     *  - if not found, path argument is unmodified
     *  - if from command line, output path is unmodified
     *  - if from config file and absolute, output path is unmodified
     *  - if from config file and relative, output path is resolved relative
     *      to config file directory
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param path - Output path, unmodified if key not found
     * @param ix - Option identifier
     * @return uTRUE if key exists, uFALSE otherwise. If key exists and value
     *  is from command line, then output path argument is same as command
     *  line. However, if key exists and value is from configuration file,
     *  then relative paths are resolved to be relative to the configuration
     *  file directory. Note that command line values override any
     *  configuration file values.
     */
    uBool resolve_path(std::string& path,
                       const uOptE ix,
                       const uBool check_parent = uTRUE) const;

    /**
     * Resolve path for vectorized options
     * @param path - Output path obtained using resolve_path() semantics
     * @param ix - Option identifier
     * @param off - The option offset
     * @return TRUE if key exists, FALSE o/w
     */
    uBool resolve_path_vec(std::string& path,
                           const uOptE ix,
                           const uUInt off,
                           const uBool check_parent = uTRUE) const;

    /**
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    inline uBool key_exists(const uOptE ix,
                            const uBool check_parent = uTRUE) const {
        // Check command line first
        if (m_cmd_opts.key_exists(uOpt_get_cmd_switch(ix))) {
            return uTRUE;
        }
        // Else check INI file
        /*else*/ if (m_cfg_file.key_exists(uOpt_get_ini_key(ix))) {
            return uTRUE;
        }
        // Else check INI archetype
        /*else*/ if (m_cfg_arch.key_exists(uOpt_get_ini_key(ix))) {
            return uTRUE;
        }
        // Else, check the parent configuration
        /*else*/ if (check_parent) {
            uSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->key_exists(ix, uTRUE /*check_parent*/);
            }
        }
        // Key not found, return false
        return uFALSE;
    }

    /**
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    template <typename t_>
    void set_option(const uOptE ix, const t_& val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(uOpt_get_cmd_switch(ix), val);
    }

    /**
     * Overload for char*
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    void set_option(const uOptE ix, const char* val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(uOpt_get_cmd_switch(ix), std::string(val));
    }

    /**
     * Set option if not already set (can be used for setting defaults)
     * @param ix - The option identifier
     * @param val - The value to assign
     * @return TRUE if option was set, FALSE otherwise (e.g. if option already
     *  specified)
     */
    template <typename t_>
    inline uBool set_option_if_absent(const uOptE ix, const t_& val) {
        if (!this->key_exists(ix)) {
            // Option was not previously set
            set_option(ix, val);
            // Option was successfully set
            return uTRUE;
        }
        // Option was not set as it exists already
        return uFALSE;
    }

    /**
     * @return pointer to trial runner sub-sim config
     */
    inline uSpConfig_t get_child_tr() const { return m_child_tr; }

    /**
     * @return pointer to trial runner select sub-sim config
     */
    inline uSpConfig_t get_child_tr_sel() const { return m_child_tr_sel; }

    /**
     * @return pointer to quality control sub-sim config
     */
    inline uSpConfig_t get_child_qc() const { return m_child_qc; }

    /**
     * Overwrite trial runner sub-sim configuration
     * @param child - child configuration
     * @param should_copy_named_vars_to_child - if true, will overwrite any
     *  named variables of the child with the parent named variables
     */
    void set_child_tr(uSpConfig_t child,
                      const uBool should_copy_named_vars_to_child = true);

    /**
     * Overwrite trial runner select sub-sim configuration
     * @param child - child configuration
     * @param should_copy_named_vars_to_child - if true, will overwrite any
     *  named variables of the child with the parent named variables
     */
    void set_child_tr_sel(uSpConfig_t child,
                          const uBool should_copy_named_vars_to_child = true);

    /**
     * Overwrite quality control sub-sim configuration
     * @param child - child configuration
     * @param should_copy_named_vars_to_child - if true, will overwrite any
     *  named variables of the child with the parent named variables
     */
    void set_child_qc(uSpConfig_t child,
                      const uBool should_copy_named_vars_to_child = true);

private:
    /**
     * Overwrite a child configuration
     * @param child_mem_ - member to assign
     * @param child - child configuration
     * @param should_copy_named_vars_to_child - if true, will overwrite any
     *  named variables of the child with the parent named variables
     */
    void set_child(uSpConfig_t& child_mem_,
                   uSpConfig_t child,
                   const uBool should_copy_named_vars_to_child);

    /**
     * Internal print adjusted for child level
     */
    void print(const uUInt level) const;

    /**
     * Conditionally initialize child members
     */
    void conditional_init_child_tr();
    void conditional_init_child_tr_sel();
    void conditional_init_child_qc();

    /**
     * Checks if child configuration should be initialized
     * @param child_mem_ - member to conditionally initialize
     * @param ini_key_ - command key for child configuration file
     * @param cmd_prefix_ - prefix to extract child command line arguments
     * @return uTRUE if child member initialized, uFALSE o/w
     */
    uBool conditional_init_child(uSpConfig_t& child_mem_,
                                 const uOptE ini_key_,
                                 const char cmd_prefix_);

    /**
     * Actual initialization for a child configuration
     * @param parent - The parent configuration
     * @param cfg_file - Child INI files
     * @param cmd_prefix_ - prefix to extract child command line arguments
     * Note: child command line options are extracted from parent command line
     */
    void init(uSpConstConfig_t parent,
              const uConfigFile& cfg_file,
              const char cmd_prefix_);

    /**
     * Copies named member variables from parameter config
     * @param cfg - Config object to copy only named variables from
     *  (does not copy pair, value mappings)
     */
    void copy_named_vars(uSpConstConfig_t cfg);

    /**
     * Command line options mapping:
     * Used for storing <key=string, value=string> tuples obtained from
     * command line
     */
    uCmdOptsMap m_cmd_opts;

    /**
     * INI file options mapping:
     * Used for storing <key=string, value=string> tuples obtained from INI
     * configuration file
     */
    uConfigFile m_cfg_file;

    /**
     * INI file archetype:
     * Settings not found in m_cfg_file will be deferred to archetype
     */
    uConfigFile m_cfg_arch;

    /**
     * Pointer to possible parent configuration. If a key,value pair is
     * missing in this config, the parent is checked for the value.
     * Using weak_ptr to prevent circular reference issues.
     */
    uWpConstConfig_t m_parent;

    /**
     * The parent configuration owns the memory for any child configurations.
     * If all parent configurations are destroyed, then the child
     * configuration will be destroyed as well.
     * Note: the parent configuration does not defer to the child
     * configuration for any key, value pairs. However, client code may
     * utilize the child configurations for initializing sub-simulations of
     * the client program.
     */

    /**
     * Child configuration for trial runner sub-simulation
     */
    uSpConfig_t m_child_tr;

    /**
     * Child configuration for trial runner selection policy sub-simulation
     */
    uSpConfig_t m_child_tr_sel;

    /**
     * Child configuration for quality control sub-simulation
     */
    uSpConfig_t m_child_qc;
};

/**
 * Typedefs
 */
typedef uConfig::uSpConfig_t uSpConfig_t;
typedef uConfig::uWpConfig_t uWpConfig_t;
typedef uConfig::uSpConstConfig_t uSpConstConfig_t;
typedef uConfig::uWpConstConfig_t uWpConstConfig_t;

#endif  // uConfig_h
