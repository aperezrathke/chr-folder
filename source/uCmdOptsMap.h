//****************************************************************************
// uCmdOptsMap.h
//****************************************************************************

/**
 * @brief Utilities for parsing stdin command line
 */

#ifndef uCmdOptsMap_h
#define uCmdOptsMap_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uStringUtils.h"
#include "uTypes.h"

#include <map>
#include <sstream>
#include <string>
#include <vector>

//****************************************************************************
// uCmdOpts
//****************************************************************************

/**
 * Parses command line into a map structure
 *
 * All commands are assumed to be of form -<opt> <opt_value> or --<switch>.
 * So, a single dash (-) indicates a unary boolean switch and two dashes (--)
 * indicate a binary <key, value> pair.
 *
 * Note: unary (-) switch keys will still be mapped to an empty string value
 */
class uCmdOptsMap {
public:
    /**
     * Default constructor
     */
    uCmdOptsMap() {}

    /**
     * Constructor initializes from command arguments
     *
     * @param argc - the number of command line arguments
     * @param argv - character string array of command line arguments
     */
    uCmdOptsMap(const int argc, const char** argv);

    /**
     * Parses command line
     *
     * Must be called before using accessor methods such as get_*_value
     *
     * @param argc - the number of command line arguments
     * @param argv - character string array of command line arguments
     * @return TRUE if no parse errors, FALSE otherwise
     */
    uBool parse(const int argc, const char** argv);

    /**
     * Search for value mapped to option key
     *
     * parse() must be called prior to using this method.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param opt_key - The key to search for in command map
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    uBool read_into(T& out_val, const std::string& opt_key) const {
        const cmd_map_t::const_iterator it(cmd_opts.find(opt_key));
        if (it != cmd_opts.end()) {
            out_val = string_as_T<T>(it->second);
            return uTRUE;
        }
        return uFALSE;
    }

    /**
     * Mirror uConfigFile interface, returns path unmodified if existing
     *
     * parse() must be called prior to using this method.
     *
     * @param path - output path as entered on command line, argument is
     *  unmodified if not found
     * @param opt_key - The key to search for in command map
     * @return TRUE if key exists, FALSE otherwise
     */
    uBool resolve_path(std::string& path, const std::string& opt_key) const {
        return read_into(path, opt_key);
    }

    /**
     * parse() must be called prior to using this method
     *
     * @return TRUE if option key exists, FALSE otherwise
     */
    inline uBool key_exists(const std::string& opt_key) const {
        return this->cmd_opts.find(opt_key) != this->cmd_opts.end();
    }

    /**
     * Obtain all registered option keys
     */
    void get_keys(std::vector<std::string>& keys) const {
        for (cmd_map_t::const_iterator it = cmd_opts.begin();
             it != cmd_opts.end();
             ++it) {
            keys.push_back(it->first);
        }
    }

    /**
     * @return TRUE if no option keys exist, FALSE o/w
     */
    inline bool empty() const { return cmd_opts.empty(); }

    /**
     * Allow clients to set options programmatically
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The value to assign
     */
    template <typename t_value>
    void set_option(const std::string& opt_key, const t_value& opt_val) {
        this->cmd_opts[opt_key] = u2Str(opt_val);
    }

    /**
     * Overload for character type
     *
     * Allow clients to set options programmatically
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The string value to assign
     */
    void set_option(const std::string& opt_key, const char* opt_val) {
        this->cmd_opts[opt_key] = opt_val;
    }

    /**
     * Conditionally sets option value if 'opt_key' not already existing
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The value to conditionally assign
     */
    template <typename t_value>
    void set_option_if_absent(const std::string& opt_key,
                              const t_value& opt_val) {
        if (!this->key_exists(opt_key)) {
            this->set_option(opt_key, opt_val);
        }
    }

    /**
     * Extract only command line options destined for child configurations
     * @param out - output command line options (@WARNING - will be cleared!)
     * @param cmd_prefix_ - prefix to extract child command line arguments
     * @WARNING - if cmd_prefix_ is non-graphical character, all commands
     *  options are returned!
     */
    void get_child_cmd(uCmdOptsMap& out, const char cmd_prefix_) const;

private:
    /**
     * Utility method to convert from a string type to any other data type
     *
     * Copied from ConfigFile.h (protected member)
     *
     * @param s - string to convert
     * @return the value of the converted string
     */
    template <class T>
    static T string_as_T(const std::string& s) {
        return uStringUtils::string_as_T<T>(s);
    }

    // Data structure for mapping <key, value> pairs
    typedef std::map<std::string, std::string> cmd_map_t;

    // Internal map of command line options
    cmd_map_t cmd_opts;
};

#endif  // uCmdOptsMap_h
