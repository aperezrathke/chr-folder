//****************************************************************************
// uIntrLibBudgUtil.h
//****************************************************************************

/**
 * Utility header for budgeted interactions
 */

#ifndef uIntrLibBudgUtil_h
#define uIntrLibBudgUtil_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibDefs.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// Interaction budget access policy
//****************************************************************************

/**
 * Null budget access policy
 */
template <typename t_SisGlue>
class uIntrLibBudgAccNull {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);
    /**
     * Indicate null budget access
     */
    enum { is_null_intr_budg = true };
    /**
     * @return Incremental (delta) number of failures for candidate
     */
    inline static uUInt get_delta_fail(const uUInt candidate_id,
                                       const sample_t& sample,
                                       const sim_t& sim U_THREAD_ID_PARAM) {
        return U_TO_UINT(0);
    }
};

//****************************************************************************
// uIntrLibBudgUtil
//****************************************************************************

/**
 * Essentially namespace for common interaction budget utilities
 */
class uIntrLibBudgUtil {
public:
    /**
     * Utility uses linear search to remove element from list
     */
    inline static void remove_fail(uSisIntrIndexVec_t& intr_idvec,
                                   const uUInt intr_id) {
        const size_t n_idvec = intr_idvec.size();
        size_t j = 0;
        for (; j < n_idvec; ++j) {
            if (intr_id == intr_idvec[j]) {
                std::swap(intr_idvec[j], intr_idvec.back());
                uAssert(intr_idvec.back() == intr_id);
                intr_idvec.pop_back();
                break;
            }
        }
        // If this assert trips, then interaction was not found!
        uAssert(j < n_idvec);
    }

private:
    // Disallow any form of instantiation
    uIntrLibBudgUtil(const uIntrLibBudgUtil&);
    uIntrLibBudgUtil& operator=(const uIntrLibBudgUtil&);
};

#endif  // uIntrLibBudgUtil_h
