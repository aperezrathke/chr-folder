//****************************************************************************
// uTestSisSkiSeoSlocIntrChr.h
//****************************************************************************

/**
 * Commandlet for testing single knock-in constrained growth; will output
 * resulting PDB samples for later visualization.
 *
 * Sis: Sequential importance sampling
 * Ski: Single knock-in
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Sloc: Single locus (only a single locus is modeled)
 *
 * Usage:
 * -test_sis_ski_seo_sloc_intr_chr --output_dir <output_dir> --frags
 * <frag_path> --knock_in <knock_in_path> --knock_in_pix <unsigned integer>
 *
 * or, if defaults are okay to use, then simply:
 * -test_sis_ski_seo_sloc_intr_chr
 */

#ifdef uTestSisSkiSeoSlocIntrChr_h
#   error "Test Sis Ski Seo Sloc Intr Chr included multiple times!"
#endif  // uTestSisSkiSeoSlocIntrChr_h
#define uTestSisSkiSeoSlocIntrChr_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uMockUpUtils.h"
#include "uSisSkiSeoSlocIntrChrMixin.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uTypes.h"

#include <iostream>
#include <string>

namespace {
/**
 * A library glue for single end growth with no quality control and single
 * knock-in interaction constraints
 */
U_MOCKUP_DECLARE_NO_QC_GLUE(uTestSisSkiSeoSlocIntrChrGlue,
                            uSisSeoSloc::GrowthMixin,
                            uSisSeoSloc::GrowthSimLevelMixin_threaded,
                            uSisHomgNodeRadiusMixin,
                            uSisHomgNodeRadiusSimLevelMixin,
                            uSisSkiSeoSlocIntrChrMixin,
                            uSisSkiSeoSlocIntrChrSimLevelMixin,
                            uSisNullIntrLamMixin,
                            uSisNullIntrLamSimLevelMixin,
                            uSisNullIntrNucbMixin,
                            uSisNullIntrNucbSimLevelMixin,
                            uSisUnifEnergyMixin,
                            uSisUnifEnergySimLevelMixin);

}  // namespace

/**
 * Entry point for commandlet
 */
int uTestSisSkiSeoSlocIntrChrMain(const uCmdOptsMap& cmd_opts) {
    // Expose final simulation
    typedef uTestSisSkiSeoSlocIntrChrGlue::sim_t sim_t;

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string TEST_NAME = "test_sis_ski_seo_sloc_intr_chr";
    const std::string DEFAULT_OUTPUT_DIR = "../output/" + TEST_NAME;
    const std::string DEFAULT_JOB_ID = TEST_NAME;
    const uUInt DEFAULT_MAX_TRIALS = 1000;
    const uUInt DEFAULT_ENSEMBLE_SIZE = 10;
    const uReal DEFAULT_MAX_NODE_DIAMETER = 206.3355200903593;
    const uReal DEFAULT_NUCLEAR_DIAMETER = 5229.1;
    const std::vector<uUInt> DEFAULT_NUM_NODES(1 /*n_chains*/, 500 /*n_nodes*/);
    const uUInt DEFAULT_NUM_UNIT_SPHERE_SAMPLE_POINTS = 50;
    const std::string DEFAULT_FRAG_PATH =
        "../tests/" + TEST_NAME + "/frags.csv";
    const std::string DEFAULT_KNOCK_IN_PATH =
        "../tests/" + TEST_NAME + "/interactions.csv";
    const uUInt DEFAULT_SKI_IX = 24;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = DEFAULT_OUTPUT_DIR;
    config->job_id = DEFAULT_JOB_ID;
    config->max_trials = DEFAULT_MAX_TRIALS;
    config->ensemble_size = DEFAULT_ENSEMBLE_SIZE;
    config->set_option(uOpt_max_node_diameter, DEFAULT_MAX_NODE_DIAMETER);
    config->set_option(uOpt_nuclear_diameter, DEFAULT_NUCLEAR_DIAMETER);
    config->num_nodes = DEFAULT_NUM_NODES;
    config->num_unit_sphere_sample_points =
        DEFAULT_NUM_UNIT_SPHERE_SAMPLE_POINTS;
    config->set_option(uOpt_chr_frag, DEFAULT_FRAG_PATH.c_str());
    config->set_option(uOpt_chr_knock_in, DEFAULT_KNOCK_IN_PATH.c_str());
    config->set_option(uOpt_ski_ix, DEFAULT_SKI_IX);

    // Override from command line
    config->init(cmd_opts, uFALSE /*should_clear*/);

    // Output configuration
    std::cout << "Running Test Sis Ski Seo Sloc Intr Chr." << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    // Export!
    uSisUtils::export_chromatin_pdb(sim, false /*extended*/);

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
