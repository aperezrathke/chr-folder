//****************************************************************************
// uLigcCore.cpp
//****************************************************************************

#include "uBuild.h"
#include "uLigcCore.h"
#include "uAssert.h"
#include "uChromatinExportCsv.h"
#include "uDistUtils.h"
#include "uExportFlags.h"
#include "uFilesystem.h"
#include "uIntrLibConfig.h"
#include "uIntrLibDefs.h"
#include "uParserTable.h"
#include "uStreamUtils.h"

#include <boost/algorithm/string.hpp>
#include <sstream>
#include <string.h>

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Ligc binary format header
 */
#define U_LIGC_BIN_HEADER "LIGC"

/**
 * Ligc binary format version
 */
#define U_LIGC_BIN_VERSION 1

/**
 * Ligc binary format element index type
 */
#define U_LIGC_BIN_EIT uLigcCore::elem_id_t

/**
 * Ligc binary format element index bytes
 */
#define U_LIGC_BIN_EIB sizeof(U_LIGC_BIN_EIT)

//****************************************************************************
// Utilities
//****************************************************************************

namespace {

/**
 * Common callback routine when ligc import fails on log weight
 */
inline uBool on_import_error(uReal& log_weight) {
    log_weight = U_SIS_DEAD_SAMPLE_LOG_WEIGHT;
    return uFALSE;
}

/**
 * Common callback routine when ligc import fails for profile and log weight
 */
inline uBool on_import_error(uLigcCore::profile_t& profile, uReal& log_weight) {
    profile.clear();
    return on_import_error(log_weight);
}

}  // anonymous namespace

//////////////////////////////////////////////////////////////////////////
// Format
//////////////////////////////////////////////////////////////////////////

/**
 * @param ligc_format - string resulting from e.g. get_format_csv() call
 * @return name of suggested subdirectory for ligc format
 */
std::string uLigcCore::get_format_subdir(const std::string& ligc_format) {
    return std::string(get_format_base()) + "." + ligc_format;
}

/**
 * @param output_dir - base output (data) directory path
 * @param ligc_format - string resulting from e.g. get_format_csv() call
 * @return directory path for ligc format
 */
std::string uLigcCore::get_format_dir(const std::string& output_dir,
                                      const std::string& ligc_format) {
    return (uFs::path(output_dir) / uFs::path(get_format_subdir(ligc_format)))
        .string();
}

/**
 * Resolves format directory path - meant for use with commandlets were the
 * import or export directory may differ from implicit paths
 * @param out - output export path
 * @param base_format_dir - base export directory
 * @param format_no_dot - file extension suffix without '.'
 */
void uLigcCore::get_format_dir2(std::string& out,
                                const std::string& base_format_dir,
                                const std::string& format_no_dot) {
    uFs::path fmt_dir(base_format_dir);
    const uFs::path fname(fmt_dir.filename());
    // @HACK - Handle special case "." and ".." which are considered
    //  special files (e.g. uFs::path::filename(".") returns "."). Note,
    //  if BOOST_VERISON >= 106300, then there exists methods:
    //      - bool uFs::path::filename_is_dot()
    //      - bool uFs::path::filename_is_dot_dot()
    //  which do same comparison as below. However, many systems may be on
    //  pre 1_63 versions of the BOOST library, so we perform the
    //  comparison ourselves to allow compilation on older systems.
    const uBool is_dot =
        (fname == uFs::path(".")) || (fname == uFs::path(".."));
    const std::string fmt_sfx = std::string(".") + format_no_dot;
    if (is_dot) {
        fmt_dir /= fmt_sfx;
    } else {
        const std::string fmt_subdir = fname.string() + fmt_sfx;
        fmt_dir.remove_filename() /= fmt_subdir;
    }
    out = fmt_dir.string();
}

//****************************************************************************
// Proximity
//****************************************************************************

/**
 * @param config - User configuration
 * @return User-defined ligation threshold interaction distance, or
 *  default if not specified
 */
uReal uLigcCore::get_dist(uSpConstConfig_t config) {
    return uIntrLibConfig::load_dist(
        config, uOpt_ligc_dist, uIntrChrDefaultKnockInDist, "ligc");
}

/**
 * To allow ligation interaction queries during ligc export, we need to
 * ensure the broad phase grid is padded by the knock-in distance if this
 * distance is larger than the max node diameter
 * @param max_node_diameter - Diameter of largest node in simulation
 * @param ki_dist - Knock-in distance
 * @param export_flags - Simulation export flags, will avoid ligc padding
 *  if no ligc export detected
 * @return broad phase collision padding
 */
uReal uLigcCore::get_collision_boundary_pad(const uReal max_node_diameter,
                                            const uReal ki_dist,
                                            const uUInt export_flags) {
    uAssert(max_node_diameter > U_TO_REAL(0.0));
    uAssert(ki_dist >= U_TO_REAL(0.0));
    return should_export(export_flags) ? std::max(max_node_diameter, ki_dist)
                                       : max_node_diameter;
}

/**
 * Calculate ligation contacts
 * @param profile - output ligation tuples
 * @param mask_buffer - pre-allocated buffer used for narrow phase checking
 * @param ki_dist - ligation proximity (Euclidean distance) threshold
 * @param node_radii - radius at each monomer node
 * @param node_positions - (x, y, z) coordinates for each node center
 * @param broad_phase - broad phase collision structure
 * @param broad_cell_length - broad phase grid cell length
 * @param broad_radius - broad phase bounding radius
 * @param keep_bonded - If 1, then mark (i,i) and (i,i+1) ligations
 */
void uLigcCore::proximity(profile_t& profile,
                          uBitset& mask_buffer,
                          const uReal ki_dist,
                          const uVecCol& node_radii,
                          const uMatrix& node_positions,
                          const uBroadPhase_t& broad_phase,
                          const uReal broad_cell_length,
                          const uReal broad_radius,
                          const uBool keep_bonded) {

    // Pre-conditions
    uAssert(ki_dist > U_TO_REAL(0.0));
    uAssertPosEq(node_positions.n_cols, node_radii.n_elem);
    uAssertPosEq(node_positions.n_rows, uDim_num);

    // Wrap mask for better intrinsics
    uWrappedBitset mask(U_WRAP_BITSET(mask_buffer));
    uAssertPosEq(U_TO_MAT_SZ_T(mask.size()), node_radii.n_elem);

    // Reset profile
    profile.clear();

    // Distance calculation buffer
    uReal dist2[uDim_num];
    // Ligation threshold
    uReal thresh2;

    // Last legal node index (assumes at least 1 monomer node exists)
    typedef uWrappedBitset::size_type bitset_sz_t;
    const bitset_sz_t last_node_index =
        static_cast<bitset_sz_t>(node_positions.n_cols) - 1;
    // Check underflow
    uAssertBounds(
        last_node_index, 0, static_cast<bitset_sz_t>(node_positions.n_cols));

    // Test each node for ligation contacts (notice last node is *not*
    // tested within loop as we are only iterating over upper triangle)
    for (bitset_sz_t i = 0; i < last_node_index; ++i) {
        // Broad phase
        const uReal* const point_i = node_positions.colptr(U_TO_MAT_SZ_T(i));
        uAssertBounds(i, 0, static_cast<bitset_sz_t>(node_radii.n_elem));
        const uReal radius_i = node_radii.at(U_TO_MAT_SZ_T(i));
        const uReal query_radius = radius_i + ki_dist;
        broad_phase.filter<uWrappedBitset>(
            mask, point_i, query_radius, broad_cell_length, broad_radius);
        // Avoid checking self
        uAssert(mask.test(i));
        mask.set(i, false);
        uAssert(!mask.test(i));

        const elem_id_t elem_id_i = static_cast<elem_id_t>(i);
        if (keep_bonded) {
            // Add self to ligation profile
            profile.push_back(std::make_pair(elem_id_i, elem_id_i));
        }

        // Narrow phase against upper triangle
        for (bitset_sz_t j =
                 mask.find_next(i + static_cast<bitset_sz_t>(!keep_bonded));
             j != uWrappedBitset::npos;
             j = mask.find_next(j)) {
            // Compute ligation threshold
            uAssertBounds(
                j, i + 1, static_cast<bitset_sz_t>(node_positions.n_cols));
            const uReal* const point_j =
                node_positions.colptr(U_TO_MAT_SZ_T(j));
            uAssertBounds(j, 0, static_cast<bitset_sz_t>(node_radii.n_elem));
            const uReal radius_j = node_radii.at(U_TO_MAT_SZ_T(j));
            thresh2 = radius_i + radius_j + ki_dist;
            thresh2 *= thresh2;
            uAssert(thresh2 > U_TO_REAL(0.0));

            // Compute squared Euclidean distance between node centers
            U_DIST2(dist2, point_i, point_j);

            // Check if within ligation threshold
            if (dist2[uDim_dist] <= thresh2) {
                // Append ligation contact
                const elem_id_t elem_id_j = static_cast<elem_id_t>(j);
                profile.push_back(std::make_pair(elem_id_i, elem_id_j));
            }
        }  // End iteration over subsequent node indices
    }      // End iteration over monomer nodes
    if (keep_bonded) {
        // Add self ligation for last monomer node
        const elem_id_t elem_id_last_node =
            static_cast<elem_id_t>(last_node_index);
        profile.push_back(std::make_pair(elem_id_last_node, elem_id_last_node));
    }
}

//****************************************************************************
// Export
//****************************************************************************

/**
 * @return uTRUE if ligc export indicated, uFALSE o/w
 */
uBool uLigcCore::should_export(const uUInt export_flags) {
    return (export_flags &
            (uExportLigcCsv | uExportLigcCsvGz | uExportLigcCsvBulk |
             uExportLigcCsvBulkGz | uExportLigcBin)) != U_TO_UINT(0);
}

/**
 * Export ligation tuples as binary format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight - log importance weight
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_bin(std::ostream& out,
                            const profile_t& profile,
                            const uReal log_weight) {
    /**
     * LIGC BINARY FORMAT SPECIFICATION
     *
     * VERSION: 1
     *
     *  Note, all multi-bytes integral values are in same endian format as
     *  machine doing the exporting!
     *
     *  BYTE START,          BYTE END,                 TOTAL BYTES,   VALUE,                 DESCRIPTION
     *  0,                   3,                        4,             "LIGC",                First 4 bytes are character string "LIGC"
     *  4,                   11,                       8,             <version>,             Next 8 bytes encode unsigned integer version
     *  12,                  19,                       8,             <log weight>,          Next 8 bytes encode sample log weight (base e) in
     *                                                                                       double format (non-portable)
     *  20,                  27,                       8,             <element index bytes>, Next 8 bytes encode unsigned integer specifying
     *                                                                                       number of bytes needed to represent each element
     *                                                                                       index (identifier)
     *  28,                  35,                       8,             <number of tuples>,    Next 8 bytes encode total number of ligation tuples
     *                                                                                       as unsigned integer
     *  36,                  36 + (num_tup*eib) - 1,   (num_tup*eib), <i elements>,          i values for all (i,j) ligation tuples where
     *                                                                                       'num_tup' is number of ligation tuples and 'eib' is
     *                                                                                       'element index bytes'
     *  36 + (num_tup*eib),  36 + 2*(num_tup*eib) - 1, (num_tup*eib), <j elements>,          j values for all (i,j) ligation tuples
     */

    if (!out) {
        return uFALSE;
    }

    // 4 bytes: "LIGC" header
    const size_t HEADER_BYTES = sizeof(U_LIGC_BIN_HEADER) - 1;
    const char HEADER[HEADER_BYTES + 1] = U_LIGC_BIN_HEADER;
    out.write(HEADER, HEADER_BYTES);
    // 8 bytes: version
    const uint64_t VERSION = U_LIGC_BIN_VERSION;
    uAssert(sizeof(uint64_t) == 8);
    out.write((char*)(&VERSION), sizeof(uint64_t));
    // 8 bytes: log weight
    double LOG_WEIGHT = static_cast<double>(log_weight);
    uAssert(sizeof(double) == 8);
    out.write((char*)(&LOG_WEIGHT), sizeof(double));
    // 8 bytes: element index bytes
    const uint64_t EIB = U_LIGC_BIN_EIB;
    uAssert(sizeof(uint64_t) == 8);
    out.write((char*)(&EIB), sizeof(uint64_t));
    // 8 bytes: number of ligation tuples
    const uint64_t NUM_TUP = profile.size();
    out.write((char*)(&NUM_TUP), sizeof(uint64_t));
    // (NUM_TUP * EIB) bytes: i-values from (i,j) tuples
    if (NUM_TUP < 1) {
        // Early out if no tuples to write
        return uTRUE;
    }
    uAssert(U_LIGC_BIN_EIB == sizeof(U_LIGC_BIN_EIT));
    const size_t IJ_BYTES = NUM_TUP * U_LIGC_BIN_EIB;
    uMatrixUtils::Col<U_LIGC_BIN_EIT> IJ_BUFF(NUM_TUP);
    for (uint64_t itr = 0; itr < NUM_TUP; ++itr) {
        IJ_BUFF.at(itr) = profile[itr].first;
    }
    out.write((char*)(IJ_BUFF.memptr()), IJ_BYTES);
    // (NUM_TUP * EIB) bytes: j-values from (i,j) tuples
    for (uint64_t itr = 0; itr < NUM_TUP; ++itr) {
        IJ_BUFF.at(itr) = profile[itr].second;
    }
    out.write((char*)(IJ_BUFF.memptr()), IJ_BYTES);
    return uTRUE;
}

/**
 * Export ligation tuples as binary format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight_str - log importance weight comment
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_bin(std::ostream& out,
                            const profile_t& profile,
                            const std::string& log_weight_comment) {

    // Extract log weight from comment string
    const size_t LOG_WEIGHT_OFF = 14;
    uReal LOG_WEIGHT = U_TO_REAL(0.0);
    uAssert(log_weight_comment.size() > LOG_WEIGHT_OFF);
    if (log_weight_comment.size() > LOG_WEIGHT_OFF) {
        const std::string LOG_WEIGHT_STR =
            log_weight_comment.substr(LOG_WEIGHT_OFF);
        LOG_WEIGHT = uStringUtils::string_as_T<uReal>(LOG_WEIGHT_STR);
    }
    return export_bin(out, profile, LOG_WEIGHT);
}

/**
 * Export ligation tuples as CSV format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight - log importance weight
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_csv(std::ostream& out,
                            const profile_t& profile,
                            const uReal log_weight) {
    const std::string log_weight_comment =
        uChromatinExportCsv::to_log_weight_comment(log_weight);
    return export_csv(out, profile, log_weight_comment);
}

/**
 * Export ligation tuples as CSV format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight_str - log importance weight comment
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_csv(std::ostream& out,
                            const profile_t& profile,
                            const std::string& log_weight_comment) {
    if (!out) {
        return uFALSE;
    }

    // Export log importance weight
    out << log_weight_comment << "\n";

    // Export contact tuples
    const size_t num_contacts = profile.size();
    for (size_t i = 0; i < num_contacts; ++i) {
        const contact_t& tup(profile[i]);
        out << tup.first << "," << tup.second << "\n";
    }

    return uTRUE;
}

/**
 * Export ligation tuples as gzip-compressed CSV format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight - log importance weight
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_csv_gz(zstr::ofstream& out,
                               const profile_t& profile,
                               const uReal log_weight) {
    if (!out) {
        return uFALSE;
    }
    // Output raw CSV to string stream
    std::stringstream ss;
    if (!export_csv(ss, profile, log_weight)) {
        return uFALSE;
    }
    // Rewind string stream to serve as input to compression
    // https://stackoverflow.com/questions/18754340/pass-ostream-into-istream
    ss.seekg(0, std::ios::beg);
    // Compress string stream
    uStreamUtils::cat(ss, out);
    return uTRUE;
}

/**
 * Export ligation tuples as gzip-compressed CSV format
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param out - output stream
 * @param profile - ligation profile
 * @param log_weight_str - log importance weight comment
 * @return TRUE if export okay, FALSE o/w
 */
uBool uLigcCore::export_csv_gz(zstr::ofstream& out,
                               const profile_t& profile,
                               const std::string& log_weight_comment) {
    if (!out) {
        return uFALSE;
    }
    // Output raw CSV to string stream
    std::stringstream ss;
    if (!export_csv(ss, profile, log_weight_comment)) {
        return uFALSE;
    }
    // Rewind string stream to serve as input to compression
    // https://stackoverflow.com/questions/18754340/pass-ostream-into-istream
    ss.seekg(0, std::ios::beg);
    // Compress string stream
    uStreamUtils::cat(ss, out);
    return uTRUE;
}

//////////////////////////////////////////////////////////////////////////
// Import
//////////////////////////////////////////////////////////////////////////

/**
 * Imports binary formatted ligc data and log weight
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param profile - imported ligation tuples, always cleared
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_bin(std::istream& in,
                            profile_t& profile,
                            uReal& log_weight) {
    if (!in) {
        return on_import_error(profile, log_weight);
    }
    // Import log weight
    if (!import_bin(in, log_weight)) {
        return on_import_error(profile, log_weight);
    }
    // 8 bytes: element index bytes
    uint64_t EIB;
    uAssert(sizeof(uint64_t) == 8);
    if (!in.read((char*)(&EIB), sizeof(uint64_t))) {
        return on_import_error(profile, log_weight);
    }
    if (EIB != U_LIGC_BIN_EIB) {
        return on_import_error(profile, log_weight);
    }
    // 8 bytes: number of ligation tuples
    uint64_t NUM_TUP;
    if (!in.read((char*)(&NUM_TUP), sizeof(uint64_t))) {
        return on_import_error(profile, log_weight);
    }
    // (NUM_TUP * EIB) bytes: i-values from (i,j) tuples
    if (NUM_TUP < 1) {
        // Early out if no tuples to write
        profile.clear();
        return uTRUE;
    }
    profile.resize(NUM_TUP);
    const size_t IJ_BYTES = NUM_TUP * EIB;
    uAssert(EIB == sizeof(U_LIGC_BIN_EIT));
    uMatrixUtils::Col<U_LIGC_BIN_EIT> IJ_BUFF(NUM_TUP);
    if (!in.read((char*)(IJ_BUFF.memptr()), IJ_BYTES)) {
        return on_import_error(profile, log_weight);
    }
    for (uint64_t itr = 0; itr < NUM_TUP; ++itr) {
        profile[itr].first = IJ_BUFF.at(itr);
    }
    // (NUM_TUP * EIB) bytes: j-values from (i,j) tuples
    if (!in.read((char*)(IJ_BUFF.memptr()), IJ_BYTES)) {
        return on_import_error(profile, log_weight);
    }
    for (uint64_t itr = 0; itr < NUM_TUP; ++itr) {
        profile[itr].second = IJ_BUFF.at(itr);
    }
    // Signal success
    return uTRUE;
}

/**
 * Imports log weight from binary formatted ligc data
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_bin(std::istream& in, uReal& log_weight) {
    // 4 bytes: "LIGC" header
    const size_t HEADER_BYTES = sizeof(U_LIGC_BIN_HEADER) - 1;
    char HEADER[HEADER_BYTES + 1];
    if (!in.read(HEADER, HEADER_BYTES)) {
        return on_import_error(log_weight);
    }
    HEADER[HEADER_BYTES] = '\0';
    if (std::string(HEADER) != std::string(U_LIGC_BIN_HEADER)) {
        return on_import_error(log_weight);
    }
    // 8 bytes: version
    uint64_t VERSION;
    uAssert(sizeof(uint64_t) == 8);
    if (!in.read((char*)(&VERSION), sizeof(uint64_t))) {
        return on_import_error(log_weight);
    }
    if (VERSION != U_LIGC_BIN_VERSION) {
        return on_import_error(log_weight);
    }
    // 8 bytes: log weight
    double LOG_WEIGHT;
    uAssert(sizeof(double) == 8);
    if (!in.read((char*)(&LOG_WEIGHT), sizeof(double))) {
        return on_import_error(log_weight);
    }
    log_weight = U_TO_REAL(LOG_WEIGHT);
    // Signal success
    return uTRUE;
}

/**
 * Imports CSV formatted ligc data and log weight
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param profile - imported ligation tuples, always cleared
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_csv(std::istream& in,
                            profile_t& profile,
                            uReal& log_weight) {
    if (!in) {
        return on_import_error(profile, log_weight);
    }
    // Import log weight
    if (!import_csv(in, log_weight)) {
        return on_import_error(profile, log_weight);
    }
    // Read tabular data set
    uParserTable table;
    table.read(in, uFALSE /*has_header*/, uFALSE /* is_jagged*/);
    const uUInt NUM_TUP = table.num_rows();
    if (NUM_TUP < U_TO_UINT(1)) {
        // Early out, no ligation tuples
        profile.clear();
        return uTRUE;
    }
    // Allocate profile
    profile.resize(NUM_TUP);
    // Parse ligation tuples
    enum { U_LIGC_CSV_COL_I = 0, U_LIGC_CSV_COL_J, U_LIGC_CSV_COL_NUM };
    for (uUInt i = 0; i < NUM_TUP; ++i) {
        if (table.num_cols(i) != U_TO_UINT(U_LIGC_CSV_COL_NUM)) {
            // Unexpected number of columns
            return on_import_error(profile, log_weight);
        }
        contact_t& tup(profile[i]);
        tup.first =
            table.get_uint_naive<contact_t::first_type>(i, U_LIGC_CSV_COL_I);
        tup.second =
            table.get_uint_naive<contact_t::second_type>(i, U_LIGC_CSV_COL_J);
    }
    // Signal success
    return uTRUE;
}

/**
 * Imports log weight from CSV formatted ligc data
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_csv(std::istream& in, uReal& log_weight) {
    // Parse log weight - assumed to be among first set of comment lines
    const std::string CSV_LOG_WEIGHT_PREFIX(
        U_CHROMATIN_EXPORT_CSV_LOG_WEIGHT_PREFIX);
    const std::string CSV_COMMENT_PREFIX(U_CHROMATIN_EXPORT_CSV_COMMENT_PREFIX);
    std::string log_weight_str;
    uBool b_parse_okay = uFALSE;
    do {
        log_weight_str.clear();
        std::getline(in, log_weight_str);
        // Verify log weight string matches expected pattern
        b_parse_okay =
            uStringUtils::starts_with(log_weight_str, CSV_LOG_WEIGHT_PREFIX);
    } while ((!b_parse_okay) &&
             uStringUtils::starts_with(log_weight_str, CSV_COMMENT_PREFIX) &&
             in.good());
    if (!b_parse_okay) {
        // Error parsing log weight
        return on_import_error(log_weight);
    }
    // Trim prefix
    log_weight_str.erase(0, CSV_LOG_WEIGHT_PREFIX.length());
    // Trim leading and trailing whitespace
    boost::trim(log_weight_str);
    // Convert to real type
    log_weight = uStringUtils::string_as_T<uReal>(log_weight_str);
    // Signal success
    return uTRUE;
}

/**
 * Imports gzip-compressed, CSV formatted ligc data and log weight
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param profile - imported ligation tuples, always cleared
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_csv_gz(zstr::ifstream& in,
                               profile_t& profile,
                               uReal& log_weight) {
    if (!in) {
        return on_import_error(profile, log_weight);
    }
    // Decompress to string stream
    std::stringstream ss;
    uStreamUtils::cat(in, ss);
    // Rewind decompressed string stream to serve as input
    // https://stackoverflow.com/questions/18754340/pass-ostream-into-istream
    ss.seekg(0, std::ios::beg);
    // Defer to plain-text CSV importer
    return import_csv(ss, profile, log_weight);
}

/**
 * Imports log weight from gzip-compressed, CSV formatted ligc data
 * @WARNING - DOES NOT CLOSE STREAM HANDLE
 * @param in - input stream
 * @param log_weight- imported log weight
 * @return TRUE if import okay, FALSE o/w
 */
uBool uLigcCore::import_csv_gz(zstr::ifstream& in, uReal& log_weight) {
    if (!in) {
        return on_import_error(log_weight);
    }
    // Decompress to string stream until first '\n' character encountered
    // (assumes log weight is encoded in first newline of file)
    const std::streamsize BUFF_SIZE = 1 << 8;
    // Note: buffer is actually BUFF_SIZE + 1!
    char BUFF[BUFF_SIZE + 1];
    std::streamsize cnt = 0;
    const char* pch = NULL;
    std::stringstream ss;
    do {
        // Cat buffer
        in.read(BUFF, BUFF_SIZE);
        cnt = in.gcount();
        // NULL terminate char buffer to allow strchr search
        uAssertBoundsInc(cnt, 0, BUFF_SIZE);
        BUFF[cnt] = '\0';
        if (cnt > 0) {
            ss.write(BUFF, cnt);
            // Search for newline
            pch = strchr(BUFF, '\n');
        }
    } while ((pch == NULL) && (cnt > 0) && (in.good()));
    // Check newline encountered
    if (pch == NULL) {
        // Unable to find newline character
        return on_import_error(log_weight);
    }
    // Rewind decompressed string stream to serve as input
    // https://stackoverflow.com/questions/18754340/pass-ostream-into-istream
    ss.seekg(0, std::ios::beg);
    // Defer to plain-text CSV importer
    return import_csv(ss, log_weight);
}
