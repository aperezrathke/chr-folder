//****************************************************************************
// uChromatinExportArgs.h
//****************************************************************************

/**
 * Structure for grouping common export arguments
 */

#ifndef uChromatinExportArgs_h
#define uChromatinExportArgs_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uTypes.h"

#include <string>
#include <vector>

//****************************************************************************
// Common chromatin exporter arguments
//****************************************************************************

/**
 * Struct for passing common arguments used by various exporters
 */
typedef struct {
    /**
     * A matrix of node positions where each node is a column:
     *      x1 x2 ... xN
     *      y1 y2 ... yN
     *      z1 z2 ... zN
     */
    const uMatrix* p_node_positions;

    /**
     * Vector of node radii, must have number of elements be same as number of
     *  columns of node_positions.
     */
    const uVecCol* p_node_radii;

    /**
     * Number of nodes defining each locus chain. Must sum to number of
     *  columns of node_positions. Each locus is assumed to be sequentially
     *  ordered within node_positions.
     */
    const std::vector<uUInt>* p_max_num_nodes_at_locus;

    /**
     * Radius of confining nuclear sphere
     */
    uReal nuclear_radius;

    /**
     * The log bias of the sample.
     */
    uReal log_weight;

    /**
     * If TRUE, extended information will be written (exporter specific)
     */
    uBool extended;

    /**
     * If TRUE, enable export of nuclear shell, MAY NOT be supported by all
     *  format or shape types
     */
    uBool nuclear_shell;

    /**
     * Certain export formats require coordinate scaling (exporter specific)
     */
    uReal scale;

    /**
     * Decimal precision (exporter specific)
     */
    int precision;

    // Arguments for exporting interactions

    /**
     * If TRUE, chr-chr interaction info will be written (exporter specific)
     */
    uBool intr_chr;

    /**
     * 2 x M matrix where M is number of fragments, defines the node spans of
     * a chromatin fragment that may participate in chr-chr interactions
     */
    const uUIMatrix* p_intr_chr_frags;

    /**
     * 2 x N matrix where N is number of knock-in chr-chr interaction pairs,
     * each column is an interaction pair, each row is column index into
     * fragments (frags) matrix
     */
    const uUIMatrix* p_intr_chr_kin;

    /**
     * 2 x N matrix where N is number of knock-out chr-chr interaction pairs,
     * each column is an interaction pair, each row is column index into
     * fragments (frags) matrix
     */
    const uUIMatrix* p_intr_chr_ko;

    /**
     * If TRUE, lamina interaction info will be written (exporter specific)
     */
    uBool intr_lam;

    /**
     * 2 x M matrix where M is number of fragments, defines the node spans of
     * a chromatin fragment that may participate in lamina interactions
     */
    const uUIMatrix* p_intr_lam_frags;

    /**
     * Vector of knock-in lamina interactions
     */
    const uUIVecCol* p_intr_lam_kin;

    /**
     * Vector of knock-out lamina interactions
     */
    const uUIVecCol* p_intr_lam_ko;

    /**
     * If TRUE, nuclear body interaction info will be written (exporter
     * specific)
     */
    uBool intr_nucb;

    /**
     * 2 x M matrix where M is number of fragments, defines the node spans of
     * a chromatin fragment that may participate in nucb-chr interactions
     */
    const uUIMatrix* p_intr_nucb_frags;

    /**
     * 2 x N matrix where N is number of knock-in nucb-chr interaction pairs,
     * each column is an interaction pair, first row is column index into
     * chromatin fragments (frags) matrix, second row is column index into
     * nuclear body centers (centers) matrix
     */
    const uUIMatrix* p_intr_nucb_kin;

    /**
     * 2 x N matrix where N is number of knock-out nucb-chr interaction pairs,
     * each column is an interaction pair, first row is column index into
     * chromatin fragments (frags) matrix, second row is column index into
     * nuclear body centers (centers) matrix
     */
    const uUIMatrix* p_intr_nucb_ko;

    /**
     * 3 x B matrix where B is number of nuclear bodies, defines nuclear body
     * (x, y, z) centers
     */
    const uMatrix* p_intr_nucb_centers;

    /**
     * Vector of length B defining non-negative radii at each nuclear body
     * @WARNING - BODIES MAY BE POINT MASSES OF RADIUS 0!
     */
    const uVecCol* p_intr_nucb_radii;
} uChromatinExportArgs_t;

/**
 * Utility to help make sure sample has reasonable format
 */
extern uBool chromatin_export_check(const uChromatinExportArgs_t& args);

#endif  // uChromatinExportArgs_h
