//****************************************************************************
// uThread.h
//****************************************************************************

/**
 * Our threading specification
 */

#ifndef uThread_h
#define uThread_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uLogf.h"

#include <time.h>

#ifdef U_BUILD_ENABLE_THREADS

#include "uAssert.h"

#include <vector>

#ifdef U_BUILD_CXX_11
    #include <atomic>
    #include <condition_variable>
    #include <thread>
    #define uTh std
#else
    #include <boost/atomic.hpp>
    #include <boost/thread.hpp>
    #include <boost/thread/condition_variable.hpp>
    #define uTh boost
#endif  // U_BUILD_CXX_11

//****************************************************************************
// Defines
//****************************************************************************

// The number of threads to initialize by default
#define U_DEFAULT_NUM_THREADS \
    (std::max<uUInt>(uTh::thread::hardware_concurrency(), 1))

//****************************************************************************
// Typedefs
//****************************************************************************

typedef uTh::thread uThread_t;
typedef uTh::mutex uMutex_t;
typedef uTh::unique_lock<uMutex_t> uLock_t;
typedef uTh::lock_guard<uMutex_t> uScopedLock_t;
typedef uTh::condition_variable uConditionVariable_t;
typedef uTh::atomic<unsigned int> uAtomicUInt;
typedef uTh::atomic<time_t> uAtomicTime_t;
typedef uTh::atomic<double> uAtomicDouble_t;

/**
 * Handle atomic add for double type
 * @param atomic_a - atomic double to increment by b
 * @param double_b - amount to increment a by
 * https://stackoverflow.com/questions/23116279/how-to-perform-basic-operations-with-stdatomic-when-the-type-is-not-integral
 */
#define U_ATOMIC_ADD_DOUBLE(atomic_a, double_b)                                \
    do {                                                                       \
        double a_loaded = atomic_a.load();                                     \
        while (!atomic_a.compare_exchange_weak(a_loaded, a_loaded + double_b)) \
            ;                                                                  \
    } while (0)

//****************************************************************************
// Parallel mapper
//****************************************************************************

#include "uParallelMapper.h"

//****************************************************************************
// TLS - Our home-brewed thread local storage system
//****************************************************************************

/**
 * Am rolling own TLS system because I don't trust boost TLS or
 * std::thread::id to not trap to the kernel.
 */

#define U_MAIN_THREAD_ID_0_ARG 0

#define U_MAIN_THREAD_ID_ARG , U_MAIN_THREAD_ID_0_ARG

#define U_THREAD_ID_VAR_NAME tId__

#define U_THREAD_ID_0_ARG U_THREAD_ID_VAR_NAME

#define U_THREAD_ID_ARG , U_THREAD_ID_VAR_NAME

#define U_THREAD_ID_0_PARAM const uUInt U_THREAD_ID_VAR_NAME

#define U_THREAD_ID_PARAM , U_THREAD_ID_0_PARAM

#define U_NUM_WORKER_THREADS_VAR_NAME num_worker_threads

#define U_EXTERN_NUM_WORKER_THREADS extern uUInt U_NUM_WORKER_THREADS_VAR_NAME;

#define U_DECLARE_NUM_WORKER_THREADS uUInt U_NUM_WORKER_THREADS_VAR_NAME;

#define U_PARALLEL_MAPPER_VAR_NAME par_map

#define U_EXTERN_PARALLEL_MAPPER \
    extern uParallelMapper U_PARALLEL_MAPPER_VAR_NAME;

#define U_DECLARE_PARALLEL_MAPPER uParallelMapper U_PARALLEL_MAPPER_VAR_NAME;

#define uParMap uG::U_PARALLEL_MAPPER_VAR_NAME

#define U_SET_NUM_WORKER_THREADS(config, num) \
    config->set_option(uOpt_num_worker_threads, num)

#define U_LOG_NUM_WORKER_THREADS \
    uLogf("Available worker threads: %u\n", uG::U_NUM_WORKER_THREADS_VAR_NAME)

#else  // !defined(U_BUILD_ENABLED_THREADS)

typedef unsigned int uAtomicUInt;

typedef time_t uAtomicTime_t;

typedef double uAtomicDouble_t;

#define U_ATOMIC_ADD_DOUBLE(atomic_a, double_b) \
    do {                                        \
        atomic_a += double_b;                   \
    } while (0)

#define U_MAIN_THREAD_ID_0_ARG

#define U_MAIN_THREAD_ID_ARG

#define U_THREAD_ID_0_ARG

#define U_THREAD_ID_ARG

#define U_THREAD_ID_0_PARAM

#define U_THREAD_ID_PARAM

#define U_EXTERN_NUM_WORKER_THREADS

#define U_DECLARE_NUM_WORKER_THREADS

#define U_EXTERN_PARALLEL_MAPPER

#define U_DECLARE_PARALLEL_MAPPER

#define U_SET_NUM_WORKER_THREADS(config, num) ((void)0)

#define U_LOG_NUM_WORKER_THREADS ((void)0)

#endif  // U_BUILD_ENABLE_THREADS

// Include default thread local storage accessors
#include "uThreadTLS.inl"

#endif  // uThread_h
