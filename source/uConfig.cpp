//****************************************************************************
// uConfig.cpp
// ****************************************************************************

/**
 * Stores user configuration, presents uniform interface such that client
 * does not need to consider source of parameter (command line vs INI)
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uCmdOptsMap.h"  // for parsing command line
#include "uFilesystem.h"
#include "uOpts.h"
#include "uStringUtils.h"

#include <limits>
#include <stdio.h>

//****************************************************************************
// Anonymous namespace utilities
//****************************************************************************

namespace {
/**
 * Utility to assign a value at a parameter index even if index exceeds
 * the current array bounds. If this is the case, then vector will be
 * resized so that the operation becomes legal.
 *
 * @param vec - vector which will have element at index overwritten by val
 * @param index - 0-based index to assign val to
 * @param val - value to assign to vec[index]
 */
void assign_at(std::vector<uUInt>& vec, const size_t index, const uUInt val) {
    if (vec.size() <= index) {
        vec.resize(index + 1, 0);
    }
    vec[index] = val;
}

/**
 * Utility updates export flag according to precedence:
 *  If unary key present, then export is always enabled
 *  Else, if binary key present, then enable if value '1'
 */
template <class t_opt_provider>
void update_export_flag(uSpConfig_t config,
                        const uOptE opt_unary,
                        const uOptE opt_binary,
                        const uExportBits bit,
                        const t_opt_provider& opts,
                        const uOpt_get_key_func get_key) {

    if (opts.key_exists(get_key(opt_unary))) {
        config->export_flags |= U_TO_UINT(1) << U_TO_UINT(bit);
    } else {
        const std::string binary_key(get_key(opt_binary));
        if (opts.key_exists(binary_key)) {
            // Clear bit
            config->export_flags &= ~(U_TO_UINT(1) << U_TO_UINT(bit));
            // Determine new bit value
            int result_sgn = 0;
            opts.read_into(result_sgn, binary_key);
            result_sgn = (result_sgn == 1) ? 1 : 0;
            const uUInt result = U_TO_UINT(result_sgn);
            // Set bit
            config->export_flags |= result << U_TO_UINT(bit);
        }
    }
}

/**
 * Utility to read named configuration options that can be specified via
 * command line or INI file.
 *
 * @param config - structure for storing the read user options
 * @param opts - the interface to read the user options from
 */
template <class t_opt_provider>
void read_named_config(uSpConfig_t config,
                       const t_opt_provider& opts,
                       const uOpt_get_key_func get_key) {
    // Stores the number of nodes for the current locus
    std::string locus_num_nodes_key;
    // The current locus being parsed
    uUInt locus;
    uUInt locus_num_nodes;

    // Parse number of nodes assigned to each locus.
    // First, check if num_nodes is specified without a locus identifier
    // suffix.
    std::string key_prefix(get_key(uOpt_num_nodes));
    if (opts.read_into(locus_num_nodes, key_prefix)) {
        assign_at(config->num_nodes, 0, locus_num_nodes);
    }

    // Now, parse num_nodes keys that do have a loci identifier suffix.
    key_prefix = key_prefix + "_";
    locus = 0;
    locus_num_nodes_key = key_prefix + u2Str(locus);
    while (opts.read_into(locus_num_nodes, locus_num_nodes_key)) {
        assign_at(config->num_nodes, locus, locus_num_nodes);
        locus_num_nodes_key = key_prefix + u2Str(++locus);
    }

    // Parse remaining scalar values
    opts.resolve_path(config->output_dir, get_key(uOpt_output_dir));
    uFs_normalize_dir_path(config->output_dir);
    opts.read_into(config->job_id, get_key(uOpt_job_id));
    opts.read_into(config->num_unit_sphere_sample_points,
                   get_key(uOpt_num_unit_sphere_sample_points));
    opts.read_into(config->ensemble_size, get_key(uOpt_ensemble_size));

    // Parse maximum number of trials. A trial is a complete run of the
    // the simulation. However, after the run, there may be loci chains
    // that died (were unable to proceed because they grew into a corner).
    // Each trial will attempt to generate the remaining chains until the
    // target ensemble size is reached or the maximum trial count is
    // reached - whichever comes first.
    const std::string max_trials_key(get_key(uOpt_max_trials));
    if (opts.key_exists(max_trials_key)) {
        // @HACK - only assign max trials if key exists. The default is set in
        // uConfig::clear() method
        int max_trials = 0;
        opts.read_into(max_trials, max_trials_key);
        config->max_trials = (max_trials <= 0)
                                 ? std::numeric_limits<uUInt>::max()
                                 : U_TO_UINT(max_trials);
    }

    // Set export flags
    update_export_flag(config,
                       uOpt_export_csv_unary,
                       uOpt_export_csv_binary,
                       uExportBitCsv,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_csv_gz_unary,
                       uOpt_export_csv_gz_binary,
                       uExportBitCsvGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_csv_bulk_unary,
                       uOpt_export_csv_bulk_binary,
                       uExportBitCsvBulk,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_csv_bulk_gz_unary,
                       uOpt_export_csv_bulk_gz_binary,
                       uExportBitCsvBulkGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pdb_unary,
                       uOpt_export_pdb_binary,
                       uExportBitPdb,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pdb_gz_unary,
                       uOpt_export_pdb_gz_binary,
                       uExportBitPdbGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pdb_bulk_unary,
                       uOpt_export_pdb_bulk_binary,
                       uExportBitPdbBulk,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pdb_bulk_gz_unary,
                       uOpt_export_pdb_bulk_gz_binary,
                       uExportBitPdbBulkGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pml_unary,
                       uOpt_export_pml_binary,
                       uExportBitPml,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pml_gz_unary,
                       uOpt_export_pml_gz_binary,
                       uExportBitPmlGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pml_bulk_unary,
                       uOpt_export_pml_bulk_binary,
                       uExportBitPmlBulk,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_pml_bulk_gz_unary,
                       uOpt_export_pml_bulk_gz_binary,
                       uExportBitPmlBulkGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_log_weights_unary,
                       uOpt_export_log_weights_binary,
                       uExportBitLogWeights,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_extended_unary,
                       uOpt_export_extended_binary,
                       uExportBitExtended,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_nucleus_unary,
                       uOpt_export_nucleus_binary,
                       uExportBitNucleus,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_intr_chr_unary,
                       uOpt_export_intr_chr_binary,
                       uExportBitIntrChr,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_intr_lam_unary,
                       uOpt_export_intr_lam_binary,
                       uExportBitIntrLam,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_intr_nucb_unary,
                       uOpt_export_intr_nucb_binary,
                       uExportBitIntrNucb,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_csv_unary,
                       uOpt_export_ligc_csv_binary,
                       uExportBitLigcCsv,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_csv_gz_unary,
                       uOpt_export_ligc_csv_gz_binary,
                       uExportBitLigcCsvGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_csv_bulk_unary,
                       uOpt_export_ligc_csv_bulk_binary,
                       uExportBitLigcCsvBulk,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_csv_bulk_gz_unary,
                       uOpt_export_ligc_csv_bulk_gz_binary,
                       uExportBitLigcCsvBulkGz,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_bin_unary,
                       uOpt_export_ligc_bin_binary,
                       uExportBitLigcBin,
                       opts,
                       get_key);
    update_export_flag(config,
                       uOpt_export_ligc_bonded_unary,
                       uOpt_export_ligc_bonded_binary,
                       uExportBitLigcBonded,
                       opts,
                       get_key);
}

/**
 * Utility to merge command line options. If should_clear is uTRUE, then
 * out_opts = in_opts (out_opts will be exact copy of in_opts). Else, if
 * should_clear is uFALSE, then all existing in_opts keys override any
 * corresponding keys in out_opts; however keys unique to out_opts will
 * retain their values.
 *
 * @param out_opts - user options to override
 * @param in_opts - options provider to override out_opts with
 * @param should_clear - If uTRUE, then out_opts = in_opts. Else, out_opts
 *  will be merged with in_opts with in_opts values overriding any common
 *  keys.
 */
void override_config(uCmdOptsMap& out_opts,
                     const uCmdOptsMap& in_opts,
                     const uBool should_clear) {
    if (should_clear) {
        // Completely overwrite
        out_opts = in_opts;
    } else {
        // Override common keys
        std::vector<std::string> keys;
        std::string key;
        std::string val;
        in_opts.get_keys(keys);
        for (std::vector<std::string>::iterator it = keys.begin();
             it != keys.end();
             it++) {
            key = *it;
            uVerify(in_opts.read_into(val, key));
            out_opts.set_option(key, val);
        }
    }
}

/**
 * Determines if archetype exists in INI file or command-line (optional) - if
 *  so, then loads INI archetype file. If command-line is valid and specifies
 *  archetype, it will take priority over INI file archetype
 * @param arch - Output archetype configuration file (WILL ALWAYS BE CLEARED!)
 * @param ini - Already initialized configuration file (MUST NOT BE SAME
 *  MEMORY LOCATION AS 'arch'!)
 * @param cmd - Optional command-line options
 * @return TRUE if archetype found and loaded, FALSE o/w
 */
uBool load_arch(uConfigFile& arch,
                const uConfigFile& ini,
                const uCmdOptsMap* const cmd = NULL) {
    // Assert addresses are unique
    uAssert(((void*)&arch) != ((void*)&ini));
    // Check if archetype path is specified, defer to command-line if present
    std::string arch_path;
    // Note, "short-circuit evaluation" applies here: if option is present on
    // command-line, then the INI path resolution will not be called
    // https://en.wikipedia.org/wiki/Short-circuit_evaluation
    const uBool result =
        (cmd && cmd->resolve_path(arch_path, uOpt_get_cmd_switch(uOpt_arch))) ||
        ini.resolve_path(arch_path, uOpt_get_ini_key(uOpt_arch));
    if (result && !arch_path.empty()) {
        // Archetype path found!
        arch = uConfigFile(arch_path);
        return uTRUE;
    }
    // Archetype path not found
    arch = uConfigFile();
    return uFALSE;
}

}  // namespace

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Default constructor
 */
uConfig::uConfig() { clear(); }

/**
 * Sets configuration to default invalid but deterministic configuration
 */
void uConfig::clear() {
    this->output_dir = std::string("");
    this->job_id = std::string("");
    this->max_trials = std::numeric_limits<uUInt>::max();
    this->num_nodes.clear();
    this->num_unit_sphere_sample_points = 0;
    this->ensemble_size = 0;
    this->export_flags = 0;

    this->m_cmd_opts = uCmdOptsMap();
    this->m_cfg_file = uConfigFile();
    this->m_cfg_arch = uConfigFile();

    this->m_parent.reset();
    this->m_child_tr.reset();
    this->m_child_tr_sel.reset();
    this->m_child_qc.reset();
}

/**
 * Initializes from command line
 *
 * @param cmd_opts - command line options
 * @param should_clear - If TRUE, erases existing state in this object
 */
void uConfig::init(const uCmdOptsMap& cmd_opts, const uBool should_clear) {
    // Reset configuration
    if (should_clear) {
        this->clear();
    }

    // Stores complete file path to INI file
    std::string ini_fpath;

    // Parse parameter INI file if specified,
    if (cmd_opts.read_into(ini_fpath, uOpt_get_cmd_switch(uOpt_conf))) {
        m_cfg_file = uConfigFile(ini_fpath);
        load_arch(m_cfg_arch, m_cfg_file, &cmd_opts);
        if (!m_cfg_arch.empty()) {
            // Initialize from INI archetype
            read_named_config(shared_from_this(), m_cfg_arch, uOpt_get_ini_key);
        }
        read_named_config(shared_from_this(), m_cfg_file, uOpt_get_ini_key);
    }

    // Override any option specified on command line
    read_named_config(shared_from_this(), cmd_opts, uOpt_get_cmd_switch);

    // Store command line options
    override_config(m_cmd_opts, cmd_opts, should_clear);

    // See if a child config needs to be initialized
    conditional_init_child_tr();
    conditional_init_child_tr_sel();
    conditional_init_child_qc();
}

/**
 * Overwrite trial runner sub-sim configuration
 * @param child - child configuration
 * @param should_copy_named_vars_to_child - if true, will overwrite any
 *  named variables of the child with the parent named variables
 */
void uConfig::set_child_tr(uSpConfig_t child,
                           const uBool should_copy_named_vars_to_child) {
    this->set_child(m_child_tr, child, should_copy_named_vars_to_child);
}

/**
 * Overwrite trial runner select sub-sim configuration
 * @param child - child configuration
 * @param should_copy_named_vars_to_child - if true, will overwrite any
 *  named variables of the child with the parent named variables
 */
void uConfig::set_child_tr_sel(uSpConfig_t child,
                               const uBool should_copy_named_vars_to_child) {
    this->set_child(m_child_tr_sel, child, should_copy_named_vars_to_child);
}

/**
 * Overwrite quality control sub-sim configuration
 * @param child - child configuration
 * @param should_copy_named_vars_to_child - if true, will overwrite any
 *  named variables of the child with the parent named variables
 */
void uConfig::set_child_qc(uSpConfig_t child,
                           const uBool should_copy_named_vars_to_child) {
    this->set_child(m_child_qc, child, should_copy_named_vars_to_child);
}

/**
 * Overwrite a child configuration
 * @param child_mem_ - member to assign
 * @param child - child configuration
 * @param should_copy_named_vars_to_child - if true, will overwrite any
 *  named variables of the child with the parent named variables
 */
void uConfig::set_child(uSpConfig_t& child_mem_,
                        uSpConfig_t child,
                        const uBool should_copy_named_vars_to_child) {
    child_mem_ = child;
    child->m_parent = shared_from_this();
    if (should_copy_named_vars_to_child) {
        child->copy_named_vars(shared_from_this());
    }
}

/**
 * Conditionally initialize trial runner child config
 */
void uConfig::conditional_init_child_tr() {
    if (!this->conditional_init_child(
            m_child_tr, uOpt_conf_child_tr, U_CONFIG_CHILD_TR_CMDARG_PREFIX)) {
        // DEPRECATED: legacy support for conf_child option
        this->conditional_init_child(
            m_child_tr, uOpt_conf_child, U_CONFIG_CHILD_TR_CMDARG_PREFIX);
    }
}

/**
 * Conditionally initialize trial runner select child config
 */
void uConfig::conditional_init_child_tr_sel() {
    this->conditional_init_child(m_child_tr_sel,
                                 uOpt_conf_child_tr_sel,
                                 U_CONFIG_CHILD_TR_SEL_CMDARG_PREFIX);
}

/**
 * Conditionally initialize quality control child config
 */
void uConfig::conditional_init_child_qc() {
    this->conditional_init_child(
        m_child_qc, uOpt_conf_child_qc, U_CONFIG_CHILD_QC_CMDARG_PREFIX);
}

/**
 * Checks if child configuration should be initialized
 * @param child_mem_ - member to conditionally initialize
 * @param ini_key_ - command key for child configuration file
 * @param cmd_prefix_ - prefix to extract child command line arguments
 * @return uTRUE if child member initialized, uFALSE o/w
 */
uBool uConfig::conditional_init_child(uSpConfig_t& child_mem_,
                                      const uOptE ini_key_,
                                      const char cmd_prefix_) {
    std::string child_ini_fpath;
    // Make sure to avoid checking the parent for a child config - else if
    // parent contains a child, we will infinitely recurse!
    if (this->resolve_path(
            child_ini_fpath, ini_key_, uFALSE /*check_parent*/)) {
        child_mem_ = uSmartPtr::make_shared<uConfig>();
        child_mem_->init(
            shared_from_this(), uConfigFile(child_ini_fpath), cmd_prefix_);
        return uTRUE;
    }
    return uFALSE;
}

/**
 * Actual initialization for a child configuration
 * @param parent - The parent configuration
 * @param cfg_file - Child INI files
 * @param cmd_prefix_ - prefix to extract child command line arguments
 * Note: child command line options are extracted from parent command line
 */
void uConfig::init(uSpConstConfig_t parent,
                   const uConfigFile& cfg_file,
                   const char cmd_prefix_) {
    clear();
    m_parent = parent;
    copy_named_vars(parent);
    // Initialize CMD options
    parent->m_cmd_opts.get_child_cmd(m_cmd_opts, cmd_prefix_);
    // Initialize INI options
    m_cfg_file = cfg_file;
    load_arch(m_cfg_arch, m_cfg_file, &m_cmd_opts);
    if (!m_cfg_arch.empty()) {
        // Initialize named options from INI archetype
        read_named_config(shared_from_this(), m_cfg_arch, uOpt_get_ini_key);
    }
    // Initialize named options from INI file
    read_named_config(shared_from_this(), m_cfg_file, uOpt_get_ini_key);
    // Initialize named options from command-line
    read_named_config(shared_from_this(), m_cmd_opts, uOpt_get_cmd_switch);
    this->conditional_init_child_tr();
    this->conditional_init_child_tr_sel();
    this->conditional_init_child_qc();
}

/**
 * Copies named member variables from parameter config
 * @param cfg - Config object to copy only named variables from
 *  (does not copy pair, value mappings)
 */
void uConfig::copy_named_vars(uSpConstConfig_t cfg) {
    this->output_dir = cfg->output_dir;
    this->job_id = cfg->job_id;
    this->max_trials = cfg->max_trials;
    this->num_nodes = cfg->num_nodes;
    this->num_unit_sphere_sample_points = cfg->num_unit_sphere_sample_points;
    this->ensemble_size = cfg->ensemble_size;
    this->export_flags = cfg->export_flags;
}

/**
 * Prints to stdout any invalid configuration options
 *
 * @return TRUE (1) if configuration is well-formed, FALSE (0) otherwise
 */
uBool uConfig::is_valid() const {
    uBool is_valid_ = uTRUE;

    if (this->output_dir.empty()) {
        printf(
            "Error: invalid configuration - please specify output directory\n");
        is_valid_ = uFALSE;
    }

    if (this->max_trials <= 0) {
        printf(
            "Error: invalid configuration - please specify max trials > 0\n");
        is_valid_ = uFALSE;
    }

    if (this->num_nodes.empty()) {
        printf(
            "Error: invalid configuration - please specify number of nodes to "
            "generate at each loci\n");
        printf("\tExample: -num_nodes_0 = 100\n");
        is_valid_ = uFALSE;
    } else {
        for (size_t i = 0; i < this->num_nodes.size(); ++i) {
            if (this->num_nodes[i] < 1) {
                printf(
                    "Error: invalid configuration - chain at locus %u must "
                    "have 1 or more nodes\n",
                    (unsigned int)i);
                printf("\t Example: -num_nodes_%u = 100\n", (unsigned int)i);
                is_valid_ = uFALSE;
            }
        }
    }

    if (this->num_unit_sphere_sample_points < 1) {
        printf(
            "Error: invalid configuration - please specify valid "
            "num_node_sample_points\n");
        is_valid_ = uFALSE;
    }

    if (this->ensemble_size < 1) {
        printf(
            "Error: invalid configuration - please specify valid "
            "ensemble_size\n");
        is_valid_ = uFALSE;
    }

    return is_valid_;
}

/**
 * Outputs configuration options to <stdout>
 */
void uConfig::print() const {
    const uUInt BAR_CHARS_NUM = U_TO_UINT(32);
    const std::string bar_top(BAR_CHARS_NUM, '~');
    const char* const cbar_top = bar_top.c_str();
    printf("%s\n", cbar_top);
    this->print(0);
    printf("%s\n", cbar_top);
}

/**
 * Utility to print configuration <key,val> pairs
 */
template <typename t_opts>
static void print_keys(const std::string& spaces, const t_opts& opts) {
    std::vector<std::string> keys;
    opts.get_keys(keys);
    std::string key;
    std::string val;
    for (std::vector<std::string>::iterator it = keys.begin(); it != keys.end();
         it++) {
        key = *it;
        uVerify(opts.read_into(val, key));
        printf("%s%s: %s\n", spaces.c_str(), key.c_str(), val.c_str());
    }
}

/**
 * Internal print adjusted for child level
 */
void uConfig::print(const uUInt level) const {
    const uUInt SPACES_PER_LEVEL = U_TO_UINT(2);
    const std::string spaces(level * SPACES_PER_LEVEL, ' ');
    const char* const cspaces = spaces.c_str();
    const int ilevel = (int)level;

    const uUInt BAR_CHARS_NUM = U_TO_UINT(16);
    const std::string bar_named(BAR_CHARS_NUM, '=');
    const char* const cbar_named = bar_named.c_str();
    const std::string bar_cmd(BAR_CHARS_NUM, '-');
    const char* const cbar_cmd = bar_cmd.c_str();
    const std::string bar_ini(BAR_CHARS_NUM, '-');
    const char* const cbar_ini = bar_ini.c_str();
    const std::string bar_tr(BAR_CHARS_NUM, U_CONFIG_CHILD_TR_CMDARG_PREFIX);
    const char* const cbar_tr = bar_tr.c_str();
    const std::string bar_tr_sel(BAR_CHARS_NUM,
                                 U_CONFIG_CHILD_TR_SEL_CMDARG_PREFIX);
    const char* const cbar_tr_sel = bar_tr_sel.c_str();
    const std::string bar_qc(BAR_CHARS_NUM, U_CONFIG_CHILD_QC_CMDARG_PREFIX);
    const char* const cbar_qc = bar_qc.c_str();

    // Named options
    printf("%s%sNAMED OPTS (%d)%s\n", cspaces, cbar_named, ilevel, cbar_named);
    printf("%s-output_dir: %s\n", cspaces, this->output_dir.c_str());
    printf("%s-job_id: %s\n", cspaces, this->job_id.c_str());
    printf("%s-max_trials: %d\n", cspaces, (int)this->max_trials);
    printf("%s-num_unit_sphere_sample_points: %d\n",
           cspaces,
           (int)this->num_unit_sphere_sample_points);
    printf("%s-ensemble_size: %d\n", cspaces, (int)this->ensemble_size);
    printf(
        "%sNumber of loci modeled: %d\n", cspaces, (int)this->num_nodes.size());
    for (size_t i = 0; i < this->num_nodes.size(); ++i) {
        printf(
            "%s-num_nodes_%d: %d\n", cspaces, (int)i, (int)this->num_nodes[i]);
    }
    printf("%sIs configuration valid: %s\n",
           cspaces,
           this->is_valid() ? "yes" : "no");

    // Keyed options: command line
    printf("%s%sCMD OPTS (%d)%s\n", cspaces, cbar_cmd, ilevel, cbar_cmd);
    print_keys(spaces, m_cmd_opts);

    // Keyed options: INI config
    printf("%s%sINI OPTS (%d)%s\n", cspaces, cbar_ini, ilevel, cbar_ini);
    print_keys(spaces, m_cfg_file);

    // Keyed options: INI archetype
    if (!m_cfg_arch.empty()) {
        printf(
            "%s%sINI ARCH OPTS (%d)%s\n", cspaces, cbar_ini, ilevel, cbar_ini);
        print_keys(spaces, m_cfg_arch);
    }

    // Print sub-sim config for trial runner
    if (m_child_tr) {
        printf(
            "%s%sTRIAL RUNNER SUB (%d)%s\n", cspaces, cbar_tr, ilevel, cbar_tr);
        m_child_tr->print(level + 1);
    }
    // Print sub-sum config for trial runner select
    if (m_child_tr_sel) {
        printf("%s%sTRIAL RUNNER SELECT SUB (%d)%s\n",
               cspaces,
               cbar_tr_sel,
               ilevel,
               cbar_tr_sel);
        m_child_tr_sel->print(level + 1);
    }
    // Print sub-sim config for quality control
    if (m_child_qc) {
        printf("%s%sQUALITY CONTROL SUB (%d)%s\n",
               cspaces,
               cbar_qc,
               ilevel,
               cbar_qc);
        m_child_qc->print(level + 1);
    }
}

/**
 * @WARNING - DOES NOT PERFORM PATH RESOLUTION!
 * @out_val - Searches for first parameter value mapped to a matching key
 *  in 'ixs' array where all command line matches have precedence over INI
 * @param ixs - Set of option keys to search for mapped value, the key
 *  priority is as follows: All command line variants have higher priority
 *  than INI, then earlier indices within array have priority over later
 * @param check_parent - If true, then parent configuration (if valid) will
 *  be recursively checked for the set of option keys
 * @return index of option read or ixs.size() if not found
 */
size_t uConfig::read_into(std::string& out_val,
                          const std::vector<uOptE>& ixs,
                          const uBool check_parent) const {
    // Flag no path resolution
    std::vector<uBool> should_resolve(ixs.size(), uFALSE);
    // Defer to common routine
    return this->read_into(out_val, ixs, should_resolve, check_parent);
}

/**
 * PERFORMS PATH RESOLUTION!
 * @out_val - Searches for first parameter value mapped to a matching key
 *  in 'ixs' array where all command line matches have precedence over INI
 * @param ixs - Set of option keys to search for mapped value, the key
 *  priority is as follows: All command line variants have higher priority
 *  than INI, then earlier indices within array have priority over later
 * @param should_resolve - Parallel vector of same size as 'ixs' with i-th
 *  element TRUE if option is a path that must be resolved, FALSE o/w
 * @param check_parent - If true, then parent configuration (if valid) will
 *  be recursively checked for the set of option keys
 * @return index of option read or ixs.size() if not found
 */
size_t uConfig::read_into(std::string& out_val,
                          const std::vector<uOptE>& ixs,
                          const std::vector<uBool>& should_resolve,
                          const uBool check_parent) const {
    uAssert(should_resolve.size() == ixs.size());
    const size_t num_keys = ixs.size();
    // Check command line first
    for (size_t i = 0; i < num_keys; ++i) {
        const uOptE ix = ixs[i];
        if (should_resolve[i]) {
            if (m_cmd_opts.resolve_path(out_val, uOpt_get_cmd_switch(ix))) {
                return i;
            }
        } else {
            if (m_cmd_opts.read_into(out_val, uOpt_get_cmd_switch(ix))) {
                return i;
            }
        }
    }  // end command line check

    // Else check INI file
    for (size_t i = 0; i < num_keys; ++i) {
        const uOptE ix = ixs[i];
        if (should_resolve[i]) {
            if (m_cfg_file.resolve_path(out_val, uOpt_get_ini_key(ix))) {
                return i;
            } else if (m_cfg_arch.resolve_path(out_val, uOpt_get_ini_key(ix))) {
                return i;
            }
        } else {
            if (m_cfg_file.read_into(out_val, uOpt_get_ini_key(ix))) {
                return i;
            } else if (m_cfg_arch.read_into(out_val, uOpt_get_ini_key(ix))) {
                return i;
            }
        }
    }  // end INI check

    // Else check the parent configuration
    if (check_parent) {
        uSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->read_into(
                out_val, ixs, should_resolve, uTRUE /*check_parent*/);
        }
    }

    // Not found, return ixs.size()
    return num_keys;
}

/**
 * Obtain path value resolved according to the rules (in order of precedence)
 *  - if not found, path argument is unmodified
 *  - if from command line, output path is unmodified
 *  - if from config file and absolute, output path is unmodified
 *  - if from config file and relative, output path is resolved relative
 *      to config file directory
 *
 * Assumes parse() has been called for internal command line and ini file
 * maps.
 *
 * @param path - Output path, unmodified if key not found
 * @param ix - Option identifier
 * @return uTRUE if key exists, UFALSE otherwise. If key exists and value
 *  is from command line, then output path argument is same as command
 *  line. However, if key exists and value is from configuration file,
 *  then relative paths are resolved to be relative to the configuration
 *  file directory. Note that command line values override any
 *  configuration file values.
 */
uBool uConfig::resolve_path(std::string& path,
                            const uOptE ix,
                            const uBool check_parent) const {
    // Check command line first
    if (m_cmd_opts.resolve_path(path, uOpt_get_cmd_switch(ix))) {
        return uTRUE;
    }
    // Else check INI file
    /*else*/ if (m_cfg_file.resolve_path(path, uOpt_get_ini_key(ix))) {
        return uTRUE;
    }
    // Else check INI archetype
    /*else*/ if (m_cfg_arch.resolve_path(path, uOpt_get_ini_key(ix))) {
        return uTRUE;
    }
    // Else check the parent configuration
    /*else*/ if (check_parent) {
        uSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->resolve_path(path, ix, uTRUE /*check_parent*/);
        }
    }

    // Key not found, return false
    return uFALSE;
}

/**
 * Resolve path for vectorized options
 * @param path - Output path obtained using resolve_path() semantics
 * @param ix - Option identifier
 * @param off - The option offset
 * @return TRUE if key exists, FALSE o/w
 */
uBool uConfig::resolve_path_vec(std::string& path,
                                const uOptE ix,
                                const uUInt off,
                                const uBool check_parent) const {
    // Check command line first
    const std::string suffix(std::string("_") + u2Str(off));
    if (m_cmd_opts.resolve_path(path, uOpt_get_cmd_switch(ix) + suffix)) {
        return uTRUE;
    }
    // Else check INI file
    /*else*/ if (m_cfg_file.resolve_path(path, uOpt_get_ini_key(ix) + suffix)) {
        return uTRUE;
    }
    /*else*/ if (m_cfg_arch.resolve_path(path, uOpt_get_ini_key(ix) + suffix)) {
        return uTRUE;
    }
    // Else check the parent configuration
    /*else*/ if (check_parent) {
        uSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->resolve_path_vec(
                path, ix, off, uTRUE /*check_parent*/);
        }
    }

    // Key not found, return false
    return uFALSE;
}
