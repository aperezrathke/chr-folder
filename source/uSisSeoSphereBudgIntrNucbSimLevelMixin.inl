//****************************************************************************
// uSisSeoSphereBudgIntrNucbSimLevelMixin.inl
//****************************************************************************

/**
 * A nuclear body interaction mixin manages nuclear body association
 *  constraints. Note, this is different from a "nuclear mixin" which is used
 *  for modeling confinement by constraining all monomer beads to reside
 *  within the nuclear volume. In contrast, the nuclear body interaction mixin
 *  enforces polymer fragments to be proximal or distal to a specific
 *  spherical mass (x, y, z) coordinate as configured by the user.
 *  Technically, this mass does not have to reside within nuclear volume.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin supports a failure budget and assumes spherical nuclear bodies.
 */

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Budgeted nuclear body interaction simulation mixin
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisSeoSphereBudgIntrNucbSimLevelMixin_threaded
#else
class uSisSeoSphereBudgIntrNucbSimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
    // @HACK - Inheritance for composition purposes (no virtual methods)
    : public uSisSeoSphereIntrNucbSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Parent type
     */
    typedef uSisSeoSphereIntrNucbSimLevelMixin<glue_t>
        parent_sim_level_intr_nucb_mixin_t;

    /**
     * Default constructor
     */
#ifdef U_INJECT_THREADED_TLS
    uSisSeoSphereBudgIntrNucbSimLevelMixin_threaded()
#else
    uSisSeoSphereBudgIntrNucbSimLevelMixin_serial()
#endif  // U_INJECT_THREADED_TLS
    {
        clear();
    }

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Reset state
        this->clear();
        // Initialize parent
        parent_sim_level_intr_nucb_mixin_t::init(sim, config U_THREAD_ID_ARG);
        // Initialize failure budgets
        uIntrLibConfig::load_fail_budg(
            m_intr_nucb_kin_max_fail,
            config,
            uOpt_nucb_knock_in_fail_budget,
            uIntrNucbDefaultKnockInFailBudgFrac,
            U_TO_UINT(this->get_intr_nucb_kin().n_cols),
            uIntrNucbKnockInDataName);
        uIntrLibConfig::load_fail_budg(
            m_intr_nucb_ko_max_fail,
            config,
            uOpt_nucb_knock_out_fail_budget,
            uIntrNucbDefaultKnockOutFailBudgFrac,
            U_TO_UINT(this->get_intr_nucb_ko().n_cols),
            uIntrNucbKnockOutDataName);
        // Initialize TLS
        const uUInt num_candidates = sim.get_num_unit_sphere_sample_points();
        uAssert(num_candidates > U_TO_UINT(0));
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_nucb_kin_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_nucb_ko_delta_fails, zeros, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_nucb_kin_fail_table, resize, num_candidates);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_intr_nucb_ko_fail_table, resize, num_candidates);
    }

    /**
     * Resets to default state
     */
    void clear() {
        parent_sim_level_intr_nucb_mixin_t::clear();
        m_intr_nucb_kin_max_fail = U_TO_UINT(0);
        m_intr_nucb_ko_max_fail = U_TO_UINT(0);
        m_intr_nucb_kin_delta_fails.clear();
        m_intr_nucb_ko_delta_fails.clear();
        m_intr_nucb_kin_fail_table.clear();
        m_intr_nucb_ko_fail_table.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Nuclear body knock-in interaction failure budget
     */
    inline uUInt get_intr_nucb_kin_max_fail() const {
        uAssertBoundsInc(m_intr_nucb_kin_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_nucb_kin().n_elem));
        return m_intr_nucb_kin_max_fail;
    }

    /**
     * @return Nuclear body knock-out interaction failure budget
     */
    inline uUInt get_intr_nucb_ko_max_fail() const {
        uAssertBoundsInc(m_intr_nucb_ko_max_fail,
                         U_TO_UINT(0),
                         U_TO_UINT(this->get_intr_nucb_ko().n_elem));
        return m_intr_nucb_ko_max_fail;
    }

    // Note: parent simulation is likely to be const, therefore making buffers
    // mutable objects
    inline uUIVecCol& get_intr_nucb_kin_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_nucb_kin_delta_fails);
    }
    inline uUIVecCol& get_intr_nucb_ko_delta_fails(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_nucb_ko_delta_fails);
    }
    inline uSisIntrNucbBudgFailTable_t& get_intr_nucb_kin_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_nucb_kin_fail_table);
    }
    inline uSisIntrNucbBudgFailTable_t& get_intr_nucb_ko_fail_table(
        U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_intr_nucb_ko_fail_table);
    }

    // @return TLS handles for budget nuclear body interaction failure policy
    inline uSisIntrNucbBudgFailPolicyArgs_t get_intr_nucb_budg_fail_policy_args(
        U_THREAD_ID_0_PARAM) const {
        uSisIntrNucbBudgFailPolicyArgs_t policy_args = {
            this->get_intr_nucb_kin_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_nucb_ko_delta_fails(U_THREAD_ID_0_ARG),
            this->get_intr_nucb_kin_fail_table(U_THREAD_ID_0_ARG),
            this->get_intr_nucb_ko_fail_table(U_THREAD_ID_0_ARG)};
        return policy_args;
    }

private:
    /**
     * Nuclear body knock-in interaction failure budget, max number of
     *  interaction constraints that are allowed to fail
     */
    uUInt m_intr_nucb_kin_max_fail;

    /**
     * Nuclear body knock-out interaction failure budget, max number of
     *  interaction constraints are allowed to fail
     */
    uUInt m_intr_nucb_ko_max_fail;

    /**
     * Scratch buffer for storing incremental (delta) knock-in failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_nucb_kin_delta_fails);

    /**
     * Scratch buffer for storing incremental (delta) knock-out failure counts
     *  at each candidate position
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_intr_nucb_ko_delta_fails);

    // Note, the size of each candidate's fail list should be same as the
    //  corresponding count in the delta fail vector. Though this information
    //  is redundant, am still retaining separate 'delta' vector for failed
    //  counts at each candidate, as this makes it easier to implement
    //  energy selection models based on failure counts.

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-in interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrNucbBudgFailTable_t,
                               m_intr_nucb_kin_fail_table);

    /**
     * Table for storing failed interaction information, each row is a list of
     *  failed knock-out interactions at a single candidate
     */
    mutable U_DECLARE_TLS_DATA(uSisIntrNucbBudgFailTable_t,
                               m_intr_nucb_ko_fail_table);
};
