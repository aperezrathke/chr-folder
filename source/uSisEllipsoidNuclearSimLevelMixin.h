//****************************************************************************
// uSisEllipsoidNuclearSimLevelMixin.h
//****************************************************************************

/**
 * A nuclear mixin manages the details for checking candidate nodes to see if
 * they lie within the nuclear volume; this mixing enforces an ellipsoidal
 * nuclear confinement.
 *
 * An accessory structure is the simulation level nuclear mixin which defines
 * global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 */

#ifndef uSisEllipsoidNuclearSimLevelMixin_h
#define uSisEllipsoidNuclearSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsNucleus.h"
#include "uSisUtilsRc.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisEllipsoidNuclearSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisEllipsoidNuclearSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisEllipsoidNuclearSimLevelMixin_h
