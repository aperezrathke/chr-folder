//****************************************************************************
// uStats.cpp
//****************************************************************************

#include "uBuild.h"
#include "uStats.h"

#ifdef U_BUILD_ENABLE_STATS

#include "uAssert.h"

#include <string>

namespace uStats {

#ifdef U_STATS_DECLARE_NAME
#   error U_STATS_DECLARE_NAME already defined!
#endif  // U_STATS_DECLARE_NAME

#define U_STATS_DECLARE_NAME(statId) #statId

/**
 * All timer stat instances
 */
TimerStat GTimerStats[uSTAT_TimerMAX];

/**
 * The name of each timer stat
 */
const char* GTimerStatsNames[uSTAT_TimerMAX] = {
    U_STATS_DECLARE_NAME(uSTAT_TotalTime),
    U_STATS_DECLARE_NAME(uSTAT_GetCandidateNodePositionsTime),
    U_STATS_DECLARE_NAME(uSTAT_NuclearFilterTime),
    U_STATS_DECLARE_NAME(uSTAT_CollisionFilterTime),
    U_STATS_DECLARE_NAME(uSTAT_CollisionBroadPhaseTime),
    U_STATS_DECLARE_NAME(uSTAT_CollisionNarrowPhaseTime),
    U_STATS_DECLARE_NAME(uSTAT_IntrChrFilterTime),
    U_STATS_DECLARE_NAME(uSTAT_IntrLamFilterTime),
    U_STATS_DECLARE_NAME(uSTAT_IntrNucbFilterTime),
    U_STATS_DECLARE_NAME(uSTAT_RcRegrowthTime)};

/**
 * All counter stat instances
 */
CounterStat GCounterStats[uSTAT_CounterMAX];

/**
 * The name of each counter stat
 */
const char* GCounterStatsNames[uSTAT_CounterMAX] = {
    U_STATS_DECLARE_NAME(uSTAT_TrialsCount),
    U_STATS_DECLARE_NAME(uSTAT_CompletedSamplesCount),
    U_STATS_DECLARE_NAME(uSTAT_RcRegrowthCount),
    U_STATS_DECLARE_NAME(uSTAT_RcReRegrowthCount)};

#undef U_STATS_DECLARE_NAME

/**
 * Report to standard out
 */
void report_stats() {
    // Header
    printf("\nSTATS REPORT\n");
    printf("------------\n");

    // Timers
    printf("\nTimer stats:\n");
    unsigned int i = 0;
    for (i = 0; i < uSTAT_TimerMAX; ++i) {
        printf("\t%s: %f minutes\n",
               GTimerStatsNames[i],
               (((double)GTimerStats[i].get_total_time()) / 60.0));
    }

    // Counters
    printf("\nCounter stats:\n");
    for (i = 0; i < uSTAT_CounterMAX; ++i) {
        printf("\t%s: %u\n",
               GCounterStatsNames[i],
               GCounterStats[i].get_counter());
    }
}

}  // namespace uStats

#endif  // U_BUILD_ENABLE_STATS
