//****************************************************************************
// uTestSisPartialRejectionControlQc.h
//****************************************************************************

/**
 * Commandlet for testing growth of a simple chromatin chain (single locus)
 * but with partial rejection control as the quality control (Qc) mixin.
 *
 * Usage:
 * -test_sis_partial_rejection_control_qc
 */

#ifdef uTestSisPartialRejectionControlQc_h
#   error "Test Sis Partial Rejection Control Qc included multiple times!"
#endif  // uTestSisPartialRejectionControlQc_h
#define uTestSisPartialRejectionControlQc_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uSisSample.h"
#include "uSisSim.h"
#include "uThread.h"
#include "uTypes.h"

// Rejection control mixin
#include "uSisDynamicEssScheduleMixin.h"
#include "uSisHomgLagScheduleMixin.h"
#include "uSisInterpWeightsScaleRcMixin.h"
#include "uSisParallelRegrowthRcMixin.h"
#include "uSisPartialRejectionControlQcSimLevelMixin.h"
#include "uSisSerialRegrowthRcMixin.h"

// Use null QC as a control
#include "uMockUpUtils.h"

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"
#include "uStats.h"

#include "uSisSerialBatchGrower.h"

#include <iostream>
#include <string>

namespace {
/**
 * Wire together a partial rejection control simulation
 */
class uTestSisRcQcGlue_partial {
public:
    // Simulation
    typedef uSisSim<uTestSisRcQcGlue_partial> sim_t;
    // Sample
    typedef uSisSample<uTestSisRcQcGlue_partial> sample_t;

    // Trial runner mixin
    typedef uSisCanonicalTrialRunnerMixin<
        uTestSisRcQcGlue_partial,
        uSisSerialBatchGrower<uTestSisRcQcGlue_partial> >
        trial_runner_mixin_t;

    // Simulation level quality control mixin
    typedef uSisPartialRejectionControlQcSimLevelMixin<
        uTestSisRcQcGlue_partial,
        uSisDynamicEssScheduleMixin<uTestSisRcQcGlue_partial>,
        uSisInterpWeightsScaleRcMixin<uTestSisRcQcGlue_partial>,
        uSisParallelRegrowthRcMixin<uTestSisRcQcGlue_partial> >
        sim_level_qc_mixin_t;

    // Simulation level node radius mixin
    typedef uSisHomgNodeRadiusSimLevelMixin<uTestSisRcQcGlue_partial>
        sim_level_node_radius_mixin_t;
    // Sample level node radius mixin
    typedef uSisHomgNodeRadiusMixin<uTestSisRcQcGlue_partial>
        node_radius_mixin_t;
    // Simulation level growth mixin
    typedef uSisSeoSloc::GrowthSimLevelMixin_threaded<uTestSisRcQcGlue_partial>
        sim_level_growth_mixin_t;
    // Sample level growth mixin
    typedef uSisSeoSloc::GrowthMixin<uTestSisRcQcGlue_partial> growth_mixin_t;
    // Simulation level nuclear mixin
    typedef uSisSphereNuclearSimLevelMixin_threaded<uTestSisRcQcGlue_partial>
        sim_level_nuclear_mixin_t;
    // Sample level nuclear mixin
    typedef uSisSphereNuclearMixin<uTestSisRcQcGlue_partial> nuclear_mixin_t;
    // Simulation level collision mixin
    typedef uSisHardShellCollisionSimLevelMixin_threaded<
        uTestSisRcQcGlue_partial>
        sim_level_collision_mixin_t;
    // Sample level collision mixin
    typedef uSisHardShellCollisionMixin<uTestSisRcQcGlue_partial, uBroadPhase_t>
        collision_mixin_t;
    // Simulation level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrSimLevelMixin<uTestSisRcQcGlue_partial>
        sim_level_intr_chr_mixin_t;
    // Sample level chrome-to-chrome interaction mixin
    typedef uSisNullIntrChrMixin<uTestSisRcQcGlue_partial> intr_chr_mixin_t;
    // Simulation level lamina interaction mixin
    typedef uSisNullIntrLamSimLevelMixin<uTestSisRcQcGlue_partial>
        sim_level_intr_lam_mixin_t;
    // Sample level lamina interaction mixin
    typedef uSisNullIntrLamMixin<uTestSisRcQcGlue_partial> intr_lam_mixin_t;
    // Simulation level nuclear body interaction mixin
    typedef uSisNullIntrNucbSimLevelMixin<uTestSisRcQcGlue_partial>
        sim_level_intr_nucb_mixin_t;
    // Sample level nuclear body interaction mixin
    typedef uSisNullIntrNucbMixin<uTestSisRcQcGlue_partial> intr_nucb_mixin_t;
    // Simulation level energy mixin
    typedef uSisUnifEnergySimLevelMixin<uTestSisRcQcGlue_partial>
        sim_level_energy_mixin_t;
    // Sample level energy mixin
    typedef uSisUnifEnergyMixin<uTestSisRcQcGlue_partial> energy_mixin_t;
    // Seed mixin
    typedef uSisSeoUnifCubeSeedMixin<uTestSisRcQcGlue_partial> seed_mixin_t;
};
}  // end of anonymous namespace

/**
 * Entry point for test
 * rc = rejection control
 * qc = quality control
 */
int uTestSisPartialRejectionControlQcMain(const uCmdOptsMap& cmd_opts) {
    /**
     * Expose partial rc qc simulation
     */
    typedef uTestSisRcQcGlue_partial::sim_t rc_sim_t;

    /**
     * Expose null qc simulation as control
     */
    typedef uMockUpUtils::uSisUnifSeoSlocHomgNullGlue::sim_t null_sim_t;

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 3;
    config->ensemble_size = 1000;
    config->set_option(uOpt_max_node_diameter, U_TO_REAL(300));
    config->set_option(uOpt_nuclear_diameter, U_TO_REAL(9000));
    config->num_nodes = std::vector<uUInt>(1 /* n_chains*/, 200 /* n_nodes*/);
    config->num_unit_sphere_sample_points = 50;

    // Configure rc schedule

    // Uncomment this is for a deterministic schedule (and also set glue)
    /*
    config->set_option(uOpt_qc_schedule_type, "homg_lag");
    config->set_option(uOpt_qc_schedule_lag, 5);
    */

    // Uncomment this for a dynamic schedule (and also set glue)
    config->set_option(uOpt_qc_schedule_type_slot0, "dynamic_ess");
    config->set_option(uOpt_qc_schedule_dynamic_ess_thresh_slot0, 0.25);

    // Configure rc scale
    config->set_option(uOpt_rc_scale_type, "interp_weights");
    config->set_option(uOpt_rc_scale_interp_weights_c_min, 0.0);
    config->set_option(uOpt_rc_scale_interp_weights_c_mean, 0.5);
    config->set_option(uOpt_rc_scale_interp_weights_c_max, 0.5);

    // Output configuration
    std::cout << "Running Test Sis Partial Rejection Control Qc." << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    U_LOG_NUM_WORKER_THREADS;

    // Feed configuration to simulation
    rc_sim_t rc_sim(config U_MAIN_THREAD_ID_ARG);

    // Do rejection control simulation!
    const uBool rc_result = rc_sim.run();

    // Compute rejection control effective sample size
    const uReal rc_ess = uSisUtilsQc::calc_ess_cv_from_log(
        rc_sim.get_completed_log_weights_view());

    std::cout << "Partial rc simulation completed with status: " << rc_result
              << std::endl;
    std::cout << "Partial rc effective sample size: " << rc_ess << std::endl;

    uSisUtilsQc::report_percent_unique_monomers(rc_sim.get_completed_samples());

    // Run control simulation
    null_sim_t null_sim(config U_MAIN_THREAD_ID_ARG);

    // Do control simulation!
    const uBool null_result = null_sim.run();

    // Compute null effective sample size
    const uReal null_ess = uSisUtilsQc::calc_ess_cv_from_log(
        null_sim.get_completed_log_weights_view());

    std::cout << "Null simulation completed with status: " << null_result
              << std::endl;
    std::cout << "Null effective sample size: " << null_ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
