//****************************************************************************
// uChromatinExportPdb.h
//****************************************************************************

/**
 * Utility for writing PDB chromatin files
 */

#ifndef uChromatinExportPdb_h
#define uChromatinExportPdb_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"

#include <iostream>
#include <string>

//****************************************************************************
// PDB export
//****************************************************************************

/**
 * Effectively a namespace for PDB chromatin export utilities
 */
class uChromatinExportPdb {
public:
    /**
     * Writes sample node positions to PDB file
     *
     * @param fpath - path to write the exported sample
     * @param args - exporter arguments, note 'scale' is applied to all node
     *  positions, this helps conform to the PDB format as otherwise the
     *  coordinates may overflow alloted character column boundaries.
     */
    static void write_chromatin(const std::string& fpath,
                                const uChromatinExportArgs_t& args);

    /**
     * Write sample node positions to 'out' stream
     * @WARNING - DOES NOT CLOSE STREAM!
     * @param out - output stream
     * @param args - exporter args
     */
    static void write_chromatin(std::ostream& out,
                                const uChromatinExportArgs_t& args);

    /**
     * @return exporter name
     */
    static std::string get_name() { return std::string("pdb"); }

    /**
     * @return exporter format
     */
    static std::string get_format() { return get_name(); }

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes);
};

#endif  // uChromatinExportPdb_h
