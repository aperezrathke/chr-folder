//****************************************************************************
// uChromatinExportPolicyBulk.h
//****************************************************************************

/**
 * Utility for writing chromatin chains to bulk file
 */

#ifndef uChromatinExportPolicyBulk_h
#define uChromatinExportPolicyBulk_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"
#include "uFilesystem.h"
#include "uSisUtilsExportPath.h"
#include "uTypes.h"

#include <fstream>
#include <iostream>
#include <sstream>

//****************************************************************************
// Bulk chromatin policy export
//****************************************************************************

/**
 * Bulk file for all chromatin chains export policy
 */
template <typename t_exporter>
class uChromatinExportPolicyBulk {
public:
    /**
     * Raw exporter type
     */
    typedef t_exporter exporter_t;

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes) {
        exporter_t::warn_format(max_total_num_nodes);
    }

    /**
     * @return exporter name
     */
    static std::string get_name() {
        return std::string("bulk.") + exporter_t::get_name();
    }

    /**
     * @return exporter format
     */
    static std::string get_format() { return get_name(); }

    /**
     * Called once before processing all sample geometries, meant to
     *  initialize export policy
     */
    void begin() { m_ss.clear(); }

    /**
     * Called once after processing all sample geometries, meant to flush and
     *  teardown export policy
     * @param config - user configuration
     */
    void end(uSpConstConfig_t config) const {
        // Determine file name
        const std::string fpath =
            uSisUtils::get_export_fpath(config,
                                        get_name(),            /*subdir*/
                                        U_SIS_BULK_EXPORT_FID, /*file_id*/
                                        get_format());         /*extension*/

        // Create output directory if not existing
        uFs_create_parent_dirs(fpath);
        std::ofstream fout(fpath.c_str(), std::ofstream::out);

        if (!fout) {
            std::cout << "Warning, unable to open: " << fpath << std::endl
                      << "\tfor writing to bulk format.\n";
            return;
        }

        fout << m_ss.rdbuf();
        fout.close();
    }

    /**
     * Process sample node positions
     * @param args - exporter arguments
     * @param i - sample index
     * @param config - user configuration
     */
    void process(const uChromatinExportArgs_t& args,
                 const uUInt i,
                 uSpConstConfig_t config) {
        if (!chromatin_export_check(args)) {
            std::cout << "Error, unable to export sample.\n";
            return;
        }
        // Aggregate chromatin sample
        exporter_t::write_chromatin(m_ss, args);
    }

private:
    /**
     * String stream used for aggregating exported geometry
     */
    std::stringstream m_ss;
};

#endif  // uChromatinExportPolicyBulk_h
