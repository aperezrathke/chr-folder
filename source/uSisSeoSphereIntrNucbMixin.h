//****************************************************************************
// uSisSeoSphereIntrNucbMixin.h
//****************************************************************************

/**
 * A nuclear body interaction mixin manages nuclear body association
 *  constraints. Note, this is different from a "nuclear mixin" which is used
 *  for modeling confinement by constraining all monomer beads to reside
 *  within the nuclear volume. In contrast, the nuclear body interaction mixin
 *  enforces polymer fragments to be proximal or distal to a specific
 *  spherical mass (x, y, z) coordinate as configured by the user.
 *  Technically, this mass does not have to reside within nuclear volume.
 *
 * This mixin does not explicitly enforce excluded volume of the mass;
 * however, this can be achieved using a radial knock-out distance constraint
 * applied to a set of fragments incorporating all beads.
 *
 * Note, if excluded volume is desired for a *large* number of nuclear bodies,
 * then a static broad phase can be created containing just the nuclear body
 * components. This *simulation level* broad phase may be checked against
 * during nuclear body filtering. However, if the number of nuclear bodies is
 * *sparse*, then it is likely more efficient to perform narrow phase checks
 * on every filter call as this implementation is intended for - @TODO -
 * consider adding implicit narrow phase excluded volume support which creates
 * a fragment containing all polymer beads and sets the knock-out distance
 * based on the nuclear body radius.
 */

#ifndef uSisSeoSphereIntrNucbMixin_h
#define uSisSeoSphereIntrNucbMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uIntrLibBudgUtil.h"
#include "uSisIntrNucbCommonBatch.h"
#include "uSisSeoSphereIntrNucbFilt.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisSeoSphereIntrNucbSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers.
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrNucbMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Knock-in budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_nucb_budg_acc_kin_t;

    /**
     * Knock-out budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_nucb_budg_acc_ko_t;

    /**
     * Batch interactions utility
     */
    typedef uSisIntrNucbCommonBatch<glue_t> intr_nucb_batch_util_t;

    /**
     * Knock-in filter
     */
    typedef uSisSeoSphereIntrNucbKinFilt<glue_t> intr_nucb_filt_kin_t;

    /**
     * Knock-out filter
     */
    typedef uSisSeoSphereIntrNucbKoFilt<glue_t> intr_nucb_filt_ko_t;

    /**
     * Initializes mixin
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        m_intr_nucb_loci_tables = sim.get_intr_nucb_loci_tables();
    }

    /**
     * Resets to default state
     */
    void clear() { intr_nucb_batch_util_t::clear(m_intr_nucb_loci_tables); }

    /**
     * Checks if seed can satisfy interaction constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE if seed can satisfy constraints, uFALSE o/w
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        // https://stackoverflow.com/questions/1840253/template-member-function-of-template-class-called-from-template-function
        // When the name of a member template specialization appears after .
        // or -> in a postfix-expression, or after nested-name-specifier in a
        // qualified-id, and the postfix-expression or qualified-id explicitly
        // depends on a template-parameter(14.6.2), the member template name
        // must be prefixed by the keyword template. Otherwise the name is
        // assumed to name a non-template.
        return intr_nucb_batch_util_t::template is_seed_okay<
            intr_nucb_filt_kin_t,
            intr_nucb_filt_ko_t,
            intr_nucb_mixin_t,
            void>(*this,
                  (void*)NULL,
                  m_intr_nucb_loci_tables,
                  nfo,
                  sample,
                  sim U_THREAD_ID_ARG);
    }

    /**
     * Determines which positions satisfy constraints
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for constraint violations.
     * @param candidate_radius - Radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - Node identifier from which candidates positions
     *  were generated
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_nucb_filter(uBoolVecCol& out_legal_candidates_primed,
                          const uMatrix& candidate_centers,
                          const uReal candidate_radius,
                          const uUInt parent_node_id,
                          const sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        intr_nucb_batch_util_t::template mark_viol<intr_nucb_filt_kin_t,
                                                   intr_nucb_filt_ko_t,
                                                   intr_nucb_mixin_t,
                                                   void>(
            *this,
            (void*)NULL,
            out_legal_candidates_primed,
            m_intr_nucb_loci_tables,
            candidate_centers,
            candidate_radius,
            parent_node_id,
            sample,
            sim U_THREAD_ID_ARG);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  nuclear body interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_nucb_kin_on_fail(void* const p_policy_args,
                                       const uUInt candidate_id,
                                       const uSisIntrNucbFilterCore_t& c,
                                       const sample_t& sample,
                                       const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  nuclear body interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_nucb_ko_on_fail(void* const p_policy_args,
                                      const uUInt candidate_id,
                                      const uSisIntrNucbFilterCore_t& c,
                                      const sample_t& sample,
                                      const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - Unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - Radius of the node to add
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation of parameter sample
     */
    void intr_nucb_update(const uUInt node_id,
                          const uReal* point,
                          const uReal radius,
                          const uUInt candidate_id,
                          sample_t& sample,
                          const sim_t& sim U_THREAD_ID_PARAM) {
        intr_nucb_batch_util_t::template remove_satisf<intr_nucb_filt_kin_t,
                                                       intr_nucb_filt_ko_t>(
            m_intr_nucb_loci_tables,
            node_id,
            point,
            radius,
            candidate_id,
            sample,
            sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Collection of active nuclear body interactions partitioned by loci
     */
    uSisIntrNucbLociTables_t m_intr_nucb_loci_tables;
};

#endif  // uSisSeoSphereIntrNucbMixin_h
