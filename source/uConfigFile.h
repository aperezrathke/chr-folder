// ConfigFile.h
// Class for reading named values from configuration files
// Richard J. Wagner  v2.1  24 May 2004  wagnerr@umich.edu

// Copyright (c) 2004 Richard J. Wagner
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

// Typical usage
// -------------
//
// Given a configuration file "settings.inp":
//   atoms  = 25
//   length = 8.0  # nanometers
//   name = Reece Surcher
//
// Named values are read in various ways, with or without default values:
//   ConfigFile config( "settings.inp" );
//   int atoms = config.read<int>( "atoms" );
//   double length = config.read( "length", 10.0 );
//   string author, title;
//   config->read_into( author, "name" );
//   config->read_into( title, "title", string("Untitled") );
//
// See file example.cpp for more examples.

#ifndef uConfigFile_h
#define uConfigFile_h

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

class uConfigFile {
    // Data
protected:
    // directory containing this file, used for path resolution
    std::string myDir;
    // separator between key and value
    std::string myDelimiter;
    // separator between value and comments
    std::string myComment;
    // optional string to signal end of file
    std::string mySentry;
    // extracted keys and values
    std::map<std::string, std::string> myContents;

    typedef std::map<std::string, std::string>::iterator mapi;
    typedef std::map<std::string, std::string>::const_iterator mapci;

    // Methods
public:
    uConfigFile(const std::string filename,
                const std::string delimiter = "=",
                const std::string comment = "#",
                const std::string sentry = "EndConfigFile");
    uConfigFile();

    // Search for key and read value or optional default value
    template <class T>
    T read(const std::string& key) const;  // call as read<T>
    template <class T>
    T read(const std::string& key, const T& value) const;
    template <class T>
    bool read_into(T& var, const std::string& key) const;
    template <class T>
    bool read_into(T& var, const std::string& key, const T& value) const;

    /**
     * @return true if key exists, false otherwise. If key exists, output path
     *  argument is always absolute. However, if user value is a relative path,
     *  the output absolute path is relative to the location of this
     *  configuration file. If the user value was already an absolute path, it
     *  is returned unmodified. If key does not exist, path argument is
     *  unmodified.
     */
    bool resolve_path(std::string& path, const std::string& key) const;

    // Modify keys and values
    template <class T>
    void add(std::string key, const T& value);
    void remove(const std::string& key);

    // Check whether key exists in configuration
    bool key_exists(const std::string& key) const;

    // Obtain all registered keys
    void get_keys(std::vector<std::string>& keys) const;

    // Check whether any keys exist
    inline bool empty() const { return myContents.empty(); }

    // Check or change configuration syntax
    std::string get_delimiter() const { return myDelimiter; }
    std::string get_comment() const { return myComment; }
    std::string get_sentry() const { return mySentry; }
    std::string set_delimiter(const std::string& s) {
        std::string old = myDelimiter;
        myDelimiter = s;
        return old;
    }
    std::string set_comment(const std::string& s) {
        std::string old = myComment;
        myComment = s;
        return old;
    }

    // Write or read configuration
    friend std::ostream& operator<<(std::ostream& os, const uConfigFile& cf);
    friend std::istream& operator>>(std::istream& is, uConfigFile& cf);

protected:
    template <class T>
    static std::string T_as_string(const T& t);
    template <class T>
    static T string_as_T(const std::string& s);
    static void trim(std::string& s);

    // Exception types
public:
    struct file_not_found {
        std::string filename;
        file_not_found(const std::string& filename_ = std::string())
            : filename(filename_) {}
    };
    struct key_not_found {  // thrown only by T read(key) variant of read()
        std::string key;
        key_not_found(const std::string& key_ = std::string()) : key(key_) {}
    };
};

/* static */
template <class T>
std::string uConfigFile::T_as_string(const T& t) {
    // Convert from a T to a string
    // Type T must support << operator
    std::ostringstream ost;
    ost << t;
    return ost.str();
}

/* static */
template <class T>
T uConfigFile::string_as_T(const std::string& s) {
    // Convert from a string to a T
    // Type T must support >> operator
    T t;
    std::istringstream ist(s);
    ist >> t;
    return t;
}

/* static */
template <>
inline std::string uConfigFile::string_as_T<std::string>(const std::string& s) {
    // Convert from a string to a string
    // In other words, do nothing
    return s;
}

/* static */
template <>
inline bool uConfigFile::string_as_T<bool>(const std::string& s) {
    // Convert from a string to a bool
    // Interpret "false", "F", "no", "n", "0" as false
    // Interpret "true", "T", "yes", "y", "1", "-1", or anything else as true
    bool b = true;
    std::string sup = s;
    for (std::string::iterator p = sup.begin(); p != sup.end(); ++p)
        *p = toupper(*p);  // make string all caps
    if (sup == std::string("FALSE") || sup == std::string("F") ||
        sup == std::string("NO") || sup == std::string("N") ||
        sup == std::string("0") || sup == std::string("NONE"))
        b = false;
    return b;
}

template <class T>
T uConfigFile::read(const std::string& key) const {
    // Read the value corresponding to key
    mapci p = myContents.find(key);
    if (p == myContents.end())
        throw key_not_found(key);
    return string_as_T<T>(p->second);
}

template <class T>
T uConfigFile::read(const std::string& key, const T& value) const {
    // Return the value corresponding to key or given default value
    // if key is not found
    mapci p = myContents.find(key);
    if (p == myContents.end())
        return value;
    return string_as_T<T>(p->second);
}

template <class T>
bool uConfigFile::read_into(T& var, const std::string& key) const {
    // Get the value corresponding to key and store in var
    // Return true if key is found
    // Otherwise leave var untouched
    mapci p = myContents.find(key);
    bool found = (p != myContents.end());
    if (found)
        var = string_as_T<T>(p->second);
    return found;
}

template <class T>
bool uConfigFile::read_into(T& var,
                            const std::string& key,
                            const T& value) const {
    // Get the value corresponding to key and store in var
    // Return true if key is found
    // Otherwise set var to given default
    mapci p = myContents.find(key);
    bool found = (p != myContents.end());
    if (found)
        var = string_as_T<T>(p->second);
    else
        var = value;
    return found;
}

template <class T>
void uConfigFile::add(std::string key, const T& value) {
    // Add a key with given value
    std::string v = T_as_string(value);
    trim(key);
    trim(v);
    myContents[key] = v;
    return;
}

#endif  // uConfigFile_h

// Release notes:
// v1.0  21 May 1999
//   + First release
//   + Template read() access only through non-member readConfigFile()
//   + ConfigurationFileBool is only built-in helper class
//
// v2.0  3 May 2002
//   + Shortened name from ConfigurationFile to ConfigFile
//   + Implemented template member functions
//   + Changed default comment separator from % to #
//   + Enabled reading of multiple-line values
//
// v2.1  24 May 2004
//   + Made template specializations inline to avoid compiler-dependent linkage
//   + Allowed comments within multiple-line values
//   + Enabled blank line termination for multiple-line values
//   + Added optional sentry to detect end of configuration file
//   + Rewrote messy trimWhitespace() function as elegant trim()
