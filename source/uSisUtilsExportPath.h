//****************************************************************************
// uSisUtilsExportPath.h
//****************************************************************************

#ifndef uSisUtilsExportPath_h
#define uSisUtilsExportPath_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uTypes.h"

//****************************************************************************
// Defines
//****************************************************************************

/**
 * File identifier used for bulk exports
 */
#define U_SIS_BULK_EXPORT_FID "0"

//****************************************************************************
// Interface
//****************************************************************************

namespace uSisUtils {

/**
 * Utility to obtain file name from arguments
 * @param job_id - Job identifier prefix
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export file name for a single export file
 */
extern std::string get_export_fname(const std::string& job_id,
                                    const std::string& file_id,
                                    const std::string& ext);

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
extern std::string get_export_fpath(uSpConstConfig_t config,
                                    const std::string& subdir,
                                    const std::string& file_id,
                                    const std::string& ext);
}  // namespace uSisUtils

#endif  // uSisUtilsExportPath_h
