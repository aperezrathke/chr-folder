//****************************************************************************
// uSisIntrChrFilterKoInter.h
//****************************************************************************

/**
 * Filter for between-loci, knock-out chr-chr interactions
 */

#ifndef uSisIntrChrFilterKoInter_h
#define uSisIntrChrFilterKoInter_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uDistUtils.h"
#include "uIntrLibDefs.h"
#include "uSisIntrChrCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// uSisIntrChrFilterKoInter
//****************************************************************************

/**
 * Stateless class for between-loci, knock-out chr-chr interaction testing
 */
template <typename t_SisGlue>
class uSisIntrChrFilterKoInter {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utility type
     */
    typedef uSisIntrChrCommonFilt<t_SisGlue> common_filt_t;

    /**
     * Determines if any candidate positions violate the parameter interaction
     * constraint - specifically a between (inter) loci, knock-out constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrChrFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim) {
        // Note, at this point we are assuming that we have not finished
        // growing at least one of the interacting fragments - this
        // assumption is verifed in check_format() assuming SEO growth
        uAssert(common_filt_t::check_format_mloc(c, sample, sim));

        // Format interaction arguments such that tested candidates are on
        // same locus as fragment B
        uSisIntrChrFilterCore_t sc;
        common_filt_t::format_as_B_mloc(sc, c, sample, sim);

        // Early out if fragment B already grown
        if (sc.node_nid > sc.frag_b_nid_hi) {
            // Assume fragment A has not been grown completely
            uAssert(common_filt_t::get_next_nid_at_locus(
                        sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim) <=
                    sc.frag_a_nid_hi);
            return;
        }

        // Determine growth state of alternate fragment A
        const uUInt next_nid_a = common_filt_t::get_next_nid_at_locus(
            sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim);

        // Determine if fragment A has been at least partially grown
        const uBool b_has_A = (next_nid_a > sc.frag_a_nid_lo);

        if (b_has_A) {
            // Fragment A has been at least partially grown, constrain testing
            // to only grown region
            uAssert(next_nid_a > U_TO_UINT(0));
            sc.frag_a_nid_hi =
                std::min(sc.frag_a_nid_hi, next_nid_a - U_TO_UINT(1));
            uAssert(common_filt_t::check_format_mloc(sc, sample, sim));
            // Defer to common routine
            common_filt_t::mark_viol_ko_full_A(policy,
                                               p_policy_args,
                                               out_legal_candidates_primed,
                                               sc,
                                               candidate_centers,
                                               sample,
                                               sim);
        } else {
            // Early out if alternate locus has not yet had any nodes placed
            if (sim.get_growth_nof(next_nid_a) == U_TO_UINT(0)) {
                return;
            }
            // Special case check when fragment A not yet reached
            mark_viol_ko_no_A(policy,
                              p_policy_args,
                              out_legal_candidates_primed,
                              sc,
                              next_nid_a,
                              candidate_centers,
                              sample,
                              sim);
        }
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a between (inter) loci, knock-out constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrChrFilterCore_t& c,
                           const uReal* const B,
                           const sample_t& sample,
                           const sim_t& sim) {
        // Note, at this point we are assuming that we have not grown past at
        // least one of the interacting fragments - this assumption is verified
        // in check_format() assuming SEO growth
        uAssert(common_filt_t::check_format_mloc(c, sample, sim));

        // Format interaction arguments such that tested node is on same locus
        // as fragment B
        uSisIntrChrFilterCore_t sc;
        common_filt_t::format_as_B_mloc(sc, c, sample, sim);

        // Test to see if we've grown both fragments at this point
        const uUInt lid_a = sim.get_growth_lid(sc.frag_a_nid_lo);
        uAssertBounds(lid_a, U_TO_UINT(0), sim.get_num_loci());
        const uUInt next_nid_a =
            common_filt_t::get_next_nid_at_locus(lid_a, sample, sim);
        // Specifically, did we just finish fragment B and fragment A is
        // already grown?
        return (sc.node_nid == sc.frag_b_nid_hi) &&
               (next_nid_a > sc.frag_a_nid_hi);
    }

private:
    /**
     * @WARNING ASSUMES AT LEAST 1 NODE OF FRAGMENT 'A' LOCUS HAS BEEN GROWN!
     * @WARNING ASSUMES that no fragment A nodes have been grown!
     * ASSUMES sequential growth from first to last node. Determines which
     * candidate positions can possibly satisfy knock-out constraint.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param sc - "Standardized" core filter arguments such that tested
     *  candidates are on fragment B locus
     * @param next_nid_a - Next grown node identifier at fragment A locus
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol_ko_no_A(policy_t& policy,
                                  policy_args_t* const p_policy_args,
                                  uBoolVecCol& out_legal_candidates_primed,
                                  const uSisIntrChrFilterCore_t& sc,
                                  const uUInt next_nid_a,
                                  const uMatrix& candidate_centers,
                                  const sample_t& sample,
                                  const sim_t& sim) {
        // Assuming fragment A has not been reached yet
        uAssert(next_nid_a <= sc.frag_a_nid_lo);
        // Assume at least a single node exists in fragment A's locus
        uAssert(next_nid_a > U_TO_UINT(0));
        uAssert(sim.get_growth_nof(next_nid_a) > U_TO_UINT(0));
        // Assume we have not grown beyond fragment B
        uAssert(sc.node_nid <= sc.frag_b_nid_hi);
        // Candidate node assumed to be from fragment B locus
        uAssert(sim.get_growth_lid(sc.frag_b_nid_lo) == sc.node_lid);

        /**
         * For this special case, no nodes in fragment A exist yet, so we have
         * to account for the additional padding from last placed node in
         * fragment A locus to first node in fragment A.
         *
         * Ultimately, a candidate position 'cand' is not a violator if the
         * following holds:
         *
         *  d(cand, A_last) + A_pad + B_pad - r(A0) - r(cand, B0) > d_ko
         *
         * where:
         *
         *  - cand: Candidate node position
         *  - A_last: Last placed node in fragment A locus, must not be within
         *      fragment A itself
         *  - d(cand, A_last): Euclidean distance between 'cand' and last
         *      placed node within fragment A locus
         *  - A_pad: Maximum center-to-center (c2c) length of the remaining
         *      nodes to be grown beyond 'A_last' until fragment A reached
         *      (assumed to be positive, non-zero, value)
         *  - B_pad: Maximum center-to-center length of the remaining nodes to
         *      be grown beyond 'cand' until fragment B is reached. This value
         *      is 0 if 'cand' is within fragment B
         *  - A0: First node in fragment A (not yet grown)
         *  - r(A0): Radius of first node in fragment A
         *  - B0: The first node of fragment B (may not have been grown)
         *  - r(cand, B0): If 'cand' is within fragment B, expression
         *      evaluates to radius of 'cand', else expression evaluates to
         *      radius of node 'B0'
         *  - d_ko: User configured knock-out distance threshold such that all
         *      nodes in fragment B are at least this distance from fragment A
         */

        // Determine node identifier of last placed node at locus A
        const uUInt last_nid_a = next_nid_a - U_TO_UINT(1);
        uAssert(last_nid_a < sc.frag_a_nid_lo);

        // Base knock-out distance
        uReal ko_dist = sim.get_intr_chr_ko_dist(sc.intr_id);
        {
            // Determine center-to-center padding until fragment A reached
            const uReal c2c_pad_A =
                sample.get_max_c2c_len(last_nid_a, sc.frag_a_nid_lo, sim);
            uAssert(c2c_pad_A > U_TO_REAL(0.0));

            // Determine if candidate is within fragment B, assumes SEO growth
            const uBool is_cand_in_frag_B = (sc.frag_b_nid_lo <= sc.node_nid);
            // Determine padding - the maximum center-to-center length of the
            // remaining nodes until fragment B is reached
            const uReal c2c_pad_B =
                (!is_cand_in_frag_B)
                    ? sample.get_max_c2c_len(sc.node_nid, sc.frag_b_nid_lo, sim)
                    : U_TO_REAL(0.0);

            // Total center-to-center padding
            const uReal c2c_pad = c2c_pad_A + c2c_pad_B;

            // Account for radius of relevant fragments A and B. Note: no radial
            // padding is needed if we are homogeneous as this is assumed to
            // have occurred in simulation initialization
            if (!sample_t::is_homg_radius) {
                const uReal radius_A =
                    sample.get_candidate_node_radius(sc.frag_a_nid_lo, sim);
                // If candidate is in fragment B, return candidate radius,
                // else use radius of first node in fragment B
                const uReal radius_B = (!is_cand_in_frag_B)
                                           ? sample.get_candidate_node_radius(
                                                 sc.frag_b_nid_lo, sim)
                                           : sc.node_radius;
                // Increment knock-out distance by radii of A and B nodes
                ko_dist += radius_A + radius_B;
            }

            // Early out if center-to-center length will satisfy inequality
            // and avoid explicit distance check
            if (c2c_pad > ko_dist) {
                return;
            }

            // Decrement the knock-out distance with center-to-center padding
            ko_dist -= c2c_pad;
        }  // end scope for early out check along with ko_dist modifications

        /**
         * If we reach here, then we need to perform explicit distance check
         * against last placed node of fragment A locus. At this point, the
         * knock-out distance threshold ('ko_dist') has been properly modified
         * to account for center-to-center and radial padding under both
         * homogeneous and heterogeneous node conditions.
         */

        // Squared knock-out distance threshold
        uAssert(ko_dist > U_TO_REAL(0.0));
        const uReal dist2_check = ko_dist * ko_dist;
        // Handle to 3-D node position of last placed node at fragment A locus
        uAssertBounds(
            last_nid_a, U_TO_UINT(0), sample.get_node_positions().n_cols);
        const uReal* const A = sample.get_node_positions().colptr(last_nid_a);
        // Buffer for storing contiguous x,y,z candidate position
        uReal B[uDim_num];
        // Buffer for computing squared distance
        uReal dist2[uDim_num];
        // Number of candidates
        const uUInt num_candidates =
            U_TO_UINT(out_legal_candidates_primed.size());

        for (uUInt i = 0; i < num_candidates; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }

            // Load candidate center
            B[uDim_X] = candidate_centers.at(i, uDim_X);
            B[uDim_Y] = candidate_centers.at(i, uDim_Y);
            B[uDim_Z] = candidate_centers.at(i, uDim_Z);

            // Compute squared distance
            U_DIST2(dist2, A, B);

            uBool b_okay = dist2[uDim_dist] > dist2_check;
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_chr_ko_on_fail(
                    p_policy_args, i, sc, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }
    }

    // Disallow any form of instantiation
    uSisIntrChrFilterKoInter(const uSisIntrChrFilterKoInter&);
    uSisIntrChrFilterKoInter& operator=(const uSisIntrChrFilterKoInter&);
};

#endif  // uSisIntrChrFilterKoInter_h
