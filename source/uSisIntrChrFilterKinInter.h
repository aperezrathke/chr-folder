//****************************************************************************
// uSisIntrChrFilterKinInter.h
//****************************************************************************

/**
 * Filter for between-loci, knock-in chr-chr interactions
 */

#ifndef uSisIntrChrFilterKinInter_h
#define uSisIntrChrFilterKinInter_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uDistUtils.h"
#include "uIntrLibDefs.h"
#include "uSisIntrChrCommonFilt.h"
#include "uSisUtilsGrowth.h"
#include "uTypes.h"

//****************************************************************************
// uSisIntrChrFilterKinInter
//****************************************************************************

/**
 * Stateless class for between-loci, knock-in chr-chr interaction testing
 */
template <typename t_SisGlue>
class uSisIntrChrFilterKinInter {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Common filter utility type
     */
    typedef uSisIntrChrCommonFilt<t_SisGlue> common_filt_t;

    /**
     * Determines if any candidate positions violate the parameter interaction
     * constraint - specifically a between (inter) loci, knock-in constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol(policy_t& policy,
                          policy_args_t* const p_policy_args,
                          uBoolVecCol& out_legal_candidates_primed,
                          const uSisIntrChrFilterCore_t& c,
                          const uMatrix& candidate_centers,
                          const sample_t& sample,
                          const sim_t& sim) {
        // Note, at this point we are assuming that we have not finished
        // growing at least one of the interacting fragments - this
        // assumption is verifed in check_format() assuming SEO growth
        uAssert(common_filt_t::check_format_mloc(c, sample, sim));

        // Format interaction arguments such that tested candidates are on
        // same locus as fragment B
        uSisIntrChrFilterCore_t sc;
        common_filt_t::format_as_B_mloc(sc, c, sample, sim);

        // Early out if fragment B already grown
        if (sc.node_nid > sc.frag_b_nid_hi) {
            // Assume fragment A has not been grown completely
            uAssert(common_filt_t::get_next_nid_at_locus(
                        sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim) <=
                    sc.frag_a_nid_hi);
            return;
        }

        // Determine growth state of alternate fragment A
        const uUInt next_nid_a = common_filt_t::get_next_nid_at_locus(
            sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim);

        // Determine if fragment A has been grown
        const uBool b_full_A = (next_nid_a > sc.frag_a_nid_hi);

        if (b_full_A) {
            // Defer to common routine
            common_filt_t::mark_viol_kin_full_A(policy,
                                                p_policy_args,
                                                out_legal_candidates_primed,
                                                sc,
                                                candidate_centers,
                                                sample,
                                                sim);
        } else {
            // Early out if alternate locus has not yet had any nodes placed
            if (sim.get_growth_nof(next_nid_a) == U_TO_UINT(0)) {
                return;
            }

            // Triangle inequality filter where fragment A is assumed to not
            // be fully grown but locus of fragment A has at least one node
            tri_ineq_filter(policy,
                            p_policy_args,
                            out_legal_candidates_primed,
                            sc,
                            next_nid_a,
                            candidate_centers,
                            sample,
                            sim);
        }
    }

    /**
     * Determines if latest node placement satisfies parameter interaction
     * constraint - specifically a between (inter) loci, knock-in constraint
     * under the assumption of single-end, ordered (SEO) growth.
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf(const uSisIntrChrFilterCore_t& c,
                           const uReal* const B,
                           const sample_t& sample,
                           const sim_t& sim) {
        uAssert(common_filt_t::check_format_mloc(c, sample, sim));

        // Format interaction arguments such that tested candidates are on
        // same locus as fragment B
        uSisIntrChrFilterCore_t sc;
        common_filt_t::format_as_B_mloc(sc, c, sample, sim);

        // Early out if we have not yet reached fragment B
        if (sc.node_nid < sc.frag_b_nid_lo) {
            return uFALSE;
        }

        // Early out if we are beyond fragment B
        if (sc.node_nid > sc.frag_b_nid_hi) {
            // Assume fragment A has not been grown completely. Note, we are
            // assuming that a future growth step for fragment A will decide
            // if interaction has been satisfied!
            uAssert(common_filt_t::get_next_nid_at_locus(
                        sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim) <=
                    sc.frag_a_nid_hi);
            return uFALSE;
        }

        // Determine growth state of alternate fragment A
        const uUInt next_nid_a = common_filt_t::get_next_nid_at_locus(
            sim.get_growth_lid(sc.frag_a_nid_lo), sample, sim);

        // Early out if alternate locus has not yet grown any portion of
        // fragment A
        if (next_nid_a <= sc.frag_a_nid_lo) {
            return uFALSE;
        }

        // Limit direct testing to only portion of fragment A that has been
        // grown. Note, if we reach here then some portion of fragment A has
        // already been grown, therefore 'next_nid_a' must be > 0.
        uAssert(next_nid_a > U_TO_UINT(0));
        sc.frag_a_nid_hi =
            std::min(sc.frag_a_nid_hi, next_nid_a - U_TO_UINT(1));
        uAssert(sc.frag_a_nid_hi >= sc.frag_a_nid_lo);

        // Defer to common routine
        return common_filt_t::is_satisf_kin_full_A(sc, B, sample, sim);
    }

private:
    /**
     * @WARNING ASSUMES AT LEAST 1 NODE OF FRAGMENT 'A' LOCUS HAS BEEN GROWN!
     * @WARNING ASSUMES that fragment A has not been completely grown!
     * ASSUMES sequential growth from first to last node. Determines which
     * positions satisfy triangle inequality which is a necessary (but not
     * sufficient) condition for satisfying knock-in proximity constraints.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param sc - "Standardized" core filter arguments such that tested
     *  candidates are on fragment B locus
     * @param next_nid_a - Next grown node identifier at fragment A locus
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    inline static void tri_ineq_filter(policy_t& policy,
                                       policy_args_t* const p_policy_args,
                                       uBoolVecCol& out_legal_candidates_primed,
                                       const uSisIntrChrFilterCore_t& sc,
                                       const uUInt next_nid_a,
                                       const uMatrix& candidate_centers,
                                       const sample_t& sample,
                                       const sim_t& sim) {
        // Assume fragment spans lo <= hi
        uAssert(sc.frag_a_nid_lo <= sc.frag_a_nid_hi);
        uAssert(sc.frag_b_nid_lo <= sc.frag_b_nid_hi);
        // Assume at least a single node exists in fragment A's locus
        uAssert(next_nid_a > U_TO_UINT(0));
        uAssert(sim.get_growth_nof(next_nid_a) > U_TO_UINT(0));
        // Assume fragment A is still growing -> if fragment A is fully grown,
        // then we should be deferring to routine(s) in common_filt_t
        uAssert(next_nid_a <= sc.frag_a_nid_hi);
        // Assume we have not grown beyond fragment B
        uAssert(sc.node_nid <= sc.frag_b_nid_hi);

        // See description of triangle inequality from:
        //  uSisIntrChrCommonFilt::tri_ineq_filter_kin_full_A(...)
        // The major difference is that fragment A is not complete and
        // therefore we can pad the distance threshold by the remaining
        // node center-to-center length of 'next_nid_a - 1' to
        // 'sc.frag_a_nid_hi' but only for the *last* placed node in fragment
        // A's locus.

        // Compute |B-C|* which is the maximum possible distance from
        // candidate node 'B' to (non-yet-grown) last node 'C' in fragment B
        const uReal dist_BC =
            sample.get_max_c2c_len(sc.node_nid, sc.frag_b_nid_hi, sim);

        // Compute (un-padded) right-hand side of triangle inequality:
        //  (|B-C|* + |A-C|)^2
        uReal dist_rhs_no_AB_pad =
            dist_BC + sim.get_intr_chr_kin_dist(sc.intr_id);
        if (!sample_t::is_homg_radius) {
            // If heterogeneous radius model, pad proximity check by node
            // radius. Note, if homogeneous radius model, radial padding is
            // assumed to have already occurred during initialization.
            dist_rhs_no_AB_pad += sc.node_radius;
        }

        // Squared proximity check with no AB pad, used for homogeneous models
        const uReal dist2_rhs_no_AB_pad =
            dist_rhs_no_AB_pad * dist_rhs_no_AB_pad;

        // Determine amount of distance padding allowed for |A-B| distance;
        // the padding accounts for the additional nodes which can be placed
        // on fragment A's locus which can decrease the |A-B| distance. This
        // padding is then used for testing if the latest placed node in
        // fragment A's locus satisfies the following triangle inequality:
        //  |A-B| - |pad| <= |B-C|* + |A-C|
        // This is equivalent to checking:
        //  |A-B| <= |B-C|* + |A-C| + |pad|
        const uUInt nid_a = next_nid_a - U_TO_UINT(1);
        const uReal dist_AB_pad =
            sample.get_max_c2c_len(nid_a, sc.frag_a_nid_hi, sim);

        // Proximity check with AB padding used for testing last placed node
        // of fragment A's locus
        uReal dist_rhs_AB_pad = dist_rhs_no_AB_pad + dist_AB_pad;
        if (!sample_t::is_homg_radius) {
            // Again, pad proximity check by radius for heterogeneous models
            dist_rhs_AB_pad += sample.get_node_radius(nid_a, sim);
        }
        // Squared proximity check with AB pad, used for homogeneous models
        const uReal dist2_rhs_AB_pad = dist_rhs_AB_pad * dist_rhs_AB_pad;

        // Handle to 3-D node positions
        const uMatrix& node_positions = sample.get_node_positions();
        // 3-D coordinate of last placed node in fragment A locus
        uAssertBounds(nid_a, U_TO_UINT(0), U_TO_UINT(node_positions.n_cols));
        const uReal* const A_head = node_positions.colptr(nid_a);

        // Buffer for storing contiguous x,y,z candidate position
        uReal B[uDim_num];
        // Buffer for computing squared distance
        uReal dist2[uDim_num];

        const uUInt num_candidates =
            U_TO_UINT(out_legal_candidates_primed.size());

        // This outer branch is used to determine if inner loop needs an
        // additional check against previously grown fragment A nodes.
        if (next_nid_a > sc.frag_a_nid_lo) {
            // CASE: Prior fragment A nodes exist:
            // Need to check latest node of A's locus using padded threshold
            // as well as previous fragment A nodes using non-padded threshold

            // Create cropped fragment A for AB testing of already placed
            // nodes which are not the most recently placed
            uSisIntrChrFilterCore_t sc_crop = sc;
            uAssertBounds(nid_a, sc.frag_a_nid_lo, sc.frag_a_nid_hi);
            sc_crop.frag_a_nid_hi = nid_a;
            uAssert(sc_crop.frag_a_nid_lo <= sc_crop.frag_a_nid_hi);

            for (uUInt i = 0; i < num_candidates; ++i) {
                // Skip if a prior constraint isn't satisfied
                if (!out_legal_candidates_primed.at(i)) {
                    continue;
                }

                // Load candidate center
                B[uDim_X] = candidate_centers.at(i, uDim_X);
                B[uDim_Y] = candidate_centers.at(i, uDim_Y);
                B[uDim_Z] = candidate_centers.at(i, uDim_Z);

                // Check if most recent node on fragment A's locus can satisfy
                // triangle inequality
                U_DIST2(dist2, A_head, B);
                uBool b_okay = (dist2[uDim_dist] <= dist2_rhs_AB_pad);

                if (!b_okay) {
                    // Most recent node test failed, see if some other node
                    // within fragment A can satisfy triangle inequality (note
                    // the lack of AB padding)
                    b_okay = (sample_t::is_homg_radius)
                                 ? common_filt_t::AB_test_homg_kin_full_A(
                                       sc_crop,
                                       B,
                                       dist2,
                                       dist2_rhs_no_AB_pad,
                                       node_positions)
                                 : common_filt_t::AB_test_hetr_kin_full_A(
                                       sc_crop,
                                       B,
                                       dist2,
                                       dist_rhs_no_AB_pad,
                                       node_positions,
                                       sample,
                                       sim);
                    if (!b_okay) {
                        // Let policy have final say
                        b_okay = policy.intr_chr_kin_on_fail(
                            p_policy_args, i, sc, sample, sim);
                    }
                }

                out_legal_candidates_primed.at(i) = b_okay;
            }  // end of iteration over primed legal candidates

        } else {
            // CASE: No previously grown fragment A nodes exist
            // Only need to check against latest node of A's locus using
            // padded threshold

            for (uUInt i = 0; i < num_candidates; ++i) {
                // Skip if a prior constraint isn't satisfied
                if (!out_legal_candidates_primed.at(i)) {
                    continue;
                }

                // Load candidate center
                B[uDim_X] = candidate_centers.at(i, uDim_X);
                B[uDim_Y] = candidate_centers.at(i, uDim_Y);
                B[uDim_Z] = candidate_centers.at(i, uDim_Z);

                // Only check if most recent node on fragment A's locus
                // can satisfy triangle inequality
                U_DIST2(dist2, A_head, B);
                uBool b_okay = (dist2[uDim_dist] <= dist2_rhs_AB_pad);
                if (!b_okay) {
                    // Let policy have final say
                    b_okay = policy.intr_chr_kin_on_fail(
                        p_policy_args, i, sc, sample, sim);
                }
                out_legal_candidates_primed.at(i) = b_okay;
            }
        }
    }

    // Disallow any form of instantiation
    uSisIntrChrFilterKinInter(const uSisIntrChrFilterKinInter&);
    uSisIntrChrFilterKinInter& operator=(const uSisIntrChrFilterKinInter&);
};

#endif  // uSisIntrChrFilterKinInter_h
