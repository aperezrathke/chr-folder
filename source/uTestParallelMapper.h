//****************************************************************************
// uTestParallelMapper.h
//****************************************************************************

/**
 * Commandlet for testing simple usage of a parallel mapper
 *
 * Usage:
 * -test_parallel_mapper
 */

#ifdef uTestParallelMapper_h
#   error "Test Parallel Mapper included multiple times!"
#endif  // uTestParallelMapper_h
#define uTestParallelMapper_h

#include "uBuild.h"

#if defined(U_BUILD_ENABLE_TESTS) && defined(U_BUILD_ENABLE_THREADS)

#include "uCmdOptsMap.h"
#include "uExitCodes.h"
#include "uLogf.h"
#include "uThread.h"

namespace {
    class uTestMappableWork : public uMappableWork {
    public:
        /**
         * Assigns an array for processing
         */
        explicit uTestMappableWork(std::vector<std::string>& array_to_process)
            : m_cached_size(U_TO_UINT(array_to_process.size())),
              m_array_to_process(array_to_process) {}

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual uBool do_range(const uUInt ix_start,
                               const uUInt count,
                               const uUInt thread_id) {
            const uUInt end = std::min(ix_start + count, m_cached_size);
            for (uUInt i = ix_start; i < end; ++i) {
                uLogf("THREAD %u: (%u, %u) -> %s\n",
                      thread_id,
                      i,
                      count,
                      m_array_to_process[i].c_str());
            }

            return end != m_cached_size;
        }

    private:
        const uUInt m_cached_size;
        std::vector<std::string>& m_array_to_process;
    };
}  // namespace

/**
 * Entry point for test
 */
int uTestParallelMapperMain(const uCmdOptsMap& cmd_opts) {
    uLogf("Running Test Parallel Mapper.\n");

    uParallelMapper pm;
    pm.init(U_DEFAULT_NUM_THREADS);
    pm.log_status();

    uLogf("\n-------------------\nFirst test:\n");

    std::vector<std::string> data;
    data.push_back("0-This");
    data.push_back("1-is");
    data.push_back("2-a");
    data.push_back("3-test");
    data.push_back("4-of");
    data.push_back("5-the");
    data.push_back("6-parallel");
    data.push_back("7-mapper.");
    data.push_back("8-We");
    data.push_back("9-are");
    data.push_back("10-printing");
    data.push_back("11-out");
    data.push_back("12-some");
    data.push_back("13-words");
    data.push_back("14-to");
    data.push_back("15-observe");
    data.push_back("16-how");
    data.push_back("17-well");
    data.push_back("18-the");
    data.push_back("19-threads");
    data.push_back("20-run.");

    uTestMappableWork work1(data);

    pm.wait_until_work_finished(work1, 5 /* inc_by */);

    uLogf("\n-------------------\nSecond test:\n");

    data.push_back("21-Let's");
    data.push_back("22-try");
    data.push_back("23-this");
    data.push_back("24-again.");

    uTestMappableWork work2(data);

    pm.wait_until_work_finished(work2, 3 /* inc_by */);

    uLogf("\n-------------------\nWork has finished. Exiting.\n");

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS && U_BUILD_ENABLE_THREADS
