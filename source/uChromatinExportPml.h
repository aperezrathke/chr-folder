//****************************************************************************
// uChromatinExportPml.h
//****************************************************************************

/**
 * Utility for writing PML chromatin files
 */

#ifndef uChromatinExportPml_h
#define uChromatinExportPml_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportArgs.h"

#include <iostream>

//****************************************************************************
// PML export
//****************************************************************************

/**
 * Effectively a namespace for Pml chromatin export utilities
 */
class uChromatinExportPml {
public:
    /**
     * Writes sample node positions to PML format. PML files can be opened
     * directly in PyMOL and may contain embedded PyMOL script commands.
     * @WARNING: The embedded script may possibly include arbitrary python
     *  commands, careful when opening (view in text editor first!)
     *
     * @param fpath - path to write the exported sample
     * @param args - exporter arguments, note 'scale' is applied to all node
     *  positions, this helps conform to the PDB format as otherwise the
     *  coordinates may overflow alloted character column boundaries.
     */
    static void write_chromatin(const std::string& fpath,
                                const uChromatinExportArgs_t& args);

    /**
     * Write sample node positions to 'out' stream
     * @WARNING - DOES NOT CLOSE STREAM!
     * @param out - output stream
     * @param args - exporter args
     */
    static void write_chromatin(std::ostream& out,
                                const uChromatinExportArgs_t& args);

    /**
     * @return exporter name
     */
    static std::string get_name() { return std::string("pml"); }

    /**
     * @return exporter format
     */
    static std::string get_format() { return get_name(); }

    /**
     * Warn user of potential formatting issues based on polymer size
     * @param max_total_num_nodes - polymer size
     */
    static void warn_format(const uUInt max_total_num_nodes);
};

#endif  // uChromatinExportPml_h
