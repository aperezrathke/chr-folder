//****************************************************************************
// uSisSample.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef uSisSample_inl
#   error "Sis Sample implementation included multiple times!"
#endif  // uSisSample_inl
#define uSisSample_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uTypes.h"

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Default constructor
 */
template <typename t_SisGlue>
uSisSample<t_SisGlue>::uSisSample() {
    clear();
}

/**
 * Constructs from simulation and configuration objects
 * @param sim - the parent simulation
 * @param config - the configuration specifying simulation parameters
 */
template <typename t_SisGlue>
uSisSample<t_SisGlue>::uSisSample(const sim_t& sim,
                                  uSpConstConfig_t config U_THREAD_ID_PARAM) {
    init(sim, config U_THREAD_ID_ARG);
}

/**
 * Initializes from simulation and configuration objects
 * @param sim - the parent simulation
 * @param config - the configuration specifying simulation parameters
 */
template <typename t_SisGlue>
void uSisSample<t_SisGlue>::init(const sim_t& sim,
                                 uSpConstConfig_t config U_THREAD_ID_PARAM) {
    // Wipe the slate
    this->clear();

    // Note: In the future, the format of the node positions matrix may be
    // left to the growth mixin.

    // Resize node positions
    this->node_positions.resize(uDim_num, sim.get_max_total_num_nodes());

    // Initialize mixins
    this->node_radius_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->growth_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->nuclear_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->collision_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->intr_chr_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->intr_lam_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->intr_nucb_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
    this->energy_mixin_t::init(*this, sim, config U_THREAD_ID_ARG);
}

/**
 * Sets sample to default uninitialized state
 */
template <typename t_SisGlue>
void uSisSample<t_SisGlue>::clear() {
    this->fail_status = 0;
    this->node_positions.clear();
    this->node_radius_mixin_t::clear();
    this->growth_mixin_t::clear();
    this->nuclear_mixin_t::clear();
    this->collision_mixin_t::clear();
    this->intr_chr_mixin_t::clear();
    this->intr_lam_mixin_t::clear();
    this->intr_nucb_mixin_t::clear();
    this->energy_mixin_t::clear();
}

/**
 * Grows the initial node(s)
 * @param log_weight - the current log weight of this sample
 * @param sim - the parent simulation
 * @return TRUE if sample needs to continue growing, FALSE if node is done
 * growing (either because of completion or because it's dead)
 */
template <typename t_SisGlue>
inline bool uSisSample<t_SisGlue>::seed(uReal& log_weight,
                                        const sim_t& sim U_THREAD_ID_PARAM) {
    // Defer to mixin
    return this->growth_mixin_t::seed(*this, log_weight, sim U_THREAD_ID_ARG);
}

/**
 * Incrementally grows all associated loci chains for a single simulation step
 * directly after seeding.
 * @param log_weight - the current log weight of this sample
 * @param num_completed_grow_steps - The number of growth
 *  calls (seed() + grow_*()) that have completed. This value is
 *  reset between trials.
 * @param sim - the parent simulation
 * @return TRUE if sample needs to continue growing, FALSE if node is done
 * growing (either because of completion or because it's dead). In addition,
 * the log weight parameter is expected to be updated.
 */
template <typename t_SisGlue>
inline bool uSisSample<t_SisGlue>::grow_2nd(
    uReal& log_weight,
    const uUInt num_completed_grow_steps,
    const sim_t& sim U_THREAD_ID_PARAM) {
    uAssert(num_completed_grow_steps == 1);
    // Should never trip these
    uAssert(!this->is_dead());
    uAssert(this->get_num_grown_nodes(num_completed_grow_steps, *this, sim) <
            sim.get_max_total_num_nodes());

    // Defer to mixin
    return this->growth_mixin_t::grow_2nd(
        *this, log_weight, num_completed_grow_steps, sim U_THREAD_ID_ARG);
}

/**
 * Incrementally grows all associated loci chains for a single simulation step
 * after seeding and second growth phases have completed.
 * @param log_weight - the current log weight of this sample
 * @param num_completed_grow_steps - The number of growth
 *  calls (seed() + grow_*()) that have completed. This value is
 *  reset between trials.
 * @param sim - the parent simulation
 * @return TRUE if sample needs to continue growing, FALSE if node is done
 * growing (either because of completion or because it's dead). In addition,
 * the log weight parameter is expected to be updated.
 */
template <typename t_SisGlue>
inline bool uSisSample<t_SisGlue>::grow_nth(
    uReal& log_weight,
    const uUInt num_completed_grow_steps,
    const sim_t& sim U_THREAD_ID_PARAM) {
    // Should never trip these
    uAssert(!this->is_dead());
    uAssert(this->get_num_grown_nodes(num_completed_grow_steps, *this, sim) <
            sim.get_max_total_num_nodes());

    // Defer to mixin
    return this->growth_mixin_t::grow_nth(
        *this, log_weight, num_completed_grow_steps, sim U_THREAD_ID_ARG);
}
