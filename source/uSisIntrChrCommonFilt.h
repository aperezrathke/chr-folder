//****************************************************************************
// uSisIntrChrCommonFilt.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utilities for chr-chr interaction filtering
 */

#ifndef uSisIntrChrCommonFilt_h
#define uSisIntrChrCommonFilt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uDistUtils.h"
#include "uIntrLibDefs.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Structs
//****************************************************************************

/**
 * Core arguments used by chr-chr interaction filter utilities
 */
typedef struct {
    /**
     * Interaction column index
     */
    uUInt intr_id;
    /**
     * Lower (min) node identifier for 1st fragment in interaction pair; note
     *  lo <= hi
     */
    uUInt frag_a_nid_lo;
    /**
     * Upper (max) node identifier for 1st fragment in interaction pair
     */
    uUInt frag_a_nid_hi;
    /**
     * Lower (min) node identifier for 2nd fragment in interaction pair
     */
    uUInt frag_b_nid_lo;
    /**
     * Upper (max) node identifier for 2nd fragment in interaction pair
     */
    uUInt frag_b_nid_hi;
    /**
     * Node identifier for either a candidate node (if marking violators) or a
     *  recently placed node (if checking constraint satisfaction)
     */
    uUInt node_nid;
    /**
     * The locus identifier corresponding to 'node_nid'
     */
    uUInt node_lid;
    /**
     * The node radius corresponding to 'node_nid'; note all candidate nodes
     *  with the same node identifier are assumed to have same radius
     */
    uReal node_radius;
} uSisIntrChrFilterCore_t;

//****************************************************************************
// Methods
//****************************************************************************

/**
 * Effectively namespace for shared interaction filter utilities
 */
template <typename t_SisGlue>
class uSisIntrChrCommonFilt {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Given that fragment A span has been grown, determines if any candidate
     * positions violate the parameter knock-out interaction constraint.
     * @WARNING - ASSUMES SINGLE-END, ORDERED (SEO) GROWTH!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol_ko_full_A(policy_t& policy,
                                    policy_args_t* const p_policy_args,
                                    uBoolVecCol& out_legal_candidates_primed,
                                    const uSisIntrChrFilterCore_t& c,
                                    const uMatrix& candidate_centers,
                                    const sample_t& sample,
                                    const sim_t& sim) {
        /**
         * Ultimately, a candidate position 'cand' is not a violator if the
         * following holds:
         *
         *  d(cand, A_min) + pad - r(A_min) - r(cand, B0) > d_ko
         *
         * which is equivalent to:
         *
         *  d(cand, A_min) > d_ko + r(A_min) + r(cand, B0) - pad
         *
         * where:
         *
         *  - cand: Candidate node position
         *  - A_min: Node in fragment A which is closest to candidate 'cand'
         *  - d(cand, A_min): Euclidean distance between 'cand' and 'A_min'
         *  - pad: Maximum center-to-center (c2c) length of the remaining
         *      nodes to be grown beyond 'cand' until fragment B is reached.
         *      This value is 0 if 'cand' is within fragment B.
         *  - r(A_min): Radius of node 'A_min' (nearest node in fragment A
         *      to 'cand')
         *  - B0: The first node of fragment B (may not have been grown yet)
         *  - r(cand, B0): If 'cand' is within fragment B, expression
         *      evaluates to radius of 'cand', else expression evaluates to
         *      radius of node 'B0'
         *  - d_ko: User configured knock-out distance threshold such that all
         *      nodes in fragment B are at least this distance from fragment A
         *
         * Ultimately, we will compare the squared version of the left-hand
         * and right-hand sides of the inequality.
         *
         * This implies we can early out and avoid computing pairwise
         * distances if the following condition is met:
         *
         *  pad > d_ko + r(cand, B0) + r_max
         *
         * where 'r_max' is maximum node radius
         *
         * Note: FOR HOMOGENEOUS POLYMERS ONLY (all nodes having same radius),
         *  the knock-out distance threshold has already been pre-incremented
         *  by the term 'r(A_min) + r(cand, B0)'. In other words:
         *
         *  d_ko = d_ko_user + r(A_min) + r(cand, B0)
         *
         * where 'd_ko_user' is the original user defined knock-out threshold
         */

        // Candidate node assumed to be from fragment B
        uAssert(sim.get_growth_lid(c.frag_b_nid_lo) == c.node_lid);
        // We should not be checking this constraint if fragment B has already
        // been grown
        uAssert(c.node_nid <= c.frag_b_nid_hi);

        // Base knock-out distance
        uReal ko_dist = sim.get_intr_chr_ko_dist(c.intr_id);

        {
            // Determine if candidate is within fragment B, assumes SEO growth
            const uBool is_cand_in_frag_B = (c.frag_b_nid_lo <= c.node_nid);
            // Determine padding - the maximum center-to-center length of the
            // remaining nodes until fragment B is reached
            const uReal c2c_pad =
                (!is_cand_in_frag_B)
                    ? sample.get_max_c2c_len(c.node_nid, c.frag_b_nid_lo, sim)
                    : U_TO_REAL(0.0);

            // Account for radius of relevant fragment B node. Note: no radial
            // padding is needed if we are homogeneous as this is assumed to
            // have occurred in simulation initialization
            if (!sample_t::is_homg_radius) {
                // If candidate is in fragment B, return candidate radius,
                // else use radius of first node in fragment B
                const uReal radius_B =
                    (!is_cand_in_frag_B)
                        ? sample.get_candidate_node_radius(c.frag_b_nid_lo, sim)
                        : c.node_radius;
                // Increment knock-out distance by radius of fragment B node
                ko_dist += radius_B;
            }

            // Early out if center-to-center length will satisfy inequality
            // and avoid explicit distance checks
            if (!sample_t::is_homg_radius) {
                // Heterogeneous radius: use worst case node radius as proxy
                // for radius of nearest node within fragment A
                const uReal radius_max =
                    U_TO_REAL(0.5) * sim.get_max_node_diameter();
                if (c2c_pad > (ko_dist + radius_max)) {
                    return;
                }
            } else {
                // Homogeneous radius: the distance threshold has already been
                // incremented by max node diameter during sim initialization
                if (c2c_pad > ko_dist) {
                    return;
                }
            }

            // Decrement the knock-out distance with center-to-center padding
            ko_dist -= c2c_pad;
        }  // end scope for early out check along with ko_dist modifications

        /**
         * If we reach here, then we need to check candidates against all
         * fragment A nodes. Also note, for both homogeneous and heterogeneous
         * nodes, the knock-out distance threshold is now equivalent to
         *
         *  ko_dist = d_ko_user + r(cand, B0) - c2c_pad
         *
         * Hence, a candidate should pass filter under the following condition
         *
         *  d(cand, a) > ko_dist + r(a) for all 'a' in fragment A
         *
         * Since we typically work with squared distances, we have to be
         * careful with the inequality as the right-hand side may in fact be
         * negative (in which case, candidate passes for that specific case)
         */

        uReal dist2_check;
        if (sample_t::is_homg_radius) {
            // For homogeneous nodes, square the threshold outside of inner
            // loop as radial padding has already been accounted
            dist2_check = ko_dist * ko_dist;
        }
        // Handle to 3-D node positions
        const uMatrix& node_positions = sample.get_node_positions();
        // Buffer for storing contiguous x,y,z candidate position
        uReal B[uDim_num];
        // Buffer for computing squared distance
        uReal dist2[uDim_num];
        // Number of candidates
        const uUInt num_candidates =
            U_TO_UINT(out_legal_candidates_primed.size());
        const int nid_lo = static_cast<int>(c.frag_a_nid_lo);
        const int nid_hi = static_cast<int>(c.frag_a_nid_hi);
        for (uUInt i = 0; i < num_candidates; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }

            // Load candidate center
            B[uDim_X] = candidate_centers.at(i, uDim_X);
            B[uDim_Y] = candidate_centers.at(i, uDim_Y);
            B[uDim_Z] = candidate_centers.at(i, uDim_Z);

            // Check for constraint violation against all fragment A nodes
            uBool b_okay = uTRUE;
            if (sample_t::is_homg_radius) {
                // HOMOGENEOUS NODE CASE
                // Note, if ko_dist < 0, then early out case should have
                // triggered already for homogeneous case
                uAssert(ko_dist > U_TO_REAL(0.0));
                // Determine if a node in fragment A satisfies spatial
                // knock-out constraint. Using reverse iteration as genomic
                // distance correlates with spatial distance and we want to
                // hit a small |A-B| distance to hopefully early out.
                for (int j = nid_hi; j >= nid_lo; --j) {
                    uAssertBounds(
                        j, 0, static_cast<int>(node_positions.n_cols));
                    const uReal* const A = node_positions.colptr(j);
                    U_DIST2(dist2, A, B);

                    // Check if distance inequality is violated. Note, the
                    // distance check has already been squared outside of
                    // inner loop
                    b_okay = (dist2[uDim_dist] > dist2_check);
                    if (!b_okay) {
                        // We found a node that is too close, no need to check
                        // any more nodes!
                        break;
                    }
                }
            } else {
                // HETEROGENEOUS NODE CASE
                // Determine if a node in fragment A satisfies spatial
                // knock-out constraint. Using reverse iteration as genomic
                // distance correlates with spatial distance and we want to
                // hit a small |A-B| distance to hopefully early out.
                for (int j = nid_hi; j >= nid_lo; --j) {
                    uAssertBounds(
                        j, 0, static_cast<int>(node_positions.n_cols));
                    const uReal* const A = node_positions.colptr(j);
                    U_DIST2(dist2, A, B);
                    dist2_check = ko_dist + sample.get_node_radius(j, sim);
                    // If we have a negative distance check, then squaring
                    // result is incorrect, simply move to next node
                    if (dist2_check < U_TO_REAL(0.0)) {
                        continue;
                    }
                    // Check if distance inequality is violated.
                    dist2_check *= dist2_check;
                    b_okay = (dist2[uDim_dist] > dist2_check);
                    if (!b_okay) {
                        // We found a node that is too close, no need to check
                        // any more nodes!
                        break;
                    }
                }
            }  // END TEST AGAINST ALL FRAGMENT A NODES

            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_chr_ko_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }  // end iteration over all candidates
    }

    /**
     * Given that fragment A span has been grown, determines if any candidate
     * positions violate the parameter knock-in interaction constraint.
     * @WARNING - ASSUMES SINGLE-END, ORDERED (SEO) GROWTH!
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *	set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    static void mark_viol_kin_full_A(policy_t& policy,
                                     policy_args_t* const p_policy_args,
                                     uBoolVecCol& out_legal_candidates_primed,
                                     const uSisIntrChrFilterCore_t& c,
                                     const uMatrix& candidate_centers,
                                     const sample_t& sample,
                                     const sim_t& sim) {
        // Candidate node assumed to be from fragment B
        uAssert(sim.get_growth_lid(c.frag_b_nid_lo) == c.node_lid);
        if (c.node_nid < c.frag_b_nid_hi) {
            // Candidate only needs to satisfy triangle inequality as there
            // are still additional nodes that can be grown which may also
            // satisfy the interaction constraint.
            tri_ineq_filter_kin_full_A(policy,
                                       p_policy_args,
                                       out_legal_candidates_primed,
                                       c,
                                       candidate_centers,
                                       sample,
                                       sim);
        } else {
            uAssert(c.node_nid == c.frag_b_nid_hi);
            // Directly check proximity constraint as there are no more nodes
            // to grow in fragment B beyond this one.
            AB_filter_kin_full_A(policy,
                                 p_policy_args,
                                 out_legal_candidates_primed,
                                 c,
                                 sim.get_intr_chr_kin_dist(c.intr_id),
                                 sim.get_intr_chr_kin_dist2(c.intr_id),
                                 candidate_centers,
                                 sample,
                                 sim);
        }
    }

    /**
     * Given that fragment A span is fully grown, determines if latest node
     * placement satisfies the parameter knock-in interaction constraint
     * @WARNING - ASSUMES SINGLE-END, ORDERED (SEO) GROWTH!
     * @param c - Core filter arguments
     * @param B - 3D coordinates of node centroid
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @return uTRUE if parameter constraint satisfied, uFALSE o/w
     */
    static uBool is_satisf_kin_full_A(const uSisIntrChrFilterCore_t& c,
                                      const uReal* const B,
                                      const sample_t& sample,
                                      const sim_t& sim) {
        // Handle to 3-D node positions
        const uMatrix& node_positions = sample.get_node_positions();

        // Buffer for computing squared distance
        uReal dist2[uDim_num];

        // See if proximity constraint is satisfied
        const uUInt i = c.intr_id;
        return (sample_t::is_homg_radius)
                   ? AB_test_homg_kin_full_A(c,
                                             B,
                                             dist2,
                                             sim.get_intr_chr_kin_dist2(i),
                                             node_positions)
                   : AB_test_hetr_kin_full_A(
                         c,
                         B,
                         dist2,
                         sim.get_intr_chr_kin_dist(i) + c.node_radius,
                         node_positions,
                         sample,
                         sim);
    }

    /**
     * Utility method for checking that some squared distance from fragment A
     * to a candidate node (B) is less than or equal to dist2_check parameter.
     * ASSUMES ALL POLYMER NODES HAVE SAME RADIUS AND THAT dist2_check HAS
     * BEEN PRE-PADDED BY THE NODE RADIUS APPROPRIATELY.
     */
    inline static uBool AB_test_homg_kin_full_A(
        const uSisIntrChrFilterCore_t& c,
        const uReal* const B,
        uReal* const dist2,
        const uReal dist2_check,
        const uMatrix& node_positions) {
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        uAssert(B);
        uAssert(dist2);

        // Determine if a node in fragment A satisfies spatial proximity.
        // Using reverse iteration as genomic distance correlates with
        // spatial distance and we want to hit a small |A-B| distance to
        // hopefully early out of the iteration.
        const int nid_lo = static_cast<int>(c.frag_a_nid_lo);
        const int nid_hi = static_cast<int>(c.frag_a_nid_hi);
        for (int j = nid_hi; j >= nid_lo; --j) {
            uAssertBounds(j, 0, static_cast<int>(node_positions.n_cols));
            const uReal* const A = node_positions.colptr(j);
            U_DIST2(dist2, A, B);

            // Check if distance inequality is satisfied.
            if (dist2[uDim_dist] <= dist2_check) {
                // We found a node that works! No need to check any more
                // nodes within fragment A.
                return uTRUE;
            }
        }
        return uFALSE;
    }

    /**
     * Utility method for checking that some squared distance from fragment A
     * to a candidate node (B) is less than or equal to dist_check parameter.
     * NOTE DIFFERENCE BETWEEN *_homg and *_hetr variants of the AB kin test:
     *  - *_homg takes an already squared, radius pre-adjusted dist2_check
     *  - *_hetr takes a non-squared, non radius pre-adjusted dist_check
     */
    inline static uBool AB_test_hetr_kin_full_A(
        const uSisIntrChrFilterCore_t& c,
        const uReal* const B,
        uReal* const dist2,
        const uReal dist_check,
        const uMatrix& node_positions,
        const sample_t& sample,
        const sim_t& sim) {
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        uAssert(B);
        uAssert(dist2);

        // Determine if a node in fragment A satisfies spatial proximity.
        // Using reverse iteration as genomic distance correlates with
        // spatial distance and we want to hit a small |A-B| distance to
        // hopefully early out of the iteration.
        const int nid_lo = static_cast<int>(c.frag_a_nid_lo);
        const int nid_hi = static_cast<int>(c.frag_a_nid_hi);
        uReal dist2_check;
        for (int j = nid_hi; j >= nid_lo; --j) {
            uAssertBounds(j, 0, static_cast<int>(node_positions.n_cols));
            const uReal* const A = node_positions.colptr(j);
            U_DIST2(dist2, A, B);

            // Compute squared distance check
            dist2_check = dist_check + sample.get_node_radius(j, sim);
            dist2_check *= dist2_check;

            // Check if distance inequality is satisfied.
            if (dist2[uDim_dist] <= dist2_check) {
                // We found a node that works! No need to check any more
                // nodes within fragment A.
                return uTRUE;
            }
        }
        return uFALSE;
    }

    /**
     * Asserts if interaction is not formatted properly for single-locus growth
     * @WARNING - ASSUMES SEO GROWTH!
     * @param c - Core filter arguments
     * @param sim - Outer simulation containing global sample data
     */
    static uBool check_format_sloc(const uSisIntrChrFilterCore_t& c,
                                   const sim_t& sim) {
        // Assume SEO growth, constraint testing should be avoided after we've
        // grown beyond fragment B.
        uAssert(c.node_nid <= c.frag_b_nid_hi);
        // Assume valid bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBounds(c.frag_a_nid_lo, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_a_nid_hi, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_b_nid_lo, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_b_nid_hi, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.node_nid, U_TO_UINT(0), max_nodes);
        // Assume single locus
        const uUInt lid = sim.get_growth_lid(c.frag_a_nid_lo);
        uAssertBounds(lid, U_TO_UINT(0), sim.get_num_loci());
        uAssert(lid == sim.get_growth_lid(c.frag_a_nid_hi));
        uAssert(lid == sim.get_growth_lid(c.frag_b_nid_lo));
        uAssert(lid == sim.get_growth_lid(c.frag_b_nid_hi));
        uAssert(lid == sim.get_growth_lid(c.node_nid));
        // Assume lo <= hi
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        uAssert(c.frag_b_nid_lo <= c.frag_b_nid_hi);
        // Assume frag_a < frag_b (non-overlapping)
        uAssert(c.frag_a_nid_hi < c.frag_b_nid_lo);
        return uTRUE;
    }

    /**
     * Used for single-locus growth, determine if we can avoid interaction
     * tests. If 'nid' is not in inclusive range [min_nid, max_nid], then we
     * may be able to avoid interaction testing.
     * @WARNING - ASSUMES SEO GROWTH!
     * @param nid - Node identifier
     * @param min_nid - Minimum node identifier
     * @param max_nid - Maximum node identifier
     * @return uTRUE if interaction testing can be avoid, uFALSE o/w
     */
    inline static uBool should_early_out_sloc(const uUInt nid,
                                              const uUInt min_nid,
                                              const uUInt max_nid) {
        uAssert(min_nid <= max_nid);
        // Early out if we're not in a relevant region
        return ((nid < min_nid) || (nid > max_nid));
    }

    /**
     * Asserts if interaction is not formatted properly for multi-loci growth
     * @WARNING - ASSUMES SEO GROWTH!
     * @param c - Core filter arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     */
    inline static uBool check_format_mloc(const uSisIntrChrFilterCore_t& c,
                                          const sample_t& sample,
                                          const sim_t& sim) {
        // Assume valid bounds
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        uAssertBounds(c.frag_a_nid_lo, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_a_nid_hi, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_b_nid_lo, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.frag_b_nid_hi, U_TO_UINT(0), max_nodes);
        uAssertBounds(c.node_nid, U_TO_UINT(0), max_nodes);
        // Assume only two loci involved
        uAssert(c.node_lid == sim.get_growth_lid(c.node_nid));
        const uUInt lid_a = sim.get_growth_lid(c.frag_a_nid_lo);
        const uUInt lid_b = sim.get_growth_lid(c.frag_b_nid_lo);
        uAssertBounds(lid_a, U_TO_UINT(0), sim.get_num_loci());
        uAssertBounds(lid_b, U_TO_UINT(0), sim.get_num_loci());
        uAssert(lid_a == sim.get_growth_lid(c.frag_a_nid_hi));
        uAssert(lid_a != lid_b);
        uAssert(lid_b == sim.get_growth_lid(c.frag_b_nid_hi));
        uAssert((c.node_lid == lid_a) || (c.node_lid == lid_b));
        // Assume lo <= hi
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        uAssert(c.frag_b_nid_lo <= c.frag_b_nid_hi);
        // Assume fragments are non-overlapping
        uAssert((c.frag_a_nid_hi < c.frag_b_nid_lo) ||
                (c.frag_b_nid_hi < c.frag_a_nid_lo));
        // Assume at least one of the fragments A or B has not been entirely
        // grown (also implicitly assumes single-end, ordered growth)
        const uUInt next_nid_a = get_next_nid_at_locus(lid_a, sample, sim);
        const uUInt next_nid_b = get_next_nid_at_locus(lid_b, sample, sim);
        uAssert((next_nid_a <= c.frag_a_nid_hi) ||
                (next_nid_b <= c.frag_b_nid_hi));
        return uTRUE;
    }

    /**
     * Format interaction arguments so that tested node is always on same
     * locus as fragment 'B'.
     * @param sc - Output standardized core arguments such that tested node
     *  will correspond to fragment 'B' parameters
     * @param c - Input non-standardized core arguments
     * @param sample - Parent sample
     * @param sim - Parent simulation
     */
    inline static void format_as_B_mloc(uSisIntrChrFilterCore_t& sc,
                                        const uSisIntrChrFilterCore_t& c,
                                        const sample_t& sample,
                                        const sim_t& sim) {
        // Format such that tested node(s) are from fragment B locus
        sc = c;
        if (sim.get_growth_lid(c.frag_a_nid_lo) == c.node_lid) {
            std::swap(sc.frag_a_nid_lo, sc.frag_b_nid_lo);
            std::swap(sc.frag_a_nid_hi, sc.frag_b_nid_hi);
        }
        uAssert(check_format_mloc(sc, sample, sim));
    }

    /**
     * Used for multi-loci growth, obtain node identifier of next node to be
     * placed at parameter 'lid' locus
     * @param lid - Locus identifier
     * @param sample - Parent sample
     * @param sim - Outer simulation
     * @return Node identifier of *next* node to be placed at parameter locus
     * @WARNING - If locus has been grown entirely, this will return a node
     *  identifier that is 1 past last legal identifier
     * @WARNING - ASSUMES SEO GROWTH!
     */
    inline static uUInt get_next_nid_at_locus(const uUInt lid,
                                              const sample_t& sample,
                                              const sim_t& sim) {
        uAssertBounds(lid, U_TO_UINT(0), sim.get_num_loci());
        const uUInt num_grow_steps =
            sim.get_num_completed_grow_steps_per_sample();
        const uUInt num_grown_at_locus = sample.get_num_grown_nodes_at_locus(
            lid, num_grow_steps, sample, sim);
        uAssertBoundsInc(num_grown_at_locus,
                         U_TO_UINT(0),
                         sim.get_max_num_nodes_at_locus()[lid]);
        const uUInt start_nid = sim.get_growth_nid(lid, U_TO_UINT(0));
        // Assumes node identifiers follow sequentially within locus (SEO)
        const uUInt next_nid = start_nid + num_grown_at_locus;
        uAssertBoundsInc(next_nid, U_TO_UINT(0), sim.get_max_total_num_nodes());
        return next_nid;
    }

private:
    /**
     * ASSUMES sequential growth from first to last node. Determines which
     * positions satisfy triangle inequality which is a necessary (but not
     * sufficient) condition for satisfying knock-in proximity constraints.
     * @param policy - Callback policy for marking violators
     * @param p_policy_args - Pointer to additional policy arguments
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param c - Core filter arguments
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for interaction constraint violations.
     * @param sample - Parent sample
     * @param sim - Parent simulation containing global sample data
     */
    template <typename policy_t, typename policy_args_t>
    inline static void tri_ineq_filter_kin_full_A(
        policy_t& policy,
        policy_args_t* const p_policy_args,
        uBoolVecCol& out_legal_candidates_primed,
        const uSisIntrChrFilterCore_t& c,
        const uMatrix& candidate_centers,
        const sample_t& sample,
        const sim_t& sim) {
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        // Assume we are not at last fragment B node
        uAssert(c.node_nid < c.frag_b_nid_hi);

        /**
         * Select candidates which satisfy triangle inequality:
         *
         *                       B
         *
         *                       +
         *                      + +
         *                     +   +
         *                    +     +
         *                   +       +
         *                  +         +
         *                 +           +
         *        |A-B|   +             +   |B-C|*
         *               +               +
         *              +                 +
         *             +                   +
         *            +                     +
         *           +                       +
         *          +                         +
         *         +                           +
         *        +                             +
         *    A  +++++++++++++++++++++++++++++++++   C
         *
         *                     |A-C|
         *
         * A, B, C are triangle vertices. A is a polymer node that has already
         * been grown, B is candidate polymer node, and therefore length of
         * vector |A-B| is known. Additionally, nodes A and C are constrained
         * to be within a known proximity distance |A-C|. The triangle
         * inequality to satisfy is therefore:
         *
         *      |A-B| <= |B-C|* + |A-C| where |B-C|* is the maximal possible
         *      length of the B to C node segment.
         *
         * Satisfying the inequality does not guarantee the interaction
         * constraint will be satisfied; however if inequality is violated,
         * then the interaction can never be satisfied (i.e. the triangle
         * inequality constraint is a necessary but not sufficient condition
         * for satisfying a proximity interaction constraint).
         *
         * Also, working with squared distances can be faster if it lets us
         * avoid square root calls. In this case, the inequality becomes:
         *
         *      |A-B|^2 <= (|B-C|* + |A-C|)^2
         *
         * This can be computed without square roots since |A-C| is already
         * known and |B-C|* is just a multiple of the node diameter.
         *
         * Note, to account for radius, we pad |A-C| such that:
         *
         *      |A-C| = threshold proximity distance + radius(A) + radius(C)
         *
         * Note, FOR HOMOGENEOUS POLYMERS ONLY (all nodes having same radius),
         * |A-C| radius padding occurred during simulation init(...).
         */

        // Compute right-hand side of triangle inequality: (|B-C|* + |A-C|)^2
        const uReal dist_BC =
            sample.get_max_c2c_len(c.node_nid, c.frag_b_nid_hi, sim);
        const uReal rhs = dist_BC + sim.get_intr_chr_kin_dist(c.intr_id);

        AB_filter_kin_full_A(policy,
                             p_policy_args,
                             out_legal_candidates_primed,
                             c,
                             rhs,
                             rhs * rhs,
                             candidate_centers,
                             sample,
                             sim);
    }

    /**
     * Utility tests candidates for violation of knock-in distance constraint
     */
    template <typename policy_t, typename policy_args_t>
    inline static void AB_filter_kin_full_A(
        policy_t& policy,
        policy_args_t* const p_policy_args,
        uBoolVecCol& out_legal_candidates_primed,
        const uSisIntrChrFilterCore_t& c,
        uReal dist_check,
        const uReal dist2_check,
        const uMatrix& candidate_centers,
        const sample_t& sample,
        const sim_t& sim) {
        uAssert(c.frag_a_nid_lo <= c.frag_a_nid_hi);
        uAssert(dist2_check > U_TO_REAL(0.0));

        // Handle to 3-D node positions
        const uMatrix& node_positions = sample.get_node_positions();

        // Buffer for storing contiguous x,y,z candidate position
        uReal B[uDim_num];
        // Buffer for computing squared distance
        uReal dist2[uDim_num];

        // Pad proximity check by node radius
        if (!sample_t::is_homg_radius) {
            dist_check += c.node_radius;
        }

        const uUInt num_candidates =
            U_TO_UINT(out_legal_candidates_primed.size());
        for (uUInt i = 0; i < num_candidates; ++i) {
            // Skip if a prior constraint isn't satisfied
            if (!out_legal_candidates_primed.at(i)) {
                continue;
            }

            // Load candidate center
            B[uDim_X] = candidate_centers.at(i, uDim_X);
            B[uDim_Y] = candidate_centers.at(i, uDim_Y);
            B[uDim_Z] = candidate_centers.at(i, uDim_Z);

            // Check for constraint violation
            uBool b_okay =
                (sample_t::is_homg_radius)
                    ? AB_test_homg_kin_full_A(
                          c, B, dist2, dist2_check, node_positions)
                    : AB_test_hetr_kin_full_A(
                          c, B, dist2, dist_check, node_positions, sample, sim);
            if (!b_okay) {
                // Let policy have final say
                b_okay = policy.intr_chr_kin_on_fail(
                    p_policy_args, i, c, sample, sim);
            }
            out_legal_candidates_primed.at(i) = b_okay;
        }  // end of iteration over primed legal candidates
    }

    // Disallow any form of instantiation
    uSisIntrChrCommonFilt(const uSisIntrChrCommonFilt&);
    uSisIntrChrCommonFilt& operator=(const uSisIntrChrCommonFilt&);
};

#endif  // uSisIntrChrCommonFilt_h
