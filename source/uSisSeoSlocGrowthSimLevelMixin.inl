//****************************************************************************
// uSisSingleEndSingleLocusOrderedGrowthSimLevelMixin.inl
//****************************************************************************

/**
 * Sis = Sequential importance sampling
 * Seo = Single-end, ordered growth is from only one end of chain. Ordered =
 *  nodes aregrown from first to last node always
 * Sloc = Single-locus, built for only single locus chain
 */
namespace uSisSeoSloc {
/**
 * Mixin encapsulating global (sample independent) data and utilities
 * In this case, the simulation level growth mixin manages the number of
 * grown nodes that all samples (that are not dead) have achieved.
 *
 * This mixin assumes that only a single node is grown within a growth step.
 */
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class GrowthSimLevelMixin_threaded
    : public uSisSeGrowthSimLevelMixin_threaded<t_SisGlue>
#else
class GrowthSimLevelMixin_serial
    : public uSisSeGrowthSimLevelMixin_serial<t_SisGlue>
#endif  // U_INJECT_THREADED_TLS
{
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    // Accessors

    /**
     * Provide consistent interface with Mloc growth
     * @return 1-D node identifier from 2-D (lid, nof) tuple
     */
    inline uUInt get_growth_nid(const uUInt lid, const uUInt nof) const {
        uAssert(lid == U_TO_UINT(0));
        return nof;
    }

    /**
     * @return locus identifier associated with with node identifier
     */
    inline uUInt get_growth_lid(const uUInt nid) const { return U_TO_UINT(0); }

    /**
     * @return locus node offset associated with node identifier
     */
    inline uUInt get_growth_nof(const uUInt nid) const { return nid; }

    /**
     * Provide consistent interface with Mloc growth
     * Obtain 2-D (lid, nof) tuple from 1-D node identifier
     */
    inline void get_growth_lid_nof(uUInt& lid,
                                   uUInt& nof,
                                   const uUInt nid) const {
        lid = U_TO_UINT(0);
        nof = nid;
    }

    /**
     * @param num_completed_grow_steps - The number of growth
     *  calls (seed() + grow_*()) that have completed. This value is
     *  reset between trials.
     * @return number of grown nodes at each sample as of the last
     * finished grow step
     */
    inline uUInt get_num_grown_nodes(
        const uUInt num_completed_grow_steps) const {
        // This is the default implementation that assumes only 1 node is
        // grown per growth call. This is hooked here so that we can
        // change it if, for example, multiple nodes are grown per call.
        return num_completed_grow_steps;
    }

    /**
     * @param sim - the parent simulation
     * @return - the total number of seed() + grow_*() calls required to
     *  grow a single sample.
     */
    inline uUInt get_max_num_grow_steps(const sim_t& sim) const {
        return sim.get_max_total_num_nodes();
    }
};

}  // namespace uSisSeoSloc
