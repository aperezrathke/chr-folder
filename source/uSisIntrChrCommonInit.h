//****************************************************************************
// uSisIntrChrCommonInit.h
//****************************************************************************

/**
 * @brief Common (i.e. shared) utility functions for initializing
 *  chromatin-to-chromatin interaction constraints
 */

#ifndef uSisIntrChrCommonInit_h
#define uSisIntrChrCommonInit_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uExitCodes.h"
#include "uIntrLibDefs.h"
#include "uLogf.h"
#include "uSisIntrChrConfig.h"
#include "uTypes.h"

#include <algorithm>

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Effectively namespace for shared chr-chr interaction utilities
 */
class uSisIntrChrCommonInit {
public:
    /**
     * SIMULATION LEVEL UTILITY
     * Validates interaction pair to ensure no overlapping fragments and no
     * boundary violations. Also, formats interaction pair such that first
     * fragment always occurs "before" second interacting fragment under SEO
     * growth model.
     * @param frag_a - column index into frags matrix for first fragment in
     *  interaction pair
     * @param frag_b - column index into frags matrix for second fragment in
     *  interaction pair
     * @param frags - 2 x M matrix where M is number of fragments, defines
     *  the node spans constituting a chromatin fragment
     * @param sim - parent simulation
     */
    template <typename sim_t>
    static void format_intr_exit_on_fail(uUInt& frag_a,
                                         uUInt& frag_b,
                                         const uUIMatrix& frags,
                                         const sim_t& sim) {
        uAssert(sim.get_max_total_num_nodes() > 1);
        // Check if out-of-bounds or malformed
        if ((frag_a >= frags.n_cols) || (frag_b >= frags.n_cols) ||
            (frag_a == frag_b)) {
            uLogf(
                "Error: out-of-bounds or malformed interaction pair (%d, %d). "
                "Exiting.\n",
                (int)frag_a,
                (int)frag_b);
            exit(uExitCode_error);
        }

        uUInt frag_a_ix_min, frag_a_ix_max, frag_b_ix_min, frag_b_ix_max;
        get_frag_range_exit_on_fail(
            frag_a_ix_min, frag_a_ix_max, frag_a, frags, sim);
        get_frag_range_exit_on_fail(
            frag_b_ix_min, frag_b_ix_max, frag_b, frags, sim);
        uAssert(frag_a_ix_min <= frag_b_ix_max);
        uAssert(frag_b_ix_min <= frag_b_ix_max);

        // Make sure first fragment starts at lower node
        if (frag_a_ix_min > frag_b_ix_min) {
            std::swap(frag_a_ix_min, frag_b_ix_min);
            std::swap(frag_a_ix_max, frag_b_ix_max);
            std::swap(frag_a, frag_b);
        }
        uAssert(frag_a_ix_min < frag_b_ix_min);

        // Check spans are disjoint
        if (frag_b_ix_min <= frag_a_ix_max) {
            uLogf(
                "Error: interacting fragment spans must be disjoint. "
                "Exiting.\n");
            exit(uExitCode_error);
        }
    }

    /**
     * SIMULATION LEVEL UTILITY
     * Verifies and optionally pads distance and squared distance thresholds
     * @param dist - output Euclidean distance, assumed initialized by user
     * @param dist2 - output squared Euclidean distance
     * @param sim - parent simulation
     */
    template <typename sim_t>
    static void init_dists(uReal& dist, uReal& dist2, const sim_t& sim) {
        // Distance threshold must be positive real
        uAssert(dist > U_TO_REAL(0.0));
        // Pad distance thresholds if nodes all have same radius
        if (sim_t::is_homg_radius) {
            dist += sim.get_max_node_diameter();
        }
        dist2 = dist * dist;
    }

private:
    /**
     * Obtains fragment node range [min, max] and exits if format error
     */
    template <typename sim_t>
    static void get_frag_range_exit_on_fail(uUInt& nid_min,
                                            uUInt& nid_max,
                                            const uUInt frag,
                                            const uUIMatrix& frags,
                                            const sim_t& sim) {
        // Obtain fragment span
        nid_min = frags.at(uIntrLibPairIxMin, frag);
        nid_max = frags.at(uIntrLibPairIxMax, frag);
        // Verify first node <= second node in span
        if (nid_min > nid_max) {
            uLogf(
                "Error: fragment %d, first node must be <= second node !(%d <= "
                "%d). Exiting.\n",
                (int)frag,
                (int)nid_min,
                (int)nid_max);
            exit(uExitCode_error);
        }
        // Verify both nodes in span contained within same locus
        const uUInt lid_min = sim.get_growth_lid(nid_min);
        uAssertBounds(lid_min, U_TO_UINT(0), sim.get_num_loci());
        const uUInt lid_max = sim.get_growth_lid(nid_max);
        uAssertBounds(lid_max, U_TO_UINT(0), sim.get_num_loci());
        if (lid_min != lid_max) {
            uLogf(
                "Error: fragment %d spanning nodes [%d, %d] maps to multiple "
                "loci (%d, %d). Exiting.\n",
                (int)frag,
                (int)nid_min,
                (int)nid_max,
                (int)lid_min,
                (int)lid_max);
            exit(uExitCode_error);
        }
        // Check spans map to actual nodes
        const uUInt max_nodes = sim.get_max_total_num_nodes();
        if (nid_max >= max_nodes) {
            uLogf("Error: fragment %d span out of bounds. Exiting.\n",
                  (int)frag);
            exit(uExitCode_error);
        }
    }

    // Disallow any form of instantiation
    uSisIntrChrCommonInit(const uSisIntrChrCommonInit&);
    uSisIntrChrCommonInit& operator=(const uSisIntrChrCommonInit&);
};

#endif  // uSisIntrChrCommonInit_h
