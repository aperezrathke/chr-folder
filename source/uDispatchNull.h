//****************************************************************************
// uDispatchNull.h
//****************************************************************************

/**
 * Dispatches to user-generated null models
 */

#ifndef uDispatchNull_h
#define uDispatchNull_h

#include "uBuild.h"
#include "uCanvasNull.h"
#include "uCmdOptsMap.h"
#include "uLogf.h"
#include "uOpts.h"

//****************************************************************************
// Main
//****************************************************************************

/**
 * The null dispatcher! Usage:
 *
 *  --null_dispatch <cmdkey>
 */
class uDispatchNull {
public:
    /**
     * Entry point for app
     */
    static int main(const uCmdOptsMap& cmd_opts) {
        std::string cmd_key;
        cmd_opts.read_into(cmd_key, uOpt_get_cmd_switch(uOpt_null_dispatch));
        uLogf("Running null dispatcher for: %s\n", cmd_key.c_str());
        return uSisNullApps::dispatch(cmd_opts, cmd_key);
    }

private:
    // Disallow construction and assignment
    uDispatchNull();
    uDispatchNull(const uDispatchNull&);
    uDispatchNull& operator=(const uDispatchNull&);
};

#endif  // uDispatchNull_h
