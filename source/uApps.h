//****************************************************************************
// uApps.h
//****************************************************************************

#ifndef uApps_h
#define uApps_h

#include "uBuild.h"
#include "uCmdOptsMap.h"
#include "uDispatchFea.h"
#include "uDispatchMain.h"
#include "uDispatchNull.h"

/**
 * App generates single knock-in interaction constrained samples using nested
 * landmark trial runners with resampling and rejection control.
 *
 * Usage:
 * -app_sis_unif_ski_lmrk_rs_rc
 */
extern int uAppSisUnifSkiLmrkRsRcMain(const uCmdOptsMap& cmd_opts);

#endif  // uApps_h
