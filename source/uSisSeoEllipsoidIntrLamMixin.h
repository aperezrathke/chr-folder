//****************************************************************************
// uSisSeoEllipsoidIntrLamMixin.h
//****************************************************************************

/**
 * A lamina interaction mixin manages nuclear lamina association constraints.
 * Note, this is different from a "nuclear mixin" which is used for modeling
 * confinement by constraining all monomer beads to reside within the nuclear
 * volume. In contrast, the lamina interaction mixin enforces polymer
 * fragments to be proximal or distal to the nuclear periphery as configured
 * by the user.
 *
 * This lamina interaction mixin supports an ellipsoidal nucleus.
 */

#ifndef uSisSeoEllipsoidIntrLamMixin_h
#define uSisSeoEllipsoidIntrLamMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibBudgUtil.h"
#include "uSisIntrLamCommonBatch.h"
#include "uSisSeoEllipsoidIntrLamFilt.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

#include "uSisSeoEllipsoidIntrLamSimLevelMixin.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

/**
 * Mixin encapsulating utilities meant to be called from the sample level.
 * This primarily works to bridge the sample mixin to the simulation level
 * mixin buffers.
 */
template <typename t_SisGlue>
class uSisSeoEllipsoidIntrLamMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Lamina knock-in budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_lam_budg_acc_kin_t;

    /**
     * Lamina knock-out budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_lam_budg_acc_ko_t;

    /**
     * Batch interactions utility
     */
    typedef uSisIntrLamCommonBatch<glue_t> intr_lam_batch_util_t;

    /**
     * Lamina interaction knock-in filter
     */
    typedef uSisSeoEllipsoidIntrLamKinFilt<glue_t> intr_lam_filt_kin_t;

    /**
     * Lamina interaction knock-out filter
     */
    typedef uSisSeoEllipsoidIntrLamKoFilt<glue_t> intr_lam_filt_ko_t;

    /**
     * Initializes mixin
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     * @param config - User configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        m_intr_lam_loci_tables = sim.get_intr_lam_loci_tables();
    }

    /**
     * Resets to default state
     */
    void clear() { intr_lam_batch_util_t::clear(m_intr_lam_loci_tables); }

    /**
     * Checks if seed can satisfy lamina interaction constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE if seed can satisfy constraints, uFALSE o/w
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) {
        // https://stackoverflow.com/questions/1840253/template-member-function-of-template-class-called-from-template-function
        // When the name of a member template specialization appears after .
        // or -> in a postfix-expression, or after nested-name-specifier in a
        // qualified-id, and the postfix-expression or qualified-id explicitly
        // depends on a template-parameter(14.6.2), the member template name
        // must be prefixed by the keyword template. Otherwise the name is
        // assumed to name a non-template.
        return intr_lam_batch_util_t::template is_seed_okay<intr_lam_filt_kin_t,
                                                            intr_lam_filt_ko_t,
                                                            intr_lam_mixin_t,
                                                            void>(
            *this,
            (void*)NULL,
            m_intr_lam_loci_tables,
            nfo,
            sample,
            sim U_THREAD_ID_ARG);
    }

    /**
     * Determines which positions satisfy lamina constraints
     * @param out_legal_candidates_primed - Output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for constraint violations.
     * @param candidate_radius - Radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - Node identifier from which candidate positions
     *  were generated
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_lam_filter(uBoolVecCol& out_legal_candidates_primed,
                         const uMatrix& candidate_centers,
                         const uReal candidate_radius,
                         const uUInt parent_node_id,
                         const sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        intr_lam_batch_util_t::template mark_viol<intr_lam_filt_kin_t,
                                                  intr_lam_filt_ko_t,
                                                  intr_lam_mixin_t,
                                                  void>(
            *this,
            (void*)NULL,
            out_legal_candidates_primed,
            m_intr_lam_loci_tables,
            candidate_centers,
            candidate_radius,
            parent_node_id,
            sample,
            sim U_THREAD_ID_ARG);
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-in,
     *  lamina interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed lamina interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_lam_kin_on_fail(void* const p_policy_args,
                                      const uUInt candidate_id,
                                      const uSisIntrLamFilterCore_t& c,
                                      const sample_t& sample,
                                      const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Callback policy for when a candidate will never satisfy a knock-out,
     *  lamina interaction
     * @param p_policy_args - Pointer to additional policy arguments
     * @param candidate_id - Candidate which failed lamina interaction
     * @param c - Filter core arguments
     * @param sample - Parent sample
     * @param sim - Outer simulation containing global sample data
     * @return uFALSE if candidate should be culled, uTRUE o/w
     *  (equivalently: uTRUE if candidate should be retained, uFALSE o/w)
     */
    inline uBool intr_lam_ko_on_fail(void* const p_policy_args,
                                     const uUInt candidate_id,
                                     const uSisIntrLamFilterCore_t& c,
                                     const sample_t& sample,
                                     const sim_t& sim) const {
        return uFALSE;
    }

    /**
     * Updates sample mixin states with new node information
     * @param node_id - Unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - Radius of the node to add
     * @param candidate_id - Identifies candidate that was selected
     * @param sample - Sample containing this mixin
     * @param sim - Parent simulation of parameter sample
     */
    void intr_lam_update(const uUInt node_id,
                         const uReal* point,
                         const uReal radius,
                         const uUInt candidate_id,
                         sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        intr_lam_batch_util_t::template remove_satisf<intr_lam_filt_kin_t,
                                                      intr_lam_filt_ko_t>(
            m_intr_lam_loci_tables,
            node_id,
            point,
            radius,
            candidate_id,
            sample,
            sim U_THREAD_ID_ARG);
    }

private:
    /**
     * Collection of active lamina interactions partitioned by loci
     */
    uSisIntrLamLociTables_t m_intr_lam_loci_tables;
};

#endif  // uSisSeoEllipsoidIntrLamMixin_h
