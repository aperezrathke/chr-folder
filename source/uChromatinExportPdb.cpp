//****************************************************************************
// uChromatinExportPdb.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportPdb.h"
#include "uAssert.h"
#include "uFilesystem.h"
#include "uIntrLibDefs.h"
#include "uLogf.h"

#include <fstream>
#include <iomanip>
#include <sstream>

//****************************************************************************
// Utilities (private)
//****************************************************************************

namespace {

/**
 * @HACK - NON-STANDARD PDB FORMAT!
 * https://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM
 * PDB standard wants X, Y, Z coordinates in 8.3 fixed point format. However,
 * for large nucleus (and large polymer) simulations, this may not provide
 * enough characters to correctly express the 'atom' coordinates. To help
 * relieve this, we can reduce the precision as needed from 3 to 0. Changing
 * the precision (i.e. digits after the decimal) to anything other than 3 is
 * NON-STANDARD with the PDB format; however, PyMOL appears to have no problem
 * parsing and rendering these less precise coordinates. This is NOT fool-
 * proof and for large enough coordinates, the coordinate WILL overflow
 * into the adjacent coordinate's columns! In practice, this appears to be
 * working okay. If PDB rendering of large simulations is still an issue,
 * another possible trick to help alleviate the fixed column limitation is to
 * transform beads to the origin and also orient beads to be in the +x, +y, +z
 * octant as much as possible.
 * @return Maximum precision in 0 to 3
 */
inline int get_precisionXYZ(const uReal x) {
    std::stringstream ss;
    ss << std::setiosflags(std::ios::fixed) << std::setprecision(3)
       << std::setw(8) << x;
    const int sz = static_cast<int>(ss.str().size());
    return (sz <= 8) ? 3 : std::max(3 - (sz - 8), 0);
}

/**
 * @return uTRUE if nuclear body identifier found within interaction matrix,
 *  uFalse o/w
 */
inline uBool is_intr_nucb_used(const uUInt body_id, const uUIMatrix& intrs) {
    uAssert(intrs.n_cols < U_TO_MAT_SZ_T(1) ||
            (intrs.n_rows == uIntrNucbPairIxNum));
    for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
        const uUInt bid = intrs.at(uIntrNucbPairIxBody, i);
        if (body_id == bid) {
            // Nuclear body reference found
            return uTRUE;
        }
    }
    // Nuclear body reference not found
    return uFALSE;
}

}  // namespace

//****************************************************************************
// Utilities (public)
//****************************************************************************

/**
 * Utility to generate PDB formatted text string
 */
void chromatin_export_get_pdb_text(std::string& out_pdb_text,
                                   const uChromatinExportArgs_t& args) {
    // Node positions are ideally in same order as alpha carbons, else may
    // not be representable using PDB fixed column widths
    const uMatrix scaled_node_positions = (*args.p_node_positions) * args.scale;

    std::stringstream sout;
    std::stringstream buffer;

    // Write sample log weight and scale as remarks
    // First remark line is blank
    // http://www.wwpdb.org/documentation/file-format-content/format33/remarks.html
    sout << "REMARK"
         << " " << std::setw(10 - 8 + 1) << "6"
         << " " << std::setw(80 - 12 + 1) << "" << std::endl;
    // Remark for log weight
    sout << "REMARK"
         << " " << std::setw(10 - 8 + 1) << "6"
         << " ";
    buffer << "LOG WEIGHT: " << args.log_weight;
    sout.setf(std::ios::left);
    sout << std::setw(80 - 12 + 1) << buffer.str() << std::endl;
    sout.unsetf(std::ios::left);
    // Remark for scale
    sout << "REMARK"
         << " " << std::setw(10 - 8 + 1) << "6"
         << " ";
    buffer.str(std::string());
    buffer << "SCALE: " << args.scale;
    sout.setf(std::ios::left);
    sout << std::setw(80 - 12 + 1) << buffer.str() << std::endl;
    sout.unsetf(std::ios::left);

    // Write ATOM records as ALA CA atoms
    for (uUInt i_col = 0; i_col < args.p_node_positions->n_cols; ++i_col) {
        // 1 - 6   Record name
        sout << "ATOM  ";
        // 7 - 11  Atom serial number (right justified)
        sout << std::setw(11 - 7 + 1) << (i_col + 1);
        // 12      Whitespace
        sout << " ";
        // 13 - 16 Atom name (left justified)
        // From
        // https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/tutorials/pdbintro.html#note1
        // Atom names start with element symbols right-justified in columns
        // 13-14 as permitted by the length of the name. For example, the
        // symbol FE for iron appears in columns 13-14, whereas the symbol C
        // for carbon appears in column 14. If an atom name has four
        // characters, however, it must start in column 13 even if the element
        // symbol is a single character (for example, see Hydrogen Atoms)
        sout.setf(std::ios::left);
        sout << std::setw(16 - 13 + 1) << "CA";
        sout.unsetf(std::ios::left);
        // 17      Alternate location indicator
        sout << " ";
        // 18 - 20 Residue name (right justified)
        sout << std::setw(20 - 18 + 1) << "ALA";
        // 21      Whitespace
        sout << " ";
        // 22      Chain identifier
        sout << " ";
        // BEGIN HACK - NON-STANDARD PDB FORMAT
        // If we have 10,000 or more monomers, we will overflow the PDB
        // standard column width for resNo. However, PyMOL will render this
        // non-standard format just fine so long as the X, Y, Z coordinates
        // are in the proper columns!
        if (i_col < U_TO_UINT(9999)) {
            // 23 - 26 Residue sequence number (right justified)
            sout << std::setw(26 - 23 + 1) << (i_col + 1);
            // 27      Code for insertion residues
            sout << " ";
        } else {
            // @WARNING - NON-STANDARD PDB FORMAT!
            // 23 - 27 Residue sequence number (right justified)
            sout << std::setw(27 - 23 + 1) << (i_col + 1);
        }
        // END HACK - NON-STANDARD PDB FORMAT
        // 28 - 30 Whitespace
        sout << std::setw(30 - 28 + 1) << "";
        // 31 - 38 X coordinates in Angstroms (8.3 right justified)
        const uReal x = scaled_node_positions.at(uDim_X, i_col);
        sout << std::setiosflags(std::ios::fixed)
             << std::setprecision(get_precisionXYZ(x));
        sout << std::setw(8) << x;
        // 39 - 46 Y coordinates in Angstroms (8.3 right justified)
        const uReal y = scaled_node_positions.at(uDim_Y, i_col);
        sout << std::setprecision(get_precisionXYZ(y)) << std::setw(8) << y;
        // 47 - 54 Z coordinates in Angstroms (8.3 right justified)
        const uReal z = scaled_node_positions.at(uDim_Z, i_col);
        sout << std::setprecision(get_precisionXYZ(z)) << std::setw(8) << z;
        // 55 - 60 Occupancy (6.2 right justified)
        sout << std::setiosflags(std::ios::fixed) << std::setprecision(2);
        sout << std::setw(6) << 1.0;
        // 61 - 66 Temperature factor (6.2 right justified)
        sout << std::setw(6) << 1.0;
        // 67 - 76 Whitespace
        sout << std::setw(76 - 67 + 1) << "";
        // 77 - 78 Element symbol (right justified)
        sout << std::setw(78 - 77 + 1) << "";
        // 79 - 80 Charge on the atom
        sout << std::setw(80 - 79 + 1) << "";
        sout << std::endl;
    }

    // Write CONECT records
    uUInt i_node = 1;
    for (uUInt i_locus = 0; i_locus < args.p_max_num_nodes_at_locus->size();
         ++i_locus) {
        const uUInt n_nodes_at_locus =
            (*args.p_max_num_nodes_at_locus)[i_locus];
        for (uUInt i_sub_node = 1; i_sub_node <= n_nodes_at_locus;
             ++i_sub_node) {
            if (i_sub_node != n_nodes_at_locus) {
                // 1 - 6   Record name
                sout << "CONECT";
                // 7 - 11  Atom serial number (right justified)
                sout << std::setw(11 - 7 + 1) << i_node;
                // 12 - 16 Serial number of bonded atom (right justified)
                sout << std::setw(16 - 12 + 1) << (i_node + 1);
                // 17 - 80 Whitespace
                sout << std::setw(80 - 17 + 1) << "";
                sout << std::endl;
            }
            ++i_node;
        }
    }
    uAssert((i_node - 1) == args.p_node_positions->n_cols);

    out_pdb_text = sout.str();
}

/**
 * Utility appends nuclear body HETATM records to PDB formatted text string
 * @return unsigned integer resNo of last HETATM record generated, will return
 *  argument 'last_resno' if no records generated
 */
uUInt chromatin_export_append_intr_nucb_pdb_text(
    std::string& out_pdb_text,
    const uUInt last_resno,
    const uChromatinExportArgs_t& args) {
    // Early out if not exporting nuclear bodies
    if (!args.intr_nucb) {
        return last_resno;
    }
    // Early out if invalid pointer
    if (!args.p_intr_nucb_centers) {
        return last_resno;
    }
    // Early out if no body centers
    if (args.p_intr_nucb_centers->n_cols < U_TO_MAT_SZ_T(1)) {
        return last_resno;
    }
    // Assume column-major
    uAssertPosEq(args.p_intr_nucb_centers->n_rows, uDim_num);

    // Scale nuclear bodies to hopefully be representable in 8.3 format
    const uMatrix scaled_intr_nucb_centers =
        (*args.p_intr_nucb_centers) * args.scale;

    // Output string stream
    std::stringstream sout;

    // Export referenced nuclear body centers as HETATM records
    uAssert(args.p_node_positions);
    uUInt i_id = last_resno;
    for (uUInt i = 0; i < args.p_intr_nucb_centers->n_cols; ++i) {
        // See if body is referenced by an interaction
        const uBool is_used = is_intr_nucb_used(i, *(args.p_intr_nucb_kin)) ||
                              is_intr_nucb_used(i, *(args.p_intr_nucb_ko));
        if (!is_used) {
            // Skip body, not used by interactions!
            continue;
        }
        // Determine offset relative to any previous ATOM or HETATM records
        i_id = last_resno + i + 1;
        // 1 - 6   Record name
        sout << "HETATM";
        // 7 - 11  Atom serial number (right justified)
        sout << std::setw(11 - 7 + 1) << i_id;
        // 12      Whitespace
        sout << " ";
        // 13 - 16 Atom name (left justified)
        // From
        // https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/tutorials/pdbintro.html#note1
        // Atom names start with element symbols right-justified in columns
        // 13-14 as permitted by the length of the name. For example, the
        // symbol FE for iron appears in columns 13-14, whereas the symbol C
        // for carbon appears in column 14. If an atom name has four
        // characters, however, it must start in column 13 even if the element
        // symbol is a single character (for example, see Hydrogen Atoms)
        sout.setf(std::ios::left);
        sout << std::setw(16 - 13 + 1) << "N";
        sout.unsetf(std::ios::left);
        // 17      Alternate location indicator
        sout << " ";
        // 18 - 20 Residue name (right justified)
        sout << std::setw(20 - 18 + 1) << "BOD";
        // 21      Whitespace
        sout << " ";
        // 22      Chain identifier
        sout << " ";
        // BEGIN HACK - NON-STANDARD PDB FORMAT
        // If we have 10,000 or more monomers, we will overflow the PDB
        // standard column width for resNo. However, PyMOL will render this
        // non-standard format just fine so long as the X, Y, Z coordinates
        // are in the proper columns!
        if (i_id <= U_TO_UINT(9999)) {
            // 23 - 26 Residue sequence number (right justified)
            sout << std::setw(26 - 23 + 1) << i_id;
            // 27      Code for insertion residues
            sout << " ";
        } else {
            // @WARNING - NON-STANDARD PDB FORMAT!
            // 23 - 27 Residue sequence number (right justified)
            sout << std::setw(27 - 23 + 1) << i_id;
        }
        // END HACK - NON-STANDARD PDB FORMAT
        // 28 - 30 Whitespace
        sout << std::setw(30 - 28 + 1) << "";
        // 31 - 38 X coordinates in Angstroms (8.3 right justified)
        const uReal x = scaled_intr_nucb_centers.at(uDim_X, i);
        sout << std::setiosflags(std::ios::fixed)
             << std::setprecision(get_precisionXYZ(x));
        sout << std::setw(8) << x;
        // 39 - 46 Y coordinates in Angstroms (8.3 right justified)
        const uReal y = scaled_intr_nucb_centers.at(uDim_Y, i);
        sout << std::setprecision(get_precisionXYZ(y)) << std::setw(8) << y;
        // 47 - 54 Z coordinates in Angstroms (8.3 right justified)
        const uReal z = scaled_intr_nucb_centers.at(uDim_Z, i);
        sout << std::setprecision(get_precisionXYZ(z)) << std::setw(8) << z;
        // 55 - 80 whitespace padding (non-standard)
        sout << std::setw(80 - 55 + 1) << "";
        sout << std::endl;
    }

    // Append records
    out_pdb_text += sout.str();
    // Return last valid identifier
    return i_id;
}

/**
 * Utility appends nuclear shell HETATM record to PDB formatted text string
 * @TODO - support ellipsoid nucleus with radius in X, Y, Z dims
 * @return unsigned integer resNo of last HETATM record generated, will return
 *  argument 'last_resno' if no records generated
 */
uUInt chromatin_export_append_intr_nucshell_pdb_text(
    std::string& out_pdb_text,
    const uUInt last_resno,
    const uChromatinExportArgs_t& args) {
    // Early out if not exporting nuclear bodies
    if (!args.nuclear_shell) {
        return last_resno;
    }
    // Early out if invalid nuclear radius
    if (args.nuclear_radius <= U_TO_REAL(0.0)) {
        return last_resno;
    }

    // Scale nuclear radius to hopefully be representable in 8.3 format
    const uReal scaled_nuclear_radius = args.nuclear_radius * args.scale;

    // Output string stream
    std::stringstream sout;

    // Determine offset relative to any previous ATOM or HETATM records
    const uUInt i_id = last_resno + 1;
    // 1 - 6   Record name
    sout << "HETATM";
    // 7 - 11  Atom serial number (right justified)
    sout << std::setw(11 - 7 + 1) << i_id;
    // 12      Whitespace
    sout << " ";
    // 13 - 16 Atom name (left justified)
    // From
    // https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/tutorials/pdbintro.html#note1
    // Atom names start with element symbols right-justified in columns
    // 13-14 as permitted by the length of the name. For example, the
    // symbol FE for iron appears in columns 13-14, whereas the symbol C
    // for carbon appears in column 14. If an atom name has four
    // characters, however, it must start in column 13 even if the element
    // symbol is a single character (for example, see Hydrogen Atoms)
    sout.setf(std::ios::left);
    sout << std::setw(16 - 13 + 1) << "N";
    sout.unsetf(std::ios::left);
    // 17      Alternate location indicator
    sout << " ";
    // 18 - 20 Residue name (right justified)
    sout << std::setw(20 - 18 + 1) << "NUC";
    // 21      Whitespace
    sout << " ";
    // 22      Chain identifier
    sout << " ";
    // BEGIN HACK - NON-STANDARD PDB FORMAT
    // If we have 10,000 or more monomers, we will overflow the PDB
    // standard column width for resNo. However, PyMOL will render this
    // non-standard format just fine so long as the X, Y, Z coordinates
    // are in the proper columns!
    if (i_id <= U_TO_UINT(9999)) {
        // 23 - 26 Residue sequence number (right justified)
        sout << std::setw(26 - 23 + 1) << i_id;
        // 27      Code for insertion residues
        sout << " ";
    } else {
        // @WARNING - NON-STANDARD PDB FORMAT!
        // 23 - 27 Residue sequence number (right justified)
        sout << std::setw(27 - 23 + 1) << i_id;
    }
    // END HACK - NON-STANDARD PDB FORMAT
    // 28 - 30 Whitespace
    sout << std::setw(30 - 28 + 1) << "";
    // 31 - 38 X coordinates in Angstroms (8.3 right justified)
    sout << std::setiosflags(std::ios::fixed)
         << std::setprecision(get_precisionXYZ(U_TO_REAL(0.0)));
    sout << std::setw(8) << U_TO_REAL(0.0);
    // 39 - 46 Y coordinates in Angstroms (8.3 right justified)
    sout << std::setprecision(get_precisionXYZ(U_TO_REAL(0.0))) << std::setw(8)
         << U_TO_REAL(0.0);
    // 47 - 54 Z coordinates in Angstroms (8.3 right justified)
    sout << std::setprecision(get_precisionXYZ(U_TO_REAL(0.0))) << std::setw(8)
         << U_TO_REAL(0.0);
    // 55 - 80 whitespace padding (non-standard)
    sout << std::setw(80 - 55 + 1) << "";
    sout << std::endl;

    // Append records
    out_pdb_text += sout.str();
    // Return last valid identifier
    return i_id;
}

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Writes sample node positions to PDB file
 *
 * @param fpath - path to write the exported sample
 * @param args - exporter arguments, note 'scale' is applied to all node
 *  positions, this helps conform to the PDB format as otherwise the
 *  coordinates may overflow alloted character column boundaries.
 */
void uChromatinExportPdb::write_chromatin(const std::string& fpath,
                                          const uChromatinExportArgs_t& args) {
    if (!chromatin_export_check(args)) {
        std::cout << "Error, unable to export sample.\n";
        return;
    }

    uFs_create_parent_dirs(fpath);
    std::ofstream fout(fpath.c_str(), std::ofstream::out);

    if (!fout) {
        std::cout << "Warning, unable to open: " << fpath << std::endl
                  << "\tfor writing to PDB format.\n";
        return;
    }

    write_chromatin(fout, args);
    fout.close();
}

/**
 * Write sample node positions to 'out' stream
 * @WARNING - DOES NOT CLOSE STREAM!
 * @param out - output stream
 * @param args - exporter args
 */
void uChromatinExportPdb::write_chromatin(std::ostream& out,
                                          const uChromatinExportArgs_t& args) {
    std::string pdb_text;
    chromatin_export_get_pdb_text(pdb_text, args);
    out << pdb_text;
}

/**
 * Warn user of potential formatting issues based on polymer size
 * @param max_total_num_nodes - polymer size
 */
void uChromatinExportPdb::warn_format(const uUInt max_total_num_nodes) {
    if (max_total_num_nodes >= U_TO_UINT(10000)) {
        uLogf(
            "WARNING: NON-STANDARD PDB EXPORT DETECTED FOR POLYMER OF SIZE >= "
            "10000!\n\tPDB MAY NOT RENDER PROPERLY IN VIEWER.\n");
    }
}
