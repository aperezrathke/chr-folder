//****************************************************************************
// uChromatinExportCsv.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportCsv.h"
#include "uAssert.h"
#include "uFilesystem.h"
#include "uStringUtils.h"

#include <fstream>
#include <iomanip>

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility to help make sure sample has reasonable format
 */
uBool chromatin_export_check(const uChromatinExportArgs_t& args) {
    uBool result = uTRUE;
    if ((!args.p_node_positions) || (args.p_node_positions->n_rows != 3)) {
        std::cout << "Warning, malformed output matrix, expected 3 rows.\n";
        result &= uFALSE;
    }

    if ((!args.p_node_positions) || (args.p_node_positions->n_cols < 1)) {
        std::cout << "Warning, empty output matrix.\n";
        result &= uFALSE;
    }

    if ((!args.p_node_positions) || (!args.p_node_radii) ||
        (args.p_node_positions->n_cols != args.p_node_radii->n_elem)) {
        std::cout << "Warning, malformed node radii\n";
        result &= uFALSE;
    }

    if ((!args.p_max_num_nodes_at_locus) ||
        (args.p_max_num_nodes_at_locus->empty())) {
        std::cout << "Warning, empty locus array.\n";
        result &= uFALSE;
    }

    if (args.nuclear_radius <= U_TO_REAL(0.0)) {
        std::cout << "Warning, malformed nuclear radius.\n";
        result &= uFALSE;
    }

    return result;
}

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Writes sample node positions to CSV file in format:
 *
 *  # LOG WEIGHT: <log_weight>
 *  <x1>,<y1>,<z1>
 *  ...
 *  <xN>,<yN>,<zN>
 *
 * If extended format:
 *
 *  # LOG WEIGHT: <log_weight>
 *  # NUCLEAR RADIUS: <nuclear_radius>
 *  <x1>,<y1>,<z1>,<radius1>,<chain1>
 *  ...
 *  <xN>,<yN>,<zN>,<radius1>,<chain1>
 *
 * @param fpath - path to write the exported sample
 * @param args - exporter arguments, note scale and interaction arguments
 *  are ignored
 */
void uChromatinExportCsv::write_chromatin(const std::string& fpath,
                                          const uChromatinExportArgs_t& args) {
    if (!chromatin_export_check(args)) {
        std::cout << "Error, unable to export sample.\n";
        return;
    }

    // Create output directory if not existing
    uFs_create_parent_dirs(fpath);
    std::ofstream fout(fpath.c_str(), std::ofstream::out);

    if (!fout) {
        std::cout << "Warning, unable to open: " << fpath << std::endl
                  << "\tfor writing to CSV format.\n";
        return;
    }

    write_chromatin(fout, args);
    fout.close();
}

/**
 * Write sample node positions to 'out' stream
 * @WARNING - DOES NOT CLOSE STREAM!
 * @param out - output stream
 * @param args - exporter args
 */
void uChromatinExportCsv::write_chromatin(std::ostream& out,
                                          const uChromatinExportArgs_t& args) {
    // Check if higher precision requested
    const std::streamsize init_precision = out.precision();
    if (args.precision > static_cast<int>(init_precision)) {
        out.precision(static_cast<std::streamsize>(args.precision));
    }

    // Use '#' as comment char to export log weight
    out << uChromatinExportCsv::to_log_weight_comment(args.log_weight)
        << std::endl;

    // Use comment to export nuclear radius
    if (args.extended) {
        out << U_CHROMATIN_EXPORT_CSV_COMMENT_PREFIX " NUCLEAR RADIUS: "
            << args.nuclear_radius << std::endl;
    }

    // Write each atom position
    uUInt i_node = 0;
    for (uUInt i_locus = 0; i_locus < args.p_max_num_nodes_at_locus->size();
         ++i_locus) {
        const uUInt n_nodes_at_locus =
            (*args.p_max_num_nodes_at_locus)[i_locus];
        for (uUInt i_sub_node = 0; i_sub_node < n_nodes_at_locus;
             ++i_sub_node) {
            const uReal x = args.p_node_positions->at(uDim_X, i_node);
            const uReal y = args.p_node_positions->at(uDim_Y, i_node);
            const uReal z = args.p_node_positions->at(uDim_Z, i_node);

            out << x << "," << y << "," << z;
            // Output node radius and locus identifier
            if (args.extended) {
                out << "," << args.p_node_radii->at(i_node) << "," << i_locus;
            }
            out << std::endl;

            ++i_node;
        }
    }
    uAssert(i_node == args.p_node_positions->n_cols);

    // Reset precision
    out.precision(init_precision);
}

/**
 * @return log weight as CSV comment string
 */
std::string uChromatinExportCsv::to_log_weight_comment(const uReal log_weight) {
    return std::string(U_CHROMATIN_EXPORT_CSV_LOG_WEIGHT_PREFIX " ") +
           u2Str(log_weight);
}
