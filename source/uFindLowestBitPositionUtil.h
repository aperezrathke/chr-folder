//****************************************************************************
// uFindLowestBitPositionUtil.h
//****************************************************************************

#ifndef uFindLowestBitPositionUtil_h
#define uFindLowestBitPositionUtil_h

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrin.h"

#ifdef U_BUILD_COMPILER_MSVC
#   pragma intrinsic(_BitScanForward)
#elif (defined(U_BUILD_COMPILER_ICC) || defined(U_BUILD_COMPILER_GCC))
#   include <stdint.h>
#endif  // U_BUILD_COMPILER_MSVC

// From suggestions found here:
// http://stackoverflow.com/questions/757059/position-of-least-significant-bit-that-is-set

namespace uBitsetUtils {
#ifdef U_BUILD_COMPILER_MSVC

    // Visual studio
    // Use bit scan forward
    // http://msdn.microsoft.com/en-us/library/wfd9z0bb.aspx

    #ifdef U_BUILD_ENABLE_64BIT_BLOCK
        // 64-bit version of types expected by compiler intrinsics
        /**
         * Specifies size of 'block' or 'mask' to search
         */
        typedef unsigned __int64 mask_type;
        /**
         * Specifies the base type of the pointer used for returning index of found bit
         */
        typedef unsigned long index_type;
        /**
         * Position of lowest bit is returned in index if found
         * @return 0 if Mask is 0, nonzero otherwise
         */
        inline unsigned char find_lowest_bit_position(index_type* Index,
                                                      mask_type Mask) {
            return _BitScanForward64(Index, Mask);
        }
    #else
        // 32-bit version
        typedef unsigned long mask_type;
        typedef unsigned long index_type;
        inline unsigned char find_lowest_bit_position(index_type* Index,
                                                      mask_type Mask) {
            return _BitScanForward(Index, Mask);
        }
    #endif  // U_BUILD_ENABLE_64BIT_BLOCK

#elif defined(U_BUILD_COMPILER_ICC)

    #ifdef U_BUILD_ENABLE_64BIT_BLOCK
    typedef uint64_t mask_type;
    typedef uint32_t index_type;
    inline unsigned char find_lowest_bit_position(index_type* Index,
        mask_type Mask) {
        return _BitScanForward64(Index, Mask);
    }
    #else
        typedef uint32_t mask_type;
        typedef uint32_t index_type;
        inline unsigned char find_lowest_bit_position(index_type* Index,
                                                      mask_type Mask) {
            return _BitScanForward(Index, Mask);
        }
    #endif  // U_BUILD_ENABLE_64BIT_BLOCK

#elif defined(U_BUILD_COMPILER_GCC)

    // GCC
    #ifdef U_BUILD_ENABLE_64BIT_BLOCK
        typedef uint64_t mask_type;
        typedef uint32_t index_type;
        /**
         * Position of lowest bit is returned in index if found
         * http://gcc.gnu.org/onlinedocs/gcc-4.4.5/gcc/Other-Builtins.html
         * Other alternative is __builtin_ffs intrinsic
         * @return 0 if Mask is 0, nonzero otherwise
         */
        inline unsigned char find_lowest_bit_position(index_type* Index,
                                                      mask_type Mask) {
            uAssert(sizeof(Mask) == 8);
            // return count of trailing zeros for unsigned long long type
            // note: if want to use 'unsigned int', then use __builtin_ctz(Mask) instead
            *Index = __builtin_ctzll(Mask);
            return 0 != Mask;
        }
    #else
    typedef uint32_t mask_type;
    typedef uint32_t index_type;
    inline unsigned char find_lowest_bit_position(index_type* Index,
        mask_type Mask) {
        *Index = __builtin_ctzl(Mask);
        return 0 != Mask;
    }
    #endif  // U_BUILD_ENABLE_64BIT_BLOCK

#else

#   error Unrecognized platform

#endif  // platform

}  // namespace uBitsetUtils

#endif  // uFindLowestBitPositionUtil_h
