//****************************************************************************
// uTestZstr.h
//****************************************************************************

/**
 * Commandlet for testing zstr library
 *
 * Usage:
 * -test_zstr [--output_dir <path>]
 *
 * Or if defaults okay (assume running under windows MSCV), then simply:
 * -test_zstr
 */

#ifdef uTestZstr_h
#   error "Test Zstr included multiple times!"
#endif  // uTestZstr_h
#define uTestZstr_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uAssert.h"
#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uFilesystem.h"
#include "uRand.h"
#include "uTypes.h"

#include <iostream>
#include <limits>
#include <stdint.h>
#include <string>
#include <zstr.hpp>

/**
 * Utility namespace
 */
namespace uTestZstr {

/**
 * Plain Old Data (POD) structure for serialization test
 */
typedef struct {
    // 1 unsigned byte
    uint8_t ub1;
    // 2 unsigned bytes
    uint16_t ub2;
    // 4 unsigned bytes
    uint32_t ub4;
    // 8 unsigned bytes
    uint64_t ub8;
    // 4 signed bytes
    int32_t b4;
    // 8 signed bytes
    int64_t b8;
    // 8 floating point
    double r8;
} POD_t;

/**
 * @return Default output directory
 */
std::string get_default_output_dir() {
    return std::string("../output/test_zstr");
}

/**
 * @return Target data path for reading|writing
 */
std::string get_data_path(const std::string& output_dir,
                          const std::string& fname) {
    const uFs::path outd(output_dir);
    const uFs::path f(fname);
    return (outd / f).string();
}

/**
 * Write POD data to stream
 * @return TRUE if stream okay, FALSE o/w
 */
bool write_pod(const POD_t& pod, std::ostream& os) {
    const char* buff = (const char*)&pod;
    const size_t nbytes = sizeof(POD_t);
    os.write(buff, nbytes);
    return !os.bad();
}

/**
 * Write POD data to uncompressed stream
 * @return TRUE if stream okay, FALSE o/w
 */
bool write_pod_std(const POD_t& pod, const std::string& fpath) {
    std::ofstream os(fpath);
    return write_pod(pod, os);
}

/**
 * Write POD data to compressed stream
 * @return TRUE if stream okay, FALSE o/w
 */
bool write_pod_zstr(const POD_t& pod, const std::string& fpath) {
    zstr::ofstream os(fpath);
    return write_pod(pod, os);
}

/**
 * Read POD from stream
 * @return TRUE if stream okay, FALSE o/w
 */
bool read_pod(POD_t& pod, std::istream& is) {
    uAssert(!is.eof());
    char* buff = (char*)&pod;
    const size_t nbytes = sizeof(POD_t);
    is.read(buff, nbytes);
    return !is.bad();
}

/**
 * Read POD data from uncompressed stream
 */
bool read_pod_std(POD_t& pod, const std::string& fpath) {
    std::ifstream is(fpath);
    return read_pod(pod, is);
}

/**
 * Read POD data from compressed stream
 */
bool read_pod_zstr(POD_t& pod, const std::string& fpath) {
    zstr::ifstream is(fpath);
    return read_pod(pod, is);
}

/**
 * Prints POD data members to stdout
 * @param pod - POD to print
 * @param id - identifier string of POD
 */
void print_pod(const POD_t& pod, const std::string& id) {
    std::cout << "---- POD: " << id << " ----\n";
    std::cout << "\t.ub1: " << pod.ub1 << std::endl;
    std::cout << "\t.ub2: " << pod.ub2 << std::endl;
    std::cout << "\t.ub4: " << pod.ub4 << std::endl;
    std::cout << "\t.ub8: " << pod.ub8 << std::endl;
    std::cout << "\t.b4: " << pod.b4 << std::endl;
    std::cout << "\t.b8: " << pod.b8 << std::endl;
    std::cout << "\t.r8: " << pod.r8 << std::endl;
    std::cout << "\tsize bytes: " << sizeof(POD_t) << std::endl;
}

/**
 * Tests POD argument can be [de]serialized okay
 * @param pod - POD to [de]serialize
 * @param output_dir - output directory to store transient data
 * @param fid - Filename prefix (no extension or path)
 * @return TRUE if read file matches parameter pod
 */
bool test_pod_match(const POD_t& pod,
                    const std::string output_dir,
                    const std::string& fid) {
    const std::string fname_zstr = fid + std::string(".zstr.bin");
    const std::string fname_std = fid + std::string(".std.bin");
    const std::string fpath_zstr(get_data_path(output_dir, fname_zstr));
    const std::string fpath_std(get_data_path(output_dir, fname_std));
    uFs_create_parent_dirs(fpath_std);
    uFs_create_parent_dirs(fpath_zstr);
    print_pod(pod, "actual");
    // Save POD to disk
    uVerify(write_pod_zstr(pod, fpath_zstr));
    uVerify(write_pod_std(pod, fpath_std));
    // Initialize new POD for loading
    POD_t pod2;
    // Set with different initial state to make test fail if not loaded okay
    memset(((void*)(&pod2)), 0, sizeof(POD_t));
    pod2.ub1 = !pod.ub1;
    pod2.ub2 = !pod.ub2;
    pod2.ub4 = !pod.ub4;
    pod2.ub8 = !pod.ub8;
    pod2.b4 = !pod.b4;
    pod2.b8 = !pod.b8;
    pod2.r8 = -pod.r8 + std::numeric_limits<double>::epsilon();
    // Read POD from disk
    uVerify(read_pod_zstr(pod2, fpath_zstr));
    print_pod(pod2, "loaded");
    const bool result =
        (0 == memcmp(((void*)(&pod)), ((void*)(&pod2)), sizeof(POD_t)));
    return result;
}

/**
 * Test reading and writing a POD with all members zero
 */
bool test_pod_zero(const std::string& output_dir) {
    std::cout << "TEST: POD ZERO" << std::endl;
    // Output file name
    const std::string fid("pod.zero");
    // Initialize POD with all members zero
    POD_t pod;
    memset(((void*)(&pod)), 0, sizeof(POD_t));
    // Test serialization
    return test_pod_match(pod, output_dir, fid);
}

/**
 * Test reading and writing a random POD
 */
bool test_pod_rand(const std::string& output_dir) {
    std::cout << "TEST: POD RAND" << std::endl;
    // Output file name
    const std::string fid("pod.rand");
    // Initialize random POD
    uRand rng;
    rng.reseed();
    POD_t pod;
    memset(((void*)(&pod)), 0, sizeof(POD_t));
    pod.ub1 = static_cast<uint8_t>(
        rng.unif_int<uint32_t>(std::numeric_limits<uint8_t>::min(),
                               std::numeric_limits<uint8_t>::max()));
    pod.ub2 = rng.unif_int<uint16_t>(std::numeric_limits<uint16_t>::min(),
                                     std::numeric_limits<uint16_t>::max());
    pod.ub4 = rng.unif_int<uint32_t>(std::numeric_limits<uint32_t>::min(),
                                     std::numeric_limits<uint32_t>::max());
    pod.ub8 = rng.unif_int<uint64_t>(std::numeric_limits<uint64_t>::min(),
                                     std::numeric_limits<uint64_t>::max());
    pod.b4 = rng.unif_int<int32_t>(std::numeric_limits<int32_t>::min(),
                                   std::numeric_limits<int32_t>::max());
    pod.b8 = rng.unif_int<int64_t>(std::numeric_limits<int64_t>::min(),
                                   std::numeric_limits<int64_t>::max());
    pod.r8 = static_cast<double>(rng.unif_real());
    // Test serialization
    return test_pod_match(pod, output_dir, fid);
}

}  // end namespace uTestZstr

/**
 * Entry point for test
 */
int uTestZstrMain(const uCmdOptsMap& cmd_opts) {
    std::string output_dir(uTestZstr::get_default_output_dir());
    cmd_opts.read_into(output_dir, uOpt_get_cmd_switch(uOpt_output_dir));
    uVerify(uTestZstr::test_pod_zero(output_dir));
    uVerify(uTestZstr::test_pod_rand(output_dir));
    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
