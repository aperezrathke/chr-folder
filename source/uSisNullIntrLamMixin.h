//****************************************************************************
// uSisNullIntrLamMixin.h
//****************************************************************************

/**
 * Defines a null (non-functional) lamina mixin.
 *
 * A lamina mixin manages the details for checking candidate nodes to see if
 * they satisfy the user-specified lamina association constraints (knock-in or
 * knock-out).
 *
 * An accessory structure is the simulation level lamina mixin which defines
 * any global data and shared utilities among all samples.
 */

#ifndef uSisNullIntrLamMixin_h
#define uSisNullIntrLamMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibBudgUtil.h"
#include "uSisNullSimLevelMixin.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNullIntrLamSimLevelMixin : public uSisNullSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    // Seed interface

    /**
     * Modifies seed radii according to lamina interaction constraints
     * @param seed_radii - Matrix of size 3 rows x (number of loci) columns,
     *  each column is the (X, Y, Z) radii respectively of the bounding seed
     *  volume for that locus
     * @param sim - Outer simulation
     * @param config - User configuration
     */
    void update_seed_radii(uMatrix& seed_radii,
                           const sim_t& sim,
                           uSpConstConfig_t config) const {}
    // Accessors

    /**
     * @return Lamina knock-in interaction failure budget
     */
    uUInt get_intr_lam_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Lamina knock-out interaction failure budget
     */
    uUInt get_intr_lam_ko_max_fail() const { return U_TO_UINT(0); }

    // Export interface

    /**
     * Copies lamina interaction fragments to 'out'
     */
    void get_intr_lam_frags(uUIMatrix& out) const { out.clear(); }

    /**
     * Copies knock-in lamina interactions to 'out'
     */
    void get_intr_lam_kin(uUIVecCol& out) const { out.clear(); }

    /**
     * Copies knock-out lamina interactions to 'out'
     */
    void get_intr_lam_ko(uUIVecCol& out) const { out.clear(); }
};

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNullIntrLamMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Lamina knock-in budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_lam_budg_acc_kin_t;

    /**
     * Lamina knock-out budget access policy
     */
    typedef uIntrLibBudgAccNull<glue_t> intr_lam_budg_acc_ko_t;

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Checks if seed can satisfy lamina interaction constraints
     * @param sample - Sample being seeded
     * @param nfo - Common seed information
     * @param sim - Outer simulation
     * @return uTRUE always!
     */
    inline uBool is_seed_okay(sample_t& sample,
                              const uSisUtils::seed_info_t& nfo,
                              const sim_t& sim U_THREAD_ID_PARAM) const {
        return uTRUE;
    }

    /**
     * Determines which positions satisfy lamina constraints
     * @param out_legal_candidates_primed - output [0,1] array with an element
     *  set to 1 if no constraints have been violated and 0 otherwise.
     * @param candidate_centers - Matrix containing candidate positions to
     *  test for constraint violations.
     * @param candidate_radius - The radius of the candidate node (assumes all
     *  candidates have same radius)
     * @param parent_node_id - The node identifier from which candidates
     *  positions were generated from.
     * @param sample - Parent sample containing this mixin
     * @param sim - Parent simulation containing global sample data
     */
    void intr_lam_filter(uBoolVecCol& out_legal_candidates_primed,
                         const uMatrix& candidate_centers,
                         const uReal candidate_radius,
                         const uUInt parent_node_id,
                         const sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) const {}

    /**
     * Updates sample mixin states with new node information
     * @param node_id - a unique chromatin node identifier
     * @param point - 3D coordinates of node centroid
     * @param radius - the radius of the node to add
     * @param candidate_id - identifies candidate that was selected
     * @param sample - the sample containing this mixin
     * @param sim - the parent simulation of parameter sample
     */
    void intr_lam_update(const uUInt node_id,
                         const uReal* point,
                         const uReal radius,
                         const uUInt candidate_id,
                         sample_t& sample,
                         const sim_t& sim U_THREAD_ID_PARAM) const {}
};

#endif  // uSisNullIntrLamMixin_h
