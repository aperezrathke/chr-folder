//****************************************************************************
// uAppFea.h
//****************************************************************************

#ifndef uAppFea_h
#define uAppFea_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uGlobals.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uSisUtilsExport.h"
#include "uThread.h"
#include "uTypes.h"

#include <string>

//****************************************************************************
// uAppFea
//****************************************************************************

class uAppFea {
public:
    /**
     * @param cmd_opts - Command line options
     * @param announce - Console output announcing app is being run
     * @return exit code normal(0) if fold successful, error(1) if invalid
     *  user options or program state, and no_fold(2) o/w
     */
    template <typename sim_t>
    static int main(const uCmdOptsMap& cmd_opts, const std::string& announce) {
        // Create configuration
        uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
        // Parse command line and any INI configuration (override defaults)
        config->init(cmd_opts);

        // Output configuration
        uLogf("%s\n", announce.c_str());
        config->print();

        // Initialize globals
        uG::init(config);
        U_LOG_NUM_WORKER_THREADS;

        // Feed configuration to simulation
        sim_t sim(config U_MAIN_THREAD_ID_ARG);

        // Run simulation!
        const uBool result = sim.run();

        // Report final status
        uLogf("SIS simulation completed with status: %d\n", (int)result);

        // Export!
        uSisUtils::export_sim(sim);

        // Destroy globals
        uG::teardown();

        return result ? uExitCode_normal : uExitCode_error_no_fold;
    }

};  // uAppFea

#endif  // uAppFea_h
