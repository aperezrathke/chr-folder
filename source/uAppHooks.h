//****************************************************************************
// uAppHooks.h
//****************************************************************************

/**
 * @brief Generic system for registering and launching applications that can
 * be executed.
 */

#ifndef uAppHooks_h
#define uAppHooks_h

#include "uBuild.h"
#include "uCmdOptsMap.h"
#include "uTypes.h"

/**
 * Application info structure
 */
struct uAppInfo {
    /**
     * The name of the application
     */
    const char* name;
    /**
     * The callback to run the application
     */
    int (*fp_main)(const uCmdOptsMap&);
};

/**
 * @return true if command line says we should run parameter commandlet, false
 * otherwise
 */
inline uBool uShouldRunApp(const uCmdOptsMap& cmd_opts, const char* app_name) {
    return cmd_opts.key_exists(app_name);
}

/**
 * Parse command line and run any utilities specified
 * Note: do { ... } while(0) allows multi-line macro to behave like single-line
 */
#define U_CONDITIONAL_RUN_APPS(cmd_opts, apps_array)                \
    do {                                                            \
        const int NUM_APPS = sizeof(apps_array) / sizeof(uAppInfo); \
        for (int ix_app = 0; ix_app < NUM_APPS; ++ix_app) {         \
            if (uShouldRunApp(cmd_opts, apps_array[ix_app].name)) { \
                return (*(apps_array[ix_app].fp_main))(cmd_opts);   \
            }                                                       \
        }                                                           \
    } while (0)

#endif  // uAppHooks_h
