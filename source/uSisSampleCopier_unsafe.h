//****************************************************************************
// uSisSampleCopier_unsafe
//****************************************************************************

#ifndef uSisSampleCopier_unsafe_h
#define uSisSampleCopier_unsafe_h

#include"uBuild.h"
#include "uAssert.h"
// Help to ensure source and destination sample sizes match - this is not a
// guarantee but this is the least we can do!
#include "uStaticAssert.h"

/**
 * @WARNING
 * @HACK - This is an implementation that assumes two sample types have the
 * same memory layout! If this is not the case (e.g. samples with different
 * collision types like octree instead of uniform grid or additional data
 * members), then the destination sample will not be properly copied and
 * will be garbage; in which case, a custom converter object to handle
 * interconversion between the two types must be defined.
 *
 * This class forces the compiler to treat the source and destination sample
 * types as equivalent and then calls the default assignment operator=
 */
class uSisSampleCopier_unsafe {
public:
    /**
     * Forces type conversion and then assigns: to = from
     * @param from - the source sample to copy from
     * @param to - the destination sample to be overwritten by from
     */
    template <typename t_sample_from, typename t_sample_to>
    inline static void to(const t_sample_from& from_, t_sample_to& to_) {
        // SOURCE AND DESTINATION TYPES MUST BE OF SAME SIZE!!
        // The least we can do is ensure sizes match. This is not a guarantee
        // that conversion will work! If this assert trips, then a custom
        // sample inter-converter for these two sample types is needed.
        U_STATIC_ASSERT_MSG(
            sizeof(t_sample_from) == sizeof(t_sample_to),
            "Error: size mismatch between source and destination types.");
        // Extra paranoid - also test as runtime assert
        uAssert(sizeof(t_sample_from) == sizeof(t_sample_to));
        /**
         * Alternative (untested) way, which may satisfy strict aliasing
         *
         * union sample_conv_t {
         *   const t_sample_from* mp_from;
         *   const t_sample_to* mp_to;
         * };
         * sample_conv_t converter;
         * converter.mp_from = &from_;
         * to_ = *(converter.mp_to);
         */
         // Is there a difference between these two lines?
         // to = reinterpret_cast<const t_sample_to&>(from);
        reinterpret_cast<t_sample_from&>(to_) = from_;
    }
};

#endif // uSisSampleCopier_unsafe_h
