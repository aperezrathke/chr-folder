//****************************************************************************
// uLogf.h
//****************************************************************************

#ifndef uLogf_h
#define uLogf_h

#include "uBuild.h"

/**
 * A logging macro useful for debugging
 */
#ifdef U_BUILD_ENABLE_LOGGING
#   include <stdio.h>
    // Flush stdout so that batch scripts can keep us updated
#   define uLogf(...)            \
        do {                     \
            printf(__VA_ARGS__); \
            fflush(stdout);      \
        } while (0)
#else
#   define uLogf(...)
#endif  // U_BUILD_ENABLE_LOGGING

#endif  // uLogf_h
