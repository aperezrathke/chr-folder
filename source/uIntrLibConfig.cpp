//****************************************************************************
// uIntrLibConfig.cpp
//****************************************************************************

/**
 * Utilities for parsing and loading interaction constraint configuration
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibConfig.h"
#include "uAssert.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uIntrLibDefs.h"
#include "uIntrLibParser.h"
#include "uLogf.h"
#include "uOpts.h"
#include "uParserRle.h"
#include "uTypes.h"

#include <string>

//****************************************************************************
// Helper namespace
//****************************************************************************

namespace {

/**
 * Copies read bit mask to destination if expected size matches, warns if size
 * mismatch encountered (and avoids copy)
 */
void try_copy_bit_mask(uBoolVecCol& out_mask,
                       const uBoolVecCol& read_mask,
                       const char* data_name) {
    uAssert(!out_mask.is_empty());
    uAssert(data_name);
    if (out_mask.n_elem == read_mask.n_elem) {
        out_mask = read_mask;
    } else {
        uLogf(
            "Warning: expected %s bit mask of size %d, but instead read %d "
            "elements. Ignoring.\n",
            data_name,
            (int)out_mask.n_elem,
            (int)read_mask.n_elem);
    }
}

/**
 * Converts read index mask to bit mask and copies to destination if no bounds
 * violations encountered, warns if violation encountered (and avoids copy)
 */
void try_copy_bit_mask_from_index(uBoolVecCol& out_mask,
                                  const uUIVecCol& read_mask,
                                  const char* data_name) {
    uAssert(!out_mask.is_empty());
    uAssert(data_name);
    if (read_mask.empty()) {
        uLogf("Warning: unable to read %s index mask.\n", data_name);
        return;
    }
    const uUInt max_index = read_mask.max();
    if (max_index < out_mask.n_elem) {
        // Convert from index mask to boolean mask
        out_mask.fill(uFALSE);
        for (uMatSz_t i = 0; i < read_mask.n_elem; ++i) {
            const uMatSz_t index = read_mask.at(i);
            uAssertBounds(index, U_TO_MAT_SZ_T(0), out_mask.n_elem);
            out_mask.at(index) = uTRUE;
        }
    } else {
        uLogf(
            "Warning: expected %s index mask with maximum index of %d, but "
            "instead read maximum index of %d. Ignoring.\n",
            data_name,
            ((int)out_mask.n_elem) - 1,
            (int)max_index);
    }
}

/**
 * @param mask - output boolean mask, assumed to be non-empty & proper size
 * @param mask_str - bit mask string
 * @param data_name - data type description (e.g. "knock-in")
 */
void load_bit_mask(uBoolVecCol& mask,
                   const std::string& mask_str,
                   const char* data_name) {
    uBoolVecCol temp;
    uIntrLibParser::read_bit_mask_inplace(temp, mask_str);
    try_copy_bit_mask(mask, temp, data_name);
}

/**
 * @param mask - output boolean mask, assumed to be non-empty & proper size
 * @param mask_fpath - path to external boolean mask file
 * @param data_name - data type description (e.g. "knock-in")
 */
void load_bit_mask_fpath(uBoolVecCol& mask,
                         const std::string& mask_fpath,
                         const char* data_name) {
    uBoolVecCol temp;
    uIntrLibParser::read_bit_mask(temp, mask_fpath);
    try_copy_bit_mask(mask, temp, data_name);
}

/**
 * @param mask - output boolean mask, assumed to be non-empty & proper size
 * @param mask_str - index mask string
 * @param data_name - data type description (e.g. "knock-in")
 */
void load_bit_mask_from_index(uBoolVecCol& mask,
                              const std::string& mask_str,
                              const char* data_name) {
    uUIVecCol temp;
    uIntrLibParser::read_index_mask_inplace(temp, mask_str);
    try_copy_bit_mask_from_index(mask, temp, data_name);
}

/**
 * @param mask - output boolean mask, assumed to be non-empty & proper size
 * @param mask_fpath - path to external index mask file
 * @param data_name - data type description (e.g. "knock-in")
 */
void load_bit_mask_from_index_fpath(uBoolVecCol& mask,
                                    const std::string& mask_fpath,
                                    const char* data_name) {
    uUIVecCol temp;
    uIntrLibParser::read_index_mask(temp, mask_fpath);
    try_copy_bit_mask_from_index(mask, temp, data_name);
}

}  // namespace

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utilities for querying and loading user-specified proximity configuration
 */
namespace uIntrLibConfig {

/**
 * Loads plain-text index vector file from disk. The expected format is
 *  whitespace or CSV delimited
 * @param out - Output index vector, always cleared even if no load occurs
 * @param config - User configuration
 * @param key - Option key for file path to index file
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index data
 * @return uTRUE if successfully loaded, uFALSE o/w
 */
uBool load_index(uUIVecCol& out,
                 uSpConstConfig_t config,
                 const uOptE key,
                 const uBool no_fail,
                 const char* data_name) {
    uAssert(data_name);
    out.clear();

    // Check if path exists in config
    std::string fpath;
    if (!config->resolve_path(fpath, key)) {
        if (no_fail) {
            uLogf(
                "Error: failed to find path for %s (%s) index data. Exiting.\n",
                data_name,
                uOpt_get_ini_key(key));
            exit(uExitCode_error);
        }
        return uFALSE;
    }

    // Parse indices
    uLogf("Loading %s\n", fpath.c_str());
    if (!out.load(fpath)) {
        out.clear();
        if (no_fail) {
            uLogf("Error: failed to load %s (%s) index data. Exiting.\n",
                  data_name,
                  uOpt_get_ini_key(key));
            exit(uExitCode_error);
        }
        return uFALSE;
    }

    return uTRUE;
}

/**
 * Loads plain-text index pairs file from disk. The expected format is:
 *
 *      <first_index><sep><second_index>
 *      ...
 *      <first_index><sep><second_index>
 *
 *  where <*index> is a 0-based integer index. The <sep> field separates
 *  integer indices and can be any length set of consecutive non-numeric
 *  characters. Therefore, <sep> formats such as comma-separated or white
 *  space delimited are both acceptable. Here is an example file in white
 *  space delimited format:
 *
 *          <line 0>: 0 4
 *          <line 1>: 5 8
 *          <line 2>: 9 12
 *          <line 3>: 13 13
 *
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class.
 * @param b_order - If 'b_order' is TRUE, then the smaller index in pair is
 *  stored in row defined by enum uIntrLibPairIxMin and the larger index is
 *  stored in row defined by enum uIntrLibPairIxMax. If 'b_order' is FALSE,
 *  the first index read in pair is in row enum uIntrLibPairIxMin and second
 *  index read is in row enum uIntrLibPairIxMax.
 * @param config - User configuration
 * @param key - Option key for file path to index pairs
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index pair data
 * @return uTRUE if matrix successfully loaded, uFALSE o/w
 */
uBool load_index_pairs(uUIMatrix& out_pairs,
                       const uBool b_order,
                       uSpConstConfig_t config,
                       const uOptE key,
                       const uBool no_fail,
                       const char* data_name) {
    uAssert(data_name);
    // Check if path exists in config
    std::string fpath;
    if (!config->resolve_path(fpath, key)) {
        if (no_fail) {
            uLogf(
                "Error: failed to find path for %s (%s) index data. "
                "Exiting.\n",
                data_name,
                uOpt_get_ini_key(key));
            exit(uExitCode_error);
        }
        return uFALSE;
    }

    // Parse index pairs
    uLogf("Loading %s\n", fpath.c_str());
    if (!uIntrLibParser::read_index_pairs(out_pairs, fpath, b_order)) {
        if (no_fail) {
            uLogf("Error: failed to load %s (%s) index data. Exiting.\n",
                  data_name,
                  uOpt_get_ini_key(key));
            exit(uExitCode_error);
        }
        return uFALSE;
    }

    return uTRUE;
}

/**
 * Loads ordered, chromatin fragments from disk but defaults to individual
 *  monomers if no file loaded and no_fail is FALSE. File format is same as
 *  load_index_pairs().
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class. If no fragments file found, then output matrix will be
 *  2 x num_mon and each column will store the 0-based column index.
 * @param config - User configuration
 * @param key - Option key for file path to index pairs
 * @param no_fail - If TRUE, will exit on error
 * @param data_name - Used for logging events involving index pair data
 * @param num_mon - Number of chromatin monomers (nodes) present in simulation
 */
void load_frag(uUIMatrix& out_pairs,
               uSpConstConfig_t config,
               const uOptE key,
               const uBool no_fail,
               const char* data_name,
               const uUInt num_mon) {
    if (!load_index_pairs(
            out_pairs, uTRUE /*b_order*/, config, key, no_fail, data_name)) {
        // load_index_pairs() will exit if no_fail is TRUE
        uAssert(!no_fail);
        uLogf(
            "Warning: no %s (%s) loaded, defaulting to individual monomers.\n",
            data_name,
            uOpt_get_ini_key(key));
        out_pairs.set_size(uIntrLibPairIxNum, num_mon);
        for (uMatSz_t i = 0; i < out_pairs.n_cols; ++i) {
            // Set fragments to be individual monomers
            out_pairs.at(uIntrLibPairIxMin, i) =
                out_pairs.at(uIntrLibPairIxMax, i) = U_TO_UINT(i);
        }
    }
}

/**
 * Initializes vector of distance constraint
 * @param out_dist - Output vector of distances
 * @WARNING - out_dist is always cleared, even if no data loaded!
 * @param num - Target size of 'out_dist'
 * @param config - User configuration
 * @param opt_fpath - Option key to RLE distances files
 * @param opt_scalar - Option key for scalar distance
 * @param default_scalar - Default scalar distance
 * @param data_name - Used for logging events involving distance data
 */
void load_dist(uVecCol& out_dist,
               const uMatSz_t num,
               uSpConstConfig_t config,
               const enum uOptE opt_fpath,
               const enum uOptE opt_scalar,
               const uReal default_scalar,
               const char* data_name) {
    // Clear any old data
    out_dist.clear();
    // Early out if no interactions
    if (num == U_TO_MAT_SZ_T(0)) {
        return;
    }
    // Check if RLE file exists
    std::string rle_path;
    if (config->read_into(rle_path, opt_fpath)) {
        if (uParserRle::read<uReal, uVecCol>(out_dist, rle_path)) {
            if (out_dist.n_elem != num) {
                out_dist.clear();
                uLogf(
                    "Warning: Ignoring %s (%s) distance RLE file (%s) which "
                    "encodes %d elements for %d target distances.\n",
                    data_name,
                    uOpt_get_ini_key(opt_fpath),
                    rle_path.c_str(),
                    (int)out_dist.n_elem,
                    (int)num);
            }
        } else {
            out_dist.clear();
            uLogf("Warning: unable to read %s (%s) distance RLE file: %s\n",
                  data_name,
                  uOpt_get_ini_key(opt_fpath),
                  rle_path.c_str());
        }
    }

    if (out_dist.is_empty()) {
        // Defer to scalar value if RLE file failed to load
        const uReal dist = uIntrLibConfig::load_dist(
            config, opt_scalar, default_scalar, data_name);
        out_dist.set_size(num);
        out_dist.fill(dist);
    }

    // Check distances are valid!
    for (uMatSz_t i = 0; i < out_dist.n_elem; ++i) {
        const uReal d = out_dist.at(i);
        if (d < U_TO_REAL(0.0)) {
            uLogf(
                "Error: %s (%s|%s) distance %d must be non-negative (%f). "
                "Exiting.\n",
                data_name,
                uOpt_get_ini_key(opt_fpath),
                uOpt_get_ini_key(opt_scalar),
                (int)i,
                (double)d);
            exit(uExitCode_error);
        }
    }

    // Final output should match target size
    uAssert(out_dist.n_elem == num);
}

/**
 * Initializes scalar distance constraint
 * @param config - User configuration
 * @param opt_scalar - Option key for scalar distance
 * @param default_scalar - Default scalar distance
 * @param data_name - Used for logging events involving distance data
 * @return User scalar distance value or default value if not specified
 */
uReal load_dist(uSpConstConfig_t config,
                const enum uOptE opt_scalar,
                const uReal default_scalar,
                const char* data_name) {
    uReal dist = default_scalar;
    config->read_into<uReal>(dist, opt_scalar);
    if (dist < U_TO_REAL(0.0)) {
        uLogf("Error: %s (%s) distance must be non-negative (%f). Exiting.\n",
              data_name,
              uOpt_get_ini_key(opt_scalar),
              (double)dist);
        exit(uExitCode_error);
    }
    return dist;
}

/**
 * Load mask from command line or file. A bit mask is a 0|1 string (all
 * characters not 0|1 are ignored). An index mask is a delimited set of
 * integer indices; the delimiter is any series of non-integer characters. The
 * priority for which mask is used when multiple masks are specified is as
 * follows:
 *
 *  bit_mask (cmd) > bit_mask_fpath (cmd) > index_mask (cmd)
 *      > index_mask_fpath (cmd) > bit_mask (INI) > bit_mask_fpath (INI)
 *          > index_mask (INI) > index_mask_fpath (INI)
 *
 * @param mask - Output boolean mask
 * @WARNING, mask always reset even if not loaded
 * @param num - Size of mask (mask will be sized to this value)
 * @param config - User configuration
 * @param min_mask_opt -Start mask user option
 * @WARNING, ASSUMES THE FOLLOWING:
 *  Will attempt to load a user mask according to the following priority:
 *  min_mask_opt + 0 = bit mask string
 *  min_mask_opt + 1 = path to external bit mask file
 *  min_mask_opt + 2 = index mask string
 *  min_mask_opt + 3 = path to external index mask file
 * @param data_name - Used for logging events involving mask data
 */
void load_mask(uBoolVecCol& mask,
               const uMatSz_t num,
               uSpConstConfig_t config,
               const uOptE min_mask_opt,
               const char* data_name) {
    /**
     * Offsets used to enumerate all possible mask options
     */
    enum {
        off_bit_mask = 0,
        off_bit_mask_fpath,
        off_index_mask,
        off_index_mask_fpath,
        off_num
    };

    // Reset mask
    mask.clear();

    // Early out if no interactions
    if (num < U_TO_MAT_SZ_T(1)) {
        return;
    }

    // Default to all interactions enabled
    mask.set_size(num);
    mask.fill(uTRUE);

    // Create vector of options
    std::vector<uOptE> ixs(off_num);
    std::vector<uBool> should_resolve(off_num);
    for (size_t i = off_bit_mask; i < off_num; ++i) {
        ixs[i] = static_cast<uOptE>(min_mask_opt + i);
        should_resolve[i] = ((i % 2) == 1) ? uTRUE : uFALSE;
    }

    // Check if configuration specifies a mask
    std::string optstr;
    const size_t index = config->read_into(optstr, ixs, should_resolve);
    if (index >= ixs.size()) {
        return;
    }

    // Process return option offset
    switch (index) {
        case off_bit_mask:
            load_bit_mask(mask, optstr, data_name);
            break;
        case off_bit_mask_fpath:
            load_bit_mask_fpath(mask, optstr, data_name);
            break;
        case off_index_mask:
            load_bit_mask_from_index(mask, optstr, data_name);
            break;
        case off_index_mask_fpath:
            load_bit_mask_from_index_fpath(mask, optstr, data_name);
            break;
        default:
            uAssert(false && "unrecognized option offset");
            break;
    }
}

/**
 * Initializes maximum number of interaction constraints that can fail. Note,
 *  user inputs a real-valued scalar option in [0,1] that represents the
 *  maximum proportion of constraints that may fail; in constrast, this method
 *  outputs an integral value in [0, 'num'].
 * @WARNING, THIS METHOD SHOULD BE CALLED AFTER THE RELEVANT CONSTRAINT SET
 *  HAS BEEN INITIALIZED AND FILTERED (i.e. after masking has been applied)!
 * @param max_fail - Output target, maximum number of constraints that are
 *  allowed to fail before sample is culled; output will always be integer in
 *  range [0, 'num'].
 * @param config - User configuration
 * @param fail_budg_opt - Option key for failure budget
 * @param default_fail_budg_frac - Default failure budget fraction in [0,1]
 * @param const uUInt num - Max number of constraints
 * @param data_name - Used for logging events
 */
extern void load_fail_budg(uUInt& max_fail,
                           uSpConstConfig_t config,
                           const uOptE fail_budg_opt,
                           const uReal default_fail_budg_frac,
                           const uUInt num,
                           const char* data_name) {
    // Resolve budget fraction
    uAssertRealBoundsInc(
        default_fail_budg_frac, U_TO_REAL(0.0), U_TO_REAL(1.0));
    uReal fail_budg_frac = default_fail_budg_frac;
    config->read_into(fail_budg_frac, fail_budg_opt);
    if ((fail_budg_frac < U_TO_REAL(0.0)) ||
        (fail_budg_frac > U_TO_REAL(1.0))) {
        uLogf(
            "Warning: %s (%s) budget fraction (%f) not in [0,1]. Setting to "
            "default (%f).\n",
            data_name,
            uOpt_get_ini_key(fail_budg_opt),
            (double)fail_budg_frac,
            (double)default_fail_budg_frac);
        fail_budg_frac = default_fail_budg_frac;
    }
    uAssertRealBoundsInc(fail_budg_frac, U_TO_REAL(0.0), U_TO_REAL(1.0));
    // Determine absolute minimum number of constraints
    // Note: 0.5 padding converts integer truncation to integer "rounding"
    const uReal fbudg = (fail_budg_frac * U_TO_REAL(num)) + U_TO_REAL(0.5);
    max_fail = U_TO_UINT(fbudg);
    // Extra paranoid: ensure output is in [0, 'num']
    max_fail = U_CLIP(max_fail, U_TO_UINT(0), num);
    // Report initialized value
    uLogf("Initialized %s (%s) failure budget to %u constraints.\n",
          data_name,
          uOpt_get_ini_key(fail_budg_opt),
          ((unsigned int)max_fail));
}  // namespace uIntrLibConfig

}  // namespace uIntrLibConfig
