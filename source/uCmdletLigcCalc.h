//****************************************************************************
// uCmdletLigcCalc.h
//****************************************************************************

/**
 * Computes contact profiles using a ligation threshold distance
 *
 * Benefits of C++ versus high level R/python implementation include:
 *  - Can easily utilize user's model INI specification/configuration for
 *      things like node radii, polymer length, output directory, etc.
 *  - Memory scalability via streaming samples, only a single sample per
 *      thread is ever resident in memory at any one time
 *  - Memory and CPU scalability in distance calculation, rather than a full
 *      O(n^2) distance matrix, we use spatial acceleration queries to
 *      drastically reduce the memory and CPU footprint for large polymers
 *  - CPU parallelization via "light weight" multi-threading rather than full
 *      processes
 *
 * Usage:
 * -cmdlet_ligc_calc --conf <path> [...]
 */

#ifdef uCmdletLigcCalc_h
#   error "Commandlet Ligc Calc included multiple times!"
#endif  // uCmdletLigcCalc_h
#define uCmdletLigcCalc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uAssert.h"
#include "uBitsetHandle.h"
#include "uBroadPhase.h"
#include "uChromatinExportCsv.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uExportFlags.h"
#include "uFilesystem.h"
#include "uGlobals.h"
#include "uLigcCore.h"
#include "uLogf.h"
#include "uParserTable.h"
#include "uSisHetrNodeRadiusMixin.h"
#include "uSisUtilsCollision.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsNucleus.h"
#include "uStringUtils.h"
#include "uThread.h"
#include "uTypes.h"

#include <boost/algorithm/string.hpp>
#include <fstream>
#include <string>
#include <utility>

//****************************************************************************
// Utilities
//****************************************************************************

namespace uLigc {

/**
 * Data needed by broad phase
 */
typedef struct {
    /**
     * The length of a uniform grid cell
     */
    uReal m_cell_length;
    /**
     * The max positive grid coordinate
     */
    uReal m_grid_radius;
} BroadCfg_t;

typedef struct {
    /**
     * Base output directory
     */
    uFs::path m_export_dir;
    /**
     * Export configuration bits
     */
    uUInt m_export;
    /**
     * Euclidean distance ligation threshold
     */
    uReal m_ki_dist;
    /**
     * Radius at each monomer node
     */
    uVecCol m_node_radii;
    /**
     * Broad phase configuration
     */
    BroadCfg_t m_broad_cfg;

} Invariant_t;

/**
 * Forward declare local type
 */
class Local;

/**
 * Sample data
 */
class Sample {
public:
    /**
     * Loads sample geometry, computes ligation contacts, and exports ligation
     * contacts to disk
     * @param local - Local arguments, see uLigc::Local
     * @param inv - Arguments which are invariant for all process() calls
     * @return TRUE if sample processed okay, FALSE o/w
     */
    static uBool process(Local& local, const Invariant_t& inv);

private:
    /**
     * Broad phase element identifier
     */
    typedef uLigcCore::elem_id_t elem_id_t;

    /**
     * Contact tuple (index of first monomer, index of second monomer)
     */
    typedef uLigcCore::contact_t contact_t;

    /**
     * Set of contact tuples within a polymer
     */
    typedef uLigcCore::profile_t profile_t;

    /**
     * Loads sample geometry from disk
     * @param fpath - path to sample data
     * @param node_radii - radius at each monomer node
     * @param broad_cfg - details needed to configure sample broad phase
     * @return TRUE if sample loaded properly, FALSE o/w
     */
    uBool load(const std::string& fpath,
               const uVecCol& node_radii,
               const BroadCfg_t& broad_cfg) {
        uAssert(!node_radii.is_empty());

        // Open file handle
        std::ifstream fin(fpath.c_str());
        if (!fin.good()) {
            // Unable to open file handle
            fin.close();
            return uFALSE;
        }

        // Parse log weight - assumed to be among first set of comment lines
        uBool b_parse_okay = uFALSE;
        do {
            m_log_weight_str.clear();
            std::getline(fin, m_log_weight_str);
            // Verify log weight string matches expected pattern
            b_parse_okay = uStringUtils::starts_with(
                m_log_weight_str, U_CHROMATIN_EXPORT_CSV_LOG_WEIGHT_PREFIX);
        } while ((!b_parse_okay) &&
                 uStringUtils::starts_with(
                     m_log_weight_str, U_CHROMATIN_EXPORT_CSV_COMMENT_PREFIX) &&
                 fin.good());
        if (!b_parse_okay) {
            // Error parsing log weight
            fin.close();
            return uFALSE;
        }
        // Trim leading and trailing whitespace
        boost::trim(m_log_weight_str);

        // Parse sample geometry data
        uParserTable table;
        b_parse_okay =
            table.read(fin, uFALSE /*has_header*/, uFALSE /*is_jagged*/);
        fin.close();
        if (!b_parse_okay) {
            // Error parsing sample geometry
            return uFALSE;
        }

        // Check expected size
        if ((table.num_rows() < U_TO_UINT(1)) ||
            (table.num_rows() != node_radii.n_elem) ||
            (table.num_cols() < uDim_num)) {
            // Error due to size mismatch
            return uFALSE;
        }

        // Extract X, Y, Z columns
        std::vector<uReal> X, Y, Z;
        table.get_col<uReal, std::vector<uReal> >(X, U_TO_UINT(uDim_X));
        table.get_col<uReal, std::vector<uReal> >(Y, U_TO_UINT(uDim_Y));
        table.get_col<uReal, std::vector<uReal> >(Z, U_TO_UINT(uDim_Z));
        uAssertPosEq(X.size(), Y.size());
        uAssertPosEq(X.size(), Z.size());

        // Reset broad phase
        m_broad_phase.init(node_radii.n_elem,
                           broad_cfg.m_cell_length,
                           broad_cfg.m_grid_radius);

        // Copy **transposed** data (more efficient proximity check)
        m_node_positions.set_size(uDim_num /*n_rows*/,
                                  table.num_rows() /*n_cols*/);
        for (uMatSz_t i = 0; i < m_node_positions.n_cols; ++i) {
            uAssertBounds(i, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(X.size()));
            uAssertBounds(i, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(Y.size()));
            uAssertBounds(i, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(Z.size()));
            // Copy node coordinates
            uReal* const point = m_node_positions.colptr(i);
            point[uDim_X] = X[i];
            point[uDim_Y] = Y[i];
            point[uDim_Z] = Z[i];
            // Update broad phase
            uAssertBounds(i, U_TO_MAT_SZ_T(0), node_radii.n_elem);
            const uReal radius = node_radii.at(i);
            m_broad_phase.add(i,
                              point,
                              radius,
                              broad_cfg.m_cell_length,
                              broad_cfg.m_grid_radius);
        }

        // Sample loaded successfully!
        return uTRUE;
    }

    /**
     * Calculate ligation contacts
     * @param mask_buffer - pre-allocated buffer used for narrow phase checking
     * @param ki_dist - ligation proximity (Euclidean distance) threshold
     * @param node_radii - radius at each monomer node
     * @param broad_cfg - details needed by broad phase
     * @param keep_bonded - If 1, then mark (i,i) and (i,i+1) ligations
     */
    void proximity(uBitset& mask_buffer,
                   const uReal ki_dist,
                   const uVecCol& node_radii,
                   const BroadCfg_t& broad_cfg,
                   const uBool keep_bonded) {
        uLigcCore::proximity(m_profile,
                             mask_buffer,
                             ki_dist,
                             node_radii,
                             m_node_positions,
                             m_broad_phase,
                             broad_cfg.m_cell_length,
                             broad_cfg.m_grid_radius,
                             keep_bonded);
    }

    /**
     * Writes ligation profile to disk
     * @param export_dir - base export directory
     * @param fstem - file name without extension
     * @param flags - export flags
     */
    uBool export_ligc(const uFs::path& export_dir,
                      const uFs::path& fstem,
                      const uUInt flags) const {
        // Check file stem is non-empty
        if (fstem.empty()) {
            return uFALSE;
        }
        // Check CSV export
        uBool result = uTRUE;
        if (flags & uExportLigcCsv) {
            result &= export_csv(export_dir, fstem);
        }
        // Check gzip compressed CSV export
        if (flags & uExportBitLigcCsvGz) {
            result &= export_csv_gz(export_dir, fstem);
        }
        // Check Bin export
        if (flags & uExportLigcBin) {
            result &= export_bin(export_dir, fstem);
        }
        return result;
    }

    /**
     * Resolves ligation profile export path
     * @param out - output export path
     * @param export_dir - base export directory
     * @param format_no_dot - file extension suffix without '.'
     */
    static void get_export_format_dir(std::string& out,
                                      const uFs::path& export_dir,
                                      const std::string& format_no_dot) {
        uLigcCore::get_format_dir2(out, export_dir.string(), format_no_dot);
    }

    /**
     * Resolves ligation profile export path
     * @param out - output export path
     * @param export_dir - base export directory
     * @param fstem - file name without extension
     * @param ext_no_dot - file extension suffix without '.'
     */
    static void get_export_path(std::string& out,
                                const uFs::path& export_dir,
                                const uFs::path& fstem,
                                const std::string& ext_no_dot) {
        std::string export_fmt_dir;
        get_export_format_dir(export_fmt_dir, export_dir, ext_no_dot);
        uFs::path export_path = export_fmt_dir;
        export_path /= fstem.string() + std::string(".") + ext_no_dot;
        out = export_path.string();
    }

    /**
     * Export ligation profile as plain-text CSV format
     * @param export_dir - base export directory
     * @param fstem - file name without extension
     */
    uBool export_csv(const uFs::path& export_dir,
                     const uFs::path& fstem) const {

        std::string fpath;
        get_export_path(fpath, export_dir, fstem, uLigcCore::get_format_csv());
        if (fpath.empty()) {
            return uFALSE;
        }

        // Create output directory if not existing
        uFs_create_parent_dirs(fpath);

        // Open file handle
        std::ofstream fout(fpath.c_str(), std::ofstream::out);
        if (!uLigcCore::export_csv(fout, m_profile, m_log_weight_str)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
            fout.close();
            // Signal failure
            return uFALSE;
        }

        fout.close();
        // Signal success
        return uTRUE;
    }

    /**
     * Export ligation profile as gzip compressed CSV format
     * @param export_dir - base export directory
     * @param fstem - file name without extension
     */
    uBool export_csv_gz(const uFs::path& export_dir,
                        const uFs::path& fstem) const {

        std::string fpath;
        get_export_path(
            fpath, export_dir, fstem, uLigcCore::get_format_csv_gz());
        if (fpath.empty()) {
            return uFALSE;
        }

        // Create output directory if not existing
        uFs_create_parent_dirs(fpath);

        // Open file handle
        // WARNING - zstr::ofstream does not have close() method!
        zstr::ofstream fout(fpath.c_str());
        if (!uLigcCore::export_csv_gz(fout, m_profile, m_log_weight_str)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
            // Signal failure
            return uFALSE;
        }

        // Signal success
        return uTRUE;
    }

    /**
     * Export ligation profile as custom binary format
     * @param export_dir - base export directory
     * @param fstem - file name without extension
     */
    uBool export_bin(const uFs::path& export_dir,
                     const uFs::path& fstem) const {
        std::string fpath;
        get_export_path(fpath, export_dir, fstem, uLigcCore::get_format_bin());
        if (fpath.empty()) {
            return uFALSE;
        }

        // Create output directory if not existing
        uFs_create_parent_dirs(fpath);

        // Open file handle
        std::ofstream fout(fpath.c_str(),
                           std::ofstream::out | std::ios::binary);
        if (!uLigcCore::export_bin(fout, m_profile, m_log_weight_str)) {
            uLogf("Warning, unable to export to path: %s.\n", fpath.c_str());
            fout.close();
            // Signal failure
            return uFALSE;
        }

        fout.close();
        // Signal success
        return uTRUE;
    }

    /**
     * Log importance weight (string)
     */
    std::string m_log_weight_str;

    /**
     * A matrix with x,y,z coordinates for each node.
     *
     * Current layout - node positions are column-wise
     *  x1 x2 ... xN
     *  y1 y2 ... yN
     *  z1 z2 ... zN
     *
     * Memory-wise, they are stored column-major
     * | x1 | y1 | z1 | .... | xN | yN | zN |
     */
    uMatrix m_node_positions;

    /**
     * Data structure for accelerating spatial queries
     */
    uBroadPhase_t m_broad_phase;

    /**
     * Contact profile at this polymer
     */
    profile_t m_profile;
};  // namespace uLigc

/**
 * Thread local arguments to process() call
 */
class Local {
public:
    /**
     * Pre-allocate buffer(s)
     */
    void prealloc(const uUInt num_nodes) {
        mask_buffer.resize(static_cast<size_t>(num_nodes));
        mask_buffer.reset();
    }
    /**
     * Sample data
     */
    Sample s;
    /**
     * Pre-allocated buffer needed for spatial queries
     */
    uBitset mask_buffer;
    /**
     * Input sample geometry path
     */
    std::string import_fpath;
    /**
     * Export file name without extension
     */
    uFs::path export_stem;
};

/**
 * Batch work object
 */
class BatchWork
#ifdef U_BUILD_ENABLE_THREADS
    : public uMappableWork
#endif  // U_BUILD_ENABLE_THREADS
{
public:
    /**
     * Initialize from user configuration
     */
    explicit BatchWork(uSpConstConfig_t config) {
        // Initialize import directory
        std::string import_dir =
            (uFs::path(config->output_dir) / uFs::path("csv")).string();
        config->resolve_path(import_dir, uOpt_ligc_calc_import_dir);
        m_import_dir = uFs::path(import_dir);
        uFs_normalize_dir_path(m_import_dir);

        // Verify import directory exists
        if (!(uFs::exists(m_import_dir) && uFs::is_directory(m_import_dir))) {
            uLogf(
                "Error: unable to find import geometry directory: %s. "
                "Exiting.\n",
                m_import_dir.string().c_str());
            exit(uExitCode_error);
        }

        // Initialize export directory
        std::string export_dir = (uFs::path(config->output_dir) /
                                  uFs::path(uLigcCore::get_format_base()))
                                     .string();
        config->resolve_path(export_dir, uOpt_ligc_calc_export_dir);
        m_inv.m_export_dir = uFs::path(export_dir);
        uFs_normalize_dir_path(m_inv.m_export_dir);

        // Initialize export options
        m_inv.m_export = config->export_flags;
        // Enable ligc CSV export by default
        if (!config->key_exists(uOpt_export_ligc_csv_unary) &&
            !config->key_exists(uOpt_export_ligc_csv_binary)) {
            uLogf(
                "Warning: ligc CSV export enabled by default. To disable, use "
                "either:\n");
            uLogf("\ta) command-line: --export_ligc_csv 0\n");
            uLogf("\tb) INI: export_ligc_csv = 0\n");
            m_inv.m_export |= U_TO_UINT(uExportLigcCsv);
        }

        // Just to be safe, clear vector (in case we extract method)
        m_fnames.clear();
        // Cycle through geometry files within import directory
        const uFs::directory_iterator end_itr;
        for (uFs::directory_iterator itr(m_import_dir); itr != end_itr; ++itr) {
            // Assume all non-directory files are sample geometry
            if (uFs::is_regular_file(itr->path())) {
                // Store geometry file name
                m_fnames.push_back(uFs::path(itr->path().filename()));
            }
        }
        // Cache number of file names encountered
        m_num_fnames = m_fnames.size();

#ifdef U_BUILD_ENABLE_THREADS
        // Initialize chunk size
        {
            uUInt num_threads = U_DEFAULT_NUM_THREADS;
            config->read_into(num_threads, uOpt_num_worker_threads);
            uAssert(num_threads > U_TO_UINT(0));
            // Set chunk size
            m_chunk_size = U_TO_UINT(m_num_fnames) / num_threads;
            m_chunk_size = std::max(U_TO_UINT(1), m_chunk_size);
            // Cap maximum chunk size to offer better thread granularity
            m_chunk_size = std::min(U_BUILD_MAX_GROW_CHUNK_SIZE, m_chunk_size);
            uAssert(m_chunk_size > U_TO_UINT(0));
        }
#endif  // U_BUILD_ENABLE_THREADS

        // Initialize Euclidean distance ligation threshold
        m_inv.m_ki_dist = uLigcCore::get_dist(config);

        // Initialize maximum total node count (sum of all locus chains)
        uUInt max_total_num_nodes = U_TO_UINT(0);
        for (uUInt i = 0; i < config->num_nodes.size(); ++i) {
            max_total_num_nodes += config->num_nodes[i];
        }
        if (max_total_num_nodes <= U_TO_UINT(0)) {
            uLogf(
                "Error: number of polymer nodes must be positive. Exiting.\n");
            exit(uExitCode_error);
        }

        // Initialize radii
        uParseHetrRadiiUtil::get_no_fail(
            m_inv.m_node_radii, config, max_total_num_nodes);
        uAssert(m_inv.m_node_radii.n_elem == max_total_num_nodes);

        // Initialize broad phase grid cell length
        m_inv.m_broad_cfg.m_cell_length =
            uSisUtils::BroadPhaseCollisionConfig::get_grid_cell_length(
                config, m_inv.m_node_radii);
        uAssert(m_inv.m_broad_cfg.m_cell_length > U_TO_REAL(0.0));

        // Initialize broad phase grid radius
        uReal max_nuc_diam = U_TO_REAL(0.0);
        uVerify(uSisUtils::NucConfig::get_max_diameter(max_nuc_diam, config));
        if (max_nuc_diam <= U_TO_REAL(0.0)) {
            uLogf(
                "Error: nuclear diameter must be positive real. "
                "Exiting.\n");
            exit(uExitCode_error);
        }
        const uReal max_nuc_rad = U_TO_REAL(0.5) * max_nuc_diam;
        const uReal max_node_diam = U_TO_REAL(2.0) * m_inv.m_node_radii.max();
        m_inv.m_broad_cfg.m_grid_radius =
            uSisUtils::BroadPhaseCollisionConfig::get_bounding_radius(
                max_nuc_rad, max_node_diam, m_inv.m_ki_dist, m_inv.m_export);

        // Allocate thread-local buffers
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_local, prealloc, max_total_num_nodes);
    }

    /**
     * Process all sample geometry files in streaming fashion
     */
    void process(U_THREAD_ID_0_PARAM) {
        uAssert(m_fnames.size() == m_num_fnames);
        // Early out if no work
        if (m_fnames.empty()) {
            return;
        }
#ifdef U_BUILD_ENABLE_THREADS
        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        uAssert(U_THREAD_ID_0_ARG == U_MAIN_THREAD_ID_0_ARG);

        // Chunk size must be positive
        uAssert(m_chunk_size > U_TO_UINT(0));

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        uParMap.wait_until_work_finished(*this, m_chunk_size);
#else
        // Else perform serial call! Note, do_range() returns FALSE if all
        // work processed
        uVerify(!this->do_range(U_TO_UINT(0),
                                U_TO_UINT(m_num_fnames) U_THREAD_ID_ARG));
#endif  // U_BUILD_ENABLE_THREADS
    }

    /**
     * Process stream of sample over parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at
     *  ix_start
     * @return TRUE if work still to be done, FALSE if finished
     */
#ifdef U_BUILD_ENABLE_THREADS
    virtual
#endif  // U_BUILD_ENABLE_THREADS
        uBool
        do_range(const uUInt ix_start, const uUInt count U_THREAD_ID_PARAM) {
        uAssertPosEq(m_num_fnames, m_fnames.size());
        const size_t end =
            std::min(static_cast<size_t>(ix_start + count), m_num_fnames);

        // Obtain handle to local data
        Local& local = this->get_local(U_THREAD_ID_0_ARG);

        // Stream range
        for (size_t i = ix_start; i < end; ++i) {
            uAssertBounds(i, 0, m_fnames.size());
            const uFs::path& fname(m_fnames[i]);
            // Determine import path
            local.import_fpath = (m_import_dir / fname).string();
            // Determine export path
            local.export_stem = fname.stem();
            // Process
            uVerify(Sample::process(local, m_inv));
        }

        // Check if we've processed last element
        return (end < m_num_fnames);
    }

private:
    // Accessors

    /**
     * @return Thread local data
     */
    inline Local& get_local(U_THREAD_ID_0_PARAM) {
        return U_ACCESS_TLS_DATA(m_local);
    }

    // Data members

#ifdef U_BUILD_ENABLE_THREADS
    /**
     * The amount of work each thread grabs at a time
     */
    uUInt m_chunk_size;
#endif  // U_BUILD_ENABLE_THREADS

    /**
     * Input directory
     */
    uFs::path m_import_dir;

    /**
     * Sample file names to process
     */
    std::vector<uFs::path> m_fnames;

    /**
     * Cached m_fnames.size()
     */
    size_t m_num_fnames;

    /**
     * Arguments invariant across all sample.process() calls
     */
    Invariant_t m_inv;

    /**
     * Thread local data
     */
    U_DECLARE_TLS_DATA(Local, m_local);
};

}  // namespace uLigc

/**
 * Loads sample geometry, computes ligation contacts, and exports ligation
 * contacts to disk
 * @param local - Local arguments, see uLigc::Local
 * @param inv - Invariant arguments across all process() calls
 * @return TRUE if sample processed okay, FALSE o/w
 */
uBool uLigc::Sample::process(uLigc::Local& local,
                             const uLigc::Invariant_t& inv) {
    // Check arguments
    uAssert(inv.m_ki_dist >= U_TO_REAL(0.0));

    // Extract local arguments
    uLigc::Sample& s = local.s;
    uBitset& mask_buffer = local.mask_buffer;
    const std::string& import_fpath = local.import_fpath;
    const uFs::path& export_dir = inv.m_export_dir;
    const uFs::path& export_stem = local.export_stem;

    // Extract invariant arguments
    const uReal ki_dist = inv.m_ki_dist;
    const uVecCol& node_radii = inv.m_node_radii;
    const uLigc::BroadCfg_t& broad_cfg = inv.m_broad_cfg;
    const uUInt export_flags = inv.m_export;

    // Load sample geometry
    if (!s.load(import_fpath, node_radii, broad_cfg)) {
        uLogf("Warning: unable to load sample geometry file: %s\n",
              import_fpath.c_str());
        return uFALSE;
    }

    // Calculate ligation contacts
    const uBool keep_bonded =
        (export_flags & uExportLigcBonded) ? uTRUE : uFALSE;
    s.proximity(mask_buffer, ki_dist, node_radii, broad_cfg, keep_bonded);

    // Export ligation contacts
    if (!s.export_ligc(export_dir, export_stem, export_flags)) {
        uLogf(
            "Warning: unable to export ligation contacts file:\n\t-export dir: "
            "%s\n\t-export stem: %s\n\t-flags: %X\n",
            export_dir.string().c_str(),
            export_stem.string().c_str(),
            ((int)export_flags));
        return uFALSE;
    }

    // Sample processed successfully
    return uTRUE;
}

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point
 */
int uCmdletLigcCalcMain(const uCmdOptsMap& cmd_opts) {
    // Create configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);

    // Output configuration
    uLogf("Calculating ligation contacts.\n");
    config->print();

    // Initialize globals
    uG::init(config);
    U_LOG_NUM_WORKER_THREADS;

    // Create and process work
    uLigc::BatchWork work(config);
    work.process(U_MAIN_THREAD_ID_0_ARG);

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_COMMANDLETS
