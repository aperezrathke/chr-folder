//****************************************************************************
// uIntrLibParser.h
//****************************************************************************

/**
 * Utility for parsing proximity constraint configuration files
 */

#ifndef uIntrLibParser_h
#define uIntrLibParser_h

#include "uBuild.h"
#include "uIntrLibDefs.h"
#include "uTypes.h"

namespace uIntrLibParser {
/**
 * Parses file where each line is a non-negative integer pair. The expected
 * format is:
 *
 *      <first_index><sep><second_index>
 *      ...
 *      <first_index><sep><second_index>
 *
 *  where <*index> is a 0-based integer index. The <sep> field separates
 *  integer indices and can be any length set of consecutive non-numeric
 *  characters. Therefore, <sep> formats such as comma-separated or white
 *  space delimited are both acceptable. Here is an example file in white
 *  space delimited format:
 *
 *          <line 0>: 0 4
 *          <line 1>: 5 8
 *          <line 2>: 9 12
 *          <line 3>: 13 13
 *
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class.
 * @WARNING - out_pairs matrix is cleared even if no indices loaded
 * @param fpath - Path to index file where each line is a pair of 0-based
 *  indices separated by non-numeric characters such as commas and/or
 *  whitespace.
 * @param b_order - If 'b_order' is TRUE, then the smaller index in pair is
 *  stored in row defined by enum uIntrLibPairIxMin and the larger index is
 *  stored in row defined by enum uIntrLibPairIxMax. If 'b_order' is FALSE,
 *  the first index read in pair is in row enum uIntrLibPairIxMin and second
 *  index read is in row enum uIntrLibPairIxMax.
 * @return uTRUE if successful, uFALSE o/w
 */
extern uBool read_index_pairs(uUIMatrix& out_pairs,
                              const std::string& fpath,
                              const uBool b_order);

/**
 * Extract set of unsigned integers present in file
 * @param out_mask - output set of unsigned integers read from file
 * @WARNING - out_mask is cleared even if no indices loaded
 * @param fpath - Path to integer mask file
 * @return uTRUE if successful, uFALSE o/w
 */
extern uBool read_index_mask(uUIVecCol& out_mask, const std::string& fpath);

/**
 * Extract set of unsigned integers present in parameter mask string
 * @param out_mask - output set of unsigned integers read from file
 * @WARNING - out_mask is cleared even if no indices loaded
 * @param mask_str - Mask string containing index set
 * @param uTRUE if successful, uFALSE o/w
 */
extern uBool read_index_mask_inplace(uUIVecCol& out_mask,
                                     const std::string& mask_str);

/**
 * Extract set of 0|1 bits present in file
 * @param out_mask - output set of 0|1 bits read from file
 * @WARNING - out_mask is cleared even if no bits loaded
 * @param fpath - Path to bit mask file
 * @return uTRUE if successful, uFALSE o/w
 */
extern uBool read_bit_mask(uBoolVecCol& out_mask, const std::string& fpath);

/**
 * Extract set of 0|1 bits present in parameter mask string
 * @param out_mask - output set of 0|1 bits read from file
 * @WARNING - out_mask is cleared even if no bits loaded
 * @param mask_str - Mask string containing bit set
 * @param uTRUE if successful, uFALSE o/w
 */
extern uBool read_bit_mask_inplace(uBoolVecCol& out_mask,
                                   const std::string& mask_str);
}  // namespace uIntrLibParser

#endif  // uIntrLibParser_h
