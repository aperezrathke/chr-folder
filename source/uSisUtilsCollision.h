//****************************************************************************
// uSisUtilsCollision.h
//****************************************************************************

/**
 * Narrow phase collision filtering of spherical chains
 */

#ifndef uSisUtilsCollision_h
#define uSisUtilsCollision_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitsetHandle.h"
#include "uDistUtils.h"
#include "uGlobals.h"
#include "uLigcCore.h"
#include "uMemAlign.h"
#include "uSisIntrChrConfig.h"
#include "uStats.h"
#include "uThread.h"
#include "uTypeTraits.h"
#include "uTypes.h"

#ifdef U_BUILD_ENABLE_SSE_COLLISION
#   include "uSse.h"
#else
#   ifdef U_BUILD_COMPILER_MSVC
#       pragma message("Warning: SSE (AVX2) collisions are disabled.")
#   elif defined(U_BUILD_COMPILER_GCC)
#       pragma message "Warning: SSE (AVX2) collisions are disabled."
#   endif  // compiler check
#endif  // U_BUILD_ENABLE_SSE_COLLISION

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility namespace
 */
namespace uSisUtils {

//****************************************************************************
// Broad phase
//****************************************************************************

/**
 * Utility for obtaining broad phase configuration parameters
 */
class BroadPhaseCollisionConfig {
public:
    /**
     * @param nuclear_radius - radius of confining nucleus
     * @param max_node_diameter - diameter of largest polymer node
     * @param ki_dist - contact ligation distance
     * @param export_flags - simulation export flags
     * @return scalar grid bounding radius
     */
    static uReal get_bounding_radius(const uReal nuclear_radius,
                                     const uReal max_node_diameter,
                                     const uReal ki_dist,
                                     const uUInt export_flags) {
        // Determine grid boundary padding
        const uReal boundary_pad = uLigcCore::get_collision_boundary_pad(
            max_node_diameter, ki_dist, export_flags);
        return nuclear_radius + boundary_pad;
    }

    /**
     * @param config - user configuration
     * @param node_radii - monomer node radii
     * @return scalar grid cell length from user configuration
     */
    static uReal get_grid_cell_length(uSpConstConfig_t config,
                                      const uVecCol& node_radii) {
        // Initialize grid cell length, if not user specified, then
        // heuristically set default to twice the median node diameter
        uReal grid_cell_length =
            U_TO_REAL(4.0) * uMatrixUtils::median(node_radii);
        config->read_into(grid_cell_length, uOpt_collision_grid_cell_length);
        uAssert(grid_cell_length > U_TO_REAL(0.0));
        return grid_cell_length;
    }
};

/**
 * Utility encapsulating a thread-local broad-phase collision mask
 */
class BroadPhaseCollisionMask {
public:
    /**
     * Allocate collision mask
     */
    void init(const uUInt max_total_num_nodes) {
        // Initialize thread local data
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(
            m_mask, resize, max_total_num_nodes, false);
    }

    /**
     * Clear collision mask
     */
    void clear() { m_mask.clear(); }

    /**
     * @return Bit set buffer for intermediate broad phase collision results
     */
    inline uBitset& get_mask(U_THREAD_ID_0_PARAM) {
        uAssert(U_ACCESS_TLS_DATA(m_mask).size() > 0);
        return U_ACCESS_TLS_DATA(m_mask);
    }

private:
    /**
     * Dynamic bit set, 1 if element needs narrow phase checking, 0 otherwise
     * @TODO - determine if this is needed!
     */
    U_DECLARE_TLS_DATA(uBitset, m_mask);
};

//****************************************************************************
// Narrow phase
//****************************************************************************

/**
 * @param candidate_center - Center of sphere to test for collision
 * @param node_centers - Matrix with current positions of nodes within
 *  chromatin chain(s) - currently each column is a 3-D position
 *  representing a spherical node center
 * @param min_homogeneous_node_to_node_sq_dist - If the squared distance
 *  between two node spheres is less than this value, then they are
 *  colliding
 * @return TRUE (1) if parameter position *does not* collide with an
 *  existing node, FALSE (0) otherwise
 */
inline uBool narrow_phase_homogeneous_sphere_collision(
    const uReal* const candidate_center,
    const uMatrix& node_centers,
    const uReal min_homogeneous_node_to_node_sq_dist) {
    U_SCOPED_STAT_TIMER(uSTAT_CollisionNarrowPhaseTime);
    uAssert(candidate_center);
    uAssert(node_centers.n_rows >= uDim_num);
    const uMatSz_t N_NODES = node_centers.n_cols;
    uMatSz_t i = 0;
#ifdef U_BUILD_ENABLE_SSE_COLLISION
    if (sizeof(uReal) == 8 /*sizeof(double)*/) {
        uAssert(u_is_aligned(node_centers.memptr(), U_SSE_ALIGNMENT_256_PD));
        uAssert(sizeof(uReal) == U_SSE_ELEM_BYTES);
        // Assuming that node positions are stored column-wise:
        // (each column is a single position)
        uAssert(node_centers.n_rows == U_SSE_VEC_DIM);
        const size_t LIMIT = N_NODES - (N_NODES % 4);
        // Copy candidate center to SSE registers
        __m256d cc = _mm256_set_pd(0.0,
                                   candidate_center[uDim_Z],
                                   candidate_center[uDim_Y],
                                   candidate_center[uDim_X]);
        // Copy distance constraint to SSE registers
        __m256d ds = _mm256_set_pd(min_homogeneous_node_to_node_sq_dist,
                                   min_homogeneous_node_to_node_sq_dist,
                                   min_homogeneous_node_to_node_sq_dist,
                                   min_homogeneous_node_to_node_sq_dist);
        // Process four node centers at same time
        // http://stackoverflow.com/questions/10454150/intel-avx-256-bits-version-of-dot-product-for-double-precision-floating-point
        for (; i < LIMIT; i += 4) {
            // Load existing nodes to SSE registers
            /*
             * Note: direct cast to __m256d:
             *  __m256d a = *((const __m256d*)(node_centers.colptr(i)));
             * produces noticeable speed-up on Windows. However, on Linux
             * Xeon compute node with gcc compiler, no noticeable gains were
             * observed, and was perhaps marginally slower. So, leaving load
             * instructions in for now.
             */
            __m256d a = _mm256_load_pd(node_centers.colptr(i));
            __m256d b = _mm256_load_pd(node_centers.colptr(i + 1));
            __m256d c = _mm256_load_pd(node_centers.colptr(i + 2));
            __m256d d = _mm256_load_pd(node_centers.colptr(i + 3));
            // Subtract candidate center from each existing node center
            __m256d w = _mm256_sub_pd(cc, a);
            __m256d x = _mm256_sub_pd(cc, b);
            __m256d y = _mm256_sub_pd(cc, c);
            __m256d z = _mm256_sub_pd(cc, d);
            // Determine squared length of resulting vectors:
            // Dot product - mult
            a = _mm256_mul_pd(w, w);
            b = _mm256_mul_pd(x, x);
            c = _mm256_mul_pd(y, y);
            d = _mm256_mul_pd(z, z);
            // Dot product - horizontal add
            w = _mm256_hadd_pd(a, b);
            x = _mm256_hadd_pd(c, d);
            // Dot product - swap and blend
            y = _mm256_permute2f128_pd(w, x, 0x21);
            z = _mm256_blend_pd(w, x, 0b1100);
            // Final dot product
            a = _mm256_add_pd(y, z);
            // Check if all elements >= min_homogeneous_node_to_node_sq_dist
            // If so, then b[i] = 0xFFFFFFFFFFFFFFFF else it's 0
            b = _mm256_cmp_pd(a, ds, _CMP_GE_OQ);
            // If not all elements 1, then we hit something
            if (_mm256_movemask_pd(b) != 0xf) {
                return uFALSE;  // We are not collision free
            }
        }
    }   // AVX2 packed double collision support
#endif  // U_BUILD_ENABLE_SSE_COLLISION
    const uReal* node_center;
    uReal dist2[uDim_num];
    for (; i < N_NODES; ++i) {
        // Compute dot product of (candidate - node):
        // @TODO: Investigate if using LAPACK (arma::dot) is
        // faster for this operation (along with fixed size vector:
        //  arma::Col<uReal>::fixed<uDim_num>)
        // Aggregate dot product and store in X dim
        node_center = node_centers.colptr(i);
        uAssert(node_center);
        U_DIST2(dist2, candidate_center, node_center);
        if (dist2[uDim_dist] < min_homogeneous_node_to_node_sq_dist) {
            return uFALSE;  // We are not collision free
        }
    }
    // If we make it here, then we did not collide with anything
    return uTRUE;
}

/**
 * @param candidate_center - Center of sphere to test for collision
 * @param node_centers - Matrix with positions of existing (already added)
 *  nodes within chromatin chain(s) - currently each column is a 4-D vector
 *  (X, Y, Z, THRESH2) where X, Y, Z is spherical center and THRESH2 is the
 *  cached value of the squared distance threshold at each node
 * @return TRUE (1) if parameter position *does not* collide with an
 *  existing node, FALSE (0) otherwise
 */
inline uBool narrow_phase_inhomogeneous_sphere_collision(
    const uReal* const candidate_center,
    const uMatrix& node_centers) {
    U_SCOPED_STAT_TIMER(uSTAT_CollisionNarrowPhaseTime);
    // Assuming that node positions are stored column-wise:
    //  - each column is a 4-D (X, Y, Z, THRESH2) vector
    uAssert(candidate_center);
    uAssert(node_centers.n_rows == uDim_num_extended);
    uReal dist2[uDim_num];
    const uReal* node_center;
    const uMatSz_t n_nodes = node_centers.n_cols;
    for (uMatSz_t i = 0; i < n_nodes; ++i) {
        uAssertBounds(i, 0, node_centers.n_cols);
        node_center = node_centers.colptr(i);
        uAssert(node_center);
        const uReal min_node_to_node_sq_dist = node_center[uDim_dist2_thresh];
        U_DIST2(dist2, candidate_center, node_center);
        if (dist2[uDim_dist] < min_node_to_node_sq_dist) {
            return uFALSE;  // We are not collision free
        }
    }
    // If we make it here, then we did not collide with anything
    return uTRUE;
}

/**
 * @param candidate_center - Center of sphere to test for collision
 * @param candidate_radius - The radius of the sphere to test for
 *   collision.
 * @param node_centers - Matrix with positions of existing (already added)
 *  nodes within chromatin chain(s) - currently each column is a 4-D vector
 *  (X, Y, Z, R) where X, Y, Z is spherical center and R is node radius
 * @param sim - Outer simulation
 * @return TRUE (1) if parameter position *does not* collide with an
 *  existing node, FALSE (0) otherwise
 */
template <typename sim_t>
inline uBool narrow_phase_inhomogeneous_sphere_collision(
    const uReal* const candidate_center,
    const uReal candidate_radius,
    const uMatrix& node_centers,
    const sim_t& sim) {
    // Get simulation level collision mixin type
    typedef
        typename sim_t::sim_level_collision_mixin_t sim_level_collision_mixin_t;
    U_SCOPED_STAT_TIMER(uSTAT_CollisionNarrowPhaseTime);
    // Assuming that node positions are stored column-wise:
    //  - each column is a 4-D (X, Y, Z, R) vector
    uAssert(candidate_center);
    uAssert(node_centers.n_rows == uDim_num_extended);
    uReal min_node_to_node_sq_dist = U_TO_REAL(0.0);
    uReal dist2[uDim_num];
    const uReal* node_center;
    const uMatSz_t n_nodes = node_centers.n_cols;
    for (uMatSz_t i = 0; i < n_nodes; ++i) {
        uAssertBounds(i, 0, node_centers.n_cols);
        node_center = node_centers.colptr(i);
        uAssert(node_center);
        uAssert(node_centers[uDim_radius] > U_TO_REAL(0.0));
        // Determine squared distance threshold
        min_node_to_node_sq_dist =
            sim.sim_level_collision_mixin_t::
                calc_collision_hetr_min_n2n_sq_dist(candidate_radius,
                                                    node_center[uDim_radius]);
        U_DIST2(dist2, candidate_center, node_center);
        if (dist2[uDim_dist] < min_node_to_node_sq_dist) {
            return uFALSE;  // We are not collision free
        }
    }
    // If we make it here, then we did not collide with anything
    return uTRUE;
}

}  // namespace uSisUtils

#endif  // uSisUtilsCollision_h
