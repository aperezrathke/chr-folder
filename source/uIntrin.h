//****************************************************************************
// uIntrin.h
//****************************************************************************

/**
 * Platform independent include for compiler intrinsics
 */

#ifndef uIntrin_h
#define uIntrin_h

#include "uBuild.h"

// http://stackoverflow.com/questions/11228855/header-files-for-x86-simd-intrinsics
#ifdef U_BUILD_COMPILER_MSVC
/* Microsoft C/C++-compatible compiler */
#   include <intrin.h>
#elif defined(U_BUILD_COMPILER_ICC)
/* Intel compiler */
#   include <immintrin.h>
#elif defined(U_BUILD_COMPILER_GCC)
#   if (defined(__x86_64__) || defined(__i386__))
/* GCC-compatible compiler, targeting x86/x86-64 */
#       include <x86intrin.h>
#   endif 
#else
#   error Unrecognized compiler
#endif

#endif  // uIntrin_h
