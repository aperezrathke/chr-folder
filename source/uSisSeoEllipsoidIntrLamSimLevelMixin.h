//****************************************************************************
// uSisSeoEllipsoidIntrLamSimLevelMixin.h
//****************************************************************************

/**
 * A lamina interaction mixin manages nuclear lamina association constraints.
 * Note, this is different from a "nuclear mixin" which is used for modeling
 * confinement by constraining all monomer beads to reside within the nuclear
 * volume. In contrast, the lamina interaction mixin enforces polymer
 * fragments to be proximal or distal to the nuclear periphery as configured
 * by the user.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin supports an ellipsoidal nucleus.
 */

#ifndef uSisSeoEllipsoidIntrLamSimLevelMixin_h
#define uSisSeoEllipsoidIntrLamSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibConfig.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uSisIntrLamCommonBatch.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Mixin encapsulating global (sample independent) data and utilities. The
 *  user configured interaction distances may be modified under the
 *  assumption of a ellipsoidal nucleus
 */
template <typename t_SisGlue>
class uSisSeoEllipsoidIntrLamSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Batch interactions utility
     */
    typedef uSisIntrLamCommonBatch<t_SisGlue> intr_lam_batch_util_t;

private:
    /**
     * Knock-in lamina interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIVecCol& get_intr(
            uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_kin;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_loci_tables.loci_kin;
        }
        static uVecCol& get_dist(uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_kin_dist;
        }
        static uReal get_dist_scalar_default() {
            return uIntrLamDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_lam_knock_in; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_lam_knock_in_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_lam_knock_in_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_lam_knock_in_bit_mask;
        }
        static const char* get_data_name() { return uIntrLamKnockInDataName; }
        /**
         * Knock-in lamina interaction distance may be padded for homogeneous
         * radii polymers, checks for trivial interactions
         * @param dist - Output Euclidean "distance to surface" threshold,
         *  will be padded by node radius if all nodes are homogeneous
         * @param sim - Parent simulation
         * @return uTRUE if distance threshold is non-trivial, uFALSE o/w
         *  (uFALSE -> interaction is always satisfiable under nuclear
         *      confinement)
         */
        static uBool init_dist_scalar(uReal& dist, const sim_t& sim) {
            // Distance threshold must be positive real
            uAssert(dist > U_TO_REAL(0.0));
            // Check if we should pad by node radius
            if (sim_t::is_homg_radius) {
                dist += (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Determine if non-trivial
            return dist > sim.get_min_nuclear_radius();
        }
    } init_intr_lam_kin_policy_t;

    /**
     * Knock-out lamina interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIVecCol& get_intr(
            uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_ko;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_loci_tables.loci_ko;
        }
        static uVecCol& get_dist(uSisSeoEllipsoidIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_ko_dist;
        }
        static uReal get_dist_scalar_default() {
            return uIntrLamDefaultKnockOutDistScale *
                   uIntrLamDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_lam_knock_out; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_lam_knock_out_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_lam_knock_out_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_lam_knock_out_bit_mask;
        }
        static const char* get_data_name() { return uIntrLamKnockOutDataName; }
        /**
         * Knock-out lamina interaction distance may be padded for homogeneous
         * radii polymers, WILL EXIT WITH ERROR CODE IF CONSTRAINT IS
         * TRIVIALLY NON-FEASIBLE!
         * @param dist - Output Euclidean "distance to surface" threshold,
         *  will be padded by node radius if all nodes are homogeneous
         * @param sim - Parent simulation
         * @return uTRUE (will exit on error)
         */
        static uBool init_dist_scalar(uReal& dist, const sim_t& sim) {
            // Distance threshold must be positive real
            uAssert(dist > U_TO_REAL(0.0));
            // Check if we should pad by node radius
            if (sim_t::is_homg_radius) {
                dist += (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Determine if trivially infeasible
            // Check if constraint is trivially infeasible
            if (dist >= sim.get_min_nuclear_radius()) {
                // This is infeasible
                uLogf(
                    "Error: infeasible knock-out lamina interaction distance "
                    "encountered. Knock-out distance must be < (min{nuclear "
                    "radius} - node radius). Exiting.\n");
                exit(uExitCode_error);
            }
            return uTRUE;
        }
    } init_intr_lam_ko_policy_t;

public:
    /**
     * Default constructor
     */
    uSisSeoEllipsoidIntrLamSimLevelMixin() { clear(); }

    /**
     * Initializes simulation level (global) data
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        // Load fragment spans
        uIntrLibConfig::load_frag(m_intr_lam_frags,
                                  config,
                                  uOpt_lam_frag,
                                  uFALSE /*no_fail*/,
                                  uIntrLamFragDataName,
                                  sim.get_max_total_num_nodes());
        uAssert(m_intr_lam_frags.n_cols >= 1);
        uAssert(m_intr_lam_frags.n_rows == uIntrLibPairIxNum);
        // Initialize knock-in interactions
        init_intrs_lam<init_intr_lam_kin_policy_t>(
            *this, config, m_intr_lam_frags, sim);
        // Initialize knock-out interactions
        init_intrs_lam<init_intr_lam_ko_policy_t>(
            *this, config, m_intr_lam_frags, sim);
    }

    /**
     * Modifies seed radii according to lamina interaction constraints
     * @param seed_radii - Matrix of size 3 rows x (number of loci) columns,
     *  each column is the (X, Y, Z) radii respectively of the bounding seed
     *  volume for that locus
     * @param sim - Outer simulation
     * @param config - User configuration
     */
    void update_seed_radii(uMatrix& seed_radii,
                           const sim_t& sim,
                           uSpConstConfig_t config) const {
        // @TODO - Is it possible to reduce seed radii according to knock-out
        //  constraints as in spherical nucleus case?
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_intr_lam_frags.clear();
        m_intr_lam_kin.clear();
        m_intr_lam_ko.clear();
        m_intr_lam_kin_dist.clear();
        m_intr_lam_ko_dist.clear();
        intr_lam_batch_util_t::clear(m_intr_lam_loci_tables);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Const handle to interaction fragments
     */
    inline const uUIMatrix& get_intr_lam_frags() const {
        return m_intr_lam_frags;
    }

    /**
     * @return Const handle to knock-in interactions
     */
    inline const uUIVecCol& get_intr_lam_kin() const { return m_intr_lam_kin; }

    /**
     * @return Const handle to knock-out interactions
     */
    inline const uUIVecCol& get_intr_lam_ko() const { return m_intr_lam_ko; }

    /**
     * @param i - Knock-in interaction index
     * @return Knock-in Euclidean distance threshold relative to nuclear
     *  surface
     */
    inline uReal get_intr_lam_kin_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_kin_dist.n_elem);
        return m_intr_lam_kin_dist.at(i);
    }

    /**
     * @param i - Knock-out interaction index
     * @return Knock-out Euclidean distance threshold relative to nuclear
     *  surface
     */
    inline uReal get_intr_lam_ko_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_ko_dist.n_elem);
        return m_intr_lam_ko_dist.at(i);
    }

    /**
     * @return Lamina knock-in interaction failure budget
     */
    inline uUInt get_intr_lam_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Lamina knock-out interaction failure budget
     */
    inline uUInt get_intr_lam_ko_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Handle to loci interaction tables
     */
    inline const uSisIntrLamLociTables_t& get_intr_lam_loci_tables() const {
        return m_intr_lam_loci_tables;
    }

    /**
     * @return Table of lamina knock-in interactions
     */
    inline const uSisIntrLociTable_t& get_intr_lam_loci_kin() const {
        return m_intr_lam_loci_tables.loci_kin;
    }

    /**
     * @return Table of lamina knock-out interactions
     */
    inline const uSisIntrLociTable_t& get_intr_lam_loci_ko() const {
        return m_intr_lam_loci_tables.loci_ko;
    }

    // Export interface

    /**
     * Copies lamina interaction fragments to 'out'
     */
    void get_intr_lam_frags(uUIMatrix& out) const { out = m_intr_lam_frags; }

    /**
     * Copies knock-in lamina interactions to 'out'
     */
    void get_intr_lam_kin(uUIVecCol& out) const { out = m_intr_lam_kin; }

    /**
     * Copies knock-out lamina interactions to 'out'
     */
    void get_intr_lam_ko(uUIVecCol& out) const { out = m_intr_lam_ko; }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Extracts masked lamina interaction data
     */
    static void apply_intrs_lam_mask(const uBoolVecCol& mask,
                                     uUIVecCol& intr,
                                     uVecCol& dist) {
        uAssert(mask.n_elem == intr.n_elem);
        const uMatSz_t n_intr = uMatrixUtils::sum(mask);
        uAssertBoundsInc(n_intr, U_TO_MAT_SZ_T(0), intr.n_elem);
        uUIVecCol temp_intr;
        uVecCol temp_dist;
        // Extract masked data
        if (n_intr > U_TO_UINT(0)) {
            temp_intr.set_size(n_intr);
            temp_dist.set_size(n_intr);
            uUInt dst = U_TO_UINT(0);
            for (uMatSz_t src = 0; src < mask.n_elem; ++src) {
                const uBool bit = mask.at(src);
                if (bit) {
                    uAssertBounds(dst, U_TO_UINT(0), temp_intr.n_elem);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), intr.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), dist.n_elem);
                    temp_intr.at(dst) = intr.at(src);
                    temp_intr.at(dst) = intr.at(src);
                    temp_dist.at(dst) = dist.at(src);
                    ++dst;
                }
            }
            uAssert(dst == n_intr);
        }
        // Overwrite buffers
        uAssert(temp_intr.n_elem <= intr.n_elem);
        uAssert(temp_dist.n_elem <= dist.n_elem);
        uAssert(temp_intr.n_elem == temp_dist.n_elem);
        intr = temp_intr;
        dist = temp_dist;
    }

    /**
     * Initialize lamina interaction
     */
    template <typename t_init>
    static void init_intrs_lam(uSisSeoEllipsoidIntrLamSimLevelMixin& this_,
                               uSpConstConfig_t config,
                               const uUIMatrix& frags,
                               const sim_t& sim) {
        const char* const data_name = t_init::get_data_name();
        uAssert(data_name);
        // This method should not be called if no fragments exist
        uAssert(frags.n_cols >= 1);
        uAssert(frags.n_rows == uIntrLibPairIxNum);

        // Load interactions
        uUIVecCol& intr = t_init::get_intr(this_);
        if (!uIntrLibConfig::load_index(intr,
                                        config,
                                        t_init::get_opt_intr(),
                                        uFALSE /*no_fail*/,
                                        data_name)) {
            uLogf("Warning: no %s lamina interactions loaded.\n", data_name);
            intr.clear();
            return;
        }
        uAssert(intr.n_elem >= 1);
        uIntrLibInit::check_bounds_exit_on_fail(intr, frags);

        // Initialize interaction distances (with optional padding)
        uVecCol& dist = t_init::get_dist(this_);
        uIntrLibConfig::load_dist(dist,
                                  intr.n_elem,
                                  config,
                                  t_init::get_opt_dist_fpath(),
                                  t_init::get_opt_dist_scalar(),
                                  t_init::get_dist_scalar_default(),
                                  data_name);
        uAssertPosEq(dist.n_elem, intr.n_elem);
        uBoolVecCol non_trivial_mask;
        non_trivial_mask.set_size(dist.n_elem);
        non_trivial_mask.fill(uTRUE);
        for (uMatSz_t i = 0; i < dist.n_elem; ++i) {
            non_trivial_mask.at(i) = t_init::init_dist_scalar(dist.at(i), sim);
        }

        // Warn if trivial (always satisfied) interactions found
        const uMatSz_t num_non_trivial = uMatrixUtils::sum(non_trivial_mask);
        uAssert(num_non_trivial <= non_trivial_mask.n_elem);
        const uBool has_trivial = num_non_trivial != non_trivial_mask.n_elem;
        if (has_trivial) {
            const uMatSz_t num_trivial =
                non_trivial_mask.n_elem - num_non_trivial;
            uLogf(
                "Warning: %d trivial %s lamina interactions found (will be "
                "ignored).\n",
                (int)num_trivial,
                data_name);
        }

        // Read interactions mask
        uBoolVecCol mask;
        uIntrLibConfig::load_mask(
            mask, intr.n_elem, config, t_init::get_opt_mask_min(), data_name);
        uAssert(mask.n_elem == intr.n_elem);

        // Apply masking
        if (has_trivial) {
            uAssertPosEq(mask.n_elem, non_trivial_mask.n_elem);
            mask %= non_trivial_mask;
        }
        apply_intrs_lam_mask(mask, intr, dist);

        // Partition interactions by loci
        intr_lam_batch_util_t::partition_by_loci(
            t_init::get_intr_loci(this_), intr, frags, sim);

        // Compute mean interaction constraint distance (avoid divide-by-zero)
        double mean_dist = 0.0;
        if (intr.n_elem > U_TO_MAT_SZ_T(0)) {
            mean_dist = (double)uMatrixUtils::mean(dist);
        }
        // Report initialized interaction state
        uAssert(mean_dist >= 0.0);
        uAssert(intr.n_elem == dist.n_elem);
        uLogf(
            "Initialized %d %s lamina interaction constraints.\n\t-<%s "
            "distance to nuclear surface> (may be padded): %f.\n",
            (int)intr.n_elem,
            data_name,
            data_name,
            mean_dist);
    }

    /**
     * Fragment spans
     */
    uUIMatrix m_intr_lam_frags;

    /**
     * Knock-in interactions, indices over frags matrix cols
     */
    uUIVecCol m_intr_lam_kin;

    /**
     * Knock-out interactions, indices over frags matrix cols
     */
    uUIVecCol m_intr_lam_ko;

    /**
     * At least one node in lamina interacting fragment can be no farther than
     * this distance from nuclear lamina.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_kin_dist;

    /**
     * All nodes in lamina interacting fragment fragment must be greater than
     * this distance from nuclear lamina.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_ko_dist;

    /**
     * Collection of lamina interactions partitioned by loci
     */
    uSisIntrLamLociTables_t m_intr_lam_loci_tables;
};

#endif  // uSisSeoEllipsoidIntrLamSimLevelMixin_h
