//****************************************************************************
// uSisSeoSphereIntrLamSimLevelMixin.h
//****************************************************************************

/**
 * A lamina interaction mixin manages nuclear lamina association constraints.
 * Note, this is different from a "nuclear mixin" which is used for modeling
 * confinement by constraining all monomer beads to reside within the nuclear
 * volume. In contrast, the lamina interaction mixin enforces polymer
 * fragments to be proximal or distal to the nuclear periphery as configured
 * by the user.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin assumes a spherical nucleus.
 */

#ifndef uSisSeoSphereIntrLamSimLevelMixin_h
#define uSisSeoSphereIntrLamSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibConfig.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uSisIntrLamCommonBatch.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

/**
 * Mixin encapsulating global (sample independent) data and utilities. The
 *  user configured interaction distances may be modified under the
 *  assumption of a spherical nucleus
 */
template <typename t_SisGlue>
class uSisSeoSphereIntrLamSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Batch interactions utility
     */
    typedef uSisIntrLamCommonBatch<t_SisGlue> intr_lam_batch_util_t;

private:
    /**
     * Knock-in lamina interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIVecCol& get_intr(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_kin;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_loci_tables.loci_kin;
        }
        static uVecCol& get_dist(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_kin_dist;
        }
        static uVecCol& get_dist2(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_kin_dist2;
        }
        static uReal get_dist_scalar_default() {
            return uIntrLamDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_lam_knock_in; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_lam_knock_in_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_lam_knock_in_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_lam_knock_in_bit_mask;
        }
        static const char* get_data_name() { return uIntrLamKnockInDataName; }
        /**
         * Knock-in lamina interaction distance is transformed from "relative
         * to sphere surface" to instead "relative to origin" as this will be
         * easier to test computationally. For homogeneous radii polymers,
         * the output distance will be padded by bead radius.
         * @param dist - Output Euclidean "distance to origin" threshold,
         *  assumed user initialized to "distance to lamina surface", will be
         *  padded by node radius if all nodes are homogeneous
         * @param dist2 - Output squared Euclidean "distance to origin"
         *  threshold - only useful is nodes are homogeneous
         * @param sim - Parent simulation
         * @return uTRUE if distance threshold is non-trivial, uFALSE o/w
         *  (uFALSE -> interaction is always satisfiable under nuclear
         *      confinement)
         */
        static uBool init_dist_scalars(uReal& dist,
                                       uReal& dist2,
                                       const sim_t& sim) {
            uBool is_non_trivial = uTRUE;
            // Distance threshold must be positive real
            uAssert(dist > U_TO_REAL(0.0));
            /**
             * Transform distance threshold from "distance to surface" to
             * equivalent "distance to origin". This condition is easier to
             * test using simple dot product of fragment bead positions.
             * Specifically:
             *
             *  nucr - sqrt{dot{bc}} - fc2c - fbr  <= dsurf
             *  =>  nucr - dsurf - fc2c - fbr <= sqrt{dot{bc}}
             *  =>  (max{nucr - dsurf - fc2c - fbr, 0})^2 <= dot{bc}
             *  =>  (max{dorig - fc2c - fbr, 0})^2 <= dot{bc}
             *
             *  Where 'nucr' is nuclear radius, 'bc' is tested bead 3D center
             *  position, 'fc2c' is maximum center-to-center length of segment
             *  defined by center of tested bead to center of last bead of
             *  constrained fragment (where tested bead index <= last bead of
             *  constrained fragment index), 'fbr' is the bead radius of the
             *  last bead of the constrained fragment, 'dsurf' is user input
             *  knock-in lamina interaction distance, 'dorig' is knock-in
             *  lamina interaction distance relative to origin of coordinate
             *  system at (0, 0, 0).
             *
             *  Note, constraint is satisfied when tested bead is within
             *  lamina interacting fragment and the following holds:
             *
             *      (max{dorig - br, 0})^2 <= dot{bc}
             *
             *  Where 'br' is tested bead radius.
             *
             *  Furthermore, if simulation consists of homogeneous radius
             *  polymers, then radial padding HAS ALREADY OCCURRED in this
             *  method and the corresponding tests become:
             *
             *  For can possibly be satisfied (homogeneous):
             *
             *      (max{dorig_pad - fc2c, 0})^2 <= dot{bc}
             *
             *  For is satisfied (homogeneous):
             *
             *      (max{dorig_pad, 0})^2 <= dot{bc}
             *          => dorig_pad^2 <= dot{bc} : assuming removal of
             *              trivial interactions where (dorig - br) <= 0
             */
            // Compute 'dorig'
            dist = sim.get_max_nuclear_radius() - dist;
            // Check if we should pre-pad by bead radius
            if (sample_t::is_homg_radius) {
                // Compute 'dorig_pad'
                dist -= (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Check if constraint is trivially satisfiable
            if (dist <= U_TO_REAL(0.0)) {
                dist = U_TO_REAL(0.0);
                // Constraint will always be satisfied under confinement
                is_non_trivial = uFALSE;
            }
            // Compute 'dorig^2' which is only useful if homogeneous in which
            // case this value is actually 'dorig_pad^2'
            dist2 = dist * dist;
            return is_non_trivial;
        }
    } init_intr_lam_kin_policy_t;

    /**
     * Knock-out lamina interaction initialization policy callback(s)
     */
    typedef struct {
        static uUIVecCol& get_intr(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_ko;
        }
        static uSisIntrLociTable_t& get_intr_loci(
            uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_loci_tables.loci_ko;
        }
        static uVecCol& get_dist(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_ko_dist;
        }
        static uVecCol& get_dist2(uSisSeoSphereIntrLamSimLevelMixin& this_) {
            return this_.m_intr_lam_ko_dist2;
        }
        static uReal get_dist_scalar_default() {
            return uIntrLamDefaultKnockOutDistScale *
                   uIntrLamDefaultKnockInDist;
        }
        static enum uOptE get_opt_intr() { return uOpt_lam_knock_out; }
        static enum uOptE get_opt_dist_fpath() {
            return uOpt_lam_knock_out_dist_fpath;
        }
        static enum uOptE get_opt_dist_scalar() {
            return uOpt_lam_knock_out_dist;
        }
        static enum uOptE get_opt_mask_min() {
            return uOpt_lam_knock_out_bit_mask;
        }
        static const char* get_data_name() { return uIntrLamKnockOutDataName; }
        /**
         * Knock-out lamina interaction distance is transformed from "relative
         * to sphere surface" to instead "relative to origin" as this will be
         * easier to test computationally - WILL EXIT WITH ERROR CODE IF
         * CONSTRAINT IS TRIVIALLY NON-FEASIBLE!
         * @param dist - Output Euclidean "distance to origin" threshold,
         *  assumed user initialized to "distance to lamina surface", will be
         *  padded by node radius if all nodes are homogeneous
         * @param dist2 - Output squared Euclidean "distance to origin"
         *  threshold - only useful is nodes are homogeneous
         * @param sim - Parent simulation
         * @return uTRUE (will exit on error)
         */
        static uBool init_dist_scalars(uReal& dist,
                                       uReal& dist2,
                                       const sim_t& sim) {
            // Distance threshold must be positive real
            uAssert(dist > U_TO_REAL(0.0));
            /**
             * Transform distance threshold from "distance to surface" to
             * equivalent "distance to origin". This condition is easier to
             * test using simple dot product of fragment bead positions.
             * Specifically:
             *
             *  nucr - sqrt{dot{bc}} + fc2c - fbr > dsurf
             *  =>  nucr - dsurf + fc2c - fbr > sqrt{dot{bc}}
             *  =>  (max{nucr - dsurf + fc2c - fbr, 0})^2 > dot{bc}
             *  =>  (max{dorig + fc2c - fbr, 0})^2 > dot{bc}
             *
             *  Where 'nucr' is nuclear radius, 'bc' is tested bead 3D center
             *  position, 'fc2c' is maximum center-to-center length of segment
             *  defined by center of tested bead to first "active" bead of the
             *  constrained fragment (where first "active" bead is first bead
             *  of constrained fragment if tested bead index < first bead of
             *  constrained fragment index, else tested bead is *assumed* to
             *  be in constrained fragment and is considered the first
             *  "active" bead and therefore 'fc2c'=0), 'fbr' is the bead
             *  radius of the first "active" bead of the constrained fragment,
             *  'dsurf' is user input knock-out lamina interaction distance,
             *  'dorig' is knock-out lamina interaction distance relative to
             *  origin of coordinate system at (0, 0, 0).
             *
             *  Note, constraint is satisfied when tested bead index passes
             *  distance inequality test and is also the last bead in
             *  interacting fragment (assuming SEO growth).
             *
             *  Furthermore, if simulation is homogeneous radius polymers,
             *  then radial padding HAS ALREADY OCCURRED in this method and
             *  the corresponding test becomes:
             *
             *      (max{dorig_pad + fc2c, 0})^2 > dot{bc} : where 'fc2c'=0
             *          when tested bead is within interacting fragment and
             *          (assuming trivially infeasible constraints have been
             *          detected), becomes:
             *      => dorig_pad^2 > dot{bc} : when homogeneous and tested
             *          bead is in interacting fragment
             */
            // Compute 'dorig'
            dist = sim.get_max_nuclear_radius() - dist;
            // Check if we should pre-pad by bead radius
            if (sample_t::is_homg_radius) {
                // Compute 'dorig_pad'
                dist -= (U_TO_REAL(0.5) * sim.get_max_node_diameter());
            }
            // Check if constraint is trivially infeasible
            if (dist <= U_TO_REAL(0.0)) {
                // This is infeasible
                uLogf(
                    "Error: infeasible knock-out lamina interaction distance "
                    "encountered. Knock-out distance must be < (nuclear radius "
                    "- node radius). Exiting.\n");
                exit(uExitCode_error);
            }
            // Compute 'dorig^2' which is only useful if homogeneous in which
            // case this value is actually 'dorig_pad^2'
            dist2 = dist * dist;
            return uTRUE;
        }
    } init_intr_lam_ko_policy_t;

public:
    /**
     * Default constructor
     */
    uSisSeoSphereIntrLamSimLevelMixin() {
        // Require spherical nucleus
        U_STATIC_ASSERT(sim_t::is_sphere_nucleus);
        clear();
    }

    /**
     * Initializes simulation level (global) data
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        this->clear();
        // Load fragment spans
        uIntrLibConfig::load_frag(m_intr_lam_frags,
                                  config,
                                  uOpt_lam_frag,
                                  uFALSE /*no_fail*/,
                                  uIntrLamFragDataName,
                                  sim.get_max_total_num_nodes());
        uAssert(m_intr_lam_frags.n_cols >= 1);
        uAssert(m_intr_lam_frags.n_rows == uIntrLibPairIxNum);
        // Initialize knock-in interactions
        init_intrs_lam<init_intr_lam_kin_policy_t>(
            *this, config, m_intr_lam_frags, sim);
        // Initialize knock-out interactions
        init_intrs_lam<init_intr_lam_ko_policy_t>(
            *this, config, m_intr_lam_frags, sim);
    }

    /**
     * Modifies seed radii according to lamina interaction constraints
     * @param seed_radii - Matrix of size 3 rows x (number of loci) columns,
     *  each column is the (X, Y, Z) radii respectively of the bounding seed
     *  volume for that locus
     * @param sim - Outer simulation
     * @param config - User configuration
     */
    void update_seed_radii(uMatrix& seed_radii,
                           const sim_t& sim,
                           uSpConstConfig_t config) const {
        if (m_intr_lam_ko.is_empty()) {
            // Early out if no knock-out lamina interactions
            return;
        }
        const uSisIntrLociTable_t& loci_ko = this->get_intr_lam_loci_ko();
        if (loci_ko.empty()) {
            // Early out if no knock-out, loci-partitioned lamina interactions
            return;
        }
        uAssertPosEq(U_TO_UINT(loci_ko.size()), sim.get_num_loci());
        uAssertPosEq(U_TO_UINT(loci_ko.size()), U_TO_UINT(seed_radii.n_cols));
        uAssertPosEq(U_TO_UINT(uDim_num), U_TO_UINT(seed_radii.n_rows));
        const uMatSz_t num_loci = seed_radii.n_cols;
        uAssert(num_loci >= U_TO_MAT_SZ_T(1));
        for (uMatSz_t lid = 0; lid < num_loci; ++lid) {
            uAssertBounds(lid, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(loci_ko.size()));
            // Get node identifier for seed node
            const uUInt node0_nid =
                sim.get_growth_nid(U_TO_UINT(lid), U_TO_UINT(0) /*nof*/);
            // Handle to locus knock-out interactions
            const uSisIntrIndexVec_t& locus_ko = loci_ko[lid];
            const uMatSz_t num_lintr = U_TO_MAT_SZ_T(locus_ko.size());
            // Process each locus knock-out interaction
            for (uMatSz_t lintr_id = 0; lintr_id < num_lintr; ++lintr_id) {
                const uMatSz_t intr_id = locus_ko[lintr_id];
                uAssertBounds(intr_id, U_TO_UINT(0), m_intr_lam_ko.n_elem);
                const uMatSz_t frag_id = m_intr_lam_ko.at(intr_id);
                uAssertBounds(
                    frag_id, U_TO_MAT_SZ_T(0), m_intr_lam_frags.n_cols);
                uAssertPosEq(m_intr_lam_frags.n_rows, uIntrLibPairIxNum);
                // Get first node identifier of knock-out fragment
                const uUInt frag_nid_lo =
                    m_intr_lam_frags.at(uIntrLibPairIxMin, frag_id);
                uAssert(frag_nid_lo >= node0_nid);
                uAssert(sim.get_growth_lid(frag_nid_lo) == U_TO_UINT(lid));
                const uReal fc2c = sim.get_max_c2c_len(node0_nid, frag_nid_lo);
                // Get constrained interaction distance relative to origin;
                // for heterogeneous case, this value must be padded by radius
                // of last node of interacting fragment and the padded result
                // MAY BE NEGATIVE
                const uReal dorig_pad =
                    (sim_t::is_homg_radius)
                        ? sim.get_intr_lam_ko_dist(intr_id)
                        : (sim.get_intr_lam_ko_dist(intr_id) -
                           sim.get_node_radius(frag_nid_lo));
                // Compute maximum allowed seed radius that would enable this
                // knock-out constraint to be satisfied
                const uReal max_radius = dorig_pad + fc2c;
                if (max_radius <= U_TO_REAL(0.0)) {
                    // This is infeasible
                    uLogf(
                        "Error: infeasible knock-out lamina interaction "
                        "distance encountered. Exiting.\n");
                    exit(uExitCode_error);
                }
                // Update allowed nuclear radii along each axis
                for (uMatSz_t axis = U_TO_MAT_SZ_T(uDim_X);
                     axis <= U_TO_MAT_SZ_T(uDim_Z);
                     ++axis) {
                    const uReal current_radius = seed_radii.at(axis, lid);
                    seed_radii.at(axis, lid) =
                        std::min(current_radius, max_radius);
                    uAssert(seed_radii.at(axis, lid) > U_TO_REAL(0.0));
                }
            }
        }
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_intr_lam_frags.clear();
        m_intr_lam_kin.clear();
        m_intr_lam_ko.clear();
        m_intr_lam_kin_dist.clear();
        m_intr_lam_kin_dist2.clear();
        m_intr_lam_ko_dist.clear();
        m_intr_lam_ko_dist2.clear();
        intr_lam_batch_util_t::clear(m_intr_lam_loci_tables);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @return Const handle to interaction fragments
     */
    inline const uUIMatrix& get_intr_lam_frags() const {
        return m_intr_lam_frags;
    }

    /**
     * @return Const handle to knock-in interactions
     */
    inline const uUIVecCol& get_intr_lam_kin() const { return m_intr_lam_kin; }

    /**
     * @return Const handle to knock-out interactions
     */
    inline const uUIVecCol& get_intr_lam_ko() const { return m_intr_lam_ko; }

    /**
     * @param i - Knock-in interaction index
     * @return [Squared] knock-in Euclidean distance threshold relative to
     *  origin
     */
    inline uReal get_intr_lam_kin_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_kin_dist.n_elem);
        return m_intr_lam_kin_dist.at(i);
    }
    inline uReal get_intr_lam_kin_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_kin_dist2.n_elem);
        return m_intr_lam_kin_dist2.at(i);
    }

    /**
     * @param i - Knock-out interaction index
     * @return [Squared] knock-out Euclidean distance threshold relative to
     *  origin
     */
    inline uReal get_intr_lam_ko_dist(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_ko_dist.n_elem);
        return m_intr_lam_ko_dist.at(i);
    }
    inline uReal get_intr_lam_ko_dist2(const uUInt i) const {
        uAssertBounds(i, U_TO_UINT(0), m_intr_lam_ko_dist2.n_elem);
        return m_intr_lam_ko_dist2.at(i);
    }

    /**
     * @return Lamina knock-in interaction failure budget
     */
    inline uUInt get_intr_lam_kin_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Lamina knock-out interaction failure budget
     */
    inline uUInt get_intr_lam_ko_max_fail() const { return U_TO_UINT(0); }

    /**
     * @return Handle to loci interaction tables
     */
    inline const uSisIntrLamLociTables_t& get_intr_lam_loci_tables() const {
        return m_intr_lam_loci_tables;
    }

    /**
     * @return Table of lamina knock-in interactions
     */
    inline const uSisIntrLociTable_t& get_intr_lam_loci_kin() const {
        return m_intr_lam_loci_tables.loci_kin;
    }

    /**
     * @return Table of lamina knock-out interactions
     */
    inline const uSisIntrLociTable_t& get_intr_lam_loci_ko() const {
        return m_intr_lam_loci_tables.loci_ko;
    }

    // Export interface

    /**
     * Copies lamina interaction fragments to 'out'
     */
    void get_intr_lam_frags(uUIMatrix& out) const { out = m_intr_lam_frags; }

    /**
     * Copies knock-in lamina interactions to 'out'
     */
    void get_intr_lam_kin(uUIVecCol& out) const { out = m_intr_lam_kin; }

    /**
     * Copies knock-out lamina interactions to 'out'
     */
    void get_intr_lam_ko(uUIVecCol& out) const { out = m_intr_lam_ko; }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Extracts masked lamina interaction data
     */
    static void apply_intrs_lam_mask(const uBoolVecCol& mask,
                                     uUIVecCol& intr,
                                     uVecCol& dist,
                                     uVecCol& dist2) {
        uAssert(mask.n_elem == intr.n_elem);
        const uMatSz_t n_intr = uMatrixUtils::sum(mask);
        uAssertBoundsInc(n_intr, U_TO_MAT_SZ_T(0), intr.n_elem);
        uUIVecCol temp_intr;
        uVecCol temp_dist;
        uVecCol temp_dist2;
        // Extract masked data
        if (n_intr > U_TO_UINT(0)) {
            temp_intr.set_size(n_intr);
            temp_dist.set_size(n_intr);
            temp_dist2.set_size(n_intr);
            uUInt dst = U_TO_UINT(0);
            for (uMatSz_t src = 0; src < mask.n_elem; ++src) {
                const uBool bit = mask.at(src);
                if (bit) {
                    uAssertBounds(dst, U_TO_UINT(0), temp_intr.n_elem);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist.n_elem);
                    uAssertBounds(dst, U_TO_UINT(0), temp_dist2.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), intr.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), dist.n_elem);
                    uAssertBounds(src, U_TO_UINT(0), dist2.n_elem);
                    temp_intr.at(dst) = intr.at(src);
                    temp_intr.at(dst) = intr.at(src);
                    temp_dist.at(dst) = dist.at(src);
                    temp_dist2.at(dst) = dist2.at(src);
                    ++dst;
                }
            }
            uAssert(dst == n_intr);
        }
        // Overwrite buffers
        uAssert(temp_intr.n_elem <= intr.n_elem);
        uAssert(temp_dist.n_elem <= dist.n_elem);
        uAssert(temp_dist2.n_elem <= dist2.n_elem);
        uAssert(temp_dist2.n_elem == temp_dist.n_elem);
        uAssert(temp_intr.n_elem == temp_dist.n_elem);
        intr = temp_intr;
        dist = temp_dist;
        dist2 = temp_dist2;
    }

    /**
     * Initialize lamina interaction
     */
    template <typename t_init>
    static void init_intrs_lam(uSisSeoSphereIntrLamSimLevelMixin& this_,
                               uSpConstConfig_t config,
                               const uUIMatrix& frags,
                               const sim_t& sim) {
        const char* const data_name = t_init::get_data_name();
        uAssert(data_name);
        // This method should not be called if no fragments exist
        uAssert(frags.n_cols >= 1);
        uAssert(frags.n_rows == uIntrLibPairIxNum);

        // Load interactions
        uUIVecCol& intr = t_init::get_intr(this_);
        if (!uIntrLibConfig::load_index(intr,
                                        config,
                                        t_init::get_opt_intr(),
                                        uFALSE /*no_fail*/,
                                        data_name)) {
            uLogf("Warning: no %s lamina interactions loaded.\n", data_name);
            intr.clear();
            return;
        }
        uAssert(intr.n_elem >= 1);
        uIntrLibInit::check_bounds_exit_on_fail(intr, frags);

        // Initialize interaction distances (with optional padding)
        uVecCol& dist = t_init::get_dist(this_);
        uVecCol& dist2 = t_init::get_dist2(this_);
        uIntrLibConfig::load_dist(dist,
                                  intr.n_elem,
                                  config,
                                  t_init::get_opt_dist_fpath(),
                                  t_init::get_opt_dist_scalar(),
                                  t_init::get_dist_scalar_default(),
                                  data_name);
        uAssertPosEq(dist.n_elem, intr.n_elem);
        dist2.set_size(dist.n_elem);
        uBoolVecCol non_trivial_mask;
        non_trivial_mask.set_size(dist.n_elem);
        non_trivial_mask.fill(uTRUE);
        for (uMatSz_t i = 0; i < dist.n_elem; ++i) {
            non_trivial_mask.at(i) =
                t_init::init_dist_scalars(dist.at(i), dist2.at(i), sim);
        }

        // Warn if trivial (always satisfied) interactions found
        const uMatSz_t num_non_trivial = uMatrixUtils::sum(non_trivial_mask);
        uAssert(num_non_trivial <= non_trivial_mask.n_elem);
        const uBool has_trivial = num_non_trivial != non_trivial_mask.n_elem;
        if (has_trivial) {
            const uMatSz_t num_trivial =
                non_trivial_mask.n_elem - num_non_trivial;
            uLogf(
                "Warning: %d trivial %s lamina interactions found (will be "
                "ignored).\n",
                (int)num_trivial,
                data_name);
        }

        // Read interactions mask
        uBoolVecCol mask;
        uIntrLibConfig::load_mask(
            mask, intr.n_elem, config, t_init::get_opt_mask_min(), data_name);
        uAssert(mask.n_elem == intr.n_elem);

        // Apply masking
        if (has_trivial) {
            uAssertPosEq(mask.n_elem, non_trivial_mask.n_elem);
            mask %= non_trivial_mask;
        }
        apply_intrs_lam_mask(mask, intr, dist, dist2);

        // Partition interactions by loci
        intr_lam_batch_util_t::partition_by_loci(
            t_init::get_intr_loci(this_), intr, frags, sim);

        // Compute mean interaction constraint distance (avoid divide-by-zero)
        double mean_dist = 0.0;
        if (intr.n_elem > U_TO_MAT_SZ_T(0)) {
            mean_dist = (double)uMatrixUtils::mean(dist);
        }
        // Report initialized interaction state
        uAssert(mean_dist >= 0.0);
        uAssert(intr.n_elem == dist.n_elem);
        uAssert(intr.n_elem == dist2.n_elem);
        uLogf(
            "Initialized %d %s lamina interaction constraints.\n\t-<%s dist to "
            "origin> (may be padded): %f.\n",
            (int)intr.n_elem,
            data_name,
            data_name,
            mean_dist);
    }

    /**
     * Fragment spans
     */
    uUIMatrix m_intr_lam_frags;

    /**
     * Knock-in interactions, indices over frags matrix cols
     */
    uUIVecCol m_intr_lam_kin;

    /**
     * Knock-out interactions, indices over frags matrix cols
     */
    uUIVecCol m_intr_lam_ko;

    /**
     * THIS VALUE IS DISTANCE-TO-ORIGIN THRESHOLD (transformed from user input
     * distance-to-surface threshold). At least one node in lamina interacting
     * fragment must be at least this far from origin (0,0,0) assuming a
     * spherical nucleus.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_kin_dist;

    /**
     * Squared distance lamina proximity threshold (relative to origin)
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_kin_dist2;

    /**
     * THIS VALUE IS DISTANCE-TO-ORIGIN THRESHOLD (transformed from user input
     * distance-to-surface threshold). All nodes in lamina interacting
     * fragment must be less than this distance from the origin at (0,0,0)
     * assuming a spherical nucleus.
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_ko_dist;

    /**
     * Squared knock-out distance threshold (relative to origin).
     *
     * NOTE: THIS VALUE IS PADDED BY NODE RADIUS AT INITIALIZATION ONLY IF
     * NODES ARE HOMOGENEOUS.
     */
    uVecCol m_intr_lam_ko_dist2;

    /**
     * Collection of lamina interactions partitioned by loci
     */
    uSisIntrLamLociTables_t m_intr_lam_loci_tables;
};

#endif  // uSisSeoSphereIntrLamSimLevelMixin_h
