//****************************************************************************
// uSisCanonicalTrialRunnerMixin.h
//****************************************************************************

/**
 * Trial Runners must implement run_trial(). This method defines the main
 * update loop during a single trial of the the simulation. A "trial" is
 * defined as a single attempt at growing all target samples from end to end.
 * At the end of a trial, the simulation object will move all completed
 * samples to a completed pool and then run an entirely new trial for the
 * failed samples.
 *
 * In addition, the trial runner must keep track of the number of growth
 * calls (seed + step) for each sample.
 *
 * The canonical trial runner uses a basic trial strategy:
 *  - seed all samples with initial nodes
 *  - while last growth step not yet reached:
 *      - incrementally grow all active samples
 *      - after each growth step, check if any quality control methods (e.g.
 *        resampling, rejection control) need to be applied
 */

#ifndef uSisCanonicalTrialRunnerMixin_h
#define uSisCanonicalTrialRunnerMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uBitsetHandle.h"
#include "uConfig.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// uSisCanonicalTrialRunnerMixin
//****************************************************************************

template <
    // The core simulation structures
    typename t_SisGlue,
    // Defines methods batch_grow_1st(), batch_grow_2nd(), and batch_grow_nth()
    // which may have parallel or serial implementations
    typename t_CnclGrower>
class uSisCanonicalTrialRunnerMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * WARNING: MAY ONLY BE CALLED FROM MAIN THREAD
     * Runs a single trial of the simulation. Assumes simulation has been
     * (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation
     */
    void run_trial(std::vector<sample_t>& trial_samples,
                   uVecCol& trial_log_weights,
                   uWrappedBitset& wrapped_trial_status,
                   sim_t& sim) {
        uAssert(m_num_completed_grow_steps_per_sample == 0);

        // Special case: seed initial nodes
        m_grower.batch_grow_1st(wrapped_trial_status,
                                trial_samples,
                                trial_log_weights,
                                *this,
                                sim,
                                1 /* target_num_grow_steps */
                                U_MAIN_THREAD_ID_ARG);

        // Let mixins know that we've completed the seed step
        sim.on_seed_finish();

        // Special case: grow second nodes
        if (wrapped_trial_status.any()) {

            m_grower.batch_grow_2nd(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_MAIN_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_MAIN_THREAD_ID_ARG);
        }

        // Grow all remaining nodes
        while (wrapped_trial_status.any()) {
            // Grow to next step
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_MAIN_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_MAIN_THREAD_ID_ARG);
        }
    }

    /**
     * Runs a single trial of the simulation with the growth count capped at
     * parameter target grow steps. Will stop trial once target grow count has
     * been reached. Assumes simulation has been (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that each sample must successfully complete
     */
    void run_trial_from_start_until(std::vector<sample_t>& trial_samples,
                                    uVecCol& trial_log_weights,
                                    uWrappedBitset& wrapped_trial_status,
                                    sim_t& sim,
                                    const uUInt target_num_grow_steps
                                        U_THREAD_ID_PARAM) {
        uAssert(m_num_completed_grow_steps_per_sample == 0);
        // Assume not running from template
        uAssert(!sim.is_run_from_template());
        // Assume that seeding is required
        uAssert(target_num_grow_steps > 1);

        // Special case: seed initial nodes
        m_grower.batch_grow_1st(wrapped_trial_status,
                                trial_samples,
                                trial_log_weights,
                                *this,
                                sim,
                                1 /* target_num_grow_steps */
                                U_THREAD_ID_ARG);

        // Let mixins know that we've completed the seed step
        sim.on_seed_finish();

        // Special case: grow second nodes
        if (wrapped_trial_status.any() &&
            (this->get_num_completed_grow_steps_per_sample() <
             target_num_grow_steps)) {
            // Grow to next step
            m_grower.batch_grow_2nd(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);
        }

        // Grow all remaining nodes
        while (wrapped_trial_status.any() &&
               (this->get_num_completed_grow_steps_per_sample() <
                target_num_grow_steps)) {

            // Grow to next step
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);
        }

        // Verify target growth count was reached
        uAssert((this->get_num_completed_grow_steps_per_sample() ==
                 target_num_grow_steps) ||
                !wrapped_trial_status.any());

        // @HACK - Signal that we are done growing non-dead chains by setting
        // all status bits to false
        // @TODO - need to be able to flag which ones actually
        // could be grown further if wanted
        wrapped_trial_status.reset();
    }

    /**
     * ASSUMES TEMPLATE SAMPLE HAS BEEN SEEDED!
     * Runs a single trial of the simulation using template sample as the
     * initial starting state for each sample to be grown. Will stop trial
     * once target grow count has been reached. Assumes simulation has been
     * (re)-initialized.
     * @param trial_samples - Output vector of samples for this trial. Must be
     *  same size as trial_log_weights.
     * @param trial_log_weights - Output vector of log weights at each trial
     *  sample. Must be same size as trial_samples.
     * @param wrapped_trial_status - Bitset with 1-bit per sample. If 1, then
     *  sample still needs growing. If 0, sample can no longer be grown either
     *  because it completed successfully or failed (got stuck). Must be same
     *  size as trial_samples.
     * @param sim - Parent simulation, also contains template sample info
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that each sample must successfully complete (includes template
     *  grow count as well)
     */
    void run_trial_from_template_until(std::vector<sample_t>& trial_samples,
                                       uVecCol& trial_log_weights,
                                       uWrappedBitset& wrapped_trial_status,
                                       sim_t& sim,
                                       const uUInt target_num_grow_steps
                                           U_THREAD_ID_PARAM) {
        // Verify we are running from template
        uAssert(sim.is_run_from_template());
        // Verify template has been seeded
        uAssert(sim.get_template_num_grow_steps() > 1);
        // Verify target grow count is greater than template count
        uAssert(sim.get_template_num_grow_steps() < target_num_grow_steps);
        // Template cannot be a finished sample
        uAssert(sim.get_template_num_grow_steps() <
                sim.get_max_num_grow_steps(sim));

        const size_t n_trial_samples = trial_samples.size();

        // Verify parallel sets
        uAssertPosEq(n_trial_samples, trial_log_weights.size());
        uAssert(n_trial_samples == wrapped_trial_status.size());

        // Counter for iterating over each sample
        size_t i = 0;

        // Store return status of growth operations
        bool status = false;

        // Initialize samples using template
        for (i = 0; i < n_trial_samples; ++i) {
            sample_t& sample(trial_samples[i]);
            sample = sim.get_template_sample();
            uReal& log_weight(trial_log_weights[U_TO_MAT_SZ_T(i)]);
            log_weight = sim.get_template_log_weight();
            uAssert(wrapped_trial_status.test(i));
        }

        // @TODO - is on_init_from_template() method needed?
        // normally, we would call on_seed_finish() or
        // reset_mixins_for_regrowth() or something like that here.
        m_num_completed_grow_steps_per_sample =
            sim.get_template_num_grow_steps();

        // Special case: grow second nodes
        if ((this->get_num_completed_grow_steps_per_sample() == 1) &&
            (this->get_num_completed_grow_steps_per_sample() <
             target_num_grow_steps) &&
            wrapped_trial_status.any()) {
            // Grow to next step
            m_grower.batch_grow_2nd(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);
        }

        // Grow all remaining nodes
        while (wrapped_trial_status.any() &&
               (this->get_num_completed_grow_steps_per_sample() <
                target_num_grow_steps)) {

            // Grow to next step
            m_grower.batch_grow_nth(
                wrapped_trial_status,
                trial_samples,
                trial_log_weights,
                *this,
                sim,
                // target_num_grow_steps
                m_num_completed_grow_steps_per_sample + 1,
                m_num_completed_grow_steps_per_sample U_THREAD_ID_ARG);

            // Let mixins know that we've completed a grow phase
            sim.on_grow_phase_finish(wrapped_trial_status U_THREAD_ID_ARG);
        }

        // Verify target growth count was reached
        uAssert((this->get_num_completed_grow_steps_per_sample() ==
                 target_num_grow_steps) ||
                !wrapped_trial_status.any());

        // Signal that we are done growing non-dead chains by setting
        // all status bits to false
        // @TODO - need to be able to flag which ones actually
        // could be grown further if wanted
        wrapped_trial_status.reset();
    }

    /**
     * Initializes trial runner
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    inline void init(sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
    }

    /**
     * Clear trial runner to default state
     */
    inline void clear() { m_num_completed_grow_steps_per_sample = 0; }

    /**
     * Preps for next trial
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    inline void reset_for_next_trial(const sim_t& sim,
                                     uSpConstConfig_t config
                                         U_THREAD_ID_PARAM) {
        clear();
    }

    /**
     * @return The number of seed() + grow_*() calls for the longest
     *  active sample within the trial
     */
    inline uUInt get_num_completed_grow_steps_per_sample() const {
        return m_num_completed_grow_steps_per_sample;
    }

    /**
     * Called after initial chromatin node(s) have been placed at all samples.
     * Updates count of grow steps.
     * @param sim - parent simulation containing global sample data
     */
    inline void on_seed_finish(const sim_t& sim) {
        m_num_completed_grow_steps_per_sample = 1;
    }

    /**
     * Called after a set of incremental chromatin nodes have been appended to
     * each active sample. Updates count of grow steps.
     * @param sim - parent simulation containing global sample data
     */
    inline void on_grow_phase_finish(const sim_t& sim U_THREAD_ID_PARAM) {
        ++m_num_completed_grow_steps_per_sample;
    }

    /**
     * Will regrow a sample using seed() and grow_*() calls. Meant to work
     * with rejection control.
     * @WARNING - Assumes sample will need at least one seed() and one
     *  grow_*() call. @TODO - fix this assumption!
     * @param out_sample - The sample to be regrown
     * @param out_log_weight - The final log weight of the sample
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that must be successfully completed
     * @param sample_id - The sample identifier (index into samples array)
     * @return final status of last grow_*() call -> TRUE if sample needs
     *  to continue growing, FALSE if finished growing. We should not return
     *  any dead chains. Currently, method will infinite loop until chain is
     *  no longer dead.
     */
    bool regrow_force_from_start(sample_t& out_sample,
                                 uReal& out_log_weight,
                                 sim_t& sim,
                                 const uUInt target_num_grow_steps,
                                 const uUInt sample_id U_THREAD_ID_PARAM) {
        // If this assert trips, client should be calling
        // regrow_attempt_from_template() method instead
        uAssert(!sim.is_run_from_template());
        // We require at least a single grow_*() call
        // @TODO - fix this assumption!
        uAssert(target_num_grow_steps > 1);
        // Same constraint - we require at least a single
        // grow_*() beyond seeding
        uAssert(sim.get_max_total_num_nodes() > 1);
        // Assume target step count is reasonable
        uAssert(target_num_grow_steps <= sim.get_max_num_grow_steps(sim));

        // Used for storing status after each grow step
        bool status = false;

        // Keeps track of number of completed grow steps
        // (seed() + grow_*) calls)
        uUInt num_grow_steps = 0;

        // @TODO - does this need to be capped?
        while (num_grow_steps < target_num_grow_steps) {
            //////////////////////////////////////////////////////
            // Reset sample
            //////////////////////////////////////////////////////

            uAssert(num_grow_steps == 0);

            // Reset any simulation level mixins that need it
            // @Warning - If they do need to be reset, they are probably not
            // thread safe!
            sim.reset_mixins_for_regrowth(out_sample, 0);

            // Reset sample weight
            out_log_weight = U_TO_REAL(0.0);

            // Reset the sample
            sim.reset_sample(out_sample U_THREAD_ID_ARG);

            //////////////////////////////////////////////////////
            // Seed sample
            //////////////////////////////////////////////////////

            // Seed initial node
            status = out_sample.seed(out_log_weight, sim U_THREAD_ID_ARG);
            // Assuming seed state cannot fail!
            uAssert(!out_sample.is_dead());

            // Let simulation/mixins know that sample was seeded
            sim.on_reseed_finish(out_sample, out_log_weight, status);

            // Set grow step to 1 -> seed successful
            num_grow_steps = 1;

            //////////////////////////////////////////////////////
            // Grow second node
            //////////////////////////////////////////////////////

            if (num_grow_steps < target_num_grow_steps) {
                uAssert(status);
                // Perform growth step
                status = out_sample.grow_2nd(
                    out_log_weight, num_grow_steps, sim U_THREAD_ID_ARG);

                // Check if dead end reached
                if (out_sample.is_dead()) {
                    // Restart dead sample
                    uAssert(!status);
                    num_grow_steps = 0;
                    continue;
                }

                num_grow_steps = 2;

                // Let simulation/mixins know that sample has completed a
                // grow_*() call
                sim.on_regrow_step_finish(out_sample,
                                          out_log_weight,
                                          status,
                                          num_grow_steps,
                                          sample_id U_THREAD_ID_ARG);

                // Check if sample was culled by mixins
                if (out_sample.is_dead()) {
                    // Restart dead sample
                    uAssert(!status);
                    num_grow_steps = 0;
                    continue;
                }
            }

            //////////////////////////////////////////////////////
            // Grow remaining nodes
            //////////////////////////////////////////////////////

            while (num_grow_steps < target_num_grow_steps) {
                // @WARNING - To simplify code and avoid extraneous
                // conditionals, am assuming client is passing a reasonable
                // value for target_num_grow_steps such that no more
                // grow_*() calls than necessary are required. In other
                // words, no overgrowth will occur.
                uAssert(status);
                uAssert(!out_sample.is_dead());

                // Perform growth step
                status = out_sample.grow_nth(
                    out_log_weight, num_grow_steps, sim U_THREAD_ID_ARG);

                // Check if dead end reached
                if (out_sample.is_dead()) {
                    // Restart dead sample
                    uAssert(!status);
                    num_grow_steps = 0;
                    break;
                }

                ++num_grow_steps;

                // Let simulation/mixins know that sample has completed a
                // grow_*() call
                sim.on_regrow_step_finish(out_sample,
                                          out_log_weight,
                                          status,
                                          num_grow_steps,
                                          sample_id U_THREAD_ID_ARG);

                // Check if sample was culled by mixins
                if (out_sample.is_dead()) {
                    // Restart dead sample
                    uAssert(!status);
                    num_grow_steps = 0;
                    break;
                }
            }  // while grow_*()
        }      // end while init, seed, and grow

        uAssert(!out_sample.is_dead());
        return status;
    }  // end force_regrow_from_start

    /**
     * Will perform a single attempt to regrow a sample using a given starting
     *  template. Meant to work with partial rejection control
     * @WARNING - Assumes no seeding is required by the sample
     * @param out_sample - The sample to attempt regrowth
     * @param out_log_weight - The log weight of the sample after regrowth
     *  attempt, will be set to highly negative value if growth fails.
     * @param sim - the parent simulation
     * @param target_num_grow_steps - the total number of seed() + grow_*()
     *  calls that will be attempted for successful completion. Note, no
     *  actual seed() calls will be made as template sample is assumed to have
     *  already been seeded.
     * @param sample_id - The sample identifier (index into samples array)
     * @param template_sample - The template sample state to start the
     *  regrowth procedure from
     * @param template_num_grow_steps - The total number of seed() +
     *  grow_*() calls assumed to have been comppleted by the template
     *  sample.
     * @param init_log_weight - The out_log_weight is initialized to this
     *  value prior to regrowth
     * @return TRUE if regrowth attempt successful and sample still needs
     *  growing. FALSE if regrowth attempt successful and sample no longer
     *  needs growing or if regrowth attempt unsuccessful (to distinguish
     *  between these two cases, check status using sample_t::is_dead())
     */
    bool regrow_attempt_from_template(sample_t& out_sample,
                                      uReal& out_log_weight,
                                      sim_t& sim,
                                      const uUInt target_num_grow_steps,
                                      const uUInt sample_id,
                                      const sample_t& template_sample,
                                      const uUInt template_num_grow_steps,
                                      const uReal init_log_weight
                                          U_THREAD_ID_PARAM) {
        // We require at least a single grow_*()
        uAssert(target_num_grow_steps > 1);
        // Same constraint - we require at least a single
        // grow_*() beyond seeding
        uAssert(sim.get_max_total_num_nodes() > 1);
        // We require that template has been seeded
        uAssert(template_num_grow_steps > 1);
        // We require that target exceeds template
        uAssert(template_num_grow_steps < target_num_grow_steps);
        // We require that template sample is valid
        uAssert(!template_sample.is_dead());
        // Assume target step count is reasonable
        uAssert(target_num_grow_steps <= sim.get_max_num_grow_steps(sim));

        // Used for storing status after each grow step
        bool status = true;

        // Reset any mixins that need it
        // @WARNING - Probably not thread safe if any sim level mixins need
        // resetting
        sim.reset_mixins_for_regrowth(out_sample, template_num_grow_steps);

        // Overwrite sample
        out_sample = template_sample;

        // Update sample weight
        out_log_weight = init_log_weight;

        // Update grow step count
        uUInt num_grow_steps = template_num_grow_steps;

        // Special case: grow second node
        if ((num_grow_steps == 1) && (num_grow_steps < target_num_grow_steps)) {
            // Perform growth step
            status = out_sample.grow_2nd(
                out_log_weight, num_grow_steps, sim U_THREAD_ID_ARG);

            // Check if dead end reached
            if (out_sample.is_dead()) {
                uAssert(!status);
                return status;
            }

            num_grow_steps = 2;
        }

        // Grow remaining nodes
        while (num_grow_steps < target_num_grow_steps) {
            uAssert(status);

            // Perform growth step
            status = out_sample.grow_nth(
                out_log_weight, num_grow_steps, sim U_THREAD_ID_ARG);

            // Check if dead end reached
            if (out_sample.is_dead()) {
                uAssert(!status);
                break;
            }

            ++num_grow_steps;

            // Let simulation/mixins know that sample has completed a
            // grow_*() call
            sim.on_regrow_step_finish(out_sample,
                                      out_log_weight,
                                      status,
                                      num_grow_steps,
                                      sample_id U_THREAD_ID_ARG);

            // Check if sample was culled by mixins
            if (out_sample.is_dead()) {
                uAssert(!status);
                break;
            }
        }

        return status;
    }  // end attempt_regrow_from_template

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE

private:
    /**
     * Seeds blank sample
     * @param target_num_grow_steps - The number of grow steps to complete
     *  (ignored by canonical trial runner)
     * @param sample - The sample to be seeded
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check log_weight to see if dead)
     */
    inline bool grow_1st(const uUInt target_num_grow_steps_unused,
                         sample_t& sample,
                         uReal& log_weight,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(target_num_grow_steps_unused == 1);
        uAssert(log_weight == U_TO_REAL(0.0));
        return sample.seed(log_weight, sim U_THREAD_ID_ARG);
    }

    /**
     * Grows sample by a single grow step - assumes sample has been seeded
     * @param target_num_grow_steps - The number of grow steps to complete
     *  (ignored by canonical trial runner)
     * @param sample - The sample to grow by a single grow step call
     * @param sample_num_grow_steps - The number of grow steps completed by
     *  parameter sample
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check log_weight to see if dead)
     */
    inline bool grow_2nd(const uUInt target_num_grow_steps_unused,
                         sample_t& sample,
                         const uUInt sample_num_grow_steps,
                         uReal& log_weight,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(sample_num_grow_steps == m_num_completed_grow_steps_per_sample);
        uAssert(target_num_grow_steps_unused ==
                (m_num_completed_grow_steps_per_sample + 1));
        uAssert(target_num_grow_steps_unused == 2);
        return sample.grow_2nd(
            log_weight, sample_num_grow_steps, sim U_THREAD_ID_ARG);
    }

    /**
     * Grows sample by a single grow step - assumes sample has been seeded
     * and also has completed second growth phase
     * @param target_num_grow_steps - The number of grow steps to complete
     *  (ignored by canonical trial runner)
     * @param sample - The sample to grow by a single grow step call
     * @param sample_num_grow_steps - The number of grow steps completed by
     *  parameter sample
     * @param log_weight - The output log weight
     * @param sim - The parent simulation
     * @return True if sample should continue growing, False if sample has
     *  completed or is dead (check log_weight to see if dead)
     */
    inline bool grow_nth(const uUInt target_num_grow_steps_unused,
                         sample_t& sample,
                         const uUInt sample_num_grow_steps,
                         uReal& log_weight,
                         const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(sample_num_grow_steps == m_num_completed_grow_steps_per_sample);
        uAssert(target_num_grow_steps_unused ==
                (m_num_completed_grow_steps_per_sample + 1));
        return sample.grow_nth(
            log_weight, sample_num_grow_steps, sim U_THREAD_ID_ARG);
    }

    typedef t_CnclGrower uCnclGrower;

    // Allow access to internal growth methods
    friend uCnclGrower;

    /**
     * Used for growing a batch of samples
     */
    uCnclGrower m_grower;

    /**
     * Counter to keep track of number of grow steps completed. This
     * is the sum of seed() + grow_*() calls within a single trial and is
     * reset between trials.
     */
    uUInt m_num_completed_grow_steps_per_sample;
};

#endif  // uSisCanonicalTrialRunnerMixin_h
