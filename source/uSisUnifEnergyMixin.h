//****************************************************************************
// uSisUnifEnergyMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Unif: Uniform sampling
 *
 * Samples are considered to all be equal energy, therefore every candidate
 *  (which satisfies hard constraints) has equal probability of being chosen.
 */

#ifndef uSisUnifEnergyMixin_h
#define uSisUnifEnergyMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisNullSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisUnifEnergySimLevelMixin : public uSisNullSimLevelMixin<t_SisGlue> {};

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisUnifEnergyMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Energy selection policy for 2nd growth phase
     */
    inline uUInt energy_select_2nd(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        return energy_select_nth(sample,
                                 log_weight,
                                 parent_id,
                                 candidate_positions,
                                 candidate_radius,
                                 is_legal,
                                 num_legal,
                                 sim U_THREAD_ID_ARG);
    }

    /**
     * THIS INTERFACE METHOD IS INTENDED TO BE CALLED AFTER FIRST AND SECOND
     * NODES OF A CHAIN HAVE BEEN SEQUENTIALLY GROWN. For uniform sampling,
     * this has no effect, but for other energy mixins, it's important to call
     * the corresponding energy sampler (energy_select_2nd, etc).
     *
     * @WARNING - ASSUMES AT LEAST 1 LEGAL CANDIDATE!
     *
     * Uniformly selects among legal candidates (does not have energy bias).
     * Will also update log_weight to account for sampling bias.
     *
     * @param sample - sample containing this mixin
     * @param log_weight - the current and output log weight of the sample
     * @param parent_id - index of parent node within sample positions matrix
     *  (note, this is not same as candidate positions matrix)
     * @param candidate_positions - matrix of row-major candidate positions
     * @param candidate_radius - radius of candidate node
     * @param is_legal - [0, 1] vector : 1 if candidate is legal for
     *  selection, 0 otherwise
     * @param num_legal - the number of 1s in legal_candidates(must be > 0)
     * @param sim - parent simulation containing global sample data
     * @return index of selected candidate.
     */
    inline uUInt energy_select_nth(const sample_t& sample,
                                   uReal& log_weight,
                                   const uUInt parent_id,
                                   const uMatrix& candidate_positions,
                                   const uReal candidate_radius,
                                   const uBoolVecCol& is_legal,
                                   const uUInt num_legal,
                                   const sim_t& sim U_THREAD_ID_PARAM) {
        uAssert(num_legal >= 1);
        // Update weight
        log_weight += log(U_TO_REAL(num_legal));
        // Uniformly select a candidate
        return uSisUtils::runif_select_legal_growth_candidate(
            is_legal, num_legal U_THREAD_ID_ARG);
    }
};

#endif  // uSisUnifEnergyMixin_h
