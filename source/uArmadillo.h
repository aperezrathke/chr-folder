//****************************************************************************
// uArmadillo.h
//****************************************************************************

/**
 * @brief Armadillo include which incorporates build flags
 */

#ifndef uArmadillo_h
#define uArmadillo_h

#include "uBuild.h"
#include "uTypes.h"

// Check if we don't want armadillo debug information (e.g. during release)
// Also, define NDEBUG before including this external library just in case it
// still has internal asserts that aren't disabled by ARMA_NO_DEBUG.
#ifndef U_BUILD_ARMADILLO_DEBUG
#   ifndef ARMA_NO_DEBUG
#       define ARMA_NO_DEBUG // tell armadillo to avoid debug checks
#   endif
#   ifndef NDEBUG // for good measure, disable asserts for this unit
#       define U_NDEBUG_TRANSIENT // this means enable asserts after include
#       define NDEBUG
#   endif // NDEBUG
#   include <armadillo>
#   ifdef U_NDEBUG_TRANSIENT // NDEBUG wasn't defined before, so undef it
#       undef U_NDEBUG_TRANSIENT
#       undef NDEBUG
#   endif // U_NDEBUG_TRANSIENT
#else
#   include <armadillo> // else, lets include extra debug checks
#endif // U_BUILD_ARMADILLO_DEBUG

// Wrap namespace
#define uMatrixUtils arma

/**
 * Matrix type
 */
typedef uMatrixUtils::Mat<uReal> uMatrix;

/**
 * Matrix for unsigned integers
 */
typedef uMatrixUtils::Mat<uUInt> uUIMatrix;

/**
 * Column vector type
 */
typedef uMatrixUtils::Col<uReal> uVecCol;

/**
 * Row vector type
 */
typedef uMatrixUtils::Row<uReal> uVecRow;

/**
 * Column vector for unsigned integers
 */
typedef uMatrixUtils::Col<uUInt> uUIVecCol;

/**
 * Column vector for small unsigned integers
 */
typedef uMatrixUtils::Col<uUInt16_t> uUI16VecCol;

/**
 * Column vector for element identifiers
 */
typedef uUI16VecCol uUElemIdVecCol;

/**
 * Column vector for boolean values
 */
typedef uMatrixUtils::Col<uBool> uBoolVecCol;

/**
 * Matrix indexing type
 */
typedef uMatrixUtils::uword uMatSz_t;

/**
 * Column vector of matrix indexing type
 */
typedef uMatrixUtils::Col<uMatSz_t> uMatSzVecCol;

/**
 * Macro for casting to matrix indexing type
 */
#define U_TO_MAT_SZ_T(i) static_cast<uMatSz_t>((i))

#endif  // uArmadillo_h
