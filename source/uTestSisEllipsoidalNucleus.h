//****************************************************************************
// uTestSisEllipsoidNucleus.h
//****************************************************************************

/**
 * Smoke test of ellipsoidal nuclear confinement
 *
 * Usage
 * -test_sis_ellipnuc [--output_dir <path>] [...]
 */

#ifdef uTestSisEllipsoidNucleus
#   error "Test Sis Ellipsoid Nucleus included multiple times!"
#endif  // uTestEllipsoidNucleus_h
#define uTestSisEllipsoidNucleus

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uAssert.h"
#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uFilesystem.h"
#include "uMockUpUtils.h"
#include "uSisEllipsoidNuclearMixin.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uStringUtils.h"
#include "uTypes.h"

#include <math.h>
#include <string>

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro for declaring a library glue with no quality control and no
 * lamina or interaction constraints, but with ellipsoidal nucleus
 */
#define U_MOCKUP_DECLARE_NULL_ELLIPNUC_GLUE(                            \
    t_Glue, t_Growth, t_GrowthSim, t_Rad, t_RadSim, t_Ener, t_EnerSim)  \
    U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(t_Glue,                        \
                                         t_Growth,                      \
                                         t_GrowthSim,                   \
                                         t_Rad,                         \
                                         t_RadSim,                      \
                                         uSisNullIntrChrMixin,          \
                                         uSisNullIntrChrSimLevelMixin,  \
                                         uSisNullIntrLamMixin,          \
                                         uSisNullIntrLamSimLevelMixin,  \
                                         uSisNullIntrNucbMixin,         \
                                         uSisNullIntrNucbSimLevelMixin, \
                                         t_Ener,                        \
                                         t_EnerSim)

namespace uTestSisEllipsoidalNucleus {

/**
 * A library glue for single end growth, single locus, homogeneous radius,
 * uniform sampling, no quality control, no interaction constraints, and with
 * ellipsoid nucleus
 */
U_MOCKUP_DECLARE_NULL_ELLIPNUC_GLUE(uHomgSlocNullGlue,
                                    uSisSeoSloc::GrowthMixin,
                                    uSisSeoSloc::GrowthSimLevelMixin_threaded,
                                    uSisHomgNodeRadiusMixin,
                                    uSisHomgNodeRadiusSimLevelMixin,
                                    uSisUnifEnergyMixin,
                                    uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, single locus, heterogeneous radius,
 * uniform sampling, no quality control, no interaction constraints, and with
 * ellipsoid nucleus
 */
U_MOCKUP_DECLARE_NULL_ELLIPNUC_GLUE(uHetrSlocNullGlue,
                                    uSisSeoSloc::GrowthMixin,
                                    uSisSeoSloc::GrowthSimLevelMixin_threaded,
                                    uSisHetrNodeRadiusMixin,
                                    uSisHetrNodeRadiusSimLevelMixin,
                                    uSisUnifEnergyMixin,
                                    uSisUnifEnergySimLevelMixin);

/**
 * Runs simulation test
 */
template <typename sim_t>
int run_test(const uCmdOptsMap& cmd_opts, const std::string& test_name) {
    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->init(cmd_opts);

    // Output configuration
    std::cout << "Running " << test_name << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;
    U_STATS_REPORT;
    std::cout << "--------------------------------------------------\n";

    // Export!
    uSisUtils::export_sim(sim);

    // Destroy globals
    uG::teardown();

    return (result) ? uExitCode_normal : uExitCode_error;
}

/**
 * @return Volume of ellipsoid defined by principal diameters (dx, dy, dz)
 */
uReal diam2vol_ellip(const uReal dx, const uReal dy, const uReal dz) {
    const uReal rx = U_TO_REAL(0.5) * dx;
    uAssert(rx > U_TO_REAL(0.0));
    const uReal ry = U_TO_REAL(0.5) * dy;
    uAssert(ry > U_TO_REAL(0.0));
    const uReal rz = U_TO_REAL(0.5) * dz;
    uAssert(rz > U_TO_REAL(0.0));
    return U_TO_REAL((4.0 / 3.0)) * rx * ry * rz * U_TO_REAL(M_PI);
}

/**
 * @return Packing volume of ellipsoid defined by principal diameters
 *  (dx, dy, dz) and monomer diameter dn
 */
uReal diam2volpack_ellip(const uReal dx,
                         const uReal dy,
                         const uReal dz,
                         const uReal dn) {
    uAssert(dn > U_TO_REAL(0.0));
    const uReal dx_pack = dx - dn;
    const uReal dy_pack = dy - dn;
    const uReal dz_pack = dz - dn;
    return diam2vol_ellip(dx_pack, dy_pack, dz_pack);
}

/**
 * Diameter of sphere with parameter volume
 */
uReal vol2diam_sph(const uReal v) {
    uAssert(v > U_TO_REAL(0.0));
    const uReal dsph =
        U_TO_REAL(2.0) *
        pow((U_TO_REAL(0.75) * v) / U_TO_REAL(M_PI), U_TO_REAL(1.0 / 3.0));
    // uAssertRealEq tolerance too low!
    uAssert(fabs(v - diam2vol_ellip(dsph, dsph, dsph)) < 0.001);
    return dsph;
}

/**
 * Diameter of sphere that would produce packed volume according to monomer
 *  diameter dn
 */
uReal volpack2diam_sph(const uReal vpack, const uReal dn) {
    uAssert(dn > U_TO_REAL(0.0));
    const uReal dsph = vol2diam_sph(vpack) + dn;
    // uAssertRealEq tolerance too low!
    uAssert(fabs(vpack - diam2volpack_ellip(dsph, dsph, dsph, dn)) < 0.001);
    return dsph;
}

/**
 * Process suite of tests over hard-coded overlap factors
 */
template <typename sim_t>
int run_test_suite(const uCmdOptsMap& cmd_opts_,
                   const std::string& test_name_) {
    uCmdOptsMap cmd_opts = cmd_opts_;
    // Initialize command line
    const std::string DEF_OUTPUT_DIR =
        (uFs::path("..") / uFs::path("output") / uFs::path("test_sis_ellipnuc"))
            .string();
    const uReal MAX_NODE_DIAM = U_TO_REAL(110.0);
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_output_dir),
                                  DEF_OUTPUT_DIR);
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_max_trials),
                                  std::string("2000"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_ensemble_size),
                                  std::string("5"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_max_node_diameter),
                                  u2Str(MAX_NODE_DIAM));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_num_nodes),
                                  std::string("550"));
    cmd_opts.set_option_if_absent(
        uOpt_get_cmd_switch(uOpt_num_unit_sphere_sample_points),
        std::string("50"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_export_csv_binary),
                                  std::string("1"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_export_pdb_binary),
                                  std::string("1"));
    cmd_opts.set_option_if_absent(
        uOpt_get_cmd_switch(uOpt_export_extended_binary), std::string("1"));

    // Principal axis diameters
    typedef struct {
        uReal x_diam;
        uReal y_diam;
        uReal z_diam;
    } nuc_diams_t;

    // Test suite configurations
    const uReal DIAM_LO = U_TO_REAL(1000.0);
    const uReal DIAM_ME = U_TO_REAL(1000.0);
    const uReal DIAM_HI = U_TO_REAL(5000.0);
    // Naive volume
    const uReal VOL_ELLIP = diam2vol_ellip(DIAM_LO, DIAM_ME, DIAM_HI);
    // Packing (accessible) volume
    const uReal VOL_PACK_ELLIP =
        diam2volpack_ellip(DIAM_LO, DIAM_ME, DIAM_HI, MAX_NODE_DIAM);
    // Compute sphere diameter with same volume as ellipse
    const uReal DIAM_SPH = vol2diam_sph(VOL_ELLIP);
    // Compute sphere diameter with same packing volume as ellipse
    const uReal DIAM_PACK_SPH = volpack2diam_sph(VOL_PACK_ELLIP, MAX_NODE_DIAM);

    const nuc_diams_t NUC_DIAMS[] = {
        {DIAM_HI, DIAM_HI, DIAM_HI},
        {DIAM_HI, DIAM_ME, DIAM_LO},
        {DIAM_LO, DIAM_HI, DIAM_ME},
        {DIAM_ME, DIAM_LO, DIAM_HI},
        {DIAM_SPH, DIAM_SPH, DIAM_SPH},
        {DIAM_PACK_SPH, DIAM_PACK_SPH, DIAM_PACK_SPH}};

    // Determine number of configurations
    const size_t NUM_TEST = sizeof(NUC_DIAMS) / sizeof(nuc_diams_t);

    // Process each configuration
    int result = uExitCode_normal;
    for (size_t i = 0; i < NUM_TEST; ++i) {
        const nuc_diams_t& nuc_diams = NUC_DIAMS[i];
        const std::string x_diam_str = u2Str(nuc_diams.x_diam);
        const std::string y_diam_str = u2Str(nuc_diams.y_diam);
        const std::string z_diam_str = u2Str(nuc_diams.z_diam);
        // Determine job export prefix
        const std::string job_id = std::string("X.") + x_diam_str +
                                   std::string(".Y.") + y_diam_str +
                                   std::string(".Z.") + z_diam_str;
        cmd_opts.set_option(uOpt_get_cmd_switch(uOpt_job_id), job_id);
        // Override nuclear principal diameters
        cmd_opts.set_option(uOpt_get_cmd_switch(uOpt_nuclear_diameter_x),
                            x_diam_str);
        cmd_opts.set_option(uOpt_get_cmd_switch(uOpt_nuclear_diameter_y),
                            y_diam_str);
        cmd_opts.set_option(uOpt_get_cmd_switch(uOpt_nuclear_diameter_z),
                            z_diam_str);
        const uReal vol = diam2vol_ellip(
            nuc_diams.x_diam, nuc_diams.y_diam, nuc_diams.z_diam);
        const uReal volpack = diam2volpack_ellip(nuc_diams.x_diam,
                                                 nuc_diams.y_diam,
                                                 nuc_diams.z_diam,
                                                 MAX_NODE_DIAM);
        std::cout << "##############################################\n";
        std::cout << "\t-DIAM X: " << x_diam_str << std::endl;
        std::cout << "\t-DIAM Y: " << y_diam_str << std::endl;
        std::cout << "\t-DIAM Z: " << z_diam_str << std::endl;
        std::cout << "\t-VOL: " << vol << std::endl;
        std::cout << "\t-VOLPACK: " << volpack << std::endl;
        std::cout << "##############################################\n";
        // Run test!
        const std::string test_name = test_name_ + std::string(" : ") + job_id;
        result = run_test<sim_t>(cmd_opts, test_name);
        if (!result == uExitCode_normal) {
            // Stop test suite if unexpected error code
            break;
        }
    }
    return result;
}

}  // namespace uTestSisEllipsoidalNucleus

/**
 * Entry point for test
 */
int uTestSisEllipsoidalNucleusMain(const uCmdOptsMap& cmd_opts) {
    // Run homogeneous, single-locus simulation
    const uBool is_okay_homg_sloc =
        uTestSisEllipsoidalNucleus::run_test_suite<
            uTestSisEllipsoidalNucleus::uHomgSlocNullGlue::sim_t>(
            cmd_opts, std::string("Homg Sloc Ellip Nuc Test")) ==
        uExitCode_normal;
    // Run heterogeneous, single-locus simulation
    const uBool is_okay_hetr_sloc =
        uTestSisEllipsoidalNucleus::run_test_suite<
            uTestSisEllipsoidalNucleus::uHetrSlocNullGlue::sim_t>(
            cmd_opts, std::string("Hetr Sloc Ellip Nuc Test")) ==
        uExitCode_normal;
    // Check if tests okay
    if (!is_okay_homg_sloc || !is_okay_hetr_sloc) {
        // Test failed!
        return uExitCode_error;
    }
    // All tests passed
    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
