//****************************************************************************
// uCommandletHooks.inl
//****************************************************************************

/**
 * General work flow for adding a commandlet:
 * 1) Create a new commandlet header file uCmdlet<name>.h
 * 2) Within commandlet header file, follow header guard and build flags
 *      policy as in other commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef U_BUILD_ENABLE_COMMANDLETS
 * 3) Within commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within commandlet header file, define uCmdlet<name>Main(opts) method
 *      (outside of anonymous namespace)
 * 5) In uOpts.h, within the commandlet options demarcated by build flag, add
 *      uOpt_cmdlet_<name> to the set of enums
 * 6) In uOpts.cpp, in the section of U_OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("cmdlet_<name>", "<description>").
 *      - make sure this is in same order as enum structure in uOpts.h
 * 7.) In uCommandletHooks.inl, include commandlet header file
 * 8.) In uCommandletHooks.inl, add to the GAppInfo structure the commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef uCommandletHooks_inl
#   error "Commandlet Hooks included multiple times!"
#endif  // uCommandletHooks_inl
#define uCommandletHooks_inl

// Implementation of commandlet hooks extracted to separate file to avoid
// cluttering uMain.cpp

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uAppHooks.h"
#include "uExitCodes.h"

// Include additional commandlets here
#include "uCmdletBenchmarkSisBendSeoSlocGrowth.h"
#include "uCmdletBenchmarkSisUnifSeoSlocGrowth.h"
#include "uCmdletLigcCalc.h"
#include "uCmdletLigcPopd.h"
#include "uCmdletSphereSurfaceRgl.h"
#include "uCmdletTuneBendRigidity.h"

// Include printf
#include <stdio.h>

// Placeholder main that dumps information about project
// Usage:
// -cmdlet_info
static int uCmdletInfoMain(const uCmdOptsMap& cmd_opts) {
    printf("Fractal chromatin modeler. University of Illinois at Chicago.\n");
    return uExitCode_normal;
}

// Register commandlets here
static struct uAppInfo GCommandlets[] = {
    {uOpt_get_cmd_switch(uOpt_benchmark),
     &uCmdletBenchmarkSisUnifSeoSlocGrowthMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_benchmark_sis_bend_seo_sloc_growth),
     &uCmdletBenchmarkSisBendSeoSlocGrowthMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_benchmark_sis_unif_seo_sloc_growth),
     &uCmdletBenchmarkSisUnifSeoSlocGrowthMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_info), &uCmdletInfoMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_sphere_surface_rgl),
     &uCmdletSphereSurfaceRglMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_tune_bend_rigidity),
     &uCmdletTuneBendRigidityMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_ligc_calc), &uCmdletLigcCalcMain},
    {uOpt_get_cmd_switch(uOpt_cmdlet_ligc_popd), &uCmdletLigcPopdMain}};

// Commandlets are enabled, route to the proper commandlet hook:
// Parse command line and run any commandlets specified
#define U_CONDITIONAL_RUN_COMMANDLETS(cmd_opts) \
    U_CONDITIONAL_RUN_APPS(cmd_opts, GCommandlets)

#else

// Commandlets are not enabled, strip away call to commandlet hook
#define U_CONDITIONAL_RUN_COMMANDLETS(cmd_opts) ((void)0)

#endif  // U_BUILD_ENABLE_COMMANDLETS
