//****************************************************************************
// uRand.h
//****************************************************************************

/**
 * Utility for generating random numbers
 */

#ifndef uRand_h
#define uRand_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uLogf.h"
#include "uTypes.h"

// Make sure to initialize and expose random API from matrix library
#include "uArmadillo.h"

// If C++11 is not supported, default to boost implementation
#ifdef U_BUILD_CXX_11
#   include <random>
#   define uRandom std
#else
#   include <boost/random.hpp>
#   include <boost/random/random_device.hpp>
#   define uRandom boost::random
#endif  // U_BUILD_CXX_11

//****************************************************************************
// Interface
//****************************************************************************

/**
 * Interface for uniform random number generation
 * @TODO - add functions for generating random vectors and matrices as needed
 * @TODO - investigate SIMD implementation of Mersenne twister:
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html
 *  (SIMD-oriented Fast Mersenne Twister (SFMT))
 */
class uRand {
public:
    /**
     * Default constructor - initializes RNG engine
     */
    uRand() : m_eng(get_seed()) {
        // Note: thread_local must be supported in order to have Mersenne
        // twister support within arma
        uMatrixUtils::arma_rng::set_seed(get_seed());
    }

    /**
     * @return Uniform random number in [0.0, 1.0)
     */
    inline uReal unif_real() { return m_unif_real_dis(m_eng); }

    /**
     * @return Uniform random number in [lower_, upper_)
     */
    inline uReal unif_real(const uReal lower_, const uReal upper_) {
        uAssert(lower_ <= upper_);
        uRandom::uniform_real_distribution<uReal> dis(lower_, upper_);
        return dis(m_eng);
    }

    /**
     * @return Integer in [lower_, upper_] inclusive
     */
    template <typename t_int>
    inline t_int unif_int(const t_int lower_, const t_int upper_) {
        uAssert(lower_ <= upper_);
        uRandom::uniform_int_distribution<t_int> dis(lower_, upper_);
        return dis(m_eng);
    }

    /**
     * @param dis - distribution object
     * @return sampled value from arbitrary distribution
     */
    template <typename t_dis>
    inline typename t_dis::result_type sample(t_dis& dis) {
        return dis(m_eng);
    }

    /**
     * @HACK - Force Intel compilers to seed RNG - certain versions of the
     *  Intel compiler do not appear to seed properly via constructor
     *  initializer list => expose reseed method to force compiler to register
     *  seed call!
     */
    void reseed() {
        m_eng.seed(get_seed());
    }

private:
    // According to:
    // https://channel9.msdn.com/Events/GoingNative/2013/rand-Considered-Harmful
    // Copying the random number engine is expensive as Mersenne twister has a
    // large internal state. It's "okay" to copy the distribution object, but
    // definitely avoid copying the engine object.

    // It would be nice to forbid (make private) the copy constructor and
    // assignment operator; however, our thread local storage implementation
    // requires them. (It's recommended to have 1 instance per thread). So,
    // for now, we are not forbidding implicit copy and assignment operations.
    typedef uRandom::random_device::result_type seed_type;

    /**
     * @return Seed to initialize RNG
     */
    static seed_type get_seed() {
#ifdef U_BUILD_SEED_RAND
        static uRandom::random_device rd;
        const seed_type seed = rd();
#else
        const seed_type seed = 7331;
#endif  // U_BUILD_SEED_RAND
        uLogf("Random seed: %u\n", seed);
        return seed;
    }

    /**
     * Typedef the random number engine
     */
    typedef uRandom::mt19937 reng_t;

    /**
     * Engine which generates the random numbers
     */
    reng_t m_eng;

    /**
     * Uniform distribution on [0,1) interval
     */
    uRandom::uniform_real_distribution<uReal> m_unif_real_dis;
};

#endif  // uRand_h
