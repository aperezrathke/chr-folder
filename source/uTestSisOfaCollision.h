//****************************************************************************
// uTestSisOfaCollision.h
//****************************************************************************

/**
 * Commandlet for testing growth of a chromatin chain (single locus) with
 * uniform energy model and overlap factor (ofa) collisions
 *
 * Usage:
 * -test_sis_ofa_collision
 */

#ifdef uTestSisOfaCollision_h
#   error "Test Sis Ofa Collision included multiple times!"
#endif  // uTestSisOfaCollision_h
#define uTestSisOfaCollision_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uAssert.h"
#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uMockUpUtils.h"
#include "uSisOfaCollisionMixin.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uTypes.h"

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro declares library glue with no quality control and 'ofa' collisions
 */
#define U_MOCKUP_DECLARE_OFA_NO_QC_GLUE(t_Glue,                       \
                                        t_Growth,                     \
                                        t_GrowthSim,                  \
                                        t_Rad,                        \
                                        t_RadSim,                     \
                                        t_IntrChr,                    \
                                        t_IntrChrSim,                 \
                                        t_IntrLam,                    \
                                        t_IntrLamSim,                 \
                                        t_IntrNucb,                   \
                                        t_IntrNucbSim,                \
                                        t_Ener,                       \
                                        t_EnerSim)                    \
                                                                      \
    class t_Glue {                                                    \
                                                                      \
    public:                                                           \
        typedef uSisSim<t_Glue> sim_t;                                \
        typedef uSisSample<t_Glue> sample_t;                          \
        typedef uSisCanonicalTrialRunnerMixin<                        \
            t_Glue,                                                   \
            uSisParallelBatchGrower<t_Glue> >                         \
            trial_runner_mixin_t;                                     \
        typedef uSisNullQcSimLevelMixin<t_Glue> sim_level_qc_mixin_t; \
        typedef t_GrowthSim<t_Glue> sim_level_growth_mixin_t;         \
        typedef t_Growth<t_Glue> growth_mixin_t;                      \
        typedef t_RadSim<t_Glue> sim_level_node_radius_mixin_t;       \
        typedef t_Rad<t_Glue> node_radius_mixin_t;                    \
        typedef uSisSphereNuclearSimLevelMixin_threaded<t_Glue>       \
            sim_level_nuclear_mixin_t;                                \
        typedef uSisSphereNuclearMixin<t_Glue> nuclear_mixin_t;       \
        typedef uSisOfaCollisionSimLevelMixin_threaded<t_Glue>        \
            sim_level_collision_mixin_t;                              \
        typedef uSisOfaCollisionMixin<t_Glue, uBroadPhase_t>          \
            collision_mixin_t;                                        \
        typedef t_IntrChrSim<t_Glue> sim_level_intr_chr_mixin_t;      \
        typedef t_IntrChr<t_Glue> intr_chr_mixin_t;                   \
        typedef t_IntrLamSim<t_Glue> sim_level_intr_lam_mixin_t;      \
        typedef t_IntrLam<t_Glue> intr_lam_mixin_t;                   \
        typedef t_IntrNucbSim<t_Glue> sim_level_intr_nucb_mixin_t;    \
        typedef t_IntrNucb<t_Glue> intr_nucb_mixin_t;                 \
        typedef t_EnerSim<t_Glue> sim_level_energy_mixin_t;           \
        typedef t_Ener<t_Glue> energy_mixin_t;                        \
        typedef uSisSeoUnifCubeSeedMixin<t_Glue> seed_mixin_t;        \
    }

/**
 * Macro for declaring a library glue with no quality control and no
 * lamina or interaction constraints, but with ofa collisions
 */
#define U_MOCKUP_DECLARE_NULL_OFA_GLUE(                                \
    t_Glue, t_Growth, t_GrowthSim, t_Rad, t_RadSim, t_Ener, t_EnerSim) \
    U_MOCKUP_DECLARE_OFA_NO_QC_GLUE(t_Glue,                            \
                                    t_Growth,                          \
                                    t_GrowthSim,                       \
                                    t_Rad,                             \
                                    t_RadSim,                          \
                                    uSisNullIntrChrMixin,              \
                                    uSisNullIntrChrSimLevelMixin,      \
                                    uSisNullIntrLamMixin,              \
                                    uSisNullIntrLamSimLevelMixin,      \
                                    uSisNullIntrNucbMixin,             \
                                    uSisNullIntrNucbSimLevelMixin,     \
                                    t_Ener,                            \
                                    t_EnerSim)

namespace uTestSisOfaCollision {

/**
 * A library glue for single end growth, single locus, homogeneous radius,
 * uniform sampling, no quality control, no interaction constraints, and with
 * 'ofa' collisions
 */
U_MOCKUP_DECLARE_NULL_OFA_GLUE(uHomgSlocNullGlue,
                               uSisSeoSloc::GrowthMixin,
                               uSisSeoSloc::GrowthSimLevelMixin_threaded,
                               uSisHomgNodeRadiusMixin,
                               uSisHomgNodeRadiusSimLevelMixin,
                               uSisUnifEnergyMixin,
                               uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, single locus, heterogeneous radius,
 * uniform sampling, no quality control, no interaction constraints, and with
 * 'ofa' collisions
 */
U_MOCKUP_DECLARE_NULL_OFA_GLUE(uHetrSlocNullGlue,
                               uSisSeoSloc::GrowthMixin,
                               uSisSeoSloc::GrowthSimLevelMixin_threaded,
                               uSisHetrNodeRadiusMixin,
                               uSisHetrNodeRadiusSimLevelMixin,
                               uSisUnifEnergyMixin,
                               uSisUnifEnergySimLevelMixin);

/**
 * Runs simulation test
 */
template <typename sim_t>
int run_test(const uCmdOptsMap& cmd_opts, const std::string& test_name) {
    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->init(cmd_opts);

    // Output configuration
    std::cout << "Running " << test_name << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;
    U_STATS_REPORT;
    std::cout << "--------------------------------------------------\n";

    // Destroy globals
    uG::teardown();

    return (result) ? uExitCode_normal : uExitCode_error;
}

/**
 * Process suite of tests over hard-coded overlap factors
 */
template <typename sim_t>
int run_test_suite(const uCmdOptsMap& cmd_opts_,
                   const std::string& test_name_) {
    uCmdOptsMap cmd_opts = cmd_opts_;
    // Initialize command line
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_output_dir),
                                  std::string("null"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_max_trials),
                                  std::string("1000"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_ensemble_size),
                                  std::string("75"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_max_node_diameter),
                                  std::string("110.0"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_nuclear_diameter),
                                  std::string("4450.3264"));
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_num_nodes),
                                  std::string("3300"));
    cmd_opts.set_option_if_absent(
        uOpt_get_cmd_switch(uOpt_num_unit_sphere_sample_points),
        std::string("64"));

    // Overlap factor suite
    const uReal OFA[] = {0.0, 0.25, 0.5, 0.75, 1.0};
    // Determine size of suite
    const size_t NUM_OFA = sizeof(OFA) / sizeof(uReal);
    // Test each overlap factor
    int result = uExitCode_normal;
    for (size_t i = 0; i < NUM_OFA; ++i) {
        const std::string ofa_str = u2Str(OFA[i]);
        const std::string test_name =
            test_name_ + std::string(" (OFA = ") + ofa_str + std::string(")");
        // Override ofa
        cmd_opts.set_option(uOpt_get_cmd_switch(uOpt_collision_ofa),
                            ofa_str.c_str());
        result = run_test<sim_t>(cmd_opts, test_name);
        if (!result == uExitCode_normal) {
            // Stop test suite if unexpected error code
            break;
        }
    }
    return result;
}

}  // namespace uTestSisOfaCollision

/**
 * Entry point for test
 */
int uTestSisOfaCollisionMain(const uCmdOptsMap& cmd_opts) {
    // Run homogeneous, single-locus simulation
    const uBool is_okay_homg_sloc =
        uTestSisOfaCollision::run_test_suite<
            uTestSisOfaCollision::uHomgSlocNullGlue::sim_t>(
            cmd_opts, std::string("Ofa Homg Sloc Collision Test")) ==
        uExitCode_normal;
    // Run heterogeneous, single-locus simulation
    const uBool is_okay_hetr_sloc =
        uTestSisOfaCollision::run_test_suite<
            uTestSisOfaCollision::uHetrSlocNullGlue::sim_t>(
            cmd_opts, std::string("Ofa Hetr Sloc Collision Test")) ==
        uExitCode_normal;
    // Check if tests okay
    if (!is_okay_homg_sloc || !is_okay_hetr_sloc) {
        // Test failed!
        return uExitCode_error;
    }
    // All tests passed
    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
