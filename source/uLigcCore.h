//****************************************************************************
// uLigcCore.h
//****************************************************************************

/**
 * Common ligation contact utilities
 */

#ifndef uLigcCore_h
#define uLigcCore_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uBitsetHandle.h"
#include "uBroadPhase.h"
#include "uConfig.h"
#include "uExportFlags.h"
#include "uTypes.h"

#include <iostream>
#include <utility>
#include <vector>
#include <zstr.hpp>

//****************************************************************************
// Ligc core
//****************************************************************************

/**
 * Effectively a namespace for common ligc utilities
 */
class uLigcCore {
public:
    //////////////////////////////////////////////////////////////////////////
    // Types
    //////////////////////////////////////////////////////////////////////////

    /**
     * Broad phase element identifier
     */
    typedef uBroadPhase_t::elem_id_t elem_id_t;

    /**
     * Contact tuple (index of first monomer, index of second monomer)
     */
    typedef std::pair<elem_id_t, elem_id_t> contact_t;

    /**
     * Set of contact tuples within a polymer
     */
    typedef std::vector<contact_t> profile_t;

    //////////////////////////////////////////////////////////////////////////
    // Format
    //////////////////////////////////////////////////////////////////////////

    /**
     * @return default base name of ligc format type
     */
    inline static const char* get_format_base() { return "ligc"; }

    /**
     * @return ligc binary format extension
     */
    inline static const char* get_format_bin() { return "bin"; }

    /**
     * @return ligc CSV format extension
     */
    inline static const char* get_format_csv() { return "csv"; }

    /**
     * @return ligc CSV gzip format extension
     */
    inline static const char* get_format_csv_gz() { return "csv.gz"; }

    /**
     * @return bulk ligc CSV format extension
     */
    inline static const char* get_format_csv_bulk() { return "bulk.csv"; }

    /**
     * @return bulk ligc CSV gzip format extension
     */
    inline static const char* get_format_csv_bulk_gz() { return "bulk.csv.gz"; }

    /**
     * @param ligc_format - string resulting from e.g. get_format_csv() call
     * @return suggested subdirectory name for ligc format
     */
    static std::string get_format_subdir(const std::string& ligc_format);

    /**
     * @param output_dir - base output (data) directory path
     * @param ligc_format - string resulting from e.g. get_format_csv() call
     * @return suggested directory path for ligc format
     */
    static std::string get_format_dir(const std::string& output_dir,
                                      const std::string& ligc_format);

    /**
     * Resolves format directory path - meant for use with commandlets were
     * the import or export directory may differ from implicit paths
     * @param out - output export path
     * @param base_format_dir - base export directory
     * @param format_no_dot - file extension suffix without '.'
     */
    static void get_format_dir2(std::string& out,
                                const std::string& base_format_dir,
                                const std::string& format_no_dot);

    //////////////////////////////////////////////////////////////////////////
    // Proximity
    //////////////////////////////////////////////////////////////////////////

    /**
     * @param config - User configuration
     * @return User-defined ligation threshold interaction distance, or
     *  default if not specified
     */
    static uReal get_dist(uSpConstConfig_t config);

    /**
     * To allow ligation interaction queries during ligc export, we need to
     * ensure the broad phase grid is padded by the knock-in distance if this
     * distance is larger than the max node diameter
     * @param max_node_diameter - Diameter of largest node in simulation
     * @param ki_dist - Knock-in distance
     * @param export_flags - Simulation export flags, will avoid ligc padding
     *  if no ligc export detected
     * @return broad phase collision padding
     */
    static uReal get_collision_boundary_pad(const uReal max_node_diameter,
                                            const uReal ki_dist,
                                            const uUInt export_flags);

    /**
     * Calculate ligation contacts
     * @param profile - output ligation tuples
     * @param mask_buffer - pre-allocated buffer used for narrow phase checking
     * @param ki_dist - ligation proximity (Euclidean distance) threshold
     * @param node_radii - radius at each monomer node
     * @param node_positions - (x, y, z) coordinates for each node center
     * @param broad_phase - broad phase collision structure
     * @param broad_cell_length - broad phase grid cell length
     * @param broad_radius - broad phase bounding radius
     * @param keep_bonded - If 1, then mark (i,i) and (i,i+1) ligations
     */
    static void proximity(profile_t& profile,
                          uBitset& mask_buffer,
                          const uReal ki_dist,
                          const uVecCol& node_radii,
                          const uMatrix& node_positions,
                          const uBroadPhase_t& broad_phase,
                          const uReal broad_cell_length,
                          const uReal broad_radius,
                          const uBool keep_bonded);

    //////////////////////////////////////////////////////////////////////////
    // Export
    //////////////////////////////////////////////////////////////////////////

    /**
     * @return uTRUE if ligc export indicated, uFALSE o/w
     */
    static uBool should_export(const uUInt export_flags);

    /**
     * Export ligation tuples as binary format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight - log importance weight
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_bin(std::ostream& out,
                            const profile_t& profile,
                            const uReal log_weight);

    /**
     * Export ligation tuples as binary format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight_comment - log importance weight comment
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_bin(std::ostream& out,
                            const profile_t& profile,
                            const std::string& log_weight_comment);

    /**
     * Export ligation tuples as CSV format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight - log importance weight
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_csv(std::ostream& out,
                            const profile_t& profile,
                            const uReal log_weight);

    /**
     * Export ligation tuples as CSV format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight_str - log importance weight comment
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_csv(std::ostream& out,
                            const profile_t& profile,
                            const std::string& log_weight_comment);

    /**
     * Export ligation tuples as gzip-compressed CSV format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight - log importance weight
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_csv_gz(zstr::ofstream& out,
                               const profile_t& profile,
                               const uReal log_weight);

    /**
     * Export ligation tuples as gzip-compressed CSV format
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param out - output stream
     * @param profile - ligation profile
     * @param log_weight_str - log importance weight comment
     * @return TRUE if export okay, FALSE o/w
     */
    static uBool export_csv_gz(zstr::ofstream& out,
                               const profile_t& profile,
                               const std::string& log_weight_comment);

    //////////////////////////////////////////////////////////////////////////
    // Import
    //////////////////////////////////////////////////////////////////////////

    /**
     * Imports binary formatted ligc data and log weight
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream (make sure to open in binary mode!)
     * @param profile - imported ligation tuples, always cleared
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_bin(std::istream& in,
                            profile_t& profile,
                            uReal& log_weight);

    /**
     * Imports log weight from binary formatted ligc data
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream (make sure to open in binary mode!)
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_bin(std::istream& in, uReal& log_weight);

    /**
     * Imports CSV formatted ligc data and log weight
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream
     * @param profile - imported ligation tuples, always cleared
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_csv(std::istream& in,
                            profile_t& profile,
                            uReal& log_weight);

    /**
     * Imports log weight from CSV formatted ligc data
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_csv(std::istream& in, uReal& log_weight);

    /**
     * Imports gzip-compressed, CSV formatted ligc data and log weight
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream (make sure to open in binary mode!)
     * @param profile - imported ligation tuples, always cleared
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_csv_gz(zstr::ifstream& in,
                               profile_t& profile,
                               uReal& log_weight);

    /**
     * Imports log weight from gzip-compressed, CSV formatted ligc data
     * @WARNING - DOES NOT CLOSE STREAM HANDLE
     * @param in - input stream (make sure to open in binary mode!)
     * @param log_weight- imported log weight
     * @return TRUE if import okay, FALSE o/w
     */
    static uBool import_csv_gz(zstr::ifstream& in, uReal& log_weight);
};

#endif  // uLigcCore_h
