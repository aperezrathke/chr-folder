//****************************************************************************
// uSisUtilsExport.h
//****************************************************************************

#ifndef uSisUtilsExport_h
#define uSisUtilsExport_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uChromatinExportCsv.h"
#include "uChromatinExportGzipper.h"
#include "uChromatinExportLigc.h"
#include "uChromatinExportPdb.h"
#include "uChromatinExportPml.h"
#include "uChromatinExportPolicyBulk.h"
#include "uChromatinExportPolicyBulkGz.h"
#include "uChromatinExportPolicyIndiv.h"
#include "uConfig.h"
#include "uExportFlags.h"
#include "uTypes.h"

#include <string.h>

//****************************************************************************
// Interface
//****************************************************************************

namespace uSisUtils {

/**
 * For PDB-based exporters, have fixed number of characters to represent 3-D
 * positions. Therefore, we scale positions in order to help mitigate issues
 * due to having a fixed number of columns to represent each position.
 * @return scale factor for PDB-based formats
 */
extern uReal get_export_pdb_scale(const uVecCol& node_radii);

/**
 * @param config - User configuration
 * @return CSV export precision.
 * @WARNING - THIS VALUE IS IGNORED IF LOWER THAN STREAM DEFAULT!
 */
extern int get_export_csv_precision(uSpConstConfig_t config);

/**
 * Utility exports all samples associated with parameter simulation using
 * templated export policy type
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 * @param scale - If supported by format, all node positions and radii are
 *   scaled by this factor
 * @param precision - If supported by format, decimal precision
 */
template <typename sim_t, typename xportpol_t>
void export_chromatin(const sim_t& sim,
                      const uUInt flags,
                      const uReal scale,
                      const int precision) {
    typedef typename sim_t::sample_t sample_t;
    const std::vector<sample_t>& samples = sim.get_completed_samples();
    const uVecCol& log_weights = sim.get_completed_log_weights_view();
    uAssertPosEq(samples.size(), log_weights.size());
    uSpConstConfig_t config = sim.get_config();

    // Check polymer size is compatible with exporter
    xportpol_t xportpol;
    xportpol.warn_format(sim.get_max_total_num_nodes());

    // Core exporter arguments
    uChromatinExportArgs_t args = {0};
    args.p_node_radii = &(sim.get_node_radii());
    args.p_max_num_nodes_at_locus = &(sim.get_max_num_nodes_at_locus());
    args.nuclear_radius = sim.get_max_nuclear_radius();
    args.extended = flags & uExportExtended;
    args.nuclear_shell = flags & uExportNucleus;
    args.scale = scale;
    args.precision = precision;

    // Chr-chr interactions
    uUIMatrix chr_frags, chr_kin, chr_ko;
    sim.get_intr_chr_frags(chr_frags);
    sim.get_intr_chr_kin(chr_kin);
    sim.get_intr_chr_ko(chr_ko);
    args.intr_chr = flags & uExportIntrChr;
    args.p_intr_chr_frags = &chr_frags;
    args.p_intr_chr_kin = &chr_kin;
    args.p_intr_chr_ko = &chr_ko;

    // Lamina interactions
    uUIMatrix lam_frags;
    uUIVecCol lam_kin, lam_ko;
    sim.get_intr_lam_frags(lam_frags);
    sim.get_intr_lam_kin(lam_kin);
    sim.get_intr_lam_ko(lam_ko);
    args.intr_lam = flags & uExportIntrLam;
    args.p_intr_lam_frags = &lam_frags;
    args.p_intr_lam_kin = &lam_kin;
    args.p_intr_lam_ko = &lam_ko;

    // Nuclear body interactions
    uUIMatrix nucb_frags, nucb_kin, nucb_ko;
    uMatrix nucb_centers;
    uVecCol nucb_radii;
    sim.get_intr_nucb_frags(nucb_frags);
    sim.get_intr_nucb_kin(nucb_kin);
    sim.get_intr_nucb_ko(nucb_ko);
    sim.get_intr_nucb_centers(nucb_centers);
    sim.get_intr_nucb_radii(nucb_radii);
    args.intr_nucb = flags & uExportIntrNucb;
    args.p_intr_nucb_frags = &nucb_frags;
    args.p_intr_nucb_kin = &nucb_kin;
    args.p_intr_nucb_ko = &nucb_ko;
    args.p_intr_nucb_centers = &nucb_centers;
    args.p_intr_nucb_radii = &nucb_radii;

    // Prime export policy
    xportpol.begin();

    // Export each sample
    const uUInt n_samples = U_TO_UINT(samples.size());
    for (uUInt i = 0; i < n_samples; ++i) {
        const sample_t& sample = samples[i];
        args.p_node_positions = &(sample.get_node_positions());
        args.log_weight = log_weights.at(i);
        xportpol.process(args, i, config);
    }

    // Flush export policy
    xportpol.end(config);
}

/**
 * Utility exports all samples as CSV format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_csv(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyIndiv<uChromatinExportCsv> xportpol_t;
    const int precision = get_export_csv_precision(sim.get_config());
    export_chromatin<sim_t, xportpol_t>(
        sim, flags, U_TO_REAL(1.0) /*scale*/, precision);
}

/**
 * Utility exports all samples as gzip-compressed, CSV format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_csv_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportGzipper<uChromatinExportCsv> exporter_gz_t;
    typedef uChromatinExportPolicyIndiv<exporter_gz_t> xportpol_t;
    const int precision = get_export_csv_precision(sim.get_config());
    export_chromatin<sim_t, xportpol_t>(
        sim, flags, U_TO_REAL(1.0) /*scale*/, precision);
}

/**
 * Utility exports all samples as bulk CSV format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_csv_bulk(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulk<uChromatinExportCsv> xportpol_t;
    const int precision = get_export_csv_precision(sim.get_config());
    export_chromatin<sim_t, xportpol_t>(
        sim, flags, U_TO_REAL(1.0) /*scale*/, precision);
}

/**
 * Utility exports all samples as bulk gzip-compressed, CSV format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_csv_bulk_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulkGz<uChromatinExportCsv> xportpol_t;
    const int precision = get_export_csv_precision(sim.get_config());
    export_chromatin<sim_t, xportpol_t>(
        sim, flags, U_TO_REAL(1.0) /*scale*/, precision);
}

/**
 * Utility exports all samples as PDB format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pdb(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyIndiv<uChromatinExportPdb> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as gzip-compressed, PDB format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pdb_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportGzipper<uChromatinExportPdb> exporter_gz_t;
    typedef uChromatinExportPolicyIndiv<exporter_gz_t> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as bulk PDB format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pdb_bulk(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulk<uChromatinExportPdb> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as bulk gzip-compressed, PDB format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pdb_bulk_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulkGz<uChromatinExportPdb> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as PML format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pml(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyIndiv<uChromatinExportPml> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as gzip-compressed, PDB format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pml_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportGzipper<uChromatinExportPml> exporter_gz_t;
    typedef uChromatinExportPolicyIndiv<exporter_gz_t> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as bulk PML format according to configuration
 * associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pml_bulk(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulk<uChromatinExportPml> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all samples as bulk gzip-compressed, PML format according to
 * configuration associated with parameter simulation.
 * @param sim - The simulation to export
 * @param flags - Needed for determining export of supplemental data
 */
template <typename sim_t>
void export_chromatin_pml_bulk_gz(const sim_t& sim, const uUInt flags) {
    typedef uChromatinExportPolicyBulkGz<uChromatinExportPml> xportpol_t;
    const uReal scale = get_export_pdb_scale(sim.get_node_radii());
    export_chromatin<sim_t, xportpol_t>(sim, flags, scale, 0 /*precision*/);
}

/**
 * Utility exports all log weights to single file
 * @param log_weights - The log weights to export
 */
extern void export_log_weights(const uVecCol& log_weights,
                               uSpConstConfig_t config);

/**
 * Utility checks export flags and exports accordingly
 * @param sim - The simulation to export
 */
template <typename sim_t>
void export_sim(const sim_t& sim) {
    const uUInt export_flags = sim.get_config()->export_flags;
    if (export_flags & uExportCsv) {
        export_chromatin_csv(sim, export_flags);
    }
    if (export_flags & uExportCsvGz) {
        export_chromatin_csv_gz(sim, export_flags);
    }
    if (export_flags & uExportCsvBulk) {
        export_chromatin_csv_bulk(sim, export_flags);
    }
    if (export_flags & uExportCsvBulkGz) {
        export_chromatin_csv_bulk_gz(sim, export_flags);
    }
    if (export_flags & uExportPdb) {
        export_chromatin_pdb(sim, export_flags);
    }
    if (export_flags & uExportPdbGz) {
        export_chromatin_pdb_gz(sim, export_flags);
    }
    if (export_flags & uExportPdbBulk) {
        export_chromatin_pdb_bulk(sim, export_flags);
    }
    if (export_flags & uExportPdbBulkGz) {
        export_chromatin_pdb_bulk_gz(sim, export_flags);
    }
    if (export_flags & uExportPml) {
        export_chromatin_pml(sim, export_flags);
    }
    if (export_flags & uExportPmlGz) {
        export_chromatin_pml_gz(sim, export_flags);
    }
    if (export_flags & uExportPmlBulk) {
        export_chromatin_pml_bulk(sim, export_flags);
    }
    if (export_flags & uExportPmlBulkGz) {
        export_chromatin_pml_bulk_gz(sim, export_flags);
    }
    if (export_flags & uExportLogWeights) {
        export_log_weights(sim.get_completed_log_weights_view(),
                           sim.get_config());
    }
    if (uChromatinExportLigc::should_export(export_flags)) {
        uChromatinExportLigc::export_ligc(sim, export_flags);
    }
}

}  // namespace uSisUtils

#endif  // uSisUtilsExport_h
