//****************************************************************************
// uMockUpUtils.h
//****************************************************************************

/**
 * Utility library for commandlets and tests
 */

#ifndef uMockUpUtils_h
#define uMockUpUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#if (defined(U_BUILD_ENABLE_COMMANDLETS) || defined(U_BUILD_ENABLE_TESTS))

#include "uBroadPhase.h"
#include "uSisCanonicalTrialRunnerMixin.h"
#include "uSisEllipsoidNuclearMixin.h"
#include "uSisHardShellCollisionMixin.h"
#include "uSisHetrNodeRadiusMixin.h"
#include "uSisHomgNodeRadiusMixin.h"
#include "uSisLmrkUtils.h"
#include "uSisNullIntrChrMixin.h"
#include "uSisNullIntrLamMixin.h"
#include "uSisNullIntrNucbMixin.h"
#include "uSisNullQcSimLevelMixin.h"
#include "uSisParallelBatchGrower.h"
#include "uSisSample.h"
#include "uSisSeoBendEnergyMixin.h"
#include "uSisSeoMlocGrowthMixin.h"
#include "uSisSeoSlocGrowthMixin.h"
#include "uSisSeoUnifCubeSeedMixin.h"
#include "uSisSim.h"
#include "uSisSphereNuclearMixin.h"
#include "uSisUnifEnergyMixin.h"

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro for declaring a library glue with no quality control
 */
#define U_MOCKUP_DECLARE_NO_QC_GLUE(t_Glue,                           \
                                    t_Growth,                         \
                                    t_GrowthSim,                      \
                                    t_Rad,                            \
                                    t_RadSim,                         \
                                    t_IntrChr,                        \
                                    t_IntrChrSim,                     \
                                    t_IntrLam,                        \
                                    t_IntrLamSim,                     \
                                    t_IntrNucb,                       \
                                    t_IntrNucbSim,                    \
                                    t_Ener,                           \
                                    t_EnerSim)                        \
                                                                      \
    class t_Glue {                                                    \
                                                                      \
    public:                                                           \
        typedef uSisSim<t_Glue> sim_t;                                \
        typedef uSisSample<t_Glue> sample_t;                          \
        typedef uSisCanonicalTrialRunnerMixin<                        \
            t_Glue,                                                   \
            uSisParallelBatchGrower<t_Glue> >                         \
            trial_runner_mixin_t;                                     \
        typedef uSisNullQcSimLevelMixin<t_Glue> sim_level_qc_mixin_t; \
        typedef t_GrowthSim<t_Glue> sim_level_growth_mixin_t;         \
        typedef t_Growth<t_Glue> growth_mixin_t;                      \
        typedef t_RadSim<t_Glue> sim_level_node_radius_mixin_t;       \
        typedef t_Rad<t_Glue> node_radius_mixin_t;                    \
        typedef uSisSphereNuclearSimLevelMixin_threaded<t_Glue>       \
            sim_level_nuclear_mixin_t;                                \
        typedef uSisSphereNuclearMixin<t_Glue> nuclear_mixin_t;       \
        typedef uSisHardShellCollisionSimLevelMixin_threaded<t_Glue>  \
            sim_level_collision_mixin_t;                              \
        typedef uSisHardShellCollisionMixin<t_Glue, uBroadPhase_t>    \
            collision_mixin_t;                                        \
        typedef t_IntrChrSim<t_Glue> sim_level_intr_chr_mixin_t;      \
        typedef t_IntrChr<t_Glue> intr_chr_mixin_t;                   \
        typedef t_IntrLamSim<t_Glue> sim_level_intr_lam_mixin_t;      \
        typedef t_IntrLam<t_Glue> intr_lam_mixin_t;                   \
        typedef t_IntrNucbSim<t_Glue> sim_level_intr_nucb_mixin_t;    \
        typedef t_IntrNucb<t_Glue> intr_nucb_mixin_t;                 \
        typedef t_EnerSim<t_Glue> sim_level_energy_mixin_t;           \
        typedef t_Ener<t_Glue> energy_mixin_t;                        \
        typedef uSisSeoUnifCubeSeedMixin<t_Glue> seed_mixin_t;        \
    }

/**
 * Macro for declaring a library glue with no quality control and no
 * lamina or interaction constraints
 */
#define U_MOCKUP_DECLARE_NULL_GLUE(                                    \
    t_Glue, t_Growth, t_GrowthSim, t_Rad, t_RadSim, t_Ener, t_EnerSim) \
    U_MOCKUP_DECLARE_NO_QC_GLUE(t_Glue,                                \
                                t_Growth,                              \
                                t_GrowthSim,                           \
                                t_Rad,                                 \
                                t_RadSim,                              \
                                uSisNullIntrChrMixin,                  \
                                uSisNullIntrChrSimLevelMixin,          \
                                uSisNullIntrLamMixin,                  \
                                uSisNullIntrLamSimLevelMixin,          \
                                uSisNullIntrNucbMixin,                 \
                                uSisNullIntrNucbSimLevelMixin,         \
                                t_Ener,                                \
                                t_EnerSim)

/**
 * Macro declares library glue with no quality control and ellipsoidal nucleus
 */
#define U_MOCKUP_DECLARE_ELLIPNUC_NO_QC_GLUE(t_Glue,                  \
                                             t_Growth,                \
                                             t_GrowthSim,             \
                                             t_Rad,                   \
                                             t_RadSim,                \
                                             t_IntrChr,               \
                                             t_IntrChrSim,            \
                                             t_IntrLam,               \
                                             t_IntrLamSim,            \
                                             t_IntrNucb,              \
                                             t_IntrNucbSim,           \
                                             t_Ener,                  \
                                             t_EnerSim)               \
                                                                      \
    class t_Glue {                                                    \
                                                                      \
    public:                                                           \
        typedef uSisSim<t_Glue> sim_t;                                \
        typedef uSisSample<t_Glue> sample_t;                          \
        typedef uSisCanonicalTrialRunnerMixin<                        \
            t_Glue,                                                   \
            uSisParallelBatchGrower<t_Glue> >                         \
            trial_runner_mixin_t;                                     \
        typedef uSisNullQcSimLevelMixin<t_Glue> sim_level_qc_mixin_t; \
        typedef t_GrowthSim<t_Glue> sim_level_growth_mixin_t;         \
        typedef t_Growth<t_Glue> growth_mixin_t;                      \
        typedef t_RadSim<t_Glue> sim_level_node_radius_mixin_t;       \
        typedef t_Rad<t_Glue> node_radius_mixin_t;                    \
        typedef uSisEllipsoidNuclearSimLevelMixin_threaded<t_Glue>    \
            sim_level_nuclear_mixin_t;                                \
        typedef uSisEllipsoidNuclearMixin<t_Glue> nuclear_mixin_t;    \
        typedef uSisHardShellCollisionSimLevelMixin_threaded<t_Glue>  \
            sim_level_collision_mixin_t;                              \
        typedef uSisHardShellCollisionMixin<t_Glue, uBroadPhase_t>    \
            collision_mixin_t;                                        \
        typedef t_IntrChrSim<t_Glue> sim_level_intr_chr_mixin_t;      \
        typedef t_IntrChr<t_Glue> intr_chr_mixin_t;                   \
        typedef t_IntrLamSim<t_Glue> sim_level_intr_lam_mixin_t;      \
        typedef t_IntrLam<t_Glue> intr_lam_mixin_t;                   \
        typedef t_IntrNucbSim<t_Glue> sim_level_intr_nucb_mixin_t;    \
        typedef t_IntrNucb<t_Glue> intr_nucb_mixin_t;                 \
        typedef t_EnerSim<t_Glue> sim_level_energy_mixin_t;           \
        typedef t_Ener<t_Glue> energy_mixin_t;                        \
        typedef uSisSeoUnifCubeSeedMixin<t_Glue> seed_mixin_t;        \
    }

//****************************************************************************
// uSisUtilsMockUp namespace
//****************************************************************************

namespace uMockUpUtils {
/**
 * A library glue for single end growth, single locus, homogeneous radius,
 * uniform sampling, no quality control, no interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Unif: Uniform energy function (all candidates have equal energy)
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Sloc: Single locus
 *  - Homg: Homogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisUnifSeoSlocHomgNullGlue,
                           uSisSeoSloc::GrowthMixin,
                           uSisSeoSloc::GrowthSimLevelMixin_threaded,
                           uSisHomgNodeRadiusMixin,
                           uSisHomgNodeRadiusSimLevelMixin,
                           uSisUnifEnergyMixin,
                           uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, single locus, heterogeneous radius,
 * uniform sampling, no quality control, no interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Unif: Uniform energy function (all candidates have equal energy)
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Sloc: Single locus
 *  - Hetr: Heterogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisUnifSeoSlocHetrNullGlue,
                           uSisSeoSloc::GrowthMixin,
                           uSisSeoSloc::GrowthSimLevelMixin_threaded,
                           uSisHetrNodeRadiusMixin,
                           uSisHetrNodeRadiusSimLevelMixin,
                           uSisUnifEnergyMixin,
                           uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, single locus, homogeneous radius,
 * Boltzmann bend energy distribution sampling, no quality control, no
 * interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Bend: Boltzmann bend energy distribution sampling
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Sloc: Single locus
 *  - Homg: Homogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisBendSeoSlocHomgNullGlue,
                           uSisSeoSloc::GrowthMixin,
                           uSisSeoSloc::GrowthSimLevelMixin_threaded,
                           uSisHomgNodeRadiusMixin,
                           uSisHomgNodeRadiusSimLevelMixin,
                           uSisSeoBendEnergyMixin,
                           uSisSeoBendEnergySimLevelMixin_threaded);

/**
 * A library glue for single end growth, single locus, heterogeneous radius,
 * Boltzmann bend energy distribution sampling, no quality control, no
 * interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Bend: Boltzmann bend energy distribution sampling
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Sloc: Single locus
 *  - Hetr: Heterogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisBendSeoSlocHetrNullGlue,
                           uSisSeoSloc::GrowthMixin,
                           uSisSeoSloc::GrowthSimLevelMixin_threaded,
                           uSisHetrNodeRadiusMixin,
                           uSisHetrNodeRadiusSimLevelMixin,
                           uSisSeoBendEnergyMixin,
                           uSisSeoBendEnergySimLevelMixin_threaded);

/**
 * A library glue for single end growth, multiple loci, homogeneous radius,
 * uniform sampling, no quality control, no interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Unif: Uniform energy function (all candidates have equal energy)
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Mloc: Multiple loci
 *  - Homg: Homogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisUnifSeoMlocHomgNullGlue,
                           uSisSeoMloc::GrowthMixin,
                           uSisSeoMloc::GrowthSimLevelMixin_threaded,
                           uSisHomgNodeRadiusMixin,
                           uSisHomgNodeRadiusSimLevelMixin,
                           uSisUnifEnergyMixin,
                           uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, multiple loci, heterogeneous radius,
 * uniform sampling, no quality control, no interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Unif: Uniform energy function (all candidates have equal energy)
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Mloc: Multiple loci
 *  - Hetr: Heterogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisUnifSeoMlocHetrNullGlue,
                           uSisSeoMloc::GrowthMixin,
                           uSisSeoMloc::GrowthSimLevelMixin_threaded,
                           uSisHetrNodeRadiusMixin,
                           uSisHetrNodeRadiusSimLevelMixin,
                           uSisUnifEnergyMixin,
                           uSisUnifEnergySimLevelMixin);

/**
 * A library glue for single end growth, multiple loci, homogeneous radius,
 * Boltzmann bend energy distribution sampling, no quality control, no
 * interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Bend: Boltzmann bend energy distribution sampling
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Mloc: Multiple loci
 *  - Homg: Homogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisBendSeoMlocHomgNullGlue,
                           uSisSeoMloc::GrowthMixin,
                           uSisSeoMloc::GrowthSimLevelMixin_threaded,
                           uSisHomgNodeRadiusMixin,
                           uSisHomgNodeRadiusSimLevelMixin,
                           uSisSeoBendEnergyMixin,
                           uSisSeoBendEnergySimLevelMixin_threaded);

/**
 * A library glue for single end growth, multiple loci, heterogeneous radius,
 * Boltzmann bend energy distribution sampling, no quality control, no
 * interaction constraints
 *  - Sis: Sequential importance sampling
 *  - Bend: Boltzmann bend energy distribution sampling
 *  - Seo: Single-end ordered (all nodes within a locus are grown in order
 *      from first node to last node
 *  - Mloc: Multiple loci
 *  - Hetr: Heterogeneous radius
 *  - Null: No interaction constraints (also no quality control in this case)
 */
U_MOCKUP_DECLARE_NULL_GLUE(uSisBendSeoMlocHetrNullGlue,
                           uSisSeoMloc::GrowthMixin,
                           uSisSeoMloc::GrowthSimLevelMixin_threaded,
                           uSisHetrNodeRadiusMixin,
                           uSisHetrNodeRadiusSimLevelMixin,
                           uSisSeoBendEnergyMixin,
                           uSisSeoBendEnergySimLevelMixin_threaded);

}  // namespace uMockUpUtils

#endif  // (defined(U_BUILD_ENABLE_COMMANDLETS) ||
        // defined(U_BUILD_ENABLE_TESTS))

#endif  // uMockUpUtils_h
