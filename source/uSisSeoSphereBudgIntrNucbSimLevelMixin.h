//****************************************************************************
// uSisSeoSphereBudgIntrNucbSimLevelMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * Seo: Single end, ordered growth - nodes grown sequentially from first to
 *  last and starting from same end always
 * Budg: Interaction constraint failures allowed up to budget
 *
 * A nuclear body interaction mixin manages nuclear body association
 *  constraints. Note, this is different from a "nuclear mixin" which is used
 *  for modeling confinement by constraining all monomer beads to reside
 *  within the nuclear volume. In contrast, the nuclear body interaction mixin
 *  enforces polymer fragments to be proximal or distal to a specific
 *  spherical mass (x, y, z) coordinate as configured by the user.
 *  Technically, this mass does not have to reside within nuclear volume.
 *
 * The simulation mixin stores common data used by all polymer samples. This
 * mixin supports a failure budget and assumes spherical nuclear bodies.
 */

#ifndef uSisSeoSphereBudgIntrNucbSimLevelMixin_h
#define uSisSeoSphereBudgIntrNucbSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uIntrLibConfig.h"
#include "uIntrLibInit.h"
#include "uLogf.h"
#include "uSisIntrNucbBudgUtil.h"
#include "uSisSeoSphereIntrNucbSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uStaticAssert.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSeoSphereBudgIntrNucbSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisSeoSphereBudgIntrNucbSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisSeoSphereBudgIntrNucbSimLevelMixin_h
