//****************************************************************************
// uDistUtils.h
//***************************************************************************

/**
 * Not pretty but we are using macros to force inlining for performance reasons
 * but still adhere to DRY principle. These macros expect 3 linear buffers each
 * of size 3 but bounds checking is not enforced!
 */

#ifndef uDistUtils_h
#define uDistUtils_h

#include "uBuild.h"
#include "uAssert.h"
#include "uIsFinite.h"
#include "uTypes.h"

#include <math.h>

// Wrapping multi-line macros in do { ... } while(0) so that they behave
// similar to single-line statements.
// http://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros

/**
 * Computes squared distance between points and b
 * @param dist2 - 3-D buffer for storing intermediate deltas.
 *  The final squared distance is stored in dist2[uDim_dist]
 * @param a - first 3-D point
 * @param b - second 3-D point
 */
#define U_DIST2(dist2, a, b)                                       \
    do {                                                           \
        uAssert(uDim_dist == uDim_X);                              \
        dist2[uDim_dist] = a[uDim_X] - b[uDim_X];                  \
        dist2[uDim_dist] *= dist2[uDim_dist];                      \
        dist2[uDim_Y] = a[uDim_Y] - b[uDim_Y];                     \
        dist2[uDim_dist] += (dist2[uDim_Y] * dist2[uDim_Y]);       \
        dist2[uDim_Z] = a[uDim_Z] - b[uDim_Z];                     \
        dist2[uDim_dist] += (dist2[uDim_Z] * dist2[uDim_Z]);       \
        uAssert((dist2[uDim_dist] + U_REAL_EPS) > U_TO_REAL(0.0)); \
    } while (0)

/**
 * Computes squared distance between points and b
 * @param dist - 3-D buffer for storing intermediate deltas.
 *  The final squared distance is stored in dist2[uDim_dist]
 * @param a - first 3-D point
 * @param b - second 3-D point
 */
#define U_DIST(dist, a, b)                       \
    do {                                         \
        U_DIST2(dist, a, b);                     \
        dist[uDim_dist] = sqrt(dist[uDim_dist]); \
        uAssert(uIsFinite(dist[uDim_dist]));     \
    } while (0)

#endif  // uDistUtils_h
