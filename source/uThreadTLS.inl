//****************************************************************************
// uThreadTLS.inl
//****************************************************************************

/**
 * Used for (re)setting TLS accessor macros based on build conditions
 */

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_THREADS

// Wrapping multi-line macros in do { ... } while(0) so that they behave
// similar to single-line statements.
// http://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros

/**
 * Undefine any previously existing macros
 */

#ifdef U_DECLARE_TLS_DATA
#   undef U_DECLARE_TLS_DATA
#endif  // U_DECLARE_TLS_DATA

#ifdef U_ACCESS_TLS_DATA
#   undef U_ACCESS_TLS_DATA
#endif  // U_ACCESS_TLS_DATA

#ifdef U_INIT_TLS_DATA_0_PARAM
#   undef U_INIT_TLS_DATA_0_PARAM
#endif  // U_INIT_TLS_DATA_0_PARAM

#ifdef U_INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef U_INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // U_INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef U_INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef U_INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // U_INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef U_TLS_DATA_FOR_EACH_0_PARAM
#   undef U_TLS_DATA_FOR_EACH_0_PARAM
#endif  // U_TLS_DATA_FOR_EACH_0_PARAM

#ifdef U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB
#   undef U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB
#endif  // U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB

#ifdef U_TLS_DATA_FOR_EACH_1_PARAM
#   undef U_TLS_DATA_FOR_EACH_1_PARAM
#endif  // U_TLS_DATA_FOR_EACH_1_PARAM

#define U_DECLARE_TLS_DATA(type, name) std::vector<type> name

#define U_ACCESS_TLS_DATA(name) name[U_THREAD_ID_VAR_NAME]

/**
 * @Warning
 * TLS Data is allocated under the assumption that the worker threads and main
 * thread are never running/accessing TLS at the same time. If this is ever
 * *NOT* the case - then we must allocate an extra slot for the main thread,
 * else it just reuses the TLS data allocated for the 0th worker thread.
 *
 * Also, if main and worker are accessing TLS, then the thread ids of the
 * worker threads must start from 1 (and not 0).
 */

#define U_INIT_TLS_DATA_0_PARAM(name)                   \
    do {                                                \
        name.clear();                                   \
        uAssert(uG::U_NUM_WORKER_THREADS_VAR_NAME > 0); \
        name.resize(uG::U_NUM_WORKER_THREADS_VAR_NAME); \
    } while (0)

#define U_INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1)           \
    do {                                                                     \
        U_INIT_TLS_DATA_0_PARAM(name);                                       \
        for (uUInt it_tls_ = 0; it_tls_ < uG::U_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                                    \
            name[it_tls_].memb_func(param_1);                                \
        }                                                                    \
    } while (0)

#define U_INIT_TLS_DATA_FOR_EACH_2_PARAM(name, memb_func, param_1, param_2)  \
    do {                                                                     \
        U_INIT_TLS_DATA_0_PARAM(name);                                       \
        for (uUInt it_tls_ = 0; it_tls_ < uG::U_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                                    \
            name[it_tls_].memb_func(param_1, param_2);                       \
        }                                                                    \
    } while (0)

#define U_TLS_DATA_FOR_EACH_0_PARAM(name, memb_func)                         \
    do {                                                                     \
        for (uUInt it_tls_ = 0; it_tls_ < uG::U_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                                    \
            name[it_tls_].memb_func();                                       \
        }                                                                    \
    } while (0)

#define U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(name, nonmemb_func)              \
    do {                                                                     \
        for (uUInt it_tls_ = 0; it_tls_ < uG::U_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                                    \
            nonmemb_func(name[it_tls_]);                                     \
        }                                                                    \
    } while (0)

#define U_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1)                \
    do {                                                                     \
        for (uUInt it_tls_ = 0; it_tls_ < uG::U_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                                    \
            name[it_tls_].memb_func(param_1);                                \
        }                                                                    \
    } while (0)

#else

#include "uThreadNullTLS.inl"

#endif  // U_BUILD_ENABLE_THREADS
