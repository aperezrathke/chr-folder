//****************************************************************************
// uSisTemplateSampleInfo.h
//****************************************************************************

#ifndef uSisTemplateSampleInfo_h
#define uSisTemplateSampleInfo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uAssert.h"
#include "uTypes.h"

//****************************************************************************
// uSisTemplateSampleInfo
//****************************************************************************

/**
 * Utility structure for managing template sample data
 */
template <typename sample_t>
struct uSisTemplateSampleInfo {
    /**
     * Default constructor
     */
    uSisTemplateSampleInfo() { clear(); }

    /**
     * Initialize info
     * @param sample - the template sample
     * @param num_grow_steps - the number of grow steps completed by the
     *  template sample
     * @param log_weight - the log weight of the template sample
     */
    inline void init(const sample_t& sample,
                     const uUInt num_grow_steps,
                     const uReal log_weight) {
        m_enabled = uTRUE;
        m_sample = sample;
        m_num_grow_steps = num_grow_steps;
        m_log_weight = log_weight;
        uAssert(check_state(uTRUE));
    }

    /**
     * Clear info
     */
    inline void clear() {
        m_enabled = uFALSE;
        m_sample = sample_t();
        m_num_grow_steps = 0;
        m_log_weight = 0;
    }

    /**
     * @return True if information is in correct state, False otherwise
     */
    inline bool check_state() const {
        if (!m_enabled) {
            return true;
        }

        return m_num_grow_steps > 0 && m_log_weight >= U_TO_REAL(0.0);
    }

    /**
     * @return True if information is in expected valid state, False otherwise
     */
    inline bool check_state(const uBool should_be_enabled) const {
        return (should_be_enabled == m_enabled) && check_state();
    }

    /**
     * If true, info contains valid template that can be used for growth
     */
    uBool m_enabled;

    /**
     * The stored template sample
     */
    sample_t m_sample;

    /**
     * The number of grow steps completed by the stored template sample
     */
    uUInt m_num_grow_steps;

    /**
     * The log weight of the template sample
     */
    uReal m_log_weight;
};

#endif  // uSisTemplateSampleInfo_h
