//****************************************************************************
// uChromatinExportPml.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uChromatinExportPml.h"
#include "uAssert.h"
#include "uFilesystem.h"
#include "uIntrLibDefs.h"
#include "uLogf.h"
#include "uStringUtils.h"

#include <fstream>
#include <map>
#include <string>

//****************************************************************************
// Defines
//****************************************************************************

// Set hard-coded transparency of exported nuclear shell
// @TODO - expose as user argument
// https://pymolwiki.org/index.php/Sphere_transparency
// 1.0 will be invisible and 0.0 a completely solid surface.
#define U_CHROMATIN_EXPORT_PML_NUCSHELL_TRANSPARENCY U_TO_REAL(0.67)

//****************************************************************************
// Utilities
//****************************************************************************

/**
 * Utility to generate PDB formatted text string
 */
extern void chromatin_export_get_pdb_text(std::string& out_pdb_text,
                                          const uChromatinExportArgs_t& args);

/**
 * Utility appends nuclear body HETATM records to PDB formatted text string
 * @return unsigned integer resNo of last HETATM record generated, will return
 *  argument 'last_resno' if no records generated
 */
extern uUInt chromatin_export_append_intr_nucb_pdb_text(
    std::string& out_pdb_text,
    const uUInt last_resno,
    const uChromatinExportArgs_t& args);

/**
 * Utility appends nuclear shell HETATM record to PDB formatted text string
 * @TODO - support ellipsoid nucleus with radius in X, Y, Z dims
 * @return unsigned integer resNo of last HETATM record generated, will return
 *  argument 'last_resno' if no records generated
 */
extern uUInt chromatin_export_append_intr_nucshell_pdb_text(
    std::string& out_pdb_text,
    const uUInt last_resno,
    const uChromatinExportArgs_t& args);

namespace {

/**
 * Ordered map from selection name to selection expression
 */
typedef std::map<std::string, std::string> sele_map_t;

/**
 * Info needed for a fragment selection
 */
typedef struct {
    std::string sele_key;
    std::string sele_val;
    std::string nid_lo_str;
    std::string nid_hi_str;
    std::string nid_lohi_str;
    uUInt nid_lo;
    uUInt nid_hi;
} frag_sele_info_t;

/**
 * Utility to create fragment selection expression
 */
void get_frag_sele(frag_sele_info_t& nfo,
                   const std::string& intr_type,
                   const uUIMatrix& frags,
                   const uUInt max_total_num_nodes,
                   const uUInt index) {
    uAssertBounds(index, U_TO_UINT(0), frags.n_cols);
    nfo.nid_lo = frags.at(uIntrLibPairIxMin, index) + U_TO_UINT(1);
    uAssertBoundsInc(nfo.nid_lo, U_TO_UINT(1), max_total_num_nodes);
    nfo.nid_hi = frags.at(uIntrLibPairIxMax, index) + U_TO_UINT(1);
    uAssertBoundsInc(nfo.nid_hi, nfo.nid_lo, max_total_num_nodes);
    nfo.nid_lo_str = u2Str(nfo.nid_lo);
    nfo.nid_hi_str = u2Str(nfo.nid_hi);
    nfo.nid_lohi_str = nfo.nid_lo_str + std::string("-") + nfo.nid_hi_str;
    const std::string div_str("_");
    nfo.sele_key =
        intr_type + div_str + std::string("frag") + div_str + nfo.nid_lohi_str;
    nfo.sele_val = std::string("sele ") + nfo.sele_key +
                   std::string(", resi ") + nfo.nid_lo_str + std::string("-") +
                   nfo.nid_hi_str;
}

/**
 * Utility to create selection expressions for chr-chr interactions
 */
void get_intr_chr_sele(sele_map_t& frags_sele,
                       sele_map_t& intrs_sele,
                       const uUIMatrix& frags,
                       const uUIMatrix& intrs,
                       const std::string& prefix,
                       const uUInt max_total_num_nodes) {

    frag_sele_info_t frag_a;
    frag_sele_info_t frag_b;
    const std::string div_str("_");
    const std::string intr_type("chr");

    for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
        // Fragment A
        const uUInt frag_a_index = intrs.at(uIntrLibPairIxMin, i);
        get_frag_sele(
            frag_a, intr_type, frags, max_total_num_nodes, frag_a_index);
        // Fragment B
        const uUInt frag_b_index = intrs.at(uIntrLibPairIxMax, i);
        get_frag_sele(
            frag_b, intr_type, frags, max_total_num_nodes, frag_b_index);
        // Interaction
        const std::string intr_sele_key = intr_type + div_str + prefix +
                                          div_str + frag_a.nid_lohi_str +
                                          div_str + frag_b.nid_lohi_str;
        const std::string intr_sele_val = std::string("sele ") + intr_sele_key +
                                          std::string(", ") + frag_a.sele_key +
                                          std::string(" | ") + frag_b.sele_key;
        // Store as map to avoid duplicates
        frags_sele[frag_a.sele_key] = frag_a.sele_val;
        frags_sele[frag_b.sele_key] = frag_b.sele_val;
        intrs_sele[intr_sele_key] = intr_sele_val;
    }
}

/**
 * Utility to create selection expressions for lamina interactions
 */
void get_intr_lam_sele(sele_map_t& frags_sele,
                       sele_map_t& intrs_sele,
                       const uUIMatrix& frags,
                       const uUIVecCol& intr,
                       const std::string& prefix,
                       const uUInt max_total_num_nodes) {

    frag_sele_info_t frag;
    const std::string div_str("_");
    const std::string intr_type("lam");

    for (uMatSz_t i = 0; i < intr.n_elem; ++i) {
        // Fragment
        const uUInt frag_index = intr.at(i);
        get_frag_sele(frag, intr_type, frags, max_total_num_nodes, frag_index);
        // Interaction
        const std::string intr_sele_key =
            intr_type + div_str + prefix + div_str + frag.nid_lohi_str;
        const std::string intr_sele_val =
            std::string("sele ") + intr_sele_key + std::string(", resi ") +
            frag.nid_lo_str + std::string("-") + frag.nid_hi_str;
        // Store as map to avoid duplicates
        frags_sele[frag.sele_key] = frag.sele_val;
        intrs_sele[intr_sele_key] = intr_sele_val;
    }
}

/**
 * Info needed for a nuclear body selection
 */
typedef struct {
    std::string sele_key;
    std::string sele_val;
    std::string raw_id;
    std::string het_id;
} nucb_sele_info_t;

/**
 * Ordered map from 0-index to selection expression
 */
typedef std::map<uUInt, nucb_sele_info_t> nucb_map_t;

/**
 * Utility to create single nuclear body selection expression
 */
void get_nucb_sele(nucb_sele_info_t& nfo,
                   const std::string& intr_type,
                   const uUInt max_total_num_nodes,
                   const uUInt index) {
    const std::string div_str("_");
    // 1-based index
    nfo.raw_id = u2Str(index + 1);
    // Actual HETATM record index
    // @TODO - Unclear if this may cause a bug if certain nuclear bodies
    //  are masked out (i.e. never used), specifically if nfo.het_id
    //  is still correct if never referenced by any interactions,
    //  it may be best to export all nuclear bodies HETATM records even
    //  if not referenced by a knock-in/knock-out interaction
    nfo.het_id = u2Str(max_total_num_nodes + index + 1);
    nfo.sele_key = intr_type + div_str + nfo.raw_id;
    nfo.sele_val = std::string("sele ") + nfo.sele_key +
                   std::string(", resi ") + nfo.het_id;
}

/**
 * Utility to create selection expressions for nuclear body interactions
 */
void get_intr_nucb_sele(sele_map_t& frags_sele,
                        sele_map_t& intrs_sele,
                        sele_map_t& bodys_sele,
                        nucb_map_t& bodys_nfo,
                        const uUIMatrix& frags,
                        const uUIMatrix& intrs,
                        const std::string& prefix,
                        const uUInt max_total_num_nodes) {

    frag_sele_info_t frag;
    nucb_sele_info_t body;
    const std::string div_str("_");
    const std::string intr_type("nucb");

    for (uMatSz_t i = 0; i < intrs.n_cols; ++i) {
        // Fragment
        const uUInt frag_index = intrs.at(uIntrNucbPairIxFrag, i);
        get_frag_sele(frag, intr_type, frags, max_total_num_nodes, frag_index);
        // Body
        const uUInt body_index = intrs.at(uIntrNucbPairIxBody, i);
        get_nucb_sele(body, intr_type, max_total_num_nodes, body_index);
        bodys_nfo[body_index] = body;
        // Interaction
        const std::string intr_sele_key = intr_type + div_str + body.raw_id +
                                          div_str + prefix + div_str +
                                          frag.nid_lohi_str;
        const std::string intr_sele_val = std::string("sele ") + intr_sele_key +
                                          std::string(", ") + frag.sele_key +
                                          std::string(" | ") + body.sele_key;
        // Store as map to avoid duplicates
        frags_sele[frag.sele_key] = frag.sele_val;
        bodys_sele[body.sele_key] = body.sele_val;
        intrs_sele[intr_sele_key] = intr_sele_val;
    }
}

/**
 * Utility to write fragment or interaction selection expressions
 */
void write_intr_sele(std::ostream& out, sele_map_t& seles) {
    for (sele_map_t::iterator it = seles.begin(); it != seles.end(); ++it) {
        out << it->second << std::endl;
    }
}

}  // namespace

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Writes sample node positions to PML format. PML files can be opened
 * directly in PyMOL and may contain embedded PyMOL script commands.
 * @WARNING: The embedded script may possibly include arbitrary python
 *  commands, careful when opening (view in text editor first!)
 *
 * @param fpath - path to write the exported sample
 * @param args - exporter arguments, note 'scale' is applied to all node
 *  positions, this helps conform to the PDB format as otherwise the
 *  coordinates may overflow alloted character column boundaries.
 */
void uChromatinExportPml::write_chromatin(const std::string& fpath,
                                          const uChromatinExportArgs_t& args) {
    if (!chromatin_export_check(args)) {
        std::cout << "Error, unable to export sample.\n";
        return;
    }

    uFs_create_parent_dirs(fpath);
    std::ofstream fout(fpath.c_str(), std::ofstream::out);

    if (!fout) {
        std::cout << "Warning, unable to open: " << fpath << std::endl
                  << "\tfor writing to PML format.\n";
        return;
    }

    write_chromatin(fout, args);
    fout.close();
}

/**
 * Write sample node positions to 'out' stream
 * @WARNING - DOES NOT CLOSE STREAM!
 * @param out - output stream
 * @param args - exporter args
 */
void uChromatinExportPml::write_chromatin(std::ostream& out,
                                          const uChromatinExportArgs_t& args) {
    // Obtain PDB text body
    std::string pdb_text;
    chromatin_export_get_pdb_text(pdb_text, args);

    // Conditionally incorporate nuclear bodies into PDB
    uUInt last_resno = U_TO_MAT_SZ_T(args.p_node_positions->n_cols);
    if (args.intr_nucb) {
        last_resno = chromatin_export_append_intr_nucb_pdb_text(
            pdb_text, last_resno, args);
    }

    // Conditionally incorporate nuclear shell into PDB
    // @WARNING - DOWNSTREAM CODE MAY ASSUME THAT NUCLEAR BODIES ARE FIRST SET
    //  OF HETATM RECORDS, DO NOT SWITCH ORDER OF EXPORT!
    uUInt nucshell_resno = U_TO_UINT(0);
    bool nucshell_any = false;
    if (args.nuclear_shell) {
        nucshell_resno = chromatin_export_append_intr_nucshell_pdb_text(
            pdb_text, last_resno, args);
        nucshell_any = nucshell_resno > last_resno;
        last_resno = nucshell_resno;
    }

    // Read PDB coordinates from string
    out << "cmd.read_pdbstr(\"\"\"\\" << std::endl;
    std::stringstream line_stream(pdb_text);
    std::string line;
    while (std::getline(line_stream, line)) {
        out << line << "\\" << std::endl;
    }
    out << "\"\"\",\"chromatin\")" << std::endl;

    // Scale all Van der Waals radii if heterogeneous
    if (uMatrixUtils::any((*args.p_node_radii) != args.p_node_radii->at(0))) {
        for (uUInt i = 0; i < U_TO_UINT(args.p_node_radii->n_elem);) {
            const uReal radius = args.p_node_radii->at(i);
            // Detect runs of consecutive radii, massive savings in
            // PyMOL start-up time
            uUInt j = i + 1;
            while ((j < U_TO_UINT(args.p_node_radii->n_elem)) &&
                   U_REAL_CMP_EQ(args.p_node_radii->at(j), radius)) {
                ++j;
            }
            if ((j - i) > 1) {
                // Batch multiple consecutive nodes with same radii
                out << "alter (resi " << (i + 1) << "-" << j << ")";
            } else {
                // Adjacent node does not have same radii
                out << "alter resi " << (i + 1);
            }
            out << ", vdw = " << args.p_node_radii->at(i) * args.scale
                << std::endl;
            i = j;
        }
        out << "rebuild" << std::endl;
    }

    // Create selections for individual chains for easier coloring
    if (args.p_max_num_nodes_at_locus->size() > 1) {
        uUInt start_node = 0;
        const uUInt num_loci = U_TO_UINT(args.p_max_num_nodes_at_locus->size());
        for (uUInt i = 0; i < num_loci; ++i) {
            out << "sele "
                << "locus_" << (i + 1) << ", resi " << (start_node + 1) << "-"
                << (start_node + (*args.p_max_num_nodes_at_locus)[i])
                << std::endl;
            start_node += (*args.p_max_num_nodes_at_locus)[i];
        }
    }

    // Selections for chr-chr (knock-in and/or knock-out) interactions
    if (args.intr_chr && args.p_intr_chr_frags) {
        sele_map_t frags_sele;
        sele_map_t intrs_sele;
        // Collect knock-in selections
        if (args.p_intr_chr_kin) {
            get_intr_chr_sele(frags_sele,
                              intrs_sele,
                              *args.p_intr_chr_frags,
                              *args.p_intr_chr_kin,
                              "ki" /*prefix*/,
                              U_TO_UINT(args.p_node_positions->n_cols));
        }
        // Collect knock-out selections
        if (args.p_intr_chr_ko) {
            get_intr_chr_sele(frags_sele,
                              intrs_sele,
                              *args.p_intr_chr_frags,
                              *args.p_intr_chr_ko,
                              "ko" /*prefix*/,
                              U_TO_UINT(args.p_node_positions->n_cols));
        }
        // Write fragment selections
        write_intr_sele(out, frags_sele);
        // Write interaction selections
        write_intr_sele(out, intrs_sele);
    }

    // Selections for lamina (knock-in and/or knock-out) interactions
    if (args.intr_lam && args.p_intr_lam_frags) {
        sele_map_t frags_sele;
        sele_map_t intrs_sele;
        // Collect knock-in selections
        if (args.p_intr_lam_kin) {
            get_intr_lam_sele(frags_sele,
                              intrs_sele,
                              *args.p_intr_lam_frags,
                              *args.p_intr_lam_kin,
                              "ki" /*prefix*/,
                              U_TO_UINT(args.p_node_positions->n_cols));
        }
        // Collect knock-out selections
        if (args.p_intr_lam_ko) {
            get_intr_lam_sele(frags_sele,
                              intrs_sele,
                              *args.p_intr_lam_frags,
                              *args.p_intr_lam_ko,
                              "ko" /*prefix*/,
                              U_TO_UINT(args.p_node_positions->n_cols));
        }
        // Write fragment selections
        write_intr_sele(out, frags_sele);
        // Write interaction selections
        write_intr_sele(out, intrs_sele);
    }

    // Selections for nuclear body (knock-in and/or knock-out) interactions
    if (args.intr_nucb && args.p_intr_nucb_frags) {
        sele_map_t frags_sele;
        sele_map_t intrs_sele;
        sele_map_t bodys_sele;
        nucb_map_t bodys_nfo;
        // Collect knock-in selections
        if (args.p_intr_nucb_kin) {
            get_intr_nucb_sele(frags_sele,
                               intrs_sele,
                               bodys_sele,
                               bodys_nfo,
                               *args.p_intr_nucb_frags,
                               *args.p_intr_nucb_kin,
                               "ki" /*prefix*/,
                               U_TO_UINT(args.p_node_positions->n_cols));
        }
        // Collect knock-out selections
        if (args.p_intr_nucb_ko) {
            get_intr_nucb_sele(frags_sele,
                               intrs_sele,
                               bodys_sele,
                               bodys_nfo,
                               *args.p_intr_nucb_frags,
                               *args.p_intr_nucb_ko,
                               "ko" /*prefix*/,
                               U_TO_UINT(args.p_node_positions->n_cols));
        }

        // Scale nuclear body Van der Waals radii
        if (!bodys_nfo.empty()) {
            for (nucb_map_t::iterator it = bodys_nfo.begin();
                 it != bodys_nfo.end();
                 ++it) {
                const uUInt body_index = it->first;
                uAssertBounds(body_index,
                              U_TO_UINT(0),
                              U_TO_UINT(args.p_intr_nucb_radii->n_elem));
                const uReal radius = args.p_intr_nucb_radii->at(body_index);
                const nucb_sele_info_t& nfo = it->second;
                out << "alter resi " << nfo.het_id
                    << ", vdw = " << radius * args.scale << std::endl;
            }
            out << "rebuild" << std::endl;
        }

        // Write nuclear body selections
        write_intr_sele(out, bodys_sele);
        // Write fragment selections
        write_intr_sele(out, frags_sele);
        // Write interaction selections
        write_intr_sele(out, intrs_sele);
    }

    // Nuclear shell selection and scale nuclear shell HETATM if present
    // @TODO - support ellipsoidal nucleus
    if (args.nuclear_shell && nucshell_any) {
        // Nuclear shell selection
        const std::string nucshell_sele_key("nuc_shell");
        const std::string nucshell_sele_val(
            std::string("sele ") + nucshell_sele_key + std::string(", resi ") +
            u2Str(nucshell_resno));
        out << nucshell_sele_val << std::endl;
        // Make nuclear shell transparent
        // https://pymolwiki.org/index.php/Sphere_transparency
        const std::string nucshell_set_trans(
            std::string("set sphere_transparency, ") +
            u2Str(U_CHROMATIN_EXPORT_PML_NUCSHELL_TRANSPARENCY) +
            std::string(", ") + nucshell_sele_key);
        out << nucshell_set_trans << std::endl;
        // Scale nuclear radius
        const uReal radius = args.nuclear_radius * args.scale;
        out << "alter resi " << nucshell_resno << ", vdw = " << radius
            << std::endl;
        // Rebuild geometry
        out << "rebuild" << std::endl;
    }
}

/**
 * Warn user of potential formatting issues based on polymer size
 * @param max_total_num_nodes - polymer size
 */
void uChromatinExportPml::warn_format(const uUInt max_total_num_nodes) {
    if (max_total_num_nodes >= U_TO_UINT(10000)) {
        uLogf(
            "WARNING: NON-STANDARD PML EXPORT DETECTED FOR POLYMER OF SIZE >= "
            "10000!\n\tPML MAY NOT RENDER PROPERLY IN VIEWER.\n");
    }
}
