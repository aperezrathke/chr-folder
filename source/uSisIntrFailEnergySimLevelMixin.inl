//****************************************************************************
// uSisIntrFailEnergySimLevelMixin.inl
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 * IntrFail: Interaction Failure
 *
 * Sample energy is proportional to [budgeted] interaction constraint
 *  failure fraction
 */

//****************************************************************************
// Simulation level mixin
//****************************************************************************
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisIntrFailEnergySimLevelMixin_threaded
#else
class uSisIntrFailEnergySimLevelMixin_serial
#endif  // U_INJECT_THREADED_TLS
{
private:
    // Array type for storing scalars related to interaction failure energies
    typedef typename uVecCol::fixed<eEnerIntrFail_num> uVecIntrFailScalar_t;

public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        clear();
        ////////////////////////////////////////////////////////////
        // Configure failure energy scales

        uAssertPosEq(m_ener_intr_fail_scale.n_elem,
                     U_TO_MAT_SZ_T(eEnerIntrFail_num));
        int iopt_scale = static_cast<int>(uOpt_energy_intr_fail_scale_chr_kin);
        for (uMatSz_t ix_scale = U_TO_MAT_SZ_T(0);
             ix_scale < U_TO_MAT_SZ_T(eEnerIntrFail_num);
             ++ix_scale) {
            const enum uOptE opt_scale = static_cast<enum uOptE>(iopt_scale);
            uAssertBounds(
                ix_scale, U_TO_MAT_SZ_T(0), m_ener_intr_fail_scale.n_elem);
            config->read_into(m_ener_intr_fail_scale.at(ix_scale), opt_scale);
            uLogf("%s = %f\n",
                  uOpt_get_ini_key(opt_scale),
                  (double)m_ener_intr_fail_scale.at(ix_scale));
            ++iopt_scale;
        }

        ////////////////////////////////////////////////////////////
        // Pre-modulate scales by fail budget limit

        // Chr-chr knock-in
        m_ener_intr_fail_scale.at(eEnerIntrFail_chr_kin) /=
            std::max(U_TO_REAL(sim.get_intr_chr_kin_max_fail()), U_REAL_EPS);

        // Chr-chr knock-out
        m_ener_intr_fail_scale.at(eEnerIntrFail_chr_ko) /=
            std::max(U_TO_REAL(sim.get_intr_chr_ko_max_fail()), U_REAL_EPS);

        // Lamina knock-in
        m_ener_intr_fail_scale.at(eEnerIntrFail_lam_kin) /=
            std::max(U_TO_REAL(sim.get_intr_lam_kin_max_fail()), U_REAL_EPS);

        // Lamina knock-out
        m_ener_intr_fail_scale.at(eEnerIntrFail_lam_ko) /=
            std::max(U_TO_REAL(sim.get_intr_lam_ko_max_fail()), U_REAL_EPS);

        // Nuclear body knock-in
        m_ener_intr_fail_scale.at(eEnerIntrFail_nucb_kin) /=
            std::max(U_TO_REAL(sim.get_intr_nucb_kin_max_fail()), U_REAL_EPS);

        // Nuclear body knock-out
        m_ener_intr_fail_scale.at(eEnerIntrFail_nucb_ko) /=
            std::max(U_TO_REAL(sim.get_intr_nucb_ko_max_fail()), U_REAL_EPS);

        ////////////////////////////////////////////////////////////
        // Allocate TLS

        // This assumes sphere sample points have been initialized!
        const uUInt num_candidates = sim.get_num_unit_sphere_sample_points();
        uAssert(num_candidates > U_TO_UINT(0));

        // Initialize defragmented candidate negative energies buffer
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_ener_cand_defrag_nener, zeros, num_candidates);

        // Initialize defragmented candidate probability mass buffer
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_ener_cand_defrag_pmass, zeros, num_candidates);

        // Initialize buffer for storing mapping from defragmented candidate
        // index to original fragmented index
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_ener_cand_defrag_ix, zeros, num_candidates);
    }

    /**
     * Resets to default state
     */
    void clear() {
        m_ener_intr_fail_scale.zeros();
        m_ener_cand_defrag_nener.clear();
        m_ener_cand_defrag_pmass.clear();
        m_ener_cand_defrag_ix.clear();
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // Accessors

    /**
     * @param eix - Failure energy component index
     * @return Interaction failure energy scale term
     */
    inline uReal get_ener_intr_fail_scale(const enum eEnerIntrFail eix) const {
        uAssertBounds(U_TO_MAT_SZ_T(eix),
                      U_TO_MAT_SZ_T(0),
                      m_ener_intr_fail_scale.n_elem);
        return m_ener_intr_fail_scale.at(U_TO_MAT_SZ_T(eix));
    }

    /**
     * @return Fixed size candidate negative energy buffer
     */
    inline uVecCol& get_ener_cand_defrag_nener(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_ener_cand_defrag_nener);
    }

    /**
     * @return Fixed size candidate exp(-energy) = probability mass buffer
     */
    inline uVecCol& get_ener_cand_defrag_pmass(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_ener_cand_defrag_pmass);
    }

    /**
     * @return Fixed size candidate positions index buffer
     */
    inline uUIVecCol& get_ener_cand_defrag_ix(U_THREAD_ID_0_PARAM) const {
        return U_ACCESS_TLS_DATA(m_ener_cand_defrag_ix);
    }

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE
private:
    /**
     * Interaction failure energy scales
     */
    uVecIntrFailScalar_t m_ener_intr_fail_scale;

    /**
     * Declaring mutable because parent level simulation is likely to be
     * declared as const. These buffers are for working with "defragmented"
     * candidate data: some candidates will not pass constraint filters
     * and they should be removed prior to energy computation.
     */

    /**
     * Scratch buffer for storing -energies at each candidate
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_ener_cand_defrag_nener);

    /**
     * Scratch buffer for storing exp(-energies) = probability mass at each
     * candidate
     */
    mutable U_DECLARE_TLS_DATA(uVecCol, m_ener_cand_defrag_pmass);

    /**
     * Candidate indices, map from defragmented index to actual index
     */
    mutable U_DECLARE_TLS_DATA(uUIVecCol, m_ener_cand_defrag_ix);
};
