//****************************************************************************
// uSisNullQcSimLevelMixin.h
//****************************************************************************

/**
 * QcMixin = A mixin that performs Quality Control to minimize distance
 *  between target and trial sampling distributions. Examples of algorithms
 *  intended to achieve this include Rejection Control and Resampling. See:
 *
 * Liu, Jun S. Monte Carlo strategies in scientific computing. Springer Science
 *   & Business Media, 2008.
 *
 * This is a null mixin which avoids performing any form of population level
 * quality control.
 */

#ifndef uSisNullQcSimLevelMixin_h
#define uSisNullQcSimLevelMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uSisNullSimLevelMixin.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

/**
 * A null quality control mixin. Performs no quality control.
 */
template <typename t_SisGlue>
class uSisNullQcSimLevelMixin : public uSisNullSimLevelMixin<t_SisGlue> {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Callback for when all samples have been seeded (have placed their
     *  initial nodes)
     * @param sim - parent simulation
     */
    void on_seed_finish(const sim_t& sim) {}

    /**
     * Callback for when all samples have finished a single grow phase
     * @param status - The current status of each trial sample - 1 means
     *  chain is still growing, 0 means chain is no longer growing. May be
     *  modified by this function call.
     * @param log_weights - The current log weights of each sample. May be
     *  modified by this function call.
     * @param samples - the current set of trial samples. May be modified by
     *  this function call.
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param sim - The parent simulation
     */
    void on_grow_phase_finish(uWrappedBitset& status,
                              uVecCol& log_weights,
                              std::vector<sample_t>& samples,
                              const uUInt num_completed_grow_steps_per_sample,
                              const sim_t& sim U_THREAD_ID_PARAM) {}

    /**
     * Hook for when a simulation run_from_template_until() trial has finished
     * @param trial_log_weights - The final sample log weights for the current
     *  trial
     * @param prev_num_completed_samples - The number of completed samples
     *  prior to latest batch being added
     */
    void on_templated_trial_finish(
        const uVecCol& trial_log_weights,
        const uUInt prev_num_completed_samples) const {}

    /**
     * Hook for when a simulation run_from_template_until() call is about to
     * finish.
     * @param completed_log_weights - The current output log weights
     */
    void on_templated_run_finish(uVecCol& completed_log_weights) const {}

    U_SIS_DECLARE_MIXIN_NULL_TRIAL_FINISH_INTERFACE

    U_SIS_DECLARE_MIXIN_NULL_RUN_FINISH_INTERFACE
};

#endif  // uSisNullQcSimLevelMixin_h
