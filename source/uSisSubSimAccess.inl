//****************************************************************************
// uSisSubSimAccess.inl
//****************************************************************************

/**
 * Manages access to a sub-simulation object.
 * Allows definition of _threaded and _serial versions.
 */
template <typename t_SisSubSim>
#ifdef U_INJECT_THREADED_TLS
class uSisSubSimAccess_threaded
#else
class uSisSubSimAccess_serial
#endif  // U_INJECT_THREADED_TLS
{
public:
    typedef t_SisSubSim sim_t;

    /**
     * Initializes sub-simulation
     * @param sub_sim_cfg - user configuration options
     */
    void init(uSpConstConfig_t sub_sim_cfg U_THREAD_ID_PARAM) {
        // Initialize sub-simulation
#ifdef U_BUILD_ENABLE_THREADS
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(
            m_sub_sim, init, sub_sim_cfg, U_THREAD_ID_VAR_NAME);
#else
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(m_sub_sim, init, sub_sim_cfg);
#endif
    }

    /**
     * Light-weight re-initialization of sub-simulation object
     */
    void init(U_THREAD_ID_0_PARAM) {
#ifdef U_BUILD_ENABLE_THREADS
        U_TLS_DATA_FOR_EACH_1_PARAM(m_sub_sim, init, U_THREAD_ID_VAR_NAME);
#else
        U_TLS_DATA_FOR_EACH_0_PARAM(m_sub_sim, init);
#endif
    }

    /**
     * Clear sub-simulation to default state
     */
    void clear() { m_sub_sim.clear(); }

    /**
     * @return thread-specific sub-simulation object
     */
    inline t_SisSubSim& get_sub_sim(U_THREAD_ID_0_PARAM) {
        return U_ACCESS_TLS_DATA(m_sub_sim);
    }

    /**
     * Sub-simulation used for generating sub-ensembles between landmark states
     */
    U_DECLARE_TLS_DATA(t_SisSubSim, m_sub_sim);
};
