//****************************************************************************
// uBitsetCompileTimeProperties.h
//****************************************************************************

#ifndef uBitsetCompileTimeProperties_h
#define uBitsetCompileTimeProperties_h

#include "uBuild.h"
#include "uFindLowestBitPositionUtil.h"

#include <limits>

/**
 * Properties common to all pooled bit set structures
 */
class uBitsetCompileTimeProperties {
public:
    /**
     * Typedefs
     */
    // Make sure hard and soft bit sets have same block width
    // also, ensure types consistent with compiler intrinsics used
    typedef uBitsetUtils::mask_type block_type;
    typedef uBitsetUtils::index_type index_type;
    typedef size_t size_type;
    typedef block_type block_width_type;
    typedef unsigned char byte_type;

    /**
     * Constants
     */
    static const block_width_type bits_per_block =
        ((block_width_type)(std::numeric_limits<block_type>::digits));
    static const size_type npos = static_cast<size_type>(-1);
    static const size_type use_default_value = static_cast<size_type>(-1);

    /**
     * From boost::dynamic_bitset - calculates number of blocks necessary to
     * contain the bit set
     */
    static inline size_type calc_num_blocks(size_type num_bits) {
        return (num_bits / bits_per_block) +
               static_cast<int>(num_bits % bits_per_block != 0);
    }
};

#endif  // uBitsetCompileTimeProperties_h
