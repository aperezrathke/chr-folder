//****************************************************************************
// uSisSeGrowthSimLevelMixin.h
//****************************************************************************

/**
 * Mixin stores pre-allocated scratch buffers used for single end growth
 * Sis = sequential importance sampling
 * Se = Single-end growth is from only one end of chain
 */

#ifndef uSisSeGrowthSimLevelMixin_h
#define uSisSeGrowthSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uSisNullGrowthSimLevelMixin.h"
#include "uTypes.h"

//****************************************************************************
// Sim-level mixin
//****************************************************************************

/**
 * Simulation level mixin - Apologies for the use of ugly macros in order to
 * define multiple class definitions. The multiple class definitions are
 * necessary to maintain DRY principle while still having both serial and
 * threaded implementations of which both can function in a threaded or
 * non-threaded environment.
 *
 * This is mostly useful for having a serial implementation in a multi-
 * threaded environment that can still forward to the proper thread-specific
 * random number generator. In turn, the serial implementation is assumed
 * to be run only on a single thread and therefore does not incur the same
 * level of overhead as the full threaded TLS class implementation.
 */

// Pre-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

// Define implementation with TLS support
#include "uThread.h"
#define U_INJECT_THREADED_TLS
#include "uSisSeGrowthSimLevelMixin.inl"
#undef U_INJECT_THREADED_TLS

// Disable TLS
#include "uThreadNullTLS.inl"
// Define implementation with no TLS support but can still function in a
// threaded environment if needed.
#include "uSisSeGrowthSimLevelMixin.inl"
// Restore thread support if enabled
#include "uThreadTLS.inl"

// Post-check - make sure TLS injection flag is not defined
#ifdef U_INJECT_THREADED_TLS
#   error "U_INJECT_THREADED_TLS should not be defined. Please undefine."
#endif  // U_INJECT_THREADED_TLS

#endif  // uSisSeGrowthSimLevelMixin_h
