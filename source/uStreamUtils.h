//****************************************************************************
// uStreamUtils.h
//****************************************************************************

/**
 * Stream utilities
 */

#ifndef uStreamUtils_h
#define uStreamUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#include <iostream>

//****************************************************************************
// uStreamUtils
//****************************************************************************

/**
 * Effectively a utility namespace
 */
class uStreamUtils {
public:
    /**
     * Based on ZSTR example: cat input stream to output stream
     * @param from - input stream
     * @param to - output stream
     */
    static void cat(std::istream& from, std::ostream& to);
};

#endif  // uStreamUtils_h
