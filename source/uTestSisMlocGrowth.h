//****************************************************************************
// uTestSisMlocGrowth.h
//****************************************************************************

/**
 * Tests homogeneous radius, multiple loci growth with uniform sampling
 *
 * Usage:
 * -test_sis_mloc_growth
 */

#ifdef uTestSisMlocGrowth_h
#   error "Test Sis Mloc Growth included multiple times!"
#endif  // uTestSisMlocGrowth_h
#define uTestSisMlocGrowth_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uMockUpUtils.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uTypes.h"

#include <iostream>
#include <string>

/**
 * Entry point for test
 */
int uTestSisMlocGrowthMain(const uCmdOptsMap& cmd_opts) {
    // Expose final simulation
    typedef uMockUpUtils::uSisUnifSeoMlocHomgNullGlue::sim_t sim_t;

    // Configure loci lengths
    const uUInt NUM_LOCI = 3;
    const uUInt NUM_NODES[NUM_LOCI] = {5, 10, 6};

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = std::string("null");
    config->max_trials = 1000;
    config->ensemble_size = 15;
    config->set_option(uOpt_max_node_diameter, U_TO_REAL(110.0));
    config->set_option(uOpt_nuclear_diameter, U_TO_REAL(4450.3264));
    config->num_nodes = std::vector<uUInt>(NUM_LOCI);
    for (uUInt i = 0; i < NUM_LOCI; ++i) {
        uAssert(NUM_NODES[i] > 0);
        config->num_nodes[i] = NUM_NODES[i];
    }
    config->num_unit_sphere_sample_points = 64;

    // Output configuration
    std::cout << "Running Test Sis Multi Loci Growth." << std::endl;
    config->print();

    // Initialize globals to default
    uG::default_init();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_TESTS
