//****************************************************************************
// uCsppline.h
//****************************************************************************

/**
 * @brief Smoothed natural cubic splines.
 */

/**
 * Example of basic usage:
 *
 *  // vec_t can be an arma::Col<>, std::vector<>, or any iterable container
 *  vec_t<double> vx, vy;
 *
 *  // ... fill vx and vy with data
 *
 *  // fit auto-smoothed spline using cross-validation
 *  uCsppline<double> f;
 *  f.fit(vx, vy, uCspplineAutoSmoothCv);
 *
 *  // evaluate spline at any arbitrary point
 *  double y = f(1.61803398875);
 */

/**
 * Implementation is meant to offer similar functionality as Matlab's csaps()
 * and R's smooth.spline() methods.
 *
 * A spline 's' is fit such that the following criterion is minimized:
 *
 *  criterion = (1-alpha) * RSS(w) +  alpha * roughness
 *      where RSS(w) = weighted residual sum of squares
 *                   = sum_j{ w_j * (y_j - s[x_j])^2 }
 *      where roughness = penalty for spline "wiggle"
 *                      = integral{ s"(t)^2 dt }
 *      - alpha is blending parameter in [0,1]
 *          : alpha = 1 gives a pure linear spline fit using least squares
 *              resulting in a high bias, low variance model
 *          : alpha = 0 gives a natural cubic spline which interpolates
 *              perfectly through all points y resulting in a low bias, high
 *              variance model
 *      - w is a weight vector, typically inversely proportional to the
 *          variance at each target point y
 *      - y is vector of observed values at each input x
 *      - x is abscissa vector
 *      - s is the natural cubic spline
 *      - s" is the second derivative of the natural cubic spline s
 */

#ifndef uCsppline_h
#define uCsppline_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uTypes.h"

#include <vector>

//****************************************************************************
// Forward declarations
//****************************************************************************

/**
 * Forward declare private spline utilities
 */
template <typename t_real, typename t_driver>
class uCspplinePutils;

/**
 * Forward declare spline driver with O(n) time and O(n) memory overhead
 *
 * Implementation based on Chapters 2 and 3 of:
 *
 * Green, Peter J., and Bernard W. Silverman. Nonparametric regression and
 *  generalized linear models: a roughness penalty approach. CRC Press, 1993.
 *
 * In particular, the Reinsch algorithm (see Green & Silverman, sections 2.3.3
 * & 3.5.2) is used for fitting the spline in O(n) time.
 *
 * For regular or generalized cross-validation (GCV), the diagonals of the
 * band matrix are computed in O(n) time using the algorithm of Hutchison and
 * de Hoog (see Green & Silverman, sections 3.2.2 & 3.5.3).
 */
template <typename t_real>
class uCspplineSparseDriver;

//****************************************************************************
// Enums
//****************************************************************************

/**
 * Enumeration used when auto-smoothing the spline (no roughness
 * penalty is specified by user).
 */
enum uCspplineAutoSmooth {
    // Spline is smoothed using leave-one-out cross-validation (recommended)
    uCspplineAutoSmoothCv = 0,
    // Spline is smoothed using generalized cross-validation
    uCspplineAutoSmoothGcv,
    // Spline is smoothed using effective degrees of freedom
    uCspplineAutoSmoothEdf
};

/**
 * Enum for evaluating spline at a set of sorted x-coordinates. If
 * x-coordinates are sorted, then this may improve efficiency of evaluation
 */
enum uCspplineSortedX {
    // X-coordinate abscissae are sorted in ascending order
    uCspplineSortedXasc = 1
};

//****************************************************************************
// Utility classes
//****************************************************************************

/**
 * Captures optional information about auto-smooth result
 */
template <typename t_real>
class uCspplineAutoSmoothInfo {
public:
    // Real-valued scalar type
    typedef t_real real_t;
    // Roughness penalty in [0,1] selected by auto-smoother
    real_t alpha;
    // Effective degrees of freedom selected by auto-smoother
    real_t edf;
    // Minimum criterion evaluation found by auto-smoother
    real_t score;
    // Number of iterations used by auto-smoother
    unsigned int num_iters;
};

/**
 * Auto-smooth configuration, specifies the range to search for a locally
 * optimal roughness penalty alpha in [min_alpha, max_alpha]. Examples of
 * criterion which can be minimized include cross-validation, generalized
 * cross-validation, and deviance from target effective degrees-of-freedom.
 */
template <typename t_real>
class uCspplineAutoSmoothConfig {
public:
    // Real-valued scalar type
    typedef t_real real_t;
    // The maximum number of iterations to search for a local minimum within
    // roughness penalty interval [min_alpha, max_alpha]
    unsigned int max_iters;
    // Minimum roughness penalty in [0, 1)
    real_t min_alpha;
    // Maximum roughness penalty in (min_alpha, 1]
    real_t max_alpha;
};

/**
 * Defaults used for auto-smoothing
 */
template <typename t_real>
class uCspplineAutoSmoothDefaults {
public:
    // Real-valued scalar type
    typedef t_real real_t;
    /**
     * @return Default effective degrees of freedom used for auto-smoothing
     */
    static inline real_t edf() { return static_cast<real_t>(1.0); }

    /**
     * @return Default max # of iterations to search for auto-smooth minima
     */
    static inline unsigned int max_iters() { return 500; }

    /**
     * @return Default minimum roughness penalty used for auto-smoothing
     */
    static inline real_t min_alpha() { return static_cast<real_t>(0.05); }

    /**
     * @return Default maximum roughness penalty used for auto-smoothing
     */
    static inline real_t max_alpha() { return static_cast<real_t>(0.95); }
};

//****************************************************************************
// uCsppline
//****************************************************************************

/**
 * Generic spline interface
 *
 * t_real = Real-valued scalar type like float or double
 *
 * t_driver = Fits spline to user data. In particular, must provide interface:
 *
 *  bool t_driver::fit(vec_t& y, vec_t& dd, const vec_t& x_filt,
 *                     const vec_t& y_filt, const vec_t& w_filt,
 *                     const real_t alpha)
 *
 *  bool t_driver::fit(vec_t &y, vec_t &dd, vec_t &a_diag,
 *                     const vec_t& x_filt, const vec_t& y_filt,
 *                     const vec_t& w_filt, const real_t alpha)
 *
 *  bool t_driver::prime(const vec_t& x_filt, const vec_t& y_filt,
 *                       const vec_t& w_filt)
 *
 *  bool t_driver::prime(const vec_t& x_filt, const vec_t& y_filt,
 *                       const vec_t& w_filt, const enum eCspplineAutoSmooth)
 *
 *  void t_driver::post()
 *
 *  void t_driver::clear()
 *
 *  where methods return true on success and false otherwise. The first
 *  fit() method fits spline to user data with a user-specified roughness
 *  penalty (alpha). The second fit() method is used by the auto-smoother to
 *  select a local minimum roughness penalty according to a criterion such as
 *  cross-validation. The method prime() is called once by the spline or
 *  auto-smoother to prepare the driver for fit() calls (e.g. allocate any
 *  internal buffers needed, etc.) and returns true if call is completed
 *  without error. The post() method is for clearing any temporary internal
 *  buffers allocated by the driver in the prime() call. The clear() method is
 *  for resetting any additional persistent state.
 *
 *  Parameters are:
 *        y : output y-coordinate at each knot x-coordinate
 *       dd : output second derivative at each knot point
 *   a_diag : output diagonals of linear smooth operator (see Green, 3.2.1)
 *   x_filt : input pre-sorted and unique knot x-coordinates, size > 2
 *   y_filt : input user y-coordinate corresponding to x_filt
 *   w_filt : input weights at each x_filt[i], y_filt[i] tuple
 *    alpha : roughness penalty in [0, +inf) where 0 corresponds to no penalty
 *            resulting in perfect interpolation and +inf corresponds to
 *            perfectly smooth linear least squares solution. Note, this alpha
 *            is a transformed version of the user's [0, 1] alpha penalty.
 */
template <typename t_real, typename t_driver = uCspplineSparseDriver<t_real> >
class uCsppline {
public:
    /**
     * Real-valued scalar type
     */
    typedef t_real real_t;

    /**
     * Vector type
     */
    typedef typename uMatrixUtils::Col<real_t> vec_t;

    /**
     * Performs heavy-lifting when fitting spline to data
     */
    typedef t_driver driver_t;

    /**
     * Detailed auto-smooth results
     */
    typedef uCspplineAutoSmoothInfo<real_t> auto_smooth_info_t;

    /**
     * Optional auto-smooth configuration parameters
     */
    typedef uCspplineAutoSmoothConfig<real_t> auto_smooth_config_t;

    /**
     * Default arguments provider
     */
    typedef uCspplineAutoSmoothDefaults<real_t> auto_smooth_defaults_t;

    /**
     * Evaluate spline at input x.
     * @param x - abscissa coordinate to evaluate spline at
     * @return output value of spline at coordinate x with default 0 if spline
     *  has not yet been fit to data
     */
    real_t operator()(const real_t x) const;

    /**
     * Evaluate spline at set of input abscissae defined by range [beg, end).
     * The outputs are written to the consecutive locations defined by out.
     * If spline has not yet been fit to data (size() == 0), then a default
     * value of 0 will be written to all output locations. Method *does not*
     * assume that x-coordinate abscissae are sorted in ascending order. If
     * abscissae are in fact ascending sorted, then pass additional argument
     * 'eCspplineSortedXasc' for more efficient spline evaluation.
     * @param out - iterator to first write-able location for storing results
     *  of spline evaluation in abscissae defined by read iterators beg, end
     * @param beg - iterator to first abscissa (x-coordinate) to evaluate
     * @param end - iterator such that spline evaluation is terminated when
     *  encountered
     */
    template <typename FwdWriteIt, typename FwdReadIt>
    void operator()(FwdWriteIt out, FwdReadIt beg, const FwdReadIt end) const;

    /**
     * ASSUMES X-COORDINATES ENCOUNTERED BY ITERATING RANGE [beg, end) ARE
     * SORTED IN ASCENDING ORDER. DOES NOT CHECK THAT RANGE IS SORTED!
     * Evaluate spline at set of input abscissae defined by range [beg, end).
     * The outputs are written to the consecutive locations defined by out.
     * If spline has not yet been fit to data (size() == 0), then a default
     * value of 0 will be written to all output locations. Assumes abscissae
     * are sorted in ascending order which may lead to more efficient spline
     * evaluation.
     * @param out - iterator to first write-able location for storing results
     *  of spline evaluation in abscissae defined by read iterators beg, end
     * @param beg - iterator to first abscissa (x-coordinate) to evaluate
     * @param end - iterator such that spline evaluation is terminated when
     *  encountered
     */
    template <typename FwdWriteIt, typename FwdReadIt>
    void operator()(FwdWriteIt out,
                    FwdReadIt beg,
                    const FwdReadIt end,
                    const uCspplineSortedX x_asc) const;

    /**
     * @return number of knots within spline
     */
    unsigned int size() const;

    /**
     * Manual smoothing with user-specified roughness penalty alpha
     *
     * Builds a weighted, smoothed natural cubic spline minimizing criterion:
     *      criterion = (1-alpha) * RSS(w) +  alpha * roughness
     *  where RSS(w) is the weighted residual sum of squares where each
     *  individual squared residual has magnitude w_i * (y_i - s(x_i))^2 and
     *  roughness is the integral of the squared second derivative (see Green,
     *  section 3.5.4).
     *
     * All design points with same 'x' coordinate will be replaced by a single
     * weighted design point (x, y_bar) where y_bar is the weighted average
     * value at x; the corresponding RSS term will have weight 'm' where 'm'
     * is the sum of the weights for all design points at x.
     *
     * @param x - input abscissae
     * @param y - observed y values at input x, must be same length as x
     * @param w - non-negative weights at each design point, typically set to
     *  values inversely proportional to the variance at each point. If not
     *  specified by user, then all weights are set to default 1.
     * @param alpha - roughness penalty in [0,1]. If 0, resulting spline will
     *  have no roughness penalty and will interpolate all points perfectly
     *  with low bias but high variance. If 1, resulting spline will have an
     *  infinite roughness penalty and produce a linear least-squares fit with
     *  high bias but low variance. If alpha < 0 or alpha > 1, then alpha will
     *  be set to a default value of (1 - 1/(1 + (h^3)/0.6)) where h is the
     *  average knot spacing; this is similar to default value described in
     *  Matlab's csaps() documentation.
     * @return true if spline build successful, false o/w
     */
    bool fit(const vec_t& x, const vec_t& y, const real_t alpha);
    bool fit(const vec_t& x,
             const vec_t& y,
             const vec_t& w,
             const real_t alpha);
    /**
     * Manual smoothing interface for std::vector
     */
    bool fit(const std::vector<real_t>& x,
             const std::vector<real_t>& y,
             const real_t alpha);
    bool fit(const std::vector<real_t>& x,
             const std::vector<real_t>& y,
             const std::vector<real_t>& w,
             const real_t alpha);
    /**
     * Manual smoothing interface for arbitrary containers all assumed to be
     *  at least size 'n'
     * @param x - forward iterator to x-coordinates
     * @param y - forward iterator to y-coordinates
     * @param w - forward iterator to weights, will default to 1 if not
     *  specified by user
     * @param n - input number of design points
     * @param alpha - smoothing parameter in [0,1]
     * @return true if spline build successful, false otherwise
     */
    template <typename FwdItX, typename FwdItY>
    bool fit(FwdItX x, FwdItY y, const unsigned int n, const real_t alpha);
    template <typename FwdItX, typename FwdItY, typename FwdItW>
    bool fit(FwdItX x,
             FwdItY y,
             FwdItW w,
             const unsigned int n,
             const real_t alpha);

    /**
     * Auto-smoothing is used to determine the roughness penalty alpha.
     *
     * Builds a weighted, smoothed natural cubic spline minimizing criterion:
     *      criterion = (1-alpha) * RSS(w) +  alpha * roughness
     *  where RSS(w) is the weighted residual sum of squares where each
     *  individual squared residual has magnitude w_i * (y_i - s(x_i))^2 and
     *  roughness is the integral of the squared second derivative (see Green,
     *  section 3.5.4).
     *
     * All design points with same 'x' coordinate will be replaced by a single
     * weighted design point (x, y_bar) where y_bar is the weighted average
     * value at x; the corresponding RSS term will have weight 'm' where 'm'
     * is the sum of the weights for all design points at x.
     *
     * @param x - input abscissae
     * @param y - observed y values at input x, must be same length as x
     * @param w - non-negative weights at each design point, typically set to
     *  values inversely proportional to the variance at each point. If not
     *  specified by user, then all weights are set to default 1.
     * @param type - specifies auto smooth algorithm:
     *      auto_smooth_cv: leave-one-out cross validation (recommended)
     *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
     *      auto_smooth_edf: effective degrees of freedom
     *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
     *      validation and generalized cross-validation.
     *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
     *      and James, Witten, Hastie, & Tibshirani, An Introduction to
     *      Statistical Learning, section 7.5.2 for description of effective
     *      degrees of freedom.
     * @param info - optional pointer to info structure for detailed results
     * @param edf - effective degrees of freedom, must be in range [1, n]
     *  where n is number of unique elements in parameter 'x', only used if
     *  parameter 'type' is 'auto_smooth_edf'.
     * @param config - optional pointer to custom auto-smoother configuration,
     *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
     * @return true if spline build successful, false o/w
     */
    bool fit(const vec_t& x,
             const vec_t& y,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);
    bool fit(const vec_t& x,
             const vec_t& y,
             const vec_t& w,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);
    /**
     * Auto-smoothing interface for std::vector
     */
    bool fit(const std::vector<real_t>& x,
             const std::vector<real_t>& y,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);
    bool fit(const std::vector<real_t>& x,
             const std::vector<real_t>& y,
             const std::vector<real_t>& w,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);
    /**
     * Auto-smoothing interface for arbitrary containers all assumed to be at
     *  least size 'n'
     * @param x - forward iterator to x-coordinates
     * @param y - forward iterator to y-coordinates
     * @param w - forward iterator to weights, will default to 1 if not
     *  specified by user
     * @param n - input number of design points
     * @param type - specifies auto smooth algorithm:
     *      auto_smooth_cv: leave-one-out cross validation (recommended)
     *      auto_smooth_gcv: generalized cross-validation (may under-smooth)
     *      auto_smooth_edf: effective degrees of freedom
     *  See Green, sections 3.2, 3.3, and 3.5 for descriptions of cross-
     *      validation and generalized cross-validation.
     *  See Hastie and Tibshirani, Generalized Additive Models, section 3.5
     *      and James, Witten, Hastie, & Tibshirani, An Introduction to
     *      Statistical Learning, section 7.5.2 for description of effective
     *      degrees of freedom.
     * @param info - optional pointer to info structure for detailed results
     * @param edf - effective degrees of freedom, must be in range [1, n]
     *  where n is number of unique elements in parameter 'x', only used if
     *  parameter 'type' is 'auto_smooth_edf'.
     * @param config - optional pointer to custom auto-smoother configuration,
     *  if NULL, then uses defaults as defined in uCspplineAutoSmoothDefaults
     * @return true if spline fit successful, false otherwise
     */
    template <typename FwdItX, typename FwdItY>
    bool fit(FwdItX x,
             FwdItY y,
             const unsigned int n,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);
    template <typename FwdItX, typename FwdItY, typename FwdItW>
    bool fit(FwdItX x,
             FwdItY y,
             FwdItW w,
             const unsigned int n,
             const uCspplineAutoSmooth type,
             auto_smooth_info_t* const info = NULL,
             const real_t edf = auto_smooth_defaults_t::edf(),
             auto_smooth_config_t* const config = NULL);

    /**
     * Resets internal state
     */
    void clear();

private:
    // X-coordinate knots
    vec_t m_x;
    // Y-coordinate at each knot
    vec_t m_y;
    // Second derivative at each knot
    vec_t m_dd;
    // Used for fitting spline and manipulating band matrices
    driver_t m_driver;

    // Allow private utilities to access spline internals
    typedef uCspplinePutils<real_t, driver_t> putils_t;
    friend putils_t;
};

/**
 * Implementation
 */
#include "uCsppline.inl"

#endif  // uCsppline_h
