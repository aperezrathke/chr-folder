//****************************************************************************
// uCmdletLigcPopd.h
//****************************************************************************

/**
 * Computes bootstrap (BLB) distribution of aggregated contact frequency
 *  matrices over population of ligation contact (ligc) profiles
 *
 * Usage:
 * -cmdlet_ligc_popd --conf <path> [...]
 */

#ifdef uCmdletLigcPopd_h
#   error "Commandlet Ligc Popd included multiple times!"
#endif  // uCmdletLigcPopd_h
#define uCmdletLigcPopd_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_COMMANDLETS

#include "uAssert.h"
#include "uBlb.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uFilesystem.h"
#include "uGlobals.h"
#include "uLigcCore.h"
#include "uLogf.h"
#include "uQuantile.h"
#include "uSisUtilsQc.h"
#include "uStreamUtils.h"
#include "uStringUtils.h"
#include "uThread.h"

#include <algorithm>
#include <boost/numeric/ublas/symmetric.hpp>
#include <fstream>
#include <sstream>
#include <vector>
#include <zstr.hpp>

//****************************************************************************
// Defines
//****************************************************************************

// Maximum number of outer BLB replicates
#define U_LIGC_POPD_MAX_BLB_REPO (U_UINT_MAX - U_TO_UINT(1))

// Default number of outer BLB replicates
#define U_LIGC_POPD_DEFAULT_BLB_REPO 5000

// Default number of inner BLB replicates
#define U_LIGC_POPD_DEFAULT_BLB_REPI 100

// Default BLB population size scaling exponent
#define U_LIGC_POPD_DEFAULT_BLB_NEXP U_TO_REAL(0.6)

// Default BLB chunk size for threaded builds
#define U_LIGC_POPD_DEFAULT_BLB_CHUNK_SIZE U_BLB_DEFAULT_CHUNK_SIZE

// Default exclude data samples with outlier weights
#define U_LIGC_POPD_DEFAULT_EXCLUDE_OUTLIERS uTRUE

// Default base export format string
#define U_LIGC_POPD_DEFAULT_EXPORT_FORMAT_BASE "popd"

// Default job identifier
#define U_LIGC_POPD_DEFAULT_JOB_ID "0"

//****************************************************************************
// Utilities
//****************************************************************************

namespace uLigcPopd {

//******************************************************************
// Enums
//******************************************************************

/**
 * Popd export option bits
 */
enum eExportPopdBits {
    eExportPopdBitCooCsv = 0,
    eExportPopdBitCooCsvGz,
    eExportPopdBitLbandCsv,
    eExportPopdBitLbandCsvGz,
    eExportPopdBitLtriCsv,
    eExportPopdBitLtriCsvGz,
    eExportPopdBitBonded,
    eXportPopdBitNum
};

/**
 * Popd export option flags
 */
enum eExportPopdFlags {
    // COOrdinate (i,j,v) tuple as CSV
    eExportPopdCooCsv = 1 << eExportPopdBitCooCsv,
    // COOrdinate (i,j,v) tuple as gzipped CSV
    eExportPopdCooCsvGz = 1 << eExportPopdBitCooCsvGz,
    // Lower bands (each row is a diagonal) as CSV
    eExportPopdLbandCsv = 1 << eExportPopdBitLbandCsv,
    // Lower bands (each row is a diagonal) as gzipped CSV
    eExportPopdLbandCsvGz = 1 << eExportPopdBitLbandCsvGz,
    // Lower triangle as CSV
    eExportPopdLtriCsv = 1 << eExportPopdBitLtriCsv,
    // Lower triangle as gzipped CSV
    eExportPopdLtriCsvGz = 1 << eExportPopdBitLtriCsvGz,
    // Export (i,i) and (i,i+1) diagonals (applies to all formats)
    eExportPopdBonded = 1 << eExportPopdBitBonded
};

//******************************************************************
// Typedefs
//******************************************************************

/**
 * Element identifier
 */
typedef uLigcCore::elem_id_t elem_id_t;

/**
 * Contact tuple (index of first monomer, index of second monomer)
 */
typedef uLigcCore::contact_t contact_t;

/**
 * Set of contact tuples within a polymer
 */
typedef uLigcCore::profile_t profile_t;

/**
 * Symmetric matrix (packed, row-major storage)
 */
typedef boost::numeric::ublas::symmetric_matrix<
    uReal,
    boost::numeric::ublas::upper,
    boost::numeric::ublas::row_major>
    symmat_t;

/**
 * Importer type loads ligc contact profile and log weight
 * @param profile - ligc contact profile: output collection of (i,j) ligations
 * @param log_weight - output ligc log weight associated to contact profile
 * @param dir - import directory containing ligc data samples
 * @param name - file name of ligc sample data
 * @return uTRUE if import okay, uFALSE o/w
 */
typedef uBool (*fp_import_prof_t)(profile_t& profile,
                                  uReal& log_weight,
                                  const std::string& dir,
                                  const std::string& name);

/**
 * Importer type loads ligc log weight only
 * @param log_weight - output ligc log weight
 * @param dir - import directory containing ligc data samples
 * @param name - file name of ligc sample data
 * @return uTRUE if import okay, uFALSE o/w
 */
typedef uBool (*fp_import_logw_t)(uReal& log_weight,
                                  const std::string& dir,
                                  const std::string& name);

/**
 * Importer callback table
 */
typedef struct {
    fp_import_prof_t m_fpprof;
    fp_import_logw_t m_fplogw;
} importer_t;

/**
 * Exporter callback for writing outer replicate
 * @param dir - target export directory
 * @param name_no_ext - base file name without .<ext> suffix
 */
typedef uBool (*fp_export_orep_t)(const symmat_t& res,
                                  const std::string& dir,
                                  const std::string& name_no_ext,
                                  const uUInt flags);

/**
 * Exporter callback and target directory
 */
typedef struct {
    // Target export directory
    std::string m_dir;
    // Export writer callback
    fp_export_orep_t m_fpwrite;
} exporter_t;

//******************************************************************
// BLB specification
//******************************************************************

/**
 * Specification
 */
typedef struct {
    /**
     * Number of contact bins, defaults to total polymer node count
     */
    uUInt m_num_bins;
    /**
     * BLB outer replicates
     */
    uUInt m_blb_repo;
    /**
     * BLB inner replicates
     */
    uUInt m_blb_repi;
    /**
     * BLB population size after outliers removed
     */
    uUInt m_blb_n;
    /**
     * BLB number of unique data samples per outer replicate
     */
    uUInt m_blb_nun;
    /**
     * For threaded-builds, defines number of inner BLB replicates processed
     * by each thread when grabbing work
     */
    uUInt m_blb_chunk_size;
    /**
     * Importer callbacks
     */
    importer_t m_im;
    /**
     * Directory to import ligc data
     */
    std::string m_import_dir;
    /**
     * Files names of imported ligc data (outliers excluded)
     */
    std::vector<std::string> m_import_fnames;
    /**
     * Job identifier to avoid overwriting previous data
     */
    std::string m_job_id;
    /**
     * Popd export formats
     */
    uUInt m_export_flags;
    /**
     * Base directory to export BLB replicates
     */
    std::string m_export_dir;
    /**
     * Exporters
     */
    std::vector<exporter_t> m_exporters;
} Spec_t;

/**
 * Import adapter for binary formatted ligc data samples
 */
class ImportAdapterBin {
public:
    /**
     * Import contact profile and log weight
     */
    static uBool import_prof(profile_t& profile,
                             uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        std::ifstream istr(fpath.c_str(), std::ios::in | std::ios::binary);
        const uBool result = uLigcCore::import_bin(istr, profile, log_weight);
        istr.close();
        return result;
    }
    /**
     * Import log weight
     */
    static uBool import_logw(uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        std::ifstream istr(fpath.c_str(), std::ios::in | std::ios::binary);
        const uBool result = uLigcCore::import_bin(istr, log_weight);
        istr.close();
        return result;
    }
    /**
     * @return Format extension (sans dot)
     */
    static const char* get_format() { return uLigcCore::get_format_bin(); }
};

/**
 * Import adapter for CSV formatted ligc data samples
 */
class ImportAdapterCsv {
public:
    /**
     * Import contact profile and log weight
     */
    static uBool import_prof(profile_t& profile,
                             uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        std::ifstream istr(fpath.c_str(), std::ios::in);
        const uBool result = uLigcCore::import_csv(istr, profile, log_weight);
        istr.close();
        return result;
    }
    /**
     * Import log weight
     */
    static uBool import_logw(uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        std::ifstream istr(fpath.c_str(), std::ios::in);
        const uBool result = uLigcCore::import_csv(istr, log_weight);
        istr.close();
        return result;
    }
    /**
     * @return Format extension (sans dot)
     */
    static const char* get_format() { return uLigcCore::get_format_csv(); }
};

/**
 * Import adapter for gzipped CSV formatted ligc data samples
 */
class ImportAdapterCsvGz {
public:
    /**
     * Import contact profile and log weight
     */
    static uBool import_prof(profile_t& profile,
                             uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        zstr::ifstream istr(fpath.c_str(), std::ios::in | std::ios::binary);
        const uBool result =
            uLigcCore::import_csv_gz(istr, profile, log_weight);
        // zstr has no close method!
        return result;
    }
    /**
     * Import log weight
     */
    static uBool import_logw(uReal& log_weight,
                             const std::string& dir,
                             const std::string& name) {
        const std::string fpath((uFs::path(dir) / uFs::path(name)).string());
        zstr::ifstream istr(fpath.c_str(), std::ios::in | std::ios::binary);
        const uBool result = uLigcCore::import_csv_gz(istr, log_weight);
        // zstr has no close method!
        return result;
    }
    /**
     * @return Format extension (sans dot)
     */
    static const char* get_format() { return uLigcCore::get_format_csv_gz(); }
};

//******************************************************************
// Functions: initialize import
//******************************************************************

/**
 * Install import adapter: collects import callbacks
 */
template <typename import_adapter_t>
void import_install(importer_t& im) {
    im.m_fpprof = import_adapter_t::import_prof;
    im.m_fplogw = import_adapter_t::import_logw;
}

/**
 * @return Default import directory
 */
template <typename import_adapter_t>
std::string import_get_default_format_dir(const std::string& output_dir) {
    return uLigcCore::get_format_dir(output_dir,
                                     import_adapter_t::get_format());
}

/**
 * @return TRUE if file name extension matches importer format, FALSE o/w
 */
template <typename import_adapter_t>
uBool import_is_format(const std::string& name) {
    return uStringUtils::ends_with(
        name, std::string(".") + std::string(import_adapter_t::get_format()));
}

/**
 * Helper to help trap heterogeneous import folders (importer expects folder
 * with all files same format)
 * @param names - List of import file names
 * @param viol - Output name of offending file if found
 * @return TRUE if all file extensions match importer format, FALSE o/w
 */
template <typename import_adapter_t>
uBool import_is_format(const std::vector<std::string>& names,
                       std::string& viol) {
    const size_t num = names.size();
    for (size_t i = 0; i < num; ++i) {
        if (!import_is_format<import_adapter_t>(names[i])) {
            viol = names[i];
            return uFALSE;
        }
    }
    return uTRUE;
}

/**
 * @dir - output import directory
 * @output_dir - Parent folder to search for default import directory
 * @return TRUE if default import format directory found, FALSE o/w
 */
template <typename import_adapter_t>
uBool import_default_dir_exists(std::string& dir,
                                const std::string& output_dir) {
    dir = import_get_default_format_dir<import_adapter_t>(output_dir);
    uFs_normalize_dir_path(dir);
    return uFs::exists(uFs::path(dir)) && uFs::is_directory(dir);
}

/**
 * Attempts to resolve import format based on file extension. If match found,
 * importer callbacks are installed. Will exit with error if match found but
 * some files do not match format (all files must match).
 * @param im - Output importer callbacks, only modified if format matches
 * @param names - List of ligc data sample file names
 * @return uTRUE if format matches and importer installed okay, uFALSE o/w
 */
template <typename import_adapter_t>
uBool import_detect(importer_t& im, const std::vector<std::string>& names) {
    uAssert(!names.empty());
    if (import_is_format<import_adapter_t>(names[0])) {
        uLogf("Detected import format: %s\n", import_adapter_t::get_format());
        std::string viol;
        if (!import_is_format<import_adapter_t>(names, viol)) {
            uLogf(
                "Error: import directory contains file with format "
                "mismatch:\n\t%s\nExiting.\n",
                viol.c_str());
            exit(uExitCode_error);
        }
        import_install<import_adapter_t>(im);
        return uTRUE;
    }
    return uFALSE;
}

/**
 * Resolves import directory containing ligc data samples
 * @param dir - Output import directory
 * @param config - User configuration
 */
void init_import_dir(std::string& dir, uSpConstConfig_t config) {
    uBool result = uFALSE;
    // Check if user has explicitly specified directory
    if (config->resolve_path(dir, uOpt_ligc_popd_import_dir)) {
        uFs_normalize_dir_path(dir);
        result = uFs::exists(uFs::path(dir));
    }
    // Else, implicitly resolve according to performance priority:
    //  - <output_dir>/<ligc bin dir>
    //  - <output_dir>/<ligc csv gz dir>
    //  - <output_dir>/<ligc csv dir>
    else if (import_default_dir_exists<ImportAdapterBin>(dir,
                                                         config->output_dir)) {
        result = uTRUE;
    } else if (import_default_dir_exists<ImportAdapterCsvGz>(
                   dir, config->output_dir)) {
        result = uTRUE;
    } else if (import_default_dir_exists<ImportAdapterCsv>(
                   dir, config->output_dir)) {
        result = uTRUE;
    }
    // Check result okay
    if (!result) {
        // No import directory found!
        uLogf(
            "Error: unable to resolve ligc import directory. Consider setting "
            "--ligc_popd_import_dir option. Exiting.\n");
        exit(uExitCode_error);
    }
    uLogf("Resolved ligc import directory:\n\t%s\n", dir.c_str());
}

/**
 * Does not perform outlier removal! Determines initial list of file names
 * @param fnames - Output list of ligc data samples (no outlier removal)
 * @param dir - Already resolved import directory containing ligc data samples
 */
void init_import_fnames(std::vector<std::string>& fnames,
                        const std::string& dir) {
    // Pre-conditions
    uAssert(uFs::exists(dir));
    uAssert(uFs::is_directory(dir));
    // Cycle through directory and collect non-directory files
    fnames.clear();
    uFs::directory_iterator end_itr;
    for (uFs::directory_iterator itr(dir); itr != end_itr; itr++) {
        if (uFs::is_regular_file(itr->status())) {
            fnames.push_back(itr->path().filename().string());
        }
    }
    // Check files found!
    uLogf("Found %d ligc files in import directory.\n", (int)fnames.size());
    if (fnames.empty()) {
        uLogf("Error: ligc import directory is empty! Exiting.\n");
        exit(uExitCode_error);
    }
}

/**
 * Resolves importer callbacks for list of ligc data sample file names
 * @param im - Output importer callbacks
 * @param fnames - Collection of ligc data sample file names
 */
void init_import_callbacks(importer_t& im,
                           const std::vector<std::string>& fnames) {
    uAssert(!fnames.empty());
    if (import_detect<ImportAdapterBin>(im, fnames)) {
        return;
    }
    if (import_detect<ImportAdapterCsvGz>(im, fnames)) {
        return;
    }
    if (import_detect<ImportAdapterCsv>(im, fnames)) {
        return;
    }
    uLogf("Error: unable to resolve import format! Exiting.\n");
    exit(uExitCode_error);
}

/**
 * If configured, will remove files names from 'fnames' with weights outside
 *  range [Q1 - 1.5*IRQ, Q3 + 1.5*IQR]
 * @param fnames - List of ligc data sample file names to maybe strip outliers
 * @param im - Importer callbacks (resolved)
 * @param dir - Import directory (resolved)
 * @param config - User configuration
 */
void init_import_exclude_outliers(std::vector<std::string>& fnames,
                                  const importer_t& im,
                                  const std::string& dir,
                                  uSpConstConfig_t config) {
    // Check configuration
    uBool exclude_outliers = U_LIGC_POPD_DEFAULT_EXCLUDE_OUTLIERS;
    if (config->key_exists(uOpt_ligc_popd_exclude_outliers_unary)) {
        exclude_outliers = uTRUE;
    } else {
        config->read_into(exclude_outliers,
                          uOpt_ligc_popd_exclude_outliers_binary);
        exclude_outliers = (exclude_outliers == uTRUE);
    }
    // See if this operation needs to be performed
    if (!exclude_outliers || (fnames.size() <= 1)) {
        uLogf("No outlier exclusion performed.\n");
        return;
    }
    // Else, perform outlier removal
    uLogf("Performing outlier removal...\n");
    // Load weights
    const uMatSz_t num_ligc = U_TO_MAT_SZ_T(fnames.size());
    uVecCol log_weights;
    log_weights.zeros(num_ligc);
    for (uMatSz_t i = 0; i < num_ligc; ++i) {
        uVerify(im.m_fplogw(log_weights.at(i), dir, fnames[i]));
    }
    // Normalize and transform out of log space, compute original ESS
    uVecCol weights;
    uReal max_log_weight;
    const uReal ess_all =
        uSisUtilsQc::calc_ess_cv_from_log(weights, max_log_weight, log_weights);
    uAssert(weights.n_elem == log_weights.n_elem);
    // Determine inter-quartile range (IQR)
    const uVecCol weights_sorted = uMatrixUtils::sort(weights, "ascend");
    const uReal Q1 =
        uQuantile::get_q_from_sorted(weights_sorted, U_TO_REAL(0.25));
    const uReal Q3 =
        uQuantile::get_q_from_sorted(weights_sorted, U_TO_REAL(0.75));
    uAssert(Q3 >= Q1);
    const uReal IQR = Q3 - Q1;
    // Determine inclusive (non-outlier) span [Q1 - 1.5IQR, Q3 + 1.5IQR]:
    const uReal w_lo = Q1 - (U_TO_REAL(1.5) * IQR);
    const uReal w_hi = Q3 + (U_TO_REAL(1.5) * IQR);
    // Exclude outliers!
    std::vector<uReal> log_weights_keep;
    for (int i = ((int)num_ligc) - 1; i >= 0; --i) {
        uAssertBounds(i, 0, weights.n_elem);
        const uReal w_i = weights.at(U_TO_MAT_SZ_T(i));
        if ((w_i < w_lo) || (w_i > w_hi)) {
            // Outlier detected - swap with last element and then pop
            std::swap(fnames[i], fnames.back());
            fnames.pop_back();
        } else {
            log_weights_keep.push_back(log_weights.at(U_TO_MAT_SZ_T(i)));
        }
    }
    const uReal ess_keep =
        uSisUtilsQc::calc_ess_cv_from_log(uVecCol(log_weights_keep));
    const uMatSz_t num_excluded = num_ligc - U_TO_MAT_SZ_T(fnames.size());
    uLogf("Outlier detection removed %d (%.1f%%) ligc data samples.\n",
          (int)num_excluded,
          (100.0 * ((double)num_excluded) / ((double)num_ligc)));
    uLogf("\t-original ESS: %f\n", (double)ess_all);
    uLogf("\t-new ESS: %f\n", (double)ess_keep);
}

/**
 * Initialize import specification from user configuration
 * @param spec - Specification object to resolve import directory, import
 *  file names, import callbacks, etc
 * @param config - Handle to user configuration
 */
void init_import(Spec_t& spec, uSpConstConfig_t config) {
    // Resolve import directory
    init_import_dir(spec.m_import_dir, config);
    // Determine ligc data file names
    init_import_fnames(spec.m_import_fnames, spec.m_import_dir);
    // Determine importer callbacks
    init_import_callbacks(spec.m_im, spec.m_import_fnames);
    // Remove outliers (if configured)
    init_import_exclude_outliers(
        spec.m_import_fnames, spec.m_im, spec.m_import_dir, config);
}

//******************************************************************
// Functions: initialize contact bins
//******************************************************************

/**
 * Initialize number of contact bins, defaults to number of polymer nodes
 * @param spec - Specification object to resolve BLB parameters
 * @param config - User configuration
 */
void init_contact_bins(Spec_t& spec, uSpConstConfig_t config) {
    // Default to total number of polymer nodes
    uUInt num_bins = U_TO_UINT(0);
    const size_t num_loci = config->num_nodes.size();
    for (size_t i = 0; i < num_loci; ++i) {
        num_bins += config->num_nodes[i];
    }
    const uUInt NUM_BEADS = num_bins;
    // Check if overload exists
    config->read_into(num_bins, uOpt_ligc_popd_num_bins);
    if (num_bins < 1) {
        uLogf("Error: Number of contacts bins must be >= 1. Exiting.\n");
        exit(uExitCode_error);
    }
    uLogf(
        "Warning: number of contact bins is %d.\n\t-Total number of polymer "
        "beads is %d.\n\t-Please ensure bin count is correct!\n",
        (int)num_bins,
        (int)NUM_BEADS);
    uAssert(num_bins >= U_TO_UINT(1));
    spec.m_num_bins = num_bins;
}

//******************************************************************
// Functions: initialize BLB
//******************************************************************

/**
 * Utility for initializing a BLB positive integer parameter
 * @param out - output positive integer
 * @param default_value - default value if user does not specify value or user
 *  value < 1
 * @param label - Human readable label for parameter
 * @param config - User configuration
 */
void init_blb_positive_int(uUInt& out,
                           const uUInt default_value,
                           const uOptE opt,
                           const std::string& label,
                           uSpConstConfig_t config) {
    uAssert(default_value >= U_TO_UINT(1));
    out = default_value;
    config->read_into(out, opt);
    if (out < U_TO_UINT(1)) {
        uLogf("Warning: %s must be >= 1! Setting to default.\n", label.c_str());
        out = default_value;
    }
    uAssert(out >= U_TO_UINT(1));
    uLogf("%s: %d\n", label.c_str(), (int)out);
}

/**
 * Initialize BLB specification from user configuration
 * @WARNING - ASSUMES IMPORT SPECIFICATION HAS BEEN INITIALIZED
 * @param spec - Specification object to resolve BLB parameters
 * @param config - Handle to user configuration
 */
void init_blb(Spec_t& spec, uSpConstConfig_t config) {
    // BLB outer replicates
    init_blb_positive_int(spec.m_blb_repo,
                          U_LIGC_POPD_DEFAULT_BLB_REPO,
                          uOpt_ligc_popd_blb_repo,
                          "BLB outer replicates",
                          config);
    if (spec.m_blb_repo > U_LIGC_POPD_MAX_BLB_REPO) {
        uLogf("Warning: BLB outer replicates exceeds %u. Exiting.\n",
              U_LIGC_POPD_MAX_BLB_REPO);
        exit(uExitCode_error);
    }
    // BLB inner replicates
    init_blb_positive_int(spec.m_blb_repi,
                          U_LIGC_POPD_DEFAULT_BLB_REPI,
                          uOpt_ligc_popd_blb_repi,
                          "BLB inner replicates",
                          config);
    // BLB population size (total number of data samples 'n' after outlier
    // removal)
    spec.m_blb_n = U_TO_UINT(spec.m_import_fnames.size());
    uLogf("BLB population size (total number of ligc data samples): %d\n",
          (int)spec.m_blb_n);
    if (spec.m_blb_n <= 1) {
        uLogf("Error: BLB population size must be > 1. Exiting.\n");
        exit(uExitCode_error);
    }
    // BLB population size scaling exponent in [0,1] such that n^nexp (where n
    // is population size) defines number of unique samples per outer BLB
    // replicate
    uReal blb_nexp = U_LIGC_POPD_DEFAULT_BLB_NEXP;
    config->read_into(blb_nexp, uOpt_ligc_popd_blb_nexp);
    if ((blb_nexp < U_TO_REAL(0.0)) || (blb_nexp > U_TO_REAL(1.0))) {
        uLogf(
            "Warning: BLB population size scaling exponent must be in [0,1]. "
            "Setting to default.\n");
        blb_nexp = U_LIGC_POPD_DEFAULT_BLB_NEXP;
    }
    uAssertBoundsInc(blb_nexp, U_TO_REAL(0.0), U_TO_REAL(1.0));
    uLogf("BLB population size scaling exponent: %f\n", (double)blb_nexp);
    // BLB number of unique samples per outer replicate
    spec.m_blb_nun = uBlb::get_nun(spec.m_blb_n, blb_nexp);
    uLogf("BLB number of unique samples per outer replicate: %d\n",
          (int)spec.m_blb_nun);
    uAssertBoundsInc(spec.m_blb_nun, U_TO_UINT(1), spec.m_blb_n);
    // BLB chunk size
    init_blb_positive_int(spec.m_blb_chunk_size,
                          U_LIGC_POPD_DEFAULT_BLB_CHUNK_SIZE,
                          uOpt_ligc_popd_blb_chunk_size,
                          "BLB chunk size",
                          config);
}

//******************************************************************
// Functions: initialize export
//******************************************************************

/**
 * @return TRUE if bonded export flag is set, FALSE o/w
 */
uBool should_export_bonded(const uUInt export_flags) {
    return (export_flags & eExportPopdBonded) ? uTRUE : uFALSE;
}

/**
 * Generic method for writing a compressed CSV format
 */
template <typename gz_util_t>
uBool write_csv_gz(zstr::ofstream& out,
                   const symmat_t& orep,
                   const uUInt flags) {
    typedef typename gz_util_t::csv_util_t csv_util_t;
    if (!out) {
        return uFALSE;
    }
    std::stringstream ss;
    if (!csv_util_t::write(ss, orep, flags)) {
        return uFALSE;
    }
    ss.seekg(0, std::ios::beg);
    uStreamUtils::cat(ss, out);
    return uTRUE;
}

/**
 * Export utilities for COO CSV
 */
class ExportUtilCooCsv {
public:
    static const char* get_format() { return "coo.csv"; }
    static const char* get_label() { return "COO CSV"; }
    static uOptE get_opt_unary() { return uOpt_ligc_popd_export_coo_csv_unary; }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_coo_csv_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitCooCsv; }
    static eExportPopdFlags get_flag() { return eExportPopdCooCsv; }
    static uBool write(std::ostream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        if (!out) {
            return uFALSE;
        }
        uAssertPosEq(orep.size1(), orep.size2());
        const size_t N = orep.size1();
        if (N < 1) {
            return uFALSE;
        }
        const uBool write_bonded = should_export_bonded(flags);
        // Write non-zero entries
        for (size_t i = 0; i < N; ++i) {
            if (write_bonded) {
                out << i << ',' << i << ',' << "1.0\n";
                if ((i + 1) < N) {
                    out << i << ',' << (i + 1) << ',' << "1.0\n";
                }
            }
            for (size_t j = i + 2; j < N; ++j) {
                const uReal value = orep(i, j);
                uAssertRealBoundsInc(value, U_TO_REAL(0.0), U_TO_REAL(1.0));
                if (value > U_TO_REAL(0.0)) {
                    out << i << ',' << j << ',' << value << '\n';
                }
            }
        }
        return out.good() ? uTRUE : uFALSE;
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        std::ofstream fout(fpath.c_str(), std::ofstream::out);
        return write(fout, orep, flags);
    }
};

/**
 * Export utilities for COO CSV gzipped
 */
class ExportUtilCooCsvGz {
public:
    typedef ExportUtilCooCsv csv_util_t;
    static const char* get_format() { return "coo.csv.gz"; }
    static const char* get_label() { return "COO CSV gzipped"; }
    static uOptE get_opt_unary() {
        return uOpt_ligc_popd_export_coo_csv_gz_unary;
    }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_coo_csv_gz_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitCooCsvGz; }
    static eExportPopdFlags get_flag() { return eExportPopdCooCsvGz; }
    static uBool write(zstr::ofstream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        return write_csv_gz<ExportUtilCooCsvGz>(out, orep, flags);
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        zstr::ofstream fout(fpath.c_str());
        return write(fout, orep, flags);
    }
};

/**
 * Export utilities for lower banded (each row is a diagonal) CSV
 */
class ExportUtilLbandCsv {
public:
    static const char* get_format() { return "lband.csv"; }
    static const char* get_label() { return "lower banded CSV"; }
    static uOptE get_opt_unary() {
        return uOpt_ligc_popd_export_lband_csv_unary;
    }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_lband_csv_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitLbandCsv; }
    static eExportPopdFlags get_flag() { return eExportPopdLbandCsv; }
    static uBool write(std::ostream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        if (!out) {
            return uFALSE;
        }
        uAssertPosEq(orep.size1(), orep.size2());
        const size_t N = orep.size1();
        if (N < 1) {
            return uFALSE;
        }
        const uBool write_bonded = should_export_bonded(flags);
        // Write diagonals
        const size_t d_start = write_bonded ? 0 : 2;
        for (size_t d = d_start; d < N; ++d) {
            out << orep(0, d);
            for (size_t j = d + 1; j < N; ++j) {
                out << ',' << orep(j - d, j);
            }
            out << '\n';
        }
        return out.good() ? uTRUE : uFALSE;
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        std::ofstream fout(fpath.c_str(), std::ofstream::out);
        return write(fout, orep, flags);
    }
};

/**
 * Export utilities for lower banded (each row is a diagonal) CSV gzipped
 */
class ExportUtilLbandCsvGz {
public:
    typedef ExportUtilLbandCsv csv_util_t;
    static const char* get_format() { return "lband.csv.gz"; }
    static const char* get_label() { return "lower banded CSV gzipped"; }
    static uOptE get_opt_unary() {
        return uOpt_ligc_popd_export_lband_csv_gz_unary;
    }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_lband_csv_gz_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitLbandCsvGz; }
    static eExportPopdFlags get_flag() { return eExportPopdLbandCsvGz; }
    static uBool write(zstr::ofstream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        return write_csv_gz<ExportUtilLbandCsvGz>(out, orep, flags);
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        zstr::ofstream fout(fpath.c_str());
        return write(fout, orep, flags);
    }
};

/**
 * Export utilities for lower triangle CSV
 */
class ExportUtilLtriCsv {
public:
    static const char* get_format() { return "ltri.csv"; }
    static const char* get_label() { return "lower triangle CSV"; }
    static uOptE get_opt_unary() {
        return uOpt_ligc_popd_export_ltri_csv_unary;
    }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_ltri_csv_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitLtriCsv; }
    static eExportPopdFlags get_flag() { return eExportPopdLtriCsv; }
    static uBool write(std::ostream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        if (!out) {
            return uFALSE;
        }
        uAssertPosEq(orep.size1(), orep.size2());
        const size_t N = orep.size1();
        if (N < 1) {
            return uFALSE;
        }
        const uBool write_bonded = should_export_bonded(flags);
        // Note, am accessing elements as transposed lower tri since matrix is
        // packed as upper tri.
        if (write_bonded) {
            out << orep(0, 0) << '\n';
            if (N > 1) {
                out << orep(0, 1) << ',' << orep(1, 1) << '\n';
            }
        }
        const size_t delta = write_bonded ? 0 : 2;
        for (size_t i = 2; i < N; ++i) {
            out << orep(0, i);
            for (int j = 1; j <= (i - delta); ++j) {
                out << ',' << orep(j, i);
            }
            out << '\n';
        }
        return out.good() ? uTRUE : uFALSE;
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        std::ofstream fout(fpath.c_str(), std::ofstream::out);
        return write(fout, orep, flags);
    }
};

/**
 * Export utilities for lower triangle CSV gzipped
 */
class ExportUtilLtriCsvGz {
public:
    typedef ExportUtilLtriCsv csv_util_t;
    static const char* get_format() { return "ltri.csv.gz"; }
    static const char* get_label() { return "lower triangle CSV gzipped"; }
    static uOptE get_opt_unary() {
        return uOpt_ligc_popd_export_ltri_csv_gz_unary;
    }
    static uOptE get_opt_binary() {
        return uOpt_ligc_popd_export_ltri_csv_gz_binary;
    }
    static eExportPopdBits get_bit() { return eExportPopdBitLtriCsvGz; }
    static eExportPopdFlags get_flag() { return eExportPopdLtriCsvGz; }
    static uBool write(zstr::ofstream& out,
                       const symmat_t& orep,
                       const uUInt flags) {
        return write_csv_gz<ExportUtilLtriCsvGz>(out, orep, flags);
    }
    static uBool write(const symmat_t& orep,
                       const std::string& fpath,
                       const uUInt flags) {
        zstr::ofstream fout(fpath.c_str());
        return write(fout, orep, flags);
    }
};

/**
 * Determines if export flag is present
 */
uBool init_export_flag(uUInt& out_flags,
                       const uOptE opt_unary,
                       const uOptE opt_binary,
                       const eExportPopdBits bit,
                       const std::string& label,
                       uSpConstConfig_t config) {
    // Determine export status
    const uUInt flag = U_TO_UINT(1) << U_TO_UINT(bit);
    if (config->key_exists(opt_unary)) {
        out_flags |= flag;
    } else {
        uUInt b_enable = 0;
        config->read_into(b_enable, opt_binary);
        b_enable = (b_enable == 1) ? 1 : 0;
        out_flags |= (b_enable << U_TO_UINT(bit));
    }
    // Report export status
    const uBool b_enabled = (out_flags & flag) > 0 ? uTRUE : uFALSE;
    uLogf("Should export %s? %d\n", label.c_str(), (int)b_enabled);
    return b_enabled;
}

/**
 * Generic function to export outer replicate result
 */
template <typename export_util_t>
uBool export_stato(const symmat_t& res,
                   const std::string& dir,
                   const std::string& name_no_ext,
                   const uUInt flags) {
    const uFs::path fdir = uFs::path(dir);
    uFs_create_dirs(fdir);
    const std::string fname = name_no_ext + std::string(".") +
                              std::string(export_util_t::get_format());
    const uFs::path fpath = fdir / uFs::path(fname);
    return export_util_t::write(res, fpath.string(), flags);
}

/**
 * Conditionally install exporter
 */
template <typename export_util_t>
void export_try_install(Spec_t& spec, uSpConstConfig_t config) {
    // Check if export is enabled
    const uBool b_enable = init_export_flag(spec.m_export_flags,
                                            export_util_t::get_opt_unary(),
                                            export_util_t::get_opt_binary(),
                                            export_util_t::get_bit(),
                                            export_util_t::get_label(),
                                            config);
    if (b_enable) {
        // Resolve export directory for format
        exporter_t ex;
        uLigcCore::get_format_dir2(
            ex.m_dir, spec.m_export_dir, export_util_t::get_format());
        // Resolve export callback
        ex.m_fpwrite = &export_stato<export_util_t>;
        spec.m_exporters.push_back(ex);
    }
}

/**
 * Initializes exporter callbacks, assumes base export directory has already
 * been initialized
 */
void init_export_callbacks(Spec_t& spec, uSpConstConfig_t config) {
    spec.m_exporters.clear();
    // COO CSV
    export_try_install<ExportUtilCooCsv>(spec, config);
    // COO CSV gzipped
    export_try_install<ExportUtilCooCsvGz>(spec, config);
    // Lower banded CSV
    export_try_install<ExportUtilLbandCsv>(spec, config);
    // Lower banded CSV gzipped
    export_try_install<ExportUtilLbandCsvGz>(spec, config);
    // Lower triangle CSV
    export_try_install<ExportUtilLtriCsv>(spec, config);
    // Lower triangle CSV gzipped
    export_try_install<ExportUtilLtriCsvGz>(spec, config);
    if (spec.m_exporters.empty()) {
        uLogf("Warning: no exporters configured.\n");
    }
}

void init_export(Spec_t& spec, uSpConstConfig_t config) {
    // Job identifier
    spec.m_job_id = std::string(U_LIGC_POPD_DEFAULT_JOB_ID);
    config->read_into(spec.m_job_id, uOpt_job_id);
    uLogf("Resolved job identifier as: %s\n", spec.m_job_id.c_str());
    // Export directory
    spec.m_export_dir = uLigcCore::get_format_dir(
        config->output_dir, U_LIGC_POPD_DEFAULT_EXPORT_FORMAT_BASE);
    config->resolve_path(spec.m_export_dir, uOpt_ligc_popd_export_dir);
    uFs_normalize_dir_path(spec.m_export_dir);
    // Export flags
    spec.m_export_flags = U_TO_UINT(0);
    // Bonded ligations
    init_export_flag(spec.m_export_flags,
                     uOpt_ligc_popd_export_bonded_unary,
                     uOpt_ligc_popd_export_bonded_binary,
                     eExportPopdBitBonded,
                     "bonded",
                     config);
    // Initialize actual exporters
    init_export_callbacks(spec, config);
}

//******************************************************************
// Functions: initialize spec
//******************************************************************

/**
 * Determines BLB specification, generally exits on error
 */
void init_spec(Spec_t& spec, uSpConstConfig_t config) {
    // Initialize import settings
    init_import(spec, config);
    // Initialize contact bins
    init_contact_bins(spec, config);
    // Initialize BLB settings
    init_blb(spec, config);
    // Initialize exporter settings
    init_export(spec, config);
}

//******************************************************************
// Cache
//******************************************************************

/**
 * Cache policy, manages loading data samples from hard disk into RAM. Defines
 * interface for accessing resident data samples to Stati. Expected by BLB:
 *  ::update_first(indices) - Primes cache with first outer replicate
 *  ::update(i, indices) - Update cache such that data samples corresponding
 *      to indices are accessible
 */
class Cache {
public:
    explicit Cache(const Spec_t& spec)
        : m_spec(spec),
          m_big_id_2_gen(spec.m_blb_n),
          m_big_id_2_lil_slot(spec.m_blb_n, uMatrixUtils::fill::zeros),
          m_big_id_miss(spec.m_blb_nun, uMatrixUtils::fill::zeros),
          m_lil_slot_2_big_id(spec.m_blb_nun, uMatrixUtils::fill::zeros),
          m_lil_prof(spec.m_blb_nun),
          m_lil_w(spec.m_blb_nun, uMatrixUtils::fill::zeros),
          m_lil_logw(spec.m_blb_nun, uMatrixUtils::fill::zeros) {
        uAssert(this->check_sizes());
        m_big_id_2_gen.fill(U_UINT_MAX);
    }

    /**
     * Primes cache with first outer replicate
     * @param indices - Data sample identifiers that must be resident in RAM
     */
    void update_first(const uUIVecCol& indices) {
        uAssert(this->check_sizes());
        // Update lil slot to big id
        m_lil_slot_2_big_id = indices;
        // Prime cache
        for (uMatSz_t ix_lil = 0; ix_lil < indices.n_elem; ++ix_lil) {
            const uUInt id_big = indices.at(ix_lil);
            uAssertBounds(
                id_big, U_TO_UINT(0), U_TO_UINT(m_big_id_2_gen.n_elem));
            uAssert(m_big_id_2_gen.at(id_big) != U_TO_UINT(0));
            // Mark first generation
            m_big_id_2_gen.at(id_big) = U_TO_UINT(0);
            // Map big id to lil slot
            uAssertBounds(
                id_big, U_TO_UINT(0), U_TO_UINT(m_big_id_2_lil_slot.n_elem));
            m_big_id_2_lil_slot.at(id_big) = ix_lil;
            // Handle to profile slot
            uAssertBounds(
                ix_lil, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(m_lil_prof.size()));
            profile_t& prof = m_lil_prof[ix_lil];
            // Handle to log weight slot
            uAssertBounds(ix_lil, U_TO_MAT_SZ_T(0), m_lil_logw.n_elem);
            uReal& logw = m_lil_logw.at(ix_lil);
            // Handle to import file name
            uAssertBounds(
                id_big, U_TO_UINT(0), U_TO_UINT(m_spec.m_import_fnames.size()));
            const std::string& fname = m_spec.m_import_fnames[id_big];
            // Stream in missing data
            uVerify(
                m_spec.m_im.m_fpprof(prof, logw, m_spec.m_import_dir, fname));
        }
        // Transform weights
        uReal max_logw;
        uSisUtilsQc::calc_norm_exp_weights(m_lil_w, max_logw, m_lil_logw);
        uAssertRealEq(U_TO_REAL(1.0), m_lil_w.max());
    }

    /**
     * Update caches with new elements, possibly evict old elements
     * @param i - Data generation index
     * @param indices - Data sample identifiers that must be resident in RAM
     */
    void update(const uUInt i, const uUIVecCol& indices) {
        uAssertBounds(i, U_TO_UINT(1), m_spec.m_blb_repo);
        uAssert(this->check_sizes());
        // Mark latest generation
        const uUInt prev_gen = i - U_TO_UINT(1);
        uMatSz_t num_miss = U_TO_MAT_SZ_T(0);
        for (uMatSz_t ix_lil = 0; ix_lil < indices.n_elem; ++ix_lil) {
            const uUInt id_big = indices.at(ix_lil);
            uAssertBounds(
                id_big, U_TO_UINT(0), U_TO_UINT(m_big_id_2_gen.n_elem));
            // Check if data needs to be streamed
            if (m_big_id_2_gen.at(id_big) != prev_gen) {
                uAssertBounds(num_miss, U_TO_MAT_SZ_T(0), m_big_id_miss.n_elem);
                m_big_id_miss.at(num_miss) = id_big;
                ++num_miss;
            }
            m_big_id_2_gen.at(id_big) = i;
        }
        uAssertBoundsInc(num_miss, U_TO_MAT_SZ_T(0), m_spec.m_blb_nun);
        // Update slots
        if (num_miss < U_TO_UINT(1)) {
            // Everything already cached, early out!
            return;
        }
        uMatSz_t i_miss = 0;
        uUInt id_big_miss = m_big_id_miss.at(i_miss);
        // Iterate over current data and check for eviction
        for (uMatSz_t ix_lil = U_TO_UINT(0);
             ix_lil < m_lil_slot_2_big_id.n_elem;
             ++ix_lil) {
            const uUInt id_big = m_lil_slot_2_big_id.at(ix_lil);
            uAssertBounds(
                id_big, U_TO_UINT(0), U_TO_UINT(m_big_id_2_gen.n_elem));
            if (m_big_id_2_gen.at(id_big) != i) {
                // Evict old data and stream in missing data
                m_lil_slot_2_big_id.at(ix_lil) = id_big_miss;
                uAssertBounds(id_big_miss,
                              U_TO_UINT(0),
                              U_TO_UINT(m_big_id_2_lil_slot.n_elem));
                m_big_id_2_lil_slot.at(id_big_miss) = ix_lil;
                // Handle to profile slot
                uAssertBounds(
                    ix_lil, U_TO_MAT_SZ_T(0), U_TO_MAT_SZ_T(m_lil_prof.size()));
                profile_t& prof = m_lil_prof[ix_lil];
                // Handle to log weight slot
                uAssertBounds(ix_lil, U_TO_MAT_SZ_T(0), m_lil_logw.n_elem);
                uReal& logw = m_lil_logw.at(ix_lil);
                // Handle to import file name
                uAssertBounds(id_big_miss,
                              U_TO_UINT(0),
                              U_TO_UINT(m_spec.m_import_fnames.size()));
                const std::string& fname = m_spec.m_import_fnames[id_big_miss];
                // Stream in missing data
                uVerify(m_spec.m_im.m_fpprof(
                    prof, logw, m_spec.m_import_dir, fname));
                // Determine next data to stream in
                ++i_miss;
                if (i_miss == num_miss) {
                    break;
                }
                uAssertBounds(i_miss, U_TO_MAT_SZ_T(0), num_miss);
                uAssertBounds(i_miss, U_TO_MAT_SZ_T(0), m_big_id_miss.n_elem);
                id_big_miss = m_big_id_miss.at(i_miss);
            }
        }
        uAssert(i_miss == num_miss);
        uAssert(!uMatrixUtils::any((uMatrixUtils::sort(indices) -
                                    uMatrixUtils::sort(m_lil_slot_2_big_id))));
        // Transform weights
        uReal max_logw;
        uSisUtilsQc::calc_norm_exp_weights(m_lil_w, max_logw, m_lil_logw);
        uAssertRealEq(U_TO_REAL(1.0), m_lil_w.max());
    }

    /**
     * @WARNING: Assumed to be resident in cache
     * @return Profile mapped to big identifier
     */
    inline const profile_t& get_prof(const uUInt big_id) const {
        uAssert(this->check_sizes());
        const uUInt lil_slot = this->get_lil_slot(big_id);
        uAssertBounds(lil_slot, U_TO_UINT(0), U_TO_UINT(m_lil_prof.size()));
        return m_lil_prof[lil_slot];
    }

    /**
     * @WARNING: Assumed to be resident in cache
     * @return Transformed weight (non-log) mapped to big identifier
     */
    inline uReal get_w(const uUInt big_id) const {
        uAssert(this->check_sizes());
        const uUInt lil_slot = this->get_lil_slot(big_id);
        uAssertBounds(lil_slot, U_TO_UINT(0), U_TO_UINT(m_lil_w.n_elem));
        return m_lil_w.at(lil_slot);
    }

private:
    /**
     * Determine which slot data sample resides
     */
    inline uUInt get_lil_slot(const uUInt big_id) const {
        uAssert(this->check_sizes());
        uAssertBounds(
            big_id, U_TO_UINT(0), U_TO_UINT(m_big_id_2_lil_slot.n_elem));
        const uUInt lil_slot = m_big_id_2_lil_slot.at(big_id);
        uAssertBounds(lil_slot, U_TO_UINT(0), m_lil_slot_2_big_id.n_elem);
        uAssert(big_id == m_lil_slot_2_big_id.at(lil_slot));
        return lil_slot;
    }

#ifdef U_BUILD_ENABLE_ASSERT
    /**
     * Asserts if size mismatch encountered
     */
    bool check_sizes() const {
        // If this assert trips, too many outer replicates specified
        uAssertBoundsInc(
            m_spec.m_blb_repo, U_TO_UINT(1), U_LIGC_POPD_MAX_BLB_REPO);
        uAssertBoundsInc(m_spec.m_blb_nun, U_TO_UINT(1), m_spec.m_blb_n);
        uAssertPosEq(U_TO_UINT(m_spec.m_import_fnames.size()), m_spec.m_blb_n);
        uAssertPosEq(m_big_id_2_gen.n_elem, U_TO_MAT_SZ_T(m_spec.m_blb_n));
        uAssertPosEq(m_big_id_2_lil_slot.n_elem, U_TO_MAT_SZ_T(m_spec.m_blb_n));
        uAssertPosEq(m_big_id_miss.n_elem, U_TO_MAT_SZ_T(m_spec.m_blb_nun));
        uAssertPosEq(m_lil_slot_2_big_id.n_elem,
                     U_TO_MAT_SZ_T(m_spec.m_blb_nun));
        uAssertPosEq(U_TO_UINT(m_lil_prof.size()), m_spec.m_blb_nun);
        uAssertPosEq(m_lil_w.n_elem, U_TO_MAT_SZ_T(m_spec.m_blb_nun));
        uAssertPosEq(m_lil_logw.n_elem, U_TO_MAT_SZ_T(m_spec.m_blb_nun));
        return true;
    }
#endif  // U_BUILD_ENABLE_ASSERT

    /**
     * Handle to BLB specification
     */
    const Spec_t& m_spec;

    /**
     * 'big' - indices/identifiers over full data set
     * 'lil' - slots/indices/identifiers over BLB set
     */

    /**
     * Generation identifiers over full data set. Maps big id to generation
     */
    uUIVecCol m_big_id_2_gen;

    /**
     * Slot identifiers over full data set. Maps big id to little slot
     */
    uUIVecCol m_big_id_2_lil_slot;

    /**
     * Contains big ids that need to be streamed in
     */
    uUIVecCol m_big_id_miss;

    /**
     * Maps little slot to big id
     */
    uUIVecCol m_lil_slot_2_big_id;

    /**
     * Resident data. Maps little slots to sample data
     */
    std::vector<profile_t> m_lil_prof;

    /**
     * Transformed weights with max weight = 1
     */
    uVecCol m_lil_w;

    /**
     * Raw log weights
     */
    uVecCol m_lil_logw;
};

//******************************************************************
// Stati - Inner replicate statistic
//******************************************************************

/**
 * Assigns zero to all elements
 */
inline void zero_fill(symmat_t& mat) {
    std::fill(mat.data().begin(), mat.data().end(), U_TO_REAL(0.0));
}

/**
 * Inner replicate statistic policy, defines the statistic computed within
 * each inner replicate. Expected BLB interface :
 *  ::init(repi) - initialize given the positive number of inner replicates,
 *      called once before any outer replicates generated
 *  ::start() - called once for every *outer* replicate before (re)-generating
 *      inner replicates, intended to reset capture state
 *  ::update(ii, indices, counts, cache THREAD_ID) - called for each inner
 *      replicate 'ii', intended to update (i.e. capture) inner estimator
 *      where indices is vector specifying which data samples are to be
 *      accessed, counts is vector parallel to indices and is the multiplicity
 *      of each data sample, and cache implicitly provides an interface for
 *      accessing data samples at indices
 *  ::finish() - called once for each outer replicate, may be used to
 *      normalize capture, export capture, etc.
 */
class Stati {
public:
    /**
     * Constructor
     */
    explicit Stati(const Spec_t& spec) : m_spec(spec) {
        uAssert(spec.m_num_bins >= U_TO_UINT(1));
        U_INIT_TLS_DATA_FOR_EACH_2_PARAM(
            m_freqtab, resize, spec.m_num_bins, false);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(m_lil_w_irep, zeros, spec.m_blb_nun);
        U_INIT_TLS_DATA_FOR_EACH_1_PARAM(
            m_lil_indices_sparse, zeros, spec.m_blb_nun);
    }

    /**
     * Initialize - called once
     * @param repo - Number of inner replicates
     */
    void init(const uUInt repi) {}

    /**
     * (Re-)start statistical capture for inner replicate
     */
    void start() { U_TLS_DATA_FOR_EACH_0_PARAM_NONMEMB(m_freqtab, zero_fill); }

    /**
     * Update statistical capture with completed inner replicate
     * @param ii - Inner replicate index
     * @param indices - Identifies data samples to be accessed in cache
     * @param counts - Replicate count for each element in 'indices'
     * @Param cache - Provides access to data samples
     */
    void update(const uUInt ii,
                const uUIVecCol& indices,
                const uUIVecCol& counts,
                const Cache& cache U_THREAD_ID_PARAM) {
        uAssertBounds(ii, U_TO_UINT(0), m_spec.m_blb_repi);
        // Handle to counts matrix
        symmat_t& mat = U_ACCESS_TLS_DATA(m_freqtab);
        uAssertPosEq(mat.size1(), mat.size2());
        uAssertPosEq(mat.size1(), m_spec.m_num_bins);
        // Handle to inner replicate weights vector
        uVecCol& w_irep = U_ACCESS_TLS_DATA(m_lil_w_irep);
        uUIVecCol lil_indices_sparse = U_ACCESS_TLS_DATA(m_lil_indices_sparse);
        const uMatSz_t NUN = U_TO_MAT_SZ_T(m_spec.m_blb_nun);
        uAssertPosEq(w_irep.n_elem, NUN);
        uAssertPosEq(lil_indices_sparse.n_elem, NUN);
        uAssertPosEq(indices.n_elem, NUN);
        uAssertPosEq(counts.n_elem, NUN);
        // Update weights
        uMatSz_t n_sparse = U_TO_MAT_SZ_T(0);
        uReal w_z = U_TO_REAL(0.0);
        for (uMatSz_t k = 0; k < NUN; ++k) {
            const uUInt count = counts.at(k);
            if (count > U_TO_UINT(0)) {
                uAssertBounds(
                    n_sparse, U_TO_MAT_SZ_T(0), lil_indices_sparse.n_elem);
                lil_indices_sparse.at(n_sparse++) = U_TO_UINT(k);
                const uUInt big_id = indices.at(k);
                const uReal countf = U_TO_REAL(count);
                const uReal w = cache.get_w(big_id);
                const uReal w_mul = countf * w;
                w_z += w_mul;
                w_irep.at(k) = w_mul;
            }
        }
        uAssertBoundsInc(n_sparse, U_TO_MAT_SZ_T(1), NUN);
        uAssert(w_z > U_TO_REAL(0.0));
        // Average over number of inner replicates
        w_z *= U_TO_REAL(m_spec.m_blb_repi);
        // Take inverse to avoid inner loop division
        w_z = U_TO_REAL(1.0) / w_z;
        // Tabulate contacts
        for (uMatSz_t q = 0; q < n_sparse; ++q) {
            const uMatSz_t k = U_TO_MAT_SZ_T(lil_indices_sparse.at(q));
            uAssertBounds(k, U_TO_MAT_SZ_T(0), NUN);
            const uUInt big_id = indices.at(k);
            const profile_t& prof = cache.get_prof(big_id);
            const uReal w_norm = w_irep.at(k) * w_z;
            const size_t num_contacts = prof.size();
            for (size_t l = 0; l < num_contacts; ++l) {
                const contact_t& tup(prof[l]);
                uAssertBounds(tup.first, U_TO_UINT(0), m_spec.m_num_bins);
                uAssertBounds(tup.second, U_TO_UINT(0), m_spec.m_num_bins);
                // For cache coherency reasons, assuming upper triangle access
                uAssert(tup.first <= tup.second);
                mat(tup.first, tup.second) += w_norm;
            }
        }
    }

    /**
     * Finish statistical capture for current outer replicate
     */
    void finish() {
        // Reduce matrices
#ifdef U_BUILD_ENABLE_THREADS
        uAssert(!m_freqtab.empty());
        const size_t n = m_freqtab.size();
        symmat_t& out = m_freqtab[0];
        for (size_t i = 1; i < n; ++i) {
            out += m_freqtab[i];
        }
#endif  // U_BUILD_ENABLE_THREADS
    }

    /**
     * @return mean matrix over inner replicates
     */
    const symmat_t& get_mean() const {
#ifdef U_BUILD_ENABLE_THREADS
        uAssert(!m_freqtab.empty());
        return m_freqtab[0];
#else
        return m_freqtab;
#endif  // U_BUILD_ENABLE_THREADS
    }

private:
    /**
     * Handle to BLB specification
     */
    const Spec_t& m_spec;

    /**
     * Thread local inner replicate results
     */
    U_DECLARE_TLS_DATA(symmat_t, m_freqtab);

    /**
     * TLS buffer for storing normalized weights at each inner replicate
     */
    U_DECLARE_TLS_DATA(uVecCol, m_lil_w_irep);

    /**
     * Stores 'lil' indices of data samples with non-zero counts
     */
    U_DECLARE_TLS_DATA(uUIVecCol, m_lil_indices_sparse);
};

//******************************************************************
// Stato - Outer replicate statistic
//******************************************************************

/**
 * Outer replicate statistic, defines the statistic computed over the inner
 * replicates. Expected BLB interface:
 *  ::init(repo) - initialize given the positive integer number of outer
 *      replicates, called once before any replicates generated
 *  ::start() - for symmetry with stati_t, called once immediately after
 *      init(...) to indicate start of statistical capture
 *  ::update(uUInt i, stati) - called after the i-th outer replicate has
 *      completed, intended to update (i.e. capture) outer estimator based on
 *      inner result
 *  ::finish() - called once after all outer replicates finished, may be used
 *      to normalize capture, export capture, etc.
 */
class Stato {
public:
    /**
     * Constructor
     */
    explicit Stato(const Spec_t& spec) : m_spec(spec) {}

    /**
     * Initialize - called once
     * @param repo - Number of outer replicates
     */
    void init(const uUInt repo) {}

    /**
     * Start statistical capture for current outer replicate
     */
    void start() {}

    /**
     * Update statistical capture with completed inner replicate
     * @param i - Outer replicate index
     * @param stati - Inner replicate statistic
     */
    void update(const uUInt i, const Stati& stati) {
        // Export mean inner replicate result
        const symmat_t& mat = stati.get_mean();
        uAssertPosEq(mat.size1(), mat.size2());
        uAssertPosEq(mat.size1(), m_spec.m_num_bins);
        const size_t num_exporters = m_spec.m_exporters.size();
        const std::string name_no_ext = std::string("ligc.popd.") +
                                        m_spec.m_job_id + std::string(".") +
                                        u2Str(i);
        for (size_t i = 0; i < num_exporters; ++i) {
            const exporter_t& ex = m_spec.m_exporters[i];
            ex.m_fpwrite(mat, ex.m_dir, name_no_ext, m_spec.m_export_flags);
        }
    }

    /**
     * Finish statistical capture for all outer replicates
     */
    void finish() {}

private:
    /**
     * Handle to BLB specification
     */
    const Spec_t& m_spec;
};

}  // namespace uLigcPopd

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point
 */
int uCmdletLigcPopdMain(const uCmdOptsMap& cmd_opts) {
    // Create configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);

    // Output configuration
    uLogf("Calculating ligation population distribution.\n");
    config->print();

    // Initialize globals
    uG::init(config);
    U_LOG_NUM_WORKER_THREADS;

    // Initialize specification
    uLigcPopd::Spec_t spec;
    uLigcPopd::init_spec(spec, config);

    // BLB
    uLigcPopd::Cache cache_(spec);
    uLigcPopd::Stati stati_(spec);
    uLigcPopd::Stato stato_(spec);
    uBlb::bootnun(spec.m_blb_repo,
                  spec.m_blb_repi,
                  spec.m_blb_n,
                  spec.m_blb_nun,
                  stato_,
                  stati_,
                  cache_,
                  spec.m_blb_chunk_size);

    // Destroy globals
    uG::teardown();

    return uExitCode_normal;
}

#endif  // U_BUILD_ENABLE_COMMANDLETS
