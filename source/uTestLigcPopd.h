//****************************************************************************
// uTestLigcPopd.h
//****************************************************************************

/**
 * Commandlet for testing ligation contact bootstrap (BLB) distribution
 *
 * Usage:
 * -test_ligc_popd [--conf <conf_path>]
 */
#ifdef uTestLigcPopd_h
#   error "Test Ligc Popd included multiple times!"
#endif  // uTestLigcPopd_h
#define uTestLigcPopd_h

#include "uBuild.h"

#if (defined(U_BUILD_ENABLE_TESTS) && defined(U_BUILD_ENABLE_COMMANDLETS))

#include "uCmdOptsMap.h"
#include "uExitCodes.h"
#include "uLogf.h"

/**
 * Entry point for test
 */
int uTestLigcPopdMain(const uCmdOptsMap& cmd_opts_) {
    uLogf("Running Ligc Popd Test.\n");
    uCmdOptsMap cmd_opts(cmd_opts_);
    // Check if we should set default configuration path
    const std::string DEFAULT_CONF_PATH = "../tests/test_ligc_popd/ligc.ini";
    cmd_opts.set_option_if_absent(uOpt_get_cmd_switch(uOpt_conf),
                                  DEFAULT_CONF_PATH);
    // Smoke test
    return uCmdletLigcPopdMain(cmd_opts);
}

#endif  // (defined(U_BUILD_ENABLE_TESTS) &&
        // defined(U_BUILD_ENABLE_COMMANDLETS))
