//****************************************************************************
// uSisNodeRadiusMixin.h
//****************************************************************************

/**
 * Common sample-level node radius mixin
 */

#ifndef uSisNodeRadiusMixin_h
#define uSisNodeRadiusMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uSisUtilsGrowth.h"

//****************************************************************************
// Sample level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNodeRadiusMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Flag that simulation is homogeneous wrt to node radius
     */
    enum { is_homg_radius = sim_level_node_radius_mixin_t::is_homg_radius };

    /**
     * Initializes mixin
     * @param sample - parent sample containing this mixin
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sample_t& sample,
              const sim_t& sim,
              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * @param node_id - node identifier to obtain radius of
     * @param sim - parent simulation
     * @return radius of i-th node
     */
    inline uReal get_node_radius(const uUInt node_id, const sim_t& sim) const {
        return sim.get_node_radius(node_id);
    }

    /**
     * @return vector of radii at this sample
     */
    inline const uVecCol& get_node_radii(const sim_t& sim) const {
        return sim.get_node_radii();
    }

    /**
     * @param node_id - node identifier to obtain radius of
     * @param sim - parent simulation
     * @return radius of candidate for i-th node
     */
    inline uReal get_candidate_node_radius(const uUInt candidate_node_id,
                                           const sim_t& sim) const {
        return sim.get_node_radius(candidate_node_id);
    }

    /**
     * Compute max node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible distance between the two node centers
     */
    inline uReal get_max_c2c_len(const uUInt start_id,
                                 const uUInt end_id,
                                 const sim_t& sim) const {
        return sim.get_max_c2c_len(start_id, end_id);
    }

    /**
     * Compute max squared node center-to-center length of a subsegment
     * @param start_id - index of first node identifier, must be <= end_id
     * @param end_id - index of second node identifier, must be >= start_id
     * @return maximum possible squared distance between the two node centers
     */
    inline uReal get_max_sq_c2c_len(const uUInt start_id,
                                    const uUInt end_id,
                                    const sim_t& sim) const {
        return sim.get_max_sq_c2c_len(start_id, end_id);
    }
};

#endif  // uSisNodeRadiusMixin_h
