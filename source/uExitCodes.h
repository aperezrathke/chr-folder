//****************************************************************************
// uExitCodes.h
//****************************************************************************

/**
 * Defines the program exit codes
 */

#ifndef uExitCodes_h
#define uExitCodes_h

#include "uBuild.h"

/**
 * Exit codes
 */
enum uExitCode {
    /**
     * Program terminated normally
     */
    uExitCode_normal = 0,
    /**
     * Program terminated with error
     */
    uExitCode_error,
    /**
     * Error code for when a simulation failed to generate a sample, intended
     * for use with fea app(s)
     */
    uExitCode_error_no_fold
};

#endif  // uExitCodes_h
