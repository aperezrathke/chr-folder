//****************************************************************************
// uSisOfaCollisionSimLevelMixin.inl
//****************************************************************************

/**
 * A collision mixin manages the details for checking candidate nodes to see if
 * the satisfy self-avoidance (do not collide with any existing nodes)
 *
 * An accessory structure is the simulation level collision mixin which
 * define global data and shared utilities among all samples.
 *
 * Sis - Sequential importance sampling
 * Ofa - Uses broad phase filtering and then applies a narrow phase overlap
 *  factor (allows partial overlap) collision model to enforce self-avoidance
 *  among monomer nodes
 */

/**
 * Mixin encapsulating global (sample independent) data and utilities. In
 * this case, the simulation level collision mixin manages the pre-allocated
 * scratch buffers used in collision checking.
 */
// @HACK - Inheritance is used instead of composition, re-using parent class
//  for management of broad phase
template <typename t_SisGlue>
#ifdef U_INJECT_THREADED_TLS
class uSisOfaCollisionSimLevelMixin_threaded
    : public uSisHardShellCollisionSimLevelMixin_threaded<t_SisGlue>
#else
class uSisOfaCollisionSimLevelMixin_serial
    : public uSisHardShellCollisionSimLevelMixin_serial<t_SisGlue>
#endif  // U_INJECT_THREADED_TLS
{
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    // Parent collision mixin type
#ifdef U_INJECT_THREADED_TLS
    typedef uSisHardShellCollisionSimLevelMixin_threaded<glue_t>
        parent_sim_level_collision_mixin_t;
#else
    typedef uSisHardShellCollisionSimLevelMixin_serial<glue_t>
        parent_sim_level_collision_mixin_t;
#endif  // U_INJECT_THREADED_TLS

    /**
     * Broad phase results type
     */
    typedef typename parent_sim_level_collision_mixin_t::broad_phase_results_t
        broad_phase_results_t;

    /**
     * Initializes simulation level collision mixin by pre-allocating scratch
     * buffers used for collision checking.
     * @param sim - parent simulation containing global sample data
     * @param config - user configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {
        // Reset state
        this->clear();
        // Initialize broad phase (managed by parent)
        parent_sim_level_collision_mixin_t::init(sim, config U_THREAD_ID_ARG);
        // Initialize overlap factor (ofa) threshold
        // Set default to 'hard shell' => ofa = 1.0
        m_collision_ofa2 = U_TO_REAL(1.0);
        config->read_into(m_collision_ofa2, uOpt_collision_ofa);
        if ((m_collision_ofa2 < U_TO_REAL(0.0)) ||
            (m_collision_ofa2 > U_TO_REAL(1.0))) {
            uLogf(
                "Warning: collision overlap factor (ofa = %f) not in [0,1], "
                "clipping to be in range.\n",
                (double)m_collision_ofa2);
            m_collision_ofa2 =
                U_CLIP(m_collision_ofa2, U_TO_REAL(0.0), U_TO_REAL(1.0));
        }
        uLogf("Initialized collision overlap factor: %f\n",
              (double)m_collision_ofa2);
        // Square overlap factor
        m_collision_ofa2 *= m_collision_ofa2;
        // Check if homogeneous squared distance threshold should be updated
        // according to overlap factor
        if (sim_t::is_homg_radius) {
            const uReal sq_dist = m_collision_ofa2 *
                                  parent_sim_level_collision_mixin_t::
                                      get_collision_homg_min_n2n_sq_dist();
            parent_sim_level_collision_mixin_t::
                set_collision_homg_min_n2n_sq_dist(sq_dist);
        }
    }

    /**
     * Resets state
     */
    void clear() {
        parent_sim_level_collision_mixin_t::clear();
        m_collision_ofa2 = U_TO_REAL(0.0);
    }

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * @return Minimum squared distance collision threshold for heterogeneous
     *  nodes
     */
    inline uReal calc_collision_hetr_min_n2n_sq_dist(
        const uReal radius_a,
        const uReal radius_b) const {
        uAssert(radius_a > U_TO_REAL(0.0));
        uAssert(radius_b > U_TO_REAL(0.0));
        uReal min_n2n_sq_dist = radius_a + radius_b;
        min_n2n_sq_dist *= min_n2n_sq_dist;
        // Modulate by squared overlap factor
        min_n2n_sq_dist *= m_collision_ofa2;
        min_n2n_sq_dist -= U_REAL_EPS;
        return min_n2n_sq_dist;
    }

private:
    /**
     * Squared collision overlap factor where 1.0 is canonical 'hard shell'
     * collision, 0.0 is effectively no collisions, and value in (0,1) is
     * 'soft' collisions according to monomer overlap percentage. Note, if
     * this value is > 1.0, then collision radius is inflated such that the
     * minimal distance among the nodes will be greater than the sum of their
     * radii.
     */
    uReal m_collision_ofa2;
};
