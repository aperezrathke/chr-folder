//****************************************************************************
// uIntrLibParser.cpp
//****************************************************************************

/**
 * Utility for parsing constraint configurations
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uIntrLibParser.h"
#include "uAssert.h"
#include "uStringUtils.h"

// Include sort() and unique()
#include <algorithm>
// Include isdigit() method
#include <ctype.h>
// Include C++ input file streams
#include <fstream>
#include <sstream>

//****************************************************************************
// Helper namespace
//****************************************************************************

namespace {
/**
 * Utility extracts a non-negative integer pair from string
 * @param pair_buffer - Output integer pair is stored in this buffer
 * @param line - The string to parse for the integer pair
 * @return uTRUE if integer pair successfully parsed, uFALSE o/w
 */
uBool parse_index_pair(std::pair<uUInt, uUInt>& pair_buffer,
                       const std::string& line) {
    // Early out if line is empty
    if (line.empty()) {
        return uFALSE;
    }

    // Buffer for storing a non-negative integer
    std::string nnegi("");

    // Current position in line
    size_t i = 0;

    // Eat up any leading non-digit characters
    while (!isdigit(line[i])) {
        if (++i >= line.size()) {
            // No integers found before end of line reached
            return uFALSE;
        }
    }
    uAssert(i < line.size());
    uAssert(isdigit(line[i]));

    // Parse first non-negative integer
    while (isdigit(line[i])) {
        nnegi += line[i];
        if (++i >= line.size()) {
            // Only 1 integer found before end of line reached
            return uFALSE;
        }
    }
    uAssert(i < line.size());
    uAssert(!isdigit(line[i]));
    pair_buffer.first = uStringUtils::string_as_T<uUInt>(nnegi);

    // Eat up gap non-digit characters
    while (!isdigit(line[i])) {
        if (++i >= line.size()) {
            // Only 1 integer found before end of line reached
            return uFALSE;
        }
    }
    uAssert(i < line.size());
    uAssert(isdigit(line[i]));

    // Parse second non-negative integer
    nnegi = "";
    while (isdigit(line[i])) {
        nnegi += line[i];
        if (++i >= line.size()) {
            break;
        }
    }
    uAssert(!nnegi.empty());
    pair_buffer.second = uStringUtils::string_as_T<uUInt>(nnegi);

    // Found non-negative integer pair
    return uTRUE;
}

/**
 * @param out_mask - output parsed mask, only modified if legal values read
 * @param stream - stream to parse
 */
void parse_index_mask(uUIVecCol& out_mask, std::istream& stream) {
    // Parse file digit by digit
    std::vector<uUInt> parsed_mask;
    std::string index_str("");
    char c;
    while (stream.get(c)) {
        if (isdigit(c)) {
            index_str += c;
        } else {
            if (!index_str.empty()) {
                const uUInt index = uStringUtils::string_as_T<uUInt>(index_str);
                parsed_mask.push_back(index);
                index_str.clear();
            }
        }
    }

    // Handle last parsed index
    if (!index_str.empty()) {
        const uUInt index = uStringUtils::string_as_T<uUInt>(index_str);
        parsed_mask.push_back(index);
        index_str.clear();
    }

    // Copy to index vector
    if (!parsed_mask.empty()) {
        // Sort and strip duplicates
        std::sort(parsed_mask.begin(), parsed_mask.end());
        parsed_mask.erase(std::unique(parsed_mask.begin(), parsed_mask.end()),
                          parsed_mask.end());
        uAssert(!parsed_mask.empty());
        // Copy sorted, unique indices
        const size_t n_index = parsed_mask.size();
        out_mask.set_size(U_TO_MAT_SZ_T(n_index));
        for (size_t i = 0; i < n_index; ++i) {
            const uUInt index = parsed_mask[i];
            out_mask.at(U_TO_MAT_SZ_T(i)) = index;
        }
    }
}

/**
 * @param out_mask - output parsed mask, only modified if legal values read
 * @param stream - stream to parse
 */
void parse_bit_mask(uBoolVecCol& out_mask, std::istream& stream) {
    // Parse file bit by bit
    std::vector<uBool> parsed_mask;
    char c;
    while (stream.get(c)) {
        if (c == '0') {
            parsed_mask.push_back(uFALSE);
        } else if (c == '1') {
            parsed_mask.push_back(uTRUE);
        }
    }

    // Copy to bit vector
    if (!parsed_mask.empty()) {
        const size_t n_index = parsed_mask.size();
        out_mask.set_size(U_TO_MAT_SZ_T(n_index));
        for (size_t i = 0; i < n_index; ++i) {
            const uBool bit = parsed_mask[i];
            out_mask.at(U_TO_MAT_SZ_T(i)) = bit;
        }
    }
}

}  // namespace

//****************************************************************************
// Implementation
//****************************************************************************

namespace uIntrLibParser {
/**
 * Parses file where each line is a non-negative integer pair. The expected
 * format is:
 *
 *      <first_index><sep><second_index>
 *      ...
 *      <first_index><sep><second_index>
 *
 *  where <*index> is a 0-based integer index. The <sep> field separates
 *  integer indices and can be any length set of consecutive non-numeric
 *  characters. Therefore, <sep> formats such as comma-separated or white
 *  space delimited are both acceptable. Here is an example file in white
 *  space delimited format:
 *
 *          <line 0>: 0 4
 *          <line 1>: 5 8
 *          <line 2>: 9 12
 *          <line 3>: 13 13
 *
 * @param out_pairs - 2 x NUM_PAIRS matrix of indices where each column is a
 *  line in index file, stored column major as that is internal memory layout
 *  of matrix class.
 * @WARNING - out_pairs matrix is cleared even if no indices loaded
 * @param fpath - Path to index file where each line is a pair of 0-based
 *  indices separated by non-numeric characters such as commas and/or
 *  whitespace.
 * @param b_order - If 'b_order' is TRUE, then the smaller index in pair is
 *  stored in row defined by enum uIntrLibPairIxMin and the larger index is
 *  stored in row defined by enum uIntrLibPairIxMax. If 'b_order' is FALSE,
 *  the first index read in pair is in row enum uIntrLibPairIxMin and second
 *  index read is in row enum uIntrLibPairIxMax.
 * @return uTRUE if successful, uFALSE o/w
 */
uBool read_index_pairs(uUIMatrix& out_pairs,
                       const std::string& fpath,
                       const uBool b_order) {
    // Reset to empty matrix
    out_pairs.clear();

    // Attempt to open file
    std::ifstream fin(fpath.c_str());
    if (!fin) {
        // ERROR: Unable to open file
        return uFALSE;
    }

    // Parse file line by line
    std::string line;
    std::pair<uUInt, uUInt> pair_buffer;
    std::vector<std::pair<uUInt, uUInt> > parsed_pairs;
    while (std::getline(fin, line)) {
        if (parse_index_pair(pair_buffer, line)) {
            parsed_pairs.push_back(pair_buffer);
        }
    }
    fin.close();

    // Check pairs were read
    const size_t n_pairs = parsed_pairs.size();
    if (n_pairs <= 0) {
        // ERROR: Unable to find any index pairs
        return uFALSE;
    }

    // Copy to index matrix
    out_pairs.set_size(U_TO_MAT_SZ_T(uIntrLibPairIxNum) /*n_rows*/,
                       U_TO_MAT_SZ_T(n_pairs) /*n_cols*/);

    if (b_order) {
        uUInt min_ix;
        uUInt max_ix;
        for (size_t i = 0; i < n_pairs; ++i) {
            pair_buffer = parsed_pairs[i];
            min_ix = std::min(pair_buffer.first, pair_buffer.second);
            max_ix = std::max(pair_buffer.first, pair_buffer.second);
            out_pairs.at(uIntrLibPairIxMin, U_TO_MAT_SZ_T(i)) = min_ix;
            out_pairs.at(uIntrLibPairIxMax, U_TO_MAT_SZ_T(i)) = max_ix;
        }
    } else {
        for (size_t i = 0; i < n_pairs; ++i) {
            out_pairs.at(uIntrLibPairIxMin, U_TO_MAT_SZ_T(i)) =
                parsed_pairs[i].first;
            out_pairs.at(uIntrLibPairIxMax, U_TO_MAT_SZ_T(i)) =
                parsed_pairs[i].second;
        }
    }
    return uTRUE;
}

/**
 * Extract set of unsigned integers present in file
 * @param out_mask - output set of unsigned integers read from file
 * @WARNING - out_mask is cleared even if no indices loaded
 * @param fpath - Path to integer mask file
 * @return uTRUE if successful, uFALSE o/w
 */
uBool read_index_mask(uUIVecCol& out_mask, const std::string& fpath) {
    // Reset to empty mask
    out_mask.clear();

    // Check if path is empty
    if (fpath.empty()) {
        // ERROR: empty path
        return uFALSE;
    }

    // Attempt to open file
    std::ifstream fin(fpath.c_str());
    if (!fin) {
        // ERROR: Unable to open file
        return uFALSE;
    }

    parse_index_mask(out_mask, fin);

    // Close file handle
    fin.close();

    // File was able to be opened and parsed
    return uTRUE;
}

/**
 * Extract set of unsigned integers present in parameter mask string
 * @param out_mask - output set of unsigned integers read from file
 * @WARNING - out_mask is cleared even if no indices loaded
 * @param mask_str - Mask string containing index set
 * @param uTRUE if successful, uFALSE o/w
 */
uBool read_index_mask_inplace(uUIVecCol& out_mask,
                              const std::string& mask_str) {
    // Reset to empty mask
    out_mask.clear();

    // Check if mask is empty
    if (mask_str.empty()) {
        // No mask available
        return uTRUE;
    }

    // Attempt to open file
    std::istringstream ist(mask_str);
    if (!ist) {
        // ERROR: Unable to open string stream
        return uFALSE;
    }

    parse_index_mask(out_mask, ist);

    // Stream was able to be opened and parsed
    return uTRUE;
}

/**
 * Extract set of 0|1 bits present in file
 * @param out_mask - output set of 0|1 bits read from file
 * @WARNING - out_mask is cleared even if no bits loaded
 * @param fpath - Path to bit mask file
 * @return uTRUE if successful, uFALSE o/w
 */
uBool read_bit_mask(uBoolVecCol& out_mask, const std::string& fpath) {
    // Reset to empty mask
    out_mask.clear();

    // Check if path is empty
    if (fpath.empty()) {
        // ERROR: empty path
        return uFALSE;
    }

    // Attempt to open file
    std::ifstream fin(fpath.c_str());
    if (!fin) {
        // ERROR: Unable to open file
        return uFALSE;
    }

    parse_bit_mask(out_mask, fin);

    // Close file handle
    fin.close();

    // File was able to be opened and parsed
    return uTRUE;
}

/**
 * Extract set of 0|1 bits present in parameter mask string
 * @param out_mask - output set of 0|1 bits read from file
 * @WARNING - out_mask is cleared even if no bits loaded
 * @param mask_str - Mask string containing bit set
 * @param uTRUE if successful, uFALSE o/w
 */
uBool read_bit_mask_inplace(uBoolVecCol& out_mask,
                            const std::string& mask_str) {

    // Reset to empty mask
    out_mask.clear();

    // Check if mask is empty
    if (mask_str.empty()) {
        // No mask available
        return uTRUE;
    }

    // Attempt to open file
    std::istringstream ist(mask_str);
    if (!ist) {
        // ERROR: Unable to open string stream
        return uFALSE;
    }

    parse_bit_mask(out_mask, ist);

    // Stream was able to be opened and parsed
    return uTRUE;
}

}  // namespace uIntrLibParser
