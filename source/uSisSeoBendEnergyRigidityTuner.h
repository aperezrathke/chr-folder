//****************************************************************************
// uSisSeoBendEnergyRigidityTuner.h
//****************************************************************************

/**
 * Tuner uses stochastic approximation root finding to assign bending
 * rigidities at each node.
 */

/**
 * Tuner works by assigning all nodes a default bend rigidity based on
 * persistent ideal chains. It then uses stochastic approximation to determine
 * a bend rigidity for each unique pair of monomer lengths. The tuner makes
 * use of the following parameters with optional ones in brackets []:
 *
 *  --ebr_tuner_decorr_bp <+float> : Real-valued scalar number of base pairs
 *      defining the chromatin "decorrelation" length (e.g. Kuhn length or
 *      persistence length). This value defines the number of base pairs such
 *      that the expected mean cosine is the target value as defined by
 *      uOpt_ebr_tuner_decorr_cos (typically decorr_cos is 0.0).
 *  --ebr_tuner_out_fpath <path> : Path to write tuned bend rigidities in run-
 *      length encoded format. This output file may then serve as a simulation
 *      input by setting uOpt_energy_bend_rigidity_fpath to this file path.
 *  --ebr_tuner_node_bp_fpath <path> : Path to run-length encoded file with
 *      number of base pairs represented by each monomer node
 *  [--ebr_tuner_max_iters <+integer>] : Maximum number of iterations to run
 *      stochastic approximation (default = 1000)
 *  [--ebr_tuner_step_decay_rate <+float>] : Positive step-size decay rate
 *      (default=3) for stochastic approximation. If Polyak-Ruppert averaging,
 *      must be in range (0,1) exclusive (default in this case is 0.9).
 *  [--ebr_tuner_step_scale <+float> : Positive step-size scale (default=1)
 *      factor used for stochastic approximation
 *  [--ebr_tuner_should_avg <0|1> : '1' if Polyak-Ruppert averaging and '0'
 *      otherwise (default)
 *  [--ebr_tuner_use_weighted_interp <0|1>: If '1' (default) weighted
 *      interpolation of the <cos> at the target genomic decorrelation length is
 *      used, else if '0', unweighted interpolation is used.
 *  [--ebr_tuner_decorr_cos <float in [-1 to 1]>] : Target mean cosine value
 *      at the specified decorrelation length. Tuner attempts to find a
 *      bending rigidity constant such that the mean cosine at the
 *      decorrelation length (in base pairs) matches this value. MUST BE IN
 *      [-1 to 1] and default is 0.0 (implying ebr_tuner_decorr_bp is a
 *      persistence length)
 *  [--ebr_tuner_rigid_min <float>]: Restricts search domain of bending
 *      rigidity constant such that proposed values must be >= this value
 *  [--ebr_tuner_rigid_max <float>]: Restricts search domain of bending
 *      rigidity constant such that proposed values must be <= this value
 *  [--ebr_tuner_conv_thresh <+float>] : If successive values of the stochastic
 *      approximation root finding algorithm are within this tolerance, then
 *      tuner considers itself converged, default = 0.01
 *  [--ebr_tuner_conv_iters <+integer>] : This many successive iterations must
 *      be within convergence tolerance for tuner to be considered converged,
 *      default is 10
 *  [--ebr_tuner_bond_iter <+integer>] : For tuning multiple bond types, this
 *      value defines how many iterations to tune a single bond type before
 *      switching to the next most frequent bond type, default is 1
 *  [--ebr_tuner_log_interval <+integer>]: Tuner status is reported on
 *      multiples of the log interval
 *  [--energy_bend_rigidity_fpath <path>] : Input path to run-length encoded
 *      starting bend rigidities, this may be used to resume the tuner from
 *      a previous run and/or bypass the default ideal chain initialization
 *  [-conf <path>] : Path to INI configuration file
 */

/**
 * References:
 *
 * For review of the stochastic root finding problem:

 *  Pasupathy, Raghu, and Sujin Kim. "The stochastic root-finding problem:
 *  Overview, solutions, and open questions." ACM Transactions on Modeling and
 *  Computer Simulation (TOMACS) 21, no. 3 (2011): 19.
 *
 * For analytic equation for bending rigidity of an ideal persistent chain,
 * (used as initial guess) see equation 4 of:
 *
 *  Muralidhar, Abhiram, and Kevin D. Dorfman. "Kirkwood diffusivity of long
 *  semiflexible chains in nanochannel confinement." Macromolecules 48, no. 8
 *  (2015): 2829-2839.
 *
 * as well as pg. 228, equation 6.4.8 of 2nd edition text:
 *
 *  Hiemenz, Paul C., and Timothy P. Lodge. Polymer chemistry. CRC press, 2007.
 *
 * For experimentally measured Kuhn length (~1 KB) in human chromatin, see:
 *
 *  Sanborn, Adrian L., Suhas SP Rao, Su-Chen Huang, Neva C. Durand, Miriam H.
 *  Huntley, Andrew I. Jewett, Ivan D. Bochkov et al. "Chromatin extrusion
 *  explains key features of loop and domain formation in wild-type and
 *  engineered genomes." Proceedings of the National Academy of Sciences 112,
 *  no. 47 (2015): E6456-E6465.
 */

/**
 * An alternative and possibly complementary approach for parameter tuning:
 *
 *  Norgaard, Anders B., Jesper Ferkinghoff-Borg, and Kresten Lindorff-Larsen.
 *  "Experimental parameterization of an energy function for the simulation of
 *  unfolded proteins." Biophysical journal 94, no. 1 (2008): 182-192.
 */

#ifndef uSisSeoBendEnergyRigidityTuner_h
#define uSisSeoBendEnergyRigidityTuner_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"

#if defined(U_BUILD_ENABLE_COMMANDLETS) || defined(U_BUILD_ENABLE_TESTS)

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uCsppline.h"
#include "uExitCodes.h"
#include "uHashMap.h"
#include "uIsFinite.h"
#include "uParserRle.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

#include <boost/math/constants/constants.hpp>
#include <boost/math/tools/roots.hpp>
#include <fstream>
#include <limits>
#include <map>
#include <math.h>
#include <set>
#include <vector>

//****************************************************************************
// uSisSeoBendEnergyRigidityTuner
//****************************************************************************

/**
 * Uses Robins-Munro stochastic approximation with optional Polyak-Ruppert
 * averaging to match bending rigidities to target Kuhn length.
 *
 * See https://en.wikipedia.org/wiki/Stochastic_approximation
 */
template <typename t_SisGlue>
class uSisSeoBendEnergyRigidityTuner {
private:
    /**
     * Bond-length to set of node identifiers tuple
     */
    typedef std::pair<uReal, std::vector<uUInt> > b2ntup_t;

    /**
     * Utility for maintaining a simple moving average of the last 'n' elements
     */
    class MovAvg {
    public:
        /**
         * Constructor
         * @param size - window size, average is computed over these elements
         */
        explicit MovAvg(const uMatSz_t size)
            : m_mean(U_TO_REAL(0.0)),
              m_inv_n(U_TO_REAL(1.0) / U_TO_REAL(size)),
              m_ix(0) {
            uAssert(size > 0);
            m_window.zeros(size);
        }
        /**
         * @return window average
         */
        uReal mean() const { return m_mean; }
        /**
         * @param value - update moving average with this value, least recent
         * value is replaced
         */
        void operator()(const uReal value) {
            uAssertBounds(m_ix, 0, m_window.n_elem);
            const uReal prev = m_window.at(m_ix);
            const uReal next = value * m_inv_n;
            m_mean += (next - prev);
            m_window.at(m_ix) = next;
            m_ix = (m_ix + 1) % m_window.n_elem;
        }

    private:
        uReal m_mean;
        uReal m_inv_n;
        uVecCol m_window;
        uMatSz_t m_ix;
    };

public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Default constructor
     */
    uSisSeoBendEnergyRigidityTuner() { this->clear(); }

    /**
     * Tune bend rigidities from command line options
     */
    int tune(const uCmdOptsMap& cmd_opts) {
        // Create configuration
        uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
        // Parse command line and any INI configuration (override defaults)
        config->init(cmd_opts);
        this->preamble(config);
        this->stoch_approx();
        this->postamble();
        return uExitCode_normal;
    }

private:
    /**
     * Reset internal state
     */
    void clear() {
        m_sim.clear();
        m_b2nmap.clear();
        m_n2bmap.clear();
        m_node_bp.clear();
        m_step_decay_rate = U_TO_REAL(0.0);
        m_step_scale = U_TO_REAL(0.0);
        m_step_smooth_lambda = U_TO_REAL(0.0);
        m_decorr_bp = U_TO_REAL(0.0);
        m_decorr_cos = U_TO_REAL(0.0);
        m_rigid_min = U_TO_REAL(0.0);
        m_rigid_max = U_TO_REAL(0.0);
        m_conv_thresh = U_TO_REAL(0.0);
        m_max_iters = U_TO_UINT(0);
        m_conv_iters = U_TO_UINT(0);
        m_bond_iters = U_TO_UINT(0);
        m_step_smooth_count = U_TO_UINT(0);
        m_step_smooth_start_iter = U_TO_UINT(0);
        m_log_interval = U_TO_UINT(0);
        m_should_avg = uFALSE;
        m_use_weighted_interp = uFALSE;
        m_out_fpath.clear();
    }

    /**
     * Perform stochastic approximation root finding using Robbins-Monro
     * algorithm with optional Polyak-Ruppert averaging
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     */
    void stoch_approx() {
        // Pre-conditions
        uAssert(m_max_iters > 0);
        uAssert(m_step_decay_rate > U_TO_REAL(0.0));
        uAssert(m_step_scale > U_TO_REAL(0.0));
        uAssert(!m_b2nmap.empty());
        uAssert(m_conv_thresh > U_TO_REAL(0.0));
        uAssert(m_conv_iters > U_TO_UINT(0));
        uAssert(m_bond_iters > U_TO_UINT(0));
        uAssertBoundsInc(m_step_smooth_lambda, U_TO_REAL(0.0), U_TO_REAL(1.0));
        uAssert(m_step_smooth_count > 0);
        // Keep track of average decorrelation length
        MovAvg decorr_mov_avg(m_step_smooth_count);
        uMatrixUtils::running_stat<uReal> decorr_run_avg;
        const size_t num_bond_types = m_b2nmap.size();
        uAssert(num_bond_types > 0);
        // Allocate mean rigidities and base counters
        uVecCol rigid_means(U_TO_MAT_SZ_T(num_bond_types));
        uVecCol bases(U_TO_MAT_SZ_T(num_bond_types));
        // Initialize mean rigidities
        for (size_t ibtype = 0; ibtype < num_bond_types; ++ibtype) {
            uAssertBounds(ibtype, 0, rigid_means.n_elem);
            const std::vector<uUInt>& nodes = m_b2nmap[ibtype].second;
            uAssert(!nodes.empty());
            uAssertBounds(
                nodes.front(), 0, m_sim.get_energy_bend_rigidities().size());
            const uReal rigid =
                m_sim.get_energy_bend_rigidities()[nodes.front()];
            rigid_means.at(U_TO_MAT_SZ_T(ibtype)) = rigid;
        }
        // Initialize base counters
        bases.ones();
        // Perform blocked Robbins-Monro
        uUInt conv_iters = 0;
        for (uUInt iter = 0; iter < m_max_iters; ++iter) {
            // Determine smoothing lambda
            const uReal smooth_lambda = (iter == m_step_smooth_start_iter)
                                            ? m_step_smooth_lambda
                                            : U_TO_REAL(0.0);
            // Iterate over bond types
            uUInt conv_count = 0;
            for (size_t ibtype = 0; ibtype < num_bond_types; ++ibtype) {
                uAssertBounds(ibtype, 0, m_b2nmap.size());
                const uReal bond_bp = m_b2nmap[ibtype].first;
                uAssert(bond_bp > U_TO_REAL(0.0));
                const std::vector<uUInt>& nodes = m_b2nmap[ibtype].second;
                uAssert(!nodes.empty());
                uAssertBounds(nodes.front(),
                              0,
                              m_sim.get_energy_bend_rigidities().size());
                uReal rigid = m_sim.get_energy_bend_rigidities()[nodes.front()];
                uAssertBoundsInc(rigid, m_rigid_min, m_rigid_max);
                uAssertBounds(ibtype, 0, bases.n_elem);
                uReal& base = bases.at(U_TO_MAT_SZ_T(ibtype));
                uAssert(base >= U_TO_REAL(0.0));
                uAssertBounds(ibtype, 0, rigid_means.n_elem);
                uReal& rigid_mean = rigid_means.at(U_TO_MAT_SZ_T(ibtype));
                uAssertBoundsInc(rigid_mean, m_rigid_min, m_rigid_max);
                for (uUInt iblock = 0; iblock < m_bond_iters; ++iblock) {
                    // Light-weight reinitialize simulation
                    m_sim.init(U_MAIN_THREAD_ID_0_ARG);
                    // Run simulation
                    uVerify(m_sim.run());
                    // Interpolate new decorrelation length
                    const uReal new_decorr_bp = this->interp_decorr();
                    uAssert(uIsFinite(new_decorr_bp));
                    // Update decorrelation stats
                    decorr_mov_avg(new_decorr_bp);
                    decorr_run_avg(new_decorr_bp);
                    // Calculate new step
                    const uReal delta_raw = new_decorr_bp - m_decorr_bp;
                    const uReal delta_smooth =
                        decorr_mov_avg.mean() - m_decorr_bp;
                    // Use smoothing to help dampen oscillations in step size
                    const uReal delta =
                        ((U_TO_REAL(1.0) - smooth_lambda) * delta_raw) +
                        (smooth_lambda * delta_smooth);
                    uAssert(uIsFinite(delta));
                    const uReal weight =
                        m_step_scale * pow(base, -m_step_decay_rate);
                    uAssert(uIsFinite(weight));
                    const uReal step = weight * delta;
                    uAssert(uIsFinite(step));
                    uReal new_rigid = rigid - step;
                    uAssert(uIsFinite(new_rigid));
                    // Constrain search domain
                    new_rigid =
                        std::max(std::min(new_rigid, m_rigid_max), m_rigid_min);
                    // Report tuner status
                    if (iter % m_log_interval == 0) {
                        uLogf(
                            "EBR TUNER (%d) : bond (bp) = %f, decorr (bp) = %f "
                            "<%f, %f>\n\trigidity = %f <%f> -> %f, weight = "
                            "%f\n",
                            (int)iter,
                            (double)bond_bp,
                            (double)new_decorr_bp,
                            (double)decorr_mov_avg.mean(),
                            (double)decorr_run_avg.mean(),
                            (double)rigid,
                            (double)rigid_mean,
                            (double)new_rigid,
                            (double)weight);
                    }
                    // Cumulative moving average
                    // https://en.wikipedia.org/wiki/Moving_average
                    rigid_mean = (new_rigid + (base * rigid_mean)) /
                                 (base + U_TO_REAL(1.0));
                    uAssert(uIsFinite(rigid_mean));
                    // Apply new rigidity
                    this->assign_rigidity(new_rigid, nodes);
                    conv_count += (fabs(new_rigid - rigid) <= m_conv_thresh);
                    // Update weight counter
                    base += U_TO_REAL(1.0);
                    // Update current parameter
                    rigid = new_rigid;
                }  // end loop over bond block
            }      // end loop over all bonds

            // Check convergence
            uAssertBoundsInc(conv_count, 0, num_bond_types * m_bond_iters);
            if (conv_count == (num_bond_types * m_bond_iters)) {
                // All bonds converged, update streak!
                ++conv_iters;
                if (conv_iters >= m_conv_iters) {
                    // WE CONVERGED!
                    // Check if we should perform Polyak-Ruppert averaging
                    if (m_should_avg) {
                        for (size_t ibtype = 0; ibtype < num_bond_types;
                             ++ibtype) {
                            uAssertBounds(ibtype, 0, m_b2nmap.size());
                            const std::vector<uUInt>& nodes =
                                m_b2nmap[ibtype].second;
                            uAssert(!nodes.empty());
                            uAssertBounds(ibtype, 0, rigid_means.n_elem);
                            const uReal rigid_mean =
                                rigid_means.at(U_TO_MAT_SZ_T(ibtype));
                            uAssertBoundsInc(
                                rigid_mean, m_rigid_min, m_rigid_max);
                            this->assign_rigidity(rigid_mean, nodes);
                        }
                    }
                    break;
                }
            } else {
                // At least one bond failed, reset streak
                conv_iters = 0;
            }
        }  // end blocked Robbins-Monro
    }

    /**
     * Perform start-up work
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     */
    void preamble(uSpConstConfig_t config) {
        // Announce tuner is running
        uLogf("Running EBR tuner.\n");
        // Output initial user configuration
        config->print();
        // Initialize globals
        uG::init(config);
        // Report number of worker threads
        U_LOG_NUM_WORKER_THREADS;
        // Feed configuration to simulation
        m_sim.init(config U_MAIN_THREAD_ID_ARG);
        // Initialize tuner from config
        this->init_core(config);
        this->init_node_bp(config);
        this->init_decorr(config);
        this->init_conv(config);
        this->init_log_interval(config);
        this->init_out_fpath(config);
        this->init_bond_maps(config);
        this->init_rigid(config);
        // Output tuner configuration
        this->print_tuner_config();
    }

    /**
     * Export results and tear-down tuner
     */
    void postamble() {
        // Export tuned rigidities
        this->export_rigid();
        // Optional: export last simulation samples
        uSisUtils::export_sim(m_sim);
        // Destroy globals
        uG::teardown();
        // Clear tuner state
        this->clear();
    }

    /**
     * Initialize path to write tuned rigidities
     */
    void init_out_fpath(uSpConstConfig_t config) {
        if (!config->resolve_path(m_out_fpath, uOpt_ebr_tuner_out_fpath)) {
            uLogf("Error: ebr tuner output path not specified. Exiting.\n");
            exit(uExitCode_error);
        }
    }

    /**
     * Initializes stochastic approximation "core" parameters. Namely, maximum
     * number of iterations, step decay rate, step scale, and if Polyak-Ruppert
     * averaging should be used.
     */
    void init_core(uSpConstConfig_t config) {
        // Initialize maximum iterations for stochastic approximation
        const uUInt DEFAULT_MAX_ITERS = 1000;
        m_max_iters = DEFAULT_MAX_ITERS;
        config->read_into(m_max_iters, uOpt_ebr_tuner_max_iters);
        if (m_max_iters <= 0) {
            uLogf(
                "Warning: ebr tuner max iterations must be > 0. Setting to "
                "default.\n");
            m_max_iters = DEFAULT_MAX_ITERS;
        }

        // Initialize if Polyak-Ruppert averaging should be used
        m_should_avg = uFALSE;
        config->read_into(m_should_avg, uOpt_ebr_tuner_should_avg);
        m_should_avg = (m_should_avg == uTRUE);

        // Initialize step size decay rate
        const uReal DEFAULT_STEP_DECAY_RATE =
            m_should_avg ? U_TO_REAL(0.9) : U_TO_REAL(3.0);
        m_step_decay_rate = DEFAULT_STEP_DECAY_RATE;
        config->read_into(m_step_decay_rate, uOpt_ebr_tuner_step_decay_rate);
        if ((m_step_decay_rate <= U_TO_REAL(0.0)) ||
            (m_should_avg && (m_step_decay_rate >= U_TO_REAL(1.0)))) {
            uLogf(
                "Warning: ebr tuner step decay rate must be in (0, +inf) or "
                "(0,1) if Polyak-Ruppert averaging. Setting to default.\n");
            m_step_decay_rate = DEFAULT_STEP_DECAY_RATE;
        }

        // Initialize step size scale
        const uReal DEFAULT_STEP_SCALE = U_TO_REAL(1.0);
        m_step_scale = DEFAULT_STEP_SCALE;
        config->read_into(m_step_scale, uOpt_ebr_tuner_step_scale);
        if (m_step_scale <= U_TO_REAL(0.0)) {
            uLogf(
                "Warning: ebr tuner step scale must be > 0. Setting to "
                "default.\n");
            m_step_scale = DEFAULT_STEP_SCALE;
        }

        // Initialize step smooth lambda
        const uReal DEFAULT_STEP_SMOOTH_LAMDA = U_TO_REAL(0.0);
        m_step_smooth_lambda = DEFAULT_STEP_SMOOTH_LAMDA;
        config->read_into(m_step_smooth_lambda,
                          uOpt_ebr_tuner_step_smooth_lambda);
        if ((m_step_smooth_lambda < U_TO_REAL(0.0)) ||
            (m_step_smooth_lambda > U_TO_REAL(1.0))) {
            uLogf(
                "Warning: ebr tuner step smooth lambda must be in range [0,1]. "
                "Setting to default.\n");
            m_step_smooth_lambda = DEFAULT_STEP_SMOOTH_LAMDA;
        }

        // Initialize step smooth count
        const uUInt DEFAULT_STEP_SMOOTH_COUNT = U_TO_UINT(10);
        m_step_smooth_count = DEFAULT_STEP_SMOOTH_COUNT;
        config->read_into(m_step_smooth_count,
                          uOpt_ebr_tuner_step_smooth_count);
        if (m_step_smooth_count < 1) {
            uLogf(
                "Warning: ebr tuner step smooth count must be > 1. Setting to "
                "default.\n");
            m_step_smooth_count = DEFAULT_STEP_SMOOTH_COUNT;
        }

        // Initialize step smooth start iteration
        const uUInt DEFAULT_STEP_SMOOTH_START_ITER = U_TO_UINT(10);
        m_step_smooth_start_iter = DEFAULT_STEP_SMOOTH_START_ITER;
        config->read_into(m_step_smooth_start_iter,
                          uOpt_ebr_tuner_step_smooth_start_iter);
    }

    /**
     * Initialize base pair length of each monomer node. Order is assumed to
     * be same as sim_t::get_max_num_nodes_at_locus()
     */
    void init_node_bp(uSpConstConfig_t config) {
        // Stores complete file path to node base pair specification file
        std::string node_bp_fpath;

        // Determine parameter node base pair specification file
        if (!config->resolve_path(node_bp_fpath,
                                  uOpt_ebr_tuner_node_bp_fpath)) {
            uLogf("Error: ebr tuner node base pair file not found.\n");
            exit(uExitCode_error);
        }

        // Parse node base pair lengths
        if (!uParserRle::read<uReal, uVecCol>(m_node_bp, node_bp_fpath)) {
            uLogf("Error: ebr tuner unable to parse %s.\n",
                  node_bp_fpath.c_str());
            exit(uExitCode_error);
        }

        // Check read expected count
        if (U_TO_UINT(m_node_bp.n_elem) != m_sim.get_max_total_num_nodes()) {
            uLogf(
                "Error: ebr tuner expected %d node base pair lengths but "
                "parsed %d. Exiting.\n",
                ((int)m_sim.get_max_total_num_nodes()),
                ((int)m_node_bp.n_elem));
            exit(uExitCode_error);
        }

        // Check all base pair lengths are positive
        for (uMatSz_t i = 0; i < m_node_bp.n_elem; ++i) {
            const uReal node_bp = m_node_bp.at(i);
            if (node_bp <= U_TO_REAL(0.0)) {
                uLogf(
                    "Error: node %d (zero-based) has base pair length %f < 0. "
                    "Exiting.\n",
                    ((int)i),
                    ((double)node_bp));
                exit(uExitCode_error);
            }
        }
    }

    /**
     * Initialize decorrelation length and cosine from user configuration
     */
    void init_decorr(uSpConstConfig_t config) {
        // Initialize decorrelation length
        m_decorr_bp = U_TO_REAL(-1.0);
        config->read_into(m_decorr_bp, uOpt_ebr_tuner_decorr_bp);
        if (m_decorr_bp <= U_TO_REAL(0.0)) {
            uLogf(
                "Error: ebr tuner decorrelation length (base pairs) must be "
                "positive. Exiting.\n");
            exit(uExitCode_error);
        }
        // Initialize target mean cosine at decorrelation length
        m_decorr_cos = U_TO_REAL(0.0);
        config->read_into(m_decorr_cos, uOpt_ebr_tuner_decorr_cos);
        if ((m_decorr_cos < U_TO_REAL(-1.0)) ||
            (m_decorr_cos > U_TO_REAL(1.0))) {
            uLogf(
                "Error: ebr tuner decorrelation mean cosine %f not in [-1 "
                "to 1]. Exiting\n",
                (double)m_decorr_cos);
            exit(uExitCode_error);
        }
        // Initialize if weighted interpolation should be used
        m_use_weighted_interp = uTRUE;
        config->read_into(m_use_weighted_interp,
                          uOpt_ebr_tuner_use_weighted_interp);
        m_use_weighted_interp = (m_use_weighted_interp == uTRUE);
        // Post-conditions
        uAssert(m_decorr_bp > U_TO_REAL(0.0));
        uAssertBoundsInc(m_decorr_cos, U_TO_REAL(-1.0), U_TO_REAL(1.0));
    }

    /**
     * Initialize convergence criteria
     */
    void init_conv(uSpConstConfig_t config) {
        const uReal DEFAULT_CONV_THRESH = U_TO_REAL(0.1);
        m_conv_thresh = DEFAULT_CONV_THRESH;
        config->read_into(m_conv_thresh, uOpt_ebr_tuner_conv_thresh);
        if (m_conv_thresh <= U_TO_REAL(0.0)) {
            uLogf(
                "Warning: ebr tuner convergence threshold must be positive. "
                "Setting to default.\n");
            m_conv_thresh = DEFAULT_CONV_THRESH;
        }
        const uUInt DEFAULT_CONV_ITERS = 10;
        m_conv_iters = DEFAULT_CONV_ITERS;
        config->read_into(m_conv_iters, uOpt_ebr_tuner_conv_iters);
        if (m_conv_iters < 1) {
            uLogf(
                "Warning: ebr tuner convergence iterations must be positive. "
                "Setting to default.\n");
            m_conv_iters = DEFAULT_CONV_ITERS;
        }
    }

    /**
     * Initialize logging interval from user configuration
     */
    void init_log_interval(uSpConstConfig_t config) {
        m_log_interval = U_TO_UINT(10);
        config->read_into(m_log_interval, uOpt_ebr_tuner_log_interval);
        if (m_log_interval < U_TO_UINT(1)) {
            uLogf("Warning: ebr tuner log interval < 1. Setting to 1.\n");
            m_log_interval = U_TO_UINT(1);
        }
    }

    /**
     * Initialize bond length mappings
     */
    void init_bond_maps(uSpConstConfig_t config) {
        // Transient ordered map used for sorting bond lengths
        typedef std::map<b2ntup_t::first_type, b2ntup_t::second_type>
            stdb2nmap_t;
        stdb2nmap_t bond2nodes;

        // Mapping from locus id -> number of nodes at locus
        const std::vector<uUInt>& num_nodes_at_locus =
            m_sim.get_max_num_nodes_at_locus();
        const uUInt num_loci = U_TO_UINT(num_nodes_at_locus.size());
        uAssert(num_loci > 0);

        // Allocate mapping from node i to bond length(i-1, i) in bp
        uAssert(m_sim.get_max_total_num_nodes() > 1);
        m_n2bmap.zeros(m_sim.get_max_total_num_nodes());

        // Traverse loci
        uMatSz_t i = 0;
        for (uUInt l = 0; l < num_loci; ++l) {
            const uMatSz_t num_nodes = U_TO_MAT_SZ_T(num_nodes_at_locus[l]);
            uAssert(num_nodes > 0);
            uMatSz_t n = 1;
            for (; (n + 1) < num_nodes; ++n) {
                const uMatSz_t j = i + n;
                uAssertBounds(j + 1, 2, m_node_bp.n_elem);
                // Compute bond length in base pairs between nodes j-1, j
                const uReal dup_bond_len_bp =
                    U_TO_REAL(0.5) * (m_node_bp.at(j) + m_node_bp.at(j - 1));
                uAssertBounds(j, 1, m_n2bmap.n_elem);
                m_n2bmap.at(j) = dup_bond_len_bp;
                // Compute bond length in base pairs between nodes j-1, j, j+1
                const uReal tri_bond_len_bp =
                    (U_TO_REAL(0.5) *
                     (m_node_bp.at(j - 1) + m_node_bp.at(j + 1))) +
                    m_node_bp.at(j);
                uAssert(tri_bond_len_bp > U_TO_REAL(0.0));
                bond2nodes[tri_bond_len_bp].push_back(U_TO_UINT(j));
            }
            uAssert((n + 1) == num_nodes);
            // Account for last node
            if (num_nodes > 2) {
                const uMatSz_t j = i + n;
                uAssertBounds(j, 2, m_node_bp.n_elem);
                // Compute bond length in base pairs between nodes j-1, j
                const uReal dup_bond_len_bp =
                    U_TO_REAL(0.5) * (m_node_bp.at(j) + m_node_bp.at(j - 1));
                uAssertBounds(j, 2, m_n2bmap.n_elem);
                m_n2bmap.at(j) = dup_bond_len_bp;
            }
            i += num_nodes;
        }
        uAssertPosEq(i, m_node_bp.n_elem);

        // Data structure for sorting by:
        //  1.) most frequent bond length
        //  2.) magnitude of bond length
        std::map<size_t, std::set<uReal> > bondcount2lens;
        for (stdb2nmap_t::iterator it = bond2nodes.begin();
             it != bond2nodes.end();
             ++it) {
            bondcount2lens[it->second.size()].insert(it->first);
        }

        // Store sorted bond length mapping
        for (std::map<size_t, std::set<uReal> >::reverse_iterator ritbc2l =
                 bondcount2lens.rbegin();
             ritbc2l != bondcount2lens.rend();
             ++ritbc2l) {
            const std::set<uReal>& bond_lens = ritbc2l->second;
            for (std::set<uReal>::reverse_iterator ritbl = bond_lens.rbegin();
                 ritbl != bond_lens.rend();
                 ++ritbl) {
                stdb2nmap_t::iterator itb2n = bond2nodes.find(*ritbl);
                uAssert(itb2n != bond2nodes.end());
                m_b2nmap.push_back(*itb2n);
            }
        }

        // Init bond iterations
        m_bond_iters = U_TO_UINT(1);
        config->read_into(m_bond_iters, uOpt_ebr_tuner_bond_iters);
        if (m_bond_iters < 1) {
            uLogf(
                "Warning: ebr tuner must have bond iterations > 0. Setting to "
                "default.\n");
            m_bond_iters = U_TO_UINT(1);
        }
    }

    // Functor: f(x) = Pade approximant - cos(pi * l_b / l_k)
    class CosPadeFunc {
    public:
        inline uReal operator()(const uReal x) const {
            const uReal x2 = x * x;
            const uReal pade =
                (x * ((x2 + U_TO_REAL(189.0)) * x2 + U_TO_REAL(3465.0))) /
                ((U_TO_REAL(21.0) * x2 + U_TO_REAL(1260.0)) * x2 +
                 U_TO_REAL(10395.0));
            return pade - m_target_cos;
        }
        uReal m_target_cos;
    };

    /**
     * Initializes bend rigidities to ideal, persistent chain values based on
     * Eq. 4 of:
     *
     *  Muralidhar, Abhiram, and Kevin D. Dorfman. "Kirkwood diffusivity of
     *  long semiflexible chains in nanochannel confinement." Macromolecules
     *  48, no. 8 (2015): 2829-2839.
     *
     * However, if uOpt_energy_bend_rigidity_fpath exists on command line,
     * then assume we are resuming a previous run and/or user has provided
     * a better initialization, therefore we skip initialization in this case.
     */
    void init_rigid(uSpConstConfig_t config) {
        // Pre-conditions
        uAssert(m_decorr_bp > U_TO_REAL(0.0));
        uAssertBoundsInc(m_decorr_cos, U_TO_REAL(-1.0), U_TO_REAL(1.0));

        // Initialize search domain
        m_rigid_min = U_TO_REAL(-20.0);
        m_rigid_max = U_TO_REAL(20.0);
        config->read_into(m_rigid_min, uOpt_ebr_tuner_rigid_min);
        config->read_into(m_rigid_max, uOpt_ebr_tuner_rigid_max);
        if (m_rigid_min >= m_rigid_max) {
            uLogf("Error: ebr tuner min rigidity >= max rigidity. Exiting.\n");
            exit(uExitCode_error);
        }

        // Skip initialization if rigidity file exists
        std::string rigidity_fpath;
        if (config->resolve_path(rigidity_fpath,
                                 uOpt_energy_bend_rigidity_fpath)) {
            uLogf(
                "ebr tuner rigidity initialization bypassed in favor of values "
                "specified by: %s\n",
                rigidity_fpath.c_str());
            return;
        }

        // Else, approximate based on ideal, persistent chain. For analytic
        // equation for bending rigidity of an ideal persistent chain, (used
        // as initial guess) see equation 4 of:
        //
        //  Muralidhar, Abhiram, and Kevin D.Dorfman. "Kirkwood diffusivity of
        //      long semiflexible chains in nanochannel confinement."
        //      Macromolecules 48, no. 8 (2015) : 2829 - 2839.
        //
        // as well as pg. 228, equation 6.4.8 of 2nd edition text :
        //
        //  Hiemenz, Paul C., and Timothy P.Lodge. Polymer chemistry. CRC
        //      press, 2007.

        // First, we assume a periodic behavior such that the mean cosine
        // for bond length l_b can be approximated according to the following:
        //
        //  <cos> = cos(acos(decorr_cos) * l_b / l_decorr)
        //      where l_b is bond length and l_decorr decorrelation length
        //
        // (assumption being that the expected cosine is periodic, but note
        // also that variance likely increases as function of bond length)
        //
        // Second, according to Muralidhar & Dorfman, the analytical mean
        // cosine is given by the following expression:
        //
        //  <cos> = coth(x) - 1/x where x is bending rigidity
        //
        // Third, for better numerical stability, we use the Pade approximant
        //
        //  <cos> = (x ((x^2 + 189) x^2 + 3465))/((21 x^2 + 1260) x^2 + 10395)
        //
        // Which appears to give much better RMSD to the analytical expression
        // than a Taylor series about 0.
        //
        // Hence, our objective is to guess an initial bending rigidity by
        // finding the root of the following function:
        //
        //  f(x) = Pade approximant - cos(pi * l_b / l_k)
        //       = (x ((x^2 + 189) x^2 + 3465))/((21 x^2 + 1260) x^2 + 10395)
        //          - cos(pi * l_b / l_k)

        // Process each bond length
        CosPadeFunc f;
        std::pair<uReal, uReal> bracket;
        const size_t num_bonds = m_b2nmap.size();
        const int digits = std::min(std::numeric_limits<uReal>::digits - 3, 0);
        boost::math::tools::eps_tolerance<uReal> tol(U_TO_UINT(digits));
        const boost::uintmax_t& max_iter = 1000;
        boost::uintmax_t it;
        const uReal theta = acos(m_decorr_cos);
        uAssertBoundsInc(
            theta, U_TO_REAL(0.0), boost::math::constants::pi<uReal>());
        for (size_t i = 0; i < num_bonds; ++i) {
            const b2ntup_t& b2n = m_b2nmap[i];
            const uReal bond_len_bp = b2n.first;
            uAssert(bond_len_bp >= U_TO_REAL(0.0));
            f.m_target_cos = cos(theta * bond_len_bp / m_decorr_bp);
            if (U_REAL_CMP_EQ(f.m_target_cos, U_TO_REAL(0.0))) {
                // Special case for cos == 0, bracket_and_solve() expects a
                // signed guess. Therefore, handle this case explicitly
                bracket.first = U_TO_REAL(0.0);
                bracket.second = U_TO_REAL(0.0);
            } else {
                // Use TOMS748 root-finder
                it = max_iter;
                bracket = boost::math::tools::bracket_and_solve_root(
                    f,
                    f.m_target_cos /*guess*/,
                    U_TO_REAL(2.0) /*factor*/,
                    true /*is_rising*/,
                    tol,
                    it);
            }
            // Use average value of returned bracket
            const uReal ideal_rigidity =
                U_TO_REAL(0.5) * (bracket.first + bracket.second);
            // Constrain to user defined region
            const uReal bounded_rigidity =
                std::max(std::min(ideal_rigidity, m_rigid_max), m_rigid_min);
            // Assign rigidity to target nodes
            this->assign_rigidity(bounded_rigidity, b2n.second);
        }
    }

    /**
     * Assigns bend rigidity to target nodes
     */
    void assign_rigidity(const uReal rigidity,
                         const std::vector<uUInt>& nodes) {
        const size_t num_nodes = nodes.size();
        uAssert(num_nodes > 0);
        std::vector<uReal>& rigidities = m_sim.get_energy_bend_rigidities();
        for (size_t j = 0; j < num_nodes; ++j) {
            const uUInt node_id = nodes[j];
            uAssertBounds(node_id, 0, rigidities.size());
            rigidities[node_id] = rigidity;
        }
    }

    /**
     * Exports final set of rigidities
     */
    void export_rigid() const {
        // Open file stream
        std::ofstream fout(m_out_fpath.c_str(), std::ofstream::out);
        if (!fout) {
            uLogf("Error: ebr tuner unable to write to file %s. Exiting.\n",
                  m_out_fpath.c_str());
            exit(uExitCode_error);
        }
        const uUInt num_nodes = m_sim.get_max_total_num_nodes();
        uUInt i = 0;
        while (i < num_nodes) {
            const uReal rigid_i = m_sim.get_energy_bend_rigidity(i);
            // Determine run
            uUInt j = i + 1;
            while ((j < num_nodes) &&
                   (m_sim.get_energy_bend_rigidity(j) == rigid_i)) {
                ++j;
            }
            const uUInt run = j - i;
            uAssert(run >= 1);
            uAssert((i + run) <= num_nodes);
            uAssert(rigid_i == m_sim.get_energy_bend_rigidity(i + run - 1));
            uAssert((i + run) == num_nodes ||
                    (m_sim.get_energy_bend_rigidity(i + run) != rigid_i));
            // Write RLE tuple
            fout << run << "," << rigid_i << std::endl;
            i += run;
        }
        uAssert(i = num_nodes);
        fout.close();
    }

    // Structure for storing bias-corrected stats
    typedef struct {
        uReal mean;
        uReal var;
        uReal count;
    } bcstat_t;

    /**
     * Uses simulation samples to interpolate a new decorrelation length (bp)
     */
    uReal interp_decorr() const {
        // Use spline to interpolate new decorrelation length which is
        // defined as smallest genomic length (bp) such that <cos> = target

        // Obtain loci sizes
        const std::vector<uUInt>& num_nodes_at_locus =
            m_sim.get_max_num_nodes_at_locus();
        const size_t num_loci = num_nodes_at_locus.size();
        uAssert(num_loci > 0);

        // Obtain completed simulation samples
        const std::vector<sample_t>& samples = m_sim.get_completed_samples();
        const size_t num_samples = samples.size();
        uAssert(num_samples > 0);

        // Map from bond length in base pairs to running <cos> and <variance>
        typedef U_HASH_MAP<uReal, uMatrixUtils::running_stat<uReal> >
            b2rstatmap_t;

        // Map from bond length in base pairs to bias-corrected <cos>,
        // <variance>, and bond length multiplicities
        typedef U_HASH_MAP<uReal, bcstat_t> b2bcstatmap_t;
        typedef typename b2bcstatmap_t::iterator b2bcstatiter_t;

        // Matrix of normalized bond directions, not computed for 0th node
        uMatrix bond_dirs;

        // Compute normalized sample weights
        const uVecCol& log_weights = m_sim.get_completed_log_weights_view();
        uAssert(log_weights.n_elem == num_samples);
        uVecCol weights(U_TO_MAT_SZ_T(num_samples));
        uReal max_log_w;
        uSisUtilsQc::calc_norm_exp_weights(weights, max_log_w, log_weights);
        // Normalize by weighted sum to eliminate any unknown partition
        // constants
        const uReal Z = uMatrixUtils::sum(weights);
        uAssert(Z > U_TO_REAL(0.0));
        weights /= Z;

        // Final set of bias-corrected stats
        b2bcstatmap_t b2bcstat;

        // Process each sample
        for (size_t isamp = 0; isamp < num_samples; ++isamp) {
            const sample_t& sample = samples[isamp];
            const uMatrix& nodes_pos = sample.get_node_positions();
            bond_dirs.zeros(nodes_pos.n_rows, nodes_pos.n_cols);
            // Local stats for this current sample. To correct for bias, these
            // values must be multiplied by the sample weight
            b2rstatmap_t b2rstat;
            // Compute normalized bond directions at this sample
            uUInt inode = 0;
            for (size_t iloc = 0; iloc < num_loci; ++iloc) {
                const uUInt num_nodes = num_nodes_at_locus[iloc];
                for (uUInt ioff = 1; ioff < num_nodes; ++ioff) {
                    const uMatSz_t icurr = U_TO_MAT_SZ_T(inode + ioff);
                    const uMatSz_t iprev = U_TO_MAT_SZ_T(icurr - 1);
                    uAssertPosEq(iprev + 1, icurr);
                    uAssertBounds(uDim_X, 0, nodes_pos.n_rows);
                    uAssertBounds(uDim_Y, 0, nodes_pos.n_rows);
                    uAssertBounds(uDim_Z, 0, nodes_pos.n_rows);
                    uAssertBounds(icurr, 1, nodes_pos.n_cols);
                    const uReal xprev = nodes_pos.at(uDim_X, iprev);
                    const uReal yprev = nodes_pos.at(uDim_Y, iprev);
                    const uReal zprev = nodes_pos.at(uDim_Z, iprev);
                    const uReal xcurr = nodes_pos.at(uDim_X, icurr);
                    const uReal ycurr = nodes_pos.at(uDim_Y, icurr);
                    const uReal zcurr = nodes_pos.at(uDim_Z, icurr);
                    uAssertBounds(iprev, 0, m_sim.get_max_total_num_nodes());
                    uAssertBounds(icurr, 1, m_sim.get_max_total_num_nodes());
                    const uReal rprev = m_sim.get_node_radius(U_TO_UINT(iprev));
                    const uReal rcurr = m_sim.get_node_radius(U_TO_UINT(icurr));
                    uAssert(rprev > U_TO_REAL(0.0));
                    uAssert(rcurr > U_TO_REAL(0.0));
                    const uReal inv_bond_len_euc =
                        U_TO_REAL(1.0) / (rprev + rcurr);
                    const uReal xnorm = inv_bond_len_euc * (xcurr - xprev);
                    const uReal ynorm = inv_bond_len_euc * (ycurr - yprev);
                    const uReal znorm = inv_bond_len_euc * (zcurr - zprev);
                    uAssertRealEq(
                        ((xnorm * xnorm) + (ynorm * ynorm) + (znorm * znorm)),
                        U_TO_REAL(1.0));
                    uAssertBounds(uDim_X, 0, bond_dirs.n_rows);
                    uAssertBounds(uDim_Y, 0, bond_dirs.n_rows);
                    uAssertBounds(uDim_Z, 0, bond_dirs.n_rows);
                    uAssertBounds(icurr, 1, bond_dirs.n_cols);
                    bond_dirs.at(uDim_X, icurr) = xnorm;
                    bond_dirs.at(uDim_Y, icurr) = ynorm;
                    bond_dirs.at(uDim_Z, icurr) = znorm;
                }
                inode += num_nodes;
            }  // end iteration over loci
            uAssert(inode == m_sim.get_max_total_num_nodes());
            // Compute pairwise dot products at each locus
            inode = 0;
            for (size_t iloc = 0; iloc < num_loci; ++iloc) {
                const uUInt num_nodes = num_nodes_at_locus[iloc];
                for (uUInt ioff = 1; (ioff + 1) < num_nodes; ++ioff) {
                    const uMatSz_t ii = U_TO_MAT_SZ_T(inode + ioff);
                    uAssertBounds(uDim_X, 0, bond_dirs.n_rows);
                    uAssertBounds(uDim_Y, 0, bond_dirs.n_rows);
                    uAssertBounds(uDim_Z, 0, bond_dirs.n_rows);
                    uAssertBounds(ii, 1, bond_dirs.n_cols);
                    const uReal xi = bond_dirs.at(uDim_X, ii);
                    const uReal yi = bond_dirs.at(uDim_Y, ii);
                    const uReal zi = bond_dirs.at(uDim_Z, ii);
                    uAssertRealEq(((xi * xi) + (yi * yi) + (zi * zi)),
                                  U_TO_REAL(1.0));
                    uAssertBounds(ii, 1, m_n2bmap.n_elem);
                    const uReal ri = m_n2bmap.at(ii);
                    uAssert(ri > U_TO_REAL(0.0));
                    uReal dist_bp = ri;
                    for (uUInt joff = ioff + 1; joff < num_nodes; ++joff) {
                        const uMatSz_t ij = U_TO_MAT_SZ_T(inode + joff);
                        uAssertBounds(ij, ioff + 1, bond_dirs.n_cols);
                        const uReal xj = bond_dirs.at(uDim_X, ij);
                        const uReal yj = bond_dirs.at(uDim_Y, ij);
                        const uReal zj = bond_dirs.at(uDim_Z, ij);
                        uAssertRealEq(((xj * xj) + (yj * yj) + (zj * zj)),
                                      U_TO_REAL(1.0));
                        const uReal cosij = (xi * xj) + (yi * yj) + (zi * zj);
                        uAssertRealBoundsInc(
                            cosij, U_TO_REAL(-1.0), U_TO_REAL(1.0));
                        uAssertBounds(ij, ioff + 1, m_n2bmap.n_elem);
                        const uReal rj = m_n2bmap.at(ij);
                        uAssert(rj > U_TO_REAL(0.0));
                        dist_bp += rj;
                        // Update running stats
                        b2rstat[dist_bp](cosij);
                    }
                }
                inode += num_nodes;
            }  // end iteration over loci
            uAssert(inode == m_sim.get_max_total_num_nodes());
            // Adjust for sampling bias
            const uReal w = weights.at(U_TO_MAT_SZ_T(isamp));
            uAssert(w >= U_TO_REAL(0.0));
            for (b2rstatmap_t::iterator rit = b2rstat.begin();
                 rit != b2rstat.end();
                 ++rit) {
                const uReal bond_len_bp = rit->first;
                const uMatrixUtils::running_stat<uReal>& rstat = rit->second;
                uAssertRealBoundsInc(
                    rstat.mean(), U_TO_REAL(-1.0), U_TO_REAL(1.0));
                uAssert(rstat.count() > U_TO_REAL(0.0));
                bcstat_t bcstat = {
                    rstat.mean() * w, rstat.var() * w, rstat.count()};
                b2bcstatiter_t bcit = b2bcstat.find(bond_len_bp);
                if (bcit != b2bcstat.end()) {
                    bcstat_t& prev_bcstat = bcit->second;
                    prev_bcstat.mean += bcstat.mean;
                    prev_bcstat.var += bcstat.var;
                    uAssertPosEq(prev_bcstat.count, bcstat.count);
                } else {
                    b2bcstat[bond_len_bp] = bcstat;
                }
            }
        }  // end iteration over samples

        // Collect spline design points
        uVecCol x_bp(U_TO_MAT_SZ_T(b2bcstat.size() + 1));
        uVecCol y_cos(x_bp.n_elem);
        uVecCol w_interp(x_bp.n_elem);
        // First knot is always 0 bp -> <cos> == 1
        x_bp.at(0) = U_TO_REAL(0.0);
        y_cos.at(0) = U_TO_REAL(1.0);
        w_interp.at(0) = U_TO_REAL(1.0);
        uMatSz_t iknot = 1;
        for (b2bcstatiter_t it = b2bcstat.begin(); it != b2bcstat.end(); ++it) {
            uAssertBounds(iknot, 0, x_bp.n_elem);
            uAssertBounds(iknot, 0, y_cos.n_elem);
            const uReal bond_len_bp = it->first;
            const bcstat_t& bcstat = it->second;
            x_bp.at(iknot) = bond_len_bp;
            y_cos.at(iknot) = bcstat.mean;
            w_interp.at(iknot) =
                m_use_weighted_interp ? bcstat.count : U_TO_REAL(1.0);
            uAssertRealBoundsInc(bcstat.mean, U_TO_REAL(-1.0), U_TO_REAL(1.0));
            ++iknot;
        }
        w_interp.at(0) = uMatrixUtils::max(w_interp);
        // Fit spline to function f : bp -> <cos> where bp is bond length in
        // base pairs and <cos> is mean cosine
        uCsppline<uReal> fbp2cos;
        fbp2cos.fit(x_bp, y_cos, uCspplineAutoSmoothCv);

        // Interpolate decorrelation length
        uAssert(x_bp.n_elem > 0);
        uAssertBoundsInc(m_decorr_cos, U_TO_REAL(-1.0), U_TO_REAL(1.0));
        // @TODO - set max base pairs to largest locus length
        const uReal bp_max = x_bp.at(x_bp.n_elem - 1);
        const uReal bp_res = U_TO_REAL(1.0);
        uAssert(bp_max > U_TO_REAL(0.0));
        uAssert(bp_res > U_TO_REAL(0.0));
        uReal bp_decorr = U_TO_REAL(0.0);
        // Grid search first location meeting bending threshold
        for (; bp_decorr <= bp_max; bp_decorr += bp_res) {
            if (fbp2cos(bp_decorr) <= m_decorr_cos) {
                break;
            }
        }
        bp_decorr = std::min(bp_max, bp_decorr);
        return bp_decorr;
    }

    /**
     * Prints tuner configuration to stdout
     */
    void print_tuner_config() const {
        uLogf("------------------------\n");
        uLogf("EBR tuner configuration:\n");
        uLogf("------------------------\n");
        uLogf("Decorr length (bp): %f\n", (double)m_decorr_bp);
        uLogf("Decorr <cos>: %f\n", (double)m_decorr_cos);
        uLogf("Output path: %s\n", m_out_fpath.c_str());
        uLogf("Max iters: %d\n", (int)m_max_iters);
        uLogf("Step decay rate: %f\n", (double)m_step_decay_rate);
        uLogf("Step scale: %f\n", (double)m_step_scale);
        uLogf("Step smooth lambda: %f\n", (double)m_step_smooth_lambda);
        uLogf("Step smooth count: %d\n", (int)m_step_smooth_count);
        uLogf("Step smooth start iter: %d\n", (int)m_step_smooth_start_iter);
        uLogf("Polyak-Ruppert averaging: %d\n", (int)m_should_avg);
        uLogf("Use weighted interp: %d\n", (int)m_use_weighted_interp);
        uLogf("Convergence threshold: %.10e\n", (double)m_conv_thresh);
        uLogf("Convergence iters: %d\n", (int)m_conv_iters);
        uLogf("Bond iters: %d\n", (int)m_bond_iters);
        uLogf("Rigidity search domain: [%f, %f]\n",
              (double)m_rigid_min,
              (double)m_rigid_max);
        uLogf("Logging interval: %d iterations\n", (int)m_log_interval);
        for (size_t i = 0; i < m_b2nmap.size(); ++i) {
            const uReal bond_len_bp = m_b2nmap[i].first;
            const std::vector<uUInt>& nodes = m_b2nmap[i].second;
            uAssert(!nodes.empty());
            uLogf(
                "EBR initialized mapping from bond length %f bp to %d nodes "
                "with rigidity at node %d = %f\n",
                (double)bond_len_bp,
                (int)nodes.size(),
                (int)nodes.front(),
                (double)m_sim.get_energy_bend_rigidity(nodes.front()));
        }
        uLogf("------------------------\n");
    }

    // Data members

    /**
     * Simulation object
     */
    sim_t m_sim;

    /**
     * Mapping from "bond length" to set of monomer nodes with corresponding
     * bond length in base pairs. The "bond length" is defined as the number
     * of base pairs between node centers of the triplet { i-1, i, i+1 },
     * where 'i' is the monomer index. This value is ignored for the first
     * and last nodes of a locus chain.
     */
    std::vector<b2ntup_t> m_b2nmap;

    /**
     * Mapping from node identifier to "bond length", where in this case, the
     * "bond length" at node i is the number of base pairs from node i-1 to i.
     * This value is ignored for the first node of a locus chain. Note, this
     * is *NOT* the same definition of bond length as m_b2nmap!
     */
    uVecCol m_n2bmap;

    /**
     * Number of base pairs represented by each chromatin node
     */
    uVecCol m_node_bp;

    /**
     * Step-size decay exponent used for stochastic approximation. Exponent
     * must be in (0, +inf). However, if using Polyak-Ruppert averaging
     * (m_should_avg = 1), then decay exponent must be in (0,1). Note, this is
     * called a rate simply because higher values lead to faster step decay.
     */
    uReal m_step_decay_rate;

    /**
     * Constant scale factor applied to each stochastic approximation step, must
     * be positive and defaults to 1.0
     */
    uReal m_step_scale;

    /**
     * Smoothing factor in [0,1] used for dampening oscillations in step size
     */
    uReal m_step_smooth_lambda;

    /**
     * Number of base pairs defining the "decorrelation" length (e.g. Kuhn or
     * persistence length)
     */
    uReal m_decorr_bp;

    /**
     * Target mean cosine defining decorrelation, must be in [-1, 1] and
     * should really be in [-1, 0]
     */
    uReal m_decorr_cos;

    /**
     * Minimum rigidity value allowed to be proposed by tuner
     */
    uReal m_rigid_min;

    /**
     * Maximum rigidity value allowed to be proposed by tuner
     */
    uReal m_rigid_max;

    /**
     * Convergence threshold
     */
    uReal m_conv_thresh;

    /**
     * Maximum number of iterations for stochastic approximation
     */
    uUInt m_max_iters;

    /**
     * Number of consecutive iterations that estimate must be within
     * convergence threshold to be considered converged.
     */
    uUInt m_conv_iters;

    /**
     * Number of iterations to sample from current bond type before
     * switching to next bond type
     */
    uUInt m_bond_iters;

    /**
     * Window size used for smoothing the observed decorrelation length
     */
    uUInt m_step_smooth_count;

    /**
     * 0-based iteration for when to start step smoothing
     */
    uUInt m_step_smooth_start_iter;

    /**
     * User defined tuner log interval, must be positive
     */
    uUInt m_log_interval;

    /**
     * Determines if Polyak-Ruppert averaging should be used at end of
     * stochastic approximation pass
     */
    uBool m_should_avg;

    /**
     * If "1", weighted spline interpolation is used to infer <cos> at target
     * genomic decorrelation length, else unweighted interpolation is used.
     */
    uBool m_use_weighted_interp;

    /**
     * Output path for tuned rigidities
     */
    std::string m_out_fpath;
};

#endif  // defined(U_BUILD_ENABLE_COMMANDLETS) || defined(U_BUILD_ENABLE_TESTS)

#endif  // uSisSeoBendEnergyRigidityTuner_h
