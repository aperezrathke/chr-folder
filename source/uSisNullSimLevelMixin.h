//****************************************************************************
// uSisNullSimLevelMixin.h
//****************************************************************************

/**
 * Sis: Sequential importance sampling
 *
 * Minimal interface for simulation level mixin. Provides no functionality.
 */

#ifndef uSisNullSimLevelMixin_h
#define uSisNullSimLevelMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "uBuild.h"
#include "uConfig.h"
#include "uSisUtilsGrowth.h"
#include "uSisUtilsRc.h"
#include "uThread.h"
#include "uTypes.h"

//****************************************************************************
// Simulation level mixin
//****************************************************************************

template <typename t_SisGlue>
class uSisNullSimLevelMixin {
public:
    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes simulation level (global) data
     * @param sim - Parent simulation
     * @param config - User configuration options
     */
    void init(const sim_t& sim, uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() {}

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    U_SIS_DECLARE_MIXIN_NULL_REGROW_INTERFACE
};

#endif  // uSisNullSimLevelMixin_h
