//****************************************************************************
// uSisHetrLagScheduleMixin.h
//****************************************************************************

/**
 * ScheduleMixin = A mixin that defines the checkpoint schedule for
 * a quality control algorithm.
 */

#ifndef uSisHetrLagScheduleMixin_h
#define uSisHetrLagScheduleMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uOpts.h"
#include "uParserRle.h"
#include "uSisNullQcModMixin.h"
#include "uSisUtilsQc.h"
#include "uThread.h"
#include "uTypes.h"

#include <vector>

/**
 * A quality control schedule is responsible for determining when check points
 * occur. This schedule can can be initialized from a run-length-encoded (RLE)
 * file, which allows simulations with heterogeneous lag values. The lag can
 * also be initialized from command line or config (INI) file to a single
 * scalar value (in which case its functionality will be equivalent to the
 * homogeneous lag schedule).
 *
 * The format for the RLE file: each row is a <run, lag> pair.
 * Example:
 *
 *  5, 2
 *  10, 4
 *  5, 3
 *
 * Will create a lag where for the first 5 completed grow steps (1,2,3,4,5),
 * the lag value will be 2 (meaning a checkpoint will occur when 2 and 4 steps
 * have been completed). For the next 10 (6,7,8,9,10,11,12,13,14,15), the lag
 * value will be 4 (meaning checkpoints will occur after 8 and 12 grow steps
 * have been completed). For the next 5 completed steps (16,17,18,19,20), the
 * lag value will be 3 (meaning a checkpoint after 18 steps have completed).
 * THIS IS EFFECTIVELY 1-BASED INDEXING!
 *
 * Note: If a completed grow step count is encountered that is past the end
 * of the RLE encoded values, the last lag value will be used.
 *
 * This schedule is deterministic and supports landmark trial runner
 *  tr_find_first() and tr_find_next() calls.
 */
template <
    // The core simulation structures
    typename t_SisGlue,
    // The key used for initializing heterogeneous lags from run-length encoded
    // file
    enum uOptE e_KeyLagFile = uOpt_qc_schedule_lag_file_slot0,
    // The key used for initializing homogeneous scalar lag from user
    // configuration
    enum uOptE e_KeyLag = uOpt_qc_schedule_lag_slot0>
class uSisHetrLagScheduleMixin : public uSisNullQcModMixin<t_SisGlue> {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Must be defined - if true, the qc mixin avoids recomputing exponential
     * weights
     */
    enum { calculates_norm_exp_weights = false };

    /**
     * The default lag value
     */
    enum { default_lag = 1 };

    /**
     * Constructor
     */
    uSisHetrLagScheduleMixin() { clear(); }

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     * @param is_tr - set to true if schedule is being used for landmark trial
     *  running, false o/w (lag schedule will be patched to keep expected
     *  checkpoint behavior consistent between quality control and trial runner)
     */
    void init(const sim_t& sim,
              uSpConstConfig_t config,
              const bool is_tr U_THREAD_ID_PARAM) {
        clear();
        // Check if heterogeneous RLE lag file exists
        std::string lag_fpath;
        uBool lag_read_ok = uFALSE;
        if (config->resolve_path(lag_fpath, e_KeyLagFile)) {
            if (uParserRle::read<uUInt, std::vector<uUInt> >(m_lags,
                                                             lag_fpath)) {
                uAssert(!m_lags.empty());
                // Pad first element as completed grow steps are 1-based
                m_lags.insert(m_lags.begin(), m_lags.front());
                lag_read_ok = uTRUE;
            } else {
                uLogf("Warning: unable to parse %s.\n", lag_fpath.c_str());
            }
        }

        // Check if homogeneous scalar lag exists
        uUInt lag = default_lag;
        if (!lag_read_ok) {
            config->read_into(lag, e_KeyLag);
            uAssert(lag >= 1);
            m_lags.clear();
            m_lags.resize(1, lag);
        }

        // Check all lags are >= 1
        const size_t NUM_LAGS = m_lags.size();
        uAssert(NUM_LAGS > 0);
        for (size_t i = 0; i < NUM_LAGS; ++i) {
            lag = m_lags[i];
            // Check lag is positive
            if (lag < 1) {
                uLogf("Error: lag schedule requires all lags > 0. Exiting.\n");
                exit(uExitCode_error);
            }
        }

        // Make sure lag behavior is consistent between quality control
        // checkpoint scheduling and trial runner scheduling
        if (is_tr) {
            lag = m_lags.front();
            size_t prev = 0;
            uUInt next = lag;
            for (size_t i = 1; i < NUM_LAGS; ++i) {
                uAssertBounds(i, 0, m_lags.size());
                lag = m_lags[i];
                if (U_TO_UINT(i) == next) {
                    prev = i;
                    next = U_TO_UINT(U_NEXT_MULTIPLE(i, lag));
                } else if ((i % lag) == 0) {
                    uAssert(i < next);
                    uAssert(i > prev);
                    uAssertBounds(prev, 0, m_lags.size());
                    // We've encountered a checkpoint that would be skipped
                    // under trial runner scheduling. Lets patch up the lags
                    // array to make schedules consistent under both paradigms.
                    for (size_t j = prev; j < i; ++j) {
                        m_lags[j] = U_TO_UINT(i);
                    }
                    prev = i;
                    next = U_TO_UINT(U_NEXT_MULTIPLE(i, lag));
                }
            }
        }
    }

    /**
     * Light-weight (re-)initialization
     */
    void init_light(const sim_t& sim,
                    uSpConstConfig_t config,
                    const bool is_tr U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    void clear() { m_lags.clear(); }

    /**
     * Quality control interface
     * Determines if a quality control check point has been reached (meaning
     * it's okay to go ahead and perform quality control step).
     * @param out_norm_exp_weights - an optional *uninitialized* vector of
     *  exponential weights that must be initialized if:
     *      calculates_norm_exp_weights == TRUE
     * @param out_max_log_weight - an optional *uninitialized* reference to the
     *  maximum log weight within the log_weights vector. Must be initialized
     *  if: calculates_norm_exp_weights == TRUE
     * @param num_completed_grow_steps_per_sample - the number of seed +
     *  grow_step calls undergone by all samples in simulation
     * @param qc - the parent quality control mixin
     * @param sim - parent simulation
     */
    inline uBool qc_checkpoint_reached(
        uVecCol& out_norm_exp_weights,
        uReal& out_max_log_weight,
        const uVecCol& log_weights,
        const uUInt num_completed_grow_steps_per_sample,
        const sim_level_qc_mixin_t& qc,
        const sim_t& sim) const {
        const uUInt lag = this->get_lag(num_completed_grow_steps_per_sample);
        return ((num_completed_grow_steps_per_sample % lag) == 0);
    }

    /**
     * Trial runner interface
     * @param max_grow_steps - max grow step count
     * @return first landmark grow step count
     */
    inline uUInt tr_find_first(const uUInt max_grow_steps) const {
        uAssert(!m_lags.empty());
        const uUInt lag = m_lags.front();
        // Trial runner expects at least 2 steps
        uAssert(lag > 1);
        return std::min(lag, max_grow_steps);
    }

    /**
     * Trial runner interface
     * @param completed_grow_steps - number of grow steps already completed
     * @param max_grow_steps - maximum number of grow steps allowed
     * @return next landmark grow step count
     */
    inline uUInt tr_find_next(const uUInt completed_grow_steps,
                              const uUInt max_grow_steps) const {
        const uUInt lag = this->get_lag(completed_grow_steps);
        const uUInt next_grow_steps =
            U_NEXT_MULTIPLE(completed_grow_steps, lag);
        return std::min(next_grow_steps, max_grow_steps);
    }

private:
    /**
     * @return lag value at completed step count
     */
    inline uUInt get_lag(const uUInt completed_steps) const {
        uAssert(!m_lags.empty());
        const size_t ix =
            std::min((m_lags.size() - 1), static_cast<size_t>(completed_steps));
        uAssertBounds(ix, 0, m_lags.size());
        const uUInt lag = m_lags[ix];
        uAssert(lag > 0);
        return lag;
    }

    /**
     * A check point is encountered based on multiples of the active lag
     * value. For example, if lag value is 1, then a check point occurs after
     * every completed grow step.
     */
    std::vector<uUInt> m_lags;
};

#endif  // uSisHetrLagScheduleMixin_h
