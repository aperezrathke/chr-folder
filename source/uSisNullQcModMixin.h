//****************************************************************************
// uSisNullQcModMixin.h
//****************************************************************************

/**
 * A null quality control module mixin - defines empty boilerplate interface
 */

#ifndef uSisNullQcModMixin_h
#define uSisNullQcModMixin_h

#include "uBuild.h"
#include "uConfig.h"
#include "uOpts.h"
#include "uTypes.h"

/**
 * Null interface for a module that is to be mixed-into a quality control
 * object (which, in turn, the qc object is mixed-into a simulation object)
 */
template <
    // The core simulation structures
    typename t_SisGlue>
class uSisNullQcModMixin {
public:
    // Typedefs

    U_SIS_INJECT_TYPEDEFS(t_SisGlue);

    /**
     * Initializes schedule
     * @param sim - parent simulation
     * @param config - user configuration options
     */
    inline void init(const sim_t& sim,
                     uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    /**
     * Resets to default state
     */
    inline void clear() {}

    /**
     * Preps for next trial
     */
    void reset_for_next_trial(const sim_t& sim,
                              uSpConstConfig_t config U_THREAD_ID_PARAM) {}

    // BEGIN REGROW INTERFACE

    /**
     * The following methods are used by rejection control mixin for regrowing
     * a single sample.
     */

    /**
     * Called when a sample is being regrown.
     */
    void reset_for_regrowth(const sim_level_qc_mixin_t& qc,
                            const sample_t& sample,
                            const uUInt growth_phase_count,
                            const sim_t& sim,
                            uSpConstConfig_t config) {}

    /**
     * Called when a sample has finished reseeding
     */
    void on_reseed_finish(const sim_level_qc_mixin_t& qc,
                          const sample_t& sample,
                          const sim_t& sim) {}

    /**
     * Called when a sample has completed a regrowth step
     */
    void on_regrow_step_finish(const sim_level_qc_mixin_t& qc,
                               const sample_t& sample,
                               const bool status,
                               const uUInt growth_phase_count,
                               const sim_t& sim) {}

    // END REGROW INTERFACE
};

#endif  // uSisNullQcModMixin_h
