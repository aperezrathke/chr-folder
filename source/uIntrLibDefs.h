//****************************************************************************
// uIntrLibDefs.h
//****************************************************************************

/**
 * Global constants useful for interaction constraint utilities
 */

#ifndef uIntrLibDefs_h
#define uIntrLibDefs_h

#include "uBuild.h"
#include "uStaticAssert.h"
#include "uTypes.h"

//****************************************************************************
// Typedefs, structs, enums
//****************************************************************************

/**
 * Offsets for min and max pair indices
 */
enum uIntrLibPairIx {
    uIntrLibPairIxMin = 0,
    uIntrLibPairIxMax,
    uIntrLibPairIxNum
};

/**
 * Vector of interaction look-up indices
 */
typedef std::vector<uUInt16_t> uSisIntrIndexVec_t;

/**
 * [Jagged] table of interaction look-up indices at each locus
 */
typedef std::vector<uSisIntrIndexVec_t> uSisIntrLociTable_t;

//****************************************************************************
// Chr-chr interactions
//****************************************************************************

/**
 * Default distance threshold in Angstroms for knock-in chr-chr interaction
 * constraints, based on 3C cross-linking threshold as reported in:
 *
 *  Giorgetti, Luca, and Edith Heard. "Closing the loop: 3C versus DNA FISH
 *      DNA FISH." Genome biology 17.1 (2016): 215
 *
 * NOTE : In above review, threshold is given as 1000 Angstroms, but Giorgetti
 * actually cites one of his earlier Cell papers which reports an optimal
 * cross-linking distance of 795 Angstroms, see:
 *
 *  Giorgetti, Luca, et al. "Predictive polymer modeling reveals coupled
 *      fluctuations in chromosome conformation and transcription." Cell 157.4
 *      (2014): 950-963.
 */
#define uIntrChrDefaultKnockInDist U_TO_REAL(795.0)

/**
 * Default distance scale for knock-out chr-chr interaction constraints. The
 * default behavior for knock-out constraints is to simply set the minimal
 * distance to be a multiple of the *knock-in* distance.
 */
#define uIntrChrDefaultKnockOutDistScale U_TO_REAL(1.25)

/**
 * Default knock-in chr-chr interaction failure budget in [0,1]
 */
#define uIntrChrDefaultKnockInFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrChrDefaultKnockInFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrChrDefaultKnockInFailBudgFrac >= U_TO_REAL(0.0));
#endif

/**
 * Default knock-out chr-chr interaction failure budget in [0,1]
 */
#define uIntrChrDefaultKnockOutFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrChrDefaultKnockOutFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrChrDefaultKnockOutFailBudgFrac >= U_TO_REAL(0.0));
#endif

//****************************************************************************
// Lamina interactions
//****************************************************************************

/**
 * Description of chr-chr interaction fragments
 */
#define uIntrChrFragDataName "fragments"

/**
 * Description for chr-chr knock-in interactions
 */
#define uIntrChrKnockInDataName "knock-in"

/**
 * Description for chr-chr knock-out interactions
 */
#define uIntrChrKnockOutDataName "knock-out"

/**
 * Default distance threshold in Angstroms for knock-in lam-chr interaction
 * constraints, based on:
 *
 *  Robson, Michael I., Jose I. de las Heras, Rafal Czapiewski, Aishwarya
 *      Sivakumar, Alastair RW Kerr, and Eric Schirmer. "Constrained release
 *      of lamina-associated enhancers and genes from the nuclear envelope
 *      during T-cell activation facilitates their association in chromosome
 *      compartments." Genome research (2017): gr-212308.
 *
 * Figure 3 shows median distances to the lamina for 5 lamina associated loci
 * (GBP, CBLB, CD300, BTLA, IL2) during T-cell resting and active states. We
 * take the median of the median distances during the resting state since FISH
 * shows the loci to be at the nuclear periphery. The median lamina distances
 * during the T-cell resting state for GBP, CBLB, CD200, BTLA, and IL2 are
 * 0.63, 0.26, 0.35, 0.29, and 0.29 microns respectively; the median among
 * these values is:
 *
 *  median(0.63, 0.26, 0.35, 0.29, 0.29) = 0.29 microns = 2900 Angstroms
 */
#define uIntrLamDefaultKnockInDist U_TO_REAL(2900.0)

/**
 * Default distance scale for knock-out lam-chr interaction constraints (see
 * description for uIntrChrDefaultKnockOutDistScale)
 */
#define uIntrLamDefaultKnockOutDistScale uIntrChrDefaultKnockOutDistScale

/**
 * Default knock-in lamina interaction failure budget in [0,1]
 */
#define uIntrLamDefaultKnockInFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrLamDefaultKnockInFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrLamDefaultKnockInFailBudgFrac >= U_TO_REAL(0.0));
#endif

/**
 * Default knock-out lamina interaction budget in [0,1]
 */
#define uIntrLamDefaultKnockOutFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrLamDefaultKnockOutFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrLamDefaultKnockOutFailBudgFrac >= U_TO_REAL(0.0));
#endif

/**
 * Description of lamina interaction fragments
 */
#define uIntrLamFragDataName "fragments"

/**
 * Description for lamina knock-in interactions
 */
#define uIntrLamKnockInDataName "knock-in"

/**
 * Description for lamina knock-out interactions
 */
#define uIntrLamKnockOutDataName "knock-out"

//****************************************************************************
// Nuclear body interactions
//****************************************************************************

/**
 * Offsets for fragment and body pair indices
 */
enum uIntrNucbPairIx {
    uIntrNucbPairIxFrag = 0,
    uIntrNucbPairIxBody,
    uIntrNucbPairIxNum
};

/**
 * Default distance threshold for knock-in nuclear body interaction
 *  constraints (see description for uIntrChrDefaultKnockInDist)
 */
#define uIntrNucbDefaultKnockInDist uIntrChrDefaultKnockInDist

/**
 * Default distance scale for knock-out nuclear body interaction constraints
 * (see description for uIntrChrDefaultKnockOutDistScale)
 */
#define uIntrNucbDefaultKnockOutDistScale uIntrChrDefaultKnockOutDistScale

/**
 * Default knock-in nuclear body interaction failure budget in [0,1]
 */
#define uIntrNucbDefaultKnockInFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrNucbDefaultKnockInFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrNucbDefaultKnockInFailBudgFrac >= U_TO_REAL(0.0));
#endif

/**
 * Default knock-out nuclear body interaction budget in [0,1]
 */
#define uIntrNucbDefaultKnockOutFailBudgFrac U_TO_REAL(0.0)
#ifdef U_BUILD_CXX_11
U_STATIC_ASSERT(uIntrNucbDefaultKnockOutFailBudgFrac <= U_TO_REAL(1.0));
U_STATIC_ASSERT(uIntrNucbDefaultKnockOutFailBudgFrac >= U_TO_REAL(0.0));
#endif

/**
 * Description of nuclear body interaction fragments
 */
#define uIntrNucbFragDataName "fragments"

/**
 * Description for nuclear body knock-in interactions
 */
#define uIntrNucbKnockInDataName "knock-in"

/**
 * Description for nuclear body knock-out interactions
 */
#define uIntrNucbKnockOutDataName "knock-out"

#endif  // uIntrLibDefs_h
