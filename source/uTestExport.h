//****************************************************************************
// uTestExport.h
//****************************************************************************

/**
 * Commandlets for testing export of chromatin chains
 *
 * Usage:
 * -test_pdb | -test_pml | -test_csv |
 *  -test_pdb_gz | -test_pml_gz | test_csv_gz |
 *  -test_pdb_bulk | -test_pml_bulk | test_csv_bulk |
 *  -test_pdb_bulk_gz | test_pml_bulk_gz | test_csv_bulk_gz |
 *  -test_ligc_csv | -test_ligc_csv_gz |-test_ligc_bin |
 *  -test_ligc_csv_bulk | -test_ligc_csv_bulk_gz
 *      [--output_dir <output_dir>] [--energy_bend_rigidity <float >= 0.0>]
 *      [--radii_fpath <filepath>]
 *      [--energy_hetr_bend_rigidity_fpath <filepath>]
 *
 * or, if default settings are okay to use, then simply:
 * -test_pdb | -test_pml | -test_csv | ...
 */

#ifdef uTestExport_h
#   error "Test Export included multiple times!"
#endif  // uTestExport_h
#define uTestExport_h

#include "uBuild.h"

#ifdef U_BUILD_ENABLE_TESTS

#include "uCmdOptsMap.h"
#include "uConfig.h"
#include "uExitCodes.h"
#include "uMockUpUtils.h"
#include "uSisUtilsExport.h"
#include "uSisUtilsQc.h"
#include "uStats.h"
#include "uStringUtils.h"
#include "uThread.h"
#include "uTypes.h"

#include <iostream>
#include <numeric>
#include <string>

namespace uTestExport {

/**
 * Test export for templated glue object
 */
template <typename t_Glue>
int test_export(const uCmdOptsMap& cmd_opts,
                const std::string& default_output_dir,
                const std::string& default_job_id,
                const uUInt default_export_flags,
                const std::vector<uUInt>& default_num_nodes,
                const uReal default_energy_bend_rigidity) {
    // Macro for simulation type
    typedef typename t_Glue::sim_t sim_t;

    const uUInt DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES = std::accumulate(
        default_num_nodes.begin(), default_num_nodes.end(), U_TO_UINT(0));
    const uUInt DEFAULT_EXPORT_TEST_MAX_TRIALS = 1000;
    const uUInt DEFAULT_EXPORT_TEST_ENSEMBLE_SIZE = 3;
    const uReal DEFAULT_EXPORT_TEST_MAX_NODE_DIAMETER = U_TO_REAL(110.0);
    const uReal DEFAULT_EXPORT_TEST_NUCLEAR_DIAMETER = U_TO_REAL(8000.0);
    const uUInt DEFAULT_EXPORT_TEST_NUM_UNIT_SPHERE_SAMPLE_POINTS = 64;
    const std::string DEFAULT_EXPORT_TEST_DIAM_DIR(
        "../tests/test_sis_hetr_diam/");
    const std::string DEFAULT_EXPORT_TEST_DIAM_FPATH(
        DEFAULT_EXPORT_TEST_DIAM_DIR + "diam." +
        u2Str(DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES) + ".txt");
    const std::string DEFAULT_EXPORT_TEST_RIGID_DIR(
        "../tests/test_sis_hetr_rigid/");
    const std::string DEFAULT_EXPORT_TEST_RIGID_FPATH(
        DEFAULT_EXPORT_TEST_RIGID_DIR + "rigid." +
        u2Str(DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES) + ".txt");

    // Create a configuration
    uSpConfig_t config = uSmartPtr::make_shared<uConfig>();
    config->output_dir = default_output_dir;
    config->job_id = default_job_id;
    config->max_trials = DEFAULT_EXPORT_TEST_MAX_TRIALS;
    config->ensemble_size = DEFAULT_EXPORT_TEST_ENSEMBLE_SIZE;
    config->set_option(uOpt_max_node_diameter,
                       DEFAULT_EXPORT_TEST_MAX_NODE_DIAMETER);
    config->set_option(uOpt_nuclear_diameter,
                       DEFAULT_EXPORT_TEST_NUCLEAR_DIAMETER);
    config->num_nodes = default_num_nodes;
    config->num_unit_sphere_sample_points =
        DEFAULT_EXPORT_TEST_NUM_UNIT_SPHERE_SAMPLE_POINTS;
    config->set_option(uOpt_energy_bend_rigidity, default_energy_bend_rigidity);
    // Only test reading bend rigidities from file if non-heterogeneous nodes
    if (!sim_t::is_homg_radius) {
        config->set_option(uOpt_energy_bend_rigidity_fpath,
                           DEFAULT_EXPORT_TEST_RIGID_FPATH);
    }
    config->export_flags = default_export_flags;
    config->set_option(uOpt_node_diameter_fpath,
                       DEFAULT_EXPORT_TEST_DIAM_FPATH);

    // Override from command line
    config->init(cmd_opts, uFALSE /*should_clear*/);

    // Output configuration
    std::cout << "Running Test Export for " << config->job_id << std::endl;
    config->print();

    // Feed configuration to simulation
    sim_t sim(config U_MAIN_THREAD_ID_ARG);

    // Do simulation!
    const uBool result = sim.run();

    std::cout << "Simulation completed with status: " << result << std::endl;

    // Compute effective sample size
    const uReal ess =
        uSisUtilsQc::calc_ess_cv_from_log(sim.get_completed_log_weights_view());
    std::cout << "Effective sample size: " << ess << std::endl;

    U_STATS_REPORT;

    // Export!
    uSisUtils::export_sim(sim);

    return uExitCode_normal;
}
}  // namespace uTestExport

/**
 * Entry point for test
 */
int uTestExportMain(const uCmdOptsMap& cmd_opts,
                    const std::string& default_output_dir,
                    const std::string& default_job_id,
                    const uUInt default_export_flags) {
    // Default Boltzmann energy sampling configuration
    const uReal DEFAULT_EXPORT_TEST_BEND_RIGIDITY = U_TO_REAL(2.0);
    // Default single locus length
    const uUInt DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES = U_TO_UINT(3000);
    const std::vector<uUInt> DEFAULT_EXPORT_TEST_NUM_NODES_SLOC(
        1 /*n_chains*/, DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES /*n_nodes*/);
    // Default multi-loci lengths
    const uUInt DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC = 3;
    const uUInt DEFAULT_EXPORT_TEST_MIN_NODES_PER_LOCUS =
        DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES / DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC;
    const uUInt DEFAULT_EXPORT_TEST_MAX_DELTA_NODES =
        (DEFAULT_EXPORT_TEST_TOTAL_NUM_NODES %
         DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC);
    const uUInt DEFAULT_EXPORT_TEST_NUM_NODES_MLOC_ARRAY
        [DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC] = {
            DEFAULT_EXPORT_TEST_MIN_NODES_PER_LOCUS,
            DEFAULT_EXPORT_TEST_MIN_NODES_PER_LOCUS,
            DEFAULT_EXPORT_TEST_MIN_NODES_PER_LOCUS +
                DEFAULT_EXPORT_TEST_MAX_DELTA_NODES};
    std::vector<uUInt> DEFAULT_EXPORT_TEST_NUM_NODES_MLOC(
        DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC);
    for (uUInt i = 0; i < DEFAULT_EXPORT_TEST_NUM_LOCI_MLOC; ++i) {
        uAssert(DEFAULT_EXPORT_TEST_NUM_NODES_MLOC_ARRAY[i] > 0);
        DEFAULT_EXPORT_TEST_NUM_NODES_MLOC[i] =
            DEFAULT_EXPORT_TEST_NUM_NODES_MLOC_ARRAY[i];
    }

    ////////////////////////////////////////////////////////////////
    // Test Boltzmann bend energy distribution

    // Single chain, homogeneous
    std::string job_id =
        default_job_id + std::string(".bend.seo.sloc.homg.null");
    uTestExport::test_export<uMockUpUtils::uSisBendSeoSlocHomgNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_SLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Single chain, heterogeneous
    job_id = default_job_id + std::string(".bend.seo.sloc.hetr.null");
    uTestExport::test_export<uMockUpUtils::uSisBendSeoSlocHetrNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_SLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Multi-chain, homogeneous
    job_id = default_job_id + std::string(".bend.seo.mloc.homg.null");
    uTestExport::test_export<uMockUpUtils::uSisBendSeoMlocHomgNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_MLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Multi-chain, heterogeneous
    job_id = default_job_id + std::string(".bend.seo.mloc.hetr.null");
    uTestExport::test_export<uMockUpUtils::uSisBendSeoMlocHetrNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_MLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    ////////////////////////////////////////////////////////////////
    // Test uniform energy distribution

    // Single chain, homogeneous
    job_id = default_job_id + std::string(".unif.seo.sloc.homg.null");
    uTestExport::test_export<uMockUpUtils::uSisUnifSeoSlocHomgNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_SLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Single chain, heterogeneous
    job_id = default_job_id + std::string(".unif.seo.sloc.hetr.null");
    uTestExport::test_export<uMockUpUtils::uSisUnifSeoSlocHetrNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_SLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Multi-chain, homogeneous
    job_id = default_job_id + std::string(".unif.seo.mloc.homg.null");
    uTestExport::test_export<uMockUpUtils::uSisUnifSeoMlocHomgNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_MLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    // Multi-chain, homogeneous
    job_id = default_job_id + std::string(".unif.seo.mloc.hetr.null");
    uTestExport::test_export<uMockUpUtils::uSisUnifSeoMlocHetrNullGlue>(
        cmd_opts,
        default_output_dir,
        job_id,
        default_export_flags,
        DEFAULT_EXPORT_TEST_NUM_NODES_MLOC,
        DEFAULT_EXPORT_TEST_BEND_RIGIDITY);

    return uExitCode_normal;
}

/**
 * Entry point for CSV export test. Usage:
 *  -test_csv
 */
int uTestCsvMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_CSV_TEST_JOB_ID("test_csv");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_CSV_TEST_OUTPUT_DIR = "../output/test_csv";

    // Initialize globals to default
    uG::default_init();

    const uUInt result = uTestExportMain(cmd_opts,
                                         DEFAULT_CSV_TEST_OUTPUT_DIR,
                                         DEFAULT_CSV_TEST_JOB_ID,
                                         uExportCsv | uExportExtended);

    // Destroy globals
    uG::teardown();

    return ((int)result);
}

/**
 * Entry point for compressed CSV export test. Usage:
 *  -test_csv_gz
 */
int uTestCsvGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_CSV_GZ_TEST_JOB_ID("test_csv_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_CSV_GZ_TEST_OUTPUT_DIR = "../output/test_csv_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_CSV_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_CSV_GZ_TEST_JOB_ID,
                                       uExportCsvGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk CSV export test. Usage:
 *  -test_csv_bulk
 */
int uTestCsvBulkMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_CSV_BULK_TEST_JOB_ID("test_csv_bulk");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_CSV_TEST_OUTPUT_DIR = "../output/test_csv_bulk";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_CSV_TEST_OUTPUT_DIR,
                                       DEFAULT_CSV_BULK_TEST_JOB_ID,
                                       uExportCsvBulk | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk compressed CSV export test. Usage:
 *  -test_csv_bulk_gz
 */
int uTestCsvBulkGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_CSV_BULK_GZ_TEST_JOB_ID("test_csv_bulk_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_CSV_GZ_TEST_OUTPUT_DIR =
        "../output/test_csv_bulk_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_CSV_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_CSV_BULK_GZ_TEST_JOB_ID,
                                       uExportCsvBulkGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for PDB export test. Usage:
 *  -test_pdb
 */
int uTestPdbMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PDB_TEST_JOB_ID("test_pdb");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PDB_TEST_OUTPUT_DIR = "../output/test_pdb";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PDB_TEST_OUTPUT_DIR,
                                       DEFAULT_PDB_TEST_JOB_ID,
                                       uExportPdb | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for compressed PDB export test. Usage:
 *  -test_pdb_gz
 */
int uTestPdbGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PDB_GZ_TEST_JOB_ID("test_pdb_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PDB_GZ_TEST_OUTPUT_DIR = "../output/test_pdb_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PDB_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_PDB_GZ_TEST_JOB_ID,
                                       uExportPdbGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk PDB export test. Usage:
 *  -test_pdb_bulk
 */
int uTestPdbBulkMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PDB_BULK_TEST_JOB_ID("test_pdb_bulk");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PDB_BULK_TEST_OUTPUT_DIR =
        "../output/test_pdb_bulk";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PDB_BULK_TEST_OUTPUT_DIR,
                                       DEFAULT_PDB_BULK_TEST_JOB_ID,
                                       uExportPdbBulk | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk compressed PDB export test. Usage:
 *  -test_pdb_bulk_gz
 */
int uTestPdbBulkGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PDB_BULK_GZ_TEST_JOB_ID("test_pdb_bulk_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PDB_BULK_GZ_TEST_OUTPUT_DIR =
        "../output/test_pdb_bulk_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PDB_BULK_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_PDB_BULK_GZ_TEST_JOB_ID,
                                       uExportPdbBulkGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for PML export test. Usage:
 *  -test_pml
 */
int uTestPmlMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PML_TEST_JOB_ID("test_pml");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PML_TEST_OUTPUT_DIR = "../output/test_pml";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PML_TEST_OUTPUT_DIR,
                                       DEFAULT_PML_TEST_JOB_ID,
                                       uExportPml | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for PML compressed export test. Usage:
 *  -test_pml_gz
 */
int uTestPmlGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PML_GZ_TEST_JOB_ID("test_pml_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PML_GZ_TEST_OUTPUT_DIR = "../output/test_pml_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PML_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_PML_GZ_TEST_JOB_ID,
                                       uExportPmlGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk PML export test. Usage:
 *  -test_pml_bulk
 */
int uTestPmlBulkMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PML_BULK_TEST_JOB_ID("test_pml_bulk");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PML_BULK_TEST_OUTPUT_DIR =
        "../output/test_pml_bulk";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PML_BULK_TEST_OUTPUT_DIR,
                                       DEFAULT_PML_BULK_TEST_JOB_ID,
                                       uExportPmlBulk | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk PML compressed export test. Usage:
 *  -test_pml_bulk_gz
 */
int uTestPmlBulkGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_PML_BULK_GZ_TEST_JOB_ID("test_pml_bulk_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_PML_BULK_GZ_TEST_OUTPUT_DIR =
        "../output/test_pml_bulk_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_PML_BULK_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_PML_BULK_GZ_TEST_JOB_ID,
                                       uExportPmlBulkGz | uExportExtended);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for ligc CSV export test. Usage:
 *  -test_ligc_csv
 */
int uTestLigcCsvMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_LIGC_CSV_TEST_JOB_ID("test_ligc_csv");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_LIGC_CSV_TEST_OUTPUT_DIR =
        "../output/test_ligc_csv";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_LIGC_CSV_TEST_OUTPUT_DIR,
                                       DEFAULT_LIGC_CSV_TEST_JOB_ID,
                                       uExportLigcCsv);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for ligc compressed CSV export test. Usage:
 *  -test_ligc_csv_gz
 */
int uTestLigcCsvGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_LIGC_CSV_GZ_TEST_JOB_ID("test_ligc_csv_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_LIGC_CSV_GZ_TEST_OUTPUT_DIR =
        "../output/test_ligc_csv_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_LIGC_CSV_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_LIGC_CSV_GZ_TEST_JOB_ID,
                                       uExportLigcCsvGz);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk ligc CSV export test. Usage:
 *  -test_ligc_csv_bulk
 */
int uTestLigcCsvBulkMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_LIGC_CSV_BULK_TEST_JOB_ID("test_ligc_csv_bulk");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_LIGC_CSV_BULK_TEST_OUTPUT_DIR =
        "../output/test_ligc_csv_bulk";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_LIGC_CSV_BULK_TEST_OUTPUT_DIR,
                                       DEFAULT_LIGC_CSV_BULK_TEST_JOB_ID,
                                       uExportLigcCsvBulk);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for bulk ligc compressed CSV export test. Usage:
 *  -test_ligc_csv_gz_bulk
 */
int uTestLigcCsvBulkGzMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_LIGC_CSV_BULK_GZ_TEST_JOB_ID(
        "test_ligc_csv_bulk_gz");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_LIGC_CSV_BULK_GZ_TEST_OUTPUT_DIR =
        "../output/test_ligc_csv_bulk_gz";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_LIGC_CSV_BULK_GZ_TEST_OUTPUT_DIR,
                                       DEFAULT_LIGC_CSV_BULK_GZ_TEST_JOB_ID,
                                       uExportLigcCsvBulkGz);

    // Destroy globals
    uG::teardown();

    return result;
}

/**
 * Entry point for ligc binary export test. Usage:
 *  -test_ligc_bin
 */
int uTestLigcBinMain(const uCmdOptsMap& cmd_opts) {
    // Default base job identifier
    const std::string DEFAULT_LIGC_BIN_TEST_JOB_ID("test_ligc_bin");

    // Defaults - assumes working directory is folder containing .vcproj file
    // which is fine when running in visual studio but will need to be
    // explicitly specified with "--output_dir <path>" command line option
    // when testing under a different environment.
    const std::string DEFAULT_LIGC_BIN_TEST_OUTPUT_DIR =
        "../output/test_ligc_bin";

    // Initialize globals to default
    uG::default_init();

    const int result = uTestExportMain(cmd_opts,
                                       DEFAULT_LIGC_BIN_TEST_OUTPUT_DIR,
                                       DEFAULT_LIGC_BIN_TEST_JOB_ID,
                                       uExportLigcBin);

    // Destroy globals
    uG::teardown();

    return result;
}

#endif  // U_BUILD_ENABLE_TESTS
