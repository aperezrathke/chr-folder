#!/bin/bash 

# Script bakes model .bun and compiles resulting C++ code

# This script must be called whenever the model .json or .bun are modified

###################################################
# Script paths
###################################################

# Path to this script's directory
DEM_SCRIPT_PATH="$(readlink -f $0)"
DEM_SCRIPT_DIR="$(dirname $DEM_SCRIPT_PATH)"
DEM_SCRIPT_DIR=$(cd "$DEM_SCRIPT_DIR"; pwd)

# Root project directory
ROOT_DIR="$DEM_SCRIPT_DIR/../.."
# Use absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)

# Determine target output directory
# Base output directory
OUTPUT_DIR="$ROOT_DIR/output"
# https://stackoverflow.com/questions/8223170/bash-extracting-last-two-dirs-for-a-pathname
# Determine name of tutorial folder
DEM_NAME="$(basename $DEM_SCRIPT_DIR)"
# Set target output directory
TARGET_OUTPUT_DIR="$OUTPUT_DIR/demo.$DEM_NAME"

# Path to .bun file
BUN_PATH="$DEM_SCRIPT_DIR/demo.bun"

# Compilation target
COMPILE="linux-gnu-no-avx"

# Determine path to bake script
BAKE_SCRIPT_PATH="$ROOT_DIR/scripts/Template/bake.py"

# Build command line
cmd="python $BAKE_SCRIPT_PATH --bun $BUN_PATH --out $TARGET_OUTPUT_DIR --compile $COMPILE --force"

# Execute bake script
echo "Running demo $DEM_NAME bake:"
echo $cmd
$cmd
echo "Finished tutorial $DEM_NAME bake"
echo "Please inspect output directory: $TARGET_OUTPUT_DIR"

