# Lamina Interactions Demo

Demonstration scripts for spherical nuclear lamina interactions

First, bake demos by executing [bake.sh](./bake.sh)

To run a demo, navigate to corresponding subfolder and execute `run.sh`

Demo output is to `../../output/demo.lam_sphere`

## Demo 0

Simulation with one knock-in lamina interaction [(link)](./0)

## Demo 1

Simulation with one knock-out lamina interaction [(link)](./1)

## Demo 2

Simulation with knock-in and knock-out lamina interaction for same fragment, and with knock-out distance less than knock-in distance which confines fragment to a concentric shell [(link)](./2)

## Demo 3

Simulation with two knock-in lamina interactions [(link)](./3)

## Demo 4

Simulation with two knock-out lamina interactions [(link)](./4)

## Demo 5

Simulation with two knock-in lamina interactions and two knock-out lamina interactions (and with non-overlapping fragments) [(link)](./5)

## Demo 6

Simulation with three knock-in lamina interactions and three knock-out lamina interactions (and with non-overlapping fragments) [(link)](./6)

## Demo 7

Simulation with **bit** masking of the knock-in lamina interactions and **index** masking of the knock-out lamina interactions (and with non-overlapping fragments) [(link)](./7)

## Demo 8

Another simulation with **bit** masking of the knock-in lamina interactions and **index** masking of the knock-out lamina interactions (and with non-overlapping fragments) [(link)](./8)

## Demo 9

Simulation with file-based **bit** masking of the knock-in lamina interactions and file-based **index** masking of the knock-out lamina interactions (and with non-overlapping fragments) [(link)](./9)

## Demo 10

Another simulation with file-based **bit** masking of the knock-in lamina interactions and file-based **index** masking of the knock-out lamina interactions (and with non-overlapping fragments) [(link)](./10)

## Demo 11

Simulation with three knock-in lamina interactions and three knock-out lamina interactions (and with non-overlapping fragments) but with heterogeneous interaction distances set by file (run-length encoded format) [(link)](./11)
