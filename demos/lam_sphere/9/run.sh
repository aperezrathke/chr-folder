#!/bin/bash 

# Script generates samples according to model configuration

# Path to this script's directory
DEM_SCRIPT_PATH="$(readlink -f $0)"
DEM_SCRIPT_DIR="$(dirname $DEM_SCRIPT_PATH)"
DEM_SCRIPT_DIR=$(cd "$DEM_SCRIPT_DIR"; pwd)

# Root project directory
ROOT_DIR="$DEM_SCRIPT_DIR/../../.."
# Use absolute path
ROOT_DIR=$(cd "$ROOT_DIR"; pwd)

# Base output directory
OUTPUT_DIR="$ROOT_DIR/output"
# https://stackoverflow.com/questions/8223170/bash-extracting-last-two-dirs-for-a-pathname
# Determine name of sub-demo folder
SUBDEM_NAME="$(basename $DEM_SCRIPT_DIR)"
# Determine name of parent demo folder
PARDEM_NAME="$(basename $(dirname $DEM_SCRIPT_DIR))"
# Determine parent demo folder
OUTPUT_PARDEM_DIR="$OUTPUT_DIR/demo.$PARDEM_NAME"

# Set export options
EXPORT_ARG="-export_csv -export_pml -export_log_weights"
EXPORT_ARG="$EXPORT_ARG -export_extended -export_nucleus"
EXPORT_ARG="$EXPORT_ARG -export_intr_lam"

# Set .ini configuration option
KEY="$SUBDEM_NAME"
INI_PATH="$DEM_SCRIPT_DIR/demo.ini"
CONF_ARG="--conf $INI_PATH"

# Determine dispatch option
DISPATCH_ARG="--main_dispatch demo$KEY"

# Determine job prefix - all output sample files will have this prefix. Set to
# random value to avoid overwriting previous data!
# e.g. :
#   job_prefix="$(date +%Y%m%d%H%M%S).$RANDOM"
# will ensure that old data is not overwritten. However, this is a demo,
# so we always use the same prefix!
JOB_PREFIX_ARG="--job_id $KEY"

# Determine path to executable binary
EXE_DIR="$OUTPUT_PARDEM_DIR/build/bin/Release_nossecol"
# Executable name
EXE_NAME="u_sac"
EXE_PATH="$EXE_DIR/$EXE_NAME"

# Build command line
cmd="$EXE_PATH $JOB_PREFIX_ARG $DISPATCH_ARG $CONF_ARG $EXPORT_ARG"

# Run command line
echo "Running demo $KEY simulation:"
echo $cmd
$cmd
echo "Finished demo $KEY simulation"

