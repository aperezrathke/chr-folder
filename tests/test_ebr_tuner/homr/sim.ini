# Any of these parameters can be overridden via command line using
# --<option_name> <option_value>
#
# Example, to override output_dir, enter the following command:
# <path_to_exe> --output_dir <overridden path>
output_dir = ../../output/test_ebr_tuner

max_trials = -1

# Number of nodes (monomers) for a chromatin polymer chain.
#
# Format:
# num_nodes_<locus_id> = <positive integer>
#
# where <locus_id> is a unique, sequential 0-based index identifying the
# chromatin chain and <positive integer> is the number of nodes belonging to
# that chain.
#
# Each <locus_id> represents a unique genomic loci.
#
# E.g.:
# num_nodes_0 = 100
#
# will assign 100 chromatin nodes to the 0-th chromatin chain
#
# If <locus_id> is omitted (e.g. - num_nodes = 100), then the <locus_id> will
# default to 0.
num_nodes_0 = 500

# Nodes are rejected if they do not fit within nuclear sphere.
#
# Units: Angstroms
nuclear_diameter = 5229.1

# Number of points to sample on surface of node sphere when growing a chain
# and deciding where to add the next node.
num_unit_sphere_sample_points = 50

# Number of samples to be generated for each run of the program.
# A sample may consist of multiple chromatin chains if modeling multiple loci
# simultaneously.
ensemble_size = 50

# Nodes are rejected if they collide with an existing node based on this
# length.
#
# Note: node == smallest monomer unit modeled when generating a chromatin
# polymer chain.
#
node_diameter_fpath = node_diam.txt

# EBR tuner: Kuhn length in base pairs
# Sanborn, Adrian L., Suhas SP Rao, Su-Chen Huang, Neva C. Durand, Miriam H.
#   Huntley, Andrew I. Jewett, Ivan D. Bochkov et al. "Chromatin extrusion
#   explains key features of loop and domain formation in wild-type and
#   engineered genomes." Proceedings of the National Academy of Sciences 112,
#   no. 47 (2015):E6456-E6465.
ebr_tuner_decorr_bp = 1000

# Target mean cosine (<cos>) at de-correlation length
ebr_tuner_decorr_cos = 0.0

# Core stochastic approximation parameters
ebr_tuner_max_iters = 1000
ebr_tuner_step_decay_rate = 3.0
ebr_tuner_step_scale = 1.0
ebr_tuner_step_smooth_lambda = 0.5
ebr_tuner_step_smooth_count = 10
ebr_tuner_step_smooth_start_iter = 20
ebr_tuner_should_avg = 0

# Restrict tuner search domain to [rigid_min, rigid_max]
ebr_tuner_rigid_min = -20.0
ebr_tuner_rigid_max = 20.0

# Convergence criteria
ebr_tuner_conv_thresh = 0.01
ebr_tuner_conv_iters = 10

# Report tuner status interval
ebr_tuner_log_interval = 1

# Iterations per bond block
ebr_tuner_bond_iters = 1

# EBR tuner: base pair length of each node
ebr_tuner_node_bp_fpath = node_bp.txt

# EBR tuner: output path for bend rigidities
ebr_tuner_out_fpath = ../../output/test_ebr_tuner/rigid.txt
