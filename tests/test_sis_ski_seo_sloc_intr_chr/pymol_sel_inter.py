# Usage: copy and paste into pymol
# Colors target interacting regions
hide everything
show spheres
bg_color white
color grey
select fraga, resi 175-182
select fragb, resi 342-348
color red, fraga
color blue, fragb
