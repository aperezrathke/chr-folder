# Chromatin folding via fractal Monte Carlo #

This program is part of the **CHROMATIX** chromatin folding software suite.

![heterogeneous chromatin](./tutorial/A/A.png)

If using this software, please cite:

* Perez-Rathke, Alan, Qiu Sun, Boshen Wang, Valentina Boeva, Zhifeng Shao, and Jie Liang. "CHROMATIX: computing the functional landscape of many-body chromatin interactions in transcriptionally active loci from deconvolved single cells." Genome Biology 21, no. 1 (2020): 1-17. [https://doi.org/10.1186/s13059-019-1904-z](https://doi.org/10.1186/s13059-019-1904-z).

See also additional **CHROMATIX** software:

* [CMX](https://bitbucket.org/aperezrathke/cmx/) - Markov chain Monte Carlo Bayesian sampler for deconvolving population Hi-C into single-cell contact states
* [MBI-MAM](https://bitbucket.org/aperezrathke/chr-loc-mbi-mam/) - Application of CHROMATIX for detection of **m**any-**b**ody **i**nteractions in the **mam**malian GM12878 cell line

Application framework is based on [SKELeton](https://bitbucket.org/aperezrathke/skel).

## Tutorials ##

* [A: Chromatin folding basics](tutorial/A/tutorial.md)
* [B: Quality control introduction](tutorial/B/tutorial.md)
* [C: Quality control with fractal Monte Carlo](tutorial/C/tutorial.md)
* [D: Quality control with Delphic resampling](tutorial/D/tutorial.md)
* [E: Chromatin-to-chromatin proximity interaction modeling](tutorial/E/tutorial.md)

## Setup ##

Please see [here](setup.md), for Linux/Windows setup instructions.

## License ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
