Note, all systems require Python 2.7 installation for baking (instructions not provided).

## CentOS build instructions

These instructions have been tested on CentOS 7.

#### Git clone repository

```bash
git clone https://<user-name>@bitbucket.org/aperezrathke/chr-folder.git
```

### Install CMake (version >= 2.8)

Enter the following in terminal and respond `y` to prompts:

```bash
sudo yum install cmake
```

#### Install BOOST C++ libraries

```bash
sudo yum install boost-devel
```

#### Install LAPACK

```bash
sudo yum install lapack-devel
```

#### Install Zlib

```bash
sudo yum install zlib-devel
```

To compile, navigate to project folder `scripts/Template/build/linux/` and run compilation shell script (e.g `compile.gnu.sh`).

## Ubuntu build instructions

#### Git clone repository

```bash
git clone https://<user-name>@bitbucket.org/aperezrathke/chr-folder.git
```

#### Install CMake (version >= 2.8), in bash terminal:

```bash
sudo apt-get install cmake
```

#### Install BOOST C++ libraries

```bash
sudo apt-get install libboost-all-dev
```

#### Install PyMOL

```bash 
sudo apt-get install pymol
```

#### Install LAPACK

* In _Synaptic Package Manager_ (or terminal: `sudo apt-get install`):
    * Install `libblas-dev`
    * Install `liblapack-dev`

To compile, navigate to project folder `scripts/Template/build/linux/` and run compilation shell script (e.g `compile.gnu.sh`).

## Windows build instructions

* Obtain source code from [git repository](https://bitbucket.org/aperezrathke/chr-folder).

* [Visual Studio](https://visualstudio.microsoft.com/vs/community/) is the primary environment for compiling under Windows. The following must be installed to use Visual Studio on Windows:
    * Install [Boost](https://www.boost.org/) library:
        * Download [64-bit Windows binaries](https://dl.bintray.com/boostorg/release/1.72.0/binaries/) for version greater >= 1.58.
            * e.g: Download `boost_1_72_0-msvc-14.2-64.exe`
        * Run installer executable and note installation directory used `<boost_dir>`
        * _Set `BOOST_ROOT` Environment Variable_:
            * Visual Studio needs to be aware of the Boost installation directory `<boost_dir>` containing the header files *and* compiled binaries.
            * We need to add an environment variable named `BOOST_ROOT` which has value `<boost_dir>`. In Windows, navigate to `Control Panel > System > Advanced system settings > Environment Variables`. Under *System variables*, press `New` button and create a variable named `BOOST_ROOT` with value `<boost_dir>`, where `<boost_dir>` is the parent folder containing a subfolder with compiled binaries (e.g. `lib64-msvc-*` where `*` is visual studio version) and another subfolder with C++ header files (e.g. `boost`).

To compile, navigate to project folder `vstudio` and open `u_sac.sln` using Visual Studio. Within Visual Studio, press `ctrl+shift+b` to compile solution.

## Misc

Certain utility scripts may require the following python package(s):

* [Namedlist](https://pypi.org/project/namedlist/)
    * `pip install namedlist`
